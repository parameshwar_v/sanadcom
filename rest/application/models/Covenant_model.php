<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Covenant_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function getCovenantsList($data)
    {
        $this->db->select('c.covenant_name,c.frequency_id,mct.*,mc.child_name as covenant_type,mc1.child_name as frequency,GROUP_CONCAT(CONCAT(u.first_name,\' \',u.last_name)) as assigned_to,IF(cp.project_title is null,concat(ct.collateral_type_name," / ",co.collateral_number),cp.project_title) as title');
        $this->db->from('covenant c');
        $this->db->join('master_child mc','c.covenant_type_id=mc.id_child','left');
        $this->db->join('master_child mc1','c.frequency_id=mc1.id_child','left');
        $this->db->join('module_covenant mct','mct.covenant_id=c.id_covenant','right');
        $this->db->join('covenant_user cu','mct.id_module_covenant=cu.module_covenant_id','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('crm_project cp','cp.id_crm_project=mct.module_id and mct.module_type="project"','left');
        $this->db->join('collateral co','co.id_collateral=mct.module_id and mct.module_type="collateral"','left');
        $this->db->join('collateral_type ct','ct.id_collateral_type=co.collateral_type_id','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);

        $this->db->where('c.covenant_status',1);
        if(isset($data['assigned_to']) && isset($data['assigned_to']))
        {
            $this->db->where('(cu.user_id ='.$data['assigned_to'].' or mct.created_by = '.$data['created_by'].')');
        }
        else
        {
            if(isset($data['assigned_to'])){
                $this->db->where('cu.user_id',$data['assigned_to']);
            }
            if(isset($data['created_by'])){
                $this->db->where('mct.created_by',$data['created_by']);
            }
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
        }

        $this->db->group_by('mct.id_module_covenant');
        $this->db->order_by('mct.id_module_covenant','desc');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getModuleCovenantLastReviewDate($data)
    {
        $this->db->select('*,cta.created_date_time as review_date');
        $this->db->from('covenant_task ct');
        $this->db->join('covenant_task_action cta','ct.id_covenant_task=cta.covenant_task_id','left');
        $this->db->where('ct.task_status','submitted');
        $this->db->order_by('cta.id_covenant_task_action','DESC');
        if(isset($data['module_covenant_id']))
            $this->db->where('ct.module_covenant_id',$data['module_covenant_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCovenantsListCount($data)
    {
        $this->db->select('count(DISTINCT(mct.id_module_covenant)) as total');
        $this->db->from('covenant c');
        $this->db->join('module_covenant mct','mct.covenant_id=c.id_covenant','right');
        $this->db->join('covenant_user cu','mct.id_module_covenant=cu.module_covenant_id','left');
        if(isset($data['assigned_to']) && isset($data['assigned_to']))
        {
            $this->db->where('(cu.user_id ='.$data['assigned_to'].' or mct.created_by = '.$data['created_by'].')');
        }
        else
        {
            if(isset($data['assigned_to'])){
                $this->db->where('cu.user_id',$data['assigned_to']);
            }
            if(isset($data['created_by'])){
                $this->db->where('mct.created_by',$data['created_by']);
            }
        }
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);

        $this->db->where('c.covenant_status',1);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCovenants($data)
    {
        $this->db->select('c.*,mc.child_name as covenant_type,mc1.child_name as frequency');
        $this->db->from('covenant c');
        $this->db->join('master_child mc','c.covenant_type_id=mc.id_child','left');
        $this->db->join('master_child mc1','c.frequency_id=mc.id_child','left');

        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['id_covenant']))
            $this->db->where('c.id_covenant',$data['id_covenant']);

        if(isset($data['covenant_ids']))
            $this->db->where_in('c.id_covenant',$data['covenant_ids']);

        if(isset($data['covenant_type_id']) && $data['covenant_type_id'])
            $this->db->where('c.covenant_type_id',$data['covenant_type_id']);

        if(isset($data['sector_id']) && $data['sector_id']){
            $this->db->join('covenant_sector cs','c.id_covenant=cs.covenant_id','left');
            $this->db->where('cs.sector_id',$data['sector_id']);
        }

        if(isset($data['category_id']) &&$data['category_id']){
            $this->db->join('covenant_category cc','c.id_covenant=cc.covenant_id','left');
            $this->db->where('cc.category_id',$data['category_id']);
        }

        if(isset($data['frequency_id']) && $data['frequency_id'])
            $this->db->where('c.frequency_id',$data['frequency_id']);

        $this->db->where('c.covenant_status',1);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
        }
        $this->db->where('c.covenant_status',1);
        $this->db->group_by('id_covenant');
        $this->db->order_by('id_covenant','desc');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function covenantCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('covenant c');

        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['id_covenant']))
            $this->db->where('c.id_covenant',$data['id_covenant']);

        if(isset($data['covenant_type_id']) && $data['covenant_type_id'])
            $this->db->where('c.covenant_type_id',$data['covenant_type_id']);

        if(isset($data['sector_id']) && $data['sector_id']){
            $this->db->join('covenant_sector cs','c.id_covenant=cs.covenant_id','left');
            $this->db->where('cs.sector_id',$data['sector_id']);
        }

        if(isset($data['category_id']) &&$data['category_id']){
            $this->db->join('covenant_category cc','c.id_covenant=cc.covenant_id','left');
            $this->db->where('cc.category_id',$data['category_id']);
        }

        if(isset($data['frequency_id']) && $data['frequency_id'])
            $this->db->where('c.frequency_id',$data['frequency_id']);
        $this->db->where('c.covenant_status',1);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCovenantSector($data)
    {
        $this->db->select('c.sector_id,s.sector_name');
        $this->db->from('covenant_sector c');
        $this->db->join('sector s','c.sector_id=s.id_sector','left');
        if(isset($data['covenant_id']))
            $this->db->where('c.covenant_id',$data['covenant_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCovenantCategory($data)
    {
        $this->db->select('*');
        $this->db->from('covenant_category c');
        if(isset($data['covenant_id']))
            $this->db->where('c.covenant_id',$data['covenant_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addCovenant($data)
    {
        $this->db->insert('covenant', $data);
        return $this->db->insert_id();
    }

    public function addCovenantSector($data)
    {
        $this->db->insert_batch('covenant_sector', $data);
        return $this->db->insert_id();
    }

    public function addCovenantCategory($data)
    {
        $this->db->insert_batch('covenant_category', $data);
        return $this->db->insert_id();
    }

    public function updateCovenant($data)
    {//echo "<pre>11"; print_r($data); exit;
        $this->db->where('id_covenant',$data['id_covenant']);
        $this->db->update('covenant', $data);
        return 1;
    }

    public function deleteCovenant($data)
    {
        $this->db->where('id_covenant',$data['covenant_id']);
        $this->db->delete('covenant');
        return 1;
    }

    public function deleteCovenantSector($data)
    {
        if(isset($data['covenant_id']))
            $this->db->where_in('covenant_id',$data['covenant_id']);
        if(isset($data['sector_id']))
            $this->db->where_in('sector_id',$data['sector_id']);

        $this->db->delete('covenant_sector');
        return 1;
    }

    public function deleteCovenantCategory($data)
    {
        if(isset($data['covenant_id']))
            $this->db->where_in('covenant_id',$data['covenant_id']);
        if(isset($data['category_id']))
            $this->db->where_in('category_id',$data['category_id']);

        $this->db->delete('covenant_category');
        return 1;
    }

    public function getCovenantType()
    {
        $this->db->select('mc.child_name,count(c.id_covenant) as covenants,mc.id_child as covenant_type_id');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id','left');
        $this->db->join('covenant c','mc.id_child=c.covenant_type_id and covenant_status=1','left');
        $this->db->where('m.master_key','covenant_types');
        $this->db->group_by('mc.id_child');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSearchCovenants($data)
    {
        $this->db->select('c.id_covenant,c.covenant_name');
        $this->db->from('covenant c');
        /*$this->db->join('covenant_category cc','c.id_covenant=cc.covenant_id','left');
        $this->db->join('master_child mc2','cc.category_id=mc.id_child','left');*/
        if(isset($data['module_id']))
            $this->db->where('c.id_covenant not in (select covenant_id from module_covenant where module_type="'.$data["module_type"].'" and module_id="'.$data["module_id"].'" and module_covenant_status!="deleted")');

        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['covenant_type_id']) && $data['covenant_type_id'])
            $this->db->where('c.covenant_type_id',$data['covenant_type_id']);

        if(isset($data['sector_id']) && $data['sector_id']){
            $this->db->join('covenant_sector cs','c.id_covenant=cs.covenant_id','left');
            $this->db->where('cs.sector_id',$data['sector_id']);
        }

        if(isset($data['category_id']) &&$data['category_id']){
            $this->db->join('covenant_category cc','c.id_covenant=cc.covenant_id','left');
            $this->db->where('cc.category_id',$data['category_id']);
        }

        if(isset($data['frequency_id']) && $data['frequency_id'])
            $this->db->where('c.frequency_id',$data['frequency_id']);

        /*if(isset($data['module_type']) && $data['module_type']){
            $this->db->where('');
        }*/

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
            /*$this->db->where('mc2.child_name like "%'.$data['search_key'].'%"');*/
        }
        $this->db->where('c.covenant_status',1);
        $this->db->group_by('id_covenant');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getModuleCovenants($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('mc.*,c.covenant_name,c.covenant_description,c.covenant_type_id,mc1.child_name as covenant_type_name,mc2.child_name as frequency_name,mc2.child_key as frequency_key,group_concat(CONCAT(u.first_name," ",u.last_name)) as assigned_to_name,group_concat(cu.user_id) as assigned_to,group_concat(u.email_id) as email,concat(u1.first_name," ",u1.last_name) as created_user_name');
        $this->db->from('module_covenant mc');
        $this->db->join('covenant c','mc.covenant_id=c.id_covenant','left');
        $this->db->join('master_child mc1','c.covenant_type_id=mc1.id_child','left');
        $this->db->join('master_child mc2','mc.covenant_frequency_id=mc2.id_child','left');
        $this->db->join('covenant_user cu','mc.id_module_covenant=cu.module_covenant_id','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('user u1','mc.created_by=u1.id_user','left');

        if(isset($data['module_type']))
            $this->db->where('mc.module_type',$data['module_type']);

        if(isset($data['module_id']))
            $this->db->where('mc.module_id',$data['module_id']);

        if(isset($data['reference_type']))
            $this->db->where('mc.reference_type',$data['reference_type']);

        if(isset($data['reference_id']))
            $this->db->where('mc.reference_id',$data['reference_id']);

        if(isset($data['project_id']))
            $this->db->where('mc.module_id!=',$data['project_id']);

        if(isset($data['id_module_covenant']))
            $this->db->where('mc.id_module_covenant=',$data['id_module_covenant']);

        if(isset($data['notification_date']))
            $this->db->where('mc.notification_date',$data['notification_date']);

        if(isset($data['search_type']) && $data['search_type'] && isset($data['module_id']) && isset($data['search_key']) && $data['search_key']!=''){
            $this->db->join('crm_project cp','mc.module_id=cp.id_crm_project','left');
            $this->db->where('cp.project_title like "%'.$data['search_key'].'%"');
        }
        else if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.covenant_name like "%'.$data['search_key'].'%"');
        }

        $this->db->where('mc.module_covenant_status !=','deleted');

        $this->db->group_by('mc.id_module_covenant');
        $this->db->order_by('mc.id_module_covenant','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getOtherModuleCovenants($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('c.id_covenant,c.covenant_name');
        $this->db->from('module_covenant mc');
        $this->db->join('covenant c','mc.covenant_id=c.id_covenant','left');
        if(isset($data['module_type']) && $data['module_type']=='project')
            $this->db->join('crm_project cp','mc.module_id=cp.id_crm_project','left');
        if(isset($data['module_type']) && $data['module_type']=='collateral')
            $this->db->join('collateral co','mc.module_id=co.id_collateral','left');

        if(isset($data['module_type']))
            $this->db->where('mc.module_type',$data['module_type']);

        if(isset($data['module_id']))
            $this->db->where('mc.module_id!=',$data['module_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            if(isset($data['module_type']) && $data['module_type']=='project')
                $this->db->where('cp.project_title like "%'.$data['search_key'].'%"');
            if(isset($data['module_type']) && $data['module_type']=='collateral')
                $this->db->where('co.collateral_number like "%'.$data['search_key'].'%"');
        }
        $this->db->where('mc.module_covenant_status !=','deleted');
        //$this->db->group_by('mc.covenant_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addBatchModuleCovenant($data)
    {
        $this->db->insert_batch('module_covenant', $data);
        return $this->db->insert_id();
    }

    public function updateModuleCovenant($data)
    {
        $this->db->where('id_module_covenant',$data['id_module_covenant']);
        $this->db->update('module_covenant', $data);
        return 1;
    }

    public function getCovenantUsers($data)
    {
        $this->db->select('*');
        $this->db->from('covenant_user');
        if(isset($data['module_covenant_id']))
            $this->db->where('module_covenant_id',$data['module_covenant_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCovenantUsers($data)
    {
        $this->db->insert_batch('covenant_user', $data);
        return $this->db->insert_id();
    }

    public function deleteCovenantUsers($data)
    {
        if(isset($data['user_id']))
            $this->db->where_in('user_id',$data['user_id']);
        if(isset($data['module_covenant_id']))
            $this->db->where('module_covenant_id',$data['module_covenant_id']);
        $this->db->delete('covenant_user');
        return 1;
    }

    public function deleteModuleCovenant($data)
    {
        if(isset($data['id_module_covenant']))
            $this->db->where('id_module_covenant',$data['id_module_covenant']);
        $this->db->delete('module_covenant');
        return 1;
    }

    public function deleteModuleCovenantUsers($data)
    {
        if(isset($data['module_covenant_id']))
            $this->db->where('module_covenant_id',$data['module_covenant_id']);
        $this->db->delete('covenant_user');
        return 1;
    }

    public function updateTask($data)
    {
        $this->db->where('id_covenant_task',$data['id_covenant_task']);
        $this->db->update('covenant_task', $data);
        return 1;
    }

    public function addTaskAction($data)
    {
        $this->db->insert('covenant_task_action', $data);
        return $this->db->insert_id();
    }

    public function getTaskHistory($data)
    {
        $this->db->select('ct.*,GROUP_CONCAT(CONCAT(u.first_name," ",u.last_name)) as user_name');
        $this->db->from('covenant_task ct');
        $this->db->join('module_covenant mc','ct.module_covenant_id=mc.id_module_covenant','left');
        $this->db->join('covenant_user cu','ct.module_covenant_id=cu.module_covenant_id','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        if(isset($data['module_covenant_id']))
            $this->db->where('ct.module_covenant_id',$data['module_covenant_id']);
        $this->db->order_by('id_covenant_task','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->group_by('ct.id_covenant_task');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskHistoryCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('covenant_task ct');
        $this->db->join('user u','ct.updated_by=u.id_user');
        if(isset($data['module_covenant_id']))
            $this->db->where('module_covenant_id',$data['module_covenant_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTaskActionHistory($data)
    {
        $this->db->select('cta.*,CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('covenant_task_action cta');
        $this->db->join('user u','cta.action_by=u.id_user');
        if(isset($data['covenant_task_id']))
            $this->db->where('covenant_task_id',$data['covenant_task_id']);

        $this->db->order_by('id_covenant_task_action','DESC');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTaskActionHistoryCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('covenant_task_action cta');
        $this->db->join('user u','cta.action_by=u.id_user');
        if(isset($data['covenant_task_id']))
            $this->db->where('covenant_task_id',$data['covenant_task_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCovenantTask($data)
    {
        $this->db->insert('covenant_task', $data);
        return $this->db->insert_id();
    }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_company_model extends CI_Model
{
    function getRatiosCategory($data)
    {
        $this->db->select('rc.*,group_concat(rcs.sector_id) as sector_id');
        $this->db->from('ratio_category rc');
        $this->db->join('ratio_category_sector rcs','rc.id_ratio_category=rcs.ratio_category_id','left');
        if (isset($data['company_id']))
            $this->db->where('rc.company_id', $data['company_id']);
        if (isset($data['id_ratio_category']))
            $this->db->where('rc.id_ratio_category', $data['id_ratio_category']);
        if (isset($data['id_ratio_category_not']))
            $this->db->where('rc.id_ratio_category !=', $data['id_ratio_category_not']);
        if (isset($data['category_name']))
            $this->db->where('rc.category_name', $data['category_name']);
        $this->db->group_by('id_ratio_category');
        $this->db->order_by('id_ratio_category','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addRatioCategory($data)
    {
        $this->db->insert('ratio_category', $data);
        return $this->db->insert_id();
    }

    public function updateRatioCategory($data)
    {
        $this->db->where('id_ratio_category',$data['id_ratio_category']);
        $this->db->update('ratio_category',$data);
        return 1;
    }

    public function getRatioCategorySector($data)
    {
        $this->db->select('*');
        $this->db->from('ratio_category_sector');
        if (isset($data['ratio_category_id']))
            $this->db->where('ratio_category_id', $data['ratio_category_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addRatioCategorySector($data)
    {
        $this->db->insert_batch('ratio_category_sector', $data);
        return $this->db->insert_id();
    }

    public function deleteRatioCategorySector($data)
    {
        $this->db->where(array('ratio_category_id' => $data['ratio_category_id'],'sector_id' => $data['sector_id']));
        $this->db->delete('ratio_category_sector');
        return 1;
    }

    function getRatios($data)
    {
        $this->db->select('*');
        $this->db->from('ratio');
        if(isset($data['ratio_category_id']))
            $this->db->where('ratio_category_id',$data['ratio_category_id']);
        $this->db->order_by('id_ratio','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function addRatios($data)
    {
        $this->db->insert('ratio', $data);
        return $this->db->insert_id();
    }

    function updateRatios($data)
    {
        $this->db->where('id_ratio',$data['id_ratio']);
        $this->db->update('ratio',$data);
        return 1;
    }

    function getCompanyRatiosList($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_ratio cr');

        if (isset($data['id_crm_company_ratio']))
            $this->db->where('cr.id_crm_company_ratio', $data['id_crm_company_ratio']);
        if (isset($data['crm_company_id']))
            $this->db->where('cr.crm_company_id', $data['crm_company_id']);
        $this->db->order_by('id_crm_company_ratio','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function getCompanyRatiosData($data)
    {
        $this->db->select('r.*');
        $this->db->from('ratio_category rc');
        $this->db->join('ratio r','rc.id_ratio_category=r.ratio_category_id','');
        $this->db->join('ratio_category_sector rcs','rc.id_ratio_category=rcs.ratio_category_id','left');

        if (isset($data['id_crm_company_ratio'])) {
            $this->db->select('cr.id_crm_company_ratio_data,cr.crm_company_ratio_id,cr.ratio_value,cr.upper_ceiling as company_upper_ceiling,cr.lower_ceiling as company_lower_ceiling');
            $this->db->join('crm_company_ratio_data cr','r.id_ratio=cr.ratio_id','left');
            $this->db->where('cr.crm_company_ratio_id',$data['id_crm_company_ratio']);
        }
        if (isset($data['sector_id']))
            $this->db->where('rcs.sector_id', $data['sector_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    function addCompanyRatios($data)
    {
        $this->db->insert('crm_company_ratio', $data);
        return $this->db->insert_id();
    }

    function addCompanyRatiosData($data)
    {
        $this->db->insert('crm_company_ratio_data', $data);
        return $this->db->insert_id();
    }

    function updateCompanyRatios($data)
    {
        $this->db->where('id_crm_company_ratio',$data['id_crm_company_ratio']);
        $this->db->update('crm_company_ratio',$data);
        return 1;
    }

    function deleteCompanyRatios($data)
    {
        $this->db->where('id_crm_company_ratio',$data['id_crm_company_ratio']);
        $this->db->delete('crm_company_ratio');
        return 1;
    }

    function getCompanyRatioTrend($data)
    {
        $this->db->select('ccrt.year,ccrt.trend_value as value');
        $this->db->from('crm_company_ratio_trend ccrt');
        if(isset($data['crm_company_ratio_data_id']))
            $this->db->where('ccrt.crm_company_ratio_data_id',$data['crm_company_ratio_data_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function addCompanyRatioTrend($data)
    {
        $this->db->insert_batch('crm_company_ratio_trend', $data);
        return $this->db->insert_id();
    }

    function deleteCompanyRatioTrend($data)
    {
        $this->db->where('crm_company_ratio_id',$data['id_crm_company_ratio']);
        $this->db->delete('crm_company_ratio_trend');
        return 1;
    }

    function deleteCompanyRatioDataTrend($data)
    {
        $this->db->query('delete tr from crm_company_ratio_trend tr left join crm_company_ratio_data rd on rd.id_crm_company_ratio_data=tr.crm_company_ratio_data_id where rd.crm_company_ratio_id='.$data['id_crm_company_ratio']);
        //$this->db->query('delete from crm_company_ratio_data rd where rd.crm_company_ratio_id='.$data['id_crm_company_ratio']);
        $this->db->where('crm_company_ratio_id', $data['id_crm_company_ratio']);
        $this->db->delete('crm_company_ratio_data');
    }

    function getProjectCheckList($data)
    {
        $this->db->select('*');
        $this->db->from('project_checklist pc');
        if(isset($data['crm_project_id']))
            $this->db->where('pc.crm_project_id',$data['crm_project_id']);
        if(isset($data['project_form_id']))
            $this->db->where('pc.project_form_id',$data['project_form_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getProjectCheckListQuestions($data)
    {
        $this->db->select('aq.*,pc.*,group_concat(pcf.project_facility_id) as project_facility_id');
        $this->db->from('assessment_question aq');
        $this->db->join('project_checklist pc','aq.id_assessment_question=pc.assessment_question_id','left');
        $this->db->join('project_checklist_facility pcf','pc.id_project_checklist=pcf.project_checklist_id','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pc.crm_project_id',$data['crm_project_id']);
        if(isset($data['assessment_question_category_id']))
            $this->db->where('aq.assessment_question_category_id',$data['assessment_question_category_id']);
        if(isset($data['project_form_id']))
            $this->db->where('pc.project_form_id',$data['project_form_id']);
        $this->db->group_by('aq.id_assessment_question');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function addProjectCheckList_batch($data)
    {
        $this->db->insert_batch('project_checklist', $data);
        return $this->db->insert_id();
    }

    function deleteCheckList($data)
    {
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id', $data['crm_project_id']);
        if(isset($data['assessment_question_id']))
            $this->db->where('assessment_question_id', $data['assessment_question_id']);
        if(isset($data['id_project_checklist']))
            $this->db->where('id_project_checklist', $data['id_project_checklist']);
        $this->db->delete('project_checklist');
        return 1;
    }

    function getChecklistFacility($data)
    {
        $this->db->select('*');
        $this->db->from('project_checklist_facility');
        if(isset($data['project_checklist_id']))
            $this->db->where('project_checklist_id',$data['project_checklist_id']);
        if(isset($data['project_facility_id']))
            $this->db->where('project_facility_id',$data['project_facility_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function addChecklistFacility($data)
    {
        $this->db->insert('project_checklist_facility', $data);
        return $this->db->insert_id();
    }

    function updateChecklistFacility($data)
    {
        $this->db->where('id_project_checklist_facility',$data['id_project_checklist_facility']);
        $this->db->update('project_checklist_facility',$data);
        return 1;
    }

    function deleteCheckListFacility($data)
    {
        if(isset($data['project_checklist_id']))
            $this->db->where('project_checklist_id', $data['project_checklist_id']);
        if(isset($data['project_facility_id']))
            $this->db->where('project_facility_id',$data['project_facility_id']);
        $this->db->delete('project_checklist_facility');
        return 1;
    }

    function getContactIdentification($data=array())
    {
        $this->db->select('ci.*,mc.child_name as contact_identification_name');
        $this->db->from('contact_identification ci');
        $this->db->join('master_child mc','ci.type_of_id=mc.id_child','left');
        if(isset($data['crm_contact_id']))
            $this->db->where('crm_contact_id',$data['crm_contact_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function addContactIdentification($data)
    {
        $this->db->insert('contact_identification', $data);
        return $this->db->insert_id();
    }

    function updateContactIdentification($data)
    {
        $this->db->where('id_contact_identification',$data['id_contact_identification']);
        $this->db->update('contact_identification',$data);
        return 1;
    }

    function deleteContactIdentification($data)
    {
        $this->db->where('id_contact_identification',$data['id_contact_identification']);
        $this->db->delete('contact_identification');
        return 1;
    }

    function getExistingCompanyRelation($data)
    {
        $this->db->select('crm_company_id as company_id');
        $this->db->from('crm_company_relation');
        $this->db->where('parent_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        $subQuery1 = $this->db->last_query();
        $query->result_array();

        $this->db->select('parent_company_id as company_id');
        $this->db->from('crm_company_relation');
        $this->db->where('crm_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        $subQuery2 = $this->db->last_query();
        $query->result_array();

        $query = $this->db->query("$subQuery1 UNION All $subQuery2");
        return $query->result_array();
    }
    function isCompanyRelationPresent($data){
        $this->db->select('*');
        $this->db->from('crm_company_shareholder_details');
        $this->db->where('crm_company_id' , $data['crm_company_id']);
        $this->db->where('module_id' , $data['module_id']);
        $this->db->where('module_type' , 'company');
        $query = $this->db->get();
        return $query->result_array();
    }
    function getCompanyRelationSearch($data)
    {
        $this->db->select('id_crm_company,company_name');
        $this->db->from('crm_company');
        if(isset($data['crm_company_id']) && !empty($data['crm_company_id']))
            $this->db->where_not_in('id_crm_company',$data['crm_company_id']);
        if(isset($data['crm_company_ids']) && !empty($data['crm_company_ids']))
            $this->db->where_not_in('id_crm_company',$data['crm_company_ids']);
        if(isset($data['search_key']))
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        $query = $this->db->get();
        return $query->result_array();
    }

    /*function getParentCompany($data)
    {
        $this->db->select('c.company_name,c.email,c.phone_number,c.company_description');
        $this->db->from('crm_company_relation s');
        $this->db->join('crm_company c','s.parent_company_id=c.id_crm_company','left');
        $this->db->where('crm_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getChildCompany($data)
    {
        $this->db->select('c.company_name,c.email,c.phone_number,c.company_description');
        $this->db->from('crm_company_relation s');
        $this->db->join('crm_company c','s.crm_company_id=c.id_crm_company','left');
        $this->db->where('parent_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }*/

    function getParentCompany($data){
        $this->db->select('s.id_crm_company_relation,c.id_crm_company,c.company_name,c.email,c.phone_number,c.company_description,mc.child_name');
        $this->db->from('crm_company_relation s');
        $this->db->join('crm_company c','s.parent_company_id=c.id_crm_company','left');
        $this->db->join('master_child mc','mc.id_child=s.relation_id','left');
        $this->db->where('crm_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getChildCompany($data){
        $this->db->select('s.id_crm_company_relation,c.id_crm_company,c.company_name,c.email,c.phone_number,c.company_description,mc.child_name');
        $this->db->from('crm_company_relation s');
        $this->db->join('crm_company c','s.crm_company_id=c.id_crm_company','left');
        $this->db->join('master_child mc','mc.id_child=s.relation_id','left');
        $this->db->where('parent_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function addCompanyRelation($data)
    {//echo "<pre>"; print_r($data); exit;
        $this->db->insert('crm_company_relation', $data);
        return $this->db->insert_id();
    }
    function deleteCompanyRelation($data)
    {
        $this->db->where('id_crm_company_relation',$data['id_crm_company_relation']);
        $this->db->delete('crm_company_relation');
        return 1;
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function getCustomersByCountry()
    {
        $this->db->select('count(*) as total_customers,cn.id_country,cn.country_name');
        $this->db->join('country cn','cn.id_country=c.country_id','left');
        $this->db->from('company c');
        $this->db->group_by('c.country_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCustomersByCategory()
    {
        $this->db->select('count(*) as total_companies,bc.id_bank_category,bc.bank_category_name,bc.bank_category_code');
        $this->db->from('company c');
        $this->db->join('bank_category bc','bc.id_bank_category=c.bank_category_id','left');
        $this->db->group_by('c.bank_category_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    /*public function getLoanOfficerDashboardDetails($userId,$dashboardDate)
    {
        $this->db->select('`u`.`id_user`,u.profile_image,ar.approval_name, `cb`.`branch_logo`, `cb`.`branch_code`, `cb`.`legal_name`, `ct`.`country_name`,
                           COUNT(DISTINCT cp.id_crm_project) as customers, IFNULL(ROUND(SUM(os.usd_equiv), 2), 0) as exposure,
                           (SELECT IFNULL(ROUND(SUM(pf_1.facility_usd_amount), 2), 0) as facility_amount FROM project_facility pf_1 JOIN crm_project cp_1 ON pf_1.crm_project_id=cp_1.id_crm_project AND cp_1.project_status="Pipeline" WHERE cp_1.created_by='.$userId.' AND DATE_FORMAT(cp_1.created_date_time, "%Y-%m")<=DATE_FORMAT("'.$dashboardDate.'", "%Y-%m") AND (pf_1.project_facility_status = "active" OR pf_1.project_facility_status = "approved") ) as pipeline');
        $this->db->from('`user` u');
        $this->db->join('company_user cu','u.id_user=cu.user_id','left');
        $this->db->join('company_approval_role car','cu.company_approval_role_id = car.id_company_approval_role','left');
        $this->db->join('approval_role ar','car.approval_role_id = ar.id_approval_role','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('country ct','cb.country_id=ct.id_country','left');
        $this->db->join('crm_project cp','u.id_user=cp.created_by','left');
        $this->db->join('project_company pc','cp.id_crm_project=pc.crm_project_id','left');
        $this->db->join('project_facility pf','pc.crm_project_id=pf.crm_project_id AND (`pf`.`project_facility_status` = "active" OR `pf`.`project_facility_status` = "approved")','left');
        $this->db->join('facility_disbursement fd','pf.contract_id=fd.contract_id','left');
        $this->db->join('outstanding_amount os','fd.contract_id=os.contract_id AND DATE_FORMAT(os.outstanding_date, "%Y-%m")=DATE_FORMAT(\''.$dashboardDate.'\', "%Y-%m")','left');
        $this->db->where('u.id_user',$userId);
        $this->db->group_by('cp.company_id');
        $query = $this->db->get();
        echo $this->db->last_query();exit;
        return $query->result_array();
    }*/
    public function getDashboardLoanOfficerDetails($userId)
    {
        $this->db->select('`u`.`id_user`, `u`.`profile_image`, `ar`.`approval_name`, `cb`.`branch_logo`, `cb`.`branch_code`, `cb`.`legal_name`, `ct`.`country_name`');
        $this->db->from('`user` u');
        $this->db->join('company_user cu','u.id_user=cu.user_id');
        $this->db->join('company_approval_role car','cu.company_approval_role_id = car.id_company_approval_role');
        $this->db->join('approval_role ar','car.approval_role_id = ar.id_approval_role');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch');
        $this->db->join('country ct','cb.country_id=ct.id_country');
        $this->db->where('u.id_user',$userId);
        $query = $this->db->get();
        return (array)$query->row();
    }
    public function getDashboardTotalCustomersAndExposure($userId,$outstandingDate)
    {
        $this->db->select('COUNT(DISTINCT pc.id_project_company) as customers,IFNULL(ROUND(SUM(oa.usd_equiv),2),0) as exposure');
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','cp.id_crm_project = pf.crm_project_id');
        $this->db->join('project_company pc','cp.id_crm_project = pc.crm_project_id');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id');
        $this->db->where('oa.outstanding_date',$outstandingDate);
        $this->db->where_in('cp.created_by',$userId);
        $query = $this->db->get();
        return (array)$query->row();
    }
    public function getDashboardPipelineData($userId,$createdDate)
    {
        $this->db->select('IFNULL(ROUND(SUM(pf_1.facility_usd_amount), 2), 0) as pipeline');
        $this->db->from('project_facility pf_1');
        $this->db->join('crm_project cp_1','pf_1.crm_project_id=cp_1.id_crm_project');
        $this->db->where('cp_1.project_status','Pipeline');
        $this->db->where('DATE_FORMAT(cp_1.created_date_time, "%Y-%m") = DATE_FORMAT("'.$createdDate.'", "%Y-%m")');
        $this->db->where_in('cp_1.created_by',$userId);
        $query = $this->db->get();
        return (array)$query->row();
    }

    public function getLoanOfficerDashboardStageAmountGraphData($data)
    {
        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) >= '".$data['startDate']."') AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) <= '".$data['endDate']."') AND
                (tm.serial < DATE_FORMAT('".$data['endDate']."','%c'))";
        $this->db->select('IFNULL(SUM(pf.facility_usd_amount), 0) as preapprovals,tm.`name`');
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','cp.id_crm_project=pf.crm_project_id and (cp.project_status = "new" OR cp.project_status = "ITS Accepted")','left');
        $this->db->join('temp_month tm','DATE_FORMAT(pf.created_date_time,"%M") = `tm`.`name`','right');
        $this->db->where($where);
        $this->db->group_by('tm.serial');
        $this->db->order_by('tm.serial','ASC');
        $query = $this->db->get();
        $preapprovals=$query->result_array();

        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) >= '".$data['startDate']."') AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) <= '".$data['endDate']."') AND
                (tm.serial < DATE_FORMAT('".$data['endDate']."','%c'))";
        $this->db->select('IFNULL(SUM(pf.facility_usd_amount), 0) as approvals,tm.`name`');
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','cp.id_crm_project=pf.crm_project_id and cp.project_status = "Approved"','left');
        $this->db->join('temp_month tm','DATE_FORMAT(pf.created_date_time,"%M") = `tm`.`name`','right');
        $this->db->where($where);
        $this->db->group_by('tm.serial');
        $this->db->order_by('tm.serial','ASC');
        $query = $this->db->get();
        $approvals=$query->result_array();

        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) >= '".$data['startDate']."') AND
                (pf.created_date_time IS NULL OR DATE(pf.created_date_time) <= '".$data['endDate']."') AND
                (tm.serial < DATE_FORMAT('".$data['endDate']."','%c'))";
        $this->db->select('IFNULL(SUM(fd.usd_equiv), 0) as disbursements,tm.`name`');
        $this->db->from('temp_month tm');
        $this->db->join('crm_project cp','tm.`name` = DATE_FORMAT(cp.created_date_time,"%M")','left');
        $this->db->join('project_company pc','`cp`.`id_crm_project`=`pc`.`crm_project_id`','left');
        $this->db->join('project_facility pf','`pc`.`crm_project_id`=`pf`.`crm_project_id`','left');
        $this->db->join('facility_disbursement fd','`pf`.`contract_id`=`fd`.`contract_id`','left');
        $this->db->where($where);
        $this->db->group_by('tm.serial');
        $this->db->order_by('tm.serial','ASC');
        $query = $this->db->get();
        $disbursements=$query->result_array();

        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (os.outstanding_date IS NULL OR DATE(os.outstanding_date) >= '".$data['startDate']."') AND
                (os.outstanding_date IS NULL OR DATE(os.outstanding_date) <= '".$data['endDate']."') AND
                (tm.serial < DATE_FORMAT('".$data['endDate']."','%c'))";
        $this->db->select('IFNULL(ROUND(SUM(os.usd_equiv), 2),0) as collections, `tm`.`name`');
        $this->db->from('temp_month tm');
        $this->db->join('crm_project cp','tm.`name` = DATE_FORMAT(cp.created_date_time,"%M")','left');
        $this->db->join('project_company pc','`cp`.`id_crm_project`=`pc`.`crm_project_id`','left');
        $this->db->join('project_facility pf','`pc`.`crm_project_id`=`pf`.`crm_project_id`','left');
        $this->db->join('outstanding_amount os','pf.contract_id = os.contract_id','left');
        $this->db->where($where);
        $this->db->group_by('tm.serial');
        $this->db->order_by('tm.serial','ASC');
        $query = $this->db->get();
        $collections=$query->result_array();
        $result = [];

        for($i=0;$i<count($preapprovals);$i++)
        {
            if(!isset($approvals[$i]['approvals']))
                $approvals[$i]['approvals']=0;

            if(!isset($disbursements[$i]['disbursements']))
                $disbursements[$i]['disbursements']=0;

            if(!isset($collections[$i]['collections']))
                $collections[$i]['collections']=0;

            $result[$preapprovals[$i]['name']]['approvals']=$approvals[$i]['approvals'];
            $result[$preapprovals[$i]['name']]['preapprovals']=$preapprovals[$i]['preapprovals'];
            $result[$preapprovals[$i]['name']]['disbursements']=$disbursements[$i]['disbursements'];
            $result[$preapprovals[$i]['name']]['collections']=$collections[$i]['collections'];
        }
        return $result;
    }

    public function getLoanOfficerDashboardStageAmountData($data)
    {
        $query = $this->db->query("call getLoanOfficerDashboardStageAmountData(".$data['user_id'].",'".$data['currentDate']."','".$data['outstandingDate']."')");
        $result =  $query->result_array();
        return $result;
    }

    public function getLoanOfficerDashboardSectorWiseLoans($data)
    {
        $this->db->select('sc.sector_name,COUNT(cp.id_crm_project) as loans');
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','cp.id_crm_project = pf.crm_project_id','left');
        $this->db->join('sector sc','cp.project_main_sector_id = sc.id_sector','left');
        $this->db->where('cp.created_by',$data['user_id']);
        $this->db->where('cp.project_main_sector_id !=','');
        $this->db->group_by('sc.id_sector');
        $this->db->order_by('sc.sector_name','ASC');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }

    public function getChildUserListById($userId_list)
    {
        $this->db->select('u.id_user,CONCAT_WS(" ", u.first_name, u.last_name) AS user_name');
        $this->db->from('user u');
        $this->db->where_in('id_user',$userId_list);
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function getUserCollections($data,$dashboardDate)
    {
        $where="(cp.created_by = ".$data['user_id'].") AND
                (DATE_FORMAT(flr.due_date,'%Y-%m-%d') >= DATE_FORMAT('".$dashboardDate."','%Y-%m-%d'))";
        $this->db->select("cp.id_crm_project,cp.project_title,flr.due_date,flr.usd_equiv,flr.total_due,flr.currency,pf.contract_id,ccm.company_name,ccm.id_crm_company,
                           (SELECT IFNULL(SUM(os.usd_equiv),0) as outstanding_amount FROM outstanding_amount os WHERE contract_id=flr.contract_id AND DATE_FORMAT(os.outstanding_date, '%Y-%m')=DATE_FORMAT('".$dashboardDate."', '%Y-%m')) as outstanding_amount");
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','cp.id_crm_project=pf.crm_project_id','left');
        $this->db->join('project_company pc','cp.id_crm_project=pc.crm_project_id','left');
        $this->db->join('crm_company ccm','pc.crm_company_id = ccm.id_crm_company','left');
        $this->db->join('facility_loan_repayment flr','flr.contract_id=pf.contract_id');
        $this->db->where($where);
        $this->db->group_by('cp.id_crm_project');
        $this->db->order_by('flr.due_date');
        $this->db->limit($data['limit'], $data['offset']);
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function getProjectCountryList($data)
    {
        $this->db->select("cp.country_id,ct.country_name,ROUND(IFNULL(SUM(oa.usd_equiv),0),2) as facility_amount");
        $this->db->from('crm_project cp');
        $this->db->join('country ct','cp.country_id=ct.id_country');
        $this->db->join('project_facility pf','cp.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id');
        $this->db->where('cp.created_by',$data['user_id']);
        $this->db->where('DATE_FORMAT(oa.outstanding_date,"%Y-%m") = DATE_FORMAT("'.$data['outstandingDate'].'", "%Y-%m")');
        $this->db->group_by('cp.country_id');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function getCountryWiseProjectList($data)
    {
        $this->db->select("c.id_crm_company,ct.country_name,c.company_name,count(pf.id_project_facility) as facility_count,ROUND(IFNULL(SUM(ou.usd_equiv), 0),2) as amount,ffd.field_name,ccd.form_field_value,GROUP_CONCAT(ag.`status` ORDER BY ag.analysis_month DESC) as cp_status");
        $this->db->from('crm_company c');
        $this->db->join('country ct','c.country_id = ct.id_country','LEFT');
        $this->db->join('project_company pc','pc.crm_company_id=c.id_crm_company and company_type_id=1','RIGHT');
        $this->db->join('crm_project cp','pc.crm_project_id=cp.id_crm_project');
        $this->db->join('project_facility pf','cp.id_crm_project=pf.crm_project_id');
        $this->db->join('outstanding_amount ou','pf.contract_id=ou.contract_id','LEFT');
        $this->db->join('aging ag','ag.contract_id = ou.contract_id AND ag.record_status=1','LEFT');
        $this->db->join('crm_company_data ccd','c.id_crm_company = ccd.crm_company_id');
        $this->db->join('form_field ffd','ccd.form_field_id = ffd.id_form_field');
        $this->db->where_in('ffd.field_name',array('company_latitude','company_langitude'));
        $this->db->where('DATE_FORMAT(ou.outstanding_date,"%Y-%m") = DATE_FORMAT("'.$data['outstandingDate'].'", "%Y-%m")');
        $this->db->where('DATE_FORMAT(ag.analysis_month,"%Y-%m") = DATE_FORMAT("'.$data['agingDate'].'", "%Y-%m")');
        $this->db->where('cp.created_by',$data['user_id']);
        $this->db->where('cp.country_id',$data['country_id']);
        $this->db->group_by('pc.crm_company_id,ffd.field_name');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }

    public function getNotificationsCountByFilter($data)
    {
        $this->db->select("COUNT(nt.id_notification) as total");
        $this->db->from('notification nt');
        $this->db->join('user u','nt.assigned_by=u.id_user','left');
        $this->db->join('company_user cu','cu.user_id=nt.assigned_by','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->where('nt.notification_status','unread');
        $this->db->where('nt.assigned_to',$data['user_id']);
        $this->db->where('nt.notification_type',$data['filter']);
        if($data['filter'] == 'notification')
            $this->db->where('nt.module_reference_type','workflow');
        $query = $this->db->get();
        $result=$query->result_array();
        return $result;
    }

    public function DashboardStageAmountGraphData($data)
    {
        $result = [];
        $result['pipeline']=0;
        $result['approved']=0;
        $result['disbursements']=0;
        $result['collections']=0;
        /*select ps.project_stage_complete_status as project_status,sum(pswf.usd_equal_amount) as outstanding_amount from crm_project p
join project_stage_info psf on psf.project_id = p.id_crm_project and DATE_FORMAT(psf.end_date_time,"%m-%Y") = '10-2016'
join project_stage ps on ps.id_project_stage = psf.stage_id
join project_stage_workflow_phase pswp on pswp.project_stage_info_id = psf.id_project_stage_info
join project_stage_workflow_facility pswf on pswf.project_stage_workflow_phase_id = pswp.id_project_stage_workflow_phase
where ((psf.stage_id = 2 and psf.project_stage_status = 'in progress') or psf.project_stage_status = 'approved') and psf.stage_id < 4 AND p.created_by=252
group by ps.project_stage_complete_status*/
        $where="((psf.stage_id = 2 and psf.project_stage_status = 'in progress') or psf.project_stage_status = 'approved') and psf.stage_id < 4 AND p.created_by=".$data['user_id'];
        $this->db->select('ps.project_stage_complete_status as project_status,sum(pswf.usd_equal_amount) as outstanding_amount');
        $this->db->from('crm_project p');
        $this->db->join('project_stage_info psf','psf.project_id = p.id_crm_project and DATE_FORMAT(psf.end_date_time,"%m-%Y") = DATE_FORMAT("'.$data['stagingDate'].'","%m-%Y")');
        $this->db->join('project_stage ps','ps.id_project_stage = psf.stage_id');
        $this->db->join('project_stage_workflow_phase pswp','pswp.project_stage_info_id = psf.id_project_stage_info');
        $this->db->join('project_stage_workflow_facility pswf','pswf.project_stage_workflow_phase_id = pswp.id_project_stage_workflow_phase');
        $this->db->where($where);
        $this->db->group_by('ps.project_stage_complete_status');
        $query = $this->db->get();
        $preapprovals_approvals=$query->result_array();
        if(count($preapprovals_approvals) > 0)
        {
            for($i=0;$i<count(count($preapprovals_approvals));$i++)
            {
                $result[$preapprovals_approvals[$i]['project_status']]=$preapprovals_approvals[$i]['outstanding_amount'];
            }
        }

        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (pf.created_date_time IS NULL OR DATE_FORMAT(pf.created_date_time,'%m-%Y') = DATE_FORMAT('".$data['stagingDate']."','%m-%Y'))";
        $this->db->select('ROUND(IFNULL(SUM(fd.usd_equiv), 0),2) as disbursements');
        $this->db->from('crm_project cp');
        $this->db->join('project_company pc','`cp`.`id_crm_project`=`pc`.`crm_project_id`','left');
        $this->db->join('project_facility pf','`pc`.`crm_project_id`=`pf`.`crm_project_id`','left');
        $this->db->join('facility_disbursement fd','`pf`.`contract_id`=`fd`.`contract_id`','left');
        $this->db->where($where);
        $query = $this->db->get();
        $disbursements=$query->result_array();
        $result['disbursements']=$disbursements[0]['disbursements'];


        $where="(cp.created_by IS NULL OR cp.created_by = ".$data['user_id'].") AND
                (os.outstanding_date IS NULL OR DATE_FORMAT(os.outstanding_date,'%m-%Y') = DATE_FORMAT('".$data['stagingDate']."','%m-%Y'))";
        $this->db->select('ROUND(IFNULL(SUM(os.usd_equiv), 0),2) as collections');
        $this->db->from('crm_project cp');
        $this->db->join('project_company pc','`cp`.`id_crm_project`=`pc`.`crm_project_id`','left');
        $this->db->join('project_facility pf','`pc`.`crm_project_id`=`pf`.`crm_project_id`','left');
        $this->db->join('outstanding_amount os','pf.contract_id = os.contract_id','left');
        $this->db->where($where);
        $query = $this->db->get();
        $collections=$query->result_array();
        $result['collections']=$collections[0]['collections'];

        /*for($i=0;$i<count($preapprovals);$i++)
        {
            if(!isset($approvals[$i]['approvals']))
                $approvals[$i]['approvals']=0;

            if(!isset($disbursements[$i]['disbursements']))
                $disbursements[$i]['disbursements']=0;

            if(!isset($collections[$i]['collections']))
                $collections[$i]['collections']=0;

            $result[$preapprovals[$i]['name']]['approvals']=$approvals[$i]['approvals'];
            $result[$preapprovals[$i]['name']]['preapprovals']=$preapprovals[$i]['preapprovals'];
            $result[$preapprovals[$i]['name']]['disbursements']=$disbursements[$i]['disbursements'];
            $result[$preapprovals[$i]['name']]['collections']=$collections[$i]['collections'];
        }*/
        return $result;

    }

    public function getLatestOutstandingDate()
    {
        $this->db->select("MAX(outstanding_date) as outstanding_date");
        $this->db->from('outstanding_amount');
        $query = $this->db->get();
        $outstanding=$query->row();
        return $outstanding->outstanding_date;
    }
    public function getLatestAgingDate()
    {
        $this->db->select("MAX(analysis_month) as analysis_month");
        $this->db->from('aging');
        $query = $this->db->get();
        $aging=$query->row();
        return $aging->analysis_month;
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function getProject($data)
    {
        $this->db->select('c.*,DATE_FORMAT(c.updated_date_time,"%b %d %Y") as updated_date,p.product_name,s.sector_name as sector,s1.sector_name as sub_sector,cr.currency_code');
        $this->db->from('crm_project c');
        $this->db->join('company cm','c.company_id=cm.id_company','left');
        $this->db->join('currency cr','cm.currency_id=cr.id_currency','left');
        $this->db->join('product p','c.product_id=p.id_product','left');
        $this->db->join('sector s','c.project_main_sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        if(isset($data['crm_project_id']))
            $this->db->where('id_crm_project',$data['crm_project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectCompany($data)
    {
        $this->db->select('*');
        $this->db->from('project_company m');
        $this->db->join('crm_company c','c.id_crm_company=m.crm_company_id','left');
        if(isset($data['crm_project_id']))
            $this->db->where('m.crm_project_id',$data['crm_project_id']);
        if(isset($data['crm_company_id']))
            $this->db->where('m.crm_company_id',$data['crm_company_id']);
        if(isset($data['company_type_id']))
            $this->db->where('m.company_type_id',$data['company_type_id']);
        $this->db->where('m.project_company_status','1');
        $this->db->order_by('m.id_project_company','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectContact($data)
    {
        $this->db->select('*');
        $this->db->from('project_contact m');
        $this->db->join('crm_contact c','c.id_crm_contact=m.crm_contact_id','left');
        $this->db->where('m.crm_project_id',$data['crm_project_id']);
        $this->db->where('m.project_contact_status','1');
        $this->db->order_by('m.id_project_contact','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStageForms($data)
    {
        $this->db->select('*');
        //$this->db->from('project_stage_info psi');
        //$this->db->join('project_stage ps','psi.stage_id=ps.id_project_stage','left');
        $this->db->from('project_stage ps');
        $this->db->join('project_stage_section pss','ps.id_project_stage=pss.project_stage_id','');
        $this->db->join('project_section pse','pss.project_section_id=pse.id_project_section','');
        $this->db->join('project_stage_section_form pssf','pss.id_project_stage_section=pssf.project_stage_section_id','');
        $this->db->join('project_form pfo','pssf.project_form_id=pfo.id_project_form','');
        if(isset($data['crm_project_id']))
            $this->db->join('project_stage_info psi',' psi.stage_id = ps.id_project_stage and psi.project_id='.$data['crm_project_id'],'');
        $this->db->order_by('ps.stage_order','DESC');
        $this->db->order_by('project_stage_section_order','ASC');
        $this->db->order_by('project_stage_section_form_order','ASC');

        if(isset($data['project_stage_id']))
            $this->db->where('ps.id_project_stage',$data['project_stage_id']);
        if(isset($data['group_by']) && $data['group_by']=='project_form_id')
            $this->db->group_by('pssf.project_form_id');
        if(isset($data['project_stage_info_id']))
            $this->db->where('psi.id_project_stage_info',$data['project_stage_info_id']);
        if(isset($data['crm_project_id']))
            $this->db->where('psi.project_id',$data['crm_project_id']);

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getProjectStageSection($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_section pss');
        $this->db->join('project_section ps','pss.project_section_id=ps.id_project_section');
        if(isset($data['id_project_stage_section']))
            $this->db->where('pss.id_project_stage_section',$data['id_project_stage_section']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectStageSectionForm($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_section_form pssf');
        $this->db->join('project_form pf','pssf.project_form_id=pf.id_project_form','left');
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('pssf.id_project_stage_section_form',$data['project_stage_section_form_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormDetails($data)
    {
        $this->db->select('*');
        $this->db->from('project_form pf');
        if(isset($data['project_form_id']))
            $this->db->where('id_project_form',$data['project_form_id']);
        if(isset($data['form_key']))
            $this->db->where('form_key',$data['form_key']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormFields($data)
    {   //echo "<pre>"; print_r($data); exit;
        $this->db->select('*');
        $this->db->from('project_stage_section_form_field pssff');
        $this->db->join('project_form_field pff','pff.id_project_form_field = pssff.project_form_field_id','');
        if(isset($data['crm_project_id'])){
            $this->db->join('(select pfi.* from `project_form_info` pfi where pfi.`project_id`='.$data['crm_project_id'].' ORDER BY pfi.id_project_form_info desc)p', 'p.project_form_field_id = pssff.project_form_field_id', 'left');
            //$this->db->join('project_stage_info psi', 'pfi.project_stage_info_id = psi.id_project_stage_info and project_id='.$data['crm_project_id'], 'left');
            $this->db->group_by('pssff.project_form_field_id');
        }
        $this->db->where('pssff.project_stage_section_form_id',$data['project_stage_section_form_id']);
        $this->db->order_by('pssff.project_stage_section_form_field_order','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectStageInfo($data)
    {
        $this->db->select('*,ps.next_project_stage_id,ps.project_stage_complete_status,group_concat(u.first_name," ",u.last_name) as user_name');
        $this->db->from('project_stage_info psi');
        $this->db->join('project_stage ps','psi.stage_id=ps.id_project_stage','left');
        $this->db->join('user u','psi.created_by=u.id_user','left');
        if(isset($data['project_id']))
            $this->db->where('psi.project_id', $data['project_id']);
        if(isset($data['stage_id']))
            $this->db->where('psi.stage_id', $data['stage_id']);
        if(isset($data['id_project_stage_info']))
            $this->db->where('psi.id_project_stage_info', $data['id_project_stage_info']);
        $this->db->group_by('psi.id_project_stage_info');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getStageFacilities($where)
    {
        $this->db->select('pf.*,c.currency_code,ft.child_name as facility_type_name,fub.child_name as facility_sub_type_name,ftm.child_name as facility_term_name,sf.child_name as facility_source_of_funds');
        $this->db->from('project_facility pf');
        $this->db->join('currency c','c.id_currency = pf.currency_id','left');
        $this->db->join('master_child ft','ft.id_child = pf.facility_type','left');
        $this->db->join('master_child fub','fub.id_child = pf.facility_sub_type','left');
        $this->db->join('master_child ftm','ftm.id_child = pf.facility_term','left');
        $this->db->join('master_child sf','sf.id_child = pf.facility_source_of_payment','left');
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();

    }

    public function getProjectStageInfoDetail($data)
    {
        $this->db->select('psi.*,cb.legal_name,group_concat(u.first_name," ",u.last_name) as user_name');
        $this->db->from('project_stage_info psi');
        $this->db->join('company_user cu','psi.created_by=cu.user_id','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('user u','psi.created_by=u.id_user','left');
        if(isset($data['project_id']))
            $this->db->where('psi.project_id', $data['project_id']);
        if(isset($data['stage_id']))
            $this->db->where('psi.stage_id', $data['stage_id']);
        if(isset($data['offset']) && isset($data['limit']))
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectStageInfo($data)
    {
        $this->db->insert('project_stage_info', $data);
        return $this->db->insert_id();
    }

    public function addProjectStageFormInfo_batch($data)
    {
        $this->db->insert_batch('project_form_info', $data);
        return $this->db->insert_id();
    }

    public function updateProjectStageFormInfo_batch($data)
    {
        $this->db->update_batch('project_form_info', $data, 'id_project_form_info');
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function getProjectFormFields($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_section pss');
        $this->db->join('project_section pse','pss.project_section_id=pse.id_project_section','left');
        $this->db->join('project_stage_section_form pssf','pss.id_project_stage_section=pssf.project_stage_section_id','left');
        $this->db->join('project_form pfo','pssf.project_form_id=pfo.id_project_form','left');
        $this->db->join('project_stage_section_form_field pssff','pssf.id_project_stage_section_form=pssff.project_stage_section_form_id','left');
        $this->db->join('project_form_field pff','pssff.project_form_field_id=pff.id_project_form_field','left');
        $this->db->join('project_form_info pfi','pssff.project_form_field_id=pfi.project_form_field_id','left');
        $this->db->order_by('project_stage_section_order,project_stage_section_form_order,project_stage_section_form_field_order','ASC');

        if(isset($data['stage_id']))
            $this->db->where('pss.project_stage_id',$data['stage_id']);
        if(isset($data['project_id']))
            $this->db->where('pfi.project_id',$data['project_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectStageDetails($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_info psi');
        $this->db->join('project_stage ps','psi.stage_id=ps.id_project_stage','left');
        if(isset($data['crm_project_id']))
            $this->db->where('psi.project_id',$data['crm_project_id']);
        if(isset($data['project_stage_id']))
            $this->db->where('psi.stage_id',$data['project_stage_id']);
        if(isset($data['project_form_id'])) {
            $this->db->join('project_form_info pfi', 'psi.id_project_stage_info=pfi.project_stage_info_id', 'left');
            $this->db->where('pfi.form_id', $data['project_form_id']);
        }
        if(isset($data['project_stage_info_id']))
            $this->db->where('psi.id_project_stage_info', $data['project_stage_info_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateProject($data)
    {
        $this->db->where('id_crm_project',$data['id_crm_project']);
        $this->db->update('crm_project', $data);
        //echo $this->db->last_query();
        return 1;
    }

    public function getProjects()
    {
        $this->db->select('cm.id_crm_project as project_id');
        $this->db->from('crm_project cm');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectStages($data = array())
    {
        $this->db->select('psi.id_project_stage_info as project_stage_id');
        $this->db->from('project_stage_info psi');
        $this->db->where('psi.project_id',$data['project_id']);
        $this->db->where('psi.project_stage_status','approved');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormCommentsCount($data)
    {
        $this->db->select('count(*) as count');
        $this->db->from('form_comments fc');
        if(isset($data['reference_id']))
            $this->db->where('fc.reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
            $this->db->where('fc.reference_type', $data['reference_type']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('fc.project_stage_section_form_id', $data['project_stage_section_form_id']);
        $query = $this->db->get();
        $data =  $query->result_array();
        return $data[0]['count'];
    }

    public function getProjectStageSectionFormLastUpdate($data)
    {
        $this->db->select('pssflu.*,CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('project_stage_section_form_last_update pssflu');
        $this->db->join('user u','pssflu.updated_by=u.id_user','left');
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id',$data['crm_company_id']);
        if(isset($data['crm_contact_id']))
            $this->db->where('crm_contact_id',$data['crm_contact_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('project_stage_section_form_id',$data['project_stage_section_form_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectStageSectionFormLastUpdate($data)
    {
        $this->db->insert('project_stage_section_form_last_update', $data);
        return $this->db->insert_id();
    }

    public function updateProjectStageSectionFormLastUpdate($data)
    {
        $this->db->where('id_project_stage_section_form_last_update', $data['id_project_stage_section_form_last_update']);
        $this->db->update('project_stage_section_form_last_update', $data);
        return 1;
    }


    public function getProjectFormInfo($data)
    {
        $this->db->select('*');
        $this->db->from('project_form_info pfi');
        $this->db->join('project_stage_section_form pssf','pfi.project_stage_section_form_id=pssf.id_project_stage_section_form','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pfi.project_id', $data['crm_project_id']);
        if(isset($data['project_id']))
            $this->db->where('pfi.project_id', $data['project_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('pfi.project_stage_section_form_id', $data['project_stage_section_form_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectFormFieldData($data)
    {
        /*$this->db->select('*');
        $this->db->from('project_form_info pfi');
        $this->db->join('project_stage_section_form_field pssff','pfi.project_form_field_id=pssff.project_form_field_id','left');
        $this->db->join('project_form_field pff','pff.id_project_form_field = pfi.project_form_field_id','left');

        if(isset($data['crm_project_id']))
            $this->db->where('pfi.project_id',$data['crm_project_id']);

        if(isset($data['project_stage_section_form_id'])) {
            $this->db->where('pfi.project_stage_section_form_id', $data['project_stage_section_form_id']);
            $this->db->where('pssff.project_stage_section_form_id', $data['project_stage_section_form_id']);
        }
        if(isset($data['project_stage_info_id']))
            $this->db->where('pfi.project_stage_info_id',$data['project_stage_info_id']);*/
        $this->db->select('*');
        $this->db->from('project_stage_section_form_field pssff');
        $this->db->join('project_form_field pff','pssff.project_form_field_id=pff.id_project_form_field','left');
        $this->db->join('project_form_info pfi','pff.id_project_form_field=pfi.project_form_field_id and pfi.project_id = '.$data["crm_project_id"].' and pfi.project_stage_info_id = '.$data['project_stage_info_id'].' and pfi.project_stage_section_form_id = '.$data['project_stage_section_form_id'].'','left');
        if(isset($data['project_stage_section_form_id'])) {
            $this->db->where('pssff.project_stage_section_form_id', $data['project_stage_section_form_id']);
        }
        $this->db->order_by('pssff.project_stage_section_form_field_order','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectStageSectionFormFields($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_section_form_field pssff');
        $this->db->join('project_form_field pff','pff.id_project_form_field = pssff.project_form_field_id','left');
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('pssff.project_stage_section_form_id',$data['project_stage_section_form_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFacilityDisbursement($contract_id)
    {
        $this->db->select('*');
        $this->db->from('facility_disbursement');
        $this->db->where('contract_id',$contract_id);
        $this->db->order_by('value_dt','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFacilityLoanRepayment($contract_id)
    {
        $this->db->select('*');
        $this->db->from('facility_loan_repayment');
        $this->db->where('contract_id',$contract_id);
        $this->db->order_by('due_date','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFacilityLoanReceipts($contract_id)
    {
        $this->db->select('*');
        $this->db->from('facility_loan_receipts');
        $this->db->where('contract_id',$contract_id);
        $this->db->order_by('paid_date','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStage($data = array())
    {
        $this->db->select('*');
        $this->db->from('project_stage');
        if(isset($data['id_project_stage']))
            $this->db->where('id_project_stage',$data['id_project_stage']);
        $this->db->order_by('stage_order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormFieldsByKeys($data)
    {
        $this->db->select('*');
        $this->db->from('project_form_field pff');
        $this->db->join('project_stage_section_form_field pssff','pff.id_project_form_field=pssff.project_form_field_id','left');
        $this->db->join('project_stage_section_form pssf','pssff.project_stage_section_form_id=pssf.id_project_stage_section_form','left');
        $this->db->join('project_stage_section pss','pssf.project_stage_section_id=pss.id_project_stage_section','left');
        if(isset($data['stage_id']))
            $this->db->where('pss.project_stage_id',$data['stage_id']);
        $this->db->where_in('pff.field_key',$data);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectFormFieldValuesByKeys($data)
    {
        $this->db->select('*');
        $this->db->from('project_form_info pfi');
        $this->db->join('project_form_field pff','pfi.project_form_field_id=pff.id_project_form_field','left');
        if(isset($data['project_id']))
            $this->db->where('pfi.project_id',$data['project_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('pfi.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['keys']))
            $this->db->where_in('pff.field_key',$data['keys']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilityDetails($data)
    {
        $this->db->select('pf.*,c.*,mc.child_name as loan_payment_type_name,mc.child_key as loan_payment_type_key_name');
        $this->db->from('project_facility pf');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('master_child mc','pf.loan_payment_type=mc.id_child','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);
        $this->db->where('project_facility_status !=','inactive');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectCompanyAssessment($data)
    {
        $this->db->select('*');
        $this->db->from('assessment s');
        $this->db->join('assessment_item ai','s.id_assessment=ai.assessment_id','left');
        if(isset($data['crm_project_id']))
            $this->db->join('company_assessment_item_step cais','ai.id_assessment_item=cais.assessment_item_id and crm_company_id in (select crm_company_id from project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1 and company_type_id=1)','left');
        if(isset($data['assessment']))
            $this->db->where('s.assessment_key',$data['assessment']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getCrmCompanyDataByKeys($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_data ccd');
        $this->db->join('form_field ff','ccd.form_field_id=ff.id_form_field','left');
        if(isset($data['crm_company_id']))
            $this->db->where('ccd.crm_company_id',$data['crm_company_id']);
        if(isset($data['keys']))
            $this->db->where_in('ff.field_name',$data['keys']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateProjectStageInfo($data)
    {
        $this->db->where('id_project_stage_info',$data['id_project_stage_info']);
        $this->db->update('project_stage_info', $data);
        return 1;
    }
    public function addProjectStageVersion($data)
    {
        $this->db->insert('project_stage_version', $data);
        return $this->db->insert_id();
    }
    public function getProjectStageVersions($data)
    {
        $this->db->select('psv.version_number,psv.created_date_time, CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('project_stage_version psv');
        $this->db->join('user u','u.id_user = psv.created_by');
        if(isset($data['project_stage_info_id']))
            $this->db->where('psv.project_stage_info_id',$data['project_stage_info_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectStageWorkflowPhase()
    {
        $this->db->select('*');
        $this->db->from('project_stage ps');
        $this->db->join('workflow w','ps.workflow_id=w.id_workflow','left');
        $this->db->join('workflow_phase wp','w.id_workflow=wp.workflow_id','left');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectStageWorkflowProcess($data)
    {
        $this->db->select('psi.id_project_stage_info,psi.start_date_time,psi.end_date_time,ps.id_project_stage,ps.stage_name,ps.stage_key,pswp.id_project_stage_workflow_phase,pswp.workflow_phase_id,pswp.`status`,wp.workflow_phase_name,datediff(IFNULL(pswp.updated_date_time,CURDATE()),pswp.created_date_time) as days,wa.workflow_action_key');
        $this->db->from('project_stage_info psi');
        $this->db->join('project_stage ps','psi.stage_id=ps.id_project_stage','left');
        $this->db->join('project_stage_workflow_phase pswp','psi.id_project_stage_info=pswp.project_stage_info_id','left');
        $this->db->join('project_stage_workflow psw','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','left');
        $this->db->join('workflow_phase wp','pswp.workflow_phase_id=wp.id_workflow_phase','left');
        $this->db->join('workflow_phase_action wpa','psw.workflow_phase_action_id=wpa.id_workflow_phase_action','left');
        $this->db->join('workflow_action wa','wpa.workflow_action_id=wa.id_workflow_action','left');
        $this->db->where('psi.project_id',$data['project_id']);
        $this->db->where('pswp.workflow_phase_id!=','0');
        //$this->db->group_by('pswp.workflow_phase_id');
        $this->db->order_by('ps.id_project_stage','ASC');
        $this->db->order_by('pswp.id_project_stage_workflow_phase','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getSupervisionAssessment($data)
    {
        $this->db->select('*');
        $this->db->from('supervision_assessment sa');
        if(isset($data['project_id']))
            $this->db->where('sa.project_id',$data['project_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('sa.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('sa.project_stage_section_form_id',$data['project_stage_section_form_id']);
        if(isset($data['id_supervision_assessment']))
            $this->db->where('sa.id_supervision_assessment',$data['id_supervision_assessment']);
        $this->db->order_by('id_supervision_assessment','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addSupervisionAssessment($data)
    {
        $this->db->insert('supervision_assessment', $data);
        return $this->db->insert_id();
    }

    public function updateSupervisionAssessment($data)
    {
        $this->db->where('id_supervision_assessment',$data['id_supervision_assessment']);
        $this->db->update('supervision_assessment', $data);
        return 1;
    }

    public function deleteSupervisionAssessment($data)
    {
        $this->db->where('id_supervision_assessment',$data['id_supervision_assessment']);
        $this->db->delete('supervision_assessment');
        return 1;
    }

    public function getSupervisionProjectCommittee($data)
    {
        $this->db->select('*');
        $this->db->from('supervision_project_committee sa');
        if(isset($data['project_id']))
            $this->db->where('sa.project_id',$data['project_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('sa.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('sa.project_stage_section_form_id',$data['project_stage_section_form_id']);
        $this->db->order_by('id_supervision_project_committee','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addSupervisionProjectCommittee($data)
    {
        $this->db->insert('supervision_project_committee', $data);
        return $this->db->insert_id();
    }

    public function updateSupervisionProjectCommittee($data)
    {
        $this->db->where('id_supervision_project_committee',$data['id_supervision_project_committee']);
        $this->db->update('supervision_project_committee', $data);
        return 1;
    }

    public function deleteSupervisionProjectCommittee($data)
    {
        $this->db->where('id_supervision_project_committee',$data['id_supervision_project_committee']);
        $this->db->delete('supervision_project_committee');
        return 1;
    }

    public function getDisbursementRequest($data)
    {
        $this->db->select('sa.*,mc.child_name');
        $this->db->from('disbursement_request sa');
        $this->db->join('project_facility pf','sa.project_facility_id=pf.id_project_facility','left');
        $this->db->join('master_child mc','pf.facility_source_of_payment=mc.id_child','left');
        if(isset($data['id_disbursement_request']))
            $this->db->where('sa.id_disbursement_request',$data['id_disbursement_request']);
        if(isset($data['project_id']))
            $this->db->where('sa.project_id',$data['project_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('sa.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('sa.project_stage_section_form_id',$data['project_stage_section_form_id']);
        $this->db->order_by('id_disbursement_request','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addDisbursementRequest($data)
    {
        $this->db->insert('disbursement_request', $data);
        return $this->db->insert_id();
    }

    public function updateDisbursementRequest($data)
    {
        $this->db->where('id_disbursement_request',$data['id_disbursement_request']);
        $this->db->update('disbursement_request', $data);
        return 1;
    }

    public function deleteDisbursementRequest($data)
    {
        $this->db->where('id_disbursement_request',$data['id_disbursement_request']);
        $this->db->delete('disbursement_request');
        return 1;
    }

    public function getProjectFacilities($data)
    {
        $this->db->select('pf.*,mc.child_name as source_of_founds,c.currency_code');
        $this->db->from('project_facility pf');
        $this->db->join('master_child mc','pf.facility_source_of_payment=mc.id_child','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->where('pf.crm_project_id',$data['crm_project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getEsGrade($data)
    {
        $this->db->select('*');
        $this->db->from('project_es_grade');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        $this->db->where('es_grade_status',1);
        $this->db->order_by('id_project_es_grade','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addEsGrade($data)
    {
        $this->db->insert('project_es_grade', $data);
        return $this->db->insert_id();
    }

    public function updateEsGrade($data)
    {
        $this->db->where('id_project_es_grade',$data['id_project_es_grade']);
        $this->db->update('project_es_grade', $data);
        return 1;
    }

    public function getVersionNumber($data)
    {
        $this->db->select('psv.*,CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('project_stage_info psi');
        $this->db->join('project_stage_version psv','psi.id_project_stage_info=psv.project_stage_info_id','left');
        $this->db->join('user u','psv.created_by=u.id_user','left');
        if(isset($data['project_id']))
            $this->db->where('psi.project_id',$data['project_id']);
        if(isset($data['stage_id']))
            $this->db->where('psi.stage_id',$data['stage_id']);
        if(isset($data['version_number']))
            $this->db->where('psv.version_number',$data['version_number']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getInitialSupervisionReport($data)
    {
        $this->db->select('*');
        $this->db->from('supervision_report sa');
        if(isset($data['project_id']))
            $this->db->where('sa.crm_project_id',$data['project_id']);
        if(isset($data['id_supervision_report']))
            $this->db->where('sa.id_supervision_report',$data['id_supervision_report']);
        $this->db->order_by('id_supervision_report','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addInitialSupervisionReport($data)
    {
        $this->db->insert('supervision_report', $data);
        return $this->db->insert_id();
    }

    public function updateInitialSupervisionReport($data)
    {
        $this->db->where('id_supervision_report',$data['id_supervision_report']);
        $this->db->update('supervision_report', $data);
        return 1;
    }

    public function deleteInitialSupervisionReport($data)
    {
        $this->db->where('id_supervision_report',$data['id_supervision_report']);
        $this->db->delete('supervision_report');
        return 1;
    }

    public function getAnnualReview($data)
    {
        $this->db->select('*');
        $this->db->from('annual_review sa');
        if(isset($data['project_id']))
            $this->db->where('sa.crm_project_id',$data['project_id']);
        if(isset($data['id_annual_review']))
            $this->db->where('sa.id_annual_review',$data['id_annual_review']);
        $this->db->order_by('id_annual_review','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addAnnualReview($data)
    {
        $this->db->insert('annual_review', $data);
        return $this->db->insert_id();
    }

    public function updateAnnualReview($data)
    {
        $this->db->where('id_annual_review',$data['id_annual_review']);
        $this->db->update('annual_review', $data);
        return 1;
    }

    public function deleteAnnualReview($data)
    {
        $this->db->where('id_annual_review',$data['id_annual_review']);
        $this->db->delete('annual_review');
        return 1;
    }

    public function getOngoingMonitoring($data)
    {
        $this->db->select('*');
        $this->db->from('ongoing_monitoring sa');
        if(isset($data['project_id']))
            $this->db->where('sa.crm_project_id',$data['project_id']);
        if(isset($data['id_ongoing_monitoring']))
            $this->db->where('sa.id_ongoing_monitoring',$data['id_ongoing_monitoring']);
        $this->db->order_by('id_ongoing_monitoring','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addOngoingMonitoring($data)
    {
        $this->db->insert('ongoing_monitoring', $data);
        return $this->db->insert_id();
    }

    public function updateOngoingMonitoring($data)
    {
        $this->db->where('id_ongoing_monitoring',$data['id_ongoing_monitoring']);
        $this->db->update('ongoing_monitoring', $data);
        return 1;
    }

    public function deleteOngoingMonitoring($data)
    {
        $this->db->where('id_ongoing_monitoring',$data['id_ongoing_monitoring']);
        $this->db->delete('ongoing_monitoring');
        return 1;
    }

    public function getOtherMonitoring($data)
    {
        $this->db->select('*');
        $this->db->from('other_monitoring sa');
        if(isset($data['project_id']))
            $this->db->where('sa.crm_project_id',$data['project_id']);
        if(isset($data['id_other_monitoring']))
            $this->db->where('sa.id_other_monitoring',$data['id_other_monitoring']);
        $this->db->order_by('id_other_monitoring','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addOtherMonitoring($data)
    {
        $this->db->insert('other_monitoring', $data);
        return $this->db->insert_id();
    }

    public function updateOtherMonitoring($data)
    {
        $this->db->where('id_other_monitoring',$data['id_other_monitoring']);
        $this->db->update('other_monitoring', $data);
        return 1;
    }

    public function deleteOtherMonitoring($data)
    {
        $this->db->where('id_other_monitoring',$data['id_other_monitoring']);
        $this->db->delete('other_monitoring');
        return 1;
    }

    public function getRiskAssessment($data)
    {
        $this->db->select('ra.*,mc.risk_name as risk_type_name,mc1.risk_name as risk_sub_type1_name,mc3.child_name as grade_name');
        //$this->db->select('ra.*,mc.risk_name as risk_type_name,mc1.risk_name as risk_sub_type1_name,mc2.risk_name as risk_sub_type2_name,mc3.child_name as grade_name');
        $this->db->from('risk_assessment ra');
        $this->db->join('risk_type mc','ra.risk_type=mc.id_risk_type','left');
        $this->db->join('risk_type mc1','ra.risk_sub_type1=mc1.id_risk_type','left');
        //$this->db->join('risk_type mc2','ra.risk_sub_type2=mc2.id_risk_type','left');
        $this->db->join('master_child mc3','ra.risk_grade=mc3.id_child','left');
        if(isset($data['project_id']))
            $this->db->where('ra.crm_project_id',$data['project_id']);
        if(isset($data['risk_type']))
            $this->db->where('mc.id_master_child',$data['risk_type']);
        if(isset($data['risk_sub_type1']))
            $this->db->where('mc.id_master_child',$data['risk_sub_type1']);
        /*if(isset($data['risk_sub_type2']))
            $this->db->where('mc.id_master_child',$data['risk_sub_type2']);*/
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addRiskAssessment($data)
    {
        $this->db->insert('risk_assessment', $data);
        return $this->db->insert_id();
    }

    public function updateRiskAssessment($data)
    {
        $this->db->where('id_risk_assessment',$data['id_risk_assessment']);
        $this->db->update('risk_assessment', $data);
        return 1;
    }

    public function deleteRiskAssessment($data)
    {
        $this->db->where('id_risk_assessment',$data['id_risk_assessment']);
        $this->db->delete('risk_assessment');
        return 1;
    }

    public function getProjectChecklistByType($data)
    {
        /*SELECT aq.assessment_question
FROM master_child mc
LEFT JOIN assessment_question_category aqc ON mc.id_child = aqc.operational_checklist_type
LEFT JOIN assessment_question aq ON aqc.id_assessment_question_category = aq.assessment_question_category_id
LEFT JOIN project_checklist pc ON aq.id_assessment_question = pc.assessment_question_id
WHERE mc.child_key='conditions_precedent_to_drawdown' AND pc.crm_project_id = 4;*/
        $this->db->select('aq.assessment_question');
        $this->db->from('master_child mc');
        $this->db->join('assessment_question_category aqc','mc.id_child = aqc.operational_checklist_type','left');
        $this->db->join('assessment_question aq','aqc.id_assessment_question_category = aq.assessment_question_category_id','left');
        $this->db->join('project_checklist pc','aq.id_assessment_question = pc.assessment_question_id','left');
        $this->db->where('mc.child_key',$data['child_key']);
        $this->db->where('pc.crm_project_id',$data['project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
}
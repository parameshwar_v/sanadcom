<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mismaster_model extends CI_Model
{


    public $branch_users = [];
    public $reporting_branch_type = [];
    public $reporting_user = [];
    public $reporting_branch = [];
    public $reporting_company_approval_roles = [];

    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }

    function next_result()
    {
        if (is_object($this->conn_id))
        {
            return mysqli_next_result($this->conn_id);
        }
    }

    public function saveFacilityPlans($facility_data,$mis_type,$created_by)
    {
        if($mis_type == 'disbursement' || $mis_type == 'repayment' || $mis_type == 'collection' || $mis_type == 'aging' || $mis_type == 'outstanding')
        {
            $contract_ids=array();
            $paid_dates=array();
            $user_ref=array();
            $analysis_month = array();
            foreach($facility_data as $key => $value)
            {
                $contract_ids[$key]=$value['contract_id'];
                if($mis_type == 'collection')
                {
                    $paid_dates[$key]=$value['paid_date'];
                    $user_ref[$key]=$value['user_ref_no'];
                }
                elseif($mis_type == 'aging')
                {
                    $analysis_month[$key]=$value['analysis_month'];
                }
                elseif($mis_type == 'outstanding')
                {
                    $analysis_month[$key]=$value['outstanding_date'];
                }
            }
        }
        if($mis_type == 'disbursement')
        {
            $this->db->where_in('contract_id',$contract_ids);
            $this->db->where('status',1);
            $update_facility_data=$this->db->get('facility_disbursement')->result();
            foreach($update_facility_data as $key => $value)
            {
                $update_facility_data[$key]->status=0;
                $update_facility_data[$key]->updated_by=$created_by;
                $update_facility_data[$key]->updated_on=currentDate();
                unset($update_facility_data[$key]->created_by);
                unset($update_facility_data[$key]->created_on);
            }
            if(count($update_facility_data)>0)
                $this->db->update_batch('facility_disbursement', $update_facility_data, 'id_facility_disbursement');
            $this->db->insert_batch('facility_disbursement', $facility_data);
        }
        elseif($mis_type == 'repayment')
        {
            $this->db->where_in('contract_id',$contract_ids);
            $this->db->where('status',1);
            $update_facility_data=$this->db->get('facility_loan_repayment')->result();
            foreach($update_facility_data as $key => $value)
            {
                $update_facility_data[$key]->status=0;
                $update_facility_data[$key]->updated_by=$created_by;
                $update_facility_data[$key]->updated_on=currentDate();
                unset($update_facility_data[$key]->created_by);
                unset($update_facility_data[$key]->created_on);
            }
            if(count($update_facility_data)>0)
                $this->db->update_batch('facility_loan_repayment', $update_facility_data, 'id_facility_loan_repayment');
            $this->db->insert_batch('facility_loan_repayment', $facility_data);
        }
        elseif($mis_type == 'collection')
        {
            $this->db->where_in('contract_id',$contract_ids);
            $this->db->where_in('paid_date',$paid_dates);
            $this->db->where_in('user_ref_no',$user_ref);
            $update_facility_data=$this->db->get('facility_loan_receipts')->result_array();
            foreach($update_facility_data as $key => $value)
            {
                $update_facility_data[$key]['status']=false;
                $update_facility_data[$key]['updated_by']=$created_by;
                $update_facility_data[$key]['updated_on']=currentDate();
                unset($update_facility_data[$key]['created_by']);
                unset($update_facility_data[$key]['created_on']);
            }
            $facility_data_1 = array();
            foreach ($facility_data as $value)
            {
                $facility_data_1[] = $value;
            }

            $facility_data_2 = array();
            foreach ($update_facility_data as $value)
            {
                $facility_data_2[] = $value;
            }
            $insert_facility_data = array_values(array_diff_key($facility_data_1, $facility_data_2));
            $duplicate_facility_data=array_values(array_intersect_key($facility_data_1, $facility_data_2));

            if(count($insert_facility_data)>0)
                $this->db->insert_batch('facility_loan_receipts', $insert_facility_data);
            if(count($duplicate_facility_data)>0)
            {
                //CONTRACT REF NO	CUSTOMER NAME	USER REF NO	AC CCY	PAID DATE	EVENT	EXCH RATE	AMOUNT	USD EQUIV
                foreach ( $duplicate_facility_data as $k=>$v )
                {
                    $duplicate_facility_data[$k] ['CONTRACT_REF_NO'] = $duplicate_facility_data[$k] ['contract_id'];
                    $duplicate_facility_data[$k]['status']=false;
                    unset($duplicate_facility_data[$k]['contract_id']);
                    $duplicate_facility_data[$k]=array_change_key_case($duplicate_facility_data[$k],CASE_UPPER);
                    $duplicate_facility_data[$k] ['PAID_DATE'] = date("d M Y", strtotime($duplicate_facility_data[$k] ['PAID_DATE']));
                    unset($duplicate_facility_data[$k]['ID_COMPANY']);
                    unset($duplicate_facility_data[$k]['CREATED_BY']);
                    unset($duplicate_facility_data[$k]['CREATED_ON']);
                }
                $ordered_keys = array('CONTRACT_REF_NO','CUSTOMER_NAME','USER_REF_NO','AC_CCY','PAID_DATE','EVENT','EXCH_RATE','AMOUNT','USD_EQUIV','STATUS');
                $ordered_data = array();
                for($od=0;$od<count($duplicate_facility_data);$od++)
                {
                    foreach ($ordered_keys as $key)
                    {
                        $ordered_data[$od][$key] = $duplicate_facility_data[$od][$key];
                    }
                }
                return $ordered_data;
            }
        }
        elseif($mis_type == 'aging')
        {
            $this->db->where_in('contract_id',$contract_ids);
            $this->db->where_in('analysis_month',$analysis_month);
            $this->db->where('record_status',1);
            $update_facility_data=$this->db->get('aging')->result();
            foreach($update_facility_data as $key => $value)
            {
                $update_facility_data[$key]->record_status=0;
                $update_facility_data[$key]->updated_by=$created_by;
                $update_facility_data[$key]->updated_on=currentDate();
                unset($update_facility_data[$key]->created_by);
                unset($update_facility_data[$key]->created_on);
            }
            if(count($update_facility_data)>0)
            {
                $this->db->update_batch('aging', $update_facility_data, 'id_aging');
            }
            $this->db->insert_batch('aging', $facility_data);
        }
        elseif($mis_type == 'outstanding')
        {
            $this->db->where_in('contract_id',$contract_ids);
            $this->db->where_in('outstanding_date',$analysis_month);
            $this->db->where('record_status',1);
            $update_facility_data=$this->db->get('outstanding_amount')->result();
            foreach($update_facility_data as $key => $value)
            {
                $update_facility_data[$key]->record_status=0;
                $update_facility_data[$key]->updated_by=$created_by;
                $update_facility_data[$key]->updated_on=currentDate();
                unset($update_facility_data[$key]->created_by);
                unset($update_facility_data[$key]->created_on);
            }
            if(count($update_facility_data)>0)
            {
                $this->db->update_batch('outstanding_amount', $update_facility_data, 'id_outstanding_amount');
            }
            $this->db->insert_batch('outstanding_amount', $facility_data);
        }
        return true;
    }
    public function insertDuplicateFacilityCollections($facility_data,$mis_type,$created_by)
    {
        $contract_ids=array();
        $paid_dates=array();
        $user_ref=array();
        foreach($facility_data as $key => $value)
        {
            $contract_ids[$key]=$value['contract_id'];
            $paid_dates[$key]=$value['paid_date'];
            $user_ref[$key]=$value['user_ref_no'];
        }
        $this->db->where_in('contract_id',$contract_ids);
        $this->db->where_in('paid_date',$paid_dates);
        $this->db->where_in('user_ref_no',$user_ref);
        $data=array('status'=>0,'updated_by'=>$created_by,'updated_on'=>currentDate());
        $this->db->update('facility_loan_receipts', $data);
        if(count($facility_data)>0)
            $this->db->insert_batch('facility_loan_receipts', $facility_data);
        return $this->db->insert_id();
    }


}
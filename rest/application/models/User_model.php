<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{

    public function __construct(){
        parent::__construct();
    }


    public $key = '#@Q-lana$#';
    public function createOauthCredentials($user_id,$first_name,$last_name)
    {
        $query = $this->db->get_where('oauth_clients',array('user_id' => $user_id));
        $result = $query->result_array();
        $key = bin2hex(openssl_random_pseudo_bytes(10));
        if(empty($result))
        {
            $data = array(
                'user_id' => $user_id,
                'secret' => $key,
                'name' => $first_name.' '.$last_name,
                'created_at' => currentDate()
            );
            $this->db->insert('oauth_clients', $data);
            $client_id = $this->db->insert_id();
            return array('client_id' => $client_id, 'client_secret' => $key);
        }
        else
        {
            return array('client_id' => $result[0]['id'], 'client_secret' => $result[0]['secret']);
        }
    }

    public function getTokenDetails($access_token,$user_id)
    {
        /*$query = $this->db->query('select * from oauth_access_tokens oct
                                            left join oauth_sessions os on oct.session_id=os.id
                                            left join oauth_clients oc on oc.id=os.client_id
                                            where oct.access_token="'.$access_token.'" and oc.user_id="'.$user_id.'"');*/
        $this->db->select('*');
        $this->db->from('oauth_access_tokens oct');
        $this->db->join('oauth_sessions os','oct.session_id=os.id','left');
        $this->db->join('oauth_clients oc','oc.id=os.client_id','left');
        $this->db->where('oct.access_token',$access_token);
        $this->db->where('oc.user_id',$user_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSession($data)
    {
        $this->db->select('oc.name,os.*');
        $this->db->from('oauth_sessions os');
        $this->db->join('oauth_clients oc','oc.id=os.client_id','left');
        $this->db->where('oc.user_id',$data['user_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->order_by('os.id','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTotalSession($data)
    {
        $this->db->select('*');
        $this->db->from('oauth_sessions os');
        $this->db->join('oauth_clients oc','oc.id=os.client_id','left');
        $this->db->where('oc.user_id',$data['user_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function encode($value)
    {
        return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->key), $value, MCRYPT_MODE_CBC, md5(md5($this->key)))),'+/=', '-_,');
    }
    public function decode($value)
    {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->key), base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5($this->key))), "\0");
    }

    public function activeAccount($code)
    {
        $query = $this->db->get_where('user',array('id_user' => $this->decode($code)));
        $data = $query->row();
        if(empty($data)){ return 0; }
        else{
            $update = array('user_status' => '1');
            $this->db->where('id_user', $this->decode($code));
            $this->db->update('user', $update);
            return 1;
        }
    }

    public function login($data)
    {
        $this->db->select('ur.user_role_name,u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.profile_image,u.security_question_id,u.security_question_ans,c.id_company,c.company_name,c.company_logo,cb.legal_name,ar.approval_name as role_name,u.failed_attempt,u.password_updated_date');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role','left');
        $this->db->join('company c','c.user_id=u.id_user','left');
        $this->db->join('company_user cu','u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->where(array('u.email_id' => $data['email_id'], 'u.password' => md5($data['password']), 'u.user_status' => 1));
        $query = $this->db->get();
        return $query->row();
    }

    public function getCompanyUser($id)
    {
        $this->db->select('u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.country_id,u.profile_image,u.user_status,u.zip_code,cu.id_company_user,cu.company_id,cu.company_approval_role_id as user_role_id,cu.branch_id,cu.reporting_user_id,CONCAT(us.first_name," ",us.last_name) as reporting_user_name,cb.id_branch,cb.branch_type_id,cb.legal_name as branch_name,cb.branch_logo,cb.country_id as branch_country_id,car.id_company_approval_role,ar.approval_name,c.*');
        $this->db->from('company_user cu');
        $this->db->from('company c','c.id_company=cu.company_id','left');
        $this->db->join('company_branch cb', 'cu.branch_id = cb.id_branch', 'left');
        $this->db->join('company_approval_role car', 'cu.company_approval_role_id = car.id_company_approval_role', 'left');
        $this->db->join('approval_role ar', 'car.approval_role_id = ar.id_approval_role', 'left');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('user us', 'us.id_user = cu.reporting_user_id', 'left');
        $this->db->where('u.id_user', $id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();
    }

    public function getCompanyUserInfo($id)
    {
        $this->db->select('*');
        $this->db->from('company_user cu');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('company c','cb.company_id=c.id_company','left');
        $this->db->where('cu.user_id',$id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();
    }

    public function check_email($email,$type='',$id='')
    {
        if($type=='crm_contact') {
            $query = $this->db->get_where('crm_contact', array('email' => addslashes($email), 'company_id' => $id));
        }
        else if($type=='crm_company') {
            $query = $this->db->get_where('crm_company', array('email' => addslashes($email), 'company_id' => $id));
        }
        else if($type=='company_user'){
            $this->db->select('u.*');
            $this->db->from('user u');
            $this->db->join('company_user cu','cu.user_id=u.id_user','left');
            $this->db->where('cu.company_id',$id);
            $this->db->where('u.email_id',addslashes($email));
            $query = $this->db->get();
        }
        else if($type=='company_branch'){
            $this->db->select('*');
            $this->db->from('company_branch');
            if($id!=0 && $id!='')
                $this->db->where('id_branch!=',$id);
            $this->db->where('branch_email',addslashes($email));
            $query = $this->db->get();
        }
        else {
            $this->db->select('u.*');
            $this->db->from('user u');
            if($id!=0 && $id!='')
                $this->db->where('u.id_user!=',$id);
            $this->db->where('u.email_id',addslashes($email));
            $query = $this->db->get();
            //$query = $this->db->get_where('user', array('email_id' => addslashes($email)));
        }

        return $query->row();
    }
    public function check_answer($id)
    {
            $this->db->select('u.*');
            $this->db->from('user u');
            $this->db->where('u.id_user =',$id);
            $query = $this->db->get();
            return $query->row();
    }
    public function passwordExist($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id_user',$data['user_id']);
        $this->db->where('password',md5($data['oldpassword']));
        $query=$this->db->get();
        return $query->num_rows();
    }
    public function changePassword($data)
    {
        $update = array('password' => md5($data['password']),'password_updated_date' => currentDate());
        $this->db->where('id_user', $data['user_id']);
        $this->db->update('user', $update);
        //insert in user log table
        $logdata['user_id']=$data['user_id'];
        $logdata['client_browser']=$_SERVER['HTTP_USER_AGENT'];
        $logdata['client_remote_address']=$_SERVER['REMOTE_ADDR'];
        $logdata['type']='password_change';
        $logdata['created_on']=currentDate();
        $logdata['created_by']=$data['user_id'];
        $this->db->insert('user_log', $logdata);
        //return $this->db->insert_id();
        return 1;
    }

    public function updatePassword($password,$id)
    {
        $update = array('password' => md5($password));
        $this->db->where('id_user', $id);
        $this->db->update('user', $update);
        return 1;
    }

    public function getAdminUser($data)
    {
        $this->db->select('u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,cu.country_name,u.zip_code,u.profile_image,u.created_date_time,u.user_status');
        $this->db->from('user u'); //,ar.approval_name as role_name,
        $this->db->join('company c','c.user_id=u.id_user','left');
        $this->db->join('country cu', 'u.country_id = cu.id_country', 'left');
        if(isset($data['company_id']))
            $this->db->where('c.id_company',$data['company_id']);
        if(isset($data['user_role_id']))
            $this->db->where('u.user_role_id',$data['user_role_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getUsersList($data)
    {
        $query = $this->db->get_where('user',array('user_role_id'=>$data['type']));
        return $query->result_array();
    }

    public function getUserInfo($data)
    {
        //$query = $this->db->get_where('user', array('id_user' => $data['id']));
        $this->db->select('u.*,c.country_name');
        $this->db->from('user u');
        $this->db->join('country c','u.country_id=c.id_country','left');
        $this->db->where('u.id_user',$data['id']);
        $query = $this->db->get();
        return $query->row();
    }

    public function getUsersInfo($data)
    {
        $this->db->select('group_concat(CONCAT(u.first_name," ",u.last_name)) as user_name');
        $this->db->from('user u');
        $this->db->where_in('u.id_user',$data['id']);
        $query = $this->db->get();
        return $query->row();
    }

    public function getUserQuestion($data)
    {
        $this->db->select('u.email_id,mc.*');
        $this->db->from('user u');
        $this->db->join('master_child mc','mc.id_child=u.security_question_id','left');
        $this->db->where('u.id_user',addslashes($data['id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function createUserInfo($data)
    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function updateUserInfo($data)
    {
        $id = $data['id_user'];
        unset($data['id_user']);
        unset($data['id_company_user']);
        unset($data['branch']);
        unset($data['userRole']);
        unset($data['updated_date_time']);

        if($data['country_id']=='null' || $data['country_id']==''){
            unset($data['country_id']);
        }
        if($data['user_status'] == 1)
        {
            $data['failed_attempt'] = 0;
        }
        $this->db->where('id_user', $id);
        $this->db->update('user', $data);
        return 1;
    }

    public function updateUserData($data,$id)
    {
        $this->db->where('id_user', $id);
        $this->db->update('user', $data);
        return 1;
    }

    public function deleteUser($data)
    {
        $update = array('status'=>'inactive');
        $this->db->where('id_user', $data['id']);
        $this->db->update('user', $update);
        return 1;
    }

    public function getPreviousUserSessions($data)
    {
        $this->db->select('*,oat.id as access_token_id');
        $this->db->from('oauth_clients oc');
        $this->db->join('oauth_sessions os','oc.id=os.client_id','left');
        $this->db->join('oauth_access_tokens oat','os.id=session_id','left');
        $this->db->where('oc.user_id',$data['user_id']);
        if(isset($data['timestamp']))
            $this->db->where('oat.expire_time>',$data['timestamp']);
        if(isset($data['access_token']))
            $this->db->where('oat.access_token',$data['access_token']);
        if(isset($data['user_id']))
            $this->db->where('oc.user_id',$data['user_id']);
        if(isset($data['expired_null']))
            $this->db->where('expired_date_time',null);

        $query=$this->db->get();
        return $query->result_array();
    }

    public function updateOauthAccessToken($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('oauth_access_tokens', $data);
        $query = $this->db->get_where('oauth_access_tokens', array('id' => $data['id']));
        $accesstoken_details = $query->row();

        $data=array('updated_at'=>currentDate());
        $this->db->where('id', $accesstoken_details->session_id);
        $this->db->update('oauth_sessions', $data);
        return 1;
    }

    //Added by venki
    public function loginattempts($data)
    {
        $query = $this->db->get_where('user', array('email_id' => $data['email_id']));
        $user_details = $query->row();
        if($user_details)
        {
            if($user_details->user_status == 2)
            {
                return 2;
            }
            elseif($user_details->failed_attempt<3 && $user_details->user_status == 1)
            {
                $this->db->where('email_id', $data['email_id']);
                if($user_details->failed_attempt == 2)
                    $this->db->set('user_status', '3', FALSE);
                $this->db->set('failed_attempt', '`failed_attempt`+ 1', FALSE);
                $this->db->update('user');

                //insert in user log table
                $logdata['user_id']=$user_details->id_user;
                //$logdata['client_browser']=$_SERVER['HTTP_USER_AGENT'];
                //$ubrowser=getUserBrowser($_SERVER['HTTP_USER_AGENT']);
                $logdata['client_browser'] = getUserBrowser($_SERVER['HTTP_USER_AGENT']);
                $logdata['client_remote_address']=$_SERVER['REMOTE_ADDR'];
                $logdata['type']='failed_attempt';
                $logdata['created_on']=currentDate();
                $logdata['created_by']=$user_details->id_user;
                $this->db->insert('user_log',$logdata);

                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    }
    public function updateloginattempts($user_id)
    {
        $this->db->where('id_user', $user_id);
        $this->db->set('failed_attempt', 0, FALSE);
        $this->db->update('user');
        return 1;
    }
    public function updateLastLogin($user_id)
    {
        $this->db->where('id_user', $user_id);
        $this->db->set('last_login', currentDate());
        $this->db->update('user');
        return 1;
    }
    public function saveSecurityQuestion($data,$id)
    {
        $this->db->where('id_user', $id);
        $this->db->update('user', $data);
        //insert in user log table
        $logdata['user_id']=$id;
        $logdata['client_browser']=$_SERVER['HTTP_USER_AGENT'];
        $logdata['client_remote_address']=$_SERVER['REMOTE_ADDR'];
        $logdata['security_question_id']=$data['security_question_id'];
        $logdata['security_question_answer']=$data['security_question_ans'];
        $logdata['type']='security_question';
        $logdata['created_on']=currentDate();
        $logdata['created_by']=$id;
        $this->db->insert('user_log',$logdata);
    }

    public function getUserLogByType($data)
    {
        $user_data=[];
        $query = $this->db->get_where('user', array('id_user' => $data['user_id']));
        $user_data['user_details']=$query->row();

        if($data['type']=='security_question'){
            $this->db->select('ul.*, mc.child_name');
            $this->db->join('master_child mc', 'ul.security_question_id=mc.id_child', 'left');
        }else{
            $this->db->select('ul.*');
        }
        $this->db->from('user_log ul');
        $this->db->where('ul.user_id =',$data['user_id']);
        $this->db->where('ul.type =',$data['type']);
        $this->db->order_by('ul.id_user_log','DESC');
        $query = $this->db->get();
        $user_data['user_log']=$query->result();
        return $user_data;
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model
{

    public function __construct(){
        parent::__construct();
    }

    public function addNotification($data)
    {
        $this->db->insert('notification', $data);
        return $this->db->insert_id();
    }

    public function getNotifications($data)
    {
        $this->db->select('n.*,CONCAT(u.first_name," ",u.last_name) as user_name,u.profile_image,ar.approval_name');
        $this->db->from('notification n');
        $this->db->join('user u','n.assigned_by=u.id_user','left');
        $this->db->join('company_user cu','cu.user_id=n.assigned_by','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        if(isset($data['user_id']))
            $this->db->where('n.assigned_to',$data['user_id']);
        if(isset($data['module_id']))
            $this->db->where('n.module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('n.module_type',$data['module_type']);
        if(isset($data['filter']))
        {
            if($data['filter'] == 'new')
                $this->db->where('n.notification_status','unread');
            if($data['filter'] == 'workflow' || $data['filter'] == 'meeting')
                $this->db->where('n.notification_type',$data['filter']);
            elseif($data['filter'] == 'task' || $data['filter'] == 'alert')
                $this->db->where('n.notification_type',$data['filter']);
        }
        $this->db->where('n.notification_type !=','notification');
        if(isset($data['notification_status']))
            $this->db->where('n.notification_status',$data['notification_status']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where("(CONCAT(u.first_name, \" \", u.last_name) LIKE '%".$data['search_key']."%' || n.notification_type LIKE '%".$data['search_key']."%' || ar.approval_name LIKE '%".$data['search_key']."%' || n.notification_subject LIKE '%".$data['search_key']."%')");
        }
        $this->db->order_by('n.id_notification','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='' && !isset($data['offset']))
            $this->db->limit($data['limit'], 0);
        $this->db->where('notification_status !=','deleted');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getNotificationsCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('notification n');
        $this->db->join('user u','n.assigned_by=u.id_user','left');
        $this->db->join('company_user cu','cu.user_id=n.assigned_by','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        if(isset($data['user_id']))
            $this->db->where('n.assigned_to',$data['user_id']);
        if(isset($data['module_id']))
            $this->db->where('n.module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('n.module_type',$data['module_type']);
        if(isset($data['notification_status']))
            $this->db->where('n.notification_status',$data['notification_status']);
        if(isset($data['filter']))
        {
            if($data['filter'] == 'new')
                $this->db->where('n.notification_status','unread');
            if($data['filter'] == 'workflow' || $data['filter'] == 'meeting')
                $this->db->where('n.notification_type',$data['filter']);
            elseif($data['filter'] == 'task' || $data['filter'] == 'alert')
                $this->db->where('n.notification_type',$data['filter']);

        }
        $this->db->where('n.notification_type !=','notification');

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where("(CONCAT(u.first_name, \" \", u.last_name) LIKE '%".$data['search_key']."%' || n.notification_type LIKE '%".$data['search_key']."%' || ar.approval_name LIKE '%".$data['search_key']."%' || n.notification_subject LIKE '%".$data['search_key']."%')");
        }
        $this->db->where('notification_status !=','deleted');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateNotification($data)
    {
        $this->db->where('id_notification', $data['id_notification']);
        $this->db->update('notification', $data);
    }

}
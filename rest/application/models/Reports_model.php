<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function getPortfolioByProduct($data)
    {
        $this->db->select('mc.id_child as id,mc.child_name as product_name,mc.parent_id,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id');
        $this->db->join('project_facility pf','pf.facility_type = mc.id_child or pf.facility_sub_type = mc.id_child');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        /*$this->db->where('m.master_key','facility_type');*/
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        $this->db->group_by('mc.id_child');
        $this->db->order_by('mc.child_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getPortfolioByProductAnalysis($data)
    {
        $this->db->select('pf.facility_type as product_id, m.child_name as product_name,sum(oa.usd_equiv) as amount,`DATE_FORMAT`(oa.outstanding_date,"%m-%Y") as analysis_date');
        $this->db->from('project_facility pf');
        $this->db->join('master_child m','m.id_child = pf.facility_type');
        $this->db->join('crm_project p','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") >= "'.$data['start_month'].'"and DATE_FORMAT(oa.outstanding_date,"%m-%Y") <= "'.$data['end_month'].'"');
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") >= "'.$data['start_month'].'" and DATE_FORMAT(a.analysis_month,"%m-%Y") <= "'.$data['end_month'].'" and a.proposed = "'.$data['outstanding_status'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('pf.facility_type,DATE_FORMAT(oa.outstanding_date,"%m-%Y")');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getAvailableAnalysis($data)
    {
        $this->db->select('DATE_FORMAT(oa.outstanding_date,"%b %Y") as analysis_month,oa.outstanding_date as analysis_date');
        $this->db->from('project_facility pf');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->where('p.company_id', $data['company_id']);
        $this->db->group_by('DATE_FORMAT(oa.outstanding_date,"%m-%Y")');
        $this->db->order_by('oa.outstanding_date','desc');
        $this->db->limit(12);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioByProductClients($data)
    {
        $this->db->select('mc.id_child as id, mc.child_name as product_name, mc.parent_id, count(DISTINCT pc.crm_company_id) as clients');
        $this->db->from('master_child mc');
        $this->db->join('project_facility pf','pf.facility_type = mc.id_child or pf.facility_sub_type = mc.id_child','left');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('project_company pc','pc.crm_project_id = pf.crm_project_id and pc.company_type_id = 1 and pc.project_company_status = 1');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('mc.id_child');
         $this->db->order_by('mc.child_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }


    function getCompanyProducts($data)
    {
        $this->db->select('mc.id_child as product_id,mc.child_name as product_name');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id','left');
        if(isset($data['master_key']))
            $this->db->where('m.master_key',$data['master_key']);
        if(isset($data['parent_id']))
            $this->db->where('FIND_IN_SET ('.$data['parent_id'].',mc.parent_id)');
        if(isset($data['order']))
            $this->db->order_by($data['order'],'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function getProductItems($data)
    {
        $this->db->select('mc.id_child as product_id,mc.child_name as product_name');
        $this->db->from('master_child mc');
        if(isset($data['product_id']))
            $this->db->where('mc.parent_id',$data['product_id']);
        if(isset($data['order']))
            $this->db->order_by($data['order'],'asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getPortfolioByCountry($data)
    {
        $this->db->select('c.id_country,c.country_name,sum(oa.usd_equiv) as amount');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('c.id_country');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 'c.country_name',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioByCountryAnalysis($data)
    {
        $this->db->select('c.id_country,c.country_name,sum(oa.usd_equiv) as amount,DATE_FORMAT(oa.outstanding_date,"%m-%Y") as analysis_date');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") >= "'.$data['start_month'].'" and DATE_FORMAT(oa.outstanding_date,"%m-%Y") <= "'.$data['end_month'].'"');
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") >= "'.$data['start_month'].'" and DATE_FORMAT(a.analysis_month,"%m-%Y") <= "'.$data['end_month'].'" and a.proposed = "'.$data['outstanding_status'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('c.id_country,DATE_FORMAT(oa.outstanding_date,"%m-%Y")');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 'c.country_name',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioByCountryClients($data)
    {
        $this->db->select('c.id_country,c.country_name, count(DISTINCT pc.crm_company_id) as clients');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_company pc','pc.crm_project_id = p.id_crm_project and pc.company_type_id = 1 and pc.project_company_status = 1');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('c.id_country');
        $this->db->order_by('c.country_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getPortfolioByCurrency($data)
    {
        $this->db->select('c.id_currency,c.currency_code,sum(oa.usd_equiv) as amount');
        $this->db->from('currency c');
        $this->db->join('project_facility pf','pf.currency_id = c.id_currency');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y")= "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('c.id_currency');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 'c.currency_code',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioByCurrencyAnalysis($data)
    {
        $this->db->select('c.id_currency,c.currency_code,sum(oa.usd_equiv) as amount,DATE_FORMAT(oa.outstanding_date,"%m-%Y") as analysis_date');
        $this->db->from('currency c');
        $this->db->join('project_facility pf','pf.currency_id = c.id_currency');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") >= "'.$data['start_month'].'"and DATE_FORMAT(oa.outstanding_date,"%m-%Y") <= "'.$data['end_month'].'"');
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") >= "'.$data['start_month'].'" and DATE_FORMAT(a.analysis_month,"%m-%Y") <= "'.$data['end_month'].'" and a.proposed = "'.$data['outstanding_status'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->group_by('c.id_currency,DATE_FORMAT(oa.outstanding_date,"%m-%Y")');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 'c.currency_code',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getPortfolioBySector($data)
    {
        $this->db->select('s.id_sector,s.sector_name,sum(oa.usd_equiv) as amount');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 's.sector_name',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioBySectorAnalysis($data)
    {
        $this->db->select('s.id_sector,s.sector_name,sum(oa.usd_equiv) as amount,DATE_FORMAT(oa.outstanding_date,"%m-%Y") as analysis_date');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") >= "'.$data['start_month'].'"and DATE_FORMAT(oa.outstanding_date,"%m-%Y") <= "'.$data['end_month'].'"');
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") >= "'.$data['start_month'].'" and DATE_FORMAT(a.analysis_month,"%m-%Y") <= "'.$data['end_month'].'" and a.proposed = "'.$data['outstanding_status'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector,,DATE_FORMAT(oa.outstanding_date,"%m-%Y")');
        $this->db->order_by((isset($data['order_by'])) ? $data['order_by'] : 's.sector_name',(isset($data['order'])) ? $data['order'] : 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPortfolioByESGrade($data)
    {
        $this->db->select('pg.id_project_es_grade,pg.grade,sum(oa.usd_equiv) as amount');
        $this->db->from('project_facility pf');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('(select * from project_es_grade group by project_id order by id_project_es_grade desc) pg','pg.project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and oa.record_status = 1');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        $this->db->where('DATE_FORMAT(oa.outstanding_date,"%m-%Y") =',$data['date']);
        $this->db->group_by('pg.grade');
        $this->db->order_by('pg.grade','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingExposureByProduct($data)
    {
        $this->db->select('mc.id_child as id,mc.child_name as product_name,if(mc.parent_id is NULL,0,mc.parent_id) as parent_id,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id');
        $this->db->join('project_facility pf','pf.facility_type = mc.id_child or pf.facility_sub_type = mc.id_child');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        $this->db->group_by('mc.id_child');
        $this->db->order_by('mc.child_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingAgingByProduct($data)
    {
        $this->db->select('mc.id_child as id,mc.child_name as product_name,mc.parent_id,sum(a.lcy_arrears) as arrears,a.proposed');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id');
        $this->db->join('project_facility pf','pf.facility_type = mc.id_child or pf.facility_sub_type = mc.id_child');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        $this->db->group_by('mc.id_child,a.proposed');
        $this->db->order_by('mc.child_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingAgingByProductItem($data)
    {
        $this->db->select('mc.id_child as id,mc.child_name as product_name,mc.parent_id,sum(a.lcy_arrears) as arrears,a.proposed');
        $this->db->from('master m');
        $this->db->join('master_child mc','m.id_master=mc.master_id');
        $this->db->join('project_facility pf','pf.facility_sub_type = mc.id_child');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        $this->db->where('m.master_key','facility_sub_type');
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        $this->db->group_by('mc.id_child,a.proposed');
        $this->db->order_by('mc.child_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingExposureBySector($data)
    {
        $this->db->select('s.id_sector as id,s.sector_name,sl.limit_value,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions,0 as amount_compare,0 as transactions_compare');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->join('sector_limit sl','sl.sector_id = s.id_sector and sl.limit_status = 1','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector');
        $this->db->order_by('s.sector_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getOutstandingExposureBySectorLimits($data)
    {
        $this->db->select('s.id_sector as id,s.sector_name,s.parent_sector_id as parent_id,if(sl.limit_value is NULL,0,sl.limit_value) as limit_value,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions,0 as amount_compare,0 as transactions_compare');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('sector_limit sl','sl.sector_id = s.id_sector and sl.limit_status = 1','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector');
        $this->db->order_by('s.sector_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getOutstandingExposureBySubSectorLimits($data)
    {
        $this->db->select('s.id_sector as id,s.sector_name,s.parent_sector_id as parent_id,if(sl.limit_value is NULL,0,sl.limit_value) as limit_value,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions,0 as amount_compare,0 as transactions_compare');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_sub_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('sector_limit sl','sl.sector_id = s.id_sector and sl.limit_status = 1','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector');
        $this->db->order_by('s.sector_name','asc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getOutstandingAgingBySector($data)
    {
        $this->db->select('s.id_sector as id,s.sector_name,sum(a.lcy_arrears) as arrears,a.proposed');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('s.id_sector,a.proposed');
        $this->db->order_by('s.sector_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingExposureByCurrency($data)
    {
        $this->db->select('c.id_currency as id,c.currency_code,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions');
        $this->db->from('currency c');
        $this->db->join('project_facility pf','pf.currency_id = c.id_currency');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('c.id_currency');
        $this->db->order_by('c.currency_code','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingAgingByCurrency($data)
    {
        $this->db->select('c.id_currency as id,c.currency_code,sum(a.lcy_arrears) as arrears,a.proposed');
        $this->db->from('currency c');
        $this->db->join('project_facility pf','pf.currency_id = c.id_currency');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('c.id_currency,a.proposed');
        $this->db->order_by('c.currency_code','asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getOutstandingExposureByCountry($data)
    {
        $this->db->select('c.id_country as id,c.country_name,sum(oa.usd_equiv) as amount,count(DISTINCT pf.id_project_facility) as transactions');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id and oa.record_status = 1 and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"','left');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('c.id_country');
        $this->db->order_by('c.country_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingAgingByCountry($data)
    {
        $this->db->select('c.id_country as id,c.country_name,sum(a.lcy_arrears) as arrears,a.proposed');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"');
        $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['sector_id']) && $data['sector_id'] > 0)
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['product_id']) && $data['product_id'] > 0)
            $this->db->where('pf.facility_type',$data['product_id']);
        $this->db->group_by('c.id_country,a.proposed');
        $this->db->order_by('c.country_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnalysisCountries($data)
    {
        $this->db->select('c.id_country,c.country_name');
        $this->db->from('country c');
        $this->db->join('crm_project p','p.country_id = c.id_country');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->where('p.company_id', $data['company_id']);
        $this->db->group_by('c.id_country');
        $this->db->order_by('c.country_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnalysisSectors($data)
    {
        $this->db->select('s.id_sector,s.sector_name');
        $this->db->from('sector s');
        $this->db->join('crm_project p','p.project_main_sector_id = s.id_sector');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project');
        $this->db->where('p.company_id', $data['company_id']);
        $this->db->group_by('s.id_sector');
        $this->db->order_by('s.sector_name','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOutstandingBorrowers($data)
    {
        $this->db->select('c.id_crm_company,company_name,pf.contract_id,cn.country_name, sum(oa.usd_equiv) as amount,a.proposed');
        $this->db->from('crm_company c');
        $this->db->join('project_company pc','pc.crm_company_id = c.id_crm_company');
        $this->db->join('project_facility pf','pf.crm_project_id = pc.crm_project_id');
        $this->db->join('country cn','cn.id_country = c.country_id','left');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"');
        $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('c.country_id',$data['country_id']);
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->where('a.proposed',$data['outstanding_status']);
        $this->db->group_by('c.id_crm_company');
        $this->db->order_by('amount','desc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllOutstandingBorrowers($data)
    {
        $this->db->select('count(DISTINCT c.id_crm_company) as total_records,sum(oa.usd_equiv) as amount');
        $this->db->from('crm_company c');
        $this->db->join('project_company pc','pc.crm_company_id = c.id_crm_company');
        $this->db->join('project_facility pf','pf.crm_project_id = pc.crm_project_id');
        $this->db->join('country cn','cn.id_country = c.country_id','left');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('aging a','a.contract_id = pf.contract_id and a.record_status = 1 and DATE_FORMAT(a.analysis_month,"%m-%Y") = "'.$data['date'].'"');
        $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('c.country_id',$data['country_id']);
        if(isset($data['outstanding_status']) && $data['outstanding_status'] != '')
            $this->db->where('a.proposed',$data['outstanding_status']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getAllValueAtRisk($data)
    {
        $this->db->select('c.company_name,p.project_title,pf.contract_id,pf.facility_maturity_date,oa.usd_equiv as loan_portfolio,oa.total_provision,if(ir.pd is NULL,0,ir.pd) as pd,v.lgd, v.confidence_level, v.scaling_factor');
        $this->db->from('project_facility pf');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('project_company pc','pc.crm_project_id = p.id_crm_project and pc.company_type_id = 1 and pc.project_company_status = 1');
        $this->db->join('crm_company c','c.id_crm_company = pc.crm_company_id');
        $this->db->join('internal_rating ir','c.internal_rating >= ir.value_from and c.internal_rating <= ir.value_to');
        $this->db->join('value_at_risk v','v.company_id = '.$data['company_id']);
        $this->db->where('p.company_id',$data['company_id']);
        $this->db->where('pf.facility_type != ',12);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getValueAtRisk($data)
    {
        $this->db->select('c.company_name,p.project_title,pf.contract_id,pf.facility_maturity_date,oa.usd_equiv as loan_portfolio,oa.total_provision,if(ir.pd is NULL,0,ir.pd) as pd,v.lgd, v.confidence_level, v.scaling_factor');
        $this->db->from('project_facility pf');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$data['date'].'"');
        $this->db->join('crm_project p','p.id_crm_project = pf.crm_project_id');
        $this->db->join('project_company pc','pc.crm_project_id = p.id_crm_project and pc.company_type_id = 1 and pc.project_company_status = 1');
        $this->db->join('crm_company c','c.id_crm_company = pc.crm_company_id');
        $this->db->join('internal_rating ir','c.internal_rating >= ir.value_from and c.internal_rating <= ir.value_to');
        $this->db->join('value_at_risk v','v.company_id = '.$data['company_id']);
        $this->db->where('p.company_id',$data['company_id']);
        $this->db->where('pf.facility_type != ',12);
        if(isset($data['country_id']) && $data['country_id'] > 0)
            $this->db->where('p.country_id',$data['country_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_model extends CI_Model
{
    public function getTask($data)
    {
        $this->db->select("t.id_task,t.task_name,t.task_description,t.task_type,t.created_by,CONCAT(u1.first_name,' ',u1.last_name) as created_by_name,t.task_status,t.module_type,t.module_id,t.comment,group_concat(tm.member_id) as assigned_to,t.created_date_time,DATE(t.task_due_date) as task_due_date,GROUP_CONCAT(CONCAT(u.first_name,' ',u.last_name)) as user_name,
             CASE
              WHEN t.module_type ='project' THEN cp.project_title
              WHEN t.module_type ='company' THEN cc.company_name
             ELSE co.first_name END as title
        ");
        $this->db->from('task t');
        $this->db->join('task_member tm','t.id_task=tm.task_id','left');
        $this->db->join('user u','tm.member_id=u.id_user','left');
        $this->db->join('user u1','t.created_by=u1.id_user','left');

        $this->db->join('crm_project cp','t.module_id=cp.id_crm_project and t.module_type = "project"','left');
        $this->db->join('crm_company cc','t.module_id=cc.id_crm_company and t.module_type = "company"','left');
        $this->db->join('crm_contact co','t.module_id=co.id_crm_contact and t.module_type = "contact"','left');
        //$this->db->join('crm_project p','p.id_crm_project=t.project_id','left');
        $this->db->where('t.task_status!=', 'deleted');

        if(isset($data['id_task']))
            $this->db->where('t.id_task', $data['id_task']);

        if(isset($data['module_id']))
            $this->db->where('t.module_id', $data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('t.module_type', $data['module_type']);
        if(isset($data['module_reference_id']))
            $this->db->where('t.module_reference_id', $data['module_reference_id']);
        if(isset($data['module_reference_type']))
            $this->db->where('t.module_reference_type', $data['module_reference_type']);


        if(isset($data['type']) && $data['type']=='due_task') {
            $this->db->where('task_due_date <= CURDATE()');
            $this->db->where('t.task_status !=', 'completed');
        }
        if(isset($data['type']) && $data['type']=='upcoming_task') {
            $this->db->where('task_due_date > CURDATE()');
            $this->db->where('t.task_status !=', 'completed');
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(task_name like "%'.$data['search_key'].'%" or task_description like "%'.$data['search_key'].'%" or task_type like "%'.$data['search_key'].'%")');
        }

        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(t.created_by = '.$data['created_by'].' or tm.member_id ='.$data['assigned_to'].')');
        }
        else {
            if (isset($data['created_by']))
                $this->db->where('t.created_by ', $data['created_by']);
            if (isset($data['assigned_to'])) {
                $this->db->where('tm.member_id ', $data['assigned_to']);
            }
        }

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->order_by('t.id_task','DESC');
        $this->db->group_by('t.id_task');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalTasks($data)
    {
        //$this->db->select("count(*) as total_tasks");
        $this->db->select('count(DISTINCT(t.id_task)) as total_tasks');
        $this->db->from('task t');
        $this->db->join('task_member tm','t.id_task=tm.task_id','left');
        $this->db->where('t.task_status!=', 'deleted');

        if(isset($data['id_task']))
            $this->db->where('t.id_task', $data['id_task']);

        if(isset($data['module_id']))
            $this->db->where('t.module_id', $data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('t.module_type', $data['module_type']);
        if(isset($data['module_reference_id']))
            $this->db->where('t.module_reference_id', $data['module_reference_id']);
        if(isset($data['module_reference_type']))
            $this->db->where('t.module_reference_type', $data['module_reference_type']);

        if(isset($data['due_task'])) {
            $this->db->where('task_due_date <= CURDATE()');
            $this->db->where('t.task_status !=', 'completed');
        }
        if(isset($data['upcoming_task'])) {
            $this->db->where('task_due_date > CURDATE()');
            $this->db->where('t.task_status !=', 'completed');
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(task_name like "%'.$data['search_key'].'%" or task_description like "%'.$data['search_key'].'%" or task_type like "%'.$data['search_key'].'%")');
        }

        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(t.created_by = '.$data['created_by'].' or tm.member_id ='.$data['assigned_to'].')');
        }
        else
        {
            if (isset($data['created_by']))
                $this->db->where('t.created_by ', $data['created_by']);
            if (isset($data['assigned_to'])) {
                $this->db->where('tm.member_id ', $data['assigned_to']);
            }
        }

        if(isset($data['status']))
            $this->db->where('t.task_status', $data['status']);
        if(isset($data['project_id']))
            $this->db->where('project_id', $data['project_id']);

        $this->db->group_by('id_task');
        $this->db->order_by('id_task','DESC');
        $query = $this->db->get();

        $count =  $query->result_array();
        $count = array_sum(array_map(function($i){ return $i['total_tasks']; },$count));
        return $count;
        //return $query->row();
    }

    public function addTask($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->insert('task', $data);
        return $this->db->insert_id();
    }

    public function addTaskMembers($data)
    {
        $this->db->insert_batch('task_member', $data);
        return $this->db->insert_id();
    }

    public function deleteTaskMembers($data)
    {
        $this->db->where_in('id_task_member', $data);
        $this->db->delete('task_member');
    }

    public function getTaskMembers($data)
    {
        $this->db->select('*');
        $this->db->from('task_member');
        if(isset($data['task_id']))
            $this->db->where('task_id',$data['task_id']);
        $query = $this->db->get();
        return $query->result_array($data);
    }

    public function getMeetingList($data)
    {
        $this->db->select('m.*,cb.legal_name,CONCAT(u.first_name, " ", u.last_name) as username,GROUP_CONCAT(i.invitation_id) as invitation_id,GROUP_CONCAT(CONCAT(us.first_name," ",us.last_name)) as invitation_user_name');
        $this->db->from('meeting m');
        $this->db->join('crm_contact ct','m.module_id=ct.id_crm_contact and module_type="contact" and ct.company_id='.$data['company_id'],'left');
        $this->db->join('crm_company cm','m.module_id=cm.id_crm_company and module_type="company" and cm.company_id='.$data['company_id'],'left');
        $this->db->join('crm_project cp','m.module_id=cp.id_crm_project and module_type="project" and cp.company_id='.$data['company_id'],'left');
        $this->db->join('company_user cu','cu.user_id=m.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('user u','u.id_user=m.created_by','left');
        $this->db->join('meeting_invitation i','m.id_meeting=i.meeting_id','left');
        $this->db->join('user us','i.invitation_id=us.id_user','left');
        $this->db->where('m.module_id IS NOT NULL');
        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by ='.$data['created_by'].' or i.invitation_id = '.$data['assigned_to'].')');
        }
        else{
            if(isset($data['created_by']))
                $this->db->where('m.created_by ',$data['created_by']);
            if(isset($data['assigned_to'])){
                $this->db->where('i.invitation_id',$data['assigned_to']);
            }
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.meeting_name like "%'.$data['search_key'].'%" or m.meeting_type like "%'.$data['search_key'].'%" )');
        }


        $this->db->group_by('m.id_meeting');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getMeetingListCount($data)
    {
        $this->db->select('count(DISTINCT(m.id_meeting)) as total');
        $this->db->from('meeting m');
        $this->db->join('meeting_invitation i','m.id_meeting=i.meeting_id','left');
        $this->db->where('m.module_id IS NOT NULL');
        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by ='.$data['created_by'].' or i.invitation_id = '.$data['assigned_to'].')');
        }
        else{
            if(isset($data['created_by']))
                $this->db->where('m.created_by ',$data['created_by']);
            if(isset($data['assigned_to'])){
                $this->db->where('i.invitation_id',$data['assigned_to']);
            }
        }
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.meeting_name like "%'.$data['search_key'].'%" or m.meeting_type like "%'.$data['search_key'].'%" )');
        }
        $this->db->group_by('m.id_meeting');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskList($data)
    {
        $this->db->select('m.*,cb.legal_name,CONCAT(u.first_name, " ", u.last_name) as username,GROUP_CONCAT(CONCAT(u1.first_name,\' \',u1.last_name)) as assigned_to');
        $this->db->from('task m');
        $this->db->join('crm_contact ct','m.module_id=ct.id_crm_contact and module_type="contact" and ct.company_id='.$data['company_id'],'left');
        $this->db->join('crm_company cm','m.module_id=cm.id_crm_company and module_type="company" and cm.company_id='.$data['company_id'],'left');
        $this->db->join('crm_project cp','m.module_id=cp.id_crm_project and module_type="project" and cp.company_id='.$data['company_id'],'left');
        $this->db->join('company_user cu','cu.user_id=m.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('user u','u.id_user=m.created_by','left');
        $this->db->join('task_member tm','m.id_task=tm.task_id','left');
        $this->db->join('user u1','tm.member_id=u1.id_user','left');
        $this->db->where('m.module_id IS NOT NULL');
        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by = '.$data['created_by'].' or tm.member_id ='.$data['assigned_to'].')');
        }
        else {
            if (isset($data['created_by']))
                $this->db->where('m.created_by ', $data['created_by']);
            if (isset($data['assigned_to'])) {
                $this->db->where('tm.member_id ', $data['assigned_to']);
            }
        }
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.task_name like "%'.$data['search_key'].'%" or m.task_type like "%'.$data['search_key'].'%" or m.module_type like "%'.$data['search_key'].'%" )');
        }
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->group_by('m.id_task');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskListCount($data)
    {
        $this->db->select('count(DISTINCT(m.id_task)) as total');
        $this->db->from('task m');
        $this->db->join('task_member tm','m.id_task=tm.task_id','left');
        $this->db->where('m.module_id IS NOT NULL');
        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by = '.$data['created_by'].' or tm.member_id ='.$data['assigned_to'].')');
        }
        else {
            if (isset($data['created_by']))
                $this->db->where('m.created_by ', $data['created_by']);
            if (isset($data['assigned_to'])) {
                $this->db->where('tm.member_id ', $data['assigned_to']);
            }
        }
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.task_name like "%'.$data['search_key'].'%" or m.task_type like "%'.$data['search_key'].'%" or m.module_type like "%'.$data['search_key'].'%" )');
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addMeeting($data)
    {
        $this->db->insert('meeting', $data);
        return $this->db->insert_id();
    }

    public function updateMeeting($data)
    {
        unset($data['invitation_user_name']);
        $id_meeting = $data['id_meeting'];
        $this->db->where('id_meeting', $id_meeting);
        $this->db->update('meeting', $data);
    }

    public function getInvitationsByMeetingId($meeting_id)
    {
        $this->db->select('*');
        $this->db->from('meeting_invitation');
        $this->db->where('meeting_id',$meeting_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getGuestByMeetingId($meeting_id)
    {
        $this->db->select('*');
        $this->db->from('meeting_guest');
        $this->db->where('meeting_id',$meeting_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteMeetingInvitation($invitation_id,$meeting_id)
    {
        $this->db->where(array('invitation_id' => $invitation_id, 'meeting_id' => $meeting_id));
        $this->db->delete('meeting_invitation');
    }

    public function deleteMeetingGuest($email,$meeting_id)
    {
        $this->db->where(array('email' => $email, 'meeting_id' => $meeting_id));
        $this->db->delete('meeting_guest');
    }

    public function addMeetingInvitations($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->insert_batch('meeting_invitation', $data);
        return $this->db->insert_id();
    }

    public function addMeetingGuest($data)
    {
        $this->db->insert_batch('meeting_guest', $data);
        return $this->db->insert_id();
    }

    public function getMeeting($data)
    {
        $this->db->select('m.id_meeting,m.*,DATE(m.when) as when_date,"1" as meeting_expiry_status,GROUP_CONCAT(i.invitation_id) as invitation_id,GROUP_CONCAT(CONCAT(us.first_name," ",us.last_name)) as invitation_user_name,CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('meeting m');
        //$this->db->join('meeting_guest mg','m.id_meeting=mg.meeting_id','left');
        $this->db->join('user u','m.created_by=u.id_user','left');
        $this->db->join('meeting_invitation i','m.id_meeting=i.meeting_id','left');
        $this->db->join('user us','i.invitation_id=us.id_user','left');
        $this->db->where('m.meeting_status!=','deleted');
        //$this->db->join('crm_project p','p.id_crm_project=m.project_id','left');
        if(isset($data['id_meeting']))
            $this->db->where('m.id_meeting',$data['id_meeting']);

        if(isset($data['reference_id']))
            $this->db->where('m.reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
            $this->db->where('m.reference_type',$data['reference_type']);
        //if(isset($data['reference_type']) && $data['reference_type']=='workflow'){
            $this->db->join('project_stage_workflow_phase pswp','m.reference_id=pswp.id_project_stage_workflow_phase','left');
            if(isset($data['project_stage_info_id']))
                $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        //}

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.meeting_name like "%'.$data['search_key'].'%" or m.meeting_description like "%'.$data['search_key'].'%" or m.meeting_type like "%'.$data['search_key'].'%")');
        }

        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by ='.$data['created_by'].' or i.invitation_id = '.$data['assigned_to'].')');
        }
        else{
            if(isset($data['created_by']))
                $this->db->where('m.created_by ',$data['created_by']);
            if(isset($data['assigned_to'])){
                $this->db->where('i.invitation_id',$data['assigned_to']);
            }
        }

        if(isset($data['status'])){
            $this->db->where('m.meeting_status',$data['status']);
        }

        if(isset($data['type']) && $data['type']=='due_meeting'){
            $this->db->where('when <= CURDATE()');
            $this->db->where('m.meeting_status !=','completed');
            $this->db->where('m.meeting_status !=','cancelled');
        }
        if(isset($data['type']) && $data['type']=='upcoming_meeting'){
            $this->db->where('when > CURDATE()');
            $this->db->where('m.meeting_status !=','completed');
            $this->db->where('m.meeting_status !=','cancelled');
        }


        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->group_by('m.id_meeting');
        $this->db->order_by('m.id_meeting','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        if(isset($data['id_meeting'])){
            return $query->row();
        }
        else{
            return $query->result_array();
        }
    }

    public function getTotalMeetings($data)
    {
        $this->db->select('count(DISTINCT(m.id_meeting)) as total_meetings');
        $this->db->from('meeting m');
        $this->db->join('meeting_invitation i','m.id_meeting=i.meeting_id','left');
        //$this->db->join('meeting_guest mg','m.id_meeting=mg.meeting_id','left');

        $this->db->where('m.meeting_status!=','deleted');
        //$this->db->join('crm_project p','p.id_crm_project=m.project_id','left');
        if(isset($data['id_meeting']))
            $this->db->where('m.id_meeting',$data['id_meeting']);
        if(isset($data['status']))
            $this->db->where('m.meeting_status',$data['status']);
        if(isset($data['created_by']) && isset($data['assigned_to']))
        {
            $this->db->where('(m.created_by ='.$data['created_by'].' or i.invitation_id = '.$data['assigned_to'].')');
            //$this->db->where('(m.created_by ='.$data['created_by'].')');
        }
        else{
            if(isset($data['created_by']))
                $this->db->where('m.created_by ',$data['created_by']);
            if(isset($data['assigned_to'])){
                $this->db->where('i.invitation_id',$data['assigned_to']);
            }
        }
        if(isset($data['reference_id']))
            $this->db->where('m.reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
            $this->db->where('m.reference_type',$data['reference_type']);
        //if(isset($data['reference_type']) && $data['reference_type']=='workflow'){
        $this->db->join('project_stage_workflow_phase pswp','m.reference_id=pswp.id_project_stage_workflow_phase','left');
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        //}

        if(isset($data['due_meeting'])){
            $this->db->where('when <= CURDATE()');
            $this->db->where('m.meeting_status !=','completed');
            $this->db->where('m.meeting_status !=','cancelled');
        }
        if(isset($data['upcoming_meeting'])){
            $this->db->where('when > CURDATE()');
            $this->db->where('m.meeting_status !=','completed');
            $this->db->where('m.meeting_status !=','cancelled');
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(m.meeting_name like "%'.$data['search_key'].'%" or m.meeting_description like "%'.$data['search_key'].'%" or m.meeting_type like "%'.$data['search_key'].'%")');
        }
        $this->db->group_by('m.id_meeting');

        $query = $this->db->get();
        //echo $this->db->last_query();
        $count =  $query->result_array();
        $count = array_sum(array_map(function($i){ return $i['total_meetings']; },$count));
        return $count;
    }

    public function getActivity($data)
    {
        $this->db->select('a.*,p.project_title,DATE(a.created_date_time) as date');
        $this->db->from('activity a');
        $this->db->join('crm_project p','p.id_crm_project=a.project_id','left');
        if(isset($data['project_id']))
            $this->db->where('a.project_id',$data['project_id']);

        if(isset($data['activity_type']))
            $this->db->where('a.activity_type',$data['activity_type']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(a.activity_type like "%'.$data['search_key'].'%")');
        }

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->order_by('a.id_activity','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addActivity($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->insert('activity', $data);
        return $this->db->insert_id();
    }

    public function updateTask($data)
    {
        $id_task = $data['id_task'];
        if(isset($data['user_name']))
            unset($data['user_name']);
        if(isset($data['project_title']))
            unset($data['project_title']);
        $this->db->where('id_task', $id_task);
        $this->db->update('task', $data);
    }

    public function addActivityDiscussion($data)
    {
        $this->db->insert('activity_discussion', $data);
        return $this->db->insert_id();
    }

    public function getActivityDiscussion($data)
    {
        $this->db->select('*');
        $this->db->from('activity_discussion');
        if(isset($data['activity_id']))
            $this->db->where('activity_id', $data['activity_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->order_by('id_activity_discussion','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDiscussion()
    {
        $this->db->select('*');
        $this->db->form('discussion');

        if(isset($data['discussion_reference_id'])){
            $this->db->where('discussion_reference_id', $data['discussion_reference_id']);
        }
        if(isset($data['discussion_type'])){
            $this->db->where('discussion_type', $data['discussion_type']);
        }
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTotalDiscussion()
    {
        $this->db->select('count(*) as total_records');
        $this->db->form('discussion');

        if(isset($data['discussion_reference_id'])){
            $this->db->where('discussion_reference_id', $data['discussion_reference_id']);
        }
        if(isset($data['discussion_type'])){
            $this->db->where('discussion_type', $data['discussion_type']);
        }
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        return $query->row();
    }

    public function addDiscussion($data)
    {
        $this->db->insert('discussion', $data);
        return $this->db->insert_id();
    }

    public function updateDiscussion($data)
    {
        $id_discussion = $data['id_discussion'];
        $this->db->where('id_discussion', $id_discussion);
        $this->db->update('discussion', $data);
    }

    public function getNotification($data)
    {
        $this->db->select('n.*,DATE(n.created_date_time) as date,p.project_title,CONCAT(u.first_name," ",u.last_name) as to_user,CONCAT(us.first_name," ",us.last_name) as from_user');
        $this->db->from('notification n');
        $this->db->join('user u','n.sent_to=u.id_user','left');
        $this->db->join('user us','n.sent_by=us.id_user','left');
        $this->db->join('crm_project p','p.id_crm_project=n.project_id','left');
        if(isset($data['project_id']))
            $this->db->where('n.project_id', $data['project_id']);

        if(isset($data['company_id']))
            $this->db->where('p.company_id', $data['company_id']);

        if(isset($data['sent_to']))
            $this->db->where('n.sent_to', $data['sent_to']);

        if(isset($data['sent_by']))
            $this->db->where('n.sent_by', $data['sent_by']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('notification_status !=','deleted');
        $this->db->order_by('n.id_notification','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addNotification($data)
    {
        $this->db->insert('notification', $data);
        return $this->db->insert_id();
    }

    public function updateNotification($data)
    {
        $id_notification = $data['id_notification'];
        $this->db->where('id_notification', $id_notification);
        $this->db->update('notification', $data);
    }

    public function getAttachments($from_id,$crm_document_type_id)
    {
        $this->db->select('*,DATE(created_date_time) as date,document_source as full_path');
        $this->db->from('crm_document');
        $this->db->where(array('uploaded_from_id' => $from_id, 'crm_document_type_id' => $crm_document_type_id));
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addApproval($data)
    {
        $this->db->insert('project_approval', $data);
        return $this->db->insert_id();
    }

    public function getProjectApprovalMeeting($data)
    {
        $this->db->select('*');
        $this->db->from('project_approval_meeting');
        if(isset($data['project_approval_id']))
            $this->db->where('project_approval_id',$data['project_approval_id']);
        if(isset($data['meeting_id']))
            $this->db->where('meeting_id',$data['meeting_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateApproval($data)
    {
        $this->db->where('id_project_approval',$data['id_project_approval']);
        $this->db->update('project_approval',$data);
    }

    public function addProjectApprovalMeeting($data)
    {
        $this->db->insert('project_approval_meeting', $data);
        return $this->db->insert_id();
    }

    public function getProjectApproval($data)
    {
        $this->db->select('*');
        $this->db->from('project_approval');
        if(isset($data['id_project_approval']))
            $this->db->where('id_project_approval',$data['id_project_approval']);
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['user_id']))
            $this->db->where("(forwarded_by=".$data['user_id']." OR forwarded_to=".$data['user_id'].")");
        if(isset($data['forwarded_by']))
            $this->db->where('forwarded_by',$data['forwarded_by']);
        if(isset($data['forwarded_to']))
            $this->db->where('forwarded_to',$data['forwarded_to']);
        if(isset($data['company_approval_credit_committee_id']))
            $this->db->where('company_approval_credit_committee_id',$data['company_approval_credit_committee_id']);
        if(isset($data['order_by']))
            $this->db->order_by('id_project_approval',$data['order_by']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function deleteActivity($data)
    {
        $this->db->where('id_activity', $data['id_activity']);
        $this->db->update('activity', array('activity_status' => 0));
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function deleteNotification($data)
    {
        if(isset($data['module_id']))
            $this->db->where('module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('module_type',$data['module_type']);
        if(isset($data['module_reference_type']))
            $this->db->where('module_reference_type',$data['module_reference_type']);
        if(isset($data['module_reference_id']))
            $this->db->where('module_reference_id',$data['module_reference_id']);
        if(isset($data['id_notification']))
            $this->db->where('id_notification',$data['id_notification']);

        $this->db->update('notification',array('notification_status' => 'deleted'));
    }

    public function deleteTouchPoint($data)
    {
        if(isset($data['module_id']))
            $this->db->where('module_id',$data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('module_type',$data['module_type']);
        if(isset($data['activity_reference_id']))
            $this->db->where('activity_reference_id',$data['activity_reference_id']);
        if(isset($data['activity_type']))
            $this->db->where('activity_type',$data['activity_type']);
        if(isset($data['id_activity']))
            $this->db->where('id_activity',$data['id_activity']);

        $this->db->update('activity',array('activity_status' => 0));
    }

}
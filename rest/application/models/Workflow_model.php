<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Workflow_model extends CI_Model
{
    public function __construct(){
        parent::__construct();

    }

    public function getWorkflowPhase($data)
    {
        $this->db->select('*');
        $this->db->from('workflow_phase wph');
        if(isset($data['workflow_phase_id']))
            $this->db->where('wph.id_workflow_phase',$data['workflow_phase_id']);
        if(isset($data['parent_workflow_phase_id']))
            $this->db->where('wph.parent_workflow_phase_id',$data['parent_workflow_phase_id']);
        if(isset($data['workflow_id']))
            $this->db->where('wph.workflow_id',$data['workflow_id']);
        if(isset($data['project_id']))
            $this->db->where('wph.project_id',$data['project_id']);
        if(isset($data['project_stage_id']))
            $this->db->where('wph.project_stage_id',$data['project_stage_id']);
        if(isset($data['is_start']))
            $this->db->where('wph.is_start',$data['is_start']);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getWorkflowPhaseAction($data)
    {
        $this->db->select('*');
        $this->db->from('workflow_phase_action wpa');
        $this->db->join('workflow_phase wph','wpa.workflow_phase_id=wph.id_workflow_phase','left');
        $this->db->join('workflow_action wc','wpa.workflow_action_id=wc.id_workflow_action','left');
        if(isset($data['workflow_phase_id']))
            $this->db->where('wpa.workflow_phase_id',$data['workflow_phase_id']);
        if(isset($data['workflow_phase_action_id'])){
            $this->db->where('wpa.id_workflow_phase_action',$data['workflow_phase_action_id']);
        }
        if(isset($data['workflow_action_id'])){
            $this->db->where('wpa.workflow_action_id',$data['workflow_action_id']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getWorkflowPhaseDetails($data)
    {
        $this->db->select('*');
        $this->db->from('workflow_phase wp');
        if(isset($data['workflow_phase_id']))
            $this->db->where('wp.id_workflow_phase',$data['workflow_phase_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPhaseStageWorkflowDetails($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_workflow psw');
        $this->db->join('project_stage_workflow_phase pswp','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id');
        if(isset($data['workflow_phase_id']))
            $this->db->where('psw.forward_to_workflow_phase_id',$data['workflow_phase_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['status']))
            $this->db->where('pswp.status',$data['status']);
        $this->db->order_by('psw.id_project_stage_workflow','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPhaseStageWorkflowStatus($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_workflow_phase pswp');
        if(isset($data['workflow_phase_id']))
            $this->db->where('pswp.parent_workflow_phase_id',$data['workflow_phase_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['status']))
            $this->db->where('pswp.status',$data['status']);
        $this->db->order_by('pswp.id_project_stage_workflow_phase','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getWorkflowPhaseRoels($data)
    {
        $this->db->select('*');
        $this->db->from('workflow_phase_role wpr');
        if(isset($data['workflow_phase_id']))
            $this->db->where('workflow_phase_id',$data['workflow_phase_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUsersByApprovalRole($data)
    {
        $this->db->select('cu.user_id,CONCAT(u.first_name,\' \',u.last_name) as user_name,ar.approval_name');
        $this->db->from('company_user cu');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('company_approval_role car','cu.company_approval_role_id=car.id_company_approval_role','left');
        $this->db->join('approval_role ar','car.approval_role_id=ar.id_approval_role','left');
        if(isset($data['approval_role_array']))
            $this->db->where_in('car.approval_role_id',$data['approval_role_array']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getUsersByApprovalUser($data)
    {
        $this->db->select('cu.user_id,CONCAT(u.first_name,\' \',u.last_name) as user_name,ar.approval_name');
        $this->db->from('company_user cu');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('company_approval_role car','cu.company_approval_role_id=car.id_company_approval_role','left');
        $this->db->join('approval_role ar','car.approval_role_id=ar.id_approval_role','left');
        if(isset($data['approval_role_array']))
            $this->db->where_in('u.id_user',$data['approval_role_array']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProjectStageWorkflow($data)
    {
        $this->db->insert('project_stage_workflow', $data);
        return $this->db->insert_id();
    }

    public function updateProjectStageInfo($data)
    {
        $this->db->where('id_project_stage_info',$data['id_project_stage_info']);
        $this->db->update('project_stage_info', $data);
        return 1;
    }

    public function addProjectStageWorkflowPhase($data)
    {
        $this->db->insert('project_stage_workflow_phase', $data);
        return $this->db->insert_id();
    }
    public function addProjectStageStatusLog($data)
    {
        $this->db->insert('project_stage_status_log', $data);
        return $this->db->insert_id();
    }

    public function getProjectStageWorkflow($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_workflow');
        if(isset($data['forward_to_workflow_phase_id']))
            $this->db->where('forward_to_workflow_phase_id',$data['forward_to_workflow_phase_id']);

        if(isset($data['project_stage_workflow_phase_id']))
            $this->db->where('project_stage_workflow_phase_id',$data['project_stage_workflow_phase_id']);
        if(isset($data['order_by'])){
            $this->db->order_by('id_project_stage_workflow',$data['order_by']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectStageWorkflowPhaseDetails($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_workflow_phase pswp');
        $this->db->join('workflow_phase wp','wp.id_workflow_phase=pswp.workflow_phase_id','left');
        if(isset($data['id_project_stage_workflow_phase']))
            $this->db->where('pswp.id_project_stage_workflow_phase',$data['id_project_stage_workflow_phase']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('project_stage_info_id', $data['project_stage_info_id']);
        if(isset($data['assigned_to']))
            $this->db->where('assigned_to', $data['assigned_to']);
        if(isset($data['phase_array']))
            $this->db->where_in('workflow_phase_id',$data['phase_array']);
        if(isset($data['status']))
            $this->db->where_in('status',$data['status']);
        if(isset($data['workflow_phase_id']))
            $this->db->where('workflow_phase_id',$data['workflow_phase_id']);

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function updateProjectStageWorkflow($data)
    {
        if(isset($data['id_project_stage_workflow']))
            $this->db->where('id_project_stage_workflow',$data['id_project_stage_workflow']);
        $this->db->update('project_stage_workflow', $data);
        return 1;
    }

    public function updateProjectStageWorkflowPhase($data)
    {
        if(isset($data['project_stage_info_id']))
            $this->db->where('project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['assigned_to']))
            $this->db->where('assigned_to',$data['assigned_to']);
        if(isset($data['id_project_stage_workflow_phase']))
            $this->db->where('id_project_stage_workflow_phase',$data['id_project_stage_workflow_phase']);
        $this->db->update('project_stage_workflow_phase', $data);
        return 1;
    }

    public function getNextStageDetails($data)
    {
        $this->db->select('ps1.*,ps2.id_project_stage as next_id_project_stage,ps2.stage_name as next_stage_name,ps2.stage_key as next_stage_key,ps2.stage_order as next_stage_order,ps2.stage_workflow as next_stage_workflow,ps2.workflow_id as next_workflow_id,ps2.next_project_stage_id as next_next_project_stage_id,ps2.stage_class as next_stage_class');
        $this->db->from('project_stage ps1');
        $this->db->join('project_stage ps2','ps1.next_project_stage_id=ps2.id_project_stage','left');
        $this->db->where('ps1.id_project_stage', $data['id_project_stage']);

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectStageWorkflowDetails($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('pswp.id_project_stage_workflow_phase,psw.id_project_stage_workflow,wp.parent_workflow_phase_id,wp.workflow_phase_name as from_phase_name,wp1.workflow_phase_name as to_phase_name,wa.workflow_action_key,wa.workflow_action_key as action,CONCAT(u.first_name,\' \',u.last_name) as from_user_name,CONCAT(u1.first_name,\' \',u1.last_name) as to_user_name,CONCAT(u.email_id) as from_email,CONCAT(u1.email_id) as to_email,pswp.created_date_time,pswp.updated_date_time,DATEDIFF(pswp.updated_date_time,pswp.created_date_time) as days,wa.class,psw.forward_by,psw.forward_to');
        //$this->db->select('psw.id_project_stage_workflow,wp.parent_workflow_phase_id,wp.workflow_phase_name as from_phase_name,wp1.workflow_phase_name as to_phase_name,wa.workflow_action_key as action,CONCAT(u.first_name,\' \',u.last_name) as from_user_name,CONCAT(u1.first_name,\' \',u1.last_name) as to_user_name,CONCAT(u.email_id) as from_email,CONCAT(u1.email_id) as to_email,psw.created_date_time,psw.update_date_time,DATEDIFF(psw.update_date_time,psw.created_date_time) as days,wa.class,psw.forward_by,psw.forward_to');
        //$this->db->from('project_stage_workflow psw');
        $this->db->from('project_stage_workflow_phase pswp');
        $this->db->join('project_stage_workflow psw','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','');
        $this->db->join('workflow_phase_action wpa','psw.workflow_phase_action_id=wpa.id_workflow_phase_action','left');
        $this->db->join('workflow_action wa','wpa.workflow_action_id=wa.id_workflow_action','left');
        $this->db->join('workflow_phase wp','pswp.workflow_phase_id=wp.id_workflow_phase','left');
        $this->db->join('workflow_phase wp1','psw.forward_to_workflow_phase_id=wp1.id_workflow_phase','left');
        $this->db->join('user u','psw.forward_by=u.id_user','left');
        $this->db->join('user u1','psw.forward_to=u1.id_user','left');
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['id_project_stage_workflow']))
            $this->db->where('psw.id_project_stage_workflow',$data['id_project_stage_workflow']);
        if(isset($data['forward_to_workflow_phase_id']))
            $this->db->where('forward_to_workflow_phase_id',$data['forward_to_workflow_phase_id']);
        if(isset($data['forward_to'])) {
            $this->db->select('cb.legal_name');
            $this->db->join('company_user cu','psw.forward_to=cu.user_id','left');
            $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
            $this->db->where('psw.forward_to', $data['forward_to']);
        }
        //$this->db->group_by('psw.workflow_phase_action_id');
        $this->db->where('pswp.status!=', 'need info');
        $this->db->order_by('psw.id_project_stage_workflow','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectStageWorkflowComments($data)
    {
        /*$this->db->select('CONCAT(u.first_name," ",u.last_name) as user_name,u.profile_image,ar.approval_name,psw.created_date_time,psw.project_stage_info_id,psw.id_project_stage_workflow,psw.workflow_comments as comment');
        $this->db->from('project_stage_workflow psw');*/
        /*$this->db->select('pswp.id_project_stage_workflow_phase,pswp.workflow_phase_id,CONCAT(u.first_name," ",u.last_name) as user_name,u.profile_image,ar.approval_name,psw.created_date_time,pswp.project_stage_info_id,psw.id_project_stage_workflow,psw.workflow_comments as comment,CONCAT(u1.first_name," ",u1.last_name) as to_user_name,(case when (wa.workflow_action_key = "") THEN pswp.status  ELSE wa.workflow_action_key end) as status');
        $this->db->from('project_stage_workflow_phase pswp');
        $this->db->join('project_stage_workflow psw','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','');
        $this->db->join('company_user cu','psw.forward_by=cu.user_id','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->join('user u1','psw.forward_to=u1.id_user','left');
        $this->db->join('workflow_phase_action wpai','psw.workflow_phase_action_id=wpai.id_workflow_phase_action','left');
        $this->db->join('workflow_action wa','wpai.workflow_action_id=wa.id_workflow_action','left');
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        //$this->db->group_by('pswp.id_project_stage_workflow_phase');
        $this->db->order_by('psw.id_project_stage_workflow','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();*/

        $this->db->select('pswp.id_project_stage_workflow_phase,psw.id_project_stage_workflow,wp.parent_workflow_phase_id,wp.workflow_phase_name as from_phase_name,wp1.workflow_phase_name as to_phase_name,wa.workflow_action_key as action,CONCAT(u.first_name,\' \',u.last_name) as from_user_name,CONCAT(u1.first_name,\' \',u1.last_name) as to_user_name,CONCAT(u.email_id) as from_email,CONCAT(u1.email_id) as to_email,pswp.created_date_time,pswp.updated_date_time,DATEDIFF(pswp.updated_date_time,pswp.created_date_time) as days,wa.class,psw.forward_by,psw.forward_to,(case when (wa.workflow_action_key is null) THEN pswp.status  ELSE wa.workflow_action_key end) as status,psw.workflow_comments as comment');
        $this->db->from('project_stage_workflow_phase pswp');
        $this->db->join('project_stage_workflow psw','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','');
        $this->db->join('workflow_phase_action wpa','psw.workflow_phase_action_id=wpa.id_workflow_phase_action','left');
        $this->db->join('workflow_action wa','wpa.workflow_action_id=wa.id_workflow_action','left');
        $this->db->join('workflow_phase wp','pswp.workflow_phase_id=wp.id_workflow_phase','left');
        $this->db->join('workflow_phase wp1','psw.forward_to_workflow_phase_id=wp1.id_workflow_phase','left');

        $this->db->join('user u','psw.forward_by=u.id_user','left');
        $this->db->join('user u1','psw.forward_to=u1.id_user','left');
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        $this->db->order_by('psw.id_project_stage_workflow','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectStageStatusLog($data)
    {
        $this->db->select('pssl.*,CONCAT(u.first_name,\' \',u.last_name) as user_name');
        $this->db->from('project_stage_status_log pssl');
        $this->db->join('user u','pssl.created_by=u.id_user');
        if(isset($data['project_stage_info_id']))
            $this->db->where('pssl.project_stage_info_id',$data['project_stage_info_id']);
        $this->db->order_by('pssl.id_project_stage_status_log','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectStageStatus($project_stage_id)
    {
        $this->db->select('*');
        $this->db->from('project_stage_status');
        $this->db->where('project_stage_id',$project_stage_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getParentWorkflow($data)
    {
        $this->db->select('*');
        $this->db->from('project_stage_workflow_phase pswp');
        $this->db->join('project_stage_workflow psw','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','left');
        if(isset($data['forward_to_workflow_phase_id']))
            $this->db->where('forward_to_workflow_phase_id',$data['forward_to_workflow_phase_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('pswp.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['order_by'])){
            $this->db->order_by('id_project_stage_workflow',$data['order_by']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function addProjectStageWorkflowFacility($data)
    {
        $this->db->insert('project_stage_workflow_facility', $data);
        return $this->db->insert_id();
    }

    public function getOutstandingExposure($data)
    {
        $this->db->select('sum(oa.usd_equiv) as amount');
        $this->db->from('crm_project p');
        $this->db->join('project_facility pf','pf.crm_project_id = p.id_crm_project','left');
        $this->db->join('outstanding_amount oa','pf.contract_id = oa.contract_id','left');
        $this->db->where('p.company_id',$data['company_id']);
        $this->db->where('oa.record_status',1);
        $this->db->where('DATE_FORMAT(oa.outstanding_date,"%m-%Y")',$data['date']);
        if(isset($data['sector_id']))
            $this->db->where('p.project_main_sector_id',$data['sector_id']);
        if(isset($data['sub_sector_id']))
            $this->db->where('p.project_sub_sector_id',$data['sub_sector_id']);

        $query = $this->db->get();
        return $query->result_array();
    }
    public function getWorkflowAction()
    {
        $this->db->select('*');
        $this->db->from('workflow_action wa');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getProductPhaseWorkflowAction($data)
    {
        $this->db->select('pwpa.*,wa.*,pwp.workflow_phase_name,ps.stage_name as next_project_stage_name');
        $this->db->from('product_workflow_phase_action pwpa');
        $this->db->join('workflow_action wa','pwpa.workflow_action_id=wa.id_workflow_action','LEFT');
        $this->db->join('product_workflow_phase pwp','pwp.id_product_workflow_phase=pwpa.forward_product_workflow_phase_id','LEFT');
        $this->db->join('project_stage ps','ps.id_project_stage=pwpa.next_project_stage_id','LEFT');
        if(isset($data['id_product_workflow_phase']))
            $this->db->where('pwpa.product_workflow_phase_id',$data['id_product_workflow_phase']);
        $this->db->where('pwpa.status!=',2);
        $query = $this->db->get();
        $result= $query->result_array();
        foreach($result as $k=>$v){
            if($v['next_project_stage_assign_type']=='Role'){
                $next_project_stage_assign_to_name=$this->Company_model->getCompanyApprovalRole(array('approval_role_id'=>$v['next_project_stage_assign_to']));
                $result[$k]['next_project_stage_assign_to_name']=isset($next_project_stage_assign_to_name[0]['approval_name'])?$next_project_stage_assign_to_name[0]['approval_name']:NULL;
            }
            else if($v['next_project_stage_assign_type']=='User' && $v['next_project_stage_assign_to']!=NULL){
                $next_project_stage_assign_to_name=$this->User_model->getUserInfo(array('id'=>$v['next_project_stage_assign_to']));
                $result[$k]['next_project_stage_assign_to_name']=isset($next_project_stage_assign_to_name->first_name)?$next_project_stage_assign_to_name->first_name.' '.$next_project_stage_assign_to_name->last_name:NULL;
            }
            else{
                $result[$k]['next_project_stage_assign_to_name']=NULL;
            }
        }
        return $result;
    }
    public function addProductPhaseWorkflowAction($data)
    {
        $this->db->insert('product_workflow_phase_action', $data);
        return $this->db->insert_id();
    }

    public function updateProductPhaseWorkflowAction($data)
    {
        $this->db->where('id_product_workflow_phase_action',$data['id_product_workflow_phase_action']);
        $this->db->update('product_workflow_phase_action', $data);
        return 1;
    }
    public function getWorkFlowCommiteeUsers($data){
        $this->db->select('wpr.approval_role_id,pswp.assigned_to,pswp.id_project_stage_workflow_phase');
        $this->db->from('workflow_phase_role wpr');
        $this->db->join('project_stage_workflow_phase pswp','wpr.workflow_phase_id=pswp.workflow_phase_id and pswp.assigned_to=wpr.approval_role_id','LEFT');
        $this->db->join('workflow_phase wp','wp.id_workflow_phase=wpr.workflow_phase_id','');
        if(isset($data['workflow_phase_id']))
            $this->db->where('wpr.workflow_phase_id',$data['workflow_phase_id']);
        $this->db->where('wp.workflow_approval_type','Committee');
        //$this->db->where('wp.project_id',$data['project_id']);
        if(isset($data['status']) && $data['status']=='pending'){
            $this->db->where('pswp.assigned_to IS NOT NULL');
            $this->db->where('pswp.status','pending');
        }
        else{
            $this->db->where('pswp.assigned_to IS NULL');
        }

        $this->db->order_by('wpr.id_workflow_phase_role','ASC');
        $query = $this->db->get();
        return $query->result_array();

    }
}
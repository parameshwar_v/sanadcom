<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cleanup_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }



    public function updateContact($data)
    {
        if($data['contact_id']) {
            $data['contact_id'] = array_values(explode(',', $data['contact_id']));
            $this->db->where_in('id_crm_contact', $data['contact_id']);
        }
        $this->db->update('crm_contact',$data['update_data']);
        return 1;
    }

    public function updateCompany($data)
    {
        if($data['company_id']) {
            $data['company_id'] = array_values(explode(',', $data['company_id']));
            $this->db->where_in('id_crm_company', $data['company_id']);
        }
        $this->db->update('crm_company',$data['update_data']);
        return 1;
    }

    public function updateProject($data)
    {
        if($data['project_id']) {
            $data['project_id'] = array_values(explode(',', $data['project_id']));
            $this->db->where_in('id_crm_project', $data['project_id']);
        }
        $this->db->update('crm_project',$data['update_data']);
        return 1;
    }

    public function deleteTouchPoints($data)
    {
        $this->db->where(array('module_id' => $data['module_id'],'module_type' => $data['module_type']));
        $this->db->delete('activity');
        return 1;
    }

    /* * * Delete Contact/Company/Project * * */

    public function deleteProjectData($data)
    {
        $data['project_id'] = $project_id = array_values(explode(',',$data['project_id']));
        /*$data['project_id'] = array();
        foreach($project_id as $i){
            array_push($data['project_id'],$i);
        }*/

        $this->db->where('module_type', 'project');
        $this->db->where_in('module_id',$data['project_id']);
        $this->db->delete('activity');

        $this->db->where('crm_module_type', 'project');
        $this->db->where_in('crm_module_id',$data['project_id']);
        $this->db->delete('crm_module_status');

        $this->db->where_in('project_id', $data['project_id']);
        $this->db->delete('disbursement_request');

        $this->db->where('reference_type', 'project');
        $this->db->where_in('reference_id', $data['project_id']);
        $this->db->delete('form_comments');

        $this->db->where('module_type', 'project');
        $this->db->where_in('module_id', $data['project_id']);
        $this->db->delete('notification');

        $this->db->where_in('crm_project_id', $data['project_id']);
        $this->db->delete('project_company');

        $this->db->where_in('crm_project_id', $data['project_id']);
        $this->db->delete('project_contact');

        $this->db->where_in('project_id', $data['project_id']);
        $this->db->delete('project_es_grade');

        $this->db->where_in('project_id',$data['project_id']);
        $this->db->delete('project_form_info');

        $this->db->where_in('crm_project_id', $data['project_id']);
        $this->db->delete('project_stage_section_form_last_update');

        $this->db->where_in('project_id', $data['project_id']);
        $this->db->delete('supervision_assessment');

        $this->db->where_in('project_id', $data['project_id']);
        $this->db->delete('supervision_project_committee');

        $this->db->query('delete tm.* from task_member tm left join task t on t.id_task=tm.task_id where t.module_type="project" and t.module_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where('module_type', 'project');
        $this->db->where_in('module_id', $data['project_id']);
        $this->db->delete('task');

        /*$this->db->join('crm_document cd','cd.id_crm_document=cdv.crm_document_id','left');
        $this->db->where(array('module_type' => 'project', 'uploaded_from_id'=> $data['project_id']));
        $this->db->delete('crm_document_version cdv');*/

        $this->db->query('delete cdv.* from crm_document_version cdv left join crm_document cd on cd.id_crm_document=cdv.crm_document_id WHERE cd.module_type="project" and cd.uploaded_from_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where('module_type', 'project');
        $this->db->where_in('uploaded_from_id', $data['project_id']);
        $this->db->delete('crm_document');

        /*$this->db->join('meeting m','m.id_meeting=mg.meeting_id','left');
        $this->db->where(array('module_type' => 'project', 'module_id'=> $data['project_id']));
        $this->db->delete('meeting_guest mg');*/

        $this->db->query('delete mg.* from meeting_guest mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="project" and m.module_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('meeting m','m.id_meeting=mg.meeting_id','left');
        $this->db->where(array('module_type' => 'project', 'module_id'=> $data['project_id']));
        $this->db->delete('meeting_invitation mg');*/

        $this->db->query('delete mg.* from meeting_invitation mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="project" and m.module_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where('module_type', 'project');
        $this->db->where_in('module_id', $data['project_id']);
        $this->db->delete('meeting');

        /*$this->db->join('module_covenant mc','mc.id_module_covenant=cu.module_covenant_id','left');
        $this->db->where(array('module_type' => 'project', 'module_id'=> $data['project_id']));
        $this->db->delete('covenant_user cu');*/

        $this->db->query('delete cu.* from covenant_user cu left join module_covenant mc on mc.id_module_covenant=cu.module_covenant_id where mc.module_type="project" and mc.module_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('module_covenant mc','mc.id_module_covenant=cta.module_covenant_id','left');
        $this->db->join('covenant_task ct','ct.id_covenant_task=cta.covenant_task_id','left');
        $this->db->where(array('module_type' => 'project', 'module_id'=> $data['project_id']));
        $this->db->delete('covenant_task_action cta');*/

        $this->db->query('delete cta.* from covenant_task_action cta left join covenant_task ct on ct.id_covenant_task=cta.covenant_task_id left join module_covenant mc on mc.id_module_covenant=ct.module_covenant_id where mc.module_type="project" and mc.module_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('module_covenant mc','mc.id_module_covenant=cta.module_covenant_id','left');
        $this->db->where(array('module_type' => 'project', 'module_id'=> $data['project_id']));
        $this->db->delete('covenant_task cta');*/

        $this->db->query('delete cta.* from covenant_task cta left join module_covenant mc on mc.id_module_covenant=cta.module_covenant_id where mc.module_type="project" and mc.module_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where('module_type', 'project');
        $this->db->where_in('module_id', $data['project_id']);
        $this->db->delete('module_covenant');

        /*$this->db->join('project_facility pf','pf.id_project_facility=pswf.project_facility_id','left');
        $this->db->where(array('crm_project_id'=> $data['project_id']));
        $this->db->delete('project_stage_workflow_facility pswf');*/

        $this->db->query('delete pswf.* from project_stage_workflow_facility pswf left join project_facility pf on pf.id_project_facility=pswf.project_facility_id where  pf.crm_project_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('project_facility pf','pf.id_project_facility=pswf.project_facility_id','left');
        $this->db->where(array('crm_project_id'=> $data['project_id']));
        $this->db->delete('project_facility_field_data pswf');*/

        $this->db->query('delete pswf.* from project_facility_field_data pswf left join project_facility pf on pf.id_project_facility=pswf.project_facility_id where  pf.crm_project_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('project_facility pf','pf.id_project_facility=pswf.project_facility_id','left');
        $this->db->where(array('crm_project_id'=> $data['project_id']));
        $this->db->delete('project_facility_collateral pswf');*/

        $this->db->query('delete pswf.* from project_facility_collateral pswf left join project_facility pf on pf.id_project_facility=pswf.project_facility_id where  pf.crm_project_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where_in('crm_project_id', $data['project_id']);
        $this->db->delete('project_facility');

        /*$this->db->join('project_stage_info psi','psi.id_project_stage_info=pswp.project_stage_info_id','left');
        $this->db->join('project_stage_workflow_phase pswp','pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id','left');
        $this->db->where(array('project_id'=> $data['project_id']));
        $this->db->delete('project_stage_workflow psw');*/

        $this->db->query('delete psw.* from project_stage_workflow psw left join project_stage_workflow_phase pswp on pswp.id_project_stage_workflow_phase=psw.project_stage_workflow_phase_id left join project_stage_info psi on psi.id_project_stage_info=pswp.project_stage_info_id where  psi.project_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('project_stage_info psi','psi.id_project_stage_info=pswp.project_stage_info_id','left');
        $this->db->where(array('project_id'=> $data['project_id']));
        $this->db->delete('project_stage_workflow_phase pswp');*/

        $this->db->query('delete pswp.* from project_stage_workflow_phase pswp left join project_stage_info psi on psi.id_project_stage_info=pswp.project_stage_info_id where  psi.project_id in ('.JOIN(",",$data['project_id']).')');

        /*$this->db->join('project_stage_info psi','psi.id_project_stage_info=psv.project_stage_info_id','left');
        $this->db->where(array('project_id'=> $data['project_id']));
        $this->db->delete('project_stage_version psv');*/

        $this->db->query('delete psv.* from project_stage_version psv left join project_stage_info psi on psi.id_project_stage_info=psv.project_stage_info_id where  psi.project_id in ('.JOIN(",",$data['project_id']).')');

        $this->db->where_in('project_id', $data['project_id']);
        $this->db->delete('project_stage_info');

        $this->db->where_in('id_crm_project', $data['project_id']);
        $this->db->delete('crm_project');
    }

    public function deleteCompanyData($data){
        //echo "<pre>"; print_r($data); exit;
        $data['crm_company_id'] = $project_id = array_values(explode(',',$data['crm_company_id']));
        /*$data['project_id'] = array();
        foreach($project_id as $i){
            array_push($data['project_id'],$i);
        }*/
//echo "<pre>"; print_r($data['crm_company_id']); exit;
        $this->db->where('module_type', 'company');
        $this->db->where_in('module_id',$data['crm_company_id']);
        $this->db->delete('activity');

        $this->db->where('crm_module_type', 'company');
        $this->db->where_in('crm_module_id',$data['crm_company_id']);
        $this->db->delete('crm_module_status');

        $this->db->where('module_type', 'company');
        $this->db->where_in('module_id', $data['crm_company_id']);
        $this->db->delete('notification');

        $this->db->query('delete cdv.* from crm_document_version cdv left join crm_document cd on cd.id_crm_document=cdv.crm_document_id WHERE cd.module_type="company" and cd.uploaded_from_id in ('.JOIN(",",$data['crm_company_id']).')');

        $this->db->where('module_type', 'company');
        $this->db->where_in('uploaded_from_id', $data['crm_company_id']);
        $this->db->delete('crm_document');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('crm_company_contact');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('crm_company_relation');

        $this->db->query('delete tm.* from task_member tm left join task t on t.id_task=tm.task_id where t.module_type="company" and t.module_id in ('.JOIN(",",$data['crm_company_id']).')');
        $this->db->where('module_type', 'company');
        $this->db->where_in('module_id', $data['crm_company_id']);
        $this->db->delete('task');

        $this->db->query('delete mg.* from meeting_guest mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="company" and m.module_id in ('.JOIN(",",$data['crm_company_id']).')');
        $this->db->query('delete mg.* from meeting_invitation mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="company" and m.module_id in ('.JOIN(",",$data['crm_company_id']).')');
        $this->db->where('module_type', 'company');
        $this->db->where_in('module_id', $data['crm_company_id']);
        $this->db->delete('meeting');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('project_stage_section_form_last_update');

        $this->db->query('delete ccrt.* from crm_company_ratio_trend ccrt
  left join crm_company_ratio_data ccrd on ccrt.crm_company_ratio_data_id=ccrd.id_crm_company_ratio_data
  LEFT join crm_company_ratio ccr on ccrd.crm_company_ratio_id=ccr.id_crm_company_ratio
  WHERE ccr.crm_company_id in ('.JOIN(",",$data['crm_company_id']).')');
        $this->db->query('delete ccrd.* from crm_company_ratio_data ccrd LEFT join crm_company_ratio ccr on ccrd.crm_company_ratio_id=ccr.id_crm_company_ratio
  WHERE ccr.crm_company_id in ('.JOIN(",",$data['crm_company_id']).')');
        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('crm_company_ratio');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('company_risk_category_item');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('company_assessment_item_step');

        /*$this->db->where_in('company_id', $data['crm_company_id']);
        $this->db->delete('company_other_bank_exposure');*/

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('crm_company_data');

        $this->db->where_in('crm_company_id', $data['crm_company_id']);
        $this->db->delete('project_company');

        $this->db->where_in('id_crm_company', $data['crm_company_id']);
        $this->db->delete('crm_company');

    }

    public function deleteContactData($data){
        $data['crm_contact_id'] = $project_id = array_values(explode(',',$data['crm_contact_id']));
        /*$data['project_id'] = array();
        foreach($project_id as $i){
            array_push($data['project_id'],$i);
        }*/

        $this->db->where('module_type', 'contact');
        $this->db->where_in('module_id',$data['crm_contact_id']);
        $this->db->delete('activity');

        $this->db->where('crm_module_type', 'contact');
        $this->db->where_in('crm_module_id',$data['crm_contact_id']);
        $this->db->delete('crm_module_status');

        $this->db->where('module_type', 'contact');
        $this->db->where_in('module_id', $data['crm_contact_id']);
        $this->db->delete('notification');

        $this->db->query('delete cdv.* from crm_document_version cdv left join crm_document cd on cd.id_crm_document=cdv.crm_document_id WHERE cd.module_type="contact" and cd.uploaded_from_id in ('.JOIN(",",$data['crm_contact_id']).')');

        $this->db->where('module_type', 'contact');
        $this->db->where_in('uploaded_from_id', $data['crm_contact_id']);
        $this->db->delete('crm_document');

        $this->db->where_in('crm_contact_id', $data['crm_contact_id']);
        $this->db->delete('crm_company_contact');

        $this->db->query('delete tm.* from task_member tm left join task t on t.id_task=tm.task_id where t.module_type="contact" and t.module_id in ('.JOIN(",",$data['crm_contact_id']).')');
        $this->db->where('module_type', 'contact');
        $this->db->where_in('module_id', $data['crm_contact_id']);
        $this->db->delete('task');

        $this->db->query('delete mg.* from meeting_guest mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="contact" and m.module_id in ('.JOIN(",",$data['crm_contact_id']).')');
        $this->db->query('delete mg.* from meeting_invitation mg left join  meeting m on m.id_meeting=mg.meeting_id where m.module_type="contact" and m.module_id in ('.JOIN(",",$data['crm_contact_id']).')');
        $this->db->where('module_type', 'contact');
        $this->db->where_in('module_id', $data['crm_contact_id']);
        $this->db->delete('meeting');

        $this->db->where_in('crm_contact_id', $data['crm_contact_id']);
        $this->db->delete('project_stage_section_form_last_update');

        $this->db->where_in('crm_contact_id', $data['crm_contact_id']);
        $this->db->delete('crm_contact_data');

        $this->db->where_in('crm_contact_id', $data['crm_contact_id']);
        $this->db->delete('project_contact');

        $this->db->where_in('id_crm_contact', $data['crm_contact_id']);
        $this->db->delete('crm_contact');

    }
}
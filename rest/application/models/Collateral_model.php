<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Collateral_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function getCollateralType($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type t');
        if(isset($data['company_id']))
            $this->db->where('t.company_id',$data['company_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('t.id_collateral_type',$data['collateral_type_id']);
        if(isset($data['collateral_type_status']))
            $this->db->where('t.collateral_type_status',$data['collateral_type_status']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeFields($data)
    {
        $this->db->select('tf.*');
        $this->db->from('collateral_type ct');
        $this->db->join('collateral_type_field tf','ct.id_collateral_type=tf.collateral_type_id','left');
        if(isset($data['company_id']))
            $this->db->where('ct.company_id',$data['company_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('tf.collateral_type_id',$data['collateral_type_id']);
        $this->db->order_by('tf.field_order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function checkCollateralTypeStage($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage cs');
        $this->db->join('collateral_type_stage cts','cs.id_collateral_stage=cts.collateral_stage_id','left');
        if(isset($data['company_id']))
            $this->db->where('cts.company_id',$data['company_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('cts.collateral_type_id',$data['collateral_type_id']);
        if(isset($data['collateral_stage_id']))
            $this->db->where('cts.collateral_stage_id',$data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeStages($data)
    {
        $this->db->select('cs.*,IFNULL(cts.status,0) as is_selected');
        $this->db->from('collateral_stage cs');
        $this->db->join('collateral_type_stage cts','cs.id_collateral_stage=cts.collateral_stage_id and cts.company_id='.$data['company_id'].' and cts.collateral_type_id='.$data['collateral_type_id'],'left');
        if(isset($data['status']))
            $this->db->where('cts.status',$data['status']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralTypeStages($data)
    {
        $this->db->insert('collateral_type_stage', $data);
        return $this->db->insert_id();
    }

    public function updateCollateralTypeStages($data)
    {
        $this->db->where('id_collateral_type_stage',$data['id_collateral_type_stage']);
        $this->db->update('collateral_type_stage',$data);
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function getCollateral($data)
    {
        $this->db->select('c.*,ct.*');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','c.collateral_type_id=ct.id_collateral_type','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['id_collateral']))
            $this->db->where('c.id_collateral',$data['id_collateral']);

        if(isset($data['collateral_number']))
            $this->db->where('c.collateral_number',$data['collateral_number']);
        if(isset($data['collateral_id_not_equal']))
            $this->db->where('c.id_collateral!',$data['collateral_id_not_equal']);

        $this->db->where('c.collateral_status !=','0');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralList($data)
    {
        $this->db->select('c.*,ct.collateral_type_name,ct.collateral_type_icon,mc.child_name as application_priority,cu.currency_code,mc1.child_name as collateral_status_name,CONCAT(u.first_name, " " ,u.last_name) as user_name,cb.legal_name,ar.approval_name');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','c.collateral_type_id=ct.id_collateral_type','left');
        $this->db->join('master_child mc','c.application_priority_id=mc.id_child','left');
        $this->db->join('currency cu','c.collateral_currency_id=cu.id_currency','left');
        $this->db->join('master_child mc1','c.collateral_status=mc1.id_child','left');
        $this->db->join('company_user cur','cur.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cur.branch_id=cb.id_branch','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cur.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['id_collateral']))
            $this->db->where('c.id_collateral',$data['id_collateral']);

        if(isset($data['collateral_number']))
            $this->db->where('c.collateral_number',$data['collateral_number']);
        if(isset($data['collateral_id_not_equal']))
            $this->db->where('c.id_collateral !=',$data['collateral_id_not_equal']);
        if(isset($data['created_by']))
            $this->db->where('c.created_by',$data['created_by']);
        if(isset($data['project_facility_id']))
            $this->db->where('project_facility_id',$data['project_facility_id']);
        if(isset($data['search_key']))
            $this->db->where('(ct.collateral_type_name like "%'.$data['search_key'].'%" or c.collateral_number like "%'.$data['search_key'].'%" )');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('c.collateral_status !=','0');

        $this->db->order_by('c.id_collateral','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function getCollateralCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','c.collateral_type_id=ct.id_collateral_type','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['id_collateral']))
            $this->db->where('c.id_collateral',$data['id_collateral']);
        if(isset($data['created_by']))
            $this->db->where('c.created_by',$data['created_by']);
        if(isset($data['collateral_number']))
            $this->db->where('c.collateral_number',$data['collateral_number']);
        if(isset($data['collateral_id_not_equal']))
            $this->db->where('c.id_collateral !=',$data['collateral_id_not_equal']);
        if(isset($data['search_key']))
            $this->db->where('(ct.collateral_type_name like "%'.$data['search_key'].'%" or c.collateral_number like "%'.$data['search_key'].'%" )');

        $this->db->where('c.collateral_status !=','0');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateral($data)
    {
        $this->db->insert('collateral', $data);
        return $this->db->insert_id();
    }

    public function updateCollateral($data)
    {
        $this->db->where('id_collateral',$data['id_collateral']);
        $this->db->update('collateral',$data);
        return 1;
    }

    public function getCollateralContact($data)
    {
        $this->db->select('cc.*,mc.child_name as relation_type,concat(c.first_name," ",c.last_name) as contact_name,c.email,c.phone_number');
        $this->db->from('collateral_contact cc');
        $this->db->join('master_child mc','cc.relation_type_id=mc.id_child','left');
        $this->db->join('crm_contact c','cc.crm_contact_id=c.id_crm_contact','left');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralContact($data)
    {
        $this->db->insert('collateral_contact', $data);
        return $this->db->insert_id();
    }

    public function deleteCollateralContact($data)
    {
        $this->db->where('id_collateral_contact',$data['id_collateral_contact']);
        $this->db->delete('collateral_contact');
        return 1;
    }

    public function getValuationDetails($data)
    {
        $this->db->select('*');
        $this->db->from('valuation_details');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_valuation','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addValuationDetails($data)
    {
        $this->db->insert('valuation_details', $data);
        return $this->db->insert_id();
    }

    public function updateValuationDetails($data)
    {
        $this->db->where('id_valuation',$data['id_valuation']);
        $this->db->update('valuation_details',$data);
        return 1;
    }

    public function deleteValuationDetails($data)
    {
        $this->db->where('id_valuation',$data['id_valuation']);
        $this->db->delete('valuation_details');
        return 1;
    }

    public function getInsuranceDetails($data)
    {
        $this->db->select('i.*,c.child_name as insurance_type_name');
        $this->db->from('insurance i');
        $this->db->join('master_child c','i.insurance_type_id=c.id_child','left');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_insurance','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addInsuranceDetails($data)
    {
        $this->db->insert('insurance', $data);
        return $this->db->insert_id();
    }

    public function updateInsuranceDetails($data)
    {
        $this->db->where('id_insurance',$data['id_insurance']);
        $this->db->update('insurance',$data);
        return 1;
    }

    public function deleteInsuranceDetails($data)
    {
        $this->db->where('id_insurance',$data['id_insurance']);
        $this->db->delete('insurance');
        return 1;
    }

    public function getFieldInvestigationDetails($data)
    {
        $this->db->select('*');
        $this->db->from('field_investigation');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_field_investigation','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addFieldInvestigationDetails($data)
    {
        $this->db->insert('field_investigation', $data);
        return $this->db->insert_id();
    }

    public function updateFieldInvestigationDetails($data)
    {
        $this->db->where('id_field_investigation',$data['id_field_investigation']);
        $this->db->update('field_investigation',$data);
        return 1;
    }

    public function deleteFieldInvestigationDetails($data)
    {
        $this->db->where('id_field_investigation',$data['id_field_investigation']);
        $this->db->delete('field_investigation');
        return 1;
    }

    public function getExternalCheckDetails($data)
    {
        $this->db->select('*');
        $this->db->from('external_check');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_external_check','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addExternalCheckDetails($data)
    {
        $this->db->insert('external_check', $data);
        return $this->db->insert_id();
    }

    public function updateExternalCheckDetails($data)
    {
        $this->db->where('id_external_check',$data['id_external_check']);
        $this->db->update('external_check',$data);
        return 1;
    }

    public function deleteExternalCheckDetails($data)
    {
        $this->db->where('id_external_check',$data['id_external_check']);
        $this->db->delete('external_check');
        return 1;
    }

    public function getRiskEvaluationDetails($data)
    {
        $this->db->select('*');
        $this->db->from('risk_evaluation');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_risk_evaluation','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addRiskEvaluationDetails($data)
    {
        $this->db->insert('risk_evaluation', $data);
        return $this->db->insert_id();
    }

    public function updateRiskEvaluationDetails($data)
    {
        $this->db->where('id_risk_evaluation',$data['id_risk_evaluation']);
        $this->db->update('risk_evaluation',$data);
        return 1;
    }

    public function deleteRiskEvaluationDetails($data)
    {
        $this->db->where('id_risk_evaluation',$data['id_risk_evaluation']);
        $this->db->delete('risk_evaluation');
        return 1;
    }

    public function getLegalOpinionDetails($data)
    {
        $this->db->select('*');
        $this->db->from('legal_opinion');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_legal_opinion','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addLegalOpinionDetails($data)
{
    $this->db->insert('legal_opinion', $data);
    return $this->db->insert_id();
}

    public function updateLegalOpinionDetails($data)
    {
        $this->db->where('id_legal_opinion',$data['id_legal_opinion']);
        $this->db->update('legal_opinion',$data);
        return 1;
    }

    public function deleteLegalOpinionDetails($data)
    {
        $this->db->where('id_legal_opinion',$data['id_legal_opinion']);
        $this->db->delete('legal_opinion');
        return 1;
    }

    public function getCollateralContactDetails($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_contact');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_collateral_contact','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addCollateralContactDetails($data)
    {
        $this->db->insert('collateral_contact', $data);
        return $this->db->insert_id();
    }

    public function updateCollateralContactDetails($data)
    {
        $this->db->where('id_collateral_contact',$data['id_collateral_contact']);
        $this->db->update('collateral_contact',$data);
        return 1;
    }

    public function deleteCollateralContactDetails($data)
    {
        $this->db->where('id_collateral_contact',$data['id_collateral_contact']);
        $this->db->delete('collateral_contact');
        return 1;
    }

    public function getProjectCollateral($data)
    {
        $this->db->select('group_concat(p.project_title) as project_title');
        $this->db->from('project_facility_collateral pfc');
        $this->db->join('collateral c','pfc.collateral_id=c.id_collateral','left');
        $this->db->join('project_facility pf','pfc.project_facility_id=pf.id_project_facility','left');
        $this->db->join('crm_project p','pf.crm_project_id=p.id_crm_project','left');
        if(isset($data['project_facility_id']))
            $this->db->where('pfc.project_facility_id',$data['project_facility_id']);
        if(isset($data['collateral_id']))
            $this->db->where('pfc.collateral_id',$data['collateral_id']);
        if(isset($data['project_id']))
            $this->db->where('pf.crm_project_id',$data['project_id']);
        if(isset($data['status']))
            $this->db->where('pfc.status',$data['status']);
        //$this->db->group_by('pfc.id_project_facility_collateral');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getProjectFacilityCollateral($data)
    {
        $this->db->select('pfc.*,c.*,pf.*,cc.currency_code,mc2.child_name as facility_type_name,ct.*');
        $this->db->from('project_facility_collateral pfc');
        $this->db->join('collateral c','pfc.collateral_id=c.id_collateral','left');
        $this->db->join('collateral_type ct','c.collateral_type_id=ct.id_collateral_type','left');
        $this->db->join('project_facility pf','pfc.project_facility_id=pf.id_project_facility','left');
        $this->db->join('currency cc','pf.currency_id=cc.id_currency','left');
        $this->db->join('master_child mc2','pf.facility_type=mc2.id_child','left');
        $this->db->select('mc.child_name as sub_type');
        $this->db->join('master_child mc','pf.facility_sub_type=mc.id_child','left');
        if(isset($data['project_facility_id']))
            $this->db->where('pfc.project_facility_id',$data['project_facility_id']);
        if(isset($data['collateral_id']))
            $this->db->where('pfc.collateral_id',$data['collateral_id']);
        if(isset($data['project_id']))
            $this->db->where('pf.crm_project_id',$data['project_id']);

        if(isset($data['status']))
            $this->db->where('pfc.status',$data['status']);
        $this->db->order_by('pfc.id_project_facility_collateral','desc');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function addProjectFacilityCollateral($data)
    {
        $this->db->insert('project_facility_collateral', $data);
        return $this->db->insert_id();
    }

    public function updateProjectFacilityCollateral($data)
    {
        if(isset($data['id_project_facility_collateral']))
            $this->db->where('id_project_facility_collateral',$data['id_project_facility_collateral']);
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        if(isset($data['project_facility_id']))
            $this->db->where('project_facility_id',$data['project_facility_id']);
        $this->db->update('project_facility_collateral',$data);
        return 1;
    }

    public function getProjectFacilityForCollateral($data)
    {
        $this->db->select('pf.*');
        $this->db->from('project_facility pf');
        $this->db->join('crm_project cp','pf.crm_project_id=cp.id_crm_project','left');
        if(isset($data['collateral_id'])){
            $this->db->where('pf.id_project_facility not in (select project_facility_id from project_facility_collateral where collateral_id='.$data['collateral_id'].') ');
        }
        if(isset($data['search_key']))
            $this->db->where('(pf.project_facility_name like "%'.$data['search_key'].'%" or pf.contract_id like "%'.$data['search_key'].'%")');

        $this->db->where('cp.project_status !=','deleted');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function otherFacilityCollateral($data)
    {
        $this->db->select('c.*,ct.collateral_type_name,ct.collateral_type_icon,mc.child_name as application_priority,cu.currency_code,mc1.child_name as collateral_status_name');
        $this->db->from('project_facility_collateral pfc');
        $this->db->join('collateral c','pfc.collateral_id=c.id_collateral','left');
        $this->db->join('collateral_type ct','c.collateral_type_id=ct.id_collateral_type','left');
        $this->db->join('master_child mc','c.application_priority_id=mc.id_child','left');
        $this->db->join('currency cu','c.collateral_currency_id=cu.id_currency','left');
        $this->db->join('master_child mc1','c.collateral_status=mc1.id_child','left');
        $this->db->where('pfc.project_facility_id in (select project_facility_id from project_facility_collateral where collateral_id='.$data['collateral_id'].')');
        $this->db->where('pfc.collateral_id !=',$data['collateral_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getCollateralTypeFieldData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field ctf');
        if(isset($data['collateral_id']))
            $this->db->join('collateral_type_field_data ctd','ctf.id_collateral_type_field=ctd.collateral_type_field_id and ctd.collateral_id='.$data["collateral_id"].'','left');
        if(isset($data['collateral_type_id']))
            $this->db->where('ctf.collateral_type_id', $data['collateral_type_id']);
        $this->db->order_by('ctf.field_order','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralTypeDetails($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type');
        if(isset($data['collateral_type_id']))
            $this->db->where('id_collateral_type',$data['collateral_type_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralTypeFormData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field_data');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id', $data['collateral_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralTypeFormData($data)
    {
        $this->db->insert_batch('collateral_type_field_data', $data);
        return $this->db->insert_id();
    }

    public function updateCollateralTypeFormData($data)
    {
        $this->db->update_batch('collateral_type_field_data', $data, 'id_collateral_type_field_data');
        return true;
    }

    /*public function getFacilityCollateral($data)
    {
        $this->db->select('*');
        $this->db->from('project_facility_collateral pfc');
        $this->db->join('collateral c','pfc.collateral_id=c.id_collateral','left');
        $this->db->join('project_facility pf','pfc.project_facility_id=pf.id_project_facility','left');
        if(isset($data['project_facility_id']))
            $this->db->where('pfc.project_facility_id',$data['project_facility_id']);
        if(isset($data['collateral_id']))
            $this->db->where('pfc.collateral_id',$data['collateral_id']);
        if(isset($data['project_id']))
            $this->db->where('pf.crm_project_id',$data['project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }*/

    public function getCollateralDocument($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_document cd');
        if(isset($data['company_id']))
            $this->db->where('cd.company_id',$data['company_id']);
        if(isset($data['id_collateral_document']))
            $this->db->where('cd.id_collateral_document',$data['id_collateral_document']);
        if(isset($data['collateral_type_id']))
            $this->db->where('cd.collateral_type_id',$data['collateral_type_id']);
        $this->db->where('cd.status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralDocument($data)
    {
        $this->db->insert('collateral_document', $data);
        return $this->db->insert_id();
    }

    public function updateCollateralDocument($data)
    {
        $this->db->where('id_collateral_document',$data['id_collateral_document']);
        $this->db->update('collateral_document',$data);
        return 1;
    }

    public function getSecurityValuationDetails($data)
    {
        $this->db->select('*');
        $this->db->from('security_valuation_details');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id',$data['collateral_id']);
        $this->db->order_by('id_security_valuation_details','DESC');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        if(isset($data['count']))
            return $query->num_rows();
        else
            return $query->result_array();
    }

    public function addSecurityValuationDetails($data)
    {
        $this->db->insert('security_valuation_details', $data);
        return $this->db->insert_id();
    }

    public function updateSecurityValuationDetails($data)
    {
        $this->db->where('id_security_valuation_details',$data['id_security_valuation_details']);
        $this->db->update('security_valuation_details',$data);
        return 1;
    }

    public function deleteSecurityValuationDetails($data)
    {
        $this->db->where('id_security_valuation_details',$data['id_security_valuation_details']);
        $this->db->delete('security_valuation_details');
        return 1;
    }

    function getCollateralSearch($data){
        $this->db->select('c.id_collateral,c.collateral_number,c.collateral_amount,c.usd_equal_amount,c.collateral_type_id,ct.collateral_type_name');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','ct.id_collateral_type=c.collateral_type_id');
        /*if(isset($data['crm_contact_id']) && !empty($data['crm_contact_id']))
            $this->db->where_not_in('id_crm_contact',$data['crm_contact_id']);*/
        if(isset($data['search_key']))
            $this->db->where(' collateral_number like "%'.$data['search_key'].'%" ');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

}
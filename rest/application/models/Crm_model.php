<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model
{

    public function getContactList($data)
    {
        //$created_by = $this->allowed_created_by;
        //$allowed_company_approval_roles = $this->allowed_company_approval_roles;
        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.*,cct.crm_contact_type_name,cc.company_name as crm_company_name,ccd.designation_name as designation,CONCAT(u.first_name,\' \',u.last_name) as username,cb.legal_name,ar.approval_name,cn.country_name');
        $this->db->from('crm_contact c');
        $this->db->join('crm_contact_type cct','c.crm_contact_type_id=cct.id_crm_contact_type','left');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and is_primary_company=1 and crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','cc.id_crm_company=ccc.crm_company_id','left');
        $this->db->join('country cn','cc.country_id=cn.id_country','left');
        $this->db->join('crm_company_designation ccd','ccd.id_crm_company_designation=ccc.crm_company_designation_id','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        $this->db->where('c.company_id',$data['company_id']);
        //$this->db->where('ccc.is_primary_company','1');
        if(isset($data['created_by']))
            $this->db->where('c.created_by',$data['created_by']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.first_name like "%'.$data['search_key'].'%" or c.last_name like "%'.$data['search_key'].'%" or c.email like "%'.$data['search_key'].'%" or c.phone_number like "%'.$data['search_key'].'%")');
        }
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM project_contact where crm_project_id='.$data['crm_project_id'].' and project_contact_status=1)', NULL, FALSE);
            //$this->db->where('c.crm_contact_type_id !=',1);
        }

        if(isset($data['collateral_id'])){
            $this->db->where('id_crm_contact NOT IN (select crm_contact_id from collateral_contact where collateral_id='.$data['collateral_id'].')');
        }

        /*if(!empty($created_by))
            $this->db->where_in('c.created_by ',$this->allowed_created_by);*/
        if(!isset($data['crm_project_id'])) {
            if ($this->current_user) {
                $this->db->where_in('c.created_by ', $user_list);
            }
        }

        /*if(!empty($allowed_company_approval_roles))
            $this->db->where_in('cu.company_approval_role_id',$this->allowed_company_approval_roles);*/

        $this->db->where('contact_status',1);
        $this->db->group_by('id_crm_contact');
        $this->db->order_by('id_crm_contact','DESC');



        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getContactListCount($data)
    {

        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('count(*) as total');
        $this->db->from('crm_contact c');
        $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.first_name like "%'.$data['search_key'].'%" or c.last_name like "%'.$data['search_key'].'%" or c.email like "%'.$data['search_key'].'%" or c.phone_number like "%'.$data['search_key'].'%")');
        }
        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM project_contact where crm_project_id='.$data['crm_project_id'].' and project_contact_status=1)', NULL, FALSE);
        }
        if(isset($data['collateral_id'])){
            $this->db->where('id_crm_contact NOT IN (select crm_contact_id from collateral_contact where collateral_id='.$data['collateral_id'].')');
        }

        if(!isset($data['crm_project_id'])) {
            if ($this->current_user) {
                $this->db->where_in('c.created_by ', $user_list);
            }
        }

        if(isset($data['created_by']))
            $this->db->where('c.created_by ',$data['created_by']);
        $this->db->where('contact_status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormByFormKeys($data)
    {
        $this->db->select('*');
        $this->db->from('form_field ff');
        $this->db->join('form f','ff.form_id=f.id_form','left');
        $this->db->join('section s','f.section_id=s.id_section','left');
        if(isset($data['crm_module_id']))
            $this->db->where('s.crm_module_id',$data['crm_module_id']);
        $this->db->where_in('ff.field_name',$data);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalContactList($data)
    {
        //print_r($data);
        $this->db->select('c.*,cc.company_name,ccd.designation_name as designation');
        $this->db->from('crm_contact c');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and is_primary_company=1 and crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','cc.id_crm_company=ccc.crm_company_id','left');
        $this->db->join('crm_company_designation ccd','ccd.id_crm_company_designation=ccc.crm_company_designation_id','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.first_name like "%'.$data['search_key'].'%" or c.last_name like "%'.$data['search_key'].'%" or c.email like "%'.$data['search_key'].'%" or c.phone_number like "%'.$data['search_key'].'%")');
        }
        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM project_contact where crm_project_id='.$data['crm_project_id'].' and project_contact_status=1)', NULL, FALSE);
            $this->db->where('c.crm_contact_type_id !=',1);
        }
        $this->db->where('contact_status',1);
        $this->db->group_by('id_crm_contact');
        $this->db->order_by('id_crm_contact','DESC');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getContact($contact_id,$contact_status=0)
    {
        $this->db->select('c.*,group_concat(cc.company_name) as company_name,cct.crm_contact_type_name,cct.key_crm_contact_type,cn.country_name,');
        $this->db->from('crm_contact c');
        $this->db->join('crm_contact_type cct','c.crm_contact_type_id=cct.id_crm_contact_type','left');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and ccc.crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','ccc.crm_company_id=cc.id_crm_company','left');
        $this->db->where('id_crm_contact',$contact_id);
        $this->db->Group_by('id_crm_contact');
        if($contact_status==1)
        {

        }
        else
            $this->db->where('contact_status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getModuleDetails($module_id)
    {
        $this->db->select('*');
        $this->db->from('crm_module m');
        $this->db->join('section s','s.crm_module_id=m.id_crm_module','left');
        $this->db->join('form f','f.section_id=s.id_section','left');
        $this->db->where('m.id_crm_module',$module_id);
        $this->db->order_by('s.section_order','asc');
        $this->db->order_by('f.form_order','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSectionsByModuleId($module_id)
    {
        $this->db->get_where('section',array('crm_module_id' => $module_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContactFormData($contact_id,$form_id)
    {
        $this->db->select('*');
        $this->db->from('crm_contact_data c');
        //$this->db->join('crm_contact cc','cc.id_crm_contact=c.crm_contact_id','left');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_contact_id' => $contact_id,'f.form_id' => $form_id));
        $this->db->order_by('f.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormInformation($form_id)
    {
        $this->db->select('*');
        $this->db->from('form f');
        $this->db->where('f.id_form', $form_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormFieldsByFormId($form_id){
        $this->db->select('*');
        $this->db->from('form f');
        $this->db->join('form_field ff','ff.form_id=f.id_form','left');
        $this->db->where('f.id_form', $form_id);
        $this->db->order_by('ff.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmContact($data)
    {
        $this->db->insert('crm_contact', $data);
        return $this->db->insert_id();
    }

    public function updateCrmContact($data,$crm_contact_id)
    {
        $this->db->where('id_crm_contact', $crm_contact_id);
        $this->db->update('crm_contact', $data);
    }

    public function addCrmContactData($data)
    {
        $this->db->insert_batch('crm_contact_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmContactData($data,$type)
    {
        if($type==1)
        {
            $id_crm_contact_data = $data[0]['id_crm_contact_data'];
            unset($data[0]['id_crm_contact_data']);
            $this->db->where('id_crm_contact_data', $id_crm_contact_data);
            $this->db->update('crm_contact_data', $data[0]);
            //echo $this->db->last_query().'<br>';
        }
        else{
            $this->db->update_batch('crm_contact_data', $data, 'id_crm_contact_data');
            //echo $this->db->last_query(); exit;
        }

        return 1;
    }

    public function getFieldPercentage($crm_id,$type)
    {
        $this->db->select('count(*) as count');
        $this->db->from('form_field ff');
        $this->db->join('form f','f.id_form=ff.form_id','left');
        $this->db->join('section s','s.id_section=f.section_id','left');
        $this->db->join('crm_module m','m.id_crm_module=s.crm_module_id','left');
        $this->db->where('m.crm_module_name', $type);
        $query = $this->db->get();
        $total_field_count = $query->result_array();

        $table = 'crm_'.$type.'_data';
        $id = 'crm_'.$type.'_id';

        $this->db->select('count(*) as count');
        $this->db->from($table);
        $this->db->where(array($id => $crm_id,'form_field_value!=' => ''));
        $query = $this->db->get();
        $total_data_count = $query->result_array();
        return $total_field_count[0]['count'].','.$total_data_count[0]['count'];
    }

    public function getFormPercentage($form_id,$type,$crm_id)
    {
        $this->db->select('count(*) as count');
        $this->db->from('form_field ff');
        $this->db->join('form f','f.id_form=ff.form_id','left');
        $this->db->where('f.id_form', $form_id);
        $query = $this->db->get();
        $total_field_count = $query->result_array();

        $table = 'crm_'.$type.'_data';
        $id = 'crm_'.$type.'_id';
        $this->db->select('count(*) as count');
        $this->db->from($table.' d');
        $this->db->join('form_field ff','d.form_field_id=ff.id_form_field','left');
        $this->db->where(array('ff.form_id' => $form_id,'d.form_field_value !=' => '', $id => $crm_id));
        $query = $this->db->get();
        $total_data_count = $query->result_array();
        return $total_field_count[0]['count'].','.$total_data_count[0]['count'];
    }

    public function getCompany($crm_company_id)
    {
        $this->db->select('c.*,s.sector_name as sector,s1.sector_name as sub_sector,group_concat(cc.first_name) as contact_name');
        $this->db->from('crm_company c');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        $this->db->join('sector s','c.sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.sub_sector_id=s1.id_sector','left');
        $this->db->join('crm_company_contact ccc','c.id_crm_company=ccc.crm_company_id and crm_company_contact_status=1','left');
        $this->db->join('crm_contact cc','ccc.crm_contact_id=cc.id_crm_contact','left');
        $this->db->where('id_crm_company',$crm_company_id);
        $this->db->group_by('id_crm_company');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyList($data)
    {
        //$created_by = $this->allowed_created_by;
        //$allowed_company_approval_roles = $this->allowed_company_approval_roles;

        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('`c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, s.sector_name as sector,s1.sector_name as sub_sector,cp.amount,cp.id_crm_project,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as date,cn.country_name,cb.legal_name,ar.approval_name');
        $this->db->from('`crm_company` `c`');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        $this->db->join('sector s','c.sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.sub_sector_id=s1.id_sector','left');
        $this->db->join('`project_company` `pc`','`c`.`id_crm_company`=`pc`.`crm_company_id` and project_company_status=1','left');
        $this->db->join('`crm_project` `cp`','`pc`.`crm_project_id`=`cp`.`id_crm_project`','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('`user` `u`','`u`.`id_user`=`c`.`created_by`','left');

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        }
        if(isset($data['crm_project_id'])){
            //$this->db->where('id_crm_company IN (SELECT crm_company_id FROM crm_company_contact ccc left join project_contact pc on ccc.crm_contact_id=pc.crm_contact_id where pc.crm_project_id='.$data['crm_project_id'].' and crm_company_contact_status=1 and pc.project_contact_status)', NULL, FALSE);
            $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1)', NULL, FALSE);
        }

        if(!isset($data['crm_project_id']) && !isset($data['created_by'])){
            if($this->current_user)
                $this->db->where_in('c.created_by ',$user_list);
        }

        if(isset($data['created_by']) && $data['created_by']!='' && $data['created_by']!=0)
            $this->db->where('c.created_by',$data['created_by']);

        $this->db->where('company_status',1);

        $this->db->order_by('id_crm_company','DESC');
        $this->db->group_by('id_crm_company');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyListCount($data)
    {
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('count(*) as total');
        $this->db->from('crm_company c');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        }
        if(isset($data['crm_project_id'])){
            //$this->db->where('id_crm_company IN (SELECT crm_company_id FROM crm_company_contact ccc left join project_contact pc on ccc.crm_contact_id=pc.crm_contact_id where pc.crm_project_id='.$data['crm_project_id'].' and crm_company_contact_status=1 and pc.project_contact_status)', NULL, FALSE);
            $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1)', NULL, FALSE);
        }

        if(!isset($data['crm_project_id']) && !isset($data['created_by'])){
            if($this->current_user)
                $this->db->where_in('c.created_by ',$user_list);
        }

        if(isset($data['created_by']) && $data['created_by']!='' && $data['created_by']!=0)
            $this->db->where('c.created_by',$data['created_by']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalCompanyList($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company c');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        }

        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_company IN (SELECT crm_company_id FROM crm_company_contact ccc left join project_contact pc on ccc.crm_contact_id=pc.crm_contact_id where pc.crm_project_id='.$data['crm_project_id'].' and crm_company_contact_status=1 and pc.project_contact_status)', NULL, FALSE);
            $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1)', NULL, FALSE);
        }

        $this->db->order_by('id_crm_company','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyTabsCount($data)
    {

        if($this->current_user)
        {
            /*$user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);*/
            $user_list = array();
        }
        else{
            $user_list = array();
        }
        $sample = '(';
        for($s=0;$s<count($user_list);$s++){
            if($s>0){ $sample.=','; }
            $sample.='"'.$user_list[$s].'"';
        }
        $sample.= ')';

        if($data['status']=='Pending'){
            /*$sql = 'select count(*) as records ,sum(amount) as amount from (
                select cc.*,0 as amount from crm_company cc where id_crm_company not in (select crm_company_id from project_company) and cc.company_id='.$data['company_id'].'';

                if(!isset($data['crm_project_id'])){
                    if($this->current_user){
                        //$this->db->where_in('c.created_by ',$user_list);
                        //$sql.=' AND `cc`.`created_by` IN '.$sample.' ';
                    }
                }
            $sql.=' GROUP BY cc.id_crm_company ';

            $sql.='union
                select cc.*,cp.amount from crm_company cc left join project_company pc on cc.id_crm_company=pc.crm_company_id left join crm_project cp on pc.crm_project_id=cp.id_crm_project
	            where cp.project_status="'.$data['status'].'" and cc.company_id='.$data['company_id'];

                if(!isset($data['crm_project_id'])){
                    if($this->current_user){
                        //$this->db->where_in(\'c.created_by \',$user_list);
                        //$sql.=' AND `cc`.`created_by` IN '.$sample;
                    }
                }
            $sql.=' GROUP BY cc.id_crm_company ';
	        $sql.=' ) as tmp';*/

            $this->db->select('cc.*,0 as amount');
            $this->db->from('crm_company cc');
            $this->db->where('id_crm_company not in (select crm_company_id from project_company)');
            $this->db->where('cc.company_id',$data['company_id']);
            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                }
            }
            $this->db->group_by('cc.id_crm_company');
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('cc.*,cp.amount');
            $this->db->from('crm_company cc');
            $this->db->join('project_company pc','cc.id_crm_company=pc.crm_company_id','left');
            $this->db->join('crm_project cp','pc.crm_project_id=cp.id_crm_project','left');
            $this->db->where('cp.project_status',$data['status']);
            $this->db->where('cc.company_id',$data['company_id']);
            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                }
            }
            $this->db->group_by('cc.id_crm_company');
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('count(*) as records ,sum(amount) as amount');
            $this->db->from("($subQuery1 UNION $subQuery2)tmp");
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            //echo $sql; exit;
            //$query = $this->db->query($sql);
        }
        else{
            $this->db->select('count(*) as records,IFNULL(sum(amount),0) as amount');
            $this->db->from('crm_company cc');
            $this->db->join('project_company pc','cc.id_crm_company=pc.crm_company_id','left');
            $this->db->join('crm_project cp','pc.crm_project_id=cp.id_crm_project','left');
            if(isset($data['status']))
                $this->db->where('cp.project_status',$data['status']);
            if(isset($data['company_id']))
                $this->db->where('cc.company_id',$data['company_id']);

            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('cc.created_by ',$user_list);
                }
            }

            $query = $this->db->get();
            //echo $this->db->last_query().'--------------';
        }

        return $query->result_array();
    }

    public function getPipelineCompanies($data)
    {
        if($this->current_user)
        {
            /*$user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);*/
            $user_list = array();
        }
        else{
            $user_list = array();
        }
        $sample = '(';
        for($s=0;$s<count($user_list);$s++){
            if($s>0){ $sample.=','; }
            $sample.='"'.$user_list[$s].'"';
        }
        $sample.= ')';
        $user_list = $sample;

        /*$sql = 'select * from (SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, s.sector_name as sector,s1.sector_name as sub_sector,cp.amount,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as date
                FROM `crm_company` `c`
                LEFT JOIN  sector s ON c.sector_id=s.id_sector
                LEFT JOIN  sector s1 ON c.sub_sector_id=s.id_sector
                LEFT JOIN `project_company` `pc` ON `c`.`id_crm_company`=`pc`.`crm_company_id` and project_company_status=1
                LEFT JOIN `crm_project` `cp` ON `pc`.`crm_project_id`=`cp`.`id_crm_project`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                WHERE `c`.`company_id` = '.$data['company_id'].'
                AND `cp`.`project_status` = "'.$data['status'].'"';

                if(isset($data['search_key']) && $data['search_key']!=''){
                    $sql.=' AND (c.company_name like "%'.$data['search_key'].'%") ';
                }

                if(isset($data['sector_id']) && $data['sector_id']!=''){
                    $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
                }

                if(!isset($data['crm_project_id'])){
                    if($this->current_user){
                        //$this->db->where_in('c.created_by ',$user_list);
                        //$sql.=' AND `c`.`created_by` IN '.$user_list.' ';
                    }
                }

        if(isset($data['status']) && $data['status']=='Pending')
        {
            $sql.='UNION
                SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as username, s.sector_name as sector,s1.sector_name as sub_sector,0 as amount,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as date
                FROM `crm_company` `c`
                LEFT JOIN  sector s ON c.sector_id=s.id_sector
                LEFT JOIN  sector s1 ON c.sub_sector_id=s.id_sector
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                LEFT JOIN `project_company` `pc` ON `pc`.`crm_company_id`=`c`.`id_crm_company`
                WHERE `c`.`company_id` ='.$data['company_id'].'
                AND pc.id_project_company is NULL ';

            if(isset($data['search_key']) && $data['search_key']!=''){
                $sql.=' AND (company_name like "%'.$data['search_key'].'%") ';
            }

            if(isset($data['sector_id']) && $data['sector_id']!=''){
                $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
            }

            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                    //$sql.=' AND `c`.`created_by` IN '.$user_list.' ';
                }
            }


        }

                $sql.=')tmp ';

        if(isset($data['offset']) && isset($data['limit']))
            $sql.=' limit '.$data['offset'].','.$data['limit'];*/


        $this->db->select('`c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, s.sector_name as sector,s1.sector_name as sub_sector,cp.amount,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as date');
        $this->db->from('`crm_company` `c`');
        $this->db->join('sector s','c.sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.sub_sector_id=s1.id_sector','left');
        $this->db->join('`project_company` `pc`','`c`.`id_crm_company`=`pc`.`crm_company_id` and project_company_status=1','left');
        $this->db->join('`crm_project` `cp`','`pc`.`crm_project_id`=`cp`.`id_crm_project`','left');
        $this->db->join('`user` `u`','`u`.`id_user`=`c`.`created_by`','left');
        $this->db->where('`c`.`company_id`',$data['company_id']);
        $this->db->where('`cp`.`project_status`',$data['status']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.company_name like "%'.$data['search_key'].'%")');
        }
        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $this->db->where('c.sector_id',$data['sector_id']);
        }
        if(!isset($data['crm_project_id'])){
            if($this->current_user){
                //$this->db->where_in('c.created_by ',$user_list);
            }
        }
        $this->db->where('company_status',1);
        $query = $this->db->get();
        $subQuery1 = $this->db->last_query();
        $query->result_array();

        if(isset($data['status']) && $data['status']=='new'){
            $this->db->select('`c`.*, CONCAT(u.first_name, " ", u.last_name) as username, s.sector_name as sector,s1.sector_name as sub_sector,0 as amount,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as date');
            $this->db->from('`crm_company` `c`');
            $this->db->join('sector s','c.sector_id=s.id_sector','left');
            $this->db->join('sector s1','c.sub_sector_id=s.id_sector','left');
            $this->db->join('`user` `u`','`u`.`id_user`=`c`.`created_by`','left');
            $this->db->join('`project_company` `pc`','`c`.`id_crm_company`=`pc`.`crm_company_id`','left');
            $this->db->where('`c`.`company_id`',$data['company_id']);
            $this->db->where('pc.id_project_company is NULL');
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(company_name like "%'.$data['search_key'].'%")');
            }
            if(isset($data['sector_id']) && $data['sector_id']!=''){
                $this->db->where('c.sector_id',$data['sector_id']);
            }
            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                }
            }
            $this->db->where('company_status',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('*');
            $this->db->from("($subQuery1 UNION $subQuery2)tmp");
            if(isset($data['offset']) && isset($data['limit']))
                $this->db->limit($data['limit'],$data['offset']);
            $this->db->order_by('id_crm_company','desc');

            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
        }
        else
        {
            $this->db->select('*');
            $this->db->from("($subQuery1)tmp");
            if(isset($data['offset']) && isset($data['limit']))
                $this->db->limit($data['limit'],$data['offset']);
            $query = $this->db->get();
        }

        //$query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getPipelineCompaniesCount($data)
    {
        if($this->current_user)
        {
            /*$user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);*/
            $user_list = array();
        }
        else{
            $user_list = array();
        }
        $sample = '(';
        for($s=0;$s<count($user_list);$s++){
            if($s>0){ $sample.=','; }
            $sample.='"'.$user_list[$s].'"';
        }
        $sample.= ')';
        $user_list = $sample;

        /*$sql = 'select count(*) as total from (SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`
                FROM `crm_company` `c`
                LEFT JOIN `project_company` `pc` ON `c`.`id_crm_company`=`pc`.`crm_company_id` and pc.project_company_status=1
                LEFT JOIN `crm_project` `cp` ON `pc`.`crm_project_id`=`cp`.`id_crm_project`
                LEFT JOIN `company_user` `cu` ON `cu`.`user_id`=`c`.`created_by`
                LEFT JOIN `company_branch` `cb` ON `cu`.`branch_id`=`cb`.`id_branch`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                WHERE `c`.`company_id` = '.$data['company_id'].'
                AND `cp`.`project_status` = "'.$data['status'].'"';

                if(isset($data['search_key']) && $data['search_key']!=''){
                    $sql.=' AND (company_name like "%'.$data['search_key'].'%") ';
                }

                if(isset($data['sector_id']) && $data['sector_id']!=''){
                    $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
                }

                if(!isset($data['crm_project_id'])){
                    if($this->current_user){
                        //$this->db->where_in('c.created_by ',$user_list);
                        //$sql.=' AND `c`.`created_by` IN '.$user_list.' ';
                    }
                }
        if(isset($data['status']) && $data['status']=='Pending'){
            $sql.=' UNION
                SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`
                FROM `crm_company` `c`
                LEFT JOIN `company_user` `cu` ON `cu`.`user_id`=`c`.`created_by`
                LEFT JOIN `company_branch` `cb` ON `cu`.`branch_id`=`cb`.`id_branch`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                LEFT JOIN `project_company` `pc` ON `pc`.`crm_company_id`=`c`.`id_crm_company`
                WHERE `c`.`company_id` ='.$data['company_id'].'
                AND pc.id_project_company is NULL ';

            if(isset($data['search_key']) && $data['search_key']!=''){
                $sql.=' AND (company_name like "%'.$data['search_key'].'%") ';
            }

            if(isset($data['sector_id']) && $data['sector_id']!=''){
                $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
            }

            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                    //$sql.=' AND `c`.`created_by` IN '.$user_list.' ';
                }
            }
        }


                $sql.=')tmp ';*/


        $this->db->select('`c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`');
        $this->db->from('`crm_company` `c`');
        $this->db->join('`project_company` `pc`','`c`.`id_crm_company`=`pc`.`crm_company_id` and pc.project_company_status=1','left');
        $this->db->join('`crm_project` `cp`','`pc`.`crm_project_id`=`cp`.`id_crm_project`','left');
        $this->db->join('`company_user` `cu`','`cu`.`user_id`=`c`.`created_by`','left');
        $this->db->join('`company_branch` `cb`','`cu`.`branch_id`=`cb`.`id_branch`','left');
        $this->db->join('`user` `u`','`u`.`id_user`=`c`.`created_by`','left');
        $this->db->where('`c`.`company_id`',$data['company_id']);
        $this->db->where('`cp`.`project_status`',$data['status']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(company_name like "%'.$data['search_key'].'%")');
        }
        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $this->db->where('c.sector_id',$data['sector_id']);
        }
        if(!isset($data['crm_project_id'])){
            if($this->current_user){
                //$this->db->where_in('c.created_by ',$user_list);
            }
        }
        $this->db->where('company_status',1);
        $query = $this->db->get();
        $subQuery1 = $this->db->last_query();
        $query->result_array();

        if(isset($data['status']) && $data['status']=='Pending'){
            $this->db->select('`c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`');
            $this->db->from('`crm_company` `c`');
            $this->db->join('`company_user` `cu`','`cu`.`user_id`=`c`.`created_by`','left');
            $this->db->join('`company_branch` `cb`','`cu`.`branch_id`=`cb`.`id_branch`','left');
            $this->db->join('`user` `u`','`u`.`id_user`=`c`.`created_by`','left');
            $this->db->join('`project_company` `pc`','`pc`.`crm_company_id`=`c`.`id_crm_company`','left');
            $this->db->where('`c`.`company_id`',$data['company_id']);
            $this->db->where('pc.id_project_company is NULL');
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(company_name like "%'.$data['search_key'].'%")');
            }
            if(isset($data['sector_id']) && $data['sector_id']!=''){
                $this->db->where('c.sector_id',$data['sector_id']);
            }
            if(!isset($data['crm_project_id'])){
                if($this->current_user){
                    //$this->db->where_in('c.created_by ',$user_list);
                }
            }
            $this->db->where('company_status',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('count(*) as total');
            $this->db->from("($subQuery1 UNION $subQuery2)tmp");
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
        }
        else
        {
            $this->db->select('count(*) as total');
            $this->db->from("($subQuery1)tmp");
            $query = $this->db->get();
        }

        //$query = $this->db->query($sql);

        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getProjectCompanies($data)
    {
        if($this->current_user)
        {
            /*$user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);*/
            $user_list = array();
        }

        $this->db->select('c.*,CONCAT(u.first_name,\' \',u.last_name) as username,cb.legal_name');
        $this->db->from('crm_company c');
        $this->db->join('project_company pc','c.id_crm_company=pc.crm_company_id and pc.project_company_status=1','left');
        $this->db->join('crm_project cp','pc.crm_project_id=cp.id_crm_project','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        $this->db->where('c.company_id',$data['company_id']);
        $this->db->where('cp.project_status',$data['status']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.company_name like "%'.$data['search_key'].'%"');
        }

        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $this->db->where('c.sector_id',$data['sector_id']);
        }

        if(!isset($data['crm_project_id'])){
            if($this->current_user) {
                //$this->db->where_in('c.created_by ',$user_list);
            }
        }
        $this->db->where('company_status',1);
        $this->db->order_by('id_crm_company','DESC');

        if(isset($data['offset']) && isset($data['limit']) )
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectCompaniesCount($data)
    {
        if($this->current_user)
        {
            /*$user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);*/
            $user_list = array();
        }

        $this->db->select('count(*) as total');
        $this->db->from('crm_company c');
        $this->db->join('project_company pc','c.id_crm_company=pc.crm_company_id and pc.project_company_status=1','left');
        $this->db->join('crm_project cp','pc.crm_project_id=cp.id_crm_project','left');
        $this->db->where('c.company_id',$data['company_id']);
        $this->db->where('cp.project_status',$data['status']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.company_name like "%'.$data['search_key'].'%"');
        }

        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $this->db->where('c.sector_id',$data['sector_id']);
        }

        if(!isset($data['crm_project_id'])){
            if($this->current_user) {
                //$this->db->where_in('c.created_by ',$user_list);
            }
        }
        $this->db->where('company_status',1);
        $this->db->order_by('id_crm_company','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyFormData($crm_company_id,$form_id)
    {
        $this->db->select('c.*,f.*');
        //$this->db->from('crm_company cc');
        $this->db->from('crm_company_data c');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_company_id' => $crm_company_id,'f.form_id' => $form_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmCompany($data)
    {
        $this->db->insert('crm_company', $data);
        return $this->db->insert_id();
    }

    public function addCrmCompanyData($data)
    {
        $this->db->insert_batch('crm_company_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmCompanyData($data,$type)
    {
        //echo "<pre>"; print_r($data); exit;
        if($type==1)
        {
            $id_crm_company_data = $data[0]['id_crm_company_data'];
            unset($data[0]['id_crm_company_data']);
            $this->db->where('id_crm_company_data', $id_crm_company_data);
            $this->db->update('crm_company_data', $data[0]);
        }
        else{
            $this->db->update_batch('crm_company_data', $data, 'id_crm_company_data');
        }
        return 1;
    }

    public function updateCrmCompanyDataByCrmCompanyDataId($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->where('id_crm_company_data', $data['id_crm_company_data']);
        unset($data['id_crm_company_data']);
        $this->db->update('crm_company_data', $data);
        return 1;
    }

    public function updateCrmCompany($data,$crm_company_id)
    {
        $this->db->where('id_crm_company', $crm_company_id);
        $this->db->update('crm_company', $data);
    }

    public function addQuickContact($data)
    {
        $this->db->insert('crm_contact', $data);
        return $this->db->insert_id();
    }

    public function addQuickCompany($data)
    {
        $this->db->insert('crm_company', $data);
        return $this->db->insert_id();
    }

    public function addQuickProject($data)
    {
        $this->db->insert('crm_project', $data);
        return $this->db->insert_id();
    }

    public function addDocument($data)
    {
        $this->db->insert('crm_document', $data);
        return $this->db->insert_id();
    }

    public function updateCrmDocument($data)
    {
        if(isset($data['id_crm_document']))
            $this->db->where('id_crm_document',$data['id_crm_document']);
        $this->db->update('crm_document', $data);
    }

    public function addDocumentVersion($data)
    {
        $this->db->insert('crm_document_version', $data);
        return $this->db->insert_id();
    }

    public function getModuleByDocumentType($document_type)
    {
        $this->db->select('*');
        $this->db->from('crm_document cd');
        $this->db->join('crm_document_type cdt','cd.crm_document_type_id=cdt.id_crm_document_type','left');
        $this->db->where('cd.crm_document_type_id',$document_type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentTypeByName($document_type_name)
    {
        $query = $this->db->get_where('crm_document_type',array('document_type_name' => $document_type_name));
        /*$this->db->select('*');
        $this->db->from('crm_document_type');
        $this->db->where('crm_document_type',array('document_type_name' => $document_type_name));
        $query = $this->db->get();*/
        return $query->row();
    }

    public function getDocument($data){
        $this->db->select('c.*,cd.document_type_name,DATE(c.created_date_time) as date,CONCAT(u.first_name,\' \',u.last_name) as username');
        $this->db->from('crm_document c');
        $this->db->join('crm_document_type cd','cd.id_crm_document_type=c.crm_document_type_id','left');
        $this->db->join('user u','c.uploaded_by=u.id_user','left');
        if(isset($data['module_id']))
            $this->db->where('cd.crm_module_id',$data['module_id']);
        if(isset($data['group_id']))
            $this->db->where('c.uploaded_from_id',$data['group_id']);
        if(isset($data['type']))
        {
            if($data['type']=='document'){
                $this->db->where('c.id_crm_document',$data['crm_document_id']);
            }
            else if($data['type']=='document_type'){
                $this->db->where('c.crm_document_type_id',$data['crm_document_type_id']);
            }
        }
        if(isset($data['module_type']))
            $this->db->where('c.module_type',$data['module_type']);
        if(isset($data['group_id']))
            $this->db->where('c.uploaded_from_id',$data['group_id']);
        if(isset($data['uploaded_from_id']))
            $this->db->where('c.uploaded_from_id',$data['uploaded_from_id']);
        if(isset($data['form_key']))
            $this->db->where('c.form_key',$data['form_key']);
        if(isset($data['field_key']))
            $this->db->where('c.field_key',$data['field_key']);
        if(isset($data['id_crm_document']))
            $this->db->where('c.id_crm_document',$data['id_crm_document']);

        $this->db->order_by('c.id_crm_document','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getDocumentType($data)
    {
        $this->db->select('*');
        $this->db->from('crm_document_type');
        if(isset($data['module_id']))
            $this->db->where('crm_module_id', $data['module_id']);
        if(isset($data['form_id']))
            $this->db->where('form_id', $data['form_id']);
        if(isset($data['form_key']))
            $this->db->where('form_key', $data['form_key']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectTabsCount($data)
    {
        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('count(*) as records,IFNULL(sum(amount),0) as amount from (select cc.id_crm_project, sum(pf.facility_usd_amount) as amount');
        $this->db->from('crm_project cc');
        $this->db->join('project_facility pf','cc.id_crm_project=pf.crm_project_id','left');
        if(isset($data['status']) && $data['status']=='ITS Accepted') {
            $this->db->where('(cc.project_status = "'.$data['status'].'" or cc.project_status = "new" or cc.project_status = "pending")');
        }
        else if(isset($data['status']))
            $this->db->where('cc.project_status',$data['status']);
        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if($this->current_user) {
            if(is_array($user_list)){ $user_list = implode(',',$user_list); }
            //if(is_array($this->current_user)){ $this->current_user = implode(',',$this->current_user); }
            //$this->db->where('(cc.created_by in ('.$user_list.') or cc.assigned_to in ('.$this->current_user.') or ppa.assigned_to in ('.$this->current_user.'))');
            $this->db->where('(cc.created_by in ('.$user_list.') or cc.assigned_to in ('.$this->current_user.'))');
        }
        /*if ($this->current_user) {
            $this->db->where_in('cc.created_by ', $user_list);
        }*/
        $this->db->group_by('cc.id_crm_project) as temp');

        $query = $this->db->get();

        //echo $this->db->last_query(); exit;
        return $query->result_array();

    }

    public function getPipelineProjects($data)
    {//echo "<pre>"; print_r($data); exit;
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            //echo "<pre>"; print_r($user_list); exit;
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
            //$user_list = array();
        }

        $this->db->select('c.*,cc.company_name,s.sector_name as sector,s1.sector_name as sub_sector,DATE_FORMAT(c.created_date_time,"%d/%m/%Y") as date,cur.currency_code,mc.child_name as focus_area_name');
        $this->db->from('crm_project c');
        $this->db->join('master_child mc','c.focus_area=mc.id_child','left');
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        $this->db->join('project_company pc','pc.crm_project_id=c.id_crm_project and pc.project_company_status=1','left');
        $this->db->join('crm_company cc','pc.crm_company_id=cc.id_crm_company','left');
        $this->db->join('currency cur','c.currency_id=cur.id_currency','left');
        $this->db->join('project_stage_info psi','psi.project_id=c.id_crm_project','left');
        $this->db->join('project_stage_workflow_phase pswp','psi.id_project_stage_info=pswp.project_stage_info_id','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }

        if($this->current_user) {
            if(is_array($user_list)){ $user_list = implode(',',$user_list); }
            if(is_array($this->current_user)){ $this->current_user = implode(',',$this->current_user); }
            $this->db->where('(c.created_by in ('.$user_list.') or c.assigned_to in ('.$this->current_user.') or pswp.assigned_to in ('.$this->current_user.'))');
        }

        if(isset($data['sector_id']))
            $this->db->where('c.project_main_sector_id',$data['sector_id']);

        if(isset($data['status']) && ($data['status']=='new' || $data['status']=='pipeline' || $data['status']=='ITS Accepted'))
            $this->db->where('(c.project_status = "new" or c.project_status = "pipeline"  or c.project_status = "ITS Accepted" )');
        else if(isset($data['status']))
            $this->db->where('c.project_status',$data['status']);

        if(isset($data['offset']) && isset($data['limit']))
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('project_status !=','deleted');
        $this->db->order_by('id_crm_project','DESC');
        $this->db->group_by('id_crm_project');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        //echo "<pre>"; print_r($query->result_array()); exit;
        return $query->result_array();
    }

    public function getPipelineProjectsCount($data)
    {
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
            //$user_list = array();
        }

        $this->db->select('count(*) as total from (select c.* ');
        $this->db->from('crm_project c');
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('project_company pc','pc.crm_project_id=c.id_crm_project and pc.project_company_status=1','left');
        $this->db->join('crm_company cc','pc.crm_company_id=cc.id_crm_company','left');
        $this->db->join('project_stage_info psi','psi.project_id=c.id_crm_project','left');
        $this->db->join('project_stage_workflow_phase pswp','psi.id_project_stage_info=pswp.project_stage_info_id','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }

        if($this->current_user) {

            if(is_array($user_list)){ $user_list = implode(',',$user_list); }
            if(is_array($this->current_user)){ $this->current_user = implode(',',$this->current_user); }
            $this->db->where('(c.created_by in ('.$user_list.') or c.assigned_to in ('.$this->current_user.') or pswp.assigned_to in ('.$this->current_user.'))');
        }

        if(isset($data['status']) && ($data['status']=='new' || $data['status']=='pipeline' || $data['status']=='ITS Accepted'))
            $this->db->where('(c.project_status = "new" or c.project_status = "ITS Accepted" )');
        else if(isset($data['status']))
            $this->db->where('c.project_status',$data['status']);

        if(isset($data['sector_id']))
            $this->db->where('c.project_main_sector_id',$data['sector_id']);

        $this->db->where('project_status !=','deleted');
        $this->db->group_by('id_crm_project) as temp');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    /*public function getPipelineCompaniesCount($data)
    {
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }
        $sample = '(';
        for($s=0;$s<count($user_list);$s++){
            if($s>0){ $sample.=','; }
            $sample.='"'.$user_list[$s].'"';
        }
        $sample.= ')';
        $user_list = $sample;


        $sql = 'select count(*) as total from (SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`
                FROM `crm_company` `c`
                LEFT JOIN `project_company` `pc` ON `c`.`id_crm_company`=`pc`.`crm_company_id`
                LEFT JOIN `crm_project` `cp` ON `pc`.`crm_project_id`=`cp`.`id_crm_project`
                LEFT JOIN `company_user` `cu` ON `cu`.`user_id`=`c`.`created_by`
                LEFT JOIN `company_branch` `cb` ON `cu`.`branch_id`=`cb`.`id_branch`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                WHERE `c`.`company_id` = '.$data['company_id'].'
                AND `cp`.`project_status` = "'.$data['status'].'"';

        if(isset($data['search_key']) && $data['search_key']!=''){
            $sql.=' AND (company_name like "%'.$data['search_key'].'%") ';
        }

        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
        }

        if(!isset($data['crm_project_id'])){
            if($this->current_user){
                //$this->db->where_in('c.created_by ',$user_list);
                $sql.=' AND `c`.`created_by` IN '.$user_list.' ';
            }
        }

        $sql.='UNION
                SELECT `c`.*, CONCAT(u.first_name, " ", u.last_name) as `username`, `cb`.`legal_name`
                FROM `crm_company` `c`
                LEFT JOIN `company_user` `cu` ON `cu`.`user_id`=`c`.`created_by`
                LEFT JOIN `company_branch` `cb` ON `cu`.`branch_id`=`cb`.`id_branch`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`c`.`created_by`
                WHERE `c`.`company_id` ='.$data['company_id'].'
                AND `c`.`id_crm_company` NOT IN (select crm_company_id from project_company)';

        if(isset($data['search_key']) && $data['search_key']!=''){
            $sql.=' AND (company_name like "%'.$data['search_key'].'%") ';
        }

        if(isset($data['sector_id']) && $data['sector_id']!=''){
            $sql.=' AND  c.sector_id="'.$data['sector_id'].'"';
        }

        if(!isset($data['crm_project_id'])){
            if($this->current_user){
                //$this->db->where_in('c.created_by ',$user_list);
                $sql.=' AND `c`.`created_by` IN '.$user_list.' ';
            }
        }
        $sql.=')tmp ';

        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }*/

    public function getProject($crm_project_id)
    {
        $this->db->select('c.*,DATE_FORMAT(c.updated_date_time,"%b %d %Y") as updated_date,p.product_name,s.sector_name as sector,s1.sector_name as sub_sector,cr.currency_code,ccm.company_description as background_of_customer');
        $this->db->from('crm_project c');
        $this->db->join('company cm','c.company_id=cm.id_company','left');
        $this->db->join('currency cr','cm.currency_id=cr.id_currency','left');
        $this->db->join('project_company pc','c.id_crm_project = pc.crm_project_id','left');
        $this->db->join('crm_company ccm','pc.crm_company_id = ccm.id_crm_company','left');
        $this->db->join('product p','c.product_id=p.id_product','left');
        $this->db->join('sector s','c.project_main_sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        $this->db->where('id_crm_project',$crm_project_id);
        $this->db->where('project_status !=','deleted');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectList($data)
    {

        $user_list = '';
        if($this->current_user && $this->current_user!=0 && !isset($data['login_user']))
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.*,cc.id_crm_company,cc.company_name,s.sector_name as sector,s1.sector_name as sub_sector,DATE_FORMAT(c.created_date_time,"%d/%m/%Y") as date,cur.currency_code,mc.child_name as focus_area_name');
        $this->db->from('crm_project c');
        $this->db->join('master_child mc','c.focus_area=mc.id_child','left');
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        $this->db->join('project_company pc','pc.crm_project_id=c.id_crm_project and pc.company_type_id=1 and pc.project_company_status=1','left');
        $this->db->join('crm_company cc','pc.crm_company_id=cc.id_crm_company','left');
        $this->db->join('currency cur','c.currency_id=cur.id_currency','left');
        $this->db->join('project_pre_approval ppa','ppa.crm_project_id=c.id_crm_project','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }

        if ($this->current_user && !isset($data['created_by']) && !isset($data['all'])) {
            if (is_array($user_list)) {
                $user_list = implode(',', $user_list);
            }
            if (is_array($this->current_user)) {
                $this->current_user = implode(',', $this->current_user);
            }
            $this->db->where('(c.created_by in (' . $user_list . ') or c.assigned_to in (' . $this->current_user . ') or ppa.assigned_to in (' . $this->current_user . '))');
        }

        if(isset($data['sector_id']))
            $this->db->where('c.project_main_sector_id',$data['sector_id']);

        if(isset($data['created_by']))
            $this->db->where('c.created_by ',$data['created_by']);

        if(isset($data['offset']) && isset($data['limit']))
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('project_status !=','deleted');
        $this->db->order_by('id_crm_project','DESC');
        $this->db->group_by('id_crm_project');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectListCount($data)
    {
        $user_list = '';
        if($this->current_user && $this->current_user!=0 && !isset($data['login_user']))
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('count(*) as total');
        $this->db->from('crm_project c');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.project_title like "%'.$data['search_key'].'%"');
        }

        if ($this->current_user && !isset($data['created_by'])) {
            if (is_array($user_list)) {
                $user_list = implode(',', $user_list);
            }
            if (is_array($this->current_user)) {
                $this->current_user = implode(',', $this->current_user);
            }
            $this->db->where('(c.created_by in (' . $user_list . ') or c.assigned_to in (' . $this->current_user . ') or ppa.assigned_to in (' . $this->current_user . '))');
        }

        if(isset($data['sector_id']))
            $this->db->where('c.project_main_sector_id',$data['sector_id']);

        if(isset($data['created_by']))
            $this->db->where('c.created_by ',$data['created_by']);

        if(isset($data['offset']) && isset($data['limit']))
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('project_status !=','deleted');
        $this->db->order_by('id_crm_project','DESC');
        $this->db->group_by('id_crm_project');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalProjectList($data)
    {
        $this->db->select('c.*,s.sector_name as main_sector_name,ss.sector_name as sub_sector_name');
        $this->db->from('crm_project c');
        $this->db->where('c.company_id',$data['company_id']);
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector ss','ss.id_sector=c.project_sub_sector_id','left');
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }

        $this->db->where('project_status !=','deleted');
        $this->db->order_by('id_crm_project','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectFormData($crm_project_id,$form_id)
    {
        $this->db->select('c.*,f.*');
        //$this->db->from('crm_company cc');
        $this->db->from('crm_project_data c');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_project_id' => $crm_project_id,'f.form_id' => $form_id));
        $this->db->order_by('f.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmProject($data)
    {
        $this->db->insert('crm_project', $data);
        return $this->db->insert_id();
    }


    public function addCrmProjectData($data)
    {
        $this->db->insert_batch('crm_project_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmProjectData($data,$type)
    {
        //echo $type."<pre>"; print_r($data); exit;
        if($type==1)
        {
            $id_crm_company_data = $data[0]['id_crm_project_data'];
            unset($data[0]['id_crm_project_data']);
            $this->db->where('id_crm_project_data', $id_crm_company_data);
            $this->db->update('crm_project_data', $data[0]);
        }
        else{
            $this->db->update_batch('crm_project_data', $data, 'id_crm_project_data');
        }
        return 1;
    }

    public function updateCrmProject($data,$crm_project_id)
    {
        $this->db->where('id_crm_project', $crm_project_id);
        $this->db->update('crm_project', $data);
    }

    public function getCrmCompanyIdByModuleId($group_id,$document_type_id)
    {
        $table = ''; $where = '';
        if($document_type_id==1){ $table = 'crm_contact'; $where = array('id_crm_contact' => $group_id); }
        else if($document_type_id==2){ $table = 'crm_company'; $where = array('id_crm_company' => $group_id); }
        else if($document_type_id==3){ $table = 'crm_project'; $where = array('id_crm_project' => $group_id); }

        $query = $this->db->get_where($table,$where);
        //echo $this->db->last_query();
        return $query->row();
    }

    public function getCompanyDesignation()
    {
        $query = $this->db->get('crm_company_designation');
        return $query->result_array();
    }

    public function getCrmListForMapping($data)
    {
        //echo '<pre>';print_r($data);exit;
        if(isset($data['crm_contact_id']))
        {
            $crm_company_id = '';
            if($data['crm_contact_id']!=0){
                $contact_details = $this->getContact($data['crm_contact_id']);
                $crm_company_id = $contact_details[0]['company_id'];
            }

            /*$this->db->select('*');
            $this->db->from('crm_company_contact ccc');
            $this->db->join('crm_company cc','ccc.crm_company_id=cc.id_crm_company','right');
            $this->db->where('ccc.crm_contact_id',$data['crm_contact_id']);*/
            $this->db->select('*');
            $this->db->from('crm_company cc');
            $this->db->where('cc.company_id',$data['company_id']);
            if($data['crm_contact_id']!=0)
                $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM crm_company_contact where crm_contact_id='.$data['crm_contact_id'].' and crm_company_contact_status=1)', NULL, FALSE);
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
            if($data['crm_contact_id']!=0)
                $this->db->where('cc.company_id=',$crm_company_id);
        }
        else if(isset($data['crm_company_id']))
        {
            if($data['crm_company_id']!=0){
                $company_details = $this->getCompany($data['crm_company_id']);
                $crm_company_id = $company_details[0]['company_id'];
            }

            /*$this->db->select('*');
            $this->db->from('crm_company_contact ccc');
            $this->db->join('crm_contact cc','ccc.crm_congetProjectFacilityFieldDatatact_id=cc.id_crm_contact','right');
            $this->db->where('ccc.crm_company_id',$data['crm_company_id']);*/
            $this->db->select('*');
            $this->db->from('crm_contact cc');
            $this->db->where('cc.company_id',$data['company_id']);
            $this->db->where('cc.contact_status',1);
            //$this->db->where('cc.crm_contact_type_id =',1);
            if($data['crm_company_id']!=0)
                $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM crm_company_contact where crm_company_id='.$data['crm_company_id'].'  and crm_company_contact_status=1)', NULL, FALSE);
            $this->db->where('(first_name like "%'.$data['search_key'].'%" or last_name like "%'.$data['search_key'].'%" or email like "%'.$data['search_key'].'%")');
            if($data['crm_company_id']!=0)
                $this->db->where('cc.company_id=',$crm_company_id);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function mapModule($data)
    {
        $this->db->insert('crm_company_contact', $data);
        return $this->db->insert_id();
    }

    public function checkCompanyContactExists($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_contact');
        $this->db->where(array('crm_contact_id' => $data['crm_contact_id'], 'crm_company_id' => $data['crm_company_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateCompanyContactPrimary($crm_contact_id,$data)
    {
        $this->db->where('crm_contact_id', $crm_contact_id);
        $this->db->update('crm_company_contact', $data);
        return 1;
    }

    public function updateCompanyContact($crm_company_contact_id,$data)
    {
        $this->db->where('id_crm_company_contact',$crm_company_contact_id);
        $this->db->update('crm_company_contact', $data);
        return 1;
    }

    public function deleteCompanyContact($crm_company_contact_id)
    {
        $this->db->where('id_crm_company_contact', $crm_company_contact_id);
        $this->db->update('crm_company_contact', array('crm_company_contact_status' => '0'));
        return 1;
    }

    public function getMapModule($data)
    {
        if(isset($data['crm_contact_id']))
        {
            $this->db->select('m.*,c.*,cn.*,d.id_child as id_crm_company_designation,d.child_name as designation_name,m.created_date_time as created_date_time_new');
            $this->db->from('crm_company_contact m');
            $this->db->join('crm_company c','c.id_crm_company=m.crm_company_id','left');
            $this->db->join('master_child d','d.id_child=m.crm_company_designation_id','left');
            $this->db->join('country cn','c.country_id=cn.id_country','left');
            $this->db->where('m.crm_contact_id',$data['crm_contact_id']);
            $this->db->where('m.crm_company_contact_status','1');
            $this->db->order_by('m.id_crm_company_contact','DESC');
        }
        else if(isset($data['crm_company_id']))
        {
            $this->db->select('m.*,c.*,cn.*,d.id_child as id_crm_company_designation,d.child_name as designation_name,m.created_date_time as created_date_time_new');
            $this->db->from('crm_company_contact m');
            $this->db->join('crm_contact c','c.id_crm_contact=m.crm_contact_id','left');
            $this->db->join('country cn','c.country_id=cn.id_country','left');
            $this->db->join('master_child d','d.id_child=m.crm_company_designation_id','left');
            $this->db->where('m.crm_company_id',$data['crm_company_id']);
            $this->db->where('m.crm_company_contact_status','1');
            $this->db->order_by('m.id_crm_company_contact','DESC');
        }
        else if(isset($data['crm_project_id']))
        {

        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContactType()
    {
        $query = $this->db->get('contact_type');
        return $query->result_array();
    }

    public function getCrmContactType()
    {
        $query = $this->db->get('crm_contact_type');
        return $query->result_array();
    }

    public function getProjectContact($data)
    {
        $this->db->select('m.*,c.*,cn.country_name,ct.contact_type');
        $this->db->from('project_contact m');
        $this->db->join('crm_contact c','c.id_crm_contact=m.crm_contact_id','left');
        $this->db->join('contact_type ct','m.contact_type_id=ct.id_contact_type','left');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        $this->db->where('m.crm_project_id',$data['crm_project_id']);
        $this->db->where('m.project_contact_status','1');
        $this->db->order_by('m.id_project_contact','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getCrmProjectContactDataByKeys($data)
    {
        $this->db->select('*');
        $this->db->from('crm_contact_data ccd');
        $this->db->join('form_field ff','ccd.form_field_id=ff.id_form_field','left');
        if(isset($data['crm_company_id']))
            $this->db->where('ccd.crm_contact_id',$data['crm_contact_id']);
        if(isset($data['keys']))
            $this->db->where_in('ff.field_name',$data['keys']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function addProjectContact($data)
    {
        $this->db->insert('project_contact', $data);
        return $this->db->insert_id();
    }

    public function checkProjectContactExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_contact');
        $this->db->where(array('crm_contact_id' => $data['crm_contact_id'], 'crm_project_id' => $data['crm_project_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectContactPrimary($crm_project_id,$data)
    {
        $this->db->where('crm_project_id', $crm_project_id);
        $this->db->update('project_contact', $data);

        return 1;
    }

    public function updateProjectContact($project_contact_id,$data)
    {
        $this->db->where('id_project_contact',$project_contact_id);
        $this->db->update('project_contact', $data);
        return 1;
    }

    public function deleteProjectContact($project_contact_id)
    {
        $this->db->where('id_project_contact', $project_contact_id);
        $this->db->update('project_contact', array('project_contact_status' => '0'));
        return 1;
    }

    public function getCompanyType()
    {
        $query = $this->db->get('company_type');
        return $query->result_array();
    }

    public function getProjectCompany($data)
    {
        $this->db->select('m.*,c.*,cn.country_name');
        $this->db->from('project_company m');
        $this->db->join('crm_company c','c.id_crm_company=m.crm_company_id','left');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        if(isset($data['crm_project_id']))
            $this->db->where('m.crm_project_id',$data['crm_project_id']);
        if(isset($data['crm_company_id']))
            $this->db->where('m.crm_company_id',$data['crm_company_id']);
        if(isset($data['company_type_id']))
            $this->db->where('m.company_type_id',$data['company_type_id']);
        $this->db->where('m.project_company_status','1');
        $this->db->order_by('m.id_project_company','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectCompanyList($data)
    {
        $this->db->select('GROUP_CONCAT(crm_company_id) crm_company_id,GROUP_CONCAT(crm_project_id) as crm_project_id');
        $this->db->from('project_company m');

        if(isset($data['crm_company_id']))
            $this->db->where('m.crm_company_id',$data['crm_company_id']);

        $this->db->where('m.project_company_status','1');
        $this->db->where('m.company_type_id','1');

        $this->db->group_by('crm_company_id');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProjectCompany($data)
    {
        $this->db->insert('project_company', $data);
        return $this->db->insert_id();
    }

    public function checkProjectCompanyExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_company');
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id', $data['crm_company_id']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id', $data['crm_project_id']);
        //$this->db->where(array('crm_company_id' => $data['crm_company_id'], 'crm_project_id' => $data['crm_project_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectCompany($project_company_id,$data)
    {
        $this->db->where('id_project_company',$project_company_id);
        $this->db->update('project_company', $data);
        return 1;
    }

    public function deleteProjectCompany($project_company_id)
    {
        $this->db->where('id_project_company', $project_company_id);
        $this->db->update('project_company', array('project_company_status' => '0'));
        return 1;
    }

    public function getTouchPointType()
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTouchPoints($data)
    {
        /*$this->db->select('*');*/

        $this->db->select("GROUP_CONCAT(crm_touch_point_type_id SEPARATOR  '@@@') as touch_point_type_id,GROUP_CONCAT(crm_touch_point_type SEPARATOR  '@@@') as touch_point_type_name,GROUP_CONCAT(touch_point_description SEPARATOR  '@@@') as touch_point_description,GROUP_CONCAT(DATE(p.created_date_time) SEPARATOR  '@@@') as created_date,GROUP_CONCAT(reminder_date SEPARATOR  '@@@') as reminder_date,GROUP_CONCAT(reminder_time SEPARATOR  '@@@') as reminder_time,GROUP_CONCAT(is_reminder SEPARATOR  '@@@') as reminder,DATE(p.reminder_date) as date,GROUP_CONCAT(CONCAT(u.first_name,' ',u.last_name) SEPARATOR  '@@@') as user_name");
        $this->db->from('crm_touch_point p');
        $this->db->join('user u','p.created_by=u.id_user','left');
        $this->db->join('crm_touch_point_type pt','p.crm_touch_point_type_id=pt.id_crm_touch_point_type','left');

        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);

        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);

        if(isset($data['is_reminder'])){ //results for getting only feature reminders
            $this->db->where('p.is_reminder=','1');
            $this->db->where('DATE(p.reminder_date)>=NOW()');
        }

        if(isset($data['crm_touch_point_type_id']) && $data['crm_touch_point_type_id']!='' && $data['crm_touch_point_type_id']!='0' && $data['crm_touch_point_type_id']!='-1')
            $this->db->where('crm_touch_point_type_id',$data['crm_touch_point_type_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        if(!isset($data['is_all']) && !isset($data['is_reminder']))
            $this->db->where('DATE(p.reminder_date)<=NOW()');


        $this->db->group_by('DATE(p.reminder_date)');
        $this->db->order_by('p.reminder_date','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTotalTouchPoints($data)
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);
        if(isset($data['is_reminder'])){ //results for getting only feature reminders
            $this->db->where('is_reminder=','1');
            $this->db->where('DATE(reminder_date)>=NOW()');
        }
        else if(isset($data['crm_touch_point_type_id']) && $data['crm_touch_point_type_id']!='' && $data['crm_touch_point_type_id']!='0' && $data['crm_touch_point_type_id']!='-1')
            $this->db->where('crm_touch_point_type_id',$data['crm_touch_point_type_id']);

        if(!isset($data['is_all']) && isset($data['is_all'])==true)
            $this->db->where('DATE(p.reminder_date)<=NOW()');


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTouchPointReminder($data)
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);

        $this->db->where('is_reminder=','1');
        $this->db->where('DATE(reminder_date)>=NOW()');
        $this->db->order_by('DATE(reminder_date)','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addTouchPoint($data)
    {
        $this->db->insert('crm_touch_point', $data);
        return $this->db->insert_id();
    }

    public function deleteTouchPoints($data)
    {
        $this->db->where('id_crm_touch_point', $data['id_crm_touch_point']);
        $this->db->update('crm_touch_point', array('touch_point_status' => 1));
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function getProduct($data)
    {
        $this->db->select('p.*,cas.*,DATE(IFNULL(p.updated_date,p.created_date)) last_modified_on,concat_ws(\' \',u.first_name,u.last_name) created_by_user_name,(select count(*) from crm_project cp where cp.product_id=p.id_product) as no_of_projects');
        $this->db->from('product p');
        $this->db->join('company_approval_structure cas','p.company_approval_structure_id=cas.id_company_approval_structure','left');
        $this->db->join('user u','u.id_user=p.created_by','left');
        if(isset($data['company_id']))
            $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['company_approval_structure_id']))
            $this->db->where('p.company_approval_structure_id',$data['company_approval_structure_id']);
        if(isset($data['id_product']))
            $this->db->where('p.id_product',$data['id_product']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->order_by('p.id_product','desc');

        //$this->db->group_by('p.id_product');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProduct($data)
    {
        $this->db->insert('product', $data);
        return $this->db->insert_id();
    }

    public function updateProduct($data)
    {
        $this->db->where('id_product',$data['id_product']);
        $this->db->update('product', $data);
        return 1;
    }
    public function getProjectStages($data=array()){
        $this->db->select('*');
        $this->db->from('project_stage ps');
        if(isset($data['id_project_stage']))
            $this->db->where('id_project_stage',$data['id_project_stage']);
        $this->db->order_by('ps.stage_order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function addProductStageWorkflow($data)
    {
        $this->db->insert('product_workflow_phase', $data);
        return $this->db->insert_id();
    }

    public function updateProductStageWorkflow($data)
    {
        $this->db->where('id_product_workflow_phase',$data['id_product_workflow_phase']);
        $this->db->update('product_workflow_phase', $data);
        return 1;
    }
    public function addProductStageWorkflowApprover($data)
    {
        $this->db->insert('product_workflow_phase_approver', $data);
        return $this->db->insert_id();
    }

    public function updateProductStageWorkflowApprover($data)
    {
        $this->db->where('id_product_workflow_phase_approver',$data['id_product_workflow_phase_approver']);
        $this->db->update('product_workflow_phase_approver', $data);
        return 1;
    }
    public function getProductStageWorkflow($data)
    {
        $this->db->select('*');
        $this->db->from('product_workflow_phase pwp');
        if(isset($data['product_id']))
            $this->db->where('pwp.product_id',$data['product_id']);
        if(isset($data['project_stage_id']))
            $this->db->where('pwp.project_stage_id',$data['project_stage_id']);
        if(isset($data['id_product_workflow_phase']))
            $this->db->where('pwp.id_product_workflow_phase',$data['id_product_workflow_phase']);

        $this->db->where('pwp.workflow_phase_status!=2');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getProductStageWorkflowApprover($data)
    {

        if(isset($data['product_workflow_approval_type']) && $data['product_workflow_approval_type']=='Role')
        {
            $this->db->select('pwpa.*,ar.approval_name as approver');
            $this->db->from('product_workflow_phase_approver pwpa');
            $this->db->join('approval_role ar','pwpa.approver_id=ar.id_approval_role','LEFT');
        }
        else if(isset($data['product_workflow_approval_type']) && ($data['product_workflow_approval_type']=='Committee' || $data['product_workflow_approval_type']=='User'))
        {
            $this->db->select('pwpa.*,CONCAT_WS(\' \',u.first_name,u.last_name) as approver');
            $this->db->from('product_workflow_phase_approver pwpa');
            $this->db->join('user u','pwpa.approver_id=u.id_user','LEFT');
        }
        if(isset($data['id_product_workflow_phase']))
            $this->db->where('pwpa.product_workflow_phase_id',$data['id_product_workflow_phase']);
        $this->db->where('pwpa.status!=2');
        $this->db->order_by('pwpa.order','ASC');


        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getProjectTeam($data)
    {
        $this->db->select('t.*,cu.*,u.id_user,u.user_role_id,u.first_name,u.last_name,u.email_id,u.phone_number,u.profile_image');
        $this->db->from('project_team t');
        $this->db->join('company_user cu','t.team_member_id=cu.id_company_user','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->where('t.crm_project_id',$data['crm_project_id']);
        $this->db->order_by('t.id_project_team','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function checkProjectTeamExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_team');
        $this->db->where(array('crm_project_id' => $data['crm_project_id'], 'team_member_id' => $data['team_member_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectTeam($project_team_id,$data)
    {
        $this->db->where('id_project_team',$project_team_id);
        $this->db->update('project_team', $data);
        return 1;
    }

    public function addProjectTeam($data)
    {
        $this->db->insert('project_team', $data);
        return $this->db->insert_id();
    }

    public function deleteProjectTeam($project_team_id)
    {
        $this->db->where('id_project_team', $project_team_id);
        $this->db->update('project_team', array('team_member_status' => '0'));
        return 1;
    }

    public function updateProject($data,$crm_project_id)
    {
        $this->db->where('id_crm_project',$crm_project_id);
        $this->db->update('crm_project', $data);
        //echo $this->db->last_query();
        return 1;
    }

    public function getIntelligence($data)
    {
        if($data['type']=='crm_company')
        {
            $this->db->select('c.*,CONCAT(u.first_name,\' \',u.last_name) as username,u.phone_number,u.email_id,profile_image,ar.approval_name,id_user,cb.legal_name');
            $this->db->from('crm_company c');
            $this->db->join('user u','c.created_by=u.id_user','left');
            $this->db->join('company_user cu','u.id_user=cu.user_id','left');
            $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
            $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
            $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
            if($data['sector_id']!='' && $data['sector_id']!=0 && $data['sub_sector_id']!='' && $data['sub_sector_id']!=0 && $data['sub_sector_id']!='0')
                $this->db->where(array('c.company_id' => $data['company_id'], 'c.sector_id' => $data['sector_id'], 'c.sub_sector_id' => $data['sub_sector_id']));
            else
                $this->db->where(array('c.company_id' => $data['company_id'], 'c.sector_id' => $data['sector_id']));
            if(isset($data['crm_company_id']))
                $this->db->where('id_crm_company !='.$data['crm_company_id'], NULL, FALSE);
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(c.company_name like "%'.$data['search_key'].'%")');
            }
        }
        else if($data['type']=='crm_project')
        {
            $this->db->select('p.*,CONCAT(u.first_name,\' \',u.last_name) as username,u.phone_number,u.email_id,profile_image,ar.approval_name,id_user,cb.legal_name');
            $this->db->from('crm_project p');
            $this->db->join('user u','p.created_by=u.id_user','left');
            $this->db->join('company_user cu','u.id_user=cu.user_id','left');
            $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
            $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
            $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
            if($data['sector_id']!='' && $data['sector_id']!=0 && $data['sub_sector_id']!='' && $data['sub_sector_id']!=0 && $data['sub_sector_id']!='0')
                $this->db->where(array('p.company_id' => $data['company_id'], 'p.project_main_sector_id' => $data['sector_id'], 'p.project_sub_sector_id' => $data['sub_sector_id']));
            else
                $this->db->where(array('p.company_id' => $data['company_id'], 'p.project_main_sector_id' => $data['sector_id']));
            if(isset($data['crm_project_id']))
                $this->db->where('id_crm_project !='.$data['crm_project_id'], NULL, FALSE);
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(p.project_title like "%'.$data['search_key'].'%")');
            }
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProductTermItem($data)
    {
        $this->db->select('*');
        $this->db->from('product_term_item pti');
        if(isset($data['id_product_term_item']))
            $this->db->where('pti.id_product_term_item',$data['id_product_term_item']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProductTermItemData($data)
    {
        $this->db->select('ptid.*,pti.product_term_item_key,pti.item_type,CONCAT(u.first_name,\' \',u.last_name) as username');
        $this->db->from('product_term_item_data ptid');
        $this->db->join('product_term_item pti','ptid.product_term_item_id=pti.id_product_term_item','left');
        $this->db->join('user u','ptid.created_by=u.id_user','left');
        if(isset($data['crm_project_id']))
            $this->db->where('ptid.crm_project_id',$data['crm_project_id']);
        if(isset($data['product_term_item_id']))
            $this->db->where('ptid.product_term_item_id',$data['product_term_item_id']);
        if(isset($data['product_term_id']))
            $this->db->where('pti.product_term_id',$data['product_term_id']);
        if(isset($data['item_order']))
            $this->db->where('ptid.item_order',$data['item_order']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProductTermItemData($data)
    {
        $this->db->insert('product_term_item_data', $data);
        return $this->db->insert_id();
    }

    public function addProductTermItemData_batch($data)
    {
        $this->db->insert_batch('product_term_item_data', $data);
        return $this->db->insert_id();
    }

    public function updateProductTermItemData($data)
    {
        $this->db->where(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $data['product_term_item_id']));
        $this->db->update('product_term_item_data', $data);
        return 1;
    }

    public function updateMonitoring($data)
    {
        $this->db->update_batch('product_term_item_data', $data, 'id_product_term_item_data');
        return 1;
    }

    public function getFormFieldValueByFieldName($data)
    {
        $this->db->select('c.*');
        $this->db->from('crm_project_data c');
        $this->db->join('form_field ff','c.form_field_id=ff.id_form_field');
        $this->db->join('form f','ff.form_id=f.id_form');
        if(isset($data['form_name']))
            $this->db->where('f.form_name',$data['form_name']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getRiskCategoryItemsWithProjectItems($data)
    {
        //echo "<pre>"; print_r($data); exit;
        /*$this->db->select('r.*,IFNULL(pi.project_risk_category_item_grade,r.risk_category_item_grade) as project_risk_category_item_grade');
        $this->db->from('risk_category_item r');
        $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and pi.crm_project_id='.$data['crm_project_id'].'','left');
        if(isset($data['risk_category_id']))
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
        if(isset($data['risk_category_item_id']))
            $this->db->where('r.id_risk_category_item',$data['risk_category_item_id']);
        if(isset($data['sector_id']) && isset($data['sub_sector_id']))
            $this->db->where('(r.sector_id ='.$data['sub_sector_id'].' or r.sector_id ='.$data['sector_id'].')');*/

        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }
        else
        {
            $data['sector_id'] = 0;
            $sql = 'SELECT  SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'
                AND `r`.`sector_id` = '.$data['sector_id'].'
                UNION All
                SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }*/

        if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){

            $this->db->select('SQL_CALC_FOUND_ROWS`r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_project_id`='.$data['crm_project_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('r.sector_id',$data['sub_sector_id']);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('`r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_project_id`='.$data['crm_project_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('FOUND_ROWS()',0);
            $this->db->where('r.sector_id',$data['sector_id']);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('`r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_project_id`='.$data['crm_project_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('FOUND_ROWS()',0);
            $this->db->where('r.sector_id',0);
            $query = $this->db->get();
            $subQuery3 = $this->db->last_query();
            $query->result_array();

            $query = $this->db->query("$subQuery1 UNION All $subQuery2 UNION All $subQuery3");
        }
        else
        {
            $data['sector_id'] = 0;

            $this->db->select('SQL_CALC_FOUND_ROWS`r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_project_id`='.$data['crm_project_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            //echo $subQuery1; exit;
            $query->result_array();

            $this->db->select('`r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_project_id`='.$data['crm_project_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('FOUND_ROWS()',0);
            $this->db->where('r.sector_id',0);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            //echo $subQuery1; exit;
            $query->result_array();

            $query = $this->db->query("$subQuery1 UNION All $subQuery2");
        }

        //$query = $this->db->query($sql);


        //$query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getRiskCategoryItemsWithCompanyItems($data)
    {
        //echo "<pre>"; print_r($data); exit;
        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_company_id`='.$data['crm_company_id'].'
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_company_id`='.$data['crm_company_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_company_id`='.$data['crm_company_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }
        else
        {
            if($data['sector_id']==''){ $data['sector_id'] = 0; }
            $sql = 'SELECT  SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_company_id`='.$data['crm_company_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'
                AND `r`.`sector_id` = '.$data['sector_id'].'
                UNION All
                SELECT `r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_company_id`='.$data['crm_company_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }*/
        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){

            $this->db->select('SQL_CALC_FOUND_ROWS`r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_company_id`='.$data['crm_company_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('r.sector_id',$data['sub_sector_id']);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            //echo $subQuery1; exit;
            $query->result_array();

            $this->db->select('`r`.*,IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_company_id`='.$data['crm_company_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            //$this->db->where('FOUND_ROWS()',0);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('`r`.*,IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_company_id`='.$data['crm_company_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            //$this->db->where('FOUND_ROWS()',0);
            $this->db->where('r.sector_id',0);
            $query = $this->db->get();
            $subQuery3 = $this->db->last_query();
            $query->result_array();
            //echo "$subQuery1 UNION All $subQuery2 UNION All $subQuery3"; exit;
            $query = $this->db->query("$subQuery1 UNION All $subQuery2 UNION All $subQuery3");
            //$query = $this->db->get();
           // echo "$subQuery1 UNION All $subQuery2 UNION All $subQuery3"; exit;

        }
        else
        {
            if($data['sector_id']==''){ $data['sector_id'] = 0; }

            $this->db->select('SQL_CALC_FOUND_ROWS`r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_company_id`='.$data['crm_company_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('`r`.*, IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and `pi`.`crm_company_id`='.$data['crm_company_id'],'left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('FOUND_ROWS()',0);
            $this->db->where('r.sector_id',0);
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            $subQuery2 = $this->db->last_query();
            $query->result_array();
            $query = $this->db->query("$subQuery1 UNION All $subQuery2");
        }*/
        //$query = $this->db->query($sql);

        $this->db->select('r.*,IFNULL(pi.company_risk_category_item_grade, r.risk_category_item_grade) as company_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
        $this->db->from('risk_category_item r');
        $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and pi.crm_company_id ='.$data['crm_company_id'],'left');
        $this->db->where('r.risk_category_id',$data['risk_category_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getRiskCategoryItemsWithContactItems($data)
    {
        $this->db->select('r.*,IFNULL(pi.contact_risk_category_item_grade, r.risk_category_item_grade) as contact_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected');
        $this->db->from('risk_category_item r');
        $this->db->join('contact_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and pi.crm_contact_id ='.$data['crm_contact_id'],'left');
        $this->db->where('r.risk_category_id',$data['risk_category_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addKnowledgeDocumentViewCount($data)
    {
        $this->db->set('number_of_views', 'number_of_views+1', FALSE);
        $this->db->where('id_knowledge_document', $data['id_knowledge_document']);
        $this->db->update('knowledge_document');
        return 1;
    }

    public function getProjectRiskCategoryItem($data)
    {
        $this->db->select('*');
        $this->db->from('project_risk_category_item');
        if(isset($data['risk_category_item_id']))
            $this->db->where('risk_category_item_id',$data['risk_category_item_id']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectRiskCategory($data)
    {
        $this->db->insert_batch('project_risk_category_item', $data);
        return $this->db->insert_id();
    }

    public function updateProjectRiskCategory($data)
    {
        if(isset($data['risk_category_id'])) {
            $this->db->where('risk_category_id',$data['risk_category_id']);
            $this->db->update('project_risk_category_item',$data);
        }
        else
            $this->db->update_batch('project_risk_category_item', $data, 'id_project_risk_category_item');
        return 1;
    }

    public function checkProjectContainsProjectRiskAssessment($crm_project_id)
    {
        $query = $this->db->get_where('project_risk_category_item',array('crm_project_id' => $crm_project_id));
        return $query->result_array();
    }

    public function getCrmProjectRiskCategoryAttributesItemsGrade($data)
    {
        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }
        else
        {
            $sql = 'SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }*/
        if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){

            $this->db->select('project_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_project_id',$data['crm_project_id']);
            $this->db->where('r.sector_id',$data['sub_sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('project_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_project_id',$data['crm_project_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('project_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_project_id',$data['crm_project_id']);
            $this->db->where('r.sector_id',0);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery3 = $this->db->last_query();
            $query->result_array();

            $this->db->from("($subQuery1 UNION All $subQuery2 UNION All $subQuery3)tmp");
            $query = $this->db->get();
        }
        else
        {
            $this->db->select('project_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_project_id',$data['crm_project_id']);
            $this->db->where('r.sector_id',$data['sub_sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('project_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_project_id',$data['crm_project_id']);
            $this->db->where('r.sector_id',0);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->from("($subQuery1 UNION All $subQuery2)tmp");
            $query = $this->db->get();
        }

        //echo $sql; exit;
        //$query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getCompanyRiskCategoryItem($data)
    {
        $this->db->select('*');
        $this->db->from('company_risk_category_item');
        if(isset($data['risk_category_item_id']))
            $this->db->where('risk_category_item_id',$data['risk_category_item_id']);
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id',$data['crm_company_id']);

        $query = $this->db->get();
        return $query->result_array();
    }
    public function getContactRiskCategoryItem($data)
    {
        $this->db->select('*');
        $this->db->from('contact_risk_category_item');
        if(isset($data['risk_category_item_id']))
            $this->db->where('risk_category_item_id',$data['risk_category_item_id']);
        if(isset($data['crm_contact_id']))
            $this->db->where('crm_contact_id',$data['crm_contact_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function checkCompanyContainsCompanyRiskAssessment($crm_company_id)
    {
        $query = $this->db->get_where('company_risk_category_item',array('crm_company_id' => $crm_company_id));
        return $query->result_array();
    }
    public function checkContactContainsCompanyRiskAssessment($crm_contact_id)
    {
        $query = $this->db->get_where('contact_risk_category_item',array('crm_contact_id' => $crm_contact_id));
        return $query->result_array();
    }

    public function getCrmCompanyRiskCategoryAttributesItemsGrade($data)
    {
        //if($data['sector_id']==''){ $data['sector_id']=0; }
        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT company_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'  and `pi`.`crm_company_id`='.$data['crm_company_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT company_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_company_id`='.$data['crm_company_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT company_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_company_id`='.$data['crm_company_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }
        else
        {
            $sql = 'SELECT company_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_company_id`='.$data['crm_company_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT company_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `company_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_company_id`='.$data['crm_company_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }*/
        /*if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){

            $this->db->select('company_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_company_id',$data['crm_company_id']);
            $this->db->where('r.sector_id',$data['sub_sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('company_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_company_id',$data['crm_company_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('company_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_company_id',$data['crm_company_id']);
            $this->db->where('r.sector_id',0);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery3 = $this->db->last_query();
            $query->result_array();

            $this->db->from("($subQuery1 UNION All $subQuery2 UNION All $subQuery3)tmp");
            $query = $this->db->get();
        }
        else
        {
            $this->db->select('company_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_company_id',$data['crm_company_id']);
            $this->db->where('r.sector_id',$data['sector_id']);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('company_risk_category_item_grade');
            $this->db->from('risk_category_item r');
            $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
            $this->db->where('pi.crm_company_id',$data['crm_company_id']);
            $this->db->where('r.sector_id',0);
            $this->db->where('pi.is_active',1);
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();


            $this->db->from("($subQuery1 UNION All $subQuery2)tmp");
            $query = $this->db->get();
        }*/
        //echo $sql; exit;
        //$query = $this->db->query($sql);

        $this->db->select('company_risk_category_item_grade');
        $this->db->from('risk_category_item r');
        $this->db->join('company_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
        $this->db->where('r.risk_category_id',$data['risk_category_id']);
        $this->db->where('pi.crm_company_id',$data['crm_company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getCrmContactRiskCategoryAttributesItemsGrade($data)
    {
        $this->db->select('contact_risk_category_item_grade');
        $this->db->from('risk_category_item r');
        $this->db->join('contact_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id','left');
        $this->db->where('r.risk_category_id',$data['risk_category_id']);
        $this->db->where('pi.crm_contact_id',$data['crm_contact_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCompanyRiskCategory($data)
    {
        $this->db->insert_batch('company_risk_category_item', $data);
        return $this->db->insert_id();
    }
    public function addContactRiskCategory($data)
    {
        $this->db->insert_batch('contact_risk_category_item', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyRiskCategory($data)
    {
        if(isset($data['risk_category_id'])) {
            $this->db->where('risk_category_id',$data['risk_category_id']);
            $this->db->update('company_risk_category_item',$data);
        }
        else
            $this->db->update_batch('company_risk_category_item', $data, 'id_company_risk_category_item');
        return 1;
    }
    public function updateContactRiskCategory($data)
    {
        if(isset($data['risk_category_id'])) {
            $this->db->where('risk_category_id',$data['risk_category_id']);
            $this->db->update('contact_risk_category_item',$data);
        }
        else
            $this->db->update_batch('contact_risk_category_item', $data, 'id_contact_risk_category_item');
        return 1;
    }

    public function statusFlow($data)
    {
        $this->db->select('pa.forwarded_to as assigned_to,pa.id_project_approval,cb.legal_name,pa.approval_status,DATE_FORMAT(pa.created_date_time, "%d %b %Y") as date,c.id_company_approval_credit_committee as credit_committee_id,c.credit_committee_name,CONCAT(u.first_name,\' \',u.last_name) as username');
        $this->db->from('project_approval pa');
        $this->db->join('company_approval_credit_committee c','pa.company_approval_credit_committee_id=c.id_company_approval_credit_committee','left');
        $this->db->join('user u','pa.forwarded_to=u.id_user','left');
        $this->db->join('company_user cu','pa.forwarded_to=cu.user_id','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pa.project_id',$data['crm_project_id']);
        $this->db->order_by('pa.id_project_approval', 'DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function projectApprovalMeeting($data)
    {
        $this->db->select('m.id_meeting,m.meeting_name,m.created_by as created_by_id,IFNULL(m.from_time,0) as from_time,IFNULL(m.to_time,0) as to_time,CONCAT(m.`when`) as meeting_date, concat(m.from_time,\'/\',m.to_time) as meeting_time,GROUP_CONCAT(CONCAT(u1.first_name,\' \',u1.last_name)) as members,CONCAT(u.first_name," ",u.last_name) as created_by,m.agenda');
        $this->db->from('project_approval_meeting pam');
        $this->db->join('meeting m','pam.meeting_id=m.id_meeting','left');
        //$this->db->join('meeting_guest mg','mg.meeting_id=m.id_meeting','left');
        $this->db->join('meeting_invitation mi','m.id_meeting=mi.meeting_id','left');
        $this->db->join('user u','m.created_by=u.id_user','left');
        $this->db->join('user u1','mi.invitation_id=u1.id_user','left');

        if(isset($data['project_approval_id']))
            $this->db->where('pam.project_approval_id',$data['project_approval_id']);
        $this->db->group_by('m.id_meeting');
        $this->db->order_by('m.id_meeting','desc');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getMeetingGuest($data)
    {
        $this->db->select('group_concat(email) as guest');
        $this->db->from('meeting_guest');
        if(isset($data['meeting_id']))
            $this->db->where('meeting_id',$data['meeting_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getMeetingInvitaion($data)
    {
        $this->db->select('0');
        $this->db->from('meeting_invition');
        if(isset($data['meeting_id']))
            $this->db->where('meeting_id',$data['meeting_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function adddKnowledgeDocumentView($data)
    {
        $this->db->insert('knowledge_document_view', $data);
        return $this->db->insert_id();
    }

    public function getFacilitiesList($data)
    {
        $this->db->select('pf.*,cf.facility_name');
        $this->db->select('(SELECT SUM(fd.usd_equiv) FROM facility_disbursement fd WHERE fd.contract_id=pf.contract_id) as disbursment,(SELECT SUM(flr.usd_equiv) FROM facility_loan_receipts flr WHERE flr.contract_id=pf.contract_id) as collections');
        $this->db->from('project_facility pf');
        $this->db->join('company_facility cf','pf.company_facility_id=cf.id_company_facility','left');

        if(isset($data['type']) && $data['type']=='workflow')
        {
            $this->db->select('c.currency_code');
            $this->db->join('currency c','pf.currency_id=c.id_currency','left');
            $this->db->select('mc.child_name as sub_type');
            $this->db->join('master_child mc','pf.facility_sub_type=mc.id_child','left');
            $this->db->select('mc1.child_name as source_of_founds');
            $this->db->join('master_child mc1','pf.facility_source_of_payment=mc1.id_child','left');
            $this->db->select('mc2.child_name as facility_type_name');
            $this->db->join('master_child mc2','pf.facility_type=mc2.id_child','left');
        }

        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);
        if(isset($data['project_facility_status']))
            $this->db->where('pf.project_facility_status',$data['project_facility_status']);
        $this->db->where('pf.project_facility_status !=','inactive');
        $this->db->order_by('pf.id_project_facility','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilities($data)
    {
        $this->db->select('pf.*,c.*,cf.facility_name,IFNULL(pf.facility_type,cf.facility_type) as facility_type,IFNULL(pf.facility_type_description,cf.facility_type_description) as facility_type_description,IFNULL(pf.amount_description,cf.amount_description) as amount_description');
        $this->db->select('(SELECT SUM(fd.usd_equiv) FROM facility_disbursement fd WHERE fd.contract_id=pf.contract_id) as disbursment,(SELECT SUM(flr.usd_equiv) FROM facility_loan_receipts flr WHERE flr.contract_id=pf.contract_id) as collections');
        $this->db->from('project_facility pf');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('company_facility cf','pf.company_facility_id=cf.id_company_facility','left');

        if(isset($data['type']) && $data['type']=='workflow')
        {
            $this->db->select('mc.child_name as sub_type');
            $this->db->join('master_child mc','pf.facility_sub_type=mc.id_child','left');
            $this->db->select('mc1.child_name as source_of_founds');
            $this->db->join('master_child mc1','pf.facility_source_of_payment=mc1.id_child','left');
            $this->db->select('mc2.child_name as facility_type_name');
            $this->db->join('master_child mc2','pf.facility_type=mc2.id_child','left');
        }

        if(isset($data['id_project_facility']))
            $this->db->where('pf.id_project_facility',$data['id_project_facility']);
        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);
        if(isset($data['project_stage_info_id']))
            $this->db->where('pf.project_stage_info_id',$data['project_stage_info_id']);
        if(isset($data['project_facility_status']))
            $this->db->where('pf.project_facility_status',$data['project_facility_status']);
        $this->db->order_by('pf.id_project_facility','desc');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getFacilityActivity($data)
    {
        if(isset($data['project_approval_id'])) {
            $this->db->select('max(id_facility_activity)');
            $this->db->from('facility_activity');
            $this->db->where('project_approval_id', $data['project_approval_id']);
            $this->db->group_by('project_facility_id');
            $this->db->order_by('id_facility_activity', 'DESC');
            $subQuery = $this->db->get_compiled_select();
        }


        $this->db->select('fa.id_facility_activity,pf.id_project_facility,fa.project_facility_id,pf.facility_name,pf.expected_start_date,c.currency_code,CONCAT(pf.expected_maturity,\' \',pf.expected_maturity_type),fa.facility_amount,fa.facility_amount as loan_amount,pf.expected_maturity,pf.expected_maturity_type,fa.facility_status,fa.facility_status as project_facility_status,DATE(fa.created_date_time) as created_date_time,pf.currency_id,pf.expected_interest_rate,pf.project_loan_type_id,pf.project_payment_type_id,ppt.project_payment_type,fa.facility_comments');
        $this->db->from('facility_activity fa');
        $this->db->join('project_facility pf','fa.project_facility_id=pf.id_project_facility','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('project_payment_type ppt','pf.project_payment_type_id=ppt.id_project_payment_type','left');
        if(isset($data['project_approval_id']))
            $this->db->where('id_facility_activity IN ('.$subQuery.')', NULL, FALSE);

        $this->db->order_by('fa.id_facility_activity','DESC');
        $this->db->group_by('fa.project_facility_id');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilityActivityInitial($data)
    {
        $this->db->select('fa.id_facility_activity,pf.facility_name,pf.expected_start_date,c.currency_code,CONCAT(pf.expected_maturity,\' \',pf.expected_maturity_type),fa.facility_amount as loan_amount,pf.expected_maturity,pf.expected_maturity_type,fa.facility_status as project_facility_status,DATE(fa.created_date_time) as created_date_time,pf.currency_id,pf.expected_interest_rate,pf.project_loan_type_id,pf.project_payment_type_id,ppt.project_payment_type,fa.facility_comments');
        $this->db->from('facility_activity fa');
        $this->db->join('project_facility pf','fa.project_facility_id=pf.id_project_facility','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('project_payment_type ppt','pf.project_payment_type_id=ppt.id_project_payment_type','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);

        $this->db->where('fa.project_approval_id IS NULL');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilitiesCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('project_facility');

        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addFacilities($data)
    {
        $this->db->insert('project_facility', $data);
        return $this->db->insert_id();
    }

    public function updateFacilities($data)
    {
        $this->db->where('id_project_facility',$data['id_project_facility']);
        $this->db->update('project_facility',$data);
        return 1;
    }

    public function addCollateral($data)
    {
        $this->db->insert('collateral', $data);
        return $this->db->insert_id();
    }

    public function updateCollateral($data)
    {
        if(isset($data['id_collateral']))
            $this->db->where('id_collateral',$data['id_collateral']);
        $this->db->update('collateral',$data);
        return 1;
    }

    public function getCollateralDetails($collateral_id)
    {
        $this->db->select('*');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','ct.id_collateral_type=c.collateral_type_id');
        $this->db->where('c.id_collateral',$collateral_id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }



    public function getCollateralList($data)
    {
        if(isset($data['status']) && $data['status']=='pending')
        {
            $this->db->select('c.id_collateral,c.collateral_code');
            $this->db->from('collateral c');
            if(isset($data['company_id']))
                $this->db->where('c.company_id', $data['company_id']);
            $this->db->order_by('c.id_collateral','DESC');
            $query = $this->db->get();
        }
        else
        {
            $this->db->select('c.id_collateral,fc.facility_id,c.collateral_code,pf.facility_name,pf.loan_amount,cp.project_title,u.first_name as username');
            $this->db->from('facility_collateral fc');
            $this->db->join('collateral c','fc.collateral_id=c.id_collateral');
            $this->db->join('project_facility pf','pf.id_project_facility=fc.facility_id');
            $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id');
            $this->db->join('user u','u.id_user=c.created_by');
            if(isset($data['crm_project_id']))
                $this->db->where('cp.id_crm_project', $data['crm_project_id']);
            if(isset($data['company_id']))
                $this->db->where('cp.company_id', $data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='')
                $this->db->where('(c.collateral_code like "%'.$data['search_key'].'%")');
            if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit'],$data['offset']);

            $this->db->order_by('c.id_collateral','DESC');
            $query = $this->db->get();
        }
        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }



    public function getCollateralTypeFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field ctf');
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFormFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage_field');
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }



    public function getCollateralStageFormData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage_field_data');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id', $data['collateral_id']);
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }



    public function addCollateralStageFormData($data)
    {
        $this->db->insert_batch('collateral_stage_field_data', $data);
        return $this->db->insert_id();
    }



    public function updateCollateralStageFormData($data)
    {
        $this->db->update_batch('collateral_stage_field_data', $data, 'id_collateral_stage_field_data');
        return true;
    }

    public function getCollateralTypeStages($data)
    {
        $this->db->select('cs.*');
        $this->db->from('collateral_type_stage ctp');
        $this->db->join('collateral_stage cs','cs.id_collateral_stage=ctp.collateral_stage_id');
        if(isset($data['collateral_type_id']))
            $this->db->where('ctp.collateral_type_id', $data['collateral_type_id']);
        if(isset($data['company_id']))
            $this->db->where('ctp.company_id', $data['company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFields($data)
    {
        $collateralId = '';
        $this->db->select('ctf.*,csd.collateral_stage_field_value');
        $this->db->from('collateral_stage_field ctf');
        if(isset($data['collateral_id']))
            $collateralId = ' and csd.collateral_id ='.$data['collateral_id'];
        $this->db->join('collateral_stage_field_data csd','csd.collateral_stage_field_id=ctf.id_collateral_stage_field'.$collateralId,'left');
        if(isset($data['collateral_stage_id']))
            $this->db->where('ctf.collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralStageFieldsDataDates($data)
    {
        /*$sql = 'SELECT DISTINCT(DATE_FORMAT(csd.created_date_time,"%d-%m-%Y")) as `date`
                FROM `collateral_stage_field` `ctf`
                LEFT JOIN `collateral_stage_field_data` `csd` ON `csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ='.$data['collateral_id'].'
                WHERE `ctf`.`collateral_stage_id` = '.$data['collateral_stage_id'].' GROUP BY csd.created_date_time order by csd.created_date_time desc';*/

        $this->db->select('DISTINCT(DATE_FORMAT(csd.created_date_time,"%d-%m-%Y")) as date');
        $this->db->from('collateral_stage_field ctf');
        $this->db->join('collateral_stage_field_data csd','csd.collateral_stage_field_id=ctf.id_collateral_stage_field and csd.collateral_id='.$data['collateral_id'],'left');
        $this->db->where('ctf.collateral_stage_id',$data['collateral_stage_id']);
        $this->db->order_by('csd.created_date_time','desc');
        $this->db->group_by('csd.created_date_time');
        //echo $sql; exit;
        //$query = $this->db->query($sql);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFieldsByDate($data)
    {
        $sql = 'select * from (SELECT `ctf`.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date
                FROM `collateral_stage_field` `ctf`
                LEFT JOIN `collateral_stage_field_data` `csd` ON `csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ='.$data['collateral_id'].' and  DATE_FORMAT(csd.created_date_time,"%Y-%m-%d")="'.date('Y-m-d',strtotime($data["date"])).'"
                WHERE `ctf`.`collateral_stage_id` = '.$data['collateral_stage_id'].'  ORDER BY csd.id_collateral_stage_field_data DESC)tmp GROUP BY collateral_stage_field_id';

        /*$this->db->select('*')->from('certs');
        $sub = $this->subquery->start_subquery('where_in');
        $sub->select('id_cer')->from('revokace');
        $this->subquery->end_subquery('id', FALSE);*/

        /*$this->db->select('ctf.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date');
        $this->db->from('collateral_stage_field ctf');
        $this->db->join('collateral_stage_field_data csd','csd.collateral_stage_field_id=ctf.id_collateral_stage_field and csd.collateral_id` ='.$data['collateral_id'].' and  DATE_FORMAT(csd.created_date_time,"%Y-%m-%d")="'.date('Y-m-d',strtotime($data["date"])).'"','left');
        $this->db->where('ctf.collateral_stage_id',$data['collateral_stage_id']);
        $this->db->order_by('csd.id_collateral_stage_field_data','DESC');

        $this->db->select('*');
        $sub = $this->subquery->start_subquery('from');
        $this->group_by('collateral_stage_field_id');*/

        $this->db->select('`ctf`.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date');
        $this->db->from('`collateral_stage_field` `ctf`');
        $this->db->join('`collateral_stage_field_data` `csd`','`csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ='.$data['collateral_id'].' and  DATE_FORMAT(csd.created_date_time,"%Y-%m-%d")="'.date('Y-m-d',strtotime($data["date"])).'"','left');
        $this->db->where('`ctf`.`collateral_stage_id`',$data['collateral_stage_id']);
        $this->db->order_by('csd.id_collateral_stage_field_data','DESC');
        $query = $this->db->get();
        $subQuery = $this->db->last_query();
        $query->result_array();

        $this->db->form("($subQuery)tmp");
        $this->db->group_by('collateral_stage_field_id');
        //echo $sql; exit;
        //$query = $this->db->query($sql);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFieldData($data)
    {
        /*$sql = 'SELECT `ctf`.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date
                FROM `collateral_stage_field` `ctf`
                LEFT JOIN `collateral_stage_field_data` `csd`  ON `csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ="'.$data['collateral_id'].'"
                WHERE `ctf`.`collateral_stage_id` = '.$data['collateral_stage_id'].'  ORDER BY csd.id_collateral_stage_field_data DESC';*/

        $this->db->select('ctf.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date');
        $this->db->from('collateral_stage_field ctf');
        $this->db->join('collateral_stage_field_data csd','csd.collateral_stage_field_id=ctf.id_collateral_stage_field and csd.collateral_id ='.$data['collateral_id'],'left');
        $this->db->where('ctf.collateral_stage_id',$data['collateral_stage_id']);
        $this->db->order_by('csd.id_collateral_stage_field_data','DESC');
        //echo $sql; exit;
        //$query = $this->db->query($sql);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteCollateralStageFormData($data)
    {
        $this->db->where(array('DATE_FORMAT(created_date_time,"%Y-%m-%d")' => date('Y-m-d',strtotime($data["date"])),'collateral_id' => $data['collateral_id'],'collateral_stage_id' => $data['collateral_stage_id']));
        $this->db->delete('collateral_stage_field_data');
    }

    public function deleteFacilityCollateral($data)
    {
        $this->db->where(array('collateral_id' => $data['collateral_id'],'facility_id' => $data['facility_id']));
        $this->db->delete('facility_collateral');
    }

    public function getCollateralTypeCategories($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_question_category');
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        if(isset($data['company_id']))
            $this->db->where('company_id', $data['company_id']);
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
      /*  echo $this->db->last_query(); exit;*/
        return $query->result_array();

    }

    public function addFacilityActivity($data)
    {
        $this->db->insert('facility_activity', $data);
        return $this->db->insert_id();
    }

    public function getFacilityLoanAmountByProjectId($crm_project_id)
    {
        $this->db->select('sum(pf.facility_usd_amount) as amount');
        //$this->db->select('c.currency_code,sum(pf.facility_usd_amount) as amount');
        $this->db->from('project_facility pf');
        //$this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->where('crm_project_id',$crm_project_id);
        //$this->db->group_by('pf.currency_id');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getKnowledgeDocuments($data)
    {

        $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
        $this->db->from('knowledge_document k');
        $this->db->join('user u','k.uploaded_by=u.id_user','left');
        $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
        if(isset($data['company_id']))
            $this->db->where('k.company_id',$data['company_id']);
        if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
            $this->db->where('t.tag_name in '.$data['search_key']);
        if(isset($data['knowledge_document_id']))
            $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
        $this->db->group_by('k.id_knowledge_document');
        $this->db->order_by('k.id_knowledge_document','DESC');
        $this->db->where('knowledge_document_status',1);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();

        /*if(!isset($data['created_by']))
        {
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where('knowledge_document_status',1);
            if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit'],$data['offset']);
            else if(isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit']);
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            return $query->result_array();
        }
        else
        {
            // #1 SubQueries no.1 -------------------------------------------
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where('knowledge_document_status',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->_compile_select();
            $this->db->_reset_select();

            // #2 SubQueries no.2 -------------------------------------------
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where(array('knowledge_document_status' => 1,'uploaded_by' => $data['created_by']));
            $query = $this->db->get();
            $subQuery2 = $this->db->_compile_select();
            $this->db->_reset_select();

            // #3 Union with Simple Manual Queries --------------------------
            $this->db->query("select * from ($subQuery1 UNION $subQuery2) as unionTable");

            // #3 (alternative) Union with another Active Record ------------
            $this->db->from("($subQuery1 UNION $subQuery2)");
            $this->db->get();
            echo $this->db->last_query(); exit;
            return $query->result_array();
        }*/


    }

    public function getKnowledgeDocumentComments($data)
    {
        $this->db->select('k.comment,k.comment_status,CONCAT(u.first_name,\' \',u.last_name) as user_name,u.profile_image,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date,ar.approval_name');
        $this->db->from('knowledge_document_comment k');
        $this->db->join('user u','k.commented_by=u.id_user','left');
        $this->db->join('company_user cu','k.commented_by=cu.user_id','left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        if(isset($data['knowledge_document_id']))
            $this->db->where('knowledge_document_id',$data['knowledge_document_id']);

        $this->db->order_by('k.id_knowledge_document_comment','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addKnowledgeDocumentComments($data)
    {
        $this->db->insert('knowledge_document_comment', $data);
        return $this->db->insert_id();
    }

    public function updateKnowledgeDocumentComments($data)
    {
        $this->db->where('id_knowledge_document_comment',$data['id_knowledge_document_comment']);
        $this->db->update('knowledge_document_comment',$data);
        return 1;
    }

    public function addProjectFacilityHistory($data)
    {
        $this->db->insert('project_facility_history',$data);
        return $this->db->insert_id();
    }

    public function getCrmModuleStatus($data)
    {
        $this->db->select('*');
        $this->db->from('crm_module_status');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['crm_module_type']))
            $this->db->where('crm_module_type',$data['crm_module_type']);
        if(isset($data['reference_id']))
            $this->db->where('reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
        $this->db->where('reference_type',$data['reference_type']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addCrmModuleStatus($data)
    {
        $this->db->insert('crm_module_status',$data);
        return $this->db->insert_id();
    }

    public function getCurrency($data)
    {
        $this->db->select('*');
        $this->db->from('company_currency cc');
        $this->db->join('currency c','cc.currency_id=c.id_currency','left');
        //$this->db->join('company cm','cm.currency_id=c.id_currency','left');

        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['currency_id']))
            $this->db->where('cc.currency_id',$data['currency_id']);

        $this->db->where('cc.company_currency_status',1);
        //$this->db->group_by('c.id_currency');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyCurrencyValue($data)
    {
        $this->db->select('*');
        $this->db->from('company_currency_value ccv');
        $this->db->join('company_currency cc','ccv.company_currency_id=cc.id_company_currency','left');
        $this->db->join('currency c','cc.currency_id=c.id_currency ','left');
        //$this->db->join('company cm','cm.currency_id=c.id_currency','left');

        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['currency_id']))
            $this->db->where('cc.currency_id',$data['currency_id']);

        $this->db->where('cc.company_currency_status',1);
        $this->db->order_by('ccv.id_company_currency_value', 'DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectCovenant($data)
    {
        $this->db->select('cc.*,c.*,IFNULL(pc.covenant_id,0) as is_checked');
        $this->db->from('covenant_category cc');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        $this->db->join('covenant c','cc.id_covenant_category=c.covenant_category_id','left');
        $this->db->join('project_covenant pc','c.id_covenant=pc.covenant_id and pc.project_id='.$data['crm_project_id'],'left');
        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key', $data['covenant_type_key']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id', $data['covenant_category_id']);
        if(isset($data['sector_id']))
            $this->db->where('((FIND_IN_SET('.$data['sector_id'].',sector)>0) or (FIND_IN_SET(0,sector)>0))');
        else $this->db->where('(FIND_IN_SET(0,sector)>0)');

        $this->db->where('c.covenant_status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getSelectedProjectCovenants($data)
    {
        $this->db->select('c.*');
        $this->db->from('covenant c');
        $this->db->join('project_covenant pc','c.id_covenant=pc.covenant_id','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pc.project_id',$data['crm_project_id']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id',$data['covenant_category_id']);

        $this->db->where('c.covenant_status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectCovenantByProjectId($data)
    {
        $this->db->select('p.*');
        $this->db->from('project_covenant p');
        $this->db->join('covenant c','p.covenant_id=c.id_covenant','left');
        $this->db->join('covenant_category cc','c.covenant_category_id=cc.id_covenant_category','left');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        if(isset($data['crm_project_id']))
            $this->db->where('p.project_id',$data['crm_project_id']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id',$data['covenant_category_id']);
        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key',$data['covenant_type_key']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProjectCovenant($data)
    {
        $this->db->insert_batch('project_covenant', $data);
        return $this->db->insert_id();
    }

    public function deleteProjectCovenant($data)
    {
        $this->db->where($data);
        $this->db->delete('project_covenant');
    }

    public function getAllCompaniesList($data)
    {
        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }


        if ($this->current_user) {
            $this->db->where_in('c.created_by ', $user_list);
        }

        $this->db->select('c.id_crm_company,c.company_name');
        $this->db->from('crm_company c');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('c.company_name like "%'.$data['search_key'].'%"');
        }
        else{
            //$this->db->limit(20,0);
        }
        $this->db->where('company_status',1);
        $this->db->order_by('id_crm_company','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectRiskItems($data)
    {
        $this->db->select('*');
        $this->db->from('project_risk_item');
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectRiskItems($data)
    {
        $this->db->insert('project_risk_item',$data);
        return $this->db->insert_id();
    }

    public function updateProjectRiskItems($data)
    {
        $this->db->where('id_project_risk_item',$data['id_project_risk_item']);
        $this->db->update('project_risk_item',$data);
        return 1;
    }

    public function getCollateralProjectTabsCount($data)
    {
        /*$this->db->select('count(*) as records,IFNULL(sum(amount),0) as amount');
        $this->db->from('collateral c');
        $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id');
        $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id');

        if(isset($data['company_id']))
            $this->db->where('cp.company_id', $data['company_id']);

        if(isset($data['status']))
            $this->db->where('cp.project_status',$data['status']);

        $this->db->order_by('c.id_collateral','DESC');

        $query = $this->db->get();*/
        /*$sql = 'select sum(records) as records,amount from (select count(*) as records,IFNULL(sum(amount),0) as amount from collateral c left join project_facility pf
                on pf.id_project_facility=c.project_facility_id left join crm_project cp on cp.id_crm_project=pf.crm_project_id where true ';
        if(isset($data['company_id']))
            $sql.=' and cp.company_id="'.$data['company_id'].'" ';
        if(isset($data['status']))
            $sql.=' and cp.project_status="'.$data['status'].'" ';

        if($data['status']=='Pending') {
            $sql .= ' UNION ';
            $sql .= 'select count(*) as records,0 as amount from collateral c left join facility_collateral fc on c.id_collateral=fc.collateral_id  where c.company_id="'.$data['company_id'].'" and  fc.collateral_id is NULL';

        }
        $sql .= ')tmp';*/

        $this->db->select('count(*) as records,IFNULL(sum(amount),0) as amount');
        $this->db->from('collateral c');
        $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id','left');
        $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id','left');
        if(isset($data['company_id']))
            $this->db->where('cp.company_id',$data['company_id']);
        if(isset($data['status']))
            $this->db->where('cp.project_status',$data['status']);
        $query = $this->db->get();
        $subQuery1 = $this->db->last_query();
        $query->result_array();

        if($data['status']=='Pending') {
            $this->db->select('count(*) as records,0 as amount');
            $this->db->from('collateral c');
            $this->db->join('facility_collateral fc', 'c.id_collateral=fc.collateral_id', 'left');
            $this->db->where('c.company_id', $data['company_id']);
            $this->db->where('fc.collateral_id is NULL');
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();

            $this->db->select('sum(records) as records,amount');
            $this->db->from("($subQuery1 UNION $subQuery2)tmp");
            $query = $this->db->get();
        }
        else
        {
            $this->db->select('sum(records) as records,amount');
            $this->db->from("($subQuery1)tmp");
            $query = $this->db->get();
        }



        //$query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralProjectList($data)
    {
        if(isset($data['status']) && $data['status']=='Pending')
        {
            /*$sql='select * from (
                            select c.id_collateral,c.collateral_code,pf.facility_name,pf.loan_amount,cp.project_title,u.first_name as username,ct.collateral_type_name,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as created_date,v.collateral_stage_field_value as latest_value
                            from collateral c left join project_facility pf on pf.id_project_facility=c.project_facility_id
                            left join crm_project cp on cp.id_crm_project=pf.crm_project_id
                            left join collateral_type ct on ct.id_collateral_type = c.collateral_type_id
                            Left JOIN (select csfd.collateral_id,csfd.collateral_stage_field_value from collateral_stage_field csf
                                LEFT JOIN collateral_stage_field_data csfd on csf.id_collateral_stage_field = csfd.collateral_stage_field_id
                                WHERE csf.collateral_stage_filed_name = \'valuation_date\') v on v.collateral_id = c.id_collateral
                            left join user u on u.id_user=c.created_by  where true';
            if(isset($data['company_id']))
                $sql.=' and cp.company_id="'.$data['company_id'].'" ';

            if(isset($data['search_key']) && $data['search_key']!=''){
                $sql.=' and ((cp.project_title like "%'.$data['search_key'].'%") or (c.collateral_code like "%'.$data['search_key'].'%") or (pf.facility_name like "%'.$data['search_key'].'%")) ';
            }

            if(isset($data['sector_id']))
                $sql.=' and cp.project_main_sector_id="'.$data['sector_id'].'" ';

            if(isset($data['status']))
                $sql.=' and cp.project_status="'.$data['status'].'" ';

            $sql.=' UNION ';
            $sql.= 'select id_collateral,collateral_code,
                    NULL as facility_name, 0 as loan_amount, "" as project_title,u.first_name as username,ct.collateral_type_name,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as created_date,v.collateral_stage_field_value as latest_value from collateral c
                    left join user u on u.id_user=c.created_by
                    left join collateral_type ct on ct.id_collateral_type = c.collateral_type_id
                    Left JOIN (select csfd.collateral_id,csfd.collateral_stage_field_value from collateral_stage_field csf
                        LEFT JOIN collateral_stage_field_data csfd on csf.id_collateral_stage_field = csfd.collateral_stage_field_id
                        WHERE csf.collateral_stage_filed_name = \'valuation_date\') v on v.collateral_id = c.id_collateral
                    where project_facility_id is NULL) tmp';
            $sql.=' order by id_collateral DESC ';
            if(isset($data['offset']) && isset($data['limit']))
                $sql.=' limit '.$data['offset'].', '.$data['limit'].' ';*/
            //echo $sql;exit;

            $this->db->select('c.id_collateral,c.collateral_code,pf.facility_name,pf.loan_amount,cp.project_title,u.first_name as username,ct.collateral_type_name,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as created_date,v.collateral_stage_field_value as latest_value');
            $this->db->from('collateral c');
            $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id','left');
            $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id','left');
            $this->db->join('collateral_type ct','ct.id_collateral_type = c.collateral_type_id','left');
            $this->db->join('(select csfd.collateral_id,csfd.collateral_stage_field_value from collateral_stage_field csf
                                LEFT JOIN collateral_stage_field_data csfd on csf.id_collateral_stage_field = csfd.collateral_stage_field_id
                                WHERE csf.collateral_stage_filed_name = \'valuation_date\')v','v.collateral_id = c.id_collateral','left');
            $this->db->join('user u','u.id_user=c.created_by','left');
            if(isset($data['company_id']))
                $this->db->where('cp.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='') {
                $this->db->where('((cp.project_title like "%'.$data['search_key'].'%") or (c.collateral_code like "%'.$data['search_key'].'%") or (pf.facility_name like "%'.$data['search_key'].'%"))');
            }
            if(isset($data['sector_id']))
                $this->db->where('cp.project_main_sector_id',$data['sector_id']);
            if(isset($data['status']))
                $this->db->where('cp.project_status',$data['status']);
            $query = $this->db->get();
            $subQuery1 = $this->db->last_query();
            $query->result_array();

            $this->db->select('id_collateral,collateral_code,(NULL)as facility_name, 0 as loan_amount, "" as project_title,u.first_name as username,ct.collateral_type_name,DATE_FORMAT(c.created_date_time,"%c/%d/%Y") as created_date,v.collateral_stage_field_value as latest_value');
            $this->db->from('collateral c');
            $this->db->join('user u','u.id_user=c.created_by','left');
            $this->db->join('collateral_type ct','ct.id_collateral_type = c.collateral_type_id','left');
            $this->db->join('(select csfd.collateral_id,csfd.collateral_stage_field_value from collateral_stage_field csf
                        LEFT JOIN collateral_stage_field_data csfd on csf.id_collateral_stage_field = csfd.collateral_stage_field_id
                        WHERE csf.collateral_stage_filed_name = \'valuation_date\')v','v.collateral_id = c.id_collateral','left');
            $this->db->where('project_facility_id is NULL');
            $query = $this->db->get();
            $subQuery2 = $this->db->last_query();
            $query->result_array();


            $this->db->from("($subQuery1 UNION All $subQuery2)tmp");
            $this->db->order_by('id_collateral','DESC');
            if(isset($data['offset']) && isset($data['limit']))
                $this->db->limit($data['limit'],$data['offset']);

            $query = $this->db->get();
        }
        else
        {
            /*
            $this->db->select('c.id_collateral,c.collateral_code,pf.facility_name,pf.loan_amount,cp.project_title,u.first_name as username');
            $this->db->from('collateral c');
            $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id');
            $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id');
            $this->db->join('user u','u.id_user=c.created_by');*/


            $this->db->select('c.id_collateral,c.collateral_code,u.first_name as username,ct.collateral_type_name,DATE_FORMAT(c.created_date_time,"%b %d %Y") as created_date,pf.facility_name,cp.project_status,v.collateral_stage_field_value as latest_value');
            $this->db->from('collateral c');
            $this->db->join('collateral_type ct','ct.id_collateral_type = c.collateral_type_id','left');
            $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id','left');
            $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id','left');
            $this->db->join('(select csfd.collateral_id,csfd.collateral_stage_field_value from collateral_stage_field csf
                                LEFT JOIN collateral_stage_field_data csfd on csf.id_collateral_stage_field = csfd.collateral_stage_field_id
                                WHERE csf.collateral_stage_filed_name = "valuation_date") v','v.collateral_id = c.id_collateral','left');
            $this->db->join('user u','u.id_user=c.created_by','left');

            if(isset($data['company_id']))
                $this->db->where('cp.company_id', $data['company_id']);

            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('((cp.project_title like "%'.$data['search_key'].'%") or (c.collateral_code like "%'.$data['search_key'].'%") or (pf.facility_name like "%'.$data['search_key'].'%"))');
            }

            if(isset($data['sector_id']))
                $this->db->where('cp.project_main_sector_id',$data['sector_id']);

            if(isset($data['status']))
                $this->db->where('cp.project_status',$data['status']);

            if(isset($data['offset']) && isset($data['limit']))
                $this->db->limit($data['limit'],$data['offset']);

            $this->db->order_by('c.id_collateral','DESC');

            $query = $this->db->get();

        }

       /* echo $this->db->last_query();
        exit;*/
        return $query->result_array();
    }

    public function getCollateralProjectCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('collateral c');
        $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id');
        $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id');
        $this->db->join('user u','u.id_user=c.created_by');
        if(isset($data['company_id']))
            $this->db->where('cp.company_id', $data['company_id']);

        if(isset($data['status']))
            $this->db->where('cp.project_status',$data['status']);

        $this->db->order_by('c.id_collateral','DESC');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProductTermItemDataMaxItemOrder($data)
    {
        $this->db->select('max(item_order) as item_order');
        $this->db->from('product_term_item_data');
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id', $data['crm_project_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormComments($data)
    {
        $this->db->select('fc.comment,CONCAT(u.first_name," ",u.last_name) as user_name,u.profile_image,ar.approval_name,fc.created_date_time,DATE_FORMAT(fc.created_date_time,"%d %b %y") as date');
        $this->db->from('form_comments fc');
        $this->db->join('user u', 'u.id_user = fc.comment_by', 'left');
        $this->db->join('company_user cu', 'cu.user_id = fc.comment_by', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');

        if(isset($data['form_key']))
            $this->db->where('form_key', $data['form_key']);
        if(isset($data['crm_project_id']))
            $this->db->where('reference_type', $data['reference_type']);
        if(isset($data['reference_type']))
            $this->db->where('reference_type', $data['reference_type']);
        if(isset($data['reference_id']))
            $this->db->where('reference_id', $data['reference_id']);
        if(isset($data['project_stage_section_form_id']))
            $this->db->where('fc.project_stage_section_form_id', $data['project_stage_section_form_id']);

        $this->db->order_by('id_form_comments','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormCommentsCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('form_comments fc');

        if(isset($data['form_key']))
            $this->db->where('form_key', $data['form_key']);
        if(isset($data['crm_project_id']))
            $this->db->where('reference_type', $data['reference_type']);
        if(isset($data['reference_id']))
            $this->db->where('reference_id', $data['reference_id']);

        $this->db->order_by('id_form_comments','DESC');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data[0]['total'];
    }

    public function addFormComments($data)
    {
        $this->db->insert('form_comments',$data);
        return $this->db->insert_id();
    }



    public function getFacilityCollateral($data)
    {
        $this->db->select('*');
        $this->db->from('facility_collateral fc');
        $this->db->join('project_facility pf','fc.facility_id=pf.id_project_facility','left');
        $this->db->join('collateral c','fc.collateral_id=c.id_collateral','left');
        if(isset($data['facility_id']))
            $this->db->where('fc.facility_id',$data['facility_id']);
        if(isset($data['collateral_id']))
            $this->db->where('fc.collateral_id',$data['collateral_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addFacilityCollateral($data)
    {
        $this->db->insert('facility_collateral',$data);
        return $this->db->insert_id();
    }

    public function getCollateralDocument($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_document');
        $this->db->where('collateral_id',$data['collateral_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralDocument($data)
    {
        $this->db->insert('collateral_document',$data);
        return $this->db->insert_id();
    }

    public function updateCollateralDocument($data)
    {
        $this->db->where('id_collateral_document',$data['id_collateral_document']);
        $this->db->update('collateral_document',$data);
        return 1;
    }

    public function getExistingFacility($data)
    {
        $this->db->select('*');
        $this->db->from('project_existing_facility');
        $this->db->where('facility_id',$data['facility_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addExistingFacility($data)
    {
        $this->db->insert('project_existing_facility',$data);
        return $this->db->insert_id();
    }

    public function updateExistingFacility($data)
    {
        $this->db->where('id_project_existing_facility',$data['id_project_existing_facility']);
        $this->db->update('project_existing_facility',$data);
        return 1;
    }

    public function deleteExistingFacility($data)
    {
        $this->db->where('id_project_existing_facility',$data['id_project_existing_facility']);
        $this->db->delete('project_existing_facility');
        return 1;
    }

    public function getExistingFacilitiesByProjectId($data)
    {
        $this->db->select('pf.id_project_facility,CONCAT("Term Loan",\' \',pf.expected_maturity,\',\',"Start",\' \',DATE_FORMAT(pf.expected_start_date,"%c/%d"),\',\',"Interest",\' \',pf.expected_interest_rate,\'\',"%") as details,c.currency_code,pf.loan_amount as notional,pf.loan_amount as outstanding,IF(ISNULL(pef.facility_id),"no","yes") as status');
        $this->db->from('project_facility pf');
        $this->db->join('crm_project cp','pf.crm_project_id=cp.id_crm_project','left');
        $this->db->join('project_existing_facility pef','pf.id_project_facility=pef.facility_id','left');
        $this->db->join('currency c','cp.currency_id=c.id_currency','left');

        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);

        if(isset($data['id_project_facility']))
            $this->db->where('pf.id_project_facility',$data['id_project_facility']);

        if(isset($data['type']) && $data['type']=='pending')
            $this->db->where('pf.id_project_facility not in (select facility_id from facility_collateral)');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalComments($data)
    {
        $this->db->select('cacc.credit_committee_name as committee_name,CONCAT(u.first_name,\' \',u.last_name) as username,u.profile_image,ar.approval_name,pa.approval_status,pa.approval_comments as comments');
        $this->db->from('project_approval pa');
        $this->db->join('user u','pa.forwarded_by=u.id_user','left');
        $this->db->join('company_approval_credit_committee cacc','cacc.id_company_approval_credit_committee=pa.company_approval_credit_committee_id','left');
        $this->db->join('company_user cu','pa.forwarded_by=cu.user_id','left');
        $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
        $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
        if(isset($data['crm_project_id']))
            $this->db->where('project_id',$data['crm_project_id']);
        $this->db->order_by('pa.id_project_approval','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFormDocument($data)
    {
        $this->db->select('c.*');
        $this->db->from('crm_document c');
        if(isset($data['module_type']))
            $this->db->where('c.module_type',$data['module_type']);
        if(isset($data['group_id']))
            $this->db->where('c.uploaded_from_id',$data['group_id']);
        if(isset($data['form_key']))
            $this->db->where('c.form_key',$data['form_key']);
        if(isset($data['field_key']))
            $this->db->where('c.field_key',$data['field_key']);

        $this->db->order_by('c.id_crm_document','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentVersion($data)
    {
        $this->db->select('c.*,DATE(c.created_date_time) as date,CONCAT(u.first_name,\' \',u.last_name) as username');
        $this->db->from('crm_document_version c');
        $this->db->join('user u','c.uploaded_by=u.id_user','left');
        if(isset($data['crm_document_id']))
            $this->db->where('crm_document_id', $data['crm_document_id']);
        if(isset($data['id_crm_document_version']))
            $this->db->where('id_crm_document_version', $data['id_crm_document_version']);
        $this->db->order_by('c.id_crm_document_version','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function deleteDocument($data)
    {
        $this->db->where('id_crm_document',$data['id_crm_document']);
        $this->db->delete('crm_document');
        return 1;
    }

    public function deleteDocumentVersion($data)
    {
        if(isset($data['id_crm_document_version']))
            $this->db->where('id_crm_document_version',$data['id_crm_document_version']);
        else if(isset($data['crm_document_id']))
            $this->db->where('crm_document_id',$data['crm_document_id']);

        $this->db->delete('crm_document_version');
        return 1;
    }

    public function getFormDetails($data)
    {
        $this->db->select('*');
        $this->db->from('form f');
        if(isset($data['id_form']))
            $this->db->where('id_form', $data['id_form']);
        if(isset($data['form_key']))
            $this->db->where('form_key', $data['form_key']);
        if(isset($data['section_id']))
            $this->db->where('section_id', $data['section_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectFacilityData($data)
    {
        /*$this->db->insert('project_facility_field_data',$data);
        return $this->db->insert_id();*/
        $this->db->insert_batch('project_facility_field_data', $data);
        return $this->db->insert_id();
    }

    public function updateProjectFacilityData($data)
    {
        $this->db->update_batch('project_facility_field_data', $data, 'id_project_facility_field_data');
        return 1;
    }

    public function getProjectFacilityFieldData($data)
    {
        $this->db->select('pffd.*,ff.*,cff.payment_type,cff.id_company_facility_field');
        /*$this->db->from('project_facility_field_data pffd');
        $this->db->join('facility_field ff','pffd.facility_field_id=ff.id_facility_field','left');*/
        $this->db->from('facility_field ff');
        /*if(isset($data['id_project_facility_field_data']))
            $this->db->join('project_facility_field_data pffd','ff.id_facility_field=pffd.facility_field_id and pffd.id_project_facility_field_data='.$data['id_project_facility_field_data'],'left');
        else */if(isset($data['project_facility_id'])) {
            $this->db->join('project_facility_field_data pffd', 'ff.id_facility_field=pffd.facility_field_id and pffd.project_facility_id=' . $data['project_facility_id'], 'left');
            $this->db->group_by('pffd.id_project_facility_field_data');
        }
        else
            $this->db->join('project_facility_field_data pffd','ff.id_facility_field=pffd.facility_field_id','left');
        $this->db->join('project_facility pf','pffd.project_facility_id=pf.id_project_facility','left');
        $this->db->join('company_facility_field cff','pf.company_facility_id=cff.company_facility_id and pffd.facility_field_id=cff.facility_field_id','left');
        if(isset($data['id_project_facility_field_data']))
            $this->db->where('pffd.id_project_facility_field_data', $data['id_project_facility_field_data']);
        /*if(isset($data['project_facility_id']))
            $this->db->where('pffd.project_facility_id', $data['project_facility_id']);*/
        if(isset($data['facility_field_id']))
            $this->db->where('pffd.facility_field_id', $data['facility_field_id']);
        if(isset($data['field_section']))
            $this->db->where('ff.field_section', $data['field_section']);
        //$this->db->group_by('pffd.id_project_facility_field_data');
        $query = $this->db->get();
        //echo $this->db->last_query().'-------------------------------------';
        return $query->result_array();
    }

    public function getProjectFacility($data)
    {
        $this->db->select('*');
        $this->db->from('project_facility pf');

        if(isset($data['id_project_facility']))
            $this->db->where('pf.id_project_facility', $data['id_project_facility']);
        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id', $data['crm_project_id']);
        if(isset($data['company_facility_id']))
            $this->db->where('pf.company_facility_id', $data['company_facility_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateCompanyAssessmentStep($data)
    {
        $this->db->where('id_company_assessment_item_step',$data['id_company_assessment_item_step']);
        $this->db->update('company_assessment_item_step',$data);
        return 1;
    }

    public function getFacilityTransactionList($contract_id)
    {
        $this->db->select('contract_id,DATE(due_date) as due_date,principal,interest_due');
        $this->db->from('facility_loan_repayment');
        $this->db->where('contract_id',$contract_id);
        $this->db->order_by('due_date','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateContact($data)
    {
        $this->db->where('id_crm_contact',$data['id_crm_contact']);
        $this->db->update('crm_contact',$data);
        return 1;
    }

    public function updateCompany($data)
    {
        $this->db->where('id_crm_company',$data['id_crm_company']);
        $this->db->update('crm_company',$data);
        return 1;
    }

    public function getRisk($data)
    {
        $this->db->select('c.*,cc.company_name,s.sector_name as sector,s1.sector_name as sub_sector,DATE_FORMAT(c.created_date_time,"%d/%m/%Y") as date,cur.currency_code,mc.child_name as focus_area_name');
        $this->db->from('crm_project c');
        $this->db->join('master_child mc','c.focus_area=mc.id_child','left');
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        $this->db->join('project_company pc','pc.crm_project_id=c.id_crm_project and pc.project_company_status=1','left');
        $this->db->join('crm_company cc','pc.crm_company_id=cc.id_crm_company','left');
        $this->db->join('currency cur','c.currency_id=cur.id_currency','left');
        $this->db->join('risk_assessment ra','ra.crm_project_id=c.id_crm_project','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['risk_name']) && $data['risk_name']!='')
            $this->db->where('ra.risk_type',$data['risk_name']);

        if(isset($data['child_name']) && $data['child_name']!='')
            $this->db->where('ra.risk_sub_type1',$data['child_name']);

        if(isset($data['sub_child_name']) && $data['sub_child_name']!='')
            $this->db->where('ra.risk_sub_type2',$data['sub_child_name']);

        if(isset($data['risk_grade']) && $data['risk_grade']!='')
            $this->db->where('ra.risk_grade',$data['risk_grade']);

        if(isset($data['country_id']) && !empty($data['country_id'])) {
            $this->db->where_in('c.country_id ',$data['country_id']);
        }
        if(isset($data['focus_area']) && !empty($data['focus_area']))
            $this->db->where_in('c.focus_area ',$data['focus_area']);

        if(isset($data['sector']) && !empty($data['sector']))
            $this->db->where_in('c.project_main_sector_id ',$data['sector']);

        if(isset($data['offset']) && isset($data['limit']))
            $this->db->limit($data['limit'],$data['offset']);

        $this->db->where('c.project_status !=','deleted');
        $this->db->group_by('c.id_crm_project');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTotalRisk($data)
    {
        $this->db->select('count(*) as total_records');
        $this->db->from('crm_project c');
        $this->db->join('risk_assessment ra','ra.crm_project_id=c.id_crm_project','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['risk_name']) && $data['risk_name']!='')
            $this->db->where('ra.risk_type',$data['risk_name']);

        if(isset($data['child_name']) && $data['child_name']!='')
            $this->db->where('ra.risk_sub_type1',$data['child_name']);

        if(isset($data['sub_child_name']) && $data['sub_child_name']!='')
            $this->db->where('ra.risk_sub_type2',$data['sub_child_name']);

        if(isset($data['risk_grade']) && $data['risk_grade']!='')
            $this->db->where('ra.risk_grade',$data['risk_grade']);

        if(isset($data['country_id']) && !empty($data['country_id'])) {
            $this->db->where_in('c.country_id ',$data['country_id']);
        }
        if(isset($data['focus_area']) && !empty($data['focus_area']))
            $this->db->where_in('c.focus_area ',$data['focus_area']);

        if(isset($data['sector']) && !empty($data['sector']))
            $this->db->where_in('c.project_main_sector_id ',$data['sector']);


        $this->db->where('c.project_status !=','deleted');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getInternalRatingGrade($data=array())
    {
        $this->db->select('*');
        $this->db->from('internal_rating');
        if(isset($data['rating'])) {
            $this->db->where('value_from <=', $data['rating']);
            $this->db->where('value_to >', $data['rating']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectChecklistByFacilityId($data)
    {
        $this->db->select('aq.assessment_question');
        $this->db->from('project_checklist_facility pcf');
        $this->db->join('project_checklist pc','pcf.project_facility_id = pc.id_project_checklist','left');
        $this->db->join('assessment_question aq','pc.assessment_question_id = aq.id_assessment_question','left');
        $this->db->where('pcf.project_facility_id',$data['project_facility_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDownloadedFile($data){
        if(isset($data['id'])) {
            $this->db->select('cd.document_source, cd.document_name');
            $this->db->from('crm_document cd');
            $this->db->where('cd.id_crm_document',$data['id']);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function getContactDetails($data)
    {
        $this->db->select('*');
        $this->db->from('crm_contact c');
        $this->db->where('contact_status',1);
        if(isset($data['id_crm_contact']))
            $this->db->where('id_crm_contact',$data['id_crm_contact']);
        if(isset($data['verification_code']))
            $this->db->where('verification_code',$data['verification_code']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyDetails($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company c');
        $this->db->where('company_status',1);
        if(isset($data['id_crm_company']))
            $this->db->where('id_crm_company',$data['id_crm_company']);
        if(isset($data['verification_code']))
            $this->db->where('verification_code',$data['verification_code']);
        $query = $this->db->get();
        return $query->result_array();
    }

    /* * * 24-05-2017 * * */

    public function addNewRecord($TABLE_NAME,$TABLE_DATA){
        $this->db->insert($TABLE_NAME, $TABLE_DATA);
        return $this->db->insert_id();
    }

    public function addNewRecordBatch($TABLE_NAME,$TABLE_DATA){
        $this->db->insert_batch($TABLE_NAME, $TABLE_DATA);
        return $this->db->insert_id();
    }

    public function updateRecord($TABLE_NAME, $TABLE_DATA,$COLUMN_NAME, $COLUMN_VALUE){
        $this->db->where($COLUMN_NAME, $COLUMN_VALUE);
        $this->db->update($TABLE_NAME, $TABLE_DATA);
        return true;
    }

    public function updateRecordWhere($TABLE_NAME, $TABLE_DATA,$WHERE){
        $this->db->where($WHERE);
        $this->db->update($TABLE_NAME, $TABLE_DATA);
        return true;
    }

    public function deletRecord($TABLE_NAME, $COLUMN_NAME, $COLUMN_VALUE){
        $this->db->where($COLUMN_NAME, $COLUMN_VALUE);
        $this->db->delete($TABLE_NAME);
    }

    public function checkRecord($TABLE_NAME, $WHERE){
        $this->db->select('*');
        $this->db->from($TABLE_NAME);
        $this->db->where($WHERE);
        $query = $this->db->get();
        return $query->result_array();
    }




    public function getContactPersonalInformation($data){
        $this->db->select('*, IFNULL(martial_status,"") martial_status,IFNULL(mc1.child_name,"") martialInfo,mc2.child_name as educationInfo, IFNULL(mc3.child_name,"") occupationInfo,c1.currency_code monthly_salary_currency_info,c2.currency_code monthly_expenses_currency_info');
        $this->db->from('contact_personal_information cpi');
        $this->db->join('master_child mc1','mc1.id_child=cpi.martial_status','left');
        $this->db->join('master_child mc2','mc2.id_child=cpi.education','left');
        $this->db->join('master_child mc3','mc3.id_child=cpi.occupation','left');
        $this->db->join('currency c1','c1.id_currency=cpi.monthly_salary_currency_id','left');
        $this->db->join('currency c2','c2.id_currency=cpi.monthly_expenses_currency_id','left');
        $this->db->where('cpi.company_id',$data['crm_company_id']);
        $this->db->where('cpi.contact_id',$data['crm_contact_id']);
        $this->db->where('status',1);
        $query = $this->db->get();
        $resultParent = $query->row_array();

        return $resultParent;
    }

    public function getContactPersonalInfo($data){
        $this->db->select('cc.*');
        $this->db->from('contact_parent_children_relation cpcr');
        $this->db->join('crm_contact cc','cc.id_crm_contact=cpcr.contact_children_id');
        $this->db->where('contact_personal_information_id',$data['crm_contact_id']);
        //$this->db->where('cc.status',1);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function getReferenceGuarantor($data){
        $this->db->select('*');
        $this->db->from('contact_refrence_guarantor');
        $this->db->where('company_id',$data['crm_company_id']);
        $this->db->where('contact_id',$data['crm_contact_id']);
        $this->db->where('status',1);
        $query = $this->db->get();
        $result = $query->row_array();
        //echo $this->db->last_query();exit;
        /*if(!empty($resultParent)){
            $this->db->select('cc.*');
            $this->db->from('contact_children cc');
            $this->db->join('contact_parent_children_relation cpcr','cpcr.contact_children_id=cc.Id_contact_children');
            $this->db->join('contact_personal_information cpi','cpi.id_contact_personal_information=cpcr.contact_personal_information_id');
            $this->db->where('cpi.company_id',$data['crm_company_id']);
            $this->db->where('cpi.contact_id',$data['crm_contact_id']);
            $this->db->where('cpi.status',1);
            $query = $this->db->get();
            $resultChildren = $query->result_array();
            $resultParent['children'] = $resultChildren;
        }*/
        return $result;
    }

    public function getContactAssetsAndLiabilities($data){
        $result = array();
        $this->db->select('cta.*,mc.child_name type_name,cr.currency_code,co.collateral_number');
        $this->db->from('contact_tangible_assets cta');
        $this->db->join('master_child mc','mc.id_child=cta.type_of_asset');
        $this->db->join('currency cr','cr.id_currency=cta.currency_id','LEFT');
        $this->db->join('collateral co','co.id_collateral=cta.collateral_id','LEFT');
        $this->db->where('cta.company_id',$data['crm_company_id']);
        $this->db->where('cta.contact_id',$data['crm_contact_id']);
        $this->db->where('cta.status',1);
        $query = $this->db->get();
        $resultAssets = $query->result_array();
        $result['assets']  = $resultAssets;

        $this->db->select('cl.*,mc.child_name type_name,cr.currency_code');
        $this->db->from('contact_liabilities cl');
        $this->db->join('master_child mc','mc.id_child=cl.type_of_liabilities');
        $this->db->join('currency cr','cr.id_currency=cl.currency_id','LEFT');
        $this->db->where('cl.company_id',$data['crm_company_id']);
        $this->db->where('cl.contact_id',$data['crm_contact_id']);
        $this->db->where('cl.status',1);
        $query = $this->db->get();
        $resultLiabilities = $query->result_array();
        $result['liabilities']  = $resultLiabilities;
        return $result;
    }
    public function getContactIncomeAndExpenses($data){
        $result = array();
        $income=array();
        $expense=array();
        $this->db->select('cie.*,cr.*,mc.child_name as income_expense_type_master');
        $this->db->from('contact_income_expense cie');
        $this->db->join('currency cr','cr.id_currency=cie.currency_id','LEFT');
        $this->db->join('master_child mc','mc.id_child=cie.income_expense_type','LEFT');
        $this->db->where('cie.company_id',$data['crm_company_id']);
        $this->db->where('cie.contact_id',$data['crm_contact_id']);
        $this->db->where('cie.status',1);
        $query = $this->db->get();
        $resultAssets = $query->result_array();
        foreach($resultAssets as $k=>$v){
            if($v['type']=='income')
                $income[]=$v;
            if($v['type']=='expense')
                $expense[]=$v;
        }
        $result['income']  = $income;
        $result['expenses']  = $expense;
        return $result;
    }

    function getContactRelationSearch($data){

        if ((isset($data['relation_type']) && !empty($data['relation_type']))){
            $relationType = $this->checkRecord('master_child', array('child_key' => $data['relation_type']));
            $records = $this->checkRecord('crm_contact_relation', array('status' => 1, 'relation_id' => $relationType[0]['id_child'], 'parent_contact_id' => $data['crm_contact_id']));
            $existing_ids = array_values(array_map(function ($i) {return $i['crm_contact_id'];}, $records));
        }
        $this->db->select('cc.id_crm_contact,cc.first_name,cc.last_name,cc.email,cc.crm_contact_type_id,cct.crm_contact_type_name');
        $this->db->from('crm_contact cc');
        $this->db->join('crm_contact_type cct', 'cc.crm_contact_type_id=cct.id_crm_contact_type');
        if (isset($data['crm_contact_id']) && !empty($data['crm_contact_id'])) {
            $this->db->where_not_in('cc.id_crm_contact', $data['crm_contact_id']);
            $this->db->where('cc.id_crm_contact NOT IN (SELECT crm_contact_id FROM crm_contact_relation where parent_contact_id='.$data['crm_contact_id'].' and status=1)');

        }
        if (isset($data['search_key']))
            $this->db->where('( cc.first_name like "%' . $data['search_key'] . '%" OR cc.last_name like "%' . $data['search_key'] . '%" OR cc.email like "%' . $data['search_key'] . '%" OR concat_ws(" ",cc.first_name,cc.last_name) like "%' . $data['search_key'] . '%"  )');

        if ((isset($data['relation_type']) && !empty($data['relation_type'])) && count($existing_ids)>0)
            $this->db->where_not_in('cc.id_crm_contact', $existing_ids);

        $this->db->where('cc.contact_status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function getExistingContactRelation($data){
        $this->db->select('crm_contact_id as contact_id');
        $this->db->from('crm_contact_relation');
        $this->db->where('parent_contact_id',$data['crm_contact_id']);
        $this->db->where('status',1);
        $query = $this->db->get();
        $subQuery1 = $this->db->last_query();
        $query->result_array();

        $this->db->select('parent_contact_id as contact_id');
        $this->db->from('crm_contact_relation');
        $this->db->where('crm_contact_id',$data['crm_contact_id']);
        $query = $this->db->get();
        $subQuery2 = $this->db->last_query();
        $query->result_array();

        $query = $this->db->query("$subQuery1 UNION All $subQuery2");
        return $query->result_array();
    }

    public function getContactRelationInformation($data){
        $this->db->select('cc.id_crm_contact,cc.first_name,cc.last_name,cc.email,ccd.form_field_value');
        $this->db->from('crm_contact cc');
        $this->db->join("crm_contact_data ccd','ccd.contact_id=cc.id_crm_contact");
        $this->db->join("form_field ff','ff.id_form_field=ccd.form_field_id");
        $this->db->where('ff.field_name','client_date_of_birth');
        $this->db->where('cc.id_crm_contact',$data['crm_contact_id']);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    function getRelationshipContacts($data=array()){
        $data['child_id']=explode(',',$data['child_id']);
        $this->db->select('crm_contact_id as contact_id,id_crm_contact_relation,relation_id,comments');
        $this->db->from('crm_contact_relation');
        $this->db->where('parent_contact_id',$data['crm_contact_id']);
        if(!in_array('all',$data['child_id'])){
            $this->db->where_in('relation_id',$data['child_id']);
        }


        $this->db->where('status',1);
        $this->db->order_by('id_crm_contact_relation','desc');
        $query = $this->db->get();
        /*$subQuery1 = $this->db->last_query();
        $query->result_array();

        $this->db->select('parent_contact_id as contact_id');
        $this->db->from('crm_contact_relation');
        $this->db->where('crm_contact_id',$data['crm_contact_id']);
        $this->db->where('relation_id',$data['child_id']);
        $query = $this->db->get();
        $subQuery2 = $this->db->last_query();
        $query->result_array();

        $query = $this->db->query("$subQuery1 UNION All $subQuery2");*/
        return $query->result_array();
    }
    /*public function checkSpouse($data){
        $this->db->select('id_crm_contact_relation');
        $this->db->from('crm_contact_relation cta');
        $this->db->where('crm_contact_id',$data['crm_contact_id']);
        $this->db->or_where('parent_contact_id',$data['crm_contact_id']);
        $this->db->order_by('id_crm_contact_relation','desc');
        $query = $this->db->get();
        return $query->row_array();
    } */

    public function getMappingRelationType($data){
        $this->db->select('mc.id_child,mc.child_name,mc.child_key');
        $this->db->from('master_child mc');
        $this->db->join('master m','m.id_master=mc.master_id');
        $this->db->where('m.master_key',$data['map_type']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCurrentOutStandingMonth()
    {
        $this->db->select('DISTINCT(DATE_FORMAT(outstanding_date,"%m-%Y")) as outstanding_date');
        $this->db->from('outstanding_amount');
        $this->db->order_by('outstanding_date','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $outstanding=$query->row();
        return $outstanding->outstanding_date;
    }

    public function getProjectOutStanding($data)
    {
        $crm_project_id = $data['crm_project_id'];
        $outstanding_month = $data['outstanding_month'];
        $this->db->select('sum(oa.usd_equiv) as project_outstanding');
        $this->db->from('crm_project cp');
        $this->db->join('project_facility pf','pf.crm_project_id = cp.id_crm_project');
        $this->db->join('outstanding_amount oa','oa.contract_id = pf.contract_id and DATE_FORMAT(oa.outstanding_date,"%m-%Y") = "'.$outstanding_month.'"');
        $this->db->where('cp.id_crm_project',$crm_project_id);
        $this->db->group_by('cp.id_crm_project');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function projectWorkflowDump($data){
        $project_id=$data['project_id'];
        $product_id=$data['product_id'];
        $created_by=$data['created_by'];

        $this->db->select('pwp.*,ps.workflow_id');
        $this->db->from('product_workflow_phase pwp');
        $this->db->join('project_stage ps','ps.id_project_stage=pwp.project_stage_id');
        $this->db->where('pwp.product_id',$product_id);
        $this->db->where('pwp.workflow_phase_status',1);
        $query = $this->db->get();
        $one=$query->result_array();
        $one_copy=$one;
        $main_one=array();
        $stage_wise=array();
        foreach($one as $k=>$v){
            $id1=NULL;
            $inner=array();
            $inner['workflow_id']=$v['workflow_id'];
            $inner['workflow_phase_name']=$v['workflow_phase_name'];
            $inner['project_id']=$project_id;
            $inner['project_stage_id']=$v['project_stage_id'];
            $inner['workflow_approval_type']=$v['product_workflow_approval_type'];
            $inner['workflow_approval_process']=$v['product_workflow_approval_process'];
            //$inner['is_sub_phase']='';
            $inner['created_by']=$created_by;
            $inner['workflow_phase_status']=1;
            $inner['created_date_time']=currentDate();
            if($v['is_default']==1)
                $inner['is_start'] = 1;
            $this->db->insert('workflow_phase', $inner);
            $id1=$this->db->insert_id();
            $main_one[$v['id_product_workflow_phase']]=$id1;
            if(!isset($stage_wise[$v['project_stage_id']]))
                $stage_wise[$v['project_stage_id']]=$id1;
            //echo "<pre>";print_r($main_one);echo "</pre>";

            $inner2=array();
            $this->db->select('*');
            $this->db->from('product_workflow_phase_approver pwpa');
            $this->db->where('pwpa.product_workflow_phase_id',$v['id_product_workflow_phase']);
            $this->db->where('pwpa.status',1);
            $query = $this->db->get();
            $two=$query->result_array();
            foreach($two as $k2=>$v2){
                $inner2['workflow_phase_id']=$id1;
                if($v['is_default']==1 && $v2['approver_id']==0)
                    $v2['approver_id']=$created_by;
                $inner2['approval_role_id']=$v2['approver_id'];
                $inner2['created_date_time']=currentDate();
                $this->db->insert('workflow_phase_role', $inner2);
            }
        }
        foreach($one_copy as $kc=>$vc){
            $inner3=array();
            $this->db->select('*');
            $this->db->from('product_workflow_phase_action pwpa');
            $this->db->where('pwpa.product_workflow_phase_id',$vc['id_product_workflow_phase']);
            $this->db->where('pwpa.status',1);
            $query = $this->db->get();
            $three=$query->result_array();
            foreach($three as $k3=>$v3){
                if(isset($main_one[$v3['product_workflow_phase_id']])) {
                    $inner3['workflow_phase_id'] = $main_one[$v3['product_workflow_phase_id']];
                    $inner3['workflow_action_id'] = $v3['workflow_action_id'];
                    $inner3['next_project_stage_id'] = $v3['next_project_stage_id'];
                    $inner3['next_project_stage_assign_type'] = $v3['next_project_stage_assign_type'];
                    $inner3['next_project_stage_assign_to'] = $v3['next_project_stage_assign_to'];
                    if (isset($main_one[$v3['forward_product_workflow_phase_id']])) {
                        $inner3['workflow_phase_activity_id'] = $main_one[$v3['forward_product_workflow_phase_id']];
                    } else {
                        $inner3['workflow_phase_activity_id'] = 0;
                    }

                    $inner3['created_by'] = $created_by;
                    $inner3['created_date_time'] = currentDate();
                    $this->db->insert('workflow_phase_action', $inner3);
                }
            }

            //

        }
        //start actions
        /*$stages=$this->getProjectStages();
        foreach($stages as $k=>$v) {
            if (isset($stage_wise[$v['id_project_stage']])){
                $inner = '';
                $inner['workflow_id'] = $v['workflow_id'];
                $inner['workflow_phase_name'] = $v['stage_name'].' - Start Workflow';
                $inner['project_id'] = $project_id;
                $inner['project_stage_id'] = $v['id_project_stage'];
                $inner['workflow_approval_type'] = 'User';
                $inner['workflow_approval_process'] = 'Parallel';
                //$inner['is_sub_phase']='';
                $inner['created_by'] = $created_by;
                $inner['workflow_phase_status'] = 1;
                $inner['created_date_time'] = currentDate();
                $inner['is_start'] = 1;
                $this->db->insert('workflow_phase', $inner);
                $id1 = $this->db->insert_id();
                $inner2='';
                $inner2['workflow_phase_id']=$id1;
                $inner2['approval_role_id']=$created_by;
                $inner2['created_date_time']=currentDate();
                $this->db->insert('workflow_phase_role', $inner2);

                $inner3='';
                $inner3['workflow_phase_id'] = $id1;
                $inner3['workflow_action_id'] = 7;
                $inner3['workflow_phase_activity_id']=$stage_wise[$v['id_project_stage']];
                $inner3['created_by'] = $created_by;
                $inner3['created_date_time'] = currentDate();
                $this->db->insert('workflow_phase_action', $inner3);

            }

        }*/


    }
    /*For Company ownership details*/
    public function crmCompanyOwnershipList($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_shareholder_details');
        $this->db->where('crm_company_id', $data['crm_company_id']);
        if(isset($data['id_crm_company_shareholder_details']))
            $this->db->where('id_crm_company_shareholder_details', $data['id_crm_company_shareholder_details']);
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id', $data['crm_company_id']);
//        $this->db->limit('10','0');

        $query = $this->db->get();
        return $query->result_array();
    }
    public function crmCompanyOwnershipListCompleteView($data)
    {
        $this->db->select('shareholder_name as name_of_shareholder,no_of_shares as number_of_shares,percentage_share as percentage_of_shares,value_of_shares,currency,module_type as type');
        $this->db->from('crm_company_shareholder_details');
        $this->db->where('crm_company_id', $data['crm_company_id']);
        if(isset($data['id_crm_company_shareholder_details']))
            $this->db->where('id_crm_company_shareholder_details', $data['id_crm_company_shareholder_details']);
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id', $data['crm_company_id']);
//        $this->db->limit('10','0');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function isPresentCrmCompanyOwnership($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_shareholder_details');
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id', $data['crm_company_id']);
        if(isset($data['module_id']))
            $this->db->where('module_id', $data['module_id']);
        if(isset($data['module_type']))
            $this->db->where('module_type', $data['module_type']);
        if(isset($data['shareholder_email_id']))
            $this->db->where('shareholder_email_id', $data['shareholder_email_id']);
        if(isset($data['shareholder_name']))
            $this->db->where('shareholder_name', $data['shareholder_name']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function saveCrmShareholderData($data)
    {
        $this->db->insert('crm_company_shareholder_details', $data);
        return $this->db->insert_id();
    }

    public function updateCrmShareholderData($data)
    {
        $this->db->where('id_crm_company_shareholder_details',$data['id_crm_company_shareholder_details']);
        $this->db->update('crm_company_shareholder_details',$data);
        return 1;
    }

    public function deleteCrmCompanyOwnership($data)
    {
        $this->db->where('id_crm_company_shareholder_details',$data['id_crm_company_shareholder_details']);
        $this->db->delete('crm_company_shareholder_details');
        return 1;
    }

    public function getCompanyFormDataByFieldName($data){
        if(isset($data['crm_company_id'])) {
            $this->db->select('*');
            $this->db->from('crm_company_data ccd');
            $this->db->join('form_field ff','ccd.form_field_id=ff.id_form_field');
            $this->db->join('form f','ff.form_id=f.id_form');
            $this->db->where('f.form_name',$data['form_name']);
            $this->db->where('ff.field_name',$data['field_name']);
            $this->db->where('ccd.crm_company_id',$data['crm_company_id']);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facility_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }

    public function getFacilityList($data)
    {
        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.project_title,pf.*,cc.company_name,cn.country_name as project_country_name,,cn1.country_name as company_country_name,cb.legal_name,CONCAT(u.first_name, " ", u.last_name) as username,mc.child_name as facility_type_name');
        $this->db->from('crm_project c');
        $this->db->join('project_facility pf','pf.crm_project_id=c.id_crm_project','left');
        $this->db->join('project_company pc','pc.crm_project_id=c.id_crm_project and pc.project_company_status=1','left');
        $this->db->join('crm_company cc','pc.crm_company_id=cc.id_crm_company','left');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        $this->db->join('country cn1','cc.country_id=cn1.id_country','left');
        $this->db->join('master_child mc','pf.facility_type=mc.id_child','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['created_by']))
            $this->db->where('c.created_by ',$data['created_by']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(pf.project_facility_name like "%'.$data['search_key'].'%" or pf.contract_id like "%'.$data['search_key'].'%" )');
        }
        if(isset($data['assigned_to'])){
            /*$this->db->join('project_stage_info psi','c.id_crm_project=psi.project_id','left');
            $this->db->join('project_stage_workflow_phase pswp','psi.id_project_stage_info=pswp.project_stage_info_id','left');
            $this->db->where('pswp.assigned_to',$data['assigned_to']);*/
        }

        if ($this->current_user) {
            $this->db->where_in('c.created_by ', $user_list);
        }

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->where('pf.id_project_facility IS NOT NULL');
        $this->db->group_by('pf.id_project_facility');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilityListCount($data)
    {
        $user_list = array();
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }
        $this->db->select('count(DISTINCT(pf.id_project_facility)) as total');
        $this->db->from('crm_project c');
        $this->db->join('project_facility pf','pf.crm_project_id=c.id_crm_project','left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        if(isset($data['created_by']))
            $this->db->where('c.created_by ',$data['created_by']);
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(pf.project_facility_name like "%'.$data['search_key'].'%" or pf.contract_id like "%'.$data['search_key'].'%" )');
        }
        if(isset($data['assigned_to'])){
            /*$this->db->join('project_stage_info psi','c.id_crm_project=psi.project_id','left');
            $this->db->join('project_stage_workflow_phase pswp','psi.id_project_stage_info=pswp.project_stage_info_id','left');
            $this->db->where('pswp.assigned_to',$data['assigned_to']);*/
        }

        if ($this->current_user) {
            $this->db->where_in('c.created_by ', $user_list);
        }

        $this->db->where('pf.id_project_facility IS NOT NULL');
        $this->db->group_by('pf.id_project_facility');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyFacilities($data)
    {
        $this->db->select('pf.*,c.currency_code,ft.child_name as facility_type_name,sum(fd.usd_equiv) as disbursed_amount');
        $this->db->from('project_facility pf');
        $this->db->join('project_company pc','pc.crm_project_id = pf.crm_project_id');
        $this->db->join('currency c','c.id_currency = pf.currency_id','left');
        $this->db->join('master_child ft','ft.id_child = pf.facility_type','left');
        $this->db->join('facility_disbursement fd','fd.contract_id = pf.contract_id','left');
        $this->db->where('pc.crm_company_id',$data['crm_company_id']);
        $this->db->where('pc.company_type_id',1);
        $this->db->group_by('pf.contract_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getClientFacilities($data)
    {
        $this->db->select('pf.*,c.currency_code,ft.child_name as facility_type_name,sum(fd.usd_equiv) as disbursed_amount');
        $this->db->from('project_facility pf');
        $this->db->join('project_contact pc','pc.crm_project_id = pf.crm_project_id');
        $this->db->join('currency c','c.id_currency = pf.currency_id','left');
        $this->db->join('master_child ft','ft.id_child = pf.facility_type','left');
        $this->db->join('facility_disbursement fd','fd.contract_id = pf.contract_id','left');
        $this->db->where('pc.crm_contact_id',$data['crm_contact_id']);
        $this->db->where('pc.contact_type_id',1);
        $this->db->group_by('pf.contract_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getClientGuaranteeFacilities($data)
    {
        $this->db->select('pf.*,co.collateral_number,c.currency_code,ft.child_name as facility_type_name,sum(fd.usd_equiv) as disbursed_amount');
        $this->db->from('project_facility pf');
        $this->db->join('project_facility_collateral pfc','pfc.project_facility_id = pf.id_project_facility and pfc.status=1');
        $this->db->join('collateral co','pfc.collateral_id = co.id_collateral','left');
        $this->db->join('collateral_contact cc','cc.collateral_id = co.id_collateral','left');
        $this->db->join('currency c','c.id_currency = pf.currency_id','left');
        $this->db->join('master_child ft','ft.id_child = pf.facility_type','left');
        $this->db->join('facility_disbursement fd','fd.contract_id = pf.contract_id','left');
        $this->db->join('collateral_type ct','ct.id_collateral_type = co.collateral_type_id','left');
        $this->db->where('cc.crm_contact_id',$data['crm_contact_id']);
        $this->db->where('ct.collateral_type_name','Personal Guarantee');
        $this->db->group_by('pf.contract_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateFacility()
    {

    }
}
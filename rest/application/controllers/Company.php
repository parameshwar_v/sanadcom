<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Company extends REST_Controller
{

    public $ordered_committee_data      = array();
    public $forward_to                  = '';
    public $ordered_data                = '';
    public $last_node                   = '';
    public $count                       = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Company_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('Project_model');
        $this->load->model('Crm_company_model');
    }

    public function companyList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->companyList($data);
        for($s=0;$s<count($result);$s++){
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
                $result[$s]['company_logo'] = getImageUrl($result[$s]['company_logo'],'company');
        }
        $total_records = $this->Company_model->totalCompanyList();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_get($id)
    {
        $data['id'] = $id;
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getCompanyById($id);
        if(!empty($result)){
            $result->profile_image = getImageUrl($result->profile_image,'profile');
            $result->company_logo = getImageUrl($result->company_logo,'company');
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records_found'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function companyInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyById($data['company_id']);

        $company = array('id_company' => $result->id_company,'company_name' => $result->company_name,'company_url' => $result->company_url,'company_logo' => getImageUrl($result->company_logo,'company'),'country_id' => $result->company_country_id,'bank_category_id' => $result->bank_category_id, 'company_address' => $result->company_address, 'currency_id' => $result->currency_id,'plan_id' => $result->plan_id);
        $user = array('id_user' => $result->id_user,'first_name' => $result->first_name,'last_name' => $result->last_name,'profile_image' => getImageUrl($result->profile_image,'user'),'email_id' => $result->email_id,'phone_number' => $result->phone_number);
        $result = array('company' => $company, 'user' => $user);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            'numeric' =>  $this->lang->line('phone_num_num'),
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-10' => $this->lang->line('phone_num_max_len'),
        );
        $companyNameRules = array(
            'required'=> $this->lang->line('company_name_req')
        );

        $this->form_validator->add_rules('email_id', $emailRules);
        $this->form_validator->add_rules('phone_number', $phoneRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('company_name', $companyNameRules);
        $this->form_validator->add_rules('company_address', array('required'=>$this->lang->line('company_addr_req')));
        $this->form_validator->add_rules('plan_id', array('required'=>$this->lang->line('plan_req')));
        $this->form_validator->add_rules('country_id', array('required'=>$this->lang->line('country_req')));
        $this->form_validator->add_rules('bank_category_id', array('required'=>$this->lang->line('bank_category_req')));
        $this->form_validator->add_rules('currency_id', array('required'=>$this->lang->line('currency_req')));
        $this->form_validator->add_rules('company_url', array('required'=>$this->lang->line('company_url_req')));
        $validated = $this->form_validator->validate(array_merge($data['user'],$data['company']));
        $error = '';
        if($validated != 1)
        {
            if($error!=''){ $validated = array_merge($error,$validated); }
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $company_id = 0;
            if(isset($data['company']['id_company'])){ $company_id = $data['company']['id_company'];  }
            $check_company_name = $this->Company_model->getCompany(array('company_name' => trim($data['company']['company_name']),'id' => $company_id));
            if(!empty($check_company_name))
            {
                $result = array('status'=>FALSE,'error'=>array('company_name' => $this->lang->line('company_name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $check_company_url = $this->Company_model->getCompany(array('company_url' => trim($data['company']['company_url']),'id' => $company_id));
            unset($company_id);
            if(!empty($check_company_url))
            {
                $result = array('status'=>FALSE,'error'=>array('company_url' => $this->lang->line('company_url_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user']['id_user']) && isset($data['company']['id_company']))
        {
            $email_check = $this->User_model->check_email($data['user']['email_id'],'user',$data['user']['id_user']);
            unset($data['user']['profile_image']); unset($data['company']['company_logo']);
        }
        else
        {
            $email_check = $this->User_model->check_email($data['user']['email_id']);
        }

        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email_id'=>$this->lang->line('email_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,'','image');

            $data['user']['profile_image'] = $imageName;

        }
        else{
            $data['user']['profile_image'] = '';
        }

        if(isset($_FILES) && !empty($_FILES['file']['name']['company_logo']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['company_logo'],$_FILES['file']['name']['company_logo'],$path,'','image');

            $data['company']['company_logo'] = $imageName;

        }
        else{
            $data['company']['company_logo']='';
        }

        $user_password = generatePassword(6);
        $data['user']['password'] = md5($user_password);
        $data['user']['user_status'] = '0';


        if(isset($data['user']['id_user']) && isset($data['company']['id_company']))
        {
            unset( $data['user']['password']); unset($data['user']['user_status']);
            if($data['user']['profile_image']==''){ unset($data['user']['profile_image']); }
            if($data['company']['company_logo']==''){ unset($data['company']['company_logo']); }

            $this->Company_model->updateUser($data['user']);
            $result = $this->Company_model->updateCompanyInfo($data['company']);
            //moving image to company folder
            if(!file_exists('uploads/'.$data['company']['id_company'])){
                mkdir('uploads/'.$data['company']['id_company']);
            }

            if(isset($data['company']['company_logo']) && $data['company']['company_logo']!='')
            {

                rename('uploads/'.$data['company']['company_logo'], 'uploads/'.$data['company']['id_company'].'/'.$data['company']['company_logo']);
                $this->Company_model->updateCompanyData(array('company_logo' => $data['company']['id_company'].'/'.$data['company']['company_logo']),$data['company']['id_company']);
            }
            if(isset($data['user']['profile_image']) && $data['user']['profile_image']!='')
            {
                rename('uploads/'.$data['user']['profile_image'], 'uploads/'.$data['company']['id_company'].'/'.$data['user']['profile_image']);
                $this->User_model->updateUserData(array('profile_image' => $data['company']['id_company'].'/'.$data['user']['profile_image']),$data['user']['id_user']);
            }


        }
        else
        {
            $data['user']['created_date_time'] = currentDate();
            $data['company']['user_id'] = $this->User_model->createUserInfo($data['user']);
            $data['company']['created_date_time'] = currentDate();
            $result = $this->Company_model->createCompany($data['company']);
            //moving image to company folder
            if(!file_exists('uploads/'.$result)){
                mkdir('uploads/'.$result);
            }
            if(isset($data['user']['profile_image']) && $data['user']['profile_image']!='')
                rename('uploads/'.$data['user']['profile_image'], 'uploads/'.$result.'/'.$data['user']['profile_image']);
            if(isset($data['user']['company_logo']) && $data['user']['company_logo']!='')
                rename('uploads/'.$data['company']['company_logo'], 'uploads/'.$result.'/'.$data['company']['company_logo']);

            $this->Company_model->updateCompanyData(array('company_logo' => $data['company']['company_logo']),$result);
            $user_id = $this->Company_model->createCompanyUser(array('company_id' => $result,'user_id' => $data['company']['user_id']));
            $this->User_model->updateUserData(array('profile_image' => $data['user']['profile_image']),$user_id);

            $message = str_replace(array('{user_name}','{email}','{password}'),array($data['user']['first_name'].' '.$data['user']['last_name'],$data['user']['email_id'],$user_password),$this->lang->line('company_creation_mail'));
            $template_data = array(
                'base_url' => REST_API_URL,
                'message' => $message,
                'activation_code' => $this->User_model->encode($data['company']['user_id']),
                'mail_footer' => $this->lang->line('mail_footer')
            );
            $template_data = $this->parser->parse('templates/company.html', $template_data);
            sendmail($data['user']['email_id'],$this->lang->line('company_creation_subject'),$template_data);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('customer_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateCompany_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        $companyNameRules = array(
            'required'=> $this->lang->line('company_name_req')
        );


        $this->form_validator->add_rules('company_name', $companyNameRules);
        $this->form_validator->add_rules('company_address', array('required'=>$this->lang->line('company_addr_req')));
        $this->form_validator->add_rules('country_id', array('required'=>$this->lang->line('country_req')));
        $this->form_validator->add_rules('bank_category_id', array('required'=>$this->lang->line('bank_category_req')));
        $this->form_validator->add_rules('company_url', array('required'=>$this->lang->line('company_url_req')));
        $validated = $this->form_validator->validate(array_merge($data['user'],$data['company']));
        $error = '';
        if($validated != 1)
        {
            if($error!=''){ $validated = array_merge($error,$validated); }
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }



    public function company_delete($id)
    {
       $data['id'] = $id;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('id', $this->lang->line('id_req'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->deleteCompany($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('company_inactive'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function users_get()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getUsers($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('company_inactive'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userList_get()
    {
        $data = $this->input->get();

        if(isset($data['id_company_approval_role']) && $data['id_company_approval_role']!=''){
            $company_approval_role = $this->Company_model->getCompanyApprovalRole(array('company_id' => $data['company_id'],'id_company_approval_role' => $data['id_company_approval_role']));
            $data['approval_role_id'] = $company_approval_role[0]['approval_role_id'];

            if(isset($data['branch_id']) && $data['branch_id']!=''){
                $reporting_branch = $this->Company_model->getReportingBranch($data['branch_id']);
                $data['allowed_branch_ids'] = array();
                array_push($data['allowed_branch_ids'],$data['branch_id']);
                unset($data['branch_id']);
                if(!empty($reporting_branch)){
                    array_push($data['allowed_branch_ids'],$reporting_branch[0]);
                }

            }
        }
        if(isset($data['approval_role_id'])){

            $reporting_approval_ids = $this->Company_model->getReportingApprovalRoles($data['company_id'],$data['approval_role_id']);
            if(!empty($reporting_approval_ids))
            {
                $data['allowed_approval_role_id'] = $reporting_approval_ids[0];
                unset($data['approval_role_id']);

                $result = $this->Company_model->getCompanyUsers($data);
            }
            else{
                $result = array();
            }

        }
        else{
            $result = $this->Company_model->getCompanyUsers($data);

            if(isset($data['type']) && $data['type']=='admin'){
                $result1 = $this->User_model->getAdminUser(array('company_id' => $data['company_id'],'user_role_id' => 2));
                $result = array_merge($result,$result1);
            }
        }

        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
            if(isset($result[$s]['reporting_user_id']))
                $result[$s]['reporting'] = $this->Company_model->getReportingUser($result[$s]['reporting_user_id']);
            if(isset($data['type']) && $data['type']=='admin' && $result[$s]['user_role_id'] == 2){
                $result[$s]['role_name'] = 'Admin';
            }
            $result[$s]['loginhistory'] = $this->Company_model->getUserLoginHistory($result[$s]['id_user']);
            //$result[$s]['status'] = 'Inactive';
            if($result[$s]['user_status']==0){ $result[$s]['status'] = 'Inactive'; }
            elseif($result[$s]['user_status']==1){ $result[$s]['status'] = 'Active'; }
            elseif($result[$s]['user_status']==2){ $result[$s]['status'] = 'Disabled'; }
            elseif($result[$s]['user_status']==3){ $result[$s]['status'] = 'Locked'; }
        }
        $total_records = $this->Company_model->getTotalCompanyUsers($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function user_get($id=0)
    {
        if($id)
        {
            $result = $this->Company_model->getCompanyUser($id);
            if(!empty($result)){
                $result->profile_image = getImageUrl($result->profile_image,'profile');
                $result->branch = $this->Company_model->getBranchName(array('company_id' => $result->company_id));
                $result->userRole = $this->Company_model->getApprovalRole(array('company_id' => $result->company_id));
                if($result->user_status==0){ $result->status = 'Inactive'; }
                elseif($result->user_status==1){ $result->status = 'Active'; }
                elseif($result->user_status==2){ $result->status = 'Disabled'; }
                elseif($result->user_status==3){ $result->status = 'Locked'; }
                $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records_found'), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else{
            $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records_found'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function user_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        //validating data
        $companyRules = array(
            'required'=> $this->lang->line('company_id_req')
        );
        $userRoleRules = array(
            'required'=> $this->lang->line('user_role_req')
        );
        $userBranchRules = array(
            'required'=> $this->lang->line('branch_req')
        );

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            /*'numeric' =>  $this->lang->line('phone_num_num'),*/
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-20' => $this->lang->line('phone_num_max_len_20'),
        );

        $this->form_validator->add_rules('email_id', $emailRules);
        $this->form_validator->add_rules('phone_number', $phoneRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('company_id', $companyRules);
        $this->form_validator->add_rules('user_role_id', $userRoleRules);
        $this->form_validator->add_rules('branch_id', $userBranchRules);
        $this->form_validator->add_rules('branch_type_id', array('required'=> $this->lang->line('branch_type_req')));
        //$this->form_validator->add_rules('reporting_user_id', array('required'=> 'reporting user required'));
        $validated = $this->form_validator->validate($data['user']);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        unset($data['user']['phone_number_isd']);
        unset($data['user']['phone_number1']);

        $email_check = $this->User_model->check_email($data['user']['email_id'],'company_user',$data['user']['company_id']);

        if(!empty($email_check)){
            if(isset($data['user']['id_user']))
            {
                if($data['user']['id_user'] !=$email_check->id_user){
                    $result = array('status'=>FALSE,'error'=>array('email_id'=>$this->lang->line('email_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $result = array('status'=>FALSE,'error'=>array('email_id'=>$this->lang->line('email_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(!file_exists('uploads/'.$data['user']['company_id'])){
            mkdir('uploads/'.$data['user']['company_id']);
        }

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,$data['user']['company_id'],'image');

            $data['user']['profile_image'] = $imageName;
        }

        $branch_id = $data['user']['branch_id'];
        $company_id =  $data['user']['company_id'];
        if(isset($data['user']['reporting_user_id']))
             $reporting_user_id =  $data['user']['reporting_user_id'];
        else $reporting_user_id=0;
        $user_role_id = $data['user']['user_role_id'];
        $id_company_user = '';

        unset($data['user']['branch_id']);
        unset($data['user']['company_id']);
        unset($data['user']['reporting_user_id']);
        unset($data['user']['reporting_user_name']);
        unset($data['user']['user_role_id']);
        unset($data['user']['branch_type_id']);
        unset($data['user']['id_branch']);

        $user_password =  generatePassword(6);
        $user_password =  generatePassword(6);

        $data['user']['user_role_id'] = 3;
        if(isset($data['user']['id_user']) || isset($data['user']['user_id']))
        {
            $data['user']['profile_image'] = str_replace(REST_API_URL.'uploads/','',$data['user']['profile_image']);

            $user_info = array(
                'id_user' => $data['user']['id_user'],
                'first_name' => $data['user']['first_name'],
                'last_name' => $data['user']['last_name'],
                'email_id' => $data['user']['email_id'],
                'phone_number' => $data['user']['phone_number'],
                'address' => $data['user']['address'],
                'city' => $data['user']['city'],
                'state' => $data['user']['state'],
                'country_id' => $data['user']['country_id'],
                'zip_code' => $data['user']['zip_code'],
                'profile_image' => $data['user']['profile_image'],
                'updated_date_time' => currentDate(),
                'user_status' => $data['user']['user_status']
            );

            $id_company_user = $data['user']['id_company_user'];
            if($data['user']['id_user']){ $user_id = $data['user']['id_user']; }else { $user_id = $data['user']['user_id']; }
            $this->User_model->updateUserInfo($user_info);
            $result = $this->Company_model->updateCompanyUser(array('id_company_user' => $id_company_user, 'company_id' => $company_id, 'user_id' => $user_id, 'company_approval_role_id' => $user_role_id,'branch_id' => $branch_id, 'reporting_user_id' => $reporting_user_id));
        }
        else{
            $data['user']['password'] = md5($user_password);
            $data['user']['created_date_time'] = currentDate();
            $user_id = $this->User_model->createUserInfo($data['user']);
            $result = $this->Company_model->createCompanyUser(array('company_id' => $company_id, 'user_id' => $user_id,'company_approval_role_id' => $user_role_id, 'branch_id' => $branch_id,'reporting_user_id' => $reporting_user_id));

            $message = str_replace(array('{first_name}','{last_name}','{email}','{password}','{login_url}'),array($data['user']['first_name'],$data['user']['last_name'],$data['user']['email_id'],$user_password,WEB_BASE_URL),$this->lang->line('user_registration_mail'));
            $template_data = array(
                'web_base_url' => WEB_BASE_URL,
                'message' => $message,
                'login_url' => WEB_BASE_URL,
                'mail_footer' => $this->lang->line('mail_footer')
            );
            $subject = $this->lang->line('user_registration_subject');
            $template_data = $this->parser->parse('templates/notification.html',$template_data);
            sendmail($data['user']['email_id'],$subject,$template_data);
        }


        if(isset($data['user']['id_user']) || isset($data['user']['user_id'])){
            $suc_msg = $this->lang->line('user_update');
        }
        else{
            $suc_msg = $this->lang->line('user_add');
        }

        /*if(isset($data['user']['password']) && $data['user']['password']!='')
        {
            $this->User_model->updateUserInfo(array(
                'id_user' => $data['user']['id_user'],
                'password' => md5($data['user']['password'])
            ));
        }*/

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function changePassword_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //validating data

        $this->form_validator->add_rules('password', array('required'=> $this->lang->line('password_req')));
        $this->form_validator->add_rules('id_user', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->User_model->updateUserData(array(
            'password' => md5($data['password']),
            'security_question_id' => '',
            'password_updated_date' => date('Y-m-d', strtotime("+100 days"))
        ),$data['id_user']);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('password_changed'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function branches_get($company_id,$branch_type=0)
    {
        $branch_type = $this->Company_model->getBranchTypeByCompanyId($company_id,$branch_type);
        if(count($branch_type) > 1)
            $branch_type = $this->getOrderedData($branch_type,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($branch_type);$s++)
        {
            $branch_type[$s]['branches'] = $this->Company_model->getCompanyBranchesBybranchId($branch_type[$s]['branch_type_id'],$company_id);

            for($r=0;$r<count($branch_type[$s]['branches']);$r++){
                $branch_type[$s]['branches'][$r]['branch_logo'] = getImageUrl($branch_type[$s]['branches'][$r]['branch_logo'],'company');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$branch_type);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getOrderedData($data,$identifier,$compare_identifier,$id_identifier)
    {
        for($t=0;$t<count($data);$t++){
            if(count($this->ordered_data)==count($data)){  break; }
            if($data[$t][$compare_identifier]==$identifier){
                $this->ordered_data[] = $data[$t];
                $identifier = $data[$t][$id_identifier];
                if(count($this->ordered_data)==count($data)){  break; }
                else{ $this->getOrderedData($data,$identifier,$compare_identifier,$id_identifier); }
            }
        }

        return $this->ordered_data;
    }

    public function branchTypeStructure_get($company_id)
    {
        $result = $this->Company_model->getCompanyBranchTypeStructure($company_id);
        if(count($result) > 1)
            $result = $this->getOrderedData($result,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['created_by']=='0'){ $result[$s]['is_edit'] = 0; }
            else{ $result[$s]['is_edit'] = 1; }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchDetails_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('branch_id', array('required' => $this->lang->line('branch_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getBranchDetails($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['branch_logo'] = getImageUrl($result[$s]['branch_logo'],'company');
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branch_get($id)
    {
        $result = $this->Company_model->getCompanyBranch($id);
        $result->branch_logo = getImageUrl($result->branch_logo,'company');
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranch_get($company_id,$branch_id='')
    {
        if($branch_id!='' && $branch_id!=0){
            $branch_details = $this->Company_model->getCompanyBranch($branch_id);
            $branch_type = $branch_details->branch_type_id;
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($company_id,$branch_type);
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }

            $result = $this->Company_model->getCompanyBranchByCompanyId($company_id,$branch_id,$allowed_branch_type_ids);
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchByCompanyId($company_id,$branch_id);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCompanyBranchListByCompanyId($data);
        for($s=0;$s<count($result);$s++)
        {
            $total_users = $this->Company_model->getCompanyBranchUsersByBranchId($result[$s]['id_branch'],$data['company_id']);
            $result[$s]['branch_users'] = $total_users->total_users;
        }
        $total_records = $this->Company_model->getTotalCompanyBranchByCompanyId($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranch_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->clearRules();
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('branch_type_id', array('required' => $this->lang->line('branch_type_req')));
        $this->form_validator->add_rules('country_id', array('required' => $this->lang->line('country_req')));
        $this->form_validator->add_rules('legal_name', array('required'=> $this->lang->line('branch_name_req')));
        $this->form_validator->add_rules('branch_address', array('required'=> $this->lang->line('address_req')));
        $this->form_validator->add_rules('branch_city', array('required'=> $this->lang->line('city_req')));
        //$this->form_validator->add_rules('branch_state', array('required'=> 'State required'));
        $this->form_validator->add_rules('branch_code', array('required'=> $this->lang->line('branch_code_req')));
        //$this->form_validator->add_rules('branch_zip_code', array('required'=> 'Zip code required'));
        //$this->form_validator->add_rules('reporting_branch_id', array('required'=> 'Reporting Branch required'));
        if(isset($data['branch']) && $data['branch']!='' && $data['branch']!=0)
            $this->form_validator->add_rules('branch_phone_number', array('min_len-6' => $this->lang->line('phone_num_min_len'),'numeric' => $this->lang->line('phone_num_num')));

        $validated = $this->form_validator->validate($data['branch']);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            if(isset($data['branch']['branch_email']) && trim($data['branch']['branch_email'])!='') {
                $company_branch_id = 0;
                if(isset($data['branch']['id_branch'])){ $company_branch_id = $data['branch']['id_branch']; }
                $check_email = $this->User_model->check_email($data['branch']['branch_email'],'company_branch',$company_branch_id);
                if(!empty($check_email)){
                    $result = array('status'=>FALSE,'error'=>array('branch_email' => $this->lang->line('email_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        unset($data['branch']['id_branch_type']);
        unset($data['branch']['branch_type_name']);

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['branch_logo']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['branch_logo'],$_FILES['file']['name']['branch_logo'],$path,$data['branch']['company_id'],'image');

            $data['branch']['branch_logo'] = $imageName;
        }

        if(isset($data['branch']['id_branch']))
        {
            if(!isset($data['branch']['reporting_branch_id']) || empty($data['branch']['reporting_branch_id'])) $data['branch']['reporting_branch_id'] =0;
            $fields = array(
                        'company_id' => $data['branch']['company_id'],
                        'branch_type_id' => $data['branch']['branch_type_id'],
                        'country_id' => $data['branch']['country_id'],
                        'legal_name' => $data['branch']['legal_name'],
                        'branch_address' => $data['branch']['branch_address'],
                        'branch_city' => $data['branch']['branch_city'],
                        'branch_state' => $data['branch']['branch_state'],
                        'branch_code' => $data['branch']['branch_code'],
                        'reporting_branch_id' => $data['branch']['reporting_branch_id'],
                        'branch_logo' => $data['branch']['branch_logo'],
                        'branch_status' => $data['branch']['branch_status'],
                        'branch_zip_code' => $data['branch']['branch_zip_code'],
                        'branch_phone_number' => $data['branch']['branch_phone_number'],
                        'branch_email' => $data['branch']['branch_email']
            );

            $branch_id = $data['branch']['id_branch'];
            unset($data['branch']['id_branch']);
            $fields['branch_logo'] = str_replace(REST_API_URL.'uploads/','',$fields['branch_logo']);
            $result = $this->Company_model->updateCompanyBranch($fields,$branch_id);
            $data['branch']['id_branch'] = $branch_id;
        }
        else
        {
            $data['branch']['created_date_time'] = currentDate();
            $result = $this->Company_model->createCompanyBranch($data['branch']);
        }
        if(isset($data['branch']['id_branch'])){
            $suc_msg = $this->lang->line('branch_update');
        }
        else{
            $suc_msg = $this->lang->line('branch_add');
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchType_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyBranchType($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchType_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_branch_type']) && (!isset($data['branch_type_name']) || $data['branch_type_name']==''))
            $this->form_validator->add_rules('id_branch_type', array('required' => $this->lang->line('branch_type_req')));
        if((!$data['is_primary'] || $data['is_primary']==false || $data['is_primary']==0 ) && ((!isset($data['reporting_branch_type_id']) || $data['reporting_branch_type_id']=='' || $data['reporting_branch_type_id']==0))) {
            unset($data['reporting_branch_type_id']);
            $this->form_validator->add_rules('reporting_branch_type_id', array('required' => $this->lang->line('reporting_branch_type_req')));
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_branch_type']) && isset($data['reporting_branch_type_id']))
        {
            if($data['id_branch_type']==$data['reporting_branch_type_id']){
                $result = array('status'=>FALSE,'error'=>array('reporting_branch_type_id' => 'You can not report to same branch type'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        if(!isset($data['branch_type_code'])){ $data['branch_type_code']=''; }
        if(!isset($data['description'])){ $data['description']=''; }

        //check branch_type_id in company_branch_type_id
        if(isset($data['id_branch_type']))
        {
            $check = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'],$data['id_branch_type']);
            if(empty($check)){
                $total_company_branch_types = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id']);
                $ids = array();
                foreach($total_company_branch_types as $item){ $ids[] = $item['reporting_branch_type_id']; }
                $total_company_branch_types_str = '';
                if(count($ids) > 0)
                {
                    $total_company_branch_types_str = '(';
                    $total_company_branch_types_str .= implode(',',$ids);
                    $total_company_branch_types_str .= ')';
                }
                $last_node = $this->Company_model->getLastNodeOfCompanyBranchType($data['company_id'],$total_company_branch_types_str);
                $reporting_to = (!empty($last_node)) ? $last_node[0]['branch_type_id'] : 0;
                $this->Company_model->createCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $data['id_branch_type'], 'reporting_branch_type_id' => $reporting_to,'created_date_time' => currentDate()));
            }
        }


        //primary, checking nodes
        $current_primary = array();
        if($data['is_primary']){
            $current_primary = $this->Company_model->getPrimaryCompanyBranchType(array('company_id' => $data['company_id']));
            if(isset($data['id_branch_type'])){
                $current_updating_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'], $data['id_branch_type']);
                $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            }
        }
        else{
            //$branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            if(isset($data['id_branch_type'])){
                    $current_updating_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'], $data['id_branch_type']);
                    $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
                    //checking not updating reporting_id
                    if(isset($current_updating_branch_type[0]['reporting_branch_type_id']) && ($current_updating_branch_type[0]['reporting_branch_type_id'] == $data['reporting_branch_type_id']))
                    {
                        unset($current_updating_branch_type);
                        unset($branch_type_has_current_branch_as_reporting);
                    }
            }
            $branch_type_has_updating_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['reporting_branch_type_id']);
        }
        //checking nodes

        //update
        if(isset($data['id_branch_type']))
        {


            $branch_type_id = $data['id_branch_type'];
            if(isset($data['branch_type_name']) && $data['branch_type_name']==''){
                $result = array('status'=>FALSE,'error'=>array('branch_type_name' => $this->lang->line('branch_type_invalid')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_branch_type_id']; }

            if(isset($data['branch_type_name']))
                $this->Company_model->updateBranchType(array('id_branch_type' => $data['id_branch_type'],'branch_type_name' => $data['branch_type_name'], 'branch_type_code' => $data['branch_type_code'], 'description' => $data['description']));
            //echo $data['company_id'].'---'.$branch_type_id.'---'.$report_to; exit;
            if($branch_type_id != $report_to)
                $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_id, 'reporting_branch_type_id' => $report_to));
            $suc_msg = $this->lang->line('branch_type_update');
            //updating reporting branch types

            if($data['is_primary']){
                if(isset($current_primary[0]['branch_type_id']) && $current_primary[0]['branch_type_id'] != $branch_type_id){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $current_primary[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                    if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                        $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['reporting_branch_type_id']));
                }

                //updating branch type which has current branch as reporting

            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting)){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['reporting_branch_type_id']));
                }
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting) && isset($current_updating_branch_type) && !empty($current_updating_branch_type)){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_updating_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['branch_type_id']));
                }
            }
        }
        else if(isset($data['branch_type_name']))
        {
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_branch_type_id']; }
            if($data['branch_type_name']==''){
                $result = array('status'=>FALSE,'error'=>array('branch_type_name' => $this->lang->line('branch_type_invalid')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $branch_type_id = $this->Company_model->addBranchType(array('branch_type_name' => $data['branch_type_name'],'branch_type_code' => $data['branch_type_name'], 'description' => $data['description'], 'company_id' => $data['company_id'],'created_date_time' => currentDate()));
            $this->Company_model->createCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_id, 'reporting_branch_type_id' => $report_to,'created_date_time' => currentDate()));
            $suc_msg = $this->lang->line('branch_type_add');
            //updating reporting branch types
            if($data['is_primary']){
                if(!empty($current_primary))
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $current_primary[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting)){
                    if(!isset($current_updating_branch_type) && !empty($current_updating_branch_type)){ $branch_type_id = $current_updating_branch_type[0]['branch_type_id']; }
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_updating_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchType_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['branch_type_id']) && $data['branch_type_id']!=0 && $data['branch_type_id']!=''){
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($data['company_id'],$data['branch_type_id']);
            array_push($allowed_branch_type_ids,$data['branch_type_id']);
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }
            if($allowed_branch_type_ids){
                $result = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id'],$allowed_branch_type_ids);
            }
            else
                $result = array();
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id']);
        }


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranches_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $branch_id = $branch_type_details = $allowed_branch_type_ids = '';

        if(isset($data['branch_id']) && $data['branch_id']!='' && $data['branch_id']!='undefined'){ $branch_id = $data['branch_id']; }

        if(isset($data['branch_type_id'])){
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($data['company_id'],$data['branch_type_id']);
            array_push($allowed_branch_type_ids,$data['branch_type_id']);
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }

            $branch_type_details = $this->Company_model->getBranchTypeById($data['branch_type_id']);
            if($allowed_branch_type_ids){
                $result = $this->Company_model->getCompanyBranchByCompanyId($data['company_id'],$branch_id,$allowed_branch_type_ids);
            }
            else
                $result = array();
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchByCompanyId($data['company_id'],$branch_id);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'branch_type' =>$branch_type_details));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchType_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $company_id = array('required'=> $this->lang->line('company_id_req'));
        $this->form_validator->add_rules('company_id', $company_id);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_id = $data['company_id'];
        $data = $data['data'];
        for($s=0;$s<count($data);$s++)
        {
            $this->Company_model->createCompanyBranchType(array('company_id' => $company_id,'branch_type_id' => $data[$s]['branch_type_id'],'reporting_branch_type_id' => $data[$s]['reporting_branch_type_id'],'created_date_time' => currentDate()));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('branch_type_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchUsers_get()
    {
        $data = $this->input->get();
        $data['branch_id'] = 0;
        if(isset($data['project_id'])){
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $data['user_id'] = $project_details[0]['created_by'];
        }

        if(isset($data['user_id'])){
            $company_user = $this->Company_model->getCompanyUser($data['user_id']);
            if(empty($company_user))
            {
                $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK); exit;
            }
            $data['branch_id'] = $company_user->branch_id;
        }

        $result = $this->Company_model->getBranchUsers($data['branch_id']);
        $allBranchUsers = [];
        for($i=0; $i<count($result);$i++)
        {
            $branchUsers = $result[$i];
            for($u=0; $u<count($branchUsers); $u++)
            {
                $allBranchUsers[] = $result[$i][$u];
            }
        }
        $result = $allBranchUsers;
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function approvalRole_post()
{
    $data = json_decode(file_get_contents("php://input"), true);
    if($data){ $_POST = $data; }
    $data = $this->input->post();
    //echo "<pre>"; print_r($data); exit;
    if(empty($data)){
        $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    if(!isset($data['approval_role_code'])){ $data['approval_role_code']=''; }
    if(!isset($data['description'])){ $data['description']=''; }


    if(!isset($data['id_approval_role']) && (!isset($data['approval_name']) || $data['approval_name']==''))
        $this->form_validator->add_rules('id_approval_role', array('required' => $this->lang->line('approval_role_req')));
    if(isset($data['id_approval_role']) && isset($data['approval_name']) && trim($data['approval_name'])=='')
        $this->form_validator->add_rules('approval_name', array('required' => $this->lang->line('approval_name_req')));
    //if(!$data['is_primary'] && (!isset($data['reporting_id']) || $data['reporting_id']!=''))
    $this->form_validator->add_rules('reporting_role_id', array('required' => $this->lang->line('reporting_role_req')));
    $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
    $this->form_validator->add_rules('branch_type_id', array('required' => $this->lang->line('branch_type_req')));
    $validated = $this->form_validator->validate($data);
    if($validated != 1)
    {
        $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    if(isset($data['id_approval_role']))
    {
        if(isset($data['reporting_role_id']) && $data['id_approval_role']==$data['reporting_role_id']){
            $result = array('status'=>FALSE,'error'=>array('reporting_id' => $this->lang->line('same_approval_role_err')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }


    //check branch_type_id in company_branch_type_id
    if(isset($data['id_approval_role']))
    {
        $check = $this->Company_model->getCompanyApprovalRoleByApprovalId($data['company_id'],$data['id_approval_role']);
        if(empty($check)){
            $total_company_approval_roles = $this->Company_model->getCompanyApprovalRolesByCompanyId($data['company_id']);
            $ids = array();
            foreach($total_company_approval_roles as $item){ $ids[] = $item['reporting_role_id']; }
            $total_company_approval_roles_str = '';
            if(count($ids) > 0)
            {
                $total_company_approval_roles_str = '(';
                $total_company_approval_roles_str .= implode(',',$ids);
                $total_company_approval_roles_str .= ')';
            }
            $last_node = $this->Company_model->getLastNodeOfCompanyApprovalRole($data['company_id'],$total_company_approval_roles_str);
            $reporting_role_id = (!empty($last_node)) ? $last_node[0]['approval_role_id'] : 0;
            $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $data['id_approval_role'], 'reporting_role_id' => $reporting_role_id,'created_date_time' => currentDate()));
        }
    }

    //primary, checking nodes
    $current_primary = array();
    if($data['is_primary']){
        $current_primary = $this->Company_model->getPrimaryCompanyApprovalRole(array('company_id' => $data['company_id']));
        if(isset($data['id_approval_role'])){
            $current_updating_branch_type = $this->Company_model->getCompanyApprovalRoleByApprovalRoleId($data['company_id'], $data['id_approval_role']);
            $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['id_approval_role']);
        }
    }
    else{
        //$branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
        if(isset($data['id_approval_role'])){
            $current_updating_branch_type = $this->Company_model->getCompanyApprovalRoleByApprovalRoleId($data['company_id'], $data['id_approval_role']);
            $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['id_approval_role']);
            //checking not updating reporting_id
            if($current_updating_branch_type[0]['reporting_role_id'] == $data['reporting_role_id'])
            {
                unset($current_updating_branch_type);
                unset($branch_type_has_current_branch_as_reporting);
            }
        }
        $branch_type_has_updating_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['reporting_role_id']);
    }
    //checking nodes
    $report_to = 0;
    //update
    if(isset($data['id_approval_role']))
    {
        $approval_role_id = $data['id_approval_role'];
        //if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_role_id']; }
        $report_to = $data['reporting_role_id'];

        if(isset($data['approval_name']))
            $this->Company_model->updateApprovalRole(array('id_approval_role' => $data['id_approval_role'],'approval_name' => $data['approval_name'], 'approval_role_code' => $data['approval_role_code'], 'description' => $data['description']));

        $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to));
        $suc_msg = $this->lang->line('approval_role_update');
        //updating reporting branch types
        if($data['is_primary']){
            if(isset($current_primary[0]['approval_role_id']) && $current_primary[0]['approval_role_id'] != $approval_role_id)
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $current_primary[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
            //updating branch type which has current branch as reporting

            if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['reporting_role_id']));
        }
        else{
            if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting)){
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['reporting_role_id']));
            }
            if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting) && isset($current_updating_branch_type) && !empty($current_updating_branch_type)){
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_updating_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['approval_role_id']));
            }
        }
    }
    else if(isset($data['approval_name']))
    {
        //if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_role_id']; }
        $report_to = $data['reporting_role_id'];
        if($data['approval_name']==''){
            $result = array('status'=>FALSE,'error'=>array('approval_name' => $this->lang->line('approval_name_invalid')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $approval_role_id = $this->Company_model->addApprovalRole(array('approval_name' => $data['approval_name'],'approval_role_code' => $data['approval_role_code'], 'description' => $data['description'], 'company_id' => $data['company_id'],'created_date_time' => currentDate()));

        $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to,'created_date_time' => currentDate()));
        $suc_msg = $this->lang->line('approval_role_add');
        //updating reporting branch types
        if($data['is_primary']){
            if(isset($current_primary[0]['approval_role_id']))
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $current_primary[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
        }
        else{
            if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
            if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting)){
                if(!isset($current_updating_branch_type) && !empty($current_updating_branch_type)){ $approval_role_id = $current_updating_branch_type[0]['approval_role_id']; }
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_updating_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
            }
        }
    }
    $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
    $this->response($result, REST_Controller::HTTP_OK);
}*/

    public function approvalRole_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['approval_role_code'])){ $data['approval_role_code']=''; }
        if(!isset($data['description'])){ $data['description']=''; }


        if(!isset($data['id_approval_role']) && (!isset($data['approval_name']) || $data['approval_name']==''))
            $this->form_validator->add_rules('id_approval_role', array('required' => $this->lang->line('approval_role_req')));
        if(isset($data['id_approval_role']) && isset($data['approval_name']) && trim($data['approval_name'])=='')
            $this->form_validator->add_rules('approval_name', array('required' => $this->lang->line('approval_name_req')));
        //$this->form_validator->add_rules('reporting_role_id', array('required' => $this->lang->line('reporting_role_req')));
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('branch_type_id', array('required' => $this->lang->line('branch_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $report_to = 0;
        if(isset($data['reporting_role_id']))
            $report_to = $data['reporting_role_id'];
        $all_projects = 0;
        if(isset($data['all_projects'])){ $all_projects = $data['all_projects']; }
        if(isset($data['id_approval_role']))
        {
            $approval_role_id = $data['id_approval_role'];
            $check = $this->Company_model->getCompanyApprovalRoleByApprovalId($data['company_id'],$data['id_approval_role']);
            /*$company_approval_role = $this->Company_model->getCompanyApprovalRole(array('company_id' => $data['company_id'],'approval_role_id' => $data['id_approval_role']));
            if($company_approval_role[0]['reporting_role_id']==0){
                $reporting_roles = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'],$company_approval_role[0]['approval_role_id']);
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $reporting_roles[0]['approval_role_id'], 'reporting_role_id' => 0));
            }*/
            if(isset($data['approval_name']))
                $this->Company_model->updateApprovalRole(array('id_approval_role' => $data['id_approval_role'],'approval_name' => $data['approval_name'], 'approval_role_code' => $data['approval_role_code'], 'description' => $data['description']));

            if(empty($check))
                $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to,'created_date_time' => currentDate()));
            else
                $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to,'all_projects' => $all_projects));
            $suc_msg = $this->lang->line('approval_role_update');
        }
        else if(isset($data['approval_name']))
        {
            if($data['approval_name']==''){
                $result = array('status'=>FALSE,'error'=>array('approval_name' => $this->lang->line('approval_name_invalid')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $approval_role_id = $this->Company_model->addApprovalRole(array('approval_name' => $data['approval_name'],'approval_role_code' => $data['approval_role_code'], 'description' => $data['description'], 'company_id' => $data['company_id'],'created_date_time' => currentDate()));

            $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to,'all_projects' => $all_projects,'created_date_time' => currentDate()));
            $suc_msg = $this->lang->line('approval_role_add');
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyApprovalRole_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCompanyApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvals_get($company_id)
    {
        $approval_role = $this->Company_model->getApprovalRoleByCompanyId($company_id);
        //if(count($approval_role) > 1)
            //$approval_role = $result = $this->getOrderedData($approval_role,'0','reporting_role_id','approval_role_id');
        $this->ordered_data = '';
        for($s=0;$s<count($approval_role);$s++)
        {
            $approval_role[$s]['users'] = $this->Company_model->getCompanyUsersByApprovalRole($approval_role[$s]['company_approval_role_id'],$company_id);
            for($r=0;$r<count($approval_role[$s]['users']);$r++){
                $approval_role[$s]['users'][$r]['profile_image'] = getImageUrl($approval_role[$s]['users'][$r]['profile_image'],'profile');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$approval_role);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalTypeStructure_get($company_id)
    {
        $result = $this->Company_model->getCompanyApprovalTypeStructure($company_id);
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['is_edit']=='0'){ $result[$s]['is_edit'] = 0; }
            else{ $result[$s]['is_edit'] = 1; }
        }

        /*if(count($result) > 1)
            $result = $this->getOrderedData($result,'0','reporting_role_id','approval_role_id');*/

        //$this->ordered_data = '';
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function downloadBranchExcel_get()
    {
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>REST_API_URL.'Classes/company_branches.xls');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function downloadUserExcel_get()
    {
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>REST_API_URL.'Classes/company_users.xls');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadBranch_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(!isset($data['company_id']) || $data['company_id']==''){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $data['company_id'];
        if(isset($_FILES) && !empty($_FILES['excel']['name'])){
            $file_name = $_FILES['excel']['name'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['excel']['size']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>str_replace('MUZ',EXCEL_UPLOAD_SIZE,$this->lang->line('max_upload_size')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $headers = array('legal_name','country','state','city','address','reporting_branch','branch_type');
        $excel_headers = array_values(excelHeaders($_FILES['excel']['tmp_name'], true));

        if($headers!=$excel_headers){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('excel_headers_match'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data = excelToArray($_FILES['excel']['tmp_name'], true);

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('excel_empty'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $errors = array();
        for($s=0;$s<count($data);$s++)
        {
            $errors[$s]['row'] = ($s+2);
            $this->form_validator->clearRules();
            $this->form_validator->add_rules('legal_name', array('required'=> $this->lang->line('legal_name_req')));
            $this->form_validator->add_rules('country', array('required'=> $this->lang->line('country_req')));
            $this->form_validator->add_rules('state', array('required'=> $this->lang->line('state_req')));
            $this->form_validator->add_rules('city', array('required'=> $this->lang->line('city_req')));
            $this->form_validator->add_rules('address', array('required'=> $this->lang->line('address_req')));
            $this->form_validator->add_rules('reporting_branch', array('required'=> $this->lang->line('reporting_branch_req')));
            $this->form_validator->add_rules('branch_type', array('required'=> $this->lang->line('branch_type_req')));
            $validated = $this->form_validator->validate($data[$s]);
            if($validated != 1)
            {
                $errors[$s]['errors'] = $validated;
            }
            else
            {
                $country = $this->Company_model->getCountryByName(strtolower($data[$s]['country']));
                $reporting_branch = $this->Company_model->getCompanyBranchByName(strtolower($data[$s]['reporting_branch']));

                $branch_type = $this->Company_model->getCompanyBranchTypeByName(strtolower($data[$s]['branch_type']),$company_id);
                if(empty($country)){
                    $errors[$s]['errors']['country'] = $this->lang->line('country_invalid');
                }
                else{
                    unset($data[$s]['country']);
                    $data[$s]['country_id'] = $country->id_country;
                }
                if(empty($reporting_branch)){
                    $errors[$s]['errors']['reporting_branch'] = $this->lang->line('reporting_branch_invalid');
                }
                else{
                    unset($data[$s]['reporting_branch']);
                    $data[$s]['reporting_branch_id'] = $reporting_branch->id_branch;
                }

                if(empty($branch_type)){
                    $errors[$s]['errors']['branch_type'] = $this->lang->line('branch_type_invalid');
                }
                else{
                    unset($data[$s]['branch_type']);
                    $data[$s]['branch_type_id'] = $branch_type->id_branch_type;
                }

                if(isset($data[$s]['country_id']) && isset($data[$s]['reporting_branch_id']) && isset($data[$s]['branch_type_id'])){ //&& isset($data[$s]['legal_name'])
                    $data[$s]['branch_state'] = $data[$s]['state'];
                    $data[$s]['branch_city'] = $data[$s]['city'];
                    $data[$s]['branch_address'] = $data[$s]['address'];
                    unset($data[$s]['state']); unset($data[$s]['city']); unset($data[$s]['address']);

                    $data[$s]['company_id'] = $company_id; //change to dynamic company_id
                    $data[$s]['created_date_time'] = currentDate();
                    $this->Company_model->createCompanyBranch($data[$s]);
                    $errors[$s]['message'] = $this->lang->line('add_succ');
                }
            }
        }

        $result = array('status'=>True, 'message' => $this->lang->line('branches_uploaded_suc'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadUser_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(!isset($data['id_company']) || $data['id_company']==''){
            $result = array('status'=>FALSE,'error'=> $this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $data['id_company'];
        if(isset($_FILES) && !empty($_FILES['excel']['name'])){
            $file_name = $_FILES['excel']['name'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['excel']['size']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('max_2mb_allowed'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $headers = array('first_name','last_name','email','phone_number','address','user_role','reporting_to','branch');
        $excel_headers = array_values(excelHeaders($_FILES['excel']['tmp_name'], true));

        if($headers!=$excel_headers){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('excel_headers_match'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data = excelToArray($_FILES['excel']['tmp_name'], true);

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('excel_empty'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            'numeric' =>  $this->lang->line('phone_num_num'),
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-10' => $this->lang->line('phone_num_max_len'),
        );

        $errors = array();
        for($s=0;$s<count($data);$s++)
        {
            $errors[$s]['row'] = ($s+2);
            $this->form_validator->clearRules();
            $this->form_validator->add_rules('first_name', $firstNameRules);
            $this->form_validator->add_rules('last_name', $lastNameRules);
            $this->form_validator->add_rules('email', $emailRules);
            $this->form_validator->add_rules('phone_number', $phoneRules);
            $this->form_validator->add_rules('address', array('required'=> $this->lang->line('address_req')));
            $this->form_validator->add_rules('user_role', array('required'=> $this->lang->line('user_role_req')));
            $this->form_validator->add_rules('reporting_to', array('required'=> $this->lang->line('reporting_to_req')));
            $this->form_validator->add_rules('branch', array('required'=> $this->lang->line('branch_req')));
            $validated = $this->form_validator->validate($data[$s]);
            if($validated != 1)
            {
                $errors[$s]['errors'] = $validated;
            }
            else
            {
                $email_status = 0;
                $email_check = $this->User_model->check_email(strtolower($data[$s]['email']));
                $user_role = $this->Company_model->getCompanyApprovalRoleByName(strtolower($data[$s]['user_role']),$company_id);
                $branch = $this->Company_model->getCompanyBranchByName(strtolower($data[$s]['branch']));
                $reporting_to = $this->User_model->check_email(strtolower($data[$s]['reporting_to']));

                if(!empty($email_check)){
                    $errors[$s]['errors']['email'] = $this->lang->line('email_duplicate');
                    $email_status = 1;
                }
                else{
                    $data[$s]['email_id'] = $data[$s]['email'];
                    unset($data[$s]['email']);
                }

                if(empty($user_role)){
                    $errors[$s]['errors']['user_role'] = $this->lang->line('user_role_invalid');
                }
                else{
                    unset($data[$s]['user_role']);
                    $data[$s]['company_approval_role_id'] = $user_role->id_company_approval_role;
                }

                if(empty($branch)){
                    $errors[$s]['errors']['branch'] = $this->lang->line('branch_invalid');
                }
                else{
                    unset($data[$s]['branch']);
                    $data[$s]['branch_id'] = $branch->id_branch;
                    $data[$s]['country_id'] = $branch->country_id;
                }

                if(empty($reporting_to)){
                    $errors[$s]['errors']['reporting_to'] = $this->lang->line('reporting_to_invalid');
                }
                else{
                    unset($data[$s]['reporting_to']);
                    $data[$s]['reporting_user_id'] = $reporting_to->id_user;
                }

                if($email_status==0 && isset($data[$s]['email_id']) && isset($data[$s]['company_approval_role_id']) && isset($data[$s]['branch_id']) && isset($data[$s]['reporting_user_id']))
                {
                    $data[$s]['user_role_id'] = 3;
                    $company_approval_role_id = $data[$s]['company_approval_role_id'];
                    $branch_id = $data[$s]['branch_id'];
                    $reporting_user_id = $data[$s]['reporting_user_id'];

                    unset($data[$s]['company_approval_role_id']); unset($data[$s]['branch_id']); unset($data[$s]['reporting_user_id']);
                    $user_password = generatePassword(6);
                    $data[$s]['password'] = md5($user_password);
                    $data['created_date_time'] = currentDate();
                    $user_id = $this->User_model->createUserInfo($data[$s]);
                    $this->Company_model->createCompanyUser(array('company_id' => $company_id, 'user_id' => $user_id,'company_approval_role_id' => $company_approval_role_id, 'branch_id' => $branch_id,'reporting_user_id' => $reporting_user_id));
                    $errors[$s]['message'] = 'Successfully added';

                    sendmail($data[$s]['email_id'],$this->lang->line('account_created_label'),'<p>'.$this->lang->line('hello').' '.$data[$s]['first_name'].' '.$data[$s]['last_name'].', '.$this->lang->line('welcome_to_qlana').',</p><p>'.$this->lang->line('login_details_are').'</p><p>'.$this->lang->line('email_label').': '.$data[$s]['email_id'].'<br>'.$this->lang->line('password_label').': '.$user_password.'</p>');
                }
            }
        }

        $result = array('status'=>True, 'message' => $this->lang->line('users_upload'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchTypeRole_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getBranchTypeRole($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getOrderCommitteeData($data,$identifier)
    {
        for($t=0;$t<count($data);$t++){
            if(count($this->ordered_committee_data)==count($data)){  break; }
            if($data[$t]['credit_committee_forward_to']==$identifier){
                $this->ordered_committee_data[] = $data[$t];
                $identifier = $data[$t]['id_company_approval_credit_committee'];
                if(count($this->ordered_committee_data)==count($data)){  break; }
                else{ $this->getOrderCommitteeData($data,$identifier); }
            }
        }
        return $this->ordered_committee_data;
    }

    public function approvalStructure_get()
    {
        $data = $this->input->get();
        $approval_structure_details = $this->Company_model->getApprovalStructure($data);

        if(!empty($approval_structure_details))
            $approval_structure_name = $approval_structure_details[0]['approval_structure_name'];
        else
            $approval_structure_name = '';

        if(isset($data['id_company_approval_structure']) && $data['id_company_approval_structure']!='')
        {
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            if(count($branch_type_data) > 1)
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
            $this->ordered_data = '';

            $result = $members_data = $members = array();
            for($s=0;$s<count($branch_type_data);$s++)
            {
                $this->ordered_committee_data = array();
                $committee_data = $this->Company_model->getApprovalCreditCommittee(array('branch_type_id' => $branch_type_data[$s]['branch_type_id'],'id_company_approval_structure' => $data['id_company_approval_structure']));
                $committee_data = $this->getOrderCommitteeData($committee_data,0);
                for($r=0;$r<count($committee_data);$r++)
                {
                    $members = array();
                    $members_data = $this->Company_model->getApprovalCreditCommitteeStructure(array('id_company_approval_credit_committee' => $committee_data[$r]['id_company_approval_credit_committee']));

                    for($st=0;$st<count($members_data);$st++)
                    {
                        if($members_data[$st]['branch_type_role_id']==1){
                            $committee_data[$r]['secretary'] = $members_data[$st]['approval_name'];
                        }
                        else{
                            $members[] = $members_data[$st];
                        }
                    }
                    $committee_data[$r]['members'] = $members;
                }


                $result[] = array(
                    'id_branch_type' => $branch_type_data[$s]['branch_type_id'],
                    'branch_type_name' => $branch_type_data[$s]['branch_type_name'],
                    'committee' => $committee_data
                );
            }

        }
        else
        {
            $result = $this->Company_model->getApprovalStructure($data);
        }

        //resulting selected product id approval and pre approval structure...
        if(isset($data['product_id'])){ $product_approval_structure_id = array();
            $product_approval_structure = $this->Company_model->getProductDetails(array('id_product' => $data['product_id'], 'company_approval_category_id' => 2));
            if(!empty($product_approval_structure)){
                $product_approval_structure_id = array(
                    'id_company_approval_structure' => $product_approval_structure[0]['id_company_approval_structure'],
                    'approval_structure_name' => $product_approval_structure[0]['approval_structure_name']
                );
            }

            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> array('data' => $result, 'product_approval_structure' => $product_approval_structure_id));
        }
        else
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> array('data' => $result, 'approval_structure_name' => $approval_structure_name));

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalStructure_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('approval_structure_name', array('required'=>$this->lang->line('name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_company_approval_structure'])){
            $check_name = $this->Company_model->getApprovalStructure(array('company_id' => $data['company_id'],'approval_structure_name' => trim($data['approval_structure_name']),'id_company_approval_structure_not' => $data['id_company_approval_structure']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('approval_structure_name' => $this->lang->line('name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $company_approval_structure_id = $this->Company_model->updateCompanyApprovalStructure($data);
            $suc_msg = $this->lang->line('approval_structure_update');
        }
        else{
            $check_name = $this->Company_model->getApprovalStructure(array('company_id' => $data['company_id'],'approval_structure_name' => trim($data['approval_structure_name'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('approval_structure_name' => $this->lang->line('name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['created_date_time'] = currentDate();
            $company_approval_structure_id = $this->Company_model->addCompanyApprovalStructure($data);
            $suc_msg = $this->lang->line('approval_structure_added');
        }

        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getApprovalCreditCommitteeList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function preApprovalCommitties_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=>$this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        $company_id = $project_details[0]['company_id'];
        $user_id = $project_details[0]['assigned_to'];
        $product_id = $project_details[0]['product_id'];

        //getting approval structure
        $approval_structure = $this->Crm_model->getProduct(array('company_id' => $company_id, 'id_product'=> $product_id));
        if($approval_structure[0]['company_pre_approval_structure_id']=='' || $approval_structure[0]['company_pre_approval_structure_id']==0)
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('pre_approval_structure_for_product'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_pre_approval_structure_id = $approval_structure[0]['company_pre_approval_structure_id'];

        $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($company_id);
        $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        $committee_list = array();
        for($s=0;$s<count($branch_type_data);$s++)
        {
            $commitee = $this->Company_model->getPreApprovalCreditCommittee(array('company_id' => $company_id,'branch_type_id' => $branch_type_data[$s]['branch_type_id'], 'id_company_approval_structure' => $company_pre_approval_structure_id));
            if(!empty($commitee)) {
                //getting committee in process of pre approval or not
                for($l=0;$l<count($commitee);$l++)
                {
                    $committee_pre_approval_check = $this->Company_model->getProjectPreApproval(array(
                            'company_approval_credit_committee_id' => $commitee[$l]['id_company_approval_credit_committee'],
                            'crm_project_id' => $data['crm_project_id']
                        )
                    );
                    if(!empty($committee_pre_approval_check)){
                        $commitee[$l]['status'] = $committee_pre_approval_check[0]['status'];
                        $commitee[$l]['assigned_to'] = $committee_pre_approval_check[0]['assigned_to'];
                        foreach ($committee_pre_approval_check as $item) {
                            $commitee[$l]['comments'][] =  array(
                                'user_name' => $item['user_name'],
                                'approval_role' => $item['approval_name'],
                                'comment' => $item['comment'],
                                'date' => $item['commented_date']
                            );
                        }
                    }
                    else{
                        $commitee[$l]['status'] = 'pending';
                        $commitee[$l]['comments'] = array();
                        $commitee[$l]['assigned_to'] = 0;
                    }
                    array_push($committee_list, $commitee[$l]);
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $committee_list);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function preApprovalCommitteeSecretary_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('id_company_approval_credit_committee', array('required'=> $this->lang->line('id_company_approval_credit_committee_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $committee_structure = $this->Company_model->checkApprovalCreditCommitteeStructure(array('id_company_approval_credit_committee' => $data['id_company_approval_credit_committee']));
        $company_approval_role_id= $branch_type_id = 0;
        $branch_type_id = $committee_structure[0]['branch_type_id'];
        $company_id = $committee_structure[0]['company_id'];
        for($s=0;$s<count($committee_structure);$s++){
            if($committee_structure[$s]['branch_type_role_id']==1)
                $company_approval_role_id = $committee_structure[$s]['approval_role_id'];
        }

        //checking if pre approval in process status with this committee
        $check_process = $this->Company_model->getProjectPreApproval(array('company_approval_credit_committee_id' => $data['id_company_approval_credit_committee'],'crm_project_id' => $data['crm_project_id']));
        if(!empty($check_process) && $company_approval_role_id){
            $users = $this->Company_model->getCompanyUsers(array('company_id' => $company_id,'branch_type_id' => $branch_type_id, 'allowed_approval_role_id' => $company_approval_role_id, 'user_id' => $check_process[0]['assigned_to']));
        }
        else if($company_approval_role_id){
           $users = $this->Company_model->getCompanyUsers(array('company_id' => $company_id,'branch_type_id' => $branch_type_id, 'allowed_approval_role_id' => $company_approval_role_id, 'user_not_in' => $data['user_id']));
        }
        else
            $users = array();

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $users);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function preApprovalComment_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_approval_credit_committee_id', array('required'=> $this->lang->line('id_company_approval_credit_committee_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_req')));
        $this->form_validator->add_rules('assigned_to', array('required'=> $this->lang->line('assigned_to_req')));
        $this->form_validator->add_rules('status', array('required'=> $this->lang->line('status_req')));
        $this->form_validator->add_rules('comment', array('required'=> $this->lang->line('comment_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $pre_approval_check = $this->Company_model->getProjectPreApproval(array(
            'company_approval_credit_committee_id' => $data['company_approval_credit_committee_id'],
            'crm_project_id' => $data['crm_project_id']
        ));

        if(empty($pre_approval_check)){

            $id_project_pre_approval = $this->Company_model->addProjectPreApproval(array(
                'company_approval_credit_committee_id' => $data['company_approval_credit_committee_id'],
                'crm_project_id' => $data['crm_project_id'],
                'assigned_by' => $data['user_id'],
                'assigned_to' => $data['assigned_to'],
                'status' => $data['status'],
                'created_date_time' => currentDate()
            ));
        }
        else{
            $id_project_pre_approval = $pre_approval_check[0]['id_project_pre_approval'];
            $this->Company_model->updateProjectPreApproval(array(
                'id_project_pre_approval' => $pre_approval_check[0]['id_project_pre_approval'],
                'status' => $data['status']
            ));
        }

        $this->Company_model->addProjectPreApprovalWorkflow(array(
            'project_pre_approval_id' => $id_project_pre_approval,
            'comment' => $data['comment'],
            'commented_by' => $data['user_id'],
            'created_date_time' => currentDate()
        ));

        $committee = $this->Company_model->getCompanyApprovalCreditCommittee(array('id_company_approval_credit_committee' => $data['company_approval_credit_committee_id']));

        $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($committee[0]['company_id']);
        $committee_list = array(); $total_committee = 0;
        for($s=0;$s<count($branch_type_data);$s++)
        {
            $commitee_count = $this->Company_model->getPreApprovalCreditCommitteeCount(array('company_id' => $committee[0]['company_id'],'branch_type_id' => $branch_type_data[$s]['branch_type_id'], 'id_company_approval_structure' => $committee[0]['company_approval_structure_id']));
            if(!empty($commitee_count))
                $total_committee = $total_committee+$commitee_count[0]['total'];
        }

        $total_pre_approval = $this->Company_model->getProjectPreApprovalCount(array('crm_project_id' => $data['crm_project_id'], 'status' => 'accepted'));
        $total_pre_approval = $total_pre_approval[0]['total'];

        if($total_committee==$total_pre_approval){
            $this->Crm_model->updateProject(array('project_status' => 'pre-approved'),$data['crm_project_id']);
        }

        $user_info = $this->User_model->getUserInfo(array('id' => $data['assigned_to']));
        if(empty($pre_approval_check))
            $this->Activity_model->addActivity(array('activity_name' => 'Pre Approval','activity_template' => 'Pre approval request sent to '.$user_info->first_name.' '.$user_info->last_name,'module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'pre-approval','activity_reference_id' =>$id_project_pre_approval,'created_by' => $data['user_id'],'created_date_time' => currentDate(),'updated_date_time' => currentDate()));
        else
            $this->Activity_model->addActivity(array('activity_name' => 'Pre Approval','activity_template' => 'Pre approval request has been '.$data['status'].' by '.$user_info->first_name.' '.$user_info->last_name,'module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'pre-approval','activity_reference_id' =>$id_project_pre_approval,'created_by' => $data['user_id'],'created_date_time' => currentDate(),'updated_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function committeeSecretary_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=>$this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);
        $company_id = $project_details[0]['company_id'];
        $user_id = $project_details[0]['assigned_to'];
        $product_id = $project_details[0]['product_id'];
        $committee_id = $project_details[0]['committee_id'];
        $branch_id = 0;
        $company_branch = $this->Company_model->getCompanyUsers(array('company_id' => $company_id, 'user_id'=> $user_id));
        if(!empty($company_branch))
            $branch_id = $company_branch[0]['branch_id'];
        //getting branch
        if(!$committee_id)
        {
            //getting approval structure
            $approval_structure = $this->Crm_model->getProduct(array('company_id' => $company_id, 'id_product'=> $product_id));
            $company_approval_structure_id = $approval_structure[0]['company_approval_structure_id'];
            //getting last node
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
            $this->ordered_data = '';
            $branch_type_id = $branch_type_data[count($branch_type_data)-1]['branch_type_id'];
        }
        else
        {
            $committee_details = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_id));
            $branch_type_id = $committee_details[0]['branch_type_id'];
            $company_approval_structure_id = $committee_details[0]['company_approval_structure_id'];
        }
        $last_node = $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $company_approval_structure_id,'committee_id' => $committee_id));

        if(empty($last_node))
        {
            $data = array(
                'details' => array(
                    'committee_id' => 0,
                    'committee_name' => ''
                ),
                'users' => ''
            );
            $result = array('status'=>TRUE, 'message' => $this->lang->line('no_committees'), 'data'=> $data);
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $committee_structure = $this->Company_model->checkApprovalCreditCommitteeStructure(array('id_company_approval_credit_committee' => $last_node[0]['id_company_approval_credit_committee']));

        $approval_role_id=0;
        for($s=0;$s<count($committee_structure);$s++)
        {
            if($committee_structure[$s]['branch_type_role_id']==1)
                $approval_role_id = $committee_structure[$s]['approval_role_id'];
        }

        $company_approval_role_id = 0;
        if($approval_role_id){ $company_approval_role_id = $approval_role_id; }
        if($company_approval_role_id){
            $this->forward_to = $user_id;
            $users = $this->getReportingBranchUsers(array('company_id' => $company_id, 'id_branch' => $branch_id, 'company_approval_role_id' => $company_approval_role_id, 'user_id' => $user_id));
        }
        else
            $users = array();

        $committee = array(
            'details' => array(
                'committee_id' => $last_node[0]['id_company_approval_credit_committee'],
                'committee_name' => $last_node[0]['credit_committee_name']
            ),
            'users' => $users
        );
        //for eliminating current user
        for($s=0;$s<count($committee['users']);$s++)
        {
            if($committee['users'][$s]['id_user']==$data['user_id']){
                unset($committee['users'][$s]);
            }
        }
        $committee['users'] = array_values($committee['users']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $committee);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getReportingBranchTypeCommittee($data)
    {
        $company_id = $data['company_id'];
        $branch_type_id = $data['branch_type_id'];
        $id_company_approval_structure = $data['id_company_approval_structure'];
        $committee_id = $data['committee_id'];
        if($committee_id)
        {
            $committee_details = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_id));
            if($committee_details[0]['credit_committee_forward_to']!=0){
                $last_node = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_details[0]['credit_committee_forward_to']));
                return $last_node;
            }
            else
            {
                $company_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($company_id,$branch_type_id);
                if(empty($company_branch_type) || $company_branch_type[0]['reporting_branch_type_id']==0){
                    return array();
                }
                else{
                    $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $company_branch_type[0]['reporting_branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure,'committee_id' => 0));
                }
            }
        }
        else
        {
            $total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $id_company_approval_structure));
            if(!empty($total_committee_list))
            {
                $ids = array();
                foreach($total_committee_list as $item){ $ids[] = $item['id']; }
                $total_committee_list_str = '(';
                $total_committee_list_str .= implode(',',$ids);
                $total_committee_list_str .= ')';
                if(empty($ids)){ $total_committee_list_str = 0; }
                $this->last_node = $this->Company_model->getLastCompanyApprovalCreditCommittee(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $id_company_approval_structure, 'committee_ids' => $total_committee_list_str));
                return $this->last_node;
            }
            else
            {
                $company_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($company_id,$branch_type_id);
                if(empty($company_branch_type) || $company_branch_type[0]['reporting_branch_type_id']==0){
                    $this->last_node = array();
                }
                else{
                    $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $company_branch_type[0]['reporting_branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure,'committee_id' => $committee_id));
                }
            }
        }

        return $this->last_node;
    }

    public function getReportingBranchUsers($data)
    {
        $user_id = $data['user_id'];
        $company_id = $data['company_id'];
        $id_branch = $data['id_branch'];
        $company_approval_role_id = $data['company_approval_role_id'];
        $users = $this->Company_model->getCompanyUsersByRole(array('company_id' => $company_id, 'company_approval_role_id' => $company_approval_role_id));
        $this->last_node = $users;
        return $this->last_node;
    }

    public function getReportingUserData($data,$user_id,$company_id)
    {
        $report_user = $this->Company_model->getCompanyUsers(array('user_id' => $user_id, 'company_id' => $company_id));
        if(!empty($report_user)){
            for($s=0;$s<count($data);$s++)
            {
                if($report_user[0]['reporting_user_id']==$data[$s]['id_user']){ $this->last_node = $data[$s]; }
            }

            if($this->last_node===''){
                $this->getReportingUserData($data,$report_user[0]['reporting_user_id'],$company_id);
            }
        }
       return $this->last_node;
    }

    public function approvalCreditCommittee_get()
    {
        $data = $this->input->get();
        if(isset($data['id_company_approval_credit_committee']))
        {
            $result['committee'] = $this->Company_model->getApprovalCreditCommittee($data);
            if($result['committee'][0]['credit_committee_forward_to']=="0"){ $result['committee'][0]['is_primary_committee'] = true; }
            if(isset($result['committee'][0])){ $result['committee'] = $result['committee'][0]; }
            $members = $this->Company_model->getApprovalCreditCommitteeStructure($data);
            //echo "<pre>"; print_r($members); exit;
            for($s=0;$s<count($members);$s++)
            {
                if($members[$s]['branch_type_role_id']==1){
                    $result['committee']['committee_secretary'] = $members[$s]['approval_role_id'];
                }
                else{
                    $result['committee_members'][] = $members[$s];
                }
            }
        }
        else{
            $result = $this->Company_model->getApprovalCreditCommittee($data);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function forwardCommittee_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_approval_structure_id = 0 ;
        $result = array();
        if(isset($data['committee']['credit_committee_forward_to']))
        {
            if(isset($data['committee']['id_company_approval_credit_committee']))
                $company_approval_structure_id = $data['committee']['id_company_approval_credit_committee'];

            $result = $this->Company_model->checkCommitteesForwardTo($data['committee'],$company_approval_structure_id);
            if(!empty($result))
            {
                $result = array('status'=>FALSE, 'error' => str_replace('@C2@',$result[0]['forward_committee_name'],str_replace('@C1@',$result[0]['credit_committee_name'],$this->lang->line('committee_forward_duplicate'))), 'data'=> $result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommittee_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('credit_committee_name', array('required'=>$this->lang->line('committee_name_req')));
        $this->form_validator->add_rules('branch_type_id', array('required'=>$this->lang->line('branch_type_req')));
        $this->form_validator->add_rules('company_approval_structure_id', array('required'=>$this->lang->line('approval_structure_name_req')));
        $this->form_validator->add_rules('committee_secretary', array('required'=>$this->lang->line('committee_secretary_req')));
        $validated = $this->form_validator->validate($data['committee']);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $check_approval_credit_committee_id = 0;
            if(isset($data['committee']['id_company_approval_credit_committee'])){ $check_approval_credit_committee_id = $data['committee']['id_company_approval_credit_committee']; }
            $check_name = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_structure' => $data['committee']['company_approval_structure_id'], 'branch_type_id' => $data['committee']['branch_type_id'], 'id_company_approval_credit_committee_not' => $check_approval_credit_committee_id, 'credit_committee_name' => trim($data['committee']['credit_committee_name'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('credit_committee_name' => $this->lang->line('committee_name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(!isset($data['committee']['credit_committee_forward_to']) && !isset($data['committee']['is_primary_committee']))
        {
            $result = array('status'=>FALSE,'error'=>array('credit_committee_forwarded_to' => $this->lang->line('forwarded_to_committee_req')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $committee_secretary = $is_primary_committee = 0; $committee_structure = $insert_committee_structure = $delete_structure = array();

        if(isset($data['committee']['committee_secretary']))
            $committee_secretary = $data['committee']['committee_secretary'];
        if(isset($data['committee']['is_primary_committee']) && $data['committee']['is_primary_committee']!='' && $data['committee']['is_primary_committee']!=0){
            $is_primary_committee = 1;
        }

        unset($data['committee']['committee_secretary']);
        unset($data['committee']['is_primary_committee']);


        if($is_primary_committee){
            $current_primary = $this->Company_model->checkPrimaryCreditCommittees(array('company_id' => $data['committee']['company_id'],'company_approval_structure_id' => $data['committee']['company_approval_structure_id'],'branch_type_id' => $data['committee']['branch_type_id']));
            if(!empty($current_primary) && isset($data['committee']['id_company_approval_credit_committee']))
                if($current_primary[0]['id_company_approval_credit_committee']==$data['committee']['id_company_approval_credit_committee'])
                {
                    unset($current_primary);
                }

            if(isset($data['committee']['id_company_approval_credit_committee'])){
                $current_updating_committee = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $data['committee']['id_company_approval_credit_committee']));
                $committee_has_current_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'],$data['committee']['id_company_approval_credit_committee']);
                if(isset($current_updating_committee[0]['credit_committee_forward_to']) && ($current_updating_committee[0]['credit_committee_forward_to'] == $data['committee']['credit_committee_forward_to']))
                {
                    unset($current_updating_committee);
                    unset($committee_has_current_committee_as_reporting);
                }
            }
        }
        else{
            if(isset($data['committee']['id_company_approval_credit_committee'])){
                $current_updating_committee = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $data['committee']['id_company_approval_credit_committee']));
                $committee_has_current_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'],$data['committee']['id_company_approval_credit_committee']);
                //checking not updating reporting_id
                if(isset($current_updating_committee[0]['credit_committee_forward_to']) && ($current_updating_committee[0]['credit_committee_forward_to'] == $data['committee']['credit_committee_forward_to']))
                {
                    unset($current_updating_committee);
                    unset($committee_has_current_committee_as_reporting);
                }
            }
            $committee_has_updating_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'], $data['committee']['credit_committee_forward_to']);
        }

        if(isset($data['committee']['id_company_approval_credit_committee']))
        {
            if($is_primary_committee){ $data['committee']['credit_committee_forward_to']=0; }
            else if(!isset($data['committee']['credit_committee_forward_to'])){ $data['committee']['credit_committee_forward_to']=0; }
            else if(isset($data['committee']['credit_committee_forward_to']) && $data['committee']['credit_committee_forward_to']==''){ $data['committee']['credit_committee_forward_to']=0; }

            $this->Company_model->updateApprovalCreditCommittee($data['committee']);
            $id_company_approval_credit_committee = $data['committee']['id_company_approval_credit_committee'];
            if(isset($data['delete_members']))
            {
                for($s=0;$s<count($data['delete_members']);$s++)
                {
                    $delete_structure[] = array(
                        'id_company_approval_credit_committee_structure' => $data['delete_members'][$s],
                        'status' => '0',
                    );
                }
            }

            if(!empty($delete_structure))
                $this->Company_model->updateApprovalCreditCommitteeStructure($delete_structure);

            if($committee_secretary){
                array_push($data['committee_members'],array(
                    'branch_type_role_id' => 1,
                    'company_approval_role_id' => $committee_secretary,
                    'company_approval_credit_committee_id' => $id_company_approval_credit_committee
                ));
            }

            for($s=0;$s<count($data['committee_members']);$s++)
            {
                if($data['committee_members'][$s]['branch_type_role_id']==1){ //for checking secretary is changed or not and updating
                    $check_member_exists =$this->Company_model->checkApprovalCreditCommitteeStructure(array('branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],'id_company_approval_credit_committee' => $id_company_approval_credit_committee));
                    if(!empty($check_member_exists)){
                        $committee_structure[] = array(
                            'id_company_approval_credit_committee_structure' =>  $check_member_exists[0]['id_company_approval_credit_committee_structure'],
                            'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                            'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                            'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                            'status' => 1
                        );
                    }
                    else
                    {
                        $insert_committee_structure[] = array(
                            'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                            'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                            'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                        );
                    }
                }
                else
                {
                    $check_member_exists =$this->Company_model->checkApprovalCreditCommitteeStructure(array('branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],'id_company_approval_credit_committee' => $id_company_approval_credit_committee));
                    if(empty($check_member_exists))
                    {
                        $insert_committee_structure[] = array(
                            'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                            'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                            'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                            'created_date_time' => currentDate()
                        );
                    }
                    else
                    {
                        if(!isset($data['committee_members'][$s]['id_company_approval_credit_committee_structure'])){
                            $data['committee_members'][$s]['id_company_approval_credit_committee_structure'] = $check_member_exists[0]['id_company_approval_credit_committee_structure'];
                        }

                        $committee_structure[] = array(
                            'id_company_approval_credit_committee_structure' => $data['committee_members'][$s]['id_company_approval_credit_committee_structure'],
                            'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                            'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                            'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                            'status' => 1
                        );
                    }
                }

            }

            if(!empty($insert_committee_structure))
                $this->Company_model->addApprovalCreditCommitteeStructure($insert_committee_structure);

            if(!empty($committee_structure))
                $this->Company_model->updateApprovalCreditCommitteeStructure($committee_structure);

            $suc_msg = $this->lang->line('credit_committee_update');

            //updating reporting branch types
            if($is_primary_committee){
                if(!empty($current_primary))
                 $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $current_primary[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                //updating branch type which has current branch as reporting
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting))
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['credit_committee_forward_to']));
            }
            else{
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting)){
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['credit_committee_forward_to']));
                }
                if(isset($committee_has_updating_committee_as_reporting) && !empty($committee_has_updating_committee_as_reporting) && isset($current_updating_committee) && !empty($current_updating_committee)){
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' =>$data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_updating_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['id_company_approval_credit_committee']));
                }
            }
        }
        else
        {
            if($is_primary_committee){ $data['committee']['credit_committee_forward_to']=0; }
            else if(!isset($data['committee']['credit_committee_forward_to'])){ $data['committee']['credit_committee_forward_to']=0; }
            else if(isset($data['committee']['credit_committee_forward_to']) && $data['committee']['credit_committee_forward_to']==''){ $data['committee']['credit_committee_forward_to']=0; }
            $data['committee']['created_date_time'] = currentDate();
            $id_company_approval_credit_committee = $this->Company_model->addApprovalCreditCommittee($data['committee']);
            for($s=0;$s<count($data['committee_members']);$s++)
            {
                $committee_structure[] = array(
                    'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                    'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                    'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                    'created_date_time' => currentDate()
                );
            }
            $committee_structure[] = array(
                'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                'branch_type_role_id' => 1,
                'approval_role_id' => $committee_secretary,
                'created_date_time' => currentDate()
            );

            $this->Company_model->addApprovalCreditCommitteeStructure($committee_structure);
            $suc_msg = $this->lang->line('credit_committee_add');

            //updating reporting branch types
            if($is_primary_committee){
                if(!empty($current_primary))
                $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $current_primary[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
            }
            else{
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting))
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                if(isset($committee_has_updating_committee_as_reporting) && !empty($committee_has_updating_committee_as_reporting)){
                    if(!isset($current_updating_committee) && !empty($current_updating_committee)){ $id_company_approval_credit_committee = $current_updating_committee[0]['id_company_approval_credit_committee']; }
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_updating_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                }
            }
        }



        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeStructure_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getApprovalCreditCommitteeStructure($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeStructure_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('approval_structure_name', array('required'=>$this->lang->line('name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_company_approval_structure'])){
            $company_approval_structure_id = $this->Company_model->updateApprovalCreditCommitteeStructure($data);
            $suc_msg = $this->lang->line('credit_committee_structure_update');
        }
        else{
            $data['created_date_time'] = currentDate();
            $company_approval_structure_id = $this->Company_model->addApprovalCreditCommitteeStructure($data);
            $suc_msg = $this->lang->line('credit_committee_structure_add');
        }

        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimitSector_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorsList($data);
        $res = array();
        foreach($result as $row){
            $res[] = array(
                'id_product_approval_limit' => '0',
                'amount' => '0',
                'id_sector' => $row['id_sector'],
                'sector_name' => $row['sector_name']
            );
        }
        $total_records=$this->Master_model->getSectorCount();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$res,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function checkApprovalLimitSector_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $committees = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['company_id'], 'id_company_approval_structure' => $data['id_company_approval_structure']));

        $ids = array();
        for($s=0;$s<count($committees);$s++){
            $ids[] = $committees[$s]['id_company_approval_credit_committee'];
        }
        if(!empty($ids))
        {
            $committee_ids = '(';
            $committee_ids .= implode(',',$ids);
            $committee_ids .= ')';
            $data['committees'] = $committee_ids;
        }
        else
        {
            $data['committees'] = '(0)';
        }

        $result = $this->Company_model->getApprovalLimitExceptStatus($data);

        if(!empty($result))
        {
            $this->Company_model->updateApprovalLimitBySector($data,1);
        }
        else
        {
            $add = array();
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            for($s=0;$s<count($branch_type_data);$s++)
            {
                $data['branch_type_id'] = $branch_type_data[$s]['branch_type_id'];
                $committee_data = $this->Company_model->getApprovalCreditCommittee($data);
                for($r=0;$r<count($committee_data);$r++)
                {
                    $add[] = array(
                        'company_id' => $data['company_id'],
                        'company_approval_credit_committee_id' => $committee_data[$r]['id_company_approval_credit_committee'],
                        'sector_id' => $data['sector_id'],
                        'product_id' => $data['product_id'],
                        'amount' => '0',
                        'created_by' => $data['created_by'],
                        'created_date' => currentDate(),
                        'updated_date' => currentDate()
                    );
                }
            }
            if(!empty($add)){ $this->Company_model->addApprovalLimit($add); }
        }
        $result = array('status'=>True, 'message' => '', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimit_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $approval_limit_data = array();
        $product_details = $this->Crm_model->getProduct(array('id_product' => $data['product_id'], 'company_id' => $data['company_id']));

        $data['id_company_approval_structure'] = $product_details[0]['company_approval_structure_id'];

        $approval_structure_details = array(
            'id_company_approval_structure' => $product_details[0]['company_approval_structure_id'],
            'approval_structure_name' => $product_details[0]['approval_structure_name']
        );


        $committees = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['company_id'], 'id_company_approval_structure' => $product_details[0]['company_approval_structure_id']));

        $ids = array();
        for($s=0;$s<count($committees);$s++){
            $ids[] = $committees[$s]['id_company_approval_credit_committee'];
        }
        if(!empty($ids))
        {
            $committee_ids = '(';
            $committee_ids .= implode(',',$ids);
            $committee_ids .= ')';
            $data['committees'] = $committee_ids;
        }
        else
        {
            $data['committees'] = '(0)';
        }

        $sectors = $this->Company_model->getApprovalLimitSectors($data);

        $sector_ids = array_values(array_unique(array_map(function ($i) { return $i['id_sector']; }, $sectors)));
        if(!in_array(0,$sector_ids)){
            array_unshift($sectors, array('id_sector' => '0','sector_name' => 'All Sectors'));
        }

        if(empty($sectors)){
            $sectors[] = array(
                'id_sector' => '0',
                'sector_name' => 'All Sectors'
            );
        }
        unset($data['committees']);

        $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
        if(count($branch_type_data) > 1)
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($branch_type_data);$s++)
        {
            $data['branch_type_id'] = $branch_type_data[$s]['branch_type_id'];
            $this->ordered_committee_data = array();
            $committee_data = $this->Company_model->getApprovalCreditCommittee($data);

            $committee_data = $this->getOrderCommitteeData($committee_data,0);
            //lets check if product data exists or not
            $check_data = $this->Company_model->getApprovalLimit(array('product_id' => $data['product_id'], 'company_id' => $data['company_id']));
            //echo "<pre>"; print_r($check_data); exit;
            if(empty($check_data))
            {   //if no data present, creating all sectors data
                for($r=0;$r<count($committee_data);$r++)
                {
                    $committee_data[$r]['limit'][] = array(
                        'id_product_approval_limit' => '0',
                        'amount' => '0',
                        'id_sector' => '0',
                        'sector_name' => 'All Sectors'
                    );
                }
            }
            else
            {   //if data present, getting data from database

                for($r=0;$r<count($committee_data);$r++)
                {
                    $data['company_approval_credit_committee_id'] = $committee_data[$r]['id_company_approval_credit_committee'];

                    $committee_data[$r]['limit'] = $this->Company_model->getApprovalLimit($data);

                    if(empty($committee_data[$r]['limit'])){
                        for($sr = 0;$sr<count($sectors);$sr++)
                        {
                            $committee_data[$r]['limit'][] = array(
                                'id_product_approval_limit' => '0',
                                'amount' => '0',
                                'id_sector' => $sectors[$sr]['id_sector'],
                                'sector_name' => 'All Sectors'
                            );
                        }
                    }
                    else
                    {

                        $check_sectors = array();
                        for($u=0;$u<count($committee_data[$r]['limit']);$u++)
                        {
                            $check_sectors[] = $committee_data[$r]['limit'][$u]['id_sector'];
                        }

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            if(!in_array($sectors[$sr]['id_sector'],$check_sectors))
                            {
                                $committee_data[$r]['limit'][] = array(
                                    'id_product_approval_limit' => '0',
                                    'amount' => '0',
                                    'id_sector' => $sectors[$sr]['id_sector'],
                                    'sector_name' => 'All Sectors'
                                );
                            }
                        }
                        $temp = array();

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            if($sectors[$sr]['id_sector']==0){
                                $temp = $sectors[$sr];
                                unset($sectors[$sr]);
                                array_values($sectors);
                                array_unshift($sectors,$temp);
                                break;
                            }
                        }
                        $temp = array();
                        //ordering sector wise

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            for($u=0;$u<count($committee_data[$r]['limit']);$u++)
                            {
                                if($sectors[$sr]['id_sector']==$committee_data[$r]['limit'][$u]['id_sector'])
                                    $temp[] = $committee_data[$r]['limit'][$u];
                            }
                        }

                        $committee_data[$r]['limit'] = $temp;
                    }

                    unset($data['company_approval_credit_committee_id']);
                }
            }

            $result[] = array(
                'branch_type_id' => $branch_type_data[$s]['branch_type_id'],
                'branch_type_name' => $branch_type_data[$s]['branch_type_name'],
                'committee' => $committee_data
            );
        }


        for($u=0;$u<count($sectors);$u++)
        {
            if($sectors[$u]['id_sector']==''){ $sectors[$u]['id_sector']='0'; }
            if($sectors[$u]['sector_name']==''){ $sectors[$u]['sector_name']='All sectors'; }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> array( 'data' => $result, 'sectors' => $sectors, 'approval_structure' => $approval_structure_details));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimit_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $details = $data['details'];
        $data = $data['data'];
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $details['id_company'];
        $product_id = $details['product_id'];
        $created_by = $details['created_by'];

        $add = $update = $limit = array();
        for($s=0;$s<count($data);$s++)
        {
            $committee = $data[$s]['committee'];
            for($r=0;$r<count($committee);$r++)
            {
                $committee_id = $committee[$r]['id_company_approval_credit_committee'];
                $limit = $committee[$r]['limit'];
                for($t=0;$t<count($limit);$t++)
                {
                    if(isset($details['updatedSectorId'])){
                        if($limit[$t]['id_sector']==$details['updatedSectorId'])
                        {
                            if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']==0){
                                $add[] = array(
                                    'company_id' => $company_id,
                                    'company_approval_credit_committee_id' => $committee_id,
                                    'sector_id' => $limit[$t]['id_sector'],
                                    'product_id' => $product_id,
                                    'amount' => $limit[$t]['amount'],
                                    'created_by' => $created_by
                                );
                            }
                            else if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']!=0){
                                $update[] = array(
                                    'id_product_approval_limit' => $limit[$t]['id_product_approval_limit'],
                                    'company_id' => $company_id,
                                    'company_approval_credit_committee_id' => $committee_id,
                                    'sector_id' => $limit[$t]['id_sector'],
                                    'product_id' => $product_id,
                                    'amount' => $limit[$t]['amount'],
                                    'created_by' => $created_by
                                );
                            }
                        }
                    }
                    else
                    {
                        if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']==0){
                            $add[] = array(
                                'company_id' => $company_id,
                                'company_approval_credit_committee_id' => $committee_id,
                                'sector_id' => $limit[$t]['id_sector'],
                                'product_id' => $product_id,
                                'amount' => $limit[$t]['amount'],
                                'created_by' => $created_by,
                                'created_date' => currentDate(),
                                'updated_date' => currentDate()
                            );
                        }
                        else if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']!=0){
                            $update[] = array(
                                'id_product_approval_limit' => $limit[$t]['id_product_approval_limit'],
                                'company_id' => $company_id,
                                'company_approval_credit_committee_id' => $committee_id,
                                'sector_id' => $limit[$t]['id_sector'],
                                'product_id' => $product_id,
                                'amount' => $limit[$t]['amount'],
                                'created_by' => $created_by,
                                'updated_date' => currentDate()
                            );
                        }
                    }
                }
            }
        }

        if(!empty($add)){ $this->Company_model->addApprovalLimit($add); }
        if(!empty($update)){ $this->Company_model->updateApprovalLimit($update); }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function approvalLimit_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('product_id', array('required'=>$this->lang->line('project_id_req')));
        $this->form_validator->add_rules('sector_id', array('required'=>$this->lang->line('sector_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Company_model->updateApprovalLimitBySector($data,0);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('approval_limit_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('id_product', array('required'=> $this->lang->line('project_name_req')));
        if(!isset($data['company_pre_approval_structure_id']) && !isset($data['company_approval_structure_id']))
            $this->form_validator->add_rules('company_approval_structure_id', array('required'=> $this->lang->line('approval_struc_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['company_approval_structure_id']))
            $product = array(
                'company_id' => $data['company_id'],
                'id_product' => $data['id_product'],
                'company_approval_structure_id' => $data['company_approval_structure_id'],
                'updated_date' => currentDate()
            );
        else if($data['company_pre_approval_structure_id'])
            $product = array(
                'company_id' => $data['company_id'],
                'id_product' => $data['id_product'],
                'company_pre_approval_structure_id' => $data['company_pre_approval_structure_id'],
                'updated_date' => currentDate()
            );

        $product_id = $this->Crm_model->updateProduct($product);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('product_update'), 'data'=>$product_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function financialStatements_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getFinancialStatements($data);
        if(isset($data['crm_company_id']))
        {
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['status'] = 0;
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $result[$s]['id_financial_statement'], 'reference_type' => 'financial'));
                if(!empty($module_status)){ $result[$s]['status'] = 1; }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function financialExcel_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        /*echo '<pre>';print_r($data);
        echo '<br>';print_r($_FILES);exit;*/
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['financeData'];

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        //$this->form_validator->add_rules('financial_statement_id', array('required'=> $this->lang->line('financial_statement_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        //echo "<pre>"; print_r($validated); exit;
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['financial_statement_comments'])){
            $data['financial_statement_comments'] = '';
        }
        $financial_statement_details = $this->Company_model->getFinancialStatements(array('statement_key' => $data['form_key']));
        $data['financial_statement_id'] = $financial_statement_details[0]['id_financial_statement'];
        if(isset($_FILES) && !empty($_FILES['file']['name'])){
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                /*$result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);*/
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_id = $data['financial_statement_id'];
        $crm_company_id = $data['crm_company_id'];
        $user_id = $data['user_id'];
        $financial_statement_comments = $data['financial_statement_comments'];
        $form_key = $data['form_key'];

        $excel_headers = array_values(excelHeaders($_FILES['file']['tmp_name']['excel'], true));
        $data = excelToArray($_FILES['file']['tmp_name']['excel'], true);

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('excel_empty'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_details = $this->Crm_model->getFormDetails(array('form_key' => $form_key));
        //echo "<pre>"; print_r($form_details); exit;
        $company_financial_statement_data = array('crm_company_id' => $crm_company_id, 'created_by' => $user_id, 'financial_statement_id' => $financial_statement_id, 'financial_statement_comments' => $financial_statement_comments,'created_date_time' => currentDate());
        $company_financial_statement_id = $this->Company_model->addCompanyFinancialStatement($company_financial_statement_data);

        $financial_statement_header_item_id = $financial_statement_final_header = $header_temp_var = $temp = 0; $financial_statement_header_id = array();
        $item_type = '';

        for($s=0;$s<count($data);$s++)
        {
            for($r=0;$r<count($excel_headers);$r++)
            {
                $final_header = '';
                $data[$s][$excel_headers[$r]] = strtolower(trim($data[$s][$excel_headers[$r]]));
                if($data[$s][$excel_headers[$r]]!='')
                {
                    if($r==0)
                    {
                        $header = trim(strtolower($data[$s][$excel_headers[$r]]));
                        $header = explode('-',$header);

                        if($temp)
                        {
                            if(is_numeric($header[0]) || is_float($header[0]))
                            {
                                if (strpos($header[0], $temp.'.') !== false) {
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => $financial_statement_header_id[$temp]));
                                }
                                else if(isset($header[0]) && isset($header[1])){
                                    $temp = $header[0];
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                                    $financial_statement_final_header = 0;
                                }
                            }
                            else
                            {
                                $item_type = '';
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $final_header = $header[0];
                                if(!$financial_statement_final_header)
                                {
                                    if(isset($financial_statement_header_id[$temp]))
                                    {
                                        $financial_statement_final_header = $financial_statement_header_id[$temp]; $header_temp_var = 1;
                                    }
                                    else
                                    {
                                        $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => '', 'parent_header_id' => 0));
                                    }
                                }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_final_header, 'header_item_name' => $final_header, 'item_type' => $item_type));
                                if($header_temp_var==1){ $financial_statement_final_header = 0; $header_temp_var = 0; }
                            }
                        }
                        else
                        {
                            $temp = $header[0];
                            for($sr=1;$sr<count($header);$sr++){
                                if($sr!=1){ $final_header .= '-'; }
                                $final_header .= $header[$sr];
                            }
                            $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                            if(!is_numeric($header[0]))
                            {
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_header_id[$temp], 'header_item_name' => $header[0], 'item_type' => $item_type));
                            }
                        }
                    }
                    else
                    {
                        $this->Company_model->addFinancialItemData(array(
                            'financial_statement_header_item_id' => $financial_statement_header_item_id,
                            'column_header' => $excel_headers[$r],
                            'price' => str_replace(',','',$data[$s][$excel_headers[$r]]),
                            'created_date_time' => currentDate()
                        ));
                    }
                }
            }
        }
    }

    public function financialInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        //$this->form_validator->add_rules('financial_statement_id', array('required'=> $this->lang->line('financial_statement_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_details = $this->Company_model->getFinancialStatements(array('statement_key' => $data['form_key']));

        $financial_statement = $this->Company_model->getCompanyFinancialStatement(array('financial_statement_id' => $financial_statement_details[0]['id_financial_statement'], 'crm_company_id' => $data['crm_company_id']));


        $template = '';
        if($financial_statement_details[0]['statement_key']=='balance_sheet'){
            $template = REST_API_URL.'templates/balance_sheet.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='income_statement'){
            $template = REST_API_URL.'templates/income_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='cash_flow_statement'){
            $template = REST_API_URL.'templates/cash_flow_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='ratio_analysis'){
            $template = REST_API_URL.'templates/ratio_analysis.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='financial_projection_analysis'){
            $template = REST_API_URL.'templates/financial_projection_analysis.xlsx';
        }

        if(empty($financial_statement))
        {
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => '','template' => $template, 'financial_statement_comments' => ''));
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_comments = $financial_statement[0]['financial_statement_comments'];
        $header_names = $this->Company_model->getFinancialHeaders(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));

        $result = $this->Company_model->getFinancialInformation(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));
        $headers = array_keys($result[0]);

        $html = '<div class="col-sm-12 bal-sheet-wrap table-responsive pb20">';
        $html .= '<div class="bal-sheet-header">';
        $html .= '<h4 class="f13 bold dark-blue pt5 pb5 text-uppercase">'.$financial_statement[0]['statement_name'].'</h4>';
        $html .= '</div>';
        $html .= '<table class="table bal-sheet-table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">';
        $html .= '<thead><tr><th></th>';
        for($s=2;$s<count($headers);$s++){
            $html .='<th>'.$headers[$s].'</th>';
        }
        $html .='</tr></thead><tbody>';
        $sr=0;
        for($s=0;$s<count($header_names);$s++)
        {

            if($header_names[$s]['parent_header_id']){ $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><span class="f14 dark-blue text-uppercase" >'.$header_names[$s]['header_name'].'</span></td></tr>'; }
            else { $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><h4 ';
                   if(trim($header_names[$s]['header_name'])!=''){
                        $html .=' class="f13 bold darkblue-bg white-color pt5 pb5 text-uppercase text-center" ';
                   }
                   $html .='>'.$header_names[$s]['header_name'].'</h4></td></tr>'; }

            for($r=0;$r<$header_names[$s]['loop_count'];$r++)
            {
                if(isset($result[$sr]))
                {
                    $html .='<tr';
                    if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="mod-total" '; }
                    $html .='>';
                    for($u=0;$u<count($headers);$u++)
                    {
                        if($u==0){
                            $html .= '<td';
                            if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="text-uppercase" '; }
                            $html .=' title ="'.$result[$sr][$headers[$u]].'" >'.$result[$sr][$headers[$u]].'</td>';
                        }
                        else if($u==1){  }
                        else $html .='<td>'.'$'.number_format($result[$sr][$headers[$u]],2).'</td>';
                    }
                    $html .='</tr>';
                    $sr++;
                }
            }
        }
        $html .='</tbody></table>';

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $html,'template' => $template, 'financial_statement_comments' => $financial_statement_comments));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFinancialInformation_get()
    {
        error_reporting(0);
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_financial_statements = $this->Company_model->getAllCompanyFinancialStatements($data['crm_company_id']);

        for($s=0;$s<count($company_financial_statements);$s++)
        {
            $header_names = $this->Company_model->getFinancialHeaders(array('id_company_financial_statement' => $company_financial_statements[$s]['id_company_financial_statement']));
            $result = $this->Company_model->getFinancialInformation(array('id_company_financial_statement' => $company_financial_statements[$s]['id_company_financial_statement']));
            $headers = array_keys($result[0]);

            $html = '<div class="col-sm-12 bal-sheet-wrap pb20 table-responsive">';
            $html .= '<div class="bal-sheet-header">';
            $html .= '<h4 class="f13 bold dark-blue pt5 pb5 text-uppercase">'.$company_financial_statements[$s]['statement_name'].'</h4>';
            $html .= '</div>';
            $html .= '<table class="table bal-sheet-table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">';
            $html .= '<thead><tr><th></th>';
            for($sr=2;$sr<count($headers);$sr++){
                $html .='<th>'.$headers[$sr].'</th>';
            }
            $html .='</tr></thead><tbody>';
            $sr=0;
            for($st=0;$st<count($header_names);$st++)
            {
                if($header_names[$st]['parent_header_id']){ $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><span class="f14 dark-blue text-uppercase">'.$header_names[$st]['header_name'].'</span></td></tr>'; }
                else { $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><h4 ';
                                          if(trim($header_names[$st]['header_name'])!=''){
                                               $html .=' class="f13 bold darkblue-bg white-color pt5 pb5 text-uppercase text-center" ';
                                          }
                                          $html .='>'.$header_names[$st]['header_name'].'</h4></td></tr>'; }

                for($r=0;$r<$header_names[$st]['loop_count'];$r++)
                {
                    if(isset($result[$sr]))
                    {
                        $html .='<tr';
                        if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="mod-total" '; }
                        $html .='>';
                        for($u=0;$u<count($headers);$u++)
                        {
                            if($u==0){
                                $html .= '<td';
                                if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="text-uppercase" '; }
                                $html .=' title="'.$result[$sr][$headers[$u]].'" >'.$result[$sr][$headers[$u]].'</td>';
                            }
                            else if($u==1){  }
                            else $html .='<td>'.'$'.number_format($result[$sr][$headers[$u]],2).'</td>';
                        }
                        $html .='</tr>';
                        $sr++;
                    }
                }
            }
            $html .='</tbody></table>';
            $company_financial_statements[$s]['sheet'] = $html;
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$company_financial_statements);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function assessmentQuestion_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['collateral_stage_id'])){ $data['collateral_stage_id'] = 0; }

        if(isset($data['id_assessment_question']))
        {
            $result = $this->Company_model->getAssessmentQuestion($data);
        }
        else
        {
            $result = $this->Company_model->getAssessmentQuestionType($data);
            $order = 'DESC';
            if($result[0]['assessment_question_type_key']=='pre_disbursement_checklist'){ $order = 'ASC'; }
            for($s=0;$s<count($result);$s++)
            {
                if(!isset($data['product_id'])){ $data['product_id']=0; }
                if(!isset($data['collateral_type_id'])){ $data['collateral_type_id']=0; }
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'product_id' => $data['product_id'], 'collateral_type_id' => $data['collateral_type_id'], 'collateral_stage_id' => $data['collateral_stage_id'], 'order' => $order));
                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category']));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestionCategory_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('assessment_question_type_id', array('required'=> $this->lang->line('question_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getAssessmentQuestionCategory($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestionCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('assessment_question_category_name', array('required'=> $this->lang->line('company_name_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_type_id = $collateral_stage_id = 0;
        if(isset($data['collateral_type_id'])){ $collateral_type_id = $data['collateral_type_id']; }
        if(isset($data['collateral_stage_id'])){ $collateral_stage_id = $data['collateral_stage_id']; }

        if(isset($data['id_assessment_question_category'])){
            $check_name = $this->Company_model->getAssessmentQuestionCategory(array('company_id' => $data['company_id'], 'assessment_question_category_name' => trim($data['assessment_question_category_name']),'id_assessment_question_category_not' => $data['id_assessment_question_category'], 'collateral_type_id' =>$collateral_type_id,'collateral_stage_id' => $collateral_stage_id,'assessment_question_type_id' => $data['assessment_question_type_id'],'product_id'=>$data['product_id']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_name' => $this->lang->line('name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $result = $this->Company_model->updateAssessmentQuestionCategory($data);
            $suc_msg = $this->lang->line('question_category_update');
        }
        else
        {
            $check_name = $this->Company_model->getAssessmentQuestionCategory(array('company_id' => $data['company_id'], 'assessment_question_category_name' => trim($data['assessment_question_category_name']),'collateral_type_id' =>$collateral_type_id,'collateral_stage_id' => $collateral_stage_id,'assessment_question_type_id' => $data['assessment_question_type_id'],'product_id'=>$data['product_id']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_name' => $this->lang->line('name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $result = $this->Company_model->addAssessmentQuestionCategory($data);
            $suc_msg = $this->lang->line('question_category_add');
        }

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestion_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['question_option'])){ $data['question_option'] = json_encode($data['question_option']); }

        $this->form_validator->add_rules('assessment_question_category_id', array('required'=> $this->lang->line('question_category_id_req')));
        $this->form_validator->add_rules('assessment_question', array('required'=> $this->lang->line('question_req')));
        $this->form_validator->add_rules('question_type', array('required'=> $this->lang->line('question_type_req')));
        $this->form_validator->add_rules('question_option', array('required'=> $this->lang->line('question_option_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_assessment_question'])) {
            $check_name = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $data['assessment_question_category_id'], 'assessment_question' => trim($data['assessment_question']),'id_assessment_question_not' => $data['id_assessment_question']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_id' => 'Question already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateAssessmentQuestion($data);
            $suc_msg = 'Question updated successfully.';
        }
        else {
            $check_name = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $data['assessment_question_category_id'], 'assessment_question' => trim($data['assessment_question'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_id' => 'Question already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['created_date_time'] = currentDate();
            $this->Company_model->addAssessmentQuestion($data);
            $suc_msg = $this->lang->line('question_add');
        }
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessment_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getAssessmentForCompany($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function allCompanyAssessments_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_assessment = $this->Company_model->getCompanyAssessment($data);
        $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
        for($st=0;$st<count($company_assessment);$st++)
        {
            $data['type'] = $company_assessment[$st]['assessment_type'];
            $data['assessment_id'] = $company_assessment[$st]['id_assessment'];
            $data['assessment_key'] = $company_assessment[$st]['assessment_key'];

            if($data['type']=='item' || $data['type']=='item-canvas')
            {
                $company_assessment[$st]['data'] = $this->Company_model->getCompanyAssessment($data);
                for($s=0;$s<count($company_assessment[$st]['data']);$s++)
                {
                    $company_assessment[$st]['data'][$s]['item'] = $this->Company_model->getAssessmentItem(array('assessment_id' => $company_assessment[$st]['data'][$s]['assessment_id']));
                    for($r=0;$r<count($company_assessment[$st]['data'][$s]['item']);$r++)
                    {
                        $company_assessment[$st]['data'][$s]['item'][$r]['step'] = $this->Company_model->getCompanyAssessmentItemStep(array('crm_company_id' => $data['crm_company_id'], 'assessment_item_id' =>$company_assessment[$st]['data'][$s]['item'][$r]['id_assessment_item']));
                    }
                }
            }
            else if($data['type']='question')
            {
                $company_assessment[$st]['data'] = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['assessment_key']));
                for($s=0;$s<count($company_assessment[$st]['data']);$s++)
                {
                    $company_assessment[$st]['data'][$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $company_assessment[$st]['data'][$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $crm_company_details[0]['sector_id'], 'sub_sector_id' => $crm_company_details[0]['sub_sector_id']));
                    for($r=0;$r<count($company_assessment[$st]['data'][$s]['category']);$r++)
                    {
                        $company_assessment[$st]['data'][$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $company_assessment[$st]['data'][$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                        for($sr=0;$sr<count($company_assessment[$st]['data'][$s]['category'][$r]['question']);$sr++)
                        {
                            if(isset($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']) && $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']!=''){
                                $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']);
                            }
                            $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_company_id' => $data['crm_company_id']));

                            if(isset($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                                if($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                    $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                                else if($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                    $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer,'file');
                            }
                        }
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$company_assessment);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessment_get()
    {
        $data = $this->input->get();
//echo "<pre>"; print_r($data); exit;
        if(isset($data['type']) && $data['type']=='question') {

            $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('crm_company_id')));
            $this->form_validator->add_rules('form_key', array('required' => $this->lang->line('form_key_req')));
        }
        else if(isset($data['type']) && $data['type']=='item') {
            $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
            //$this->form_validator->add_rules('assessment_id', array('required' => $this->lang->line('assessment_id_req')));
            $this->form_validator->add_rules('form_key', array('required' => $this->lang->line('form_key_req')));

        }
        else{
            $this->form_validator->add_rules('type', array('required' => $this->lang->line('type_req')));
        }


        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $assessment_details =  $this->Company_model->getAssessment(array('assessment_key' => $data['form_key']));
        $data['assessment_id'] = $assessment_details[0]['id_assessment'];
        $data['assessment_key'] = $data['form_key'];

        if(!isset($data['type'])) {
            $result = $this->Company_model->getCompanyAssessment($data);
            if(isset($data['crm_company_id']))
            {
                for($s=0;$s<count($result);$s++)
                {
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $result[$s]['assessment_id'], 'reference_type' => 'assessment'));
                    if(empty($module_status)){ $result[$s]['status'] = 0; }
                    else{ $result[$s]['status'] = 1; }
                }
            }
        }

        else if($data['type']=='item' || $data['type']=='item-canvas')
        {
            $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
            $data['company_id'] = $crm_company_details[0]['company_id'];
            $result = $this->Company_model->getCompanyAssessment($data);
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['item'] = $this->Company_model->getAssessmentItem(array('assessment_id' => $result[$s]['assessment_id']));
                for($r=0;$r<count($result[$s]['item']);$r++)
                {
                    $result[$s]['item'][$r]['step'] = $this->Company_model->getCompanyAssessmentItemStep(array('crm_company_id' => $data['crm_company_id'], 'assessment_item_id' =>$result[$s]['item'][$r]['id_assessment_item']));
                }
            }
        }
        else if($data['type']='question')
        {
            $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
            $data['company_id'] = $crm_company_details[0]['company_id'];
            $result = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['assessment_key']));
            
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $crm_company_details[0]['sector_id'], 'sub_sector_id' => $crm_company_details[0]['sub_sector_id']));
                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                        $result[$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_company_id' => $data['crm_company_id']));

                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                            if($result[$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                            else if($result[$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                        }
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessment_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            if(!isset($data['assessments']) && empty($data['assessments'])){
                $result = array('status'=>FALSE,'error'=>$this->lang->line('assessment_select'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        $company_id = $data['company_id'];
        $assessments = $data['assessments'];
        $created_by = $data['created_by'];
        $add = $update = $delete = array();

        $total_company_assessments = $this->Company_model->getCompanyAssessment(array('company_id' => $company_id, 'company_assessment_status' => 'all'));
        $total_company_assessments = array_values(array_unique(array_map(function ($i) { return $i['assessment_id']; }, $total_company_assessments)));
        for($s=0;$s<count($assessments);$s++)
        {
            $check_company_assessment = $this->Company_model->getCompanyAssessment(array('company_id' => $company_id, 'assessment_id' => $assessments[$s],'company_assessment_status' => 'all'));
            if(empty($check_company_assessment)){
                $add[] = array(
                    'assessment_id' => $assessments[$s],
                    'company_id' => $company_id,
                    'created_by' => $created_by,
                    'created_date_time' => currentDate()
                );
            }
            else{
                $update[] = array(
                    'id_company_assessment' => $check_company_assessment[0]['id_company_assessment'],
                    'assessment_id' => $assessments[$s],

                    'company_id' => $company_id,
                    'created_by' => $created_by,
                    'company_assessment_status' => 1
                );
            }

            for($r=0;$r<count($total_company_assessments);$r++){
                if($assessments[$s]==$total_company_assessments[$r]){ unset($total_company_assessments[$r]); $total_company_assessments = array_values($total_company_assessments); }
            }

        }

        if(!empty($total_company_assessments)){
            foreach($total_company_assessments as $assessment_id){
                $delete[] = array(

                    'company_id' => $company_id,
                    'assessment_id' => $assessment_id,
                    'company_assessment_status' => 0
                );
            }
        }

        if(!empty($add)){ $this->Company_model->addCompanyAssessment($add); }
        if(!empty($update)){ $this->Company_model->updateCompanyAssessment($update); }

        foreach($delete as $item){
            $this->Company_model->updateCompanyAssessmentByAssessmentId($item);
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('assessment_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessmentItemStep_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('assessment_item_id', array('required'=> $this->lang->line('assessment_item_id_req')));
        $this->form_validator->add_rules('step_title', array('required'=> $this->lang->line('step_title_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
        unset($data['form_key']);

        $assessment_id = 0;
        if(isset($data['assessment_id'])){ $assessment_id = $data['assessment_id']; unset($data['assessment_id']); }
        $data['created_date_time'] = currentDate();
        $this->Company_model->addCompanyAssessmentItemStep($data);

        /*if(isset($data['crm_company_id']) && isset($data['assessment_id']))
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $data['crm_company_id'], 'activity_type' => 'assessment','activity_reference_id' => $data['assessment_id'],'created_by' => $data['created_by'],'created_date_time' => currentDate()));
        }*/

        /*if($assessment_id){*/
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form']));
            if(empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $data['crm_company_id'], 'activity_type' => 'assessment','activity_reference_id' => $assessment_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));
        /*}*/

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('succ_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessmentItemStep_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_company_assessment_item_step', array('required'=> $this->lang->line('item_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteCompanyAssessmentItemStep($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('succ_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentAnswer_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(isset($_FILES['file']['name'])){
            $data = $data['answer'];
        }
        else if(isset($data['answer'])){
            $data = $data['answer'];
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('assessment_question_id', array('required'=> $this->lang->line('question_id_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));

        $this->form_validator->add_rules('crm_reference_id', array('required'=> $this->lang->line('crm_reference_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['project_facility_id']) && $data['project_facility_id']=='null'){
            unset($data['project_facility_id']);
        }

        $crm_project_id = $crm_company_id = $id_assessment_question_type = $assessment_id = 0;
        $form_key = $data['form_key'];
        unset($data['form_key']);
        $project_checklist_id  = $company_id = $project_stage_section_form_id = 0;
        $project_facility_id = array();
        if(isset($data['project_facility_id'])){ $project_facility_id = $data['project_facility_id']; unset($data['project_facility_id']); }
        if(isset($data['project_checklist_id'])){ $project_checklist_id = $data['project_checklist_id']; unset($data['project_checklist_id']); }
        if(isset($data['company_id'])){ $company_id = $data['company_id']; unset($data['company_id']); }

        if(isset($data['crm_project_id'])){ $crm_project_id = $data['crm_project_id']; unset($data['crm_project_id']); }
        if(isset($data['crm_company_id'])){ $crm_company_id = $data['crm_company_id']; unset($data['crm_company_id']); }
        if(isset($data['id_assessment_question_type'])){ $id_assessment_question_type = $data['id_assessment_question_type']; unset($data['id_assessment_question_type']); }
        if(isset($data['assessment_id'])){ $assessment_id = $data['assessment_id']; unset($data['assessment_id']); }
        if(isset($data['project_stage_section_form_id'])){ $project_stage_section_form_id = $data['project_stage_section_form_id']; unset($data['project_stage_section_form_id']); }

        if(!isset($data['comment'])){ $data['comment'] = ''; }
        if($crm_project_id) {
            $form_details = $this->Project_model->getFormDetails(array('form_key' => $form_key));
            $form_details[0]['id_form'] = $form_details[0]['id_project_form'];
        }
        else {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $form_key));
        }

        if(isset($_FILES) && !empty($_FILES['file']['name']) && !isset($_FILES['file']['name']['attachments']))
        {
            $path='uploads/';
            $fileName = doUpload($_FILES['file']['tmp_name'],$_FILES['file']['name'],$path,$company_id,'');
            if(isset($data['type']) && $data['type']=='media'){
                $data['comment_file'] = $fileName;
            }
            else{
                $data['assessment_answer'] = $fileName;
            }

        }
        unset($data['company_id']); unset($data['type']);
        $question_details = $this->Company_model->getAssessmentQuestion(array('id_assessment_question' => $data['assessment_question_id']));

        if($question_details[0]['question_type']=='checkbox')
            if(isset($data['assessment_answer'])){ $data['assessment_answer'] = json_encode($data['assessment_answer']); }

        if(isset($data['id_assessment_answer'])) {
            $answer_id = $data['id_assessment_answer'];
            $this->Company_model->updateAssessmentAnswer($data);
            $suc_msg = $this->lang->line('answer_update');
        }
        else {
            $data['created_date_time'] = currentDate();
            $answer_id = $this->Company_model->addAssessmentAnswer($data);
            $suc_msg = $this->lang->line('answer_add');
        }

        $path='uploads/'; $data['document_type'] = 0;

        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $answer_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => $form_key,'form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $answer_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => $form_key,'form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        if($crm_project_id)
        {
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'assessment','activity_reference_id' => $id_assessment_question_type,'created_by' => $data['created_by'],'created_date_time' => currentDate()));
            if(isset($data['assessment_answer']) && $data['assessment_answer']!=''){
                $crm_project_data = $this->Crm_model->getProject($crm_project_id);
                if(!empty($crm_project_data) && strtolower($crm_project_data[0]['project_status']=='approved')) {
                    $this->Crm_model->updateProject(array('project_status' => 'monitoring'), $crm_project_id);
                }
            }


            $product_term_details = $this->Company_model->getProductTerms(array('id_product_term' => $id_assessment_question_type));
            if(!empty($product_term_details)){
                $this->Activity_model->addActivity(array('activity_name' => $product_term_details[0]['product_term_name'],'activity_template' => 'Information updated','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'term','activity_reference_id' =>'','created_by' => $data['created_by'],'created_date_time' => currentDate()));
            }

            if($project_stage_section_form_id) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_project_id, 'crm_module_type' => 'project', 'reference_id' => $project_stage_section_form_id, 'reference_type' => 'form'));
                if(empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $crm_project_id, 'crm_module_type' => 'project', 'reference_id' => $project_stage_section_form_id, 'reference_type' => 'form', 'created_by' => $data['created_by'],'created_date_time' => currentDate(),'updated_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $crm_project_id, 'project_stage_section_form_id' => $project_stage_section_form_id));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $crm_project_id, 'project_stage_section_form_id' => $project_stage_section_form_id, 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
            }
        }
        else if($crm_company_id)
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' =>currentDate(), 'updated_date_time' => currentDate()));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $crm_company_id, 'activity_type' => 'assessment','activity_reference_id' => $assessment_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $crm_company_id,'project_stage_section_form_id' => $form_details[0]['id_form']));
            if(empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $crm_company_id,'project_stage_section_form_id' => $form_details[0]['id_form'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        }

        if($project_checklist_id && !empty($project_facility_id)){

            $existing_checklist_facility_list = $this->Crm_company_model->getChecklistFacility(array('project_checklist_id' => $project_checklist_id));
            $existing_checklist_facility_list = array_map(function($i){ return $i['project_facility_id']; },$existing_checklist_facility_list);
            if($project_facility_id && !empty($project_facility_id)) {
                $delete = array_diff($existing_checklist_facility_list, $project_facility_id);
                foreach ($delete as $i) {
                    $this->Crm_company_model->deleteChecklistFacility(array('project_checklist_id' => $project_checklist_id, 'project_facility_id' => $i));
                }
            }

            foreach($project_facility_id as $i ) {
                if($i!='' && $i!=0) {
                    $existing_checklist_facility = $this->Crm_company_model->getChecklistFacility(array('project_checklist_id' => $project_checklist_id, 'project_facility_id' => $i));

                    if (empty($existing_checklist_facility)) {
                        $this->Crm_company_model->addChecklistFacility(array('project_checklist_id' => $project_checklist_id, 'project_facility_id' => $i));
                    } else {
                        $this->Crm_company_model->updateChecklistFacility(array('id_project_checklist_facility' => $existing_checklist_facility[0]['id_project_checklist_facility'], 'project_facility_id' => $i));
                    }
                }
            }
            //exit;
        }
        else if($project_checklist_id && empty($project_facility_id)){
            $this->Crm_company_model->deleteChecklistFacility(array('project_checklist_id' => $project_checklist_id));
        }

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentAnswer_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_assessment_answer', array('required'=> $this->lang->line('assessment_answer_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteAssessmentAnswer($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('succ_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTerm_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getProductTerms($data);
        $status = array_values(array_unique(array_map(function($i){ return $i['term_project_status']; }, $result)));
        if(isset($data['crm_project_id']))
        {
            $res = array();
            for($s=0;$s<count($result);$s++)
            {
                //getting status(user entered data or not)
                $result[$s]['status'] = 0;
                if($result[$s]['product_term_key']=='risk_assessment') {
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'risk'));
                    if(!empty($module_status)){ $result[$s]['status'] = 1; }
                }
                if($result[$s]['product_term_key']=='collateral_link') {
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'collateral'));
                    if(!empty($module_status)){ $result[$s]['status'] = 1; }
                }
                else if(($result[$s]['product_term_key']=='pre_disbursement_checklist')||($result[$s]['product_term_key']=='restriction_limit')){
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_product_term'], 'reference_type' => 'assessment'));
                    if(!empty($module_status)){ $result[$s]['status'] = 1; }
                }
                else if(($result[$s]['product_term_key']=='esme_monitoring_and_reporting')|| ($result[$s]['product_term_key']=='special_servicing_reports') || ($result[$s]['product_term_key']=='initial_supervision_report') || ($result[$s]['product_term_key']=='annual_review') || ($result[$s]['product_term_key']=='ongoing_review') || ($result[$s]['product_term_key']=='other_reports')){
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_product_term'], 'reference_type' => 'monitoring'));
                    if(!empty($module_status)){ $result[$s]['status'] = 1; }
                }
                else {
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_product_term'], 'reference_type' => 'term'));
                    if(!empty($module_status)){ $result[$s]['status'] = 1; }
                }

                //getting each form comments
                    $result[$s]['comments_count'] = $this->Crm_model->getFormCommentsCount(array('form_key' => $result[$s]['product_term_key'],'reference_type' => 'project','reference_id' => $data['crm_project_id']));

                //getting ordered data
                if($result[$s]['product_term_parent_id']!=0){
                    if(!isset($res[$result[$s]['term_project_status']][$result[$s]['product_term_parent_id']])){ $res[$result[$result[$s]['term_project_status']][$s]['product_term_parent_id']] = array(); }
                    $res[$result[$s]['term_project_status']][$result[$s]['product_term_parent_id']]['child'][] = $result[$s];
                }
                else $res[$result[$s]['term_project_status']][$result[$s]['id_product_term']] = $result[$s];
            }

            foreach($status as $r){
                $res[$r] = array_values($res[$r]);
            }

        }
        else
            $res = $result;
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTermItem_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('product_term_key', array('required'=> $this->lang->line('Product_term_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $term = $this->Company_model->getProductTerms($data);
        $result = array();
        if(!empty($term))
            $result = $this->Company_model->getProductTermItems(array('product_term_id' => $term[0]['id_product_term']));
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //company risk
    public function riskCategory_get()
    {
        $data = $this->input->get();
        $data['type']=isset($data['type'])?$data['type']:'company';
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'type'=>$data['type']));
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type'=>$data['type']));
            for($r=0;$r<count($result[$s]['sub_category']);$r++)
            {
                $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type'=>$data['type']));
                for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                {
                    /*$result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'] = $this->Company_model->getRiskCategoryItemsSectors(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));
                    for($h=0;$h<count($result[$s]['sub_category'][$r]['attributes'][$u]['sector_list']);$h++)
                    {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['items'] = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['risk_category_id'], 'sector_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['id_sector']));
                    }*/
                    $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));

                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $data['type']=isset($data['type'])?$data['type']:'company';
        $this->form_validator->add_rules('risk_category_name', array('required'=> $this->lang->line('category_name_req')));
        $this->form_validator->add_rules('parent_risk_category_id', array('required'=> $this->lang->line('parent_category_id_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('risk_percentage', array('required'=> $this->lang->line('risk_percentage_req')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['sector_id'])){ $data['sector_id'] = ''; }

        if(!isset($data['id_risk_category']))
        {
            $check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'parent_risk_category_id' =>$data['parent_risk_category_id'], 'company_id' => $data['company_id'],'sector_id' => $data['sector_id'],'type' => $data['type']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => $this->lang->line('name_duplicate')) ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $check_percentage = $this->Company_model->getTotalRiskCategoryPercentage(array('parent_risk_category_id' => $data['parent_risk_category_id'], 'company_id' => $data['company_id'],'sector_id' => $data['sector_id'],'type' => $data['type']));

            if(!empty($check_percentage) && ($check_percentage[0]['risk_percentage']+$data['risk_percentage'])>100){
                $result = array('status'=>FALSE,'error'=>array('risk_percentage' => $this->lang->line('risk_percentage_exceeds')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $risk_category_id = $this->Company_model->addRiskCategory($data);
            $suc_msg = $this->lang->line('risk_category_add');
        }
        else
        {
            $check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'company_id' => $data['company_id'], 'risk_category_id_not' => $data['id_risk_category'],'sector_id' => $data['sector_id'],'parent_risk_category_id' => $data['parent_risk_category_id'],'type' => $data['type']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => $this->lang->line('name_duplicate')) ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $check_percentage = $this->Company_model->getTotalRiskCategoryPercentage(array('parent_risk_category_id' => $data['parent_risk_category_id'],'company_id' => $data['company_id'], 'id_risk_category' => $data['id_risk_category'],'sector_id' => $data['sector_id'],'type' => $data['type']));

            if(($check_percentage[0]['risk_percentage']+$data['risk_percentage'])>100){
                $result = array('status'=>FALSE,'error'=>array('risk_percentage' => $this->lang->line('risk_percentage_exceeds')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateRiskCategory($data);
            $risk_category_id = $data['id_risk_category'];
            $suc_msg = $this->lang->line('risk_category_update');
        }

        $result = $this->Company_model->getRiskCategories(array('risk_category_id' => $risk_category_id));

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategoryItem_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('risk_category_id', array('required'=> $this->lang->line('category_id_req')));
        $this->form_validator->add_rules('risk_category_item_name', array('required'=> $this->lang->line('category_item_name_req')));
        $this->form_validator->add_rules('risk_category_item_grade', array('required'=> $this->lang->line('category_item_grade_req')));
        //$this->form_validator->add_rules('sector_id', array('required'=> $this->lang->line('sector_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_risk_category_item']))
        {
            /*$check_name = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $data['risk_category_id'], 'risk_category_item_name' => trim($data['risk_category_item_name']),'sector_id' => $data['sector_id']));

            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => $this->lang->line('name_duplicate')) ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/

            $risk_category_item_id = $this->Company_model->addRiskCategoryItem($data);
            $suc_msg = $this->lang->line('risk_category_item_add');
        }
        else
        {
            /*$check_name = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $data['risk_category_id'], 'risk_category_item_name' => trim($data['risk_category_item_name']), 'id_risk_category_item_not' => $data['id_risk_category_item']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => $this->lang->line('name_duplicate')) ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/

            $this->Company_model->updateRiskCategoryItem($data);
            $risk_category_item_id = $data['id_risk_category_item'];
            $suc_msg = $this->lang->line('risk_category_item_update');
        }
        $result = $this->Company_model->getRiskCategoryItems(array('risk_category_item_id' => $risk_category_item_id));
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategory_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        else if($data['type']=='category' || $data['type']=='sub_category' || $data['type']=='attribute' || $data['type']=='sector')
        {
            $this->form_validator->add_rules('risk_category_id', array('required'=> $this->lang->line('risk_category_id_req')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateRiskCategory(array('id_risk_category' => $data['risk_category_id'], 'risk_category_status' => 0));
        }
        else if($data['type']=='item')
        {
            $this->form_validator->add_rules('risk_category_item_id', array('required'=> $this->lang->line('risk_category_item_id_req')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->deleteRiskCategoryItem(array('id_risk_category_item' => $data['risk_category_item_id']));
        }
        else{
            $result = array('status'=>FALSE,'error'=>array('type' => $this->lang->line('invalid_type')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('succ_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledge_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //$data = $data['data'];
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('document_title', array('required'=> $this->lang->line('document_title_req')));
        $this->form_validator->add_rules('document_type', array('required'=> $this->lang->line('document_type_req')));
        $this->form_validator->add_rules('uploaded_by', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('knowledge_document_status', array('required'=> $this->lang->line('document_status_req')));
        $this->form_validator->add_rules('tags', array('required'=> $this->lang->line('tags_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $title = $source = '';
        //echo "<pre>"; print_r($_FILES); exit;
        if(!isset($data['document_description'])){ $data['document_description'] = ''; }
        if($data['document_type']=='document')
        {
            if(isset($data['id_knowledge_document']) && empty($_FILES))
            {

            }
            else
            {
                $path='uploads/';
                if(isset($_FILES) && !empty($_FILES['file']['name']['document']))
                {
                    $title = $_FILES['file']['name']['document'];
                    $fineName = doUpload($_FILES['file']['tmp_name']['document'][0],$_FILES['file']['name']['document'][0],$path,$data['company_id'],'');

                    $source = $fineName;
                }
                else
                {
                    $result = array('status'=>FALSE,'error'=>array('document' => $this->lang->line('upload_document')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        else if($data['document_type']=='video')
        {
            $source = $data['document_video'];
        }
        else if($data['document_type']=='news')
        {
            $source = $data['document_video'];
        }

        if(isset($data['id_knowledge_document'])){
            if($data['document_type']=='document' && $source=='')
            {
                $document_id = $this->Company_model->updateKnowledgeDocument(array(
                    'id_knowledge_document' => $data['id_knowledge_document'],
                    'document_title' => $data['document_title'],
                    'document_type' => $data['document_type'],
                    'document_description' => $data['document_description'],
                    'uploaded_by' => $data['uploaded_by'],
                    'company_id' => $data['company_id'],
                    'knowledge_document_status' => $data['knowledge_document_status']
                ));
            }
            else
            {
                $document_id = $this->Company_model->updateKnowledgeDocument(array(
                    'id_knowledge_document' => $data['id_knowledge_document'],
                    'document_title' => $data['document_title'],
                    'document_type' => $data['document_type'],
                    'document_source' => $source,
                    'document_description' => $data['document_description'],
                    'uploaded_by' => $data['uploaded_by'],
                    'company_id' => $data['company_id'],
                    'knowledge_document_status' => $data['knowledge_document_status']
                ));
            }

            $suc_msg = $this->lang->line('document_update');
        }
        else{
            $document_id = $this->Company_model->addKnowledgeDocument(array(
                'document_title' => $data['document_title'],
                'document_type' => $data['document_type'],
                'document_source' => $source,
                'document_description' => $data['document_description'],
                'uploaded_by' => $data['uploaded_by'],
                'company_id' => $data['company_id'],
                'knowledge_document_status' => $data['knowledge_document_status'],
                'created_date_time' => currentDate()
            ));
            $suc_msg = $this->lang->line('document_add');
        }


        $tags = array();
        for($s=0;$s<count($data['tags']);$s++){
            $tags[] = array(
                'knowledge_document_id' => $document_id,
                'tag_name' => $data['tags'][$s],
                'created_date_time' => currentDate()
            );
        }
        if(isset($data['id_knowledge_document'])){
            $previous_tags = $this->Company_model->getKnowledgeDocumentTags(array('knowledge_document_id' => $data['id_knowledge_document']));
            $previous_tag_name = array_values(array_unique(array_map(function ($i) { return $i['tag_name']; }, $previous_tags)));
            $add = $delete = array();
            $tags_lower = array_map('strtolower', $data['tags']);
            $previous_tag_name = array_map('strtolower', $previous_tag_name);

            for($s=0;$s<count($data['tags']);$s++){
                if(trim($tags_lower[$s])!='')
                {
                    if(!in_array($tags_lower[$s],$previous_tag_name)){
                        $add[] = array(
                            'knowledge_document_id' => $data['id_knowledge_document'],
                            'tag_name' => $data['tags'][$s],
                            'created_date_time' => currentDate()
                        );
                    }
                }
            }

            for($s=0;$s<count($previous_tags);$s++){
                if(!in_array(trim(strtolower($previous_tags[$s]['tag_name'])),$tags_lower)){
                    array_push($delete,$previous_tags[$s]['id_knowledge_document_tag']);
                }
            }

            if(!empty($add)){ $this->Company_model->addKnowledgeDocumentTags($add); }
            if(!empty($delete)){ $this->Company_model->deleteKnowledgeTags($delete); }
        }
        else{
            $this->Company_model->addKnowledgeDocumentTags($tags);
        }
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeTags_get()
    {
        $data = $this->input->get();
        $sector = $this->Master_model->getSectorsList(array());
        $assessment = $this->Company_model->getAssessment();
        $forms = $this->Company_model->getForms();
        $financial = $this->Company_model->getFinancialStatements(array());
        $term = $this->Company_model->getProductTerms(array());

        $result = $result1 = array();
        for($s=0;$s<count($sector);$s++)
        {
            $result[]['tag'] = $sector[$s]['sector_name'];
        }
        for($s=0;$s<count($assessment);$s++){
            $result[]['tag'] = $assessment[$s]['assessment_name'];
        }
        for($s=0;$s<count($forms);$s++){
            $result[]['tag'] = $forms[$s]['form_name'];
        }
        for($s=0;$s<count($financial);$s++){
            $result[]['tag'] = $financial[$s]['statement_name'];
        }
        for($s=0;$s<count($term);$s++){
            $result[]['tag'] = $term[$s]['product_term_name'];
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data['tags'] = array();
        if(isset($data['search_tags'])){
            $data['search_tags'] = json_decode($data['search_tags']);
            if(!empty($data['search_tags'])) {
                $tags = '(';
                for ($s = 0; $s < count($data['search_tags']); $s++) {
                    if ($s > 0) {
                        $tags .= ',';
                    }
                    $tags .= '"' . $data['search_tags'][$s] . '"';
                }
                $tags .= ')';
                $data['search_tags'] = $data['search_tags'] = $tags;
            }
            else{
                unset($data['search_tags']);
            }
        }

        if(!isset($data['created_by'])){
            if(isset($_SERVER['HTTP_USER'])){
                $data['created_by'] = $_SERVER['HTTP_USER'];
            }
        }

        if(isset($data['f_type']) && $data['f_type']=='all_documents'){
            $search_results = $this->Company_model->getKnowledgeDocuments($data);
        }
        else {
            $search_results = $this->Company_model->getKnowledgeDocuments($data);
        }
        unset($data['limit']);
        unset($data['offset']);
        if(isset($data['f_type']) && $data['f_type']=='all_documents'){
            $total_records = count($this->Company_model->getKnowledgeDocuments($data));
        }
        else {
            $total_records = count($this->Company_model->getKnowledgeDocuments($data));
        }
        unset($data['f_type']);

        for($s=0;$s<count($search_results);$s++){
            $search_results[$s]['tag_names'] = str_replace('$$',',',$search_results[$s]['tags']);
            $search_results[$s]['tags'] = explode('$$',$search_results[$s]['tags']);
            $search_results[$s]['document_video'] = '';
            if($search_results[$s]['document_type']=='video')
            {
                $search_results[$s]['document_video'] = $search_results[$s]['document_source'];
                $search_results[$s]['document_source'] = '';
            }
            if($search_results[$s]['document_type']=='news')
            {
                $search_results[$s]['document_video'] = $search_results[$s]['document_source'];
                $search_results[$s]['document_source'] = '';
            }

        }

        /*$total_records = count($this->Company_model->getKnowledgeDocuments(array('search_key' => $data['tags'], 'company_id' => $data['company_id'])));*/
        $my = $all = 0;

        if(!isset($data['created_by'])){
            if(isset($_SERVER['HTTP_USER'])){
                $data['created_by'] = $_SERVER['HTTP_USER'];
            }
        }
        $data['f_type']='my_documents';
        $my = count($this->Company_model->getKnowledgeDocuments($data));
        //echo $this->db->last_query(); exit;
        $data['f_type']='all_documents';
        $all = count($this->Company_model->getKnowledgeDocuments($data));



        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $search_results, 'total_records' => $total_records,'all_docs'=>$all, 'my_docs'=>$my));
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function knowledge_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['search_key']))
        {
            $search_results = $this->Company_model->getKnowledgeDocuments($data);
            $total_records = $this->Company_model->getKnowledgeDocuments(array('search_key' => $data['search_key'], 'company_id' => $data['company_id']));
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $search_results, 'total_records' => $total_records));
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $document_type = array(
            array(
                'name' => 'Total',
                'label' => 'Total Files',
                'total' => 0
            ),
            array(
                'name' => 'document',
                'label' => 'Documents',
                'total' => 0
            ),
            array(
                'name' => 'blog',
                'label' => 'Blog Notes',
                'total' => 0
            ),
            array(
                'name' => 'news',
                'label' => 'News Items',
                'total' => 0
            ),
            array(
                'name' => 'video',
                'label' => 'Videos',
                'total' => 0
            )
        );

        if(isset($data['created_by']))
            $knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'limit' => 5, 'created_by' => $data['created_by']));
        else
            $knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'limit' => 5));

        if(isset($data['created_by']))
            $total_knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'created_by' => $data['created_by']));
        else
            $total_knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id']));
        $total_records_count = count($this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'],'knowledge_document_status' => '1')));
        $document_type_records = $this->Company_model->getDocumentTypeKnowledgeDocuments(array('company_id' => $data['company_id'],'knowledge_document_status' => '1'));
        //echo "<pre>"; print_r($document_type_records); exit;
        for($s=0;$s<count($document_type_records);$s++)
        {
            for($r=0;$r<count($document_type);$r++)
            {
                if($document_type_records[$s]['document_type']==$document_type[$r]['name']){
                    $document_type[$r]['total'] = $document_type_records[$s]['total'];
                }
            }
        }

        for($s=0;$s<count($document_type);$s++)
        {
            if($document_type[$s]['name']=='Total')
                $document_type[$s]['total'] = $total_records_count;
        }
        $users = $this->Company_model->getKnowledgeDocumentsUsers(array('company_id' => $data['company_id']));
        for($s=0;$s<count($users);$s++)
        {
            $users[$s]['profile_image'] = getImageUrl($users[$s]['profile_image'],'profile');
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $knowledge_documents, 'total_records' => $total_records_count, 'document_type_data' => $document_type, 'users' => $users));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeGraph_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $views = $this->Company_model->getKnowledgeViewsByMonth($data);
        $uploads = $this->Company_model->getKnowledgeUploadsByMonth($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('views' => $views, 'uploads' => $uploads));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledge_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> $this->lang->line('knowledge_document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Company_model->deleteKnowledge(array('knowledge_document_id' => $data['knowledge_document_id']));
        $result = array('status'=>TRUE, 'message' =>'succ_delete', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getLowerApprovalRoles($company_id,$approval_id)
    {
        $data = $this->Company_model->getLowerApprovalRoles($company_id,$approval_id);
        return $data;
    }

    public function companyCurrencySelected_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        //$data['except_currency_id'] = $company_details->currency_id;
        $result = $this->Company_model->getCompanyCurrencySelected($data);

        for($s=0;$s<count($result);$s++){
            if($result[$s]['country_flag']!='')
                $result[$s]['country_flag'] = getImageUrl($result[$s]['country_flag'],'flag');
            $currency_value = $this->Company_model->getCompanyCurrencyValue(array('company_currency_id' => $result[$s]['id_company_currency']));
            if(!empty($currency_value) && $result[$s]['is_selected']) {
                $result[$s]['company_currency_value'] = $currency_value[0]['company_currency_value'];
            }
            else
                $result[$s]['company_currency_value'] = 0;

            $result[$s]['is_primary'] = 0;
            if($company_details->currency_id==$result[$s]['id_currency']){
                $result[$s]['is_primary'] = 1;
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        //$data['except_currency_id'] = $company_details->currency_id;
        $data['company_currency_status'] = 1;
        $result = $this->Company_model->getCompanyCurrencyDetails($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrencyList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyCurrency($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrency_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('currency_id', array('required'=> $this->lang->line('currency_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $previous_company_currencies = $this->Company_model->getCompanyCurrencyDetails(array('company_id' => $data['company_id']));
        $previous_company_currency_ids = array_map(function ($i) { return $i['currency_id']; }, $previous_company_currencies);
        $result = $add = $update = $update1 = array();

        for($s=0;$s<count($data['currency_id']);$s++)
        {
            if(!in_array($data['currency_id'][$s],$previous_company_currency_ids)){
                $add[] = array('company_id' => $data['company_id'], 'currency_id' => $data['currency_id'][$s]);
            }

        }

        for($s=0;$s<count($previous_company_currencies);$s++)
        {
            if(!in_array($previous_company_currencies[$s]['currency_id'],$data['currency_id'])){
                $update[] = array('id_company_currency' => $previous_company_currencies[$s]['id_company_currency'], 'company_currency_status' => 0);
            }
            else{
                $update1[] = array('id_company_currency' => $previous_company_currencies[$s]['id_company_currency'], 'company_currency_status' => 1);
            }
        }

        if(!empty($add)) $this->Company_model->addCompanyCurrencyBatch($add);
        if(!empty($update)) $this->Company_model->updateCompanyCurrencyBatch($update);
        if(!empty($update1)) $this->Company_model->updateCompanyCurrencyBatch($update1);


        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function companyCurrencyValue_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $add = array();
        for($s=0;$s<count($data);$s++)
        {

            if((isset($data[$s]['company_currency_id']) && $data[$s]['company_currency_id']!=0 && $data[$s]['company_currency_id']!='') && (isset($data[$s]['company_currency_value']) && $data[$s]['company_currency_value']!='')) {
                $add[] = array('company_currency_id' => $data[$s]['company_currency_id'], 'company_currency_value' => $data[$s]['company_currency_value'],'created_date_time' => currentDate());
            }
        }
        $result = array();
        if(!empty($add))
            $result = $this->Company_model->addCompanyCurrencyValue($add);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityStatus_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'facility'));
        if(empty($module_status)){ $status = 0; }
        else { $status = 1; }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$status);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getCompanyCurrencyDetails(array('company_id' => $data['company_id'], 'company_currency_status' => 1));

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function primaryCurrency_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('currency_id', array('required'=>$this->lang->line('currency_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = array('currency_id' => $data['currency_id'], 'id_company' => $data['company_id']);
        $result = $this->Company_model->updatePrimaryCurrency($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('Primary currency updated successfully.'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function collateralType_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCollateralType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStage_get()
    {
        $data = $this->input->get();
        if(isset($data['company_id']) && isset($data['collateral_type_id']))
            $results = $this->Company_model->getCollateralStageForCompany($data);
        else
            $results = $this->Company_model->getCollateralStage($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeStage_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>$this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=>$this->lang->line('collateral_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }



        $add = $update = $delete = array();
        $stage_ids = $data['collateral_stage_ids'];
        $prev_data = $this->Company_model->getCollateralTypeStage($data);
        $prev_state_id = array_values(array_unique(array_map(function ($i) { return $i['collateral_stage_id']; }, $prev_data)));


        for($s=0;$s<count($stage_ids);$s++)
        {
            if(!in_array($stage_ids[$s],$prev_state_id)){
                $add[] = array(
                    'company_id' => $data['company_id'],
                    'collateral_type_id' => $data['collateral_type_id'],
                    'collateral_stage_id' => $stage_ids[$s],
                    'created_date_time' => currentDate()
                );
            }
        }

        for($s=0;$s<count($prev_data);$s++)
        {
            if(!in_array($prev_data[$s]['collateral_stage_id'],$stage_ids)){
                $this->Company_model->updateCollateralTypeStage(array('id_collateral_type_stage' => $prev_data[$s]['id_collateral_type_stage'],'collateral_type_id' => $data['collateral_type_id'],'collateral_stage_id' => $prev_data[$s]['collateral_stage_id'],'status' => 0));
            }
            else
            {
                $this->Company_model->updateCollateralTypeStage(array('id_collateral_type_stage' => $prev_data[$s]['id_collateral_type_stage'],'collateral_type_id' => $data['collateral_type_id'],'collateral_stage_id' => $prev_data[$s]['collateral_stage_id'],'status' => 1));
            }
        }

        if(!empty($add)){ $this->Company_model->addCollateralTypeStage($add); }


        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeFields_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralTypeFields($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeStage_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralTypeStage($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFields_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> $this->lang->line('collateral_stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralStageFields($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function covenantType_get()
    {
        $data = $this->input->get();
        $results = $this->Company_model->getCovenantType($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategoryList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getCovenantCategoryList($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $covenant_category_id = array();
        $category = $this->Company_model->getCovenantCategory($data);

        for($s=0;$s<count($category);$s++)
        {
            if(!in_array($category[$s]['id_covenant_category'],$covenant_category_id)){
                array_push($covenant_category_id,$category[$s]['id_covenant_category']);

                $result[$category[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $category[$s]['id_covenant_category'],
                    'covenant_category' => $category[$s]['covenant_category'],
                    'covenant_type_id' => $category[$s]['covenant_type_id'],
                    'company_id' => $category[$s]['company_id'],
                    'sector' => $category[$s]['sector'],
                    'covenant' => array()
                );
                if($category[$s]['id_covenant']!='')
                    $result[$category[$s]['id_covenant_category']]['covenant'][] = array(
                        'covenant_category_id' => $category[$s]['covenant_category_id'],
                        'id_covenant' => $category[$s]['id_covenant'],
                        'covenant_name' => $category[$s]['covenant_name'],
                        'covenant_details_defination' => $category[$s]['covenant_details_defination'],
                        'covenant_value' => $category[$s]['covenant_value'],
                        'covenant_status'=> $category[$s]['covenant_status']
                    );
            }
            else{
                if($category[$s]['id_covenant']!='')
                    $result[$category[$s]['id_covenant_category']]['covenant'][] = array(
                        'covenant_category_id' => $category[$s]['covenant_category_id'],
                        'id_covenant' => $category[$s]['id_covenant'],
                        'covenant_name' => $category[$s]['covenant_name'],
                        'covenant_details_defination' => $category[$s]['covenant_details_defination'],
                        'covenant_value' => $category[$s]['covenant_value'],
                        'covenant_status'=> $category[$s]['covenant_status']
                    );
            }
        }
        $result = array_values($result);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $this->form_validator->add_rules('sector', array('required'=> $this->lang->line('covenant_sector_req')));
        $this->form_validator->add_rules('covenant_category', array('required'=> $this->lang->line('covenant_category_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            if(isset($data['covenant_type_key'])){
                $covenant_type_details = $this->Company_model->getCovenantType(array('covenant_type_key' => $data['covenant_type_key']));
                if(!empty($covenant_type_details))
                    $data['covenant_type_id'] = $covenant_type_details[0]['id_covenant_type'];
            }
            $covenant_category_id = 0;
            if(isset($data['id_covenant_category'])){ $covenant_category_id = $data['id_covenant_category']; }
            $check_name = $this->Company_model->getCovenantCategory(array('company_id' => $data['company_id'], 'covenant_category' => trim($data['covenant_category']),'covenant_type_id' => $data['covenant_type_id'], 'covenant_category_id_not' => $covenant_category_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('covenant_category' => $this->lang->line('covenant_category_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_covenant_category']))
        {
            $results = $this->Company_model->updateCovenantCategory(array(
                'id_covenant_category' => $data['id_covenant_category'],
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_category' => $data['covenant_category'],
                'sector' => $data['sector'],
                'company_id' => $data['company_id']
            ));
            $suc = $this->lang->line('covenant_category_update');
        }
        else{

            $results = $this->Company_model->addCovenantCategory(array(
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_category' => $data['covenant_category'],
                'sector' => $data['sector'],
                'company_id' => $data['company_id'],
                'created_date_time' => currentDate()
            ));
            $suc = $this->lang->line('covenant_category_add');
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCovenant($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $this->form_validator->add_rules('covenant_name', array('required'=> $this->lang->line('covenant_name_req')));
        $this->form_validator->add_rules('covenant_details_defination', array('required'=> $this->lang->line('covenant_details_defination_req')));
        $this->form_validator->add_rules('covenant_value', array('required'=> $this->lang->line('covenant_value_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $covenant_id = 0;
            if(isset($data['id_covenant'])){ $covenant_id = $data['id_covenant']; }
            $check_name = $this->Company_model->getCovenant(array('covenant_category_id' => $data['covenant_category_id'], 'covenant_name' => trim($data['covenant_name']),'covenant_id_not' => $covenant_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('covenant_name' => $this->lang->line('covenant_name_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(!isset($data['covenant_status'])){ $data['covenant_status']=1; }
        if(isset($data['id_covenant'])){
            $results = $this->Company_model->updateCovenant(array(
                'id_covenant' => $data['id_covenant'],
                'covenant_category_id' => $data['covenant_category_id'],
                'covenant_name' => $data['covenant_name'],
                'covenant_details_defination' => $data['covenant_details_defination'],
                'covenant_value' => $data['covenant_value'],
                'covenant_status' => $data['covenant_status'],
                'created_by' => $data['created_by']
            ));
            $suc = $this->lang->line('covenant_update');
        }
        else{
            $covenant_id = $this->Company_model->addCovenant(array(
                'covenant_category_id' => $data['covenant_category_id'],
                'covenant_name' => $data['covenant_name'],
                'covenant_details_defination' => $data['covenant_details_defination'],
                'covenant_value' => $data['covenant_value'],
                'covenant_status' => $data['covenant_status'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            ));
            $suc = $this->lang->line('covenant_add');

            if(isset($data['crm_project_id'])){
                $this->Company_model->addProjectCovenant(
                    array(
                        'covenant_id' => $covenant_id,
                        'project_id' => $data['crm_project_id'],
                        'created_by' => $data['created_by'],
                        'created_date_time' => currentDate()
                    )
                );
            }
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function getChildNodes($data,$parent_id,$array)
    {
        for($s=0;$s<count($data);$s++){
            if(isset($data[$s])){
                if($data[$s]['parent_module_id']==$parent_id){

                    $array[$data[$s]['id_module']] = array(
                        'id_module' => $data[$s]['id_module'],
                        'parent_module_id' => $data[$s]['parent_module_id'],
                        'module_name' => $data[$s]['module_name'],
                        'module_key' => $data[$s]['module_key'],
                        'module_url' => $data[$s]['module_url'],
                        'sub_module' => $data[$s]['sub_module'],
                        'childs' => array(),
                        'action' => array(),
                        'checked' => $data[$s]['checked']
                    );

                    if( $data[$s]['sub_module']==0){
                         for($st=0;$st<count($data);$st++){
                             if($data[$s]['id_module']==$data[$st]['module_id']){
                                 $array[$data[$s]['id_module']]['action'][] = array(
                                     'id_module_action' => $data[$st]['id_module_action'],
                                     'action_name' => $data[$st]['action_name'],
                                     'action_key' => $data[$st]['action_key'],
                                     'action_url' => $data[$st]['action_url'],
                                     'checked' => $data[$st]['checked']
                                 );
                             }
                         }
                    }

                    $child = $this->getChildNodes($data,$data[$s]['id_module'],$array[$data[$s]['id_module']]['childs']);

                    if(!empty($child)){
                        $array[$data[$s]['id_module']]['childs'] = array_values($child);
                    }
                    else{
                        $array[$data[$s]['id_module']]['childs'] = $child;
                    }
                }
            }
        }
        return $array;
    }

    public function module_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['type'])){ $data['type'] = 1; }
        $result = $this->Company_model->getModules($data);

        //getting modules in array format
        $final_data = $this->getChildNodes($result,0,array());
        $final_data = array_values($final_data);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$final_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCheckedActions($data,$application_role,$module_access_action_ids)
    {
        for($s=0;$s<count($data);$s++)
        {
            if($data[$s]['sub_module']==1)
            {
                $this->getCheckedActions($data[$s]['childs'],$application_role,$module_access_action_ids);
            }
            else
            {
                for($t=0;$t<count($data[$s]['action']);$t++)
                {
                    if(isset($data[$s]['action'][$t]))
                    if($data[$s]['action'][$t]['checked']==1)
                    {
                        if(in_array($data[$s]['action'][$t]['id_module_action'],$module_access_action_ids)){
                            $this->Company_model->updateModuleAccess(array('module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role, 'module_access_status' => 1));
                        }
                        else{
                            $this->Company_model->addModuleAccess(array('module_id' => $data[$s]['id_module'], 'module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role, 'module_access_status' => 1));
                        }
                        $this->ordered_data[] =array('module_id' => $data[$s]['id_module'], 'module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role);
                    }
                }
            }
        }

        return $this->ordered_data;
    }

    public function module_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(!isset($data['data'])){
            $result = array('status'=>FALSE,'error'=>array('data' => $this->lang->line('module_data_req')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $module_access = $this->Company_model->getModuleAccess(array('application_role_id' => $data['application_role_id']));

        $module_access_action_ids = array_map(function($sr){ return $sr['module_action_id']; },$module_access);

        $this->ordered_data = array();
        $module_action_post = $this->getCheckedActions($data['data'],$data['application_role_id'],$module_access_action_ids);
        $module_action_post_ids = array_map(function($sr){ return $sr['module_action_id']; },$module_action_post);

        for($s=0;$s<count($module_access);$s++)
        {
            if(!in_array($module_access[$s]['module_action_id'],$module_action_post_ids)){
                $this->Company_model->updateModuleAccess(array('module_action_id' => $module_access[$s]['module_action_id'],'application_role_id' => $data['application_role_id'], 'module_access_status' => 0));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('module_access_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function modulePage_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('module_id', $this->lang->line('module_id_req'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getModulePage($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getApplicationRole($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationRole_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_name', array('required'=> $this->lang->line('application_role_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $application_role_id = 0;
            if(isset($data['id_application_role'])){ $application_role_id = $data['id_application_role']; }
            $check_name = $this->Company_model->getApplicationRole(array('company_id' => $data['company_id'], 'application_role_name' => trim($data['application_role_name']), 'id_application_role_not' => $application_role_id));

            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('application_role_name' => $this->lang->line('application_role_name_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_application_role'])){
            $results = $this->Company_model->updateApplicationRole(array(
                'id_application_role' => $data['id_application_role'],
                'application_role_name' => $data['application_role_name']
            ));
            $suc = $this->lang->line('application_role_name_update');
        }
        else{
            $results = $this->Company_model->addApplicationRole(array(
                'application_role_name' => $data['application_role_name'],
                'company_id' => $data['company_id'],
                'created_date_time' => currentDate()
            ));
            $suc = $this->lang->line('application_role_name_add');
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getApplicationUserRole($data);
        for($s=0;$s<count($result);$s++)
        {
            //if($result[$s]['profile_image']!='')
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_req')));
        if((!isset($data['company_approval_role_id']) && !isset($data['user_id'])) || ((isset($data['company_approval_role_id']) && $data['company_approval_role_id']=='') && $data['user_id']==''))
            $this->form_validator->add_rules('company_approval_role_id', array('required'=> $this->lang->line('company_approval_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['company_approval_role_id']) && $data['company_approval_role_id']!='')
        {
            $check = $this->Company_model->getApplicationUserRole(array('company_id' => $data['company_id'],'company_approval_role_id' => $data['company_approval_role_id'],'status' => 'all'));


            if(isset($data['id_application_user_role'])){
                $results = $this->Company_model->updateApplicationUserRole(array(
                    'id_application_user_role' => $data['id_application_user_role'],
                    'application_role_id' => $data['application_role_id'],
                    'company_approval_role_id' => $data['company_approval_role_id']
                ));
                $suc = $this->lang->line('application_user_role_update');
            }
            else{

                if(!empty($check)){
                    if($check[0]['application_role_id']==$data['application_role_id'])
                    {
                        $results = $this->Company_model->updateApplicationUserRole(array(
                            'id_application_user_role' => $check[0]['id_application_user_role'],
                            'application_role_id' => $check[0]['application_role_id'],
                            'company_approval_role_id' => $check[0]['company_approval_role_id'],
                            'status' => 1
                        ));
                        $suc = $this->lang->line('application_user_role_update');
                    }
                    else{
                        $result = array('status'=>FALSE,'error'=>array('company_approval_role_id' => str_replace('%s',$check[0]['application_role_name'],$this->lang->line('approval_role_application_role_unique'))),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                }
                else
                {
                    $results = $this->Company_model->addApplicationUserRole(array(
                        'application_role_id' => $data['application_role_id'],
                        'company_approval_role_id' => $data['company_approval_role_id'],
                        'created_date_time' => currentDate()
                    ));
                    $suc = $this->lang->line('application_user_role_add');
                }
            }
        }
        else if(isset($data['user_id']) && $data['user_id']!='')
        {
            if(isset($data['id_application_user_role'])){
                $results = $this->Company_model->updateApplicationUserRole(array(
                    'id_application_user_role' => $data['id_application_user_role'],
                    'application_role_id' => $data['application_role_id'],
                    'user_id' => $data['user_id']
                ));
                $suc = $this->lang->line('application_user_role_update');
            }
            else{
                $check = $this->Company_model->getApplicationUserRole(array('company_id' => $data['company_id'],'user_id' => $data['user_id'],'status' => 'all'));

                if(!empty($check)){
                    if($check[0]['application_role_id']==$data['application_role_id'])
                    {
                        $results = $this->Company_model->updateApplicationUserRole(array(
                            'id_application_user_role' => $check[0]['id_application_user_role'],
                            'application_role_id' => $check[0]['application_role_id'],
                            'user_id' => $check[0]['user_id'],
                            'status' => 1
                        ));
                        $suc = $this->lang->line('application_user_role_update');
                    }
                    else
                    {
                        $result = array('status'=>FALSE,'error'=>array('user_id' => str_replace('%s',$check[0]['application_role_name'],$this->lang->line('user_application_role_unique'))),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $results = $this->Company_model->addApplicationUserRole(array(
                        'application_role_id' => $data['application_role_id'],
                        'user_id' => $data['user_id'],
                        'created_date_time' => currentDate()
                    ));
                    $suc = $this->lang->line('application_user_role_add');
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_application_user_role', array('required'=> $this->lang->line('application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->deleteApplicationUserRole(array(
                        'id_application_user_role' => $data['id_application_user_role']
                    ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('application_user_role_del'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getUserListForApplicationRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['type']=='user')
            $result = $this->Company_model->getUserListForApplicationRole(array('company_id' => $data['company_id'],'application_role_id' => $data['application_role_id']));
        else if($data['type']=='approval_role')
            $result = $this->Company_model->getApprovalRoleListForApplicationRole(array('company_id' => $data['company_id'],'application_role_id' => $data['application_role_id']));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function internalRating_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getInternalRating($data);
        if(isset($data['crm_company_id']))
        {
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['status'] = 0;
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => '', 'reference_type' => 'company_rating'));
                if(!empty($module_status)){ $result[$s]['status'] = 1; }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacility_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyFacility($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['general'] = 1;
            $result[$s]['pricing'] = 0;
            $result[$s]['terms_conditions'] = 0;
            $pricing = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => '0', 'crm_module_type' => 'admin_facility', 'reference_id' => $result[$s]['id_company_facility'], 'reference_type' => 'pricing'));
            $terms = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => '0', 'crm_module_type' => 'admin_facility', 'reference_id' => $result[$s]['id_company_facility'], 'reference_type' => 'terms_conditions'));
            if(!empty($pricing)){ $result[$s]['pricing']=1; }
            if(!empty($terms)){ $result[$s]['terms_conditions']=1; }
        }
        if(isset($data['id_company_facility'])){
            $result = $result[0];
        }
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacility_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('facility_name', array('required'=> $this->lang->line('facility_name_req')));
        //$this->form_validator->add_rules('facility_type', array('required'=> $this->lang->line('facility_name_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['facility_type'])){
            $data['facility_type'] = '';
        }
        $facility_type_details = $this->Company_model->getCompanyFacility(array('facility_name' => $data['facility_name']));
        if(!empty($facility_type_details)){
            if(isset($data['id_company_facility'])){
                if($facility_type_details[0]['id_company_facility']==$data['id_company_facility']){
                    $result = array('status'=>FALSE,'error'=>array('facility_name' =>  $this->lang->line('facility_name_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else{
                $result = array('status'=>FALSE,'error'=>array('facility_name' => $this->lang->line('facility_name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(!isset($data['facility_description'])){
            $data['facility_description'] = '';
        }

        $data['updated_by'] = $data['created_by'];
        if(!isset($data['id_company_facility']))
        {
            $data['created_date_time'] = currentDate();
            $data['updated_date_time'] = currentDate();
            $existing_facility = $this->Company_model->getCompanyFacility(array('facility_name' => $data['facility_name'],'status' => 0));
            if(empty($existing_facility)){
                $result = $this->Company_model->addCompanyFacility($data);
            }
            else{
                $data['id_company_facility'] = $existing_facility[0]['id_company_facility'];
                $data['status'] = 1;
                $result = $this->Company_model->updateCompanyFacility($data);
            }

            $msg = $this->lang->line('facility_add');
        }
        else
        {
            $data['updated_date_time'] = currentDate();
            $result = $this->Company_model->updateCompanyFacility($data);
            $msg = $this->lang->line('facility_update');
        }

        $result = array('status'=>TRUE, 'message' =>$msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityAmountType_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getFacilityAmountType($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function timePeriod_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getTimePeriod($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityGeneral_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('id_company_facility', array('required'=> $this->lang->line('company_facility_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('facility_name', array('required'=> $this->lang->line('field_name_req')));
        /*$this->form_validator->add_rules('facility_field_value', array('required'=> $this->lang->line('payment_type_req')));
        $this->form_validator->add_rules('field_section', array('required'=> $this->lang->line('field_section_req')));*/

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $facility_details = $this->Company_model->getCompanyFacility(array('facility_name' => $data['facility_name']));
        //echo "<pre>"; print_r($facility_details); exit;
        if(!empty($facility_details)){
            if(isset($data['id_company_facility'])){
                if($facility_details[0]['id_company_facility']!=$data['id_company_facility']){
                    $result = array('status'=>FALSE,'error'=>array('facility_name' =>  $this->lang->line('facility_name_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else{
                $result = array('status'=>FALSE,'error'=>array('facility_name' =>  $this->lang->line('facility_name_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        $data['updated_by'] = $data['created_by'];
        $data['updated_date_time'] = currentDate();

        $this->Company_model->updateCompanyFacility($data);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('facility_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityFields_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$this->form_validator->add_rules('company_facility_id', array('required'=> $this->lang->line('company_facility_id_req')));
        $this->form_validator->add_rules('field_section', array('required'=> $this->lang->line('field_section_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getFacilityFields($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacilityFields_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_facility_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('field_section', array('required'=> $this->lang->line('field_section_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyFacilityFields($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function companyFacilityFields_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_facility_id', array('required'=> $this->lang->line('company_facility_id_req')));
        $this->form_validator->add_rules('facility_field_id', array('required'=> $this->lang->line('field_name_req')));
        $this->form_validator->add_rules('field_section', array('required'=> $this->lang->line('field_section_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        if(isset($data['field_section']) && $data['field_section']=='pricing'){
            $this->form_validator->add_rules('facility_field_value', array('required'=> $this->lang->line('facility_field_value_req')));
            $this->form_validator->add_rules('payment_type', array('required'=> $this->lang->line('payment_type_req')));
        }
        else if(isset($data['field_section']) && $data['field_section']=='terms_conditions'){
            $this->form_validator->add_rules('facility_field_description', array('required'=> $this->lang->line('facility_field_description_req')));
        }

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['facility_field_description'])){
            $data['facility_field_description'] = '';
        }

        if(!isset($data['currency_id'])){ $data['currency_id'] = ''; }
        if(!isset($data['time_period_id'])){ $data['time_period_id'] = ''; }
        if(!isset($data['currency_id'])){ $data['currency_id'] = ''; }
        if(!isset($data['facility_amount_type'])){ $data['facility_amount_type'] = ''; }
        if(!isset($data['facility_field_description'])){ $data['facility_field_description'] = ''; }
        if(!isset($data['payment_type'])){ $data['payment_type'] = ''; }
        if(!isset($data['facility_field_value'])){ $data['facility_field_value'] = ''; }

        if(isset($data['id_company_facility_field']))
        {
            $company_facility_field_id = $data['id_company_facility_field'];
            $update_data = array(
                'id_company_facility_field' => $data['id_company_facility_field'],
                'payment_type' => $data['payment_type'],
                'time_period_id' => $data['time_period_id'],
                'currency_id' => $data['currency_id'],
                'facility_amount_type' => $data['facility_amount_type'],
                'facility_field_value' => $data['facility_field_value'],
                'facility_field_description' => $data['facility_field_description'],
                'updated_by' => $data['created_by'],
                'updated_date_time' => currentDate(),
                'status' => 1
            );
            $this->Company_model->updateCompanyFacilityField($update_data);
            $msg = $this->lang->line('info_update');
        }
        else
        {
            $existing_company_facility = $this->Company_model->getCompanyFacilityFields(array('company_facility_id' => $data['company_facility_id'],'facility_field_id' => $data['facility_field_id'],'status' => 0));
            if(empty($existing_company_facility)){
                $company_facility_field_id = $this->Company_model->addCompanyFacilityField(array(
                    'company_facility_id' => $data['company_facility_id'],
                    'facility_field_id' => $data['facility_field_id'],
                    'payment_type' => $data['payment_type'],
                    'time_period_id' => $data['time_period_id'],
                    'currency_id' => $data['currency_id'],
                    'facility_amount_type' => $data['facility_amount_type'],
                    'facility_field_value' => $data['facility_field_value'],
                    'facility_field_description' => $data['facility_field_description'],
                    'created_by' => $data['created_by'],
                    'updated_by' => $data['created_by'],
                    'created_date_time' => currentDate(),
                    'updated_date_time' => currentDate()
                ));
                $msg = $this->lang->line('info_update');
            }
            else{
                $company_facility_field_id = $existing_company_facility[0]['id_company_facility_field'];
                $this->Company_model->updateCompanyFacilityField(array(
                    'id_company_facility_field' => $existing_company_facility[0]['id_company_facility_field'],
                    'company_facility_id' => $data['company_facility_id'],
                    'facility_field_id' => $data['facility_field_id'],
                    'payment_type' => $data['payment_type'],
                    'time_period_id' => $data['time_period_id'],
                    'currency_id' => $data['currency_id'],
                    'facility_amount_type' => $data['facility_amount_type'],
                    'facility_field_value' => $data['facility_field_value'],
                    'facility_field_description' => $data['facility_field_description'],
                    'updated_by' => $data['created_by'],
                    'updated_date_time' => currentDate(),
                    'status' => 1
                ));
                $msg = $this->lang->line('info_update');
            }

        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => '0', 'crm_module_type' => 'admin_facility', 'reference_id' => $data['company_facility_id'], 'reference_type' => $data['field_section']));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => '0', 'crm_module_type' => 'admin_facility', 'reference_id' => $data['company_facility_id'], 'reference_type' => $data['field_section'],'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $result = $this->Company_model->getCompanyFacilityFields(array('id_company_facility_field' => $company_facility_field_id));
        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacilityFields_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_company_facility_field', array('required'=> $this->lang->line('company_facility_field_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteCompanyFacilityFields(array('id_company_facility_field' => $data['id_company_facility_field'],'status' => 0));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacility_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_company_facility', array('required'=> $this->lang->line('company_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteCompanyFacility(array('id_company_facility' => $data['id_company_facility'],'status' => 0));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('facility_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentStep_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_company_assessment_item_step', array('required'=> $this->lang->line('id_req')));
        $this->form_validator->add_rules('step_title', array('required'=> $this->lang->line('step_title_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->updateCompanyAssessmentStep($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sectorLimit_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['limit_status'] = 1;
        $result = $this->Company_model->getSectorLimit($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addSectors_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('sector_id', array('required'=> $this->lang->line('sector_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $add = $update = array();
        for($s=0;$s<count($data['sector_id']);$s++)
        {
            $sectors = array();
            $sub_sectors = $this->Master_model->getSectorsList(array('parent_sector_id' => $data['sector_id'][$s]));
            $sectors = array_map(function($i){ return $i['id_sector']; },$sub_sectors);
            array_push($sectors,$data['sector_id'][$s]);
            //echo "<pre>"; print_r($sectors); exit;
            for($r=0;$r<count($sectors);$r++)
            {
                $check = $this->Company_model->getSectorLimit(array('sector_id' => $sectors[$r]));
                if(empty($check)){
                    $add[] = array(
                        'company_id' => $data['company_id'],
                        'sector_id' => $sectors[$r],
                        'limit_value' => 0,
                        'created_by' => $data['created_by'],
                        'created_date_time' => currentDate()
                    );
                }
                else{
                    $update[] = array(
                        'id_sector_limit' => $check[0]['id_sector_limit'],
                        'limit_status' => 1,
                        'limit_value' => 0
                    );
                }
            }
        }

        if(!empty($add))
            $this->Company_model->addSectorLimit_batch($add);

        if(!empty($update))
            $this->Company_model->updateSectorLimit_batch($update);

        $result = array('status' => TRUE, 'message' => $this->lang->line('sector_limit_add'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sectorLimit_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('sector_data', array('required'=> $this->lang->line('sector_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $sector_limit_data = array();
        for($s=0;$s<count($data['sector_data']);$s++)
        {
            $sector_limit_data[] = array(
                'id_sector_limit' => $data['sector_data'][$s]['id_sector_limit'],
                'limit_value' => $data['sector_data'][$s]['limit_value'],
                'updated_by' => $data['created_by'],
                'updated_date_time' => currentDate()
            );
        }

        if(!empty($sector_limit_data))
            $this->Company_model->updateSectorLimit_batch($sector_limit_data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('sector_limit_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sectorLimit_delete()
    {
        $data = $this->input->get();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_sector_limit', array('required'=> $this->lang->line('sector_limit_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Company_model->updateSectorLimit(array(
            'id_sector_limit' => $data['id_sector_limit'],
            'limit_status' => 0
        ));

        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
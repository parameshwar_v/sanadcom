<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        //$this->load->library('common/form_validator');
    }

    public function check_email_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );

        //validating data
        $this->form_validator->add_rules('email_id', $emailRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->check_email($data['email_id']);
        $value = 0;
        if(empty($result)){ $value=1; }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$value);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function loginHistory_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_ControllFer::HTTP_OK);
        }
        $result = $this->User_model->getSession($data);
        /*for($ul=0;$ul<count($result);$ul++)
        {
            $ubrowser=$this->getUserBrowser($result[$ul]['client_browser']);
            $result[$ul]['client_browser']='{"browser_name": "'.$ubrowser['name'].' '.$ubrowser['version'].' ","browser_version": "'.$ubrowser['version'].'","os": "'.$ubrowser['platform'].'"}';
        }*/
        $total_records = count($this->User_model->getTotalSession($data));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records) );
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function changePassword_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $passwordRules               = array(
            'required'=> $this->lang->line('password_req'),
            'min_len-8' => $this->lang->line('password_num_min_len'),
            'max_len-12' => $this->lang->line('password_num_max_len'),
        );
        $confirmPasswordRules        = array(
            'required'=>$this->lang->line('confirm_password_req'),
            'match_field-password'=>$this->lang->line('password_match')
        );

        $req = array(
            'required'=> $this->lang->line('user_id_req')
        );

        if(isset($_POST['requestData']) && DATA_ENCRYPT)
        {
            $aesObj = new AES();
            $data = $aesObj->decrypt($_POST['requestData'],AES_KEY);
            $data = (array) json_decode($data,true);
            $_POST = $data;
        }

        $this->form_validator->add_rules('user_id', $req);
        $this->form_validator->add_rules('oldpassword', array('required'=>$this->lang->line('old_password_req')));
        $this->form_validator->add_rules('password', $passwordRules);
        $this->form_validator->add_rules('cpassword', $confirmPasswordRules);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($data['password'] == $data['oldpassword']){
            $result = array('status'=>FALSE,'error'=>array('password'=>$this->lang->line("old_new_password_same")),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $passwordExist=$this->User_model->passwordExist($data);
        if($passwordExist!=1)
        {
            $result = array('status'=>FALSE,'error'=>array('oldpassword'=>$this->lang->line("old_password_not_match")),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->changePassword($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('password_changed'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function usersList_get($type)
    {
        if(empty($type)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['type'] = $type;
        //validating data
        $this->form_validator->add_rules('type', array('required' => $this->lang->line('type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->getUsersList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userInfo_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->getUserInfo($data);
        $result->profile_image = getImageUrl($result->profile_image,'profile');

        unset($result->password);
        unset($result->user_status);
        unset($result->updated_date);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function userInfo_post()
    {
       /* $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }*/
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['user'];
        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );

        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $passwordRules  = array(
            'required'=> $this->lang->line('password_req')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            /*'numeric' =>  $this->lang->line('phone_num_num'),*/
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-25' => $this->lang->line('phone_num_max_len_20'),
        );

        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        //$this->form_validator->add_rules('email_id', $emailRules);
        //$this->form_validator->add_rules('password', $passwordRules);
        $this->form_validator->add_rules('phone_number', $phoneRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*$email_check = $this->User_model->check_email($data['email_id']);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email_id' => $this->lang->line('email_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->createUserInfo($data);

        sendmail($data['email_id'],'Q-lana Account Created','<p>welcome to q-lana,</p><p>hello, '.$data["email_id"].'</p>');*/

        if(!file_exists('uploads/'.$data['company_id'])){
            mkdir('uploads/'.$data['company_id']);
        }

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,$data['company_id'],'image');

            $data['profile_image'] = $imageName;
        }
        else{
            unset($data['profile_image']);
        }
        //echo "<pre>"; print_r($data); exit;
        $user_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone_number' => $data['phone_number'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country_id' => $data['country_id'],
            'zip_code' => $data['zip_code'],
        );
        if(isset($data['profile_image'])){
            $user_data['profile_image'] = $data['profile_image'];
        }

        $result = $this->User_model->updateUserData($user_data,$data['id_user']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('user_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userInfo_put()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $phoneRules  = array(
            'required'=> $this->lang->line('phone_num_req'),
            'numeric'=>  $this->lang->line('phone_num_num'),
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-10' => $this->lang->line('phone_num_max_len'),
        );

        $this->form_validator->add_rules('first_name', $firstNameRules);
        $this->form_validator->add_rules('last_name', $lastNameRules);
        $this->form_validator->add_rules('phone_number', $phoneRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->updateUserInfo($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userInfo_delete()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->deleteUser($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function logout_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $previous_session = $this->User_model->getPreviousUserSessions(array('user_id' => $_SERVER['HTTP_USER'],'access_token' => str_replace('Bearer ','',$_SERVER['HTTP_AUTHORIZATION'])));
        if(!empty($previous_session)){
            for($sr=0;$sr<count($previous_session);$sr++)
            {
                $this->User_model->updateOauthAccessToken(array('id' => $previous_session[$sr]['access_token_id'],'expire_time' => '-'.$previous_session[$sr]['expire_time'],'updated_at' => currentDate(),'expired_date_time' => currentDate()));
            }
        }
    }
    public function saveSecurityQuestion_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $passwordRules               = array(
            'required'=> $this->lang->line('password_req'),
            'min_len-6' => $this->lang->line('password_num_min_len'),
            'max_len-12' => $this->lang->line('password_num_max_len'),
        );
        $confirmPasswordRules        = array(
            'required'=>$this->lang->line('confirm_password_req'),
            'match_field-password'=>$this->lang->line('password_match')
        );
        $req = array(
            'required'=> $this->lang->line('user_id_req')
        );
        $questionsRules               = array(
            'required'=> $this->lang->line('first_name_req'),
        );

        $ansRules               = array(
            'required'=> $this->lang->line('last_name_req'),
        );

        $this->form_validator->add_rules('user_id', $req);
        $this->form_validator->add_rules('oldpassword', array('required'=>$this->lang->line('old_password_req')));
        $this->form_validator->add_rules('password', $passwordRules);
        $this->form_validator->add_rules('cpassword', $confirmPasswordRules);
        $this->form_validator->add_rules('questions', $questionsRules);
        $this->form_validator->add_rules('ans', $ansRules);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($data['password'] == $data['oldpassword']){
            $result = array('status'=>FALSE,'error'=>array('password'=>$this->lang->line("old_new_password_same")),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $passwordExist=$this->User_model->passwordExist($data);
        if($passwordExist!=1)
        {
            $result = array('status'=>FALSE,'error'=>array('oldpassword'=>$this->lang->line("old_password_not_match")),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $question_data = array('security_question_id' => $data['questions'], 'security_question_ans' => $data['ans'],);
        $this->User_model->saveSecurityQuestion($question_data,$data['user_id']);
        $user_data = array(
            'password' => $data['password'],
            'user_id' => $data['user_id'],
        );
        $result = $this->User_model->changePassword($user_data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('user_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateSecurityQuestion_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $req = array(
            'required'=> $this->lang->line('user_id_req')
        );
        $questionsRules               = array(
            'required'=> $this->lang->line('first_name_req'),
        );

        $ansRules               = array(
            'required'=> $this->lang->line('last_name_req'),
        );

        $this->form_validator->add_rules('id_user', $req);
        $this->form_validator->add_rules('security_question_id', $questionsRules);
        $this->form_validator->add_rules('security_question_ans', $ansRules);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $question_data = array('security_question_id' => $data['security_question_id'], 'security_question_ans' => $data['security_question_ans'],);
        $result = $this->User_model->saveSecurityQuestion($question_data,$data['id_user']);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('security_updated'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userLogByType_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->getUserLogByType($data);
        /*for($ul=0;$ul<count($result['user_log']);$ul++)
        {
            $ubrowser=$this->getUserBrowser($result['user_log'][$ul]->client_browser);
            $result['user_log'][$ul]->client_browser='{"browser_name": "'.$ubrowser['name'].' '.$ubrowser['version'].' ","browser_version": "'.$ubrowser['version'].'","os": "'.$ubrowser['platform'].'"}';
        }*/
        $total_records = count($result['user_log']);
        /*$result = $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records);*/
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result['user_log'],'total_records' => $total_records, 'user_details'=> $result['user_details']));
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
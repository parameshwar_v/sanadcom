<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Activity extends REST_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Activity_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Notification_model');
    }

    public function task_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        //$data = $data['task'];
        if(!isset($data['member']) && empty($data['member'])){
            unset($data['member']);
            $this->form_validator->add_rules('member',array('required'=> $this->lang->line('assigned_to_members_req')));
        }
        $this->form_validator->add_rules('task_name',array('required'=> $this->lang->line('task_req')));
        //$this->form_validator->add_rules('module_id',array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type',array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('created_by',array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['module_id'])){ $data['module_id'] = 0; }
        unset($data['assigned_by']);

        if(isset($data['company_id'])){ $company_id = $data['company_id']; unset($data['company_id']); }
        else{ $company_id = 0; }

        $data['task_type'] = 'project';
        $add = $delete = array();
        $member = $data['member'];
        unset($data['member']);
        $data['updated_by'] = $data['created_by'];
        $created_by = $data['created_by'];
        $data['updated_date_time'] = currentDate();
        $reference_title = 'General';
        if($data['module_type']=='contact'){
            $reference_module = $this->Crm_model->getContact($data['module_id']);
            $reference_title = $reference_module[0]['first_name'].' '.$reference_module[0]['last_name'];
            $company_id = $reference_module[0]['company_id'];
        }
        else if($data['module_type']=='company'){
            $reference_module = $this->Crm_model->getCompany($data['module_id']);
            $reference_title = $reference_module[0]['company_name'];
            $company_id = $reference_module[0]['company_id'];
        }
        else if($data['module_type']=='project'){
            $reference_module = $this->Crm_model->getProject($data['module_id']);
            $reference_title = $reference_module[0]['project_title'];
            $company_id = $reference_module[0]['company_id'];
        }

        $data['comment'] = isset($data['comment'])?$data['comment']:'';
        if(isset($data['id_task']) && $data['id_task']!='')
        {
            unset($data['created_by']);
            $task_id = $this->Activity_model->updateTask($data);
            $data['member'] = $member;
            $task_members = $this->Activity_model->getTaskMembers(array('task_id' => $data['id_task']));
            $task_member_ids = array_values(array_map(function ($obj) { return $obj['member_id']; }, $task_members));
            for($s=0;$s<count($data['member']);$s++)
            {
                if(!in_array($data['member'][$s],$task_member_ids)){
                    $add[] = array(
                        'task_id' => $data['id_task'],
                        'member_id' => $data['member'][$s]
                    );
                }
            }

            for($s=0;$s<count($task_members);$s++)
            {
                if(!in_array($task_members[$s]['member_id'],$data['member'])){
                    $delete[] = $task_members[$s]['id_task_member'];
                }
            }

            if(!empty($add)) $this->Activity_model->addTaskMembers($add);
            if(!empty($delete)) $this->Activity_model->deleteTaskMembers($delete);
        }
        else
        {
            $data['created_date_time'] = currentDate();
            $task_id = $this->Activity_model->addTask($data);
            $data['member'] = $member;
            for($s=0;$s<count($data['member']);$s++)
            {
                $add[] = array(
                    'task_id' => $task_id,
                    'member_id' => $data['member'][$s]
                );
            }
            $this->Activity_model->addTaskMembers($add);
        }

        $document_type = $this->Crm_model->getDocumentTypeByName('task');
        $data['document_type'] = $document_type->id_crm_document_type;

        $path='uploads/'; $no_files = 0;

        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            if(isset($data['id_task'])){ $task_id = $data['id_task']; }
            if(is_array($_FILES['file']['name']['attachment']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachment']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachment'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachment'][$s],$_FILES['file']['name']['attachment'][$s],$path,$company_id,'');
                    //echo $imageName; exit;

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $task_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $created_by, 'created_date_time' => currentDate());
                    $document_id = $this->Crm_model->addDocument($document);
                    $no_files++;

                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachment'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $task_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $created_by);
                $document_id = $this->Crm_model->addDocument($document);
                $no_files++;

            }
            if(isset($data['id_task'])){ unset($task_id); }
        }

        if(!isset($data['id_task']) || $data['id_task']==''){
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            for($s=0;$s<count($data['member']);$s++)
            {
                $assigned_user = $this->User_model->getUserInfo(array('id' => $data['member'][$s]));
                $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has created new task <a class="sky-blue" href="javascript:;" data-activity-id = "'.$task_id.'" data-activity-type = "task">'.ucfirst($data['task_name']).'</a> and assigned to <span class="blue">'.ucfirst($assigned_user->first_name).' '.ucfirst($assigned_user->last_name).'</span></p>';

                $activity = array(
                    'activity_name' => 'Task',
                    'activity_type' => 'task',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $task_id,
                    'module_type' => $data['module_type'],
                    'module_id' => $data['module_id'],
                    'created_by' => $data['created_by'],
                    'created_date_time' => currentDate()
                );
                /*$notification = array(
                    'sent_by' => $data['created_by'],
                    'sent_to' => $data['member'][$s],
                    'project_id' => $data['project_id'],
                    'notification_type' => $data['task_name'],
                    'notification_template' => '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has assigned new task</a></p>',
                    'created_date_time' => currentDate()
                );*/
                $this->addActivity($activity);
                //notification
                //$created_user

                if(!isset($data['task_due_date'])){ $data['task_due_date'] = '-'; }

                $assigned_to = $this->User_model->getUserInfo(array('id' => $data['member'][$s]));
                $link ='<a class="sky-blue" href="'.WEB_BASE_URL . '#/task/list?taskId='.base64_encode($task_id).'">here</a>';

                $notification = str_replace(array('{task_title}','{date}','{project_name}','{module_type}','{from_user}','{here}'),array($data['task_name'],$data['task_due_date'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link),$this->lang->line('task_notification'));
                $this->Notification_model->addNotification(array(
                    'assigned_to' => $data['member'][$s],
                    'assigned_by' =>  $data['created_by'],
                    'module_reference_id' => $task_id,
                    'module_reference_type' => 'task',
                    'notification_subject' => $reference_title,
                    'notification_template' => $notification,
                    'notification_link' => $link,
                    'notification_comments' => '',
                    'notification_type' => 'task',
                    'created_date_time' => currentDate(),
                    'module_id' => $data['module_id'],
                    'module_type' => $data['module_type']
                ));
                $link ='<a style="color: #22bcf2" class="sky-blue" href="'.WEB_BASE_URL . '#/task/list?taskId='.base64_encode($task_id).'">here</a>';
                if($data['module_id']==0){ $reference_title = 'General'; }
                else{ $reference_title = $reference_title.''.$this->lang->line('LR'); }
                $message = str_replace(array('{first_name}','{last_name}','{task_title}','{date}','{project_name}','{module_type}','{from_user}','{here}'),array($assigned_to->first_name,$assigned_to->last_name,$data['task_name'],date('M d, Y',strtotime($data['task_due_date'])),$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link),$this->lang->line('task_mail'));
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $data['member'][$s],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($assigned_to->email_id, str_replace(array('{task_title}'),array($data['task_name']),$this->lang->line('task_subject')),$template_data);

            }
        }
        else{

            $data['created_by'] = $data['updated_by'];
            $task_id = $data['id_task'];
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            for($s=0;$s<count($data['member']);$s++)
            {
                $assigned_user = $this->User_model->getUserInfo(array('id' => $data['member'][$s]));
                if(isset($data['task_status']) && $data['task_status']=='completed'){
                    $activityTemplate = '<p class="f12"><a class="sky-blue" href="javascript:;" data-activity-id = "'.$task_id.'" data-activity-type = "task">'.ucfirst($data['task_name']).'</a> task has been completed.  </p>';
                }
                else{
                    $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has updated task <a class="sky-blue" href="javascript:;" data-activity-id = "'.$task_id.'" data-activity-type = "task">'.ucfirst($data['task_name']).'</a> and assigned to <span class="blue">'.ucfirst($assigned_user->first_name).' '.ucfirst($assigned_user->last_name).'</span></p>';
                }


                $activity = array(
                    'activity_name' => 'Task',
                    'activity_type' => 'task',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $task_id,
                    'module_type' => $data['module_type'],
                    'module_id' => $data['module_id'],
                    'created_by' => $data['created_by'],
                    'created_date_time' => currentDate()
                );

                $this->addActivity($activity);
                if(!isset($data['task_status']) || (isset($data['task_status']) && $data['task_status']!='completed')) {
                    if (!isset($data['task_due_date'])) {
                        $data['task_due_date'] = '-';
                    }

                    $assigned_to = $this->User_model->getUserInfo(array('id' => $data['member'][$s]));
                    $link = '<a class="sky-blue" href="' . WEB_BASE_URL . '#/task/list?taskId=' . base64_encode($task_id) . '">here</a>';

                    $notification = str_replace(array('{task_title}', '{date}', '{project_name}', '{module_type}', '{from_user}', '{here}'), array($data['task_name'], $data['task_due_date'], $reference_title, $data['module_type'], $created_user->first_name . ' ' . $created_user->last_name, $link), $this->lang->line('task_notification_modified'));
                    $this->Notification_model->addNotification(array(
                        'assigned_to' => $data['member'][$s],
                        'assigned_by' => $data['created_by'],
                        'module_reference_id' => $task_id,
                        'module_reference_type' => 'task',
                        'notification_subject' => $reference_title,
                        'notification_template' => $notification,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'task',
                        'created_date_time' => currentDate(),
                        'module_id' => $data['module_id'],
                        'module_type' => $data['module_type']
                    ));
                    $link = '<a style="color: #22bcf2" class="sky-blue" href="' . WEB_BASE_URL . '#/task/list?taskId=' . base64_encode($task_id) . '">here</a>';
                    if ($data['module_id'] == 0) {
                        $reference_title = 'General';
                    } else {
                        $reference_title = $reference_title . '' . $this->lang->line('LR');
                    }
                    $message = str_replace(array('{first_name}', '{last_name}', '{task_title}', '{date}', '{project_name}', '{module_type}', '{from_user}', '{here}'), array($assigned_to->first_name, $assigned_to->last_name, $data['task_name'], date('M d, Y', strtotime($data['task_due_date'])), $reference_title, $data['module_type'], $created_user->first_name . ' ' . $created_user->last_name, $link), $this->lang->line('task_mail_modified'));
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'base_url' => REST_API_URL,
                        'to_username' => $data['member'][$s],
                        'message' => $message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    sendmail($assigned_to->email_id, str_replace(array('{task_title}'), array($data['task_name']), $this->lang->line('task_subject_modified')), $template_data);
                }
            }
        }
        if(!isset($data['id_task']) || $data['id_task']==''){
            $suc_msg = $this->lang->line('task_add');
        }
        else{
            $suc_msg = $this->lang->line('task_update');
        }
        if(isset($data['id_task'])){ $task_id = $data['id_task']; }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function task_get()
    {
        $data = $this->input->get();
        $document_type = $this->Crm_model->getDocumentTypeByName('task');
        $data['crm_document_type_id'] = $document_type->id_crm_document_type;

        if(isset($data['module_type']) && $data['module_type']=='general'){
            unset($data['module_type']);
            unset($data['module_id']);
        }

        $result = $this->Activity_model->getTask($data);
        if(isset($data['id_task']))
        {
            $result[0]['attachment'] = $this->Activity_model->getAttachments($data['id_task'],$data['crm_document_type_id']);

            for($r=0;$r<count($result[0]['attachment']);$r++)
            {
                $result[0]['attachment'][$r]['full_path'] = getImageUrl($result[0]['attachment'][$r]['full_path']);
            }

            $result[0]['member'] = $this->Activity_model->getTaskMembers(array('task_id' => $result[0]['id_task']));
            $result[0]['member'] = array_map(function($obj){ return $obj['member_id']; }, $result[0]['member']);
        }
        $total_records = $this->Activity_model->totalTasks($data);

        $data['status'] = 'pending';
        if(isset($data['user_id'])) {
            $data['assigned_to'] = $data['user_id'];
            $data['created_by'] = $data['user_id'];
            $data['status'] = 'pending';
            $data['due_task'] = 1;
            $pending_task = $this->Activity_model->totalTasks($data);
            unset($data['due_task']);
            $data['upcoming_task'] = 1;
            $upcoming_task = $this->Activity_model->totalTasks($data);

            if(isset($data['type']) && $data['type']=='due_task') {
                $total_records = $pending_task;
            }
            if(isset($data['type']) && $data['type']=='upcoming_task') {
                $total_records = $upcoming_task;
            }

            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_records' =>$total_records,'pending_task' => $pending_task,'upcoming_task' => $upcoming_task));
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_records' =>$total_records));
            $this->response($result, REST_Controller::HTTP_OK);
        }


    }

    public function updateTask_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $result = $this->Activity_model->updateTask($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('task_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function task_delete()
    {
        $data = $this->input->get();
        $data['task_status'] = 'deleted';
        $result = $this->Activity_model->updateTask($data);
        $this->deleteTouchPoint(array(
            'activity_type' => 'task',
            'activity_reference_id' => $data['id_task']
        ));
        $this->deleteNotification(array(
            'module_reference_id' => $data['id_task'],
            'module_reference_type' => 'task',
        ));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('task_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meetingList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Activity_model->getMeetingList($data);

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = $this->Activity_model->getMeetingListCount($data);

        $all_records = array_sum(array_map(function($item) {
                                        return $item['total'];
                                    }, $total_records));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $all_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function taskList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Activity_model->getTaskList($data);

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = $this->Activity_model->getTaskListCount($data);
        $all_records = array_sum(array_map(function($item) {
            return $item['total'];
        }, $total_records));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $all_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meetingGuest_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id')));
        $this->form_validator->add_rules('user_id',array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $user_branch_details = $this->Company_model->getCompanyUsers(array('user_id' => $data['user_id'], 'company_id' => $data['company_id']));
        $users = $this->Company_model->getCompanyUsers(array('branch_id' => $user_branch_details[0]['branch_id'], 'company_id' => $data['company_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$users);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        //$data = $data['meeting'];
        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id_req')));
        //$this->form_validator->add_rules('module_id',array('required'=> $this->lang->line('module_id_req')));
        if(!isset($data['invitation_id']) || empty($data['invitation_id'])){
            unset($data['invitation_id']);
            $this->form_validator->add_rules('invitation_id',array('required'=> $this->lang->line('meeting_members_req')));
        }
        $this->form_validator->add_rules('module_type',array('required'=> $this->lang->line('module_type_req')));
        if(isset($data['meeting_status']) && $data['meeting_status']=='cancelled')
        {
            $this->form_validator->add_rules('meeting_mom',array('required'=> $this->lang->line('meeting_mom_req')));
        }
        else
        {
            $this->form_validator->add_rules('meeting_name',array('required'=> $this->lang->line('meeting_name_req')));
            //$this->form_validator->add_rules('reference_id',array('required'=> $this->lang->line('reference_id_req')));
            //$this->form_validator->add_rules('reference_type',array('required'=> $this->lang->line('reference_type_req')));
            $this->form_validator->add_rules('created_by',array('required'=> $this->lang->line('user_id_req')));
            $this->form_validator->add_rules('where',array('required'=> $this->lang->line('where_req')));
            $this->form_validator->add_rules('when',array('required'=> $this->lang->line('when_req')));
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        if(!isset($data['module_id'])){ $data['module_id'] = 0; }
        if(!isset($data['reference_id'])){ $data['reference_id'] = 0; }
        if(!isset($data['reference_type'])){ $data['reference_type'] = ""; }
        if(!isset($data['from_time'])){ $data['from_time'] = "00:00"; }
        else if($data['from_time']=='null'){ $data['from_time'] = "00:00"; }
        if(!isset($data['to_time'])){ $data['to_time'] = "00:00"; }
        else if($data['to_time']=='null'){ $data['to_time'] = "00:00"; }

        unset($data['assigned_by']);

        $invitations = $invitation_person = array();
        if(isset($data['invitation_id']) && is_array($data['invitation_id'])){
            $invitations = $data['invitation_id'];
        }
        else if(isset($data['invitation_id'])){
            $invitations = explode(',',$data['invitation_id']);
        }
        unset($data['invitation_id']);

        $guest = $guest_data = array();
        if(isset($data['guest']) && !empty($data['guest']) && is_array($data['guest'])){
            $guest = array_values(array_unique($data['guest']));
            unset($data['guest']);
        }
        unset($data['guest']);
        $company_id = $data['company_id'];
        unset($data['company_id']);
        //insert or update based on id_meeting
        $data['meeting_type'] = $data['module_type'];

        $reference_title = 'General';
        if($data['module_type']=='contact'){
            $reference_module = $this->Crm_model->getContact($data['module_id']);
            $reference_title = $reference_module[0]['first_name'].' '.$reference_module[0]['last_name'];
        }
        else if($data['module_type']=='company'){
            $reference_module = $this->Crm_model->getCompany($data['module_id']);
            $reference_title = $reference_module[0]['company_name'];
        }
        else if($data['module_type']=='project'){
            $reference_module = $this->Crm_model->getProject($data['module_id']);
            $reference_title = $reference_module[0]['project_title'];
        }

        if(!isset($data['id_meeting'])){
            $data['created_date_time'] = currentDate();
            //echo "<pre>"; print_r($data); exit;
            $meeting_id = $this->Activity_model->addMeeting($data);
        }
        else{

            if($data['meeting_status']=='cancelled' || $data['meeting_status']=='completed'){
                $update_meeting_data = array(
                    'id_meeting' => $data['id_meeting'],
                    'meeting_mom' => $data['meeting_mom'],
                    'meeting_status' => $data['meeting_status'],
                    'updated_by' => $data['created_by'],
                    'updated_date_time' => currentDate()
                );
            }
            else{
                $update_meeting_data = array(
                    'id_meeting' => $data['id_meeting'],
                    'meeting_name' => $data['meeting_name'],
                    'meeting_description' => $data['meeting_description'],
                    'meeting_type' => $data['meeting_type'],
                    'where' => $data['where'],
                    'when' => $data['when'],
                    'from_time' => $data['from_time'],
                    'to_time' => $data['to_time'],
                    'agenda' => $data['agenda'],
                    'meeting_mom' => $data['meeting_mom'],
                    'meeting_status' => $data['meeting_status'],
                    'updated_by' => $data['created_by'],
                    'updated_date_time' => currentDate()
                );
            }

            $this->Activity_model->updateMeeting($update_meeting_data);
            $meeting_id = $data['id_meeting'];
        }

        if(!isset($data['meeting_status']) || (isset($data['meeting_status']) && $data['meeting_status']!='cancelled'))
        {
            $document_type = $this->Crm_model->getDocumentTypeByName('meeting');
            $data['document_type'] = $document_type->id_crm_document_type;
            $path='uploads/'; $no_files = 0;
            if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
            {
                if(isset($data['id_meeting'])){ $meeting_id = $data['id_meeting']; }
                if(is_array($_FILES['file']['name']['attachments']))
                {
                    for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                    {
                        $mediaType = $_FILES['file']['type']['attachments'][$s];
                        $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                        $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $meeting_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => $data['module_type'], 'form_key' => 'meeting');
                        $document_id = $this->Crm_model->addDocument($document);
                        $no_files++;

                    }
                }
                else{
                    $mediaType = $_FILES['file']['type']['attachments'];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $meeting_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => $data['module_type'], 'form_key' => 'meeting');
                    $document_id = $this->Crm_model->addDocument($document);
                    $no_files++;

                }
                if(isset($data['id_meeting'])){ unset($meeting_id); }
            }
            $invitation_data = $user_ids = array();
            if(isset($data['id_meeting'])){
                $exists_invitation_data = $this->Activity_model->getInvitationsByMeetingId($data['id_meeting']);
                for($s=0;$s<count($exists_invitation_data);$s++){
                    if(!in_array($exists_invitation_data[$s]['invitation_id'],$invitations)){
                        $this->Activity_model->deleteMeetingInvitation($exists_invitation_data[$s]['invitation_id'],$data['id_meeting']);
                    }
                    else {
                        $user_ids[] = $exists_invitation_data[$s]['invitation_id'];
                    }
                }

                $exists_invitation_ids = array_values(array_map(function ($obj) { return $obj['invitation_id']; }, $exists_invitation_data));
                for($s=0;$s<count($invitations);$s++){
                    if(!in_array($invitations[$s],$exists_invitation_ids)){
                        $invitation_data[] = array(
                            'meeting_id' => $data['id_meeting'],
                            'invitation_id' => $invitations[$s]
                        );
                    }
                }

                $exists_guest_data = $this->Activity_model->getGuestByMeetingId($data['id_meeting']);
                for($s=0;$s<count($exists_guest_data);$s++){
                    if(!in_array($exists_guest_data[$s]['email'],$guest)){
                        $this->Activity_model->deleteMeetingGuest($exists_guest_data[$s]['email'],$data['id_meeting']);
                    }
                }

                $exists_guest_ids = array_values(array_map(function ($obj) { return $obj['email']; }, $exists_guest_data));
                for($s=0;$s<count($guest);$s++){
                    if(!in_array($guest[$s],$exists_guest_ids)){
                        $guest_data[] = array(
                            'meeting_id' => $data['id_meeting'],
                            'email' => $guest[$s]
                        );
                    }
                }

            }
            else{
                for($s=0;$s<count($invitations);$s++){
                    $invitation_data[] = array(
                        'meeting_id' => $meeting_id,
                        'invitation_id' => $invitations[$s],
                        'created_date_time' => currentDate()
                    );
                }

                for($s=0;$s<count($guest);$s++){
                    $guest_data[] = array(
                        'meeting_id' => $meeting_id,
                        'email' => $guest[$s],
                        'created_date_time' => currentDate()
                    );
                }
            }

            if(!empty($invitation_data)){
                $this->Activity_model->addMeetingInvitations($invitation_data);
            }
            if(!empty($guest_data)){
                $this->Activity_model->addMeetingGuest($guest_data);
            }
            $user_names= '';
            for($s=0;$s<count($invitation_data);$s++)
            {
                if($s>0){ $user_names.=','; }
                $invitation_person = $this->User_model->getUserInfo(array('id' => $invitation_data[$s]['invitation_id']));
                $user_names.= ucfirst($invitation_person->first_name).' '.ucfirst($invitation_person->last_name);
                $user_ids[] =  $invitation_person->id_user;
            }
        }
        if(isset($data['id_meeting'])){ $meeting_id = $data['id_meeting']; }
        if($data['module_type']=='contact')
            $link = '<a  class="sky-blue" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
        else if($data['module_type']=='company')
            $link = '<a  class="sky-blue" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
        else if($data['module_type']=='project')
            $link = '<a  class="sky-blue" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
        else
            $link = '<a  class="sky-blue" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';

        //sending notifications and mails
        if(!isset($data['id_meeting']))
        {
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has scheduled meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$meeting_id.'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span>';
            if(trim($user_names)!='')
                $meetingTemplate.=' and invited <span class="blue">'.$user_names.'</span></p>';
            else $meetingTemplate.='</p>';

            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $meeting_id,
                'module_type' => $data['module_type'],
                'module_id' => $data['module_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            );


            $this->addActivity($activity);

            if(!empty($user_ids)){
                $participants = $this->User_model->getUsersInfo(array('id' => $user_ids));
                if(!empty($participants))
                    $participants = $participants->user_name;
                else
                    $participants = '';
                for($r=0;$r<count($user_ids);$r++)
                {

                    $assigned_user = $this->User_model->getUserInfo(array('id' => $user_ids[$r]));
                    $notification = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),
                        $this->lang->line('meeting_notification'));
                    $this->Notification_model->addNotification(array(
                        'assigned_to' =>  $user_ids[$r],
                        'assigned_by' =>  $data['created_by'],
                        'module_reference_id' => $meeting_id,
                        'module_reference_type' => 'meeting',
                        'notification_subject' => $reference_title,
                        'notification_template' => $notification,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'meeting',
                        'created_date_time' => currentDate(),
                        'module_id' => $data['module_id'],
                        'module_type' => $data['module_type']
                    ));
                    $link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
                    $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_mail'));
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'base_url' => REST_API_URL,
                        'to_username' => $assigned_user->first_name.' '.$assigned_user->last_name,
                        'message' => $message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    sendmail($assigned_user->email_id, str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject')),$template_data);
                }
            }
            for($s=0;$s<count($guest_data);$s++)
            {
                $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                    array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),
                    str_replace('Click {here} for details','',$this->lang->line('meeting_mail')));
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $guest_data[$s]['email'],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($guest_data[$s]['email'], str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject')),$template_data);
                //sendmail($guest_data[$s]['email'],'Meeting Invitation',$template_data);
            }
        }
        else if(isset($data['id_meeting']) && $data['meeting_status']=='pending')
        {
            $participants = $this->User_model->getUsersInfo(array('id' => $user_ids));
            if(!empty($participants))
                $participants = $participants->user_name;
            else
                $participants = '';
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has modified meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$data['id_meeting'].'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span></p>';
            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $data['id_meeting'],
                'module_type' => $data['module_type'],
                'module_id' => $data['module_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            );
            $this->addActivity($activity);

            if(!empty($invitations)){
                $user_ids = $invitations;
                for($r=0;$r<count($user_ids);$r++)
                {
                    $assigned_user = $this->User_model->getUserInfo(array('id' => $user_ids[$r]));
                    $notification = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_notification_modified'));
                    $this->Notification_model->addNotification(array(
                        'assigned_to' =>  $user_ids[$r],
                        'assigned_by' =>  $data['created_by'],
                        'module_reference_id' => $data['id_meeting'],
                        'module_reference_type' => 'meeting',
                        'notification_subject' => $reference_title,
                        'notification_template' => $notification,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'meeting',
                        'created_date_time' => currentDate(),
                        'module_id' => $data['module_id'],
                        'module_type' => $data['module_type']
                    ));
                    $link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($data['id_meeting']).'">here</a>';
                    $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_mail_modified'));
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'base_url' => REST_API_URL,
                        'to_username' => $assigned_user->first_name.' '.$assigned_user->last_name,
                        'message' => $message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    sendmail($assigned_user->email_id, str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_modified')),$template_data);
                }
            }
            for($s=0;$s<count($guest);$s++)
            {
                $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                    array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),
                    str_replace('Click {here} for details','',$this->lang->line('meeting_mail')));
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $guest[$s],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($guest[$s], str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_modified')),$template_data);
                //sendmail($guest[$s],'Meeting Invitation',$template_data);
            }
        }
        else if(isset($data['id_meeting']) && $data['meeting_status']=='cancelled')
        {
            $exists_invitation_data = $this->Activity_model->getInvitationsByMeetingId($data['id_meeting']);
            for($s=0;$s<count($exists_invitation_data);$s++){
                $user_ids[] = $exists_invitation_data[$s]['invitation_id'];
            }

            $participants = $this->User_model->getUsersInfo(array('id' => $user_ids));
            if(!empty($participants))
                $participants = $participants->user_name;
            else
                $participants = '';
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has cancelled meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$data['id_meeting'].'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span></p>';
            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $data['id_meeting'],
                'module_type' => $data['module_type'],
                'module_id' => $data['module_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            );
            $this->addActivity($activity);

            $meeting_details = $this->Activity_model->getMeeting(array('id_meeting' => $data['id_meeting']));
            $user_ids = explode(',',$meeting_details->invitation_id);
            if(!empty($user_ids)){
                for($r=0;$r<count($user_ids);$r++)
                {
                    $assigned_user = $this->User_model->getUserInfo(array('id' => $user_ids[$r]));
                    $notification = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_notification_cancelled'));
                    $this->Notification_model->addNotification(array(
                        'assigned_to' =>  $user_ids[$r],
                        'assigned_by' =>  $data['created_by'],
                        'module_reference_id' => $data['id_meeting'],
                        'module_reference_type' => 'meeting',
                        'notification_subject' => $reference_title,
                        'notification_template' => $notification,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'meeting',
                        'created_date_time' => currentDate(),
                        'module_id' => $data['module_id'],
                        'module_type' => $data['module_type']
                    ));
                    $link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
                    $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_mail_cancelled'));
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'base_url' => REST_API_URL,
                        'to_username' => $assigned_user->first_name.' '.$assigned_user->last_name,
                        'message' => $message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    sendmail($assigned_user->email_id, str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_cancelled')),$template_data);
                }
            }
            $guest_data = $this->Activity_model->getGuestByMeetingId($data['id_meeting']);
            for($s=0;$s<count($guest_data);$s++)
            {
                $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                    array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),
                    str_replace('Click {here} for details','',$this->lang->line('meeting_mail')));
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $guest_data[$s]['email'],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($guest_data[$s]['email'], str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_cancelled')),$template_data);
            }
        }
        else if(isset($data['id_meeting']) && $data['meeting_status']=='cancelled')
        {
            $exists_invitation_data = $this->Activity_model->getInvitationsByMeetingId($data['id_meeting']);
            for($s=0;$s<count($exists_invitation_data);$s++){
                $user_ids[] = $exists_invitation_data[$s]['invitation_id'];
            }

            $participants = $this->User_model->getUsersInfo(array('id' => $user_ids));
            if(!empty($participants))
                $participants = $participants->user_name;
            else
                $participants = '';
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has cancelled meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$data['id_meeting'].'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span></p>';
            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $data['id_meeting'],
                'module_type' => $data['module_type'],
                'module_id' => $data['module_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            );
            $this->addActivity($activity);

            $meeting_details = $this->Activity_model->getMeeting(array('id_meeting' => $data['id_meeting']));
            $user_ids = explode(',',$meeting_details->invitation_id);
            if(!empty($user_ids)){
                for($r=0;$r<count($user_ids);$r++)
                {
                    $assigned_user = $this->User_model->getUserInfo(array('id' => $user_ids[$r]));
                    $notification = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_notification_cancelled'));
                    $this->Notification_model->addNotification(array(
                        'assigned_to' =>  $user_ids[$r],
                        'assigned_by' =>  $data['created_by'],
                        'module_reference_id' => $data['id_meeting'],
                        'module_reference_type' => 'meeting',
                        'notification_subject' => $reference_title,
                        'notification_template' => $notification,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'meeting',
                        'created_date_time' => currentDate(),
                        'module_id' => $data['module_id'],
                        'module_type' => $data['module_type']
                    ));
                    $link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/meeting/list?meetingId='.base64_encode($meeting_id).'">here</a>';
                    $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                        array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),$this->lang->line('meeting_mail_cancelled'));
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'base_url' => REST_API_URL,
                        'to_username' => $assigned_user->first_name.' '.$assigned_user->last_name,
                        'message' => $message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    sendmail($assigned_user->email_id, str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_cancelled')),$template_data);
                }
            }
            $guest_data = $this->Activity_model->getGuestByMeetingId($data['id_meeting']);
            for($s=0;$s<count($guest_data);$s++)
            {
                $message = str_replace(array('{meeting_name}','{project_name}','{module_type}','{created_by}','{here}','{date}','{participants}','{location}','{description}'),
                    array($data['meeting_name'],$reference_title,$data['module_type'],$created_user->first_name.' '.$created_user->last_name,$link,date('M d, Y',strtotime($data['when'])).' ('.$data['from_time'].'-'.$data['to_time'].')',$participants,$data['where'],$data['agenda']),
                    str_replace('Click {here} for details','',$this->lang->line('meeting_mail')));
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $guest_data[$s]['email'],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($guest_data[$s]['email'], str_replace(array('{title}','{module_type}','{date_time}','{location}'),array($data['meeting_name'],$reference_title,date('M d, Y',strtotime($data['when'])),$data['where']),$this->lang->line('meeting_subject_cancelled')),$template_data);
            }
        }
        else if(isset($data['id_meeting']) && $data['meeting_status']=='completed')
        {
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has been completed meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$data['id_meeting'].'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span></p>';
            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $data['id_meeting'],
                'module_type' => $data['module_type'],
                'module_id' => $data['module_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            );
            $this->addActivity($activity);
        }

        $suc_msg = '';
        if(!isset($data['id_meeting'])){
            $suc_msg = $this->lang->line('meeting_add');
        }
        else if(isset($data['id_meeting'])){
            if(isset($data['meeting_status']) && $data['meeting_status']=='completed')
                $suc_msg = $this->lang->line('meeting_complete');
            else if(isset($data['meeting_status']) && $data['meeting_status']=='cancelled')
                $suc_msg = $this->lang->line('meeting_cancel');
            else
                $suc_msg = $this->lang->line('meeting_update');
        }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_meeting',array('required'=> $this->lang->line('meeting_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['meeting_status'] = 'deleted';
        $result = $this->Activity_model->updateMeeting($data);

        $this->deleteTouchPoint(array(
            'activity_type' => 'meeting',
            'activity_reference_id' => $data['id_meeting']
        ));
        $this->deleteNotification(array(
            'module_reference_id' => $data['id_meeting'],
            'module_reference_type' => 'meeting',
        ));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('meeting_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_get()
    {
        $data = $this->input->get();
        //getting attachment with document type and meeting id join
        if(!isset($data['id_meeting']) && (!isset($data['reference_id']) && !isset($data['reference_type']))) {
            //$
            //.this->form_validator->add_rules('reference_type', array('required' => $this->lang->line('reference_type_req')));
            if(!isset($data['project_stage_info_id'])){
                //$this->form_validator->add_rules('reference_id', array('required' => $this->lang->line('reference_id_req')));
            }
            //$this->form_validator->add_rules('project_stage_info_id', array('required' => $this->lang->line('project_stage_info_id_req')));
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*if(!isset($data['reference_id']) && isset($data['project_stage_info_id'])){
            $data['reference_id'] = $data['project_stage_info_id'];
            unset($data['project_stage_info_id']);
        }*/
        //$data['module_type'] = $data['reference_type'];
        $result = $this->Activity_model->getMeeting($data);
        $total_records = $this->Activity_model->getTotalMeetings($data);


        if(isset($data['user_id'])) {
            $data['status'] = 'pending';
            $data['assigned_to'] = $data['user_id'];
            $data['created_by'] = $data['user_id'];
            $data['due_meeting'] = 1;
            $pending_meeting = $this->Activity_model->getTotalMeetings($data);
            unset($data['due_meeting']);
            $data['upcoming_meeting'] = 1;
            $upcoming_meeting = $this->Activity_model->getTotalMeetings($data);
            //exit;

            if(isset($data['type']) && $data['type']=='due_meeting') {
                $total_records = $pending_meeting;
            }
            if(isset($data['type']) && $data['type']=='upcoming_meeting') {
                $total_records = $upcoming_meeting;
            }
        }
        //echo "<pre>"; print_r($result); exit;

        if(isset($data['id_meeting'])){
            $meeting_date = strtotime($result->when_date.' '.$result->from_time);
            $current_date = strtotime(date('Y-m-d h:i:s'));
            if($current_date>$meeting_date){ $result->meeting_expiry_status = 0; }

            $email = $this->Crm_model->getMeetingGuest(array('meeting_id' => $result->id_meeting));
            $result->guest = $email[0]['guest'];
            $data['module_type'] = $result->module_type;
            //$result->attachment = $this->Activity_model->getAttachments($data['id_meeting'],$data['crm_document_type_id']);
            $result->attachment = $this->Crm_model->getDocument(array('uploaded_from_id' => $data['id_meeting'],'module_type' => $data['module_type'],'form_key' => 'meeting'));
            for($r=0;$r<count($result->attachment);$r++){
                $result->attachment[$r]['full_path'] = getImageUrl($result->attachment[$r]['document_source']);
                $result->attachment[$r]['document_source'] = getImageUrl($result->attachment[$r]['document_source']);

            }
            $result->invitation_id = explode(',',$result->invitation_id);
            $result->invitation_name = explode(',',$result->invitation_user_name);
        }
        else
        {
            for($s=0;$s<count($result);$s++)
            {
                $email = $this->Crm_model->getMeetingGuest(array('meeting_id' => $result[$s]['id_meeting']));
                $result[$s]['guest'] = $email[0]['guest'];

                $result[$s]['invitation_id'] = explode(',',$result[$s]['invitation_id']);
                $result[$s]['invitation_name'] = explode(',',$result[$s]['invitation_user_name']);

                $result[$s]['attachment'] = $this->Crm_model->getDocument(array('module_type' => 'project','form_key' => 'meeting','uploaded_from_id' => $result[$s]['id_meeting']));
                for($r=0;$r<count($result[$s]['attachment']);$r++){
                    $result[$s]['attachment'][$r]['full_path'] = getImageUrl($result[$s]['attachment'][$r]['document_source']);
                    $result[$s]['attachment'][$r]['document_source'] = getImageUrl($result[$s]['attachment'][$r]['document_source']);

                }
            }
        }

        if(isset($data['user_id'])) {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_meetings' => $total_records,'pending_meetings' => $pending_meeting,'upcoming_meeting' => $upcoming_meeting));
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_meetings' => $total_records));
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function activity_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getActivity($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addActivity($data)
    {
        $data = $this->Activity_model->addActivity($data);
    }

    public function discussion_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getDiscussion($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function discussion_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('discussion_reference_id',array('required'=> $this->lang->line('dis_ref_id_req')));
        $this->form_validator->add_rules('discussion_type',array('required'=> $this->lang->line('dis_type_req')));
        $this->form_validator->add_rules('created_by',array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('discussion_description',array('required'=> $this->lang->line('dis_description_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['created_date_time'] = currentDate();
        $activity_discussion_id = $this->Activity_model->addDiscussion($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('dis_add'), 'data'=>$activity_discussion_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function discussion_delete()
    {
        $data = $this->input->get();
        $data['discussion_status'] = 'deleted';
        $result = $this->Activity_model->updateDiscussion($data);
        $total_records = $this->Activity_model->getTotalDiscussion($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addNotification($data)
    {
        $data = $this->Activity_model->addNotification($data);
    }

    public function notification_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getNotification($data);
        unset($data['offset']); unset($data['limit']);
        $total_records = count($this->Activity_model->getNotification($data));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function notification_delete()
    {
        $data = $this->input->get();
        $data['notification_status'] = 'deleted';
        $result = $this->Activity_model->updateNotification($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('notification_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approval_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('project_id',array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('forwarded_by',array('required'=> $this->lang->line('forward_by_req')));
        $this->form_validator->add_rules('forwarded_to',array('required'=> $this->lang->line('forward_to_req')));
        $this->form_validator->add_rules('committee_id',array('required'=> $this->lang->line('committee_id_req')));
        $this->form_validator->add_rules('approval_comments',array('required'=> $this->lang->line('comments_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['approval_status'] == 'approved')
        {
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $approval_project_data = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'],'company_approval_credit_committee_id' => $project_details[0]['committee_id']));
            if(!empty($approval_project_data)){
                $approval_id = $approval_project_data[0]['id_project_approval'];
                $this->Activity_model->updateApproval(array('id_project_approval' => $approval_project_data[0]['id_project_approval'],'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status']));
            }
        }
        else if($data['approval_status'] == 'rejected')
        {
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $approval_project_data = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'],'company_approval_credit_committee_id' => $project_details[0]['committee_id']));

            if(!empty($approval_project_data)){
                $approval_id = $approval_project_data[0]['id_project_approval'];
                $this->Activity_model->updateApproval(array('id_project_approval' => $approval_project_data[0]['id_project_approval'],'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status']));
                $this->Crm_model->updateProject(array('assigned_to' => $approval_project_data[0]['forwarded_by'], 'committee_id' => $approval_project_data[0]['committee_forwarded_by']),$data['project_id']);
            }
        }
        else
        {
            $approval_project_data = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'],'company_approval_credit_committee_id' => $data['committee_id']));
            if(!empty($approval_project_data)){
                $approval_id = $approval_project_data[0]['id_project_approval'];
                $this->Activity_model->updateApproval(array('id_project_approval' => $approval_project_data[0]['id_project_approval'],'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status']));
            }
            else{
                $latest = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'],'order_by' => 'DESC'));
                if(empty($latest)){
                    $committee_forwarded_by = 0;
                }
                else{
                    $committee_forwarded_by = $latest[0]['company_approval_credit_committee_id'];
                }
                $approval_id = $this->Activity_model->addApproval(array('project_id' => $data['project_id'], 'forwarded_by' => $data['forwarded_by'], 'forwarded_to' => $data['forwarded_to'], 'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status'], 'company_approval_credit_committee_id' => $data['committee_id'],'committee_forwarded_by' => $committee_forwarded_by,'created_date_time' => currentDate()));
            }

        }

        if(isset($data['forwarded_to']) && $data['approval_status'] == 'forwarded')
            $this->Crm_model->updateProject(array('assigned_to' => $data['forwarded_to'], 'committee_id' => $data['committee_id']),$data['project_id']);

        if(isset($data['approval_status'])){
            $approvalStatus = $data['approval_status'];
            if($data['approval_status'] == 'forwarded') $approvalStatus = 'pre_approval';
            if($data['approval_status'] == 'approved')
                $this->Crm_model->updateProject(array('project_status' => $data['approval_status'],'approval_comments' => $data['approval_comments']),$data['project_id']);
        }

        $project = $this->Crm_model->getProject($data['project_id']);
        if(isset($data['committee_id']))
            $committee_details = $this->Company_model->getApprovalCreditCommittee($data);

        if(isset($data['forwarded_by']))
            $forwarded_by = $this->Company_model->getCompanyUser($data['forwarded_by']);

        if(isset($data['forwarded_to']))
            $forwarded_to = $this->Company_model->getCompanyUser($data['forwarded_to']);

        $activity_template = $notification = $notification_template = '';
        $suc_msg = $this->lang->line('success');
        if(isset($data['approval_status']) && $data['approval_status']=='forwarded'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has forwarded to <span class="blue">'.$committee_details[0]['credit_committee_name'].'</span> assigned to <span class="blue">'.ucfirst($forwarded_to->first_name).' '.ucfirst($forwarded_to->last_name).'</span><p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $notification_template = '<p class="f12"><a class="sky-blue" href="javascript:;">'.$project[0]['project_title'].'</a>project has been forwarded for approval to you</p>';
            $suc_msg = $this->lang->line('approval_frw');
        }
        else if(isset($data['approval_status']) && $data['approval_status']=='approved'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has <span class="blue">Approved</span> project <p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $suc_msg = $this->lang->line('approval_approve');
        }
        else if(isset($data['approval_status']) && $data['approval_status']=='rejected'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has <span class="blue">Rejected</span> project <p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $suc_msg = $this->lang->line('approval_rej');
        }

        $activity = array(
            'activity_name' => 'Project approval',
            'activity_type' => 'approval',
            'activity_template' =>$activity_template,
            'activity_reference_id' => $approval_id,
            'module_type' => 'Project',
            'module_id' => $data['project_id'],
            'created_by' => $data['forwarded_by'],
            'created_date_time' => currentDate()
        );
        $notification = array(
            'sent_by' => $data['forwarded_by'],
            'sent_to' => $data['forwarded_to'],
            'project_id' => $data['project_id'],
            'notification_type' => 'approval',
            'notification_template' => $notification_template,
            'created_date_time' => currentDate()
        );
        if(!empty($activity))
            $this->addActivity($activity);
        if(!empty($notification) && $data['approval_status'] == 'forwarded')
            $this->addNotification($notification);

        //send mail to forwarded to user
        if(isset($data['approval_status']) && $data['approval_status']=='forwarded'){
            $user_deatils = $this->User_model->getUserInfo(array('id' => $data['forwarded_to']));
            sendmail($user_deatils->email_id,'Project Forwarded For Approval',$notification_template);
        }

        //forwarding facilities
        $project_approval = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'], 'company_approval_credit_committee_id' => $data['committee_id']));
        if(empty($project_approval))
        {
            $facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['project_id']));
            for($s=0;$s<count($facility);$s++)
            {
                $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => '','created_date_time' => currentDate()));
                $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => '','created_date_time' => currentDate()));
            }
        }
        else
        {
            $facility = $this->Crm_model->getFacilityActivity(array('project_approval_id' => $project_approval[0]['id_project_approval']));
            if(empty($facility))
            {
                $facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['project_id']));
                for($s=0;$s<count($facility);$s++)
                {
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => '','created_date_time' => currentDate()));
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => '','created_date_time' => currentDate()));
                }
            }
            else
            {
                for($s=0;$s<count($facility);$s++)
                {
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['project_facility_id'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['facility_amount'], 'facility_status' => $facility[$s]['facility_status'],'facility_comments' => $facility[$s]['facility_comments'],'created_date_time' => currentDate()));
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function deleteNotification($data)
    {
        $this->Activity_model->deleteNotification($data);
        return 1;
    }

    function deleteTouchPoint($data)
    {
        $this->Activity_model->deleteTouchPoint($data);
    }


}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Documentdownload extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('Workflow_model');
    }
    public function termSheet_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_stage_info', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_stage_details = $this->Project_model->getProjectStageDetails(array('project_stage_info_id' => $data['id_project_stage_info']));
        $data['project_id'] = $project_stage_details[0]['project_id'];
        $data['stage_id'] = $project_stage_details[0]['stage_id'];
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
        $country_office = $created_by[0]['branch_name'];
        $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));
        $loan_amount = array_map(function($i){ return $i['facility_usd_amount']; },$facilities);
        $loan_amount = array_sum($loan_amount);
        $contract_id = 0;
        $project_date = $project_details[0]['created_date_time'];
        $project_name = $project_details[0]['project_title'];
        if(!empty($facilities)){ $contract_id = $facilities[0]['contract_id']; }
        $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
        $company_name = $share_holder_info = ''; $info = array();
        $company_keys = array('company_ownership_details','company_street_name','company_block_number','company_city','company_region','company_country');
        $info = array('company_ownership_details' => '','company_street_name' => '','company_block_number' => '','company_city' => '','company_region' =>'','company_country' => '');
        if(!empty($borrower)){
            $company_name = $borrower[0]['company_name'];
            $key_data = $this->Project_model->getCrmCompanyDataByKeys(array('crm_company_id' => $borrower[0]['id_crm_company'],'keys' => $company_keys));
            if(!empty($key_data)){
                for($sr=0;$sr<count($key_data);$sr++) {
                    $info[$key_data[$sr]['field_name']] = $key_data[$sr]['form_field_value'];
                }
            }
        }
        $share_holder_info = $info['company_ownership_details'];
        unset($info['company_ownership_details']);
        $company_address = $info;
        $required_keys = array('project_contract_number','project_basic_information_other','project_location','pre_conditions_for_disbursement','procurement_of_goods','insurance','other_conditions','security','representations_and_warranties','information_undertakings','financial_covenants','general_undertakings','events_of_default','special_audit','legal_documentation','costs_and_expenses','applicable_law','dispute_resolution','other');
        $result_array = array('project_title' => $project_name,'loan_amount' => $loan_amount,'application_date' => $project_date,'country_office' => $country_office,'project_contract_number' => '','company_name' => $company_name,'company_address'=>$company_address,'share_holder_info' => $share_holder_info,'project_location' =>'','project_basic_information_other' => '', 'interest_rate' => '','facilities' => '','pre_conditions_for_disbursement' => '','procurement_of_goods' => '','insurance' => '','other_conditions' => '','security' => '','representations_and_warranties' => '','information_undertakings' => '','financial_covenants' => '','general_undertakings' => '','events_of_default' => '','special_audit' => '','legal_documentation' => '','costs_and_expenses' => '','applicable_law' => '','dispute_resolution' => '','other' => '');
        $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
        //echo "<pre>"; print_r($key_details); exit;
        for($s=0;$s<count($key_details);$s++){
            $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
        }
        $appraisal_fee = '';
        for($s=0;$s<count($facilities);$s++)
        {
            $interest_rate = '';
            $loan_type = '---';
            $type_of_loan = $this->Master_model->getMasterChild(array('child_id' => $facilities[$s]['loan_payment_type']));
            $pricing_array = $terms_condition_array = '';
            $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'pricing'));
            $terms_conditions = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
            //echo "<pre>"; print_r($pricing); exit;
            for($r=0;$r<count($pricing);$r++){
                $pricing_array[$r] = array(
                    'name' => $pricing[$r]['field_name'],
                    'payment_type' => $pricing[$r]['payment_type'],
                    'label' => '',
                    'field_key' => $pricing[$r]['field_key'],
                    'facility_field_value' => $pricing[$r]['facility_field_value'],
                    'facility_field_description' => $pricing[$r]['facility_field_description']
                );
                if($pricing[$r]['payment_type']=='interest_as'){
                    $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                    $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                }
                else if($pricing[$r]['payment_type']=='fee_as'){
                    $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                    $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                }
                else if($pricing[$r]['payment_type']=='fee_amount'){
                    $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                    $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                }

                if($pricing[$r]['field_key']=='interest_rate'){
                    $interest_rate =  $pricing_array[$r];
                }
                if($pricing[$r]['field_key'] == 'apraisal_fee_commission')
                {
                    $appraisal_fee = $pricing[$r]['facility_field_value'].'% '.$pricing_array[$r]['label'];
                }
            }
            //echo $appraisal_fee;exit;
            $pricing_array = array_values($pricing_array);
            //echo "<pre>"; print_r($terms_conditions); exit;

            for($r=0;$r<count($terms_conditions);$r++){
                $terms_condition_array[] = array(
                    'name' => $terms_conditions[$r]['field_name'],
                    'description' => $terms_conditions[$r]['facility_field_description']
                );
            }

            if(!empty($type_of_loan)){ $loan_type = $type_of_loan[0]['child_name']; }

            $result_array['facilities'][] = array(
                'facility_name' => $facilities[$s]['project_facility_name'],
                'currency' => $facilities[$s]['currency_code'],
                'amount' => $facilities[$s]['facility_amount'],
                'usd_amount' => $facilities[$s]['facility_usd_amount'],
                'type_of_loan' => $loan_type,
                'facility_purpose' => $facilities[$s]['facility_purpose'],
                'facility_minimum_amount' => $facilities[$s]['facility_amount'],
                'other_facility_modalities' => $facilities[$s]['other_facility_modalities'],
                'security' => $facilities[$s]['facility_security'],
                'maturity_loan_repayment' => $facilities[$s]['facility_tenor'],
                'interest_payment' => $interest_rate,
                'pricing' => $pricing_array,
                'terms_conditions' => $terms_condition_array
            );
        }
        $project_stage_details[0]['fields'] = $result_array;
        $project_stage_details = $project_stage_details[0];
        require_once './phpdocx/classes/CreateDocx.inc';
        $docx = new CreateDocx();
        $docx->setDefaultFont('Calibri');
        $template_collection_data=$result_array;

        foreach ($template_collection_data as $key => $value)
        {
            if(!is_array($value))
                $template_collection_data[$key] = preg_replace('/<table (.*?)>/', '<table border="1" style="border-collapse: collapse;border-color: #000000;" >',$value);
        }

        $reference_number=$template_collection_data['project_contract_number'];
        $project_name=$template_collection_data['project_title'];
        $loan_amount=number_format($template_collection_data['loan_amount']);
        $country=$template_collection_data['country_office'];
        $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
        $company_name=$template_collection_data['company_name'];
        $company_address='';
        $flag=0;
        if($company_name!='')
        {
            $company_address.=$company_name.',<br/>';
        }
        if($template_collection_data['company_address']['company_street_name']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_street_name'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_block_number']!='')
        {
            if($template_collection_data['company_address']['company_street_name']!='')
            {
                $company_address.=',';
            }
            $company_address.=$template_collection_data['company_address']['company_block_number'];
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.='<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_city']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_city'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_region']!='')
        {
            if($template_collection_data['company_address']['company_city']!='')
            {
                $company_address.=',';
            }
            $company_address.=$template_collection_data['company_address']['company_region'];
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.=',<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_country']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_country'];
        }
        //$appraisal_fee=$template_collection_data['appraisal_fee'];
        //$country_head=$template_collection_data['country_head'];
        //$appraisal_fee='';
        $country_head='[Name of Head of Country]';
        $legal_officer_name='[Enter Name of Legal Officer]';
        $share_holder_data=json_decode($template_collection_data['share_holder_info'],true);
        for($i=0;$i<count($share_holder_data);$i++)
        {
            if($share_holder_data[$i]['ownership_percentage']!='')
                $share_holder_data[$i]['ownership_percentage']=$share_holder_data[$i]['ownership_percentage'].'%';
        }
        //Need to build the phpdocx table
        $load_data=array(array('Name','Number Of Shares','Ownership Percentage','Value Of Shares','Currency'));
        for($t=0;$t<count($share_holder_data);$t++)
        {
            $share_holder_array = (array)$share_holder_data[$t];
            if(isset($share_holder_array['$$hashKey']))
                unset($share_holder_array['$$hashKey']);
            array_push($load_data,array_values($share_holder_array));
        }
        $share_holder_table=$docx->buildArrayToTable($load_data,'termsheet');
        //End of table build
        $project_location=$template_collection_data['project_location'];
        $project_other_information=$template_collection_data['project_basic_information_other'];
        $facility_list_data='';
        $list_item=1;$list_subitem=1;
        if(isset($template_collection_data['facilities']) && $template_collection_data['facilities']!='')
        {
            for($fc=0;$fc<count($template_collection_data['facilities']);$fc++)
            {
                $facilities_data=$template_collection_data['facilities'][$fc];
                $list_item=$list_item+1;
                $list_subitem=1;
                $facility_list_data.='<tr style="font-weight:bold;"><td style="width:90;">'.$list_item.'.</td><td style="width:100;">FACILITY</td><td style="width:250;">'.$facilities_data['facility_name'].'</td></tr>';
                $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">Currency:</td><td style="width:250;">'.$facilities_data['currency'].'</td></tr>';
                $facility_amount = number_format($facilities_data['amount']);
                if($facilities_data['currency'] != 'USD')
                    $facility_amount .=' (USD '.number_format($facilities_data['usd_amount']).')';
                $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">Amount:</td><td style="width:250;">'.$facility_amount.'</td></tr>';
                $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">Type of Loan:</td><td style="width:250;">'.$facilities_data['type_of_loan'].'</td></tr>';
                $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">Purpose of the Facility:</td><td style="width:250;">'.$facilities_data['facility_purpose'].'</td></tr>';
                $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">Minimum Amount of Facility:</td><td style="width:250;">'.number_format($facilities_data['facility_minimum_amount']).'</td></tr>';
                $list_item=$list_item+1;
                if(count($facilities_data['pricing'])>0)
                {
                    $facility_list_data.='<tr style="font-weight:bold;"><td>'.$list_item.'.</td><td colspan="2">PRICING</td></tr>';
                    $list_subitem=1;
                    for($pr=0;$pr<count($facilities_data['pricing']);$pr++)
                    {
                        $facility_pricing=$facilities_data['pricing'][$pr];
                        if($facility_pricing['payment_type'] == 'fee_amount')
                            $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">'.$facility_pricing['name'].'</td><td style="width:250;">'.number_format($facility_pricing['facility_field_value']).' '.$facility_pricing['label'].$facility_pricing['facility_field_description'].'</td></tr>';
                        else
                            $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td><td style="width:100;">'.$facility_pricing['name'].'</td><td style="width:250;">'.$facility_pricing['facility_field_value'].'% '.$facility_pricing['label'].$facility_pricing['facility_field_description'].'</td></tr>';
                    }
                }
                $list_item=$list_item+1;
                if(count($facilities_data['terms_conditions'])>0)
                {
                    $list_subitem=1;
                    $facility_list_data.='<tr style="font-weight:bold;"><td>'.$list_item.'</td><td colspan="2">REPAYMENT, PREPAYMENT AND CANCELLATION</td></tr>';//interest_payment
                    if(isset($facilities_data['interest_payment']['facility_field_value']))
                        $facility_list_data.='<tr><td>'.$list_item.'.'.$list_subitem.'</td><td>Interest Payment: </td><td>'.$facilities_data['interest_payment']['facility_field_value'].'% '.$facilities_data['interest_payment']['label'].$facilities_data['interest_payment']['facility_field_description'].'</td></tr>';
                    else
                        $facility_list_data.='<tr><td>'.$list_item.'.'.$list_subitem.'</td><td>Interest Payment: </td><td></td></tr>';
                    for($rp=0;$rp<count($facilities_data['terms_conditions']);$rp++)
                    {
                        $list_subitem=$list_subitem+1;
                        $facility_repayments=$facilities_data['terms_conditions'][$rp];
                        $facility_list_data.='<tr><td style="width:90;">'.$list_item.'.'.$list_subitem.'.</td><td style="width:100;">'.$facility_repayments['name'].'</td><td style="width:250;">'.$facility_repayments['description'].'</td></tr>';
                    }
                }
            }
        }


        $pre_conditions_for_disbursement=$template_collection_data['pre_conditions_for_disbursement'];
        $procurement_of_goods=$template_collection_data['procurement_of_goods'];
        $insurance=$template_collection_data['insurance'];
        $other_conditions=$template_collection_data['other_conditions'];
        $security=$template_collection_data['security'];
        $representations_and_warranties=$template_collection_data['representations_and_warranties'];
        $information_undertakings=$template_collection_data['information_undertakings'];
        $financial_covenants=$template_collection_data['financial_covenants'];
        $general_undertakings=$template_collection_data['general_undertakings'];
        $events_of_default=$template_collection_data['events_of_default'];
        $special_audit=$template_collection_data['special_audit'];
        $legal_documentation=$template_collection_data['legal_documentation'];
        $costs_and_expenses=$template_collection_data['costs_and_expenses'];
        $applicable_law=$template_collection_data['applicable_law'];
        $dispute_resolution=$template_collection_data['dispute_resolution'];
        $other_terms=$template_collection_data['other'];

        $textOptions = array(
            'textAlign' => 'right',
            'fontSize' => 13,
            'color' => '000000',
        );
        $headerText = new WordFragment($docx, 'defaultHeader');
        $headerText->embedHTML('<p style="font-size:9;">'.$country.', '.$application_date.'</p>');
        $firstHeader = new WordFragment($docx, 'firstHeader');
        $firstHeader->embedHTML('<div style="font-style:italic;font-family: \"Times New Roman\", Georgia, Serif;"><h3 style="font-size:20;margin-bottom:0;">EAST AFRICAN DEVELOPMENT BANK</h3><span style="font-size:14;margin-top: 0;">Head Office</span></div>');
        $docx->addHeader(array('default' => $headerText, 'first' => $firstHeader));

        $pageNumberOptions = array(
            'textAlign' => 'center',
            'fontSize' => 10
        );
        $defaultFooter = new WordFragment($docx, 'defaultFooter');
        $defaultFooter->addPageNumber('numerical', $pageNumberOptions);
        $firstFooter = new WordFragment($docx, 'firstFooter');
        $firstFooter->embedHTML('<table width="100%" style="width100%;border-collapse:collapse;padding-top:50;border-top: 1 solid #000000;font-size: 8;font-style: italic;font-family: \"Times New Roman\", Georgia, Serif;">
        <tr><td colspan="4"></td></tr>
    	<tr>
        	<td>
        	    <span style="font-weight: bold">Country office, Kenya</span><br>
                2nd Floor, Rahimtulla Tower<br>
                Upper Hill Road<br>
                P.O Box 47685, Nairobi<br>
                Tel: 254-20-340642/340656<br>
                Fax: 254-20-2731590<br>
                E-mail:cok@eadb.org
            </td>
            <td>
            	<span style="font-weight: bold">HEADQUARTERS<br>
                Kampala, Uganda</span><br>
                EADB Bldng, 4 Nile Avenue<br>
                P.O Box 7128, Kampala<br>
                Tel: 256-41-4230021/5<br>
                Fax: 256-41-4259763/253585<br>
                E-mail: admin@eadb.org
            </td>
            <td>
            	<span style="font-weight: bold">Country office, Tanzania</span><br>
                7th Floor, NSSF Waterfront House Sokoine Drive<br>
                P.O Box 9401,Dar-es-Salaam<br>
                Tel: 255-22-2113195/2116981<br>
                Fax: 255-22-2113197<br>
                E-mail:cot@eadb.org
            </td>
            <td>
            	<span style="font-weight: bold">Country Office, Rwanda</span><br>
                Kacyiru, Glory House, Grnd Floor<br>
                B.P. 6225, Kigali<br>
                Tel: 0037 570323<br>
                E-mail: cor@eadb.org
            </td>
        </tr>
        <tr><td colspan="4" style="font-weight: bold;font-style:normal;text-align: center;padding-top: 12;font-size:10;"><a href="http://eadb.org/" style="text-decoration: none;color: #000000">www.eadb.org</a></td></tr>
    </table>');
        $docx->addFooter(array('default' => $defaultFooter, 'first' => $firstFooter));
        $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));

        $termssheet_template1 = '<table style="width: 100%;font-size:10;padding-top: 100;">
            <tr>
                <td style="text-align: left"><span style="font-weight: bold;">Our Ref:</span> '.$reference_number.'</td>
                <td style="text-align: right">'.$application_date.'</td>
            </tr>
        </table>
        <div style="font-size:10;">
        <p style="padding:10 0 15 0;font-size:10;">'.$company_address.'</p>
        <p>Dear Madam/Sir,</p>
        <p style="font-weight:bold;">RE: INDICATIVE TERM SHEET FOR THE PROJECT '.$project_name.' of up to USD '.$loan_amount.'</p>
        <p>We refer to your application for a facility of USD '.$loan_amount.' to [enter manually the purpose of the facility].</p>
        <p>We wish to inform you that following a preliminary review of your project, the bank is, in principle, ready to embark on the detailed appraisal of the project.</p>
        <p>Enclosed herewith is an indicative term sheet for the proposed facility.
        To signify your acceptance of the indicative terms, please sign and return to us one (1) copy of the same and arrange to pay not later than five (5) working days after
        the date hereof, the '.$appraisal_fee.' appraisal fee in the amount of [enter manually the amount of the appraisal fee] to enable us embark on the detailed appraisal. </p>
        <p>Please note that this is not an offer for a loan. The decision to provide a loan facility will be subject to the outcome of the detailed appraisal and approval by the Bank`s management.</p>
        <p style="margin:0">Yours sincerely</p>
        <p style="font-weight:bold;margin:0;padding-bottom:40;">EAST AFRICAN DEVELOPMENT BANK</p>
        </div>
        <table style="width: 100%;font-size:10">
        <tr style="font-size:10;"><td>'.$country_head.' </td><td>'.$legal_officer_name.'</td></tr>
        <tr style="font-weight: bold;text-decoration: underline;font-size:10;"><td>HEAD OF COUNTRY BUSINESS, '.$country.'</td><td>LEGAL OFFICER</td></tr>
        </table>
        <div style="text-align:center;font-size:10">
	    <h3 style="font-size:20;font-weight:bold;text-align:center;padding-top: 650;">`SUBJECT TO CONTRACT`</h3>
    	<h3 style="font-size: 16;">CONFIDENTIAL</h3>
        <h3 style="font-size: 12;">'.$project_name.'</h3>
        <h3 style="font-size: 16;">INDICATIVE FACILITY TERM SHEET</h3>
        <p style="text-decoration: underline;font-weight: bold;line-height: 1.5">INDICATIVE TERM SHEET FOR THE PROJECT '.$project_name.' of up to USD '.$loan_amount.'.</p>
        </div>
        <div style="font-style: italic;font-size:10;line-height: 1.5;text-align: justify;">
            <p>The following indicative terms and conditions have been prepared for the sole purpose of serving as a basis for discussion of the East African Development Bank`s (the Bank or EADB) principal investment terms and conditions, if the Bank were to extend a Loan to '.$company_name.'. All figures, terms and conditions are subject to change.</p>
            <p>These terms and conditions are not therefore, an offer and do not represent a commitment on the part of the Bank to extend a Loan from the Bank or to a extend Loan on these terms and do not have precedence over an eventual Loan agreement. EADB`s decision to extend any financing is subject to the approval of EADB Management and its Board of Directors, as well as the entry into force of all the finance and project documents, and fulfilment of all conditions, including, without limitation, technical and legal. Moreover, EADB reserves the right to terminate discussions on the facility at any time during the appraisal period and shall not be obliged to assign reasons for doing so.</p>
            <p>Nothing in this Term Sheet shall be construed as a waiver, renunciation or other modification of any immunities, privileges or exemptions of the Bank accorded under the Charter and Treaty Establishing the East African Development Bank (as the same may be amended and/or re-enacted from time to time), any headquarters or host country agreement, international conventions or other applicable law. The property, assets and archives of the Bank and all documents belonging to it, or held by it, shall be inviolable wherever located.</p>
            <p>Neither the present term sheet, nor the investment conditions, can be communicated to third parties without the prior written consent of the Bank.</p>
        </div>';

        $termssheet_template2='<table border="1" width="100%" style="width: 100%;border-collapse: collapse;border-color: #000000;padding-top: 100;font-size:10;" >
			<tbody>
				<tr>
					<td nowrap="nowrap" style="width:90;font-weight: bold;text-align: left;">1.</td>
					<td nowrap="nowrap" style="width:100;font-weight: bold;text-align: left;">PARTIES</td>
					<td nowrap="nowrap" style="width:250;font-weight:bold;text-align: center;"></td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;text-align: left">1.1.</td>
					<td nowrap="nowrap" style="width:100;text-align: left">Lender:</td>
					<td nowrap="nowrap" style="width:250;">East African Development Bank ("EADB" or the "Bank")</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">1.2.</td>
					<td nowrap="nowrap" style="width:100;">Borrower:</td>
					<td nowrap="nowrap" style="width:250;">'.$company_name.' (the "Borrower")</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">1.3.</td>
					<td nowrap="nowrap" style="width:100;">Shareholders:</td>
					<td nowrap="nowrap" style="width:250;">'.$share_holder_table.'</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">1.4.</td>
					<td nowrap="nowrap" style="width:100;">Others:</td>
					<td nowrap="nowrap" style="width:250;">'.$project_other_information.'</td>
				</tr>';

        $termssheet_template2.=$facility_list_data;
        $list_subitem=1;
        $list_item=$list_item+1;
        $termssheet_template2.='<tr>
					<td nowrap="nowrap" style="width:90;font-weight: bold;">'.$list_item.'</td>
					<td colspan="2" nowrap="nowrap" style="width:100;font-weight: bold;">CONDITIONS PRECEDENT TO DISBURSEMENT</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Preconditions of Disbursement:</td>
					<td nowrap="nowrap" style="width:250;">'.$pre_conditions_for_disbursement.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Procurement of Goods:</td>
					<td nowrap="nowrap" style="width:250;">'.$procurement_of_goods.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Insurance</td>
					<td nowrap="nowrap" style="width:250;">'.$insurance.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Other Conditions</td>
					<td nowrap="nowrap" style="width:250;">'.$other_conditions.'</td>
				</tr>';
        $list_item=$list_item+1;
        $list_subitem=1;
        $termssheet_template2.='<tr>
					<td nowrap="nowrap" style="width:90;font-weight: bold;">'.$list_item.'</td>
					<td nowrap="nowrap" style="width:100;font-weight: bold;">SECURITY</td>
					<td nowrap="nowrap" style="width:250;"></td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Security: </td>
					<td nowrap="nowrap" style="width:250;">'.$security.'</td>
				</tr>';
        $list_item=$list_item+1;
        $list_subitem=1;
        $termssheet_template2.='<tr>
					<td nowrap="nowrap" style="width:90;font-weight: bold;">'.$list_item.'</td>
					<td nowrap="nowrap" style="width:100;font-weight: bold;" colspan="2">REPRESENTATIONS AND WARRANTIES, CONVENANTS AND EVENTS OF DEFAULT</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Representations and Warranties:</td>
					<td nowrap="nowrap" style="width:250;">'.$representations_and_warranties.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Information Undertakings:</td>
					<td nowrap="nowrap" style="width:250;">'.$information_undertakings.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Financial Covenants:</td>
					<td nowrap="nowrap" style="width:250;">'.$financial_covenants.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">General Undertakings:</td>
					<td nowrap="nowrap" style="width:250;">'.$general_undertakings.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Events of Default:</td>
					<td nowrap="nowrap" style="width:250;">'.$events_of_default.'</td>
				</tr>';
        $list_item=$list_item+1;
        $list_subitem=1;
        $termssheet_template2.='<tr>
					<td nowrap="nowrap" style="width:90;font-weight: bold;">'.$list_item.'</td>
					<td nowrap="nowrap" style="width:100;font-weight: bold;" colspan="2">OTHER PROVISIONS</td>
				</tr>
                <tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Special Audit:</td>
					<td nowrap="nowrap" style="width:250;">'.$special_audit.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Legal Documentation:</td>
					<td nowrap="nowrap" style="width:250;">'.$legal_documentation.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Costs and Expenses: </td>
					<td nowrap="nowrap" style="width:250;">'.$costs_and_expenses.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Applicable Law:</td>
					<td nowrap="nowrap" style="width:250;">'.$applicable_law.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Dispute Resolution:</td>
					<td nowrap="nowrap" style="width:250;">'.$dispute_resolution.'</td>
				</tr>
				<tr>
					<td nowrap="nowrap" style="width:90;">'.$list_item.'.'.$list_subitem++.'.</td>
					<td nowrap="nowrap" style="width:100;">Others:</td>
					<td nowrap="nowrap" style="width:250;">'.$other_terms.'</td>
				</tr>
			</tbody>
		</table>
		<div style="font-size:10;">
            <p style="text-align: justify;margin-bottom: 100;">
            We on behalf of '.$company_name.' confirm that this Indicative Term Sheet reflects the understanding as at the date hereof and serves to facilitate further processing of the proposed Bank financial assistance, but is not legally binding on the parties hereto. We accept the terms outlined above and we request EADB to commence appraisal of our project. We enclose our Cheque No. .................................... for <span style="font-style:italic;">[enter manually amount of appraisal fee]</span> being <span style="font-style:italic;">[appraisal fee in %]</span> deposit of the appraisal fees.</p>
            <table style="width:100%;">
                <tr>
                    <td>Signed: ...............................................................</td>
                    <td>...............................................................</td>
                </tr>
                <tr style="text-align: center;">
                    <td><p>DIRECTOR</p></td>
                    <td><p>DATE</p></td>
                </tr>
            </table>
            <table style="width:100%;">
                <tr>
                    <td>Signed: ...............................................................</td>
                    <td>...............................................................</td>
                </tr>
                <tr style="text-align: center;margin-top: 100">
                    <td><p>DIRECTOR/SECRETARY</p></td>
                    <td><p>DATE</p></td>
                </tr>
            </table>
		 </div>';

        $docx->embedHTML($termssheet_template1);
        $docx->addBreak(array('type' => 'page'));
        $docx->embedHTML($termssheet_template2);
        $filename=str_replace(' ','_',$project_name).'_indicative_facility_term_sheet_'.date("d-m-Y");
        $documentfile='template_docx/'.$filename;
        $returnfilename=REST_API_URL.$documentfile.'.docx';
        $docx->createDocx($documentfile);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$returnfilename);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityTermSheet_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_stage_info', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_stage_details = $this->Project_model->getProjectStageDetails(array('project_stage_info_id' => $data['id_project_stage_info']));
        $data['project_id'] = $project_stage_details[0]['project_id'];
        $data['stage_id'] = $project_stage_details[0]['stage_id'];
        $project_details = $this->Crm_model->getProject($data['project_id']);

        $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
        $country_office = $created_by[0]['branch_name'];
        $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));
        $loan_amount = array_map(function($i){ return $i['facility_usd_amount']; },$facilities);
        $loan_amount = array_sum($loan_amount);
        $contract_id = 0;
        $project_date = $project_details[0]['created_date_time'];
        $project_name = $project_details[0]['project_title'];
        $background_of_customer = $project_details[0]['background_of_customer'];
        $approval_request = $project_details[0]['approval_request'];
        if(!empty($facilities)){ $contract_id = $facilities[0]['contract_id']; }
        $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
        $company_name = $share_holder_info = ''; $info = array();
        $company_keys = array('company_ownership_details','company_street_name','company_block_number','company_city','company_region','company_country','company_financial_statement_comments');
        $info = array('company_ownership_details' => '','company_street_name' => '','company_block_number' => '','company_city' => '','company_region' =>'','company_country' => '','company_financial_statement_comments'=>'');
        if(!empty($borrower)){
            $company_name = $borrower[0]['company_name'];
            $key_data = $this->Project_model->getCrmCompanyDataByKeys(array('crm_company_id' => $borrower[0]['id_crm_company'],'keys' => $company_keys));
            if(!empty($key_data)){
                for($sr=0;$sr<count($key_data);$sr++) {
                    $info[$key_data[$sr]['field_name']] = $key_data[$sr]['form_field_value'];
                }
            }
        }
        $share_holder_info = $info['company_ownership_details'];
        unset($info['company_ownership_details']);
        $company_address = $info;

        $required_keys = array('approval_request','critical_credit_issues','risk_and_relationship_strategy','transaction_dynamics','ways_out_analysis','banking_relationships','risk_analysis','country_overview_industry_risk','other_conditions');
        $result_array = array('project_title' => $project_name,'background_of_customer' => $background_of_customer ,'loan_amount' => $loan_amount,'application_date' => $project_date,'country_office' => $country_office,'project_contract_number' => '','company_name' => $company_name,'company_address'=>$company_address,'share_holder_info' => $share_holder_info,'project_location' =>'','project_basic_information_other' => '', 'interest_rate' => '','facilities' => '','pre_conditions_for_disbursement' => '','procurement_of_goods' => '','insurance' => '','other_conditions' => '','security' => '','representations_and_warranties' => '','information_undertakings' => '','financial_covenants' => '','general_undertakings' => '','events_of_default' => '','special_audit' => '','legal_documentation' => '','costs_and_expenses' => '','applicable_law' => '','dispute_resolution' => '','other' => '', 'ways_out_analysis' => '');
        $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
        //echo "<pre>"; print_r($key_details); exit;
        for($s=0;$s<count($key_details);$s++){
            $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
        }
        $appraisal_fee = '';
        for($s=0;$s<count($facilities);$s++)
        {
            $interest_rate = '';
            $loan_type = '---';
            $type_of_loan = $this->Master_model->getMasterChild(array('child_id' => $facilities[$s]['loan_payment_type']));
            $pricing_array = $terms_condition_array = '';
            $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'pricing'));
            $terms_conditions = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
            //echo "<pre>"; print_r($pricing); exit;
            for($r=0;$r<count($pricing);$r++){
                $pricing_array[$r] = array(
                    'name' => $pricing[$r]['field_name'],
                    'payment_type' => $pricing[$r]['payment_type'],
                    'label' => '',
                    'field_key' => $pricing[$r]['field_key'],
                    'facility_field_value' => $pricing[$r]['facility_field_value'],
                    'facility_field_description' => $pricing[$r]['facility_field_description']
                );
                if($pricing[$r]['payment_type']=='interest_as'){
                    $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                    $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                }
                else if($pricing[$r]['payment_type']=='fee_as'){
                    $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                    $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                }
                else if($pricing[$r]['payment_type']=='fee_amount'){
                    $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                    $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                }

                if($pricing[$r]['field_key']=='interest_rate'){
                    $interest_rate =  $pricing_array[$r];
                }
                if($pricing[$r]['field_key'] == 'apraisal_fee_commission')
                {
                    $appraisal_fee = $pricing[$r]['facility_field_value'].'% '.$pricing_array[$r]['label'];
                }
            }
            //echo $appraisal_fee;exit;
            $pricing_array = array_values($pricing_array);
            //echo "<pre>"; print_r($terms_conditions); exit;

            for($r=0;$r<count($terms_conditions);$r++){
                $terms_condition_array[] = array(
                    'name' => $terms_conditions[$r]['field_name'],
                    'description' => $terms_conditions[$r]['facility_field_description']
                );
            }

            if(!empty($type_of_loan)){ $loan_type = $type_of_loan[0]['child_name']; }

            $result_array['facilities'][] = array(
                'facility_name' => $facilities[$s]['project_facility_name'],
                'currency' => $facilities[$s]['currency_code'],
                'amount' => $facilities[$s]['facility_amount'],
                'usd_amount' => $facilities[$s]['facility_usd_amount'],
                'type_of_loan' => $loan_type,
                'facility_purpose' => $facilities[$s]['facility_purpose'],
                'facility_minimum_amount' => $facilities[$s]['facility_amount'],
                'other_facility_modalities' => $facilities[$s]['other_facility_modalities'],
                'security' => $facilities[$s]['facility_security'],
                'maturity_loan_repayment' => $facilities[$s]['facility_tenor'],
                'interest_payment' => $interest_rate,
                'pricing' => $pricing_array,
                'terms_conditions' => $terms_condition_array
            );
        }
        $project_stage_details[0]['fields'] = $result_array;
        $project_stage_details = $project_stage_details[0];

        $mpdf = new mPDF('c');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetAutoFont('Calibri');

        $template_collection_data=$result_array;

        foreach ($template_collection_data as $key => $value)
        {
            if(!is_array($value))
                $template_collection_data[$key] = preg_replace('/<table (.*?)>/', '<table border="1" style="border-collapse: collapse;border-color: #000000;" >',$value);
        }

        $reference_number=$template_collection_data['project_contract_number'];
        $project_name=$template_collection_data['project_title'];
        $loan_amount=number_format($template_collection_data['loan_amount']);
        $country=$template_collection_data['country_office'];
        $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
        $company_name=$template_collection_data['company_name'];
        $company_address='';
        $flag=0;
        if($company_name!='')
        {
            $company_address.=$company_name.',<br/>';
        }
        if($template_collection_data['company_address']['company_street_name']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_street_name'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_block_number']!='')
        {
            if($template_collection_data['company_address']['company_street_name']!='')
            {
                $company_address.=',';
            }
            $company_address.=$template_collection_data['company_address']['company_block_number'];
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.='<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_city']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_city'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_region']!='')
        {
            if($template_collection_data['company_address']['company_city']!='')
            {
                $company_address.=',';
            }
            $company_address.=$template_collection_data['company_address']['company_region'];
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.=',<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_country']!='')
        {
            $company_address.=$template_collection_data['company_address']['company_country'];
        }
        $country_head='[Name of Head of Country]';
        $legal_officer_name='[Enter Name of Legal Officer]';

        $share_holder_data=json_decode($share_holder_info,true);
        for($i=0;$i<count($share_holder_data);$i++)
        {
            if($share_holder_data[$i]['ownership_percentage']!='')
                $share_holder_data[$i]['ownership_percentage']=$share_holder_data[$i]['ownership_percentage'].'%';
        }
        //Need to build the phpdocx table
        $load_data=array(array('Name','Number Of Shares','Ownership Percentage','Value Of Shares','Currency'));
        for($t=0;$t<count($share_holder_data);$t++)
        {
            $share_holder_array = (array)$share_holder_data[$t];
            if(isset($share_holder_array['$$hashKey']))
                unset($share_holder_array['$$hashKey']);
            array_push($load_data,array_values($share_holder_array));
        }

        $share_holder_table=arrayToTable($load_data,'termsheet');
        //End of table build
        $project_location=$template_collection_data['project_location'];
        $project_other_information=$template_collection_data['project_basic_information_other'];
        /*$facility_list_data='';
        $list_item=1;$list_subitem=1;

        $pre_conditions_for_disbursement=$template_collection_data['pre_conditions_for_disbursement'];
        $procurement_of_goods=$template_collection_data['procurement_of_goods'];
        $insurance=$template_collection_data['insurance'];
        $other_conditions=$template_collection_data['other_conditions'];
        $security=$template_collection_data['security'];
        $representations_and_warranties=$template_collection_data['representations_and_warranties'];
        $information_undertakings=$template_collection_data['information_undertakings'];
        $financial_covenants=$template_collection_data['financial_covenants'];
        $general_undertakings=$template_collection_data['general_undertakings'];
        $events_of_default=$template_collection_data['events_of_default'];
        $special_audit=$template_collection_data['special_audit'];
        $legal_documentation=$template_collection_data['legal_documentation'];
        $costs_and_expenses=$template_collection_data['costs_and_expenses'];
        $applicable_law=$template_collection_data['applicable_law'];
        $dispute_resolution=$template_collection_data['dispute_resolution'];
        $other_terms=$template_collection_data['other'];*/
        /*print_r($template_collection_data); die('end');*/

        $template_data='';

        $template_data .= '<style>
                            html,body{margin: 0;padding: 0;float: left;}
                            table {border:1px solid #000}
                            td,th {border:none}
                            </style>';
        /*$template_data .= '<table width="100%" style="font-size:16px;text-align:left;width: 100%;border-collapse: collapse;overflow: visible;border:0px;width:100%;margin-top:-200px;"><tbody><tr><td>Access Bank (Rwanda) Ltd<br /><span style="font-size: x-large;">'.$this->lang->line('facility_term_sheet').'</span></td>';
        $template_data .= '<td class="pull-right text-right"><img style="float: right;padding-top: 0px;" src="'.WEB_BASE_URL.'/html/images/access_logo.PNG" alt="logo"/></td></tr></tbody></table>';*/

        /*$template_data .= '<div style="margin-top: -100px;"><table width="100%" style="width: 100%;border-collapse: collapse;border: 0px;padding-top: 10px;font-size:10px;overflow: visible;margin-top: -100px;" >
			<tbody>
                <tr>
					<td nowrap="nowrap" style="width:30%; text-align: left; vertical-align: text-top;">Access Bank (Rwanda) Ltd<br /><span style="font-size: x-large;">'.$this->lang->line('facility_term_sheet').'</span></td>
					<td nowrap="nowrap" style="width:70%;float:right;text-align:right;"><img style="float: right;padding-top: 0px;" src="'.WEB_BASE_URL.'/html/images/access_logo.PNG" alt="logo"/></td>
				</tr>
                <tbody></table></div>';*/

        $template_data .= '<table width="100%" style="width: 100%;border-collapse: collapse;border-color: #000000;padding-top: 10px;font-size:10px;overflow: visible;" >
			<tbody>
                <tr>
					<td width="100%" style="width:100%; text-align: left; vertical-align: text-top;overflow: visible;">'.$this->lang->line('shareholders').': <div style="width:100%;">'.$share_holder_table.'</div></td>
				</tr>
                <tbody></table>';



        $template_data .= '<table border="1" width="100%" style="width: 100%;border-collapse: collapse;border-color: #000000;padding-top: 10px;font-size:10px;overflow: visible;" >
    <tbody>
      <tr>
        <td style="width: 100%; font-weight: bold;text-transform: capitalize;">'.$this->lang->line("background_to_the_customer").'</td>
      </tr>
      <tr >
        <td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$background_of_customer.'</td>
      </tr>
      <tr>
        <td style="width: 100%; font-weight: bold;;">'.$this->lang->line("approval_request").'</td>
      </tr>
      <tr>
        <td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['approval_request'].'</td>
      </tr>
      <tr>
        <td style="width: 100%; font-weight: bold;;">'.$this->lang->line("critical_credit_issues").'</td>
      </tr>
      <tr style="height: 100px;">
        <td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['critical_credit_issues'].'</td>
      </tr>
      <tr>
        <td style="width: 100%; font-weight: bold;">'.$this->lang->line("risk_return_and_relationship_strategy").'</td>
      </tr>
      <tr >
        <td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['risk_and_relationship_strategy'].'</td>
      </tr>
      <tr>
        <td style="width: 100%; font-weight: bold;">'.$this->lang->line("conditions_precedent_to_drawdown").'</td>
      </tr>
      <tr >
        <td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['risk_and_relationship_strategy'].'</td>
      </tr>
      </tbody>
    </table>';
        /*print_r($template_collection_data); die('end');*/

        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">'.$template_collection_data['conditions_precedent_to_drawdown'].'</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        //$project_conditions_checklist
        $conditions_checklist_data='<ul>';
        if(isset($template_collection_data['project_conditions_checklist']) &&
            count($template_collection_data['project_conditions_checklist']) > 0)
        {
            for($cd=0;$cd<count($template_collection_data['project_conditions_checklist']);$cd++)
            {
                $conditions_checklist_data.='<li>'.$template_collection_data['project_conditions_checklist'][$cd]['assessment_question'].'</li>';
            }
        }
        $conditions_checklist_data.='</ul>';

        $template_data.='<td>'.$conditions_checklist_data.'</td>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">'.$this->lang->line('other_conditions').'</th>';
        $template_data.='</tr>';
        $others_checklist_data='<ul>';
        if(isset($template_collection_data['project_others_checklist']) &&
            count($template_collection_data['project_others_checklist']) > 0)
        {
            for($cd=0;$cd<count($template_collection_data['project_others_checklist']);$cd++)
            {
                $others_checklist_data.='<li>'.$template_collection_data['project_others_checklist'][$cd]['assessment_question'].'</li>';
            }
        }
        $others_checklist_data.='</ul>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$others_checklist_data.'</td>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">'.$this->lang->line['transaction_dynamics'].'</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['transaction_dynamics'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';

        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">'.$this->lang->line('ways_out_analysis').'</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['ways_out_analysis'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">'.$this->lang->line('update_on_banking_relationships_and_existing_facilities').'</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['banking_relationships'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.= $this->lang->line('risk_analysis').':';
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['risk_analysis'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';

        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;text-transform: uppercase;">';
        $template_data.= $this->lang->line('country_overview_industry_business_risk').':';
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr >';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['country_overview_industry_risk'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.=$this->lang->line('ownership_and_management_assessment').':';
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr style="height: 10px;padding-top:20px;padding-bottom:20px;">';
        $ownership_raw_data=json_decode($template_collection_data['share_holder_info'],true);
        $ownership_data = '<ul>';
        for($owd=0;$owd<count($ownership_raw_data);$owd++)
        {
            $ownership_data.='<li>'.$ownership_raw_data[$owd]['shareholder_name'].' with '.$ownership_raw_data[$owd]['ownership_percentage'].'% of shares ('.$ownership_raw_data[$owd]['no_of_shares'].' shares)</li>';
        }
        $ownership_data .= '</ul>';

        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$ownership_data.'</td>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td>';
        if(count($template_collection_data['client_details'])>0)
        {
            for($cd=0;$cd<count($template_collection_data['client_details']);$cd++)
            {
                if(count($template_collection_data['client_details'][$cd]['Additional_data']))
                {
                    for($cad=0;$cad<count($template_collection_data['client_details'][$cd]['Additional_data']);$cad++)
                    {
                        if($template_collection_data['client_details'][$cd]['Additional_data'][$cad]['field_name'] == 'client_education' || $template_collection_data['client_details'][$cd]['Additional_data'][$cad]['field_name'] == 'client_eployeement')
                        {
                            $template_data.=$template_collection_data['client_details'][$cd]['Additional_data'][$cad]['form_field_value'];
                        }
                    }
                }
            }
        }
        $template_data.='</td>';
        $template_data.='</tr>';
        $template_data.='</table>';

        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.= $this->lang->line('historical_financial_analysis_and_outlook');
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['company_address']['company_financial_statement_comments'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';

        $template_data.='<h4 style="text-decoration: underline;margin-bottom: 0;padding-bottom: 0;">'.$this->lang->line('reference_information').'</h4>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10px;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.= $this->lang->line('business_and_industry_description');
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['company_address']['company_business_industry_description'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';

        $template_data.='<table width="100%" style="text-align: left;font-size: 10;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.= $this->lang->line('corporate_structure_and_organization');
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['company_address']['company_corporate_structure_and_organization'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;">';
        $template_data.= $this->lang->line('justification_for_the_request_credit_consideration');
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['company_address']['company_justification_for_request_consideration'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        $template_data.='<table width="100%" style="text-align: left;font-size: 10;">';
        $template_data.='<tr>';
        $template_data.='<th style="text-align: left;border-bottom: 1px solid #000000;">';
        $template_data.= $this->lang->line('ways_out_analysis');
        $template_data.='</th>';
        $template_data.='</tr>';
        $template_data.='<tr>';
        $template_data.='<td style="height: 10px;padding-top:20px;padding-bottom:20px;">'.$template_collection_data['company_address']['company_external_market_data'].'</td>';
        $template_data.='</tr>';
        $template_data.='</table>';
        /*print_r($template_data); die('end');*/

        //$docx->embedHTML($facilitytermsheet_template);
        $filename=str_replace(' ','_',$project_name).'_facility_term_sheet_'.date("d-m-Y");
        $documentfile='template_docx/'.$filename.".pdf";
        $main = "<!doctype html>
                         <html>
                         <head>
                         <meta charset=\"utf-8\">
                         </head>
                         <body>";
        $main.=$template_data;
        $main.= "</body></html>";
        $header = '
<table border="0" width="100%" style="vertical-align: center; font-family: serif; font-size: 9pt; "><tr>
<tr>
	<td width="50%">Access Bank (Rwanda) Ltd</td>
    <td width="50%" rowspan="2" align="right" >
        <img src="'.WEB_BASE_URL.'/html/images/access_logo.PNG" width="160px" />
    </td>
  </tr>
  <tr>
    <td width="50%">'.$this->lang->line('facility_term_sheet').'</td>
  </tr>
</table>
';
        $headerE = '
<table width="100%" border="0" style="vertical-align: center; font-family: serif; font-size: 9pt;"><tr>
<tr>
	<td width="50%">Access Bank (Rwanda) Ltd</td>
    <td width="50%" rowspan="2" align="right">
        <img src="'.WEB_BASE_URL.'/html/images/access_logo.PNG" width="160px" />
    </td>
  </tr>
  <tr>
    <td width="50%">'.$this->lang->line('facility_term_sheet').'</td>
  </tr>
</table>
';
        $template_data .= '<td class="pull-right text-right"><img style="float: right;padding-top: 0px;" src="'.WEB_BASE_URL.'/html/images/access_logo.PNG" alt="logo"/></td></tr></tbody></table>';
        /*$mpdf->SetHeader('Facility Term Sheet');*/

        $mpdf->displayDefaultOrientation = true;

        $mpdf->forcePortraitHeaders = true;
        $mpdf->forcePortraitMargins = true;

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLHeader($headerE,'E');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->WriteHTML($main);
        $mpdf->Output($documentfile,'F');

        $returnfilename=REST_API_URL.$documentfile;
        //$docx->createDocx($documentfile);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('fileUrl'=>$returnfilename, 'fileName'=>$filename));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stageViewDoc_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_stage_info', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_stage_details = $this->Project_model->getProjectStageDetails(array('project_stage_info_id' => $data['id_project_stage_info']));
        $data['project_id'] = $project_stage_details[0]['project_id'];
        $data['stage_id'] = $project_stage_details[0]['stage_id'];
        $team = $this->Workflow_model->getProjectStageWorkflowDetails(array('project_stage_info_id' => $data['id_project_stage_info']));

        $team_array = array();
        for($s=0;$s<count($team);$s++){
            if($team[$s]['to_phase_name']!='')
                $team_array[] = array(
                    'phase_name' => $team[$s]['to_phase_name'],
                    'user_name' => $team[$s]['to_user_name']
                );
        }
        if(isset($team[count($team)-1]['to_phase_name']) && $team[count($team)-1]['to_phase_name']!='')
            $team_array[] = array(
                'phase_name' => $team[count($team)-1]['to_phase_name'],
                'user_name' => $team[count($team)-1]['to_user_name']
            );

        $team = array();
        foreach($team_array as $i){
            $team[$i['phase_name']] = $i;
        }
        $team_array = array_values($team);
        $result_array = array();
        if(!empty($project_stage_details))
        {
            if($project_stage_details[0]['stage_key']=='concept_note'){

                $required_keys = array('project_title','borrower','application_date','country_office','main_sector','sub_sector','required_loan_amount_currency','project_description','project_concept_purpose','status_of_project_implementation','project_cost_financing_plan','summary_of_available_information','social_and_environmental_concerns','project_contract_number','project_country', 'project_background_other');
                $result_array = array('project_title' => '','borrower' => '','application_date' => '','country_office' => '','main_sector' => '','sub_sector' => '','required_loan_amount_currency' => '','project_description' => '','project_concept_purpose' => '','status_of_project_implementation' => '','project_cost_financing_plan' => '','summary_of_available_information' => '','social_and_environmental_concerns' => '','project_contract_number' => '', 'project_country' => '', 'project_background_other' => '');
                $project_details = $this->Crm_model->getProject($data['project_id']);
                $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
                $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
                //echo "<pre>"; print_r($borrower); exit;
                $result_array['application_date'] = $project_details[0]['created_date_time'];
                $result_array['main_sector'] = $project_details[0]['sector'];
                $result_array['sub_sector'] = $project_details[0]['sub_sector'];
                $result_array['country_office'] = $created_by[0]['branch_name'];
                $result_array['borrower'] = $borrower[0]['company_name'];
                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'project_stage_info_id' => $project_stage_details[0]['id_project_stage_info'],'keys' => $required_keys));
                for($r=0;$r<count($required_keys);$r++)
                {
                    for($s=0;$s<count($key_details);$s++){
                        $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                    }
                }

                $facility_details = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));

                for($s=0;$s<count($facility_details);$s++)
                {
                    $result_array['required_loan_amount_currency'][] = array(
                        'project_facility_name' => $facility_details[$s]['project_facility_name'],
                        'facility_amount' => $facility_details[$s]['facility_amount'],
                        'currency' => $facility_details[$s]['currency_code']
                    );
                }

            }
            else if($project_stage_details[0]['stage_key']=='pre_appraisal'){

                $required_keys = array('project_title','application_date','project_contract_number','project_country','statement_of_the_application','project_description','rationale_of_the_client_to_apply','project_concept_purpose','expected_economic_benefits_and_additionalities','shareholder_structure','sponsors_and_promoters','products_operations','markets','environmental_and_social_considerations','financial_aspects','risk_grading','risk_factor','comments_on_risk_assessment','strength_and_weaknesses_analysis','credit_worthiness','short_character_assessment','recommendations','other_recommendations');
                $result_array = array('project_title' => '','application_date' => '', 'project_contract_number' => '','project_country' => '','statement_of_the_application' => '','project_description' => '','rationale_of_the_client_to_apply' =>'','project_concept_purpose' => '','expected_economic_benefits_and_additionalities' =>'','shareholder_structure' => '','sponsors_and_promoters' =>'','products_operations' => '','markets' => '','environmental_and_social_considerations' => '','financial_aspects' => '','risk_grading' => '','risk_factor' => '','comments_on_risk_assessment' => '','strength_and_weaknesses_analysis' => '','credit_worthiness' => '','short_character_assessment' => '','recommendations' => '','other_recommendations' => '');

                $project_details = $this->Crm_model->getProject($data['project_id']);
                $result_array['application_date'] = $project_details[0]['created_date_time'];
                $result_array['project_title'] = $project_details[0]['project_title'];

                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'project_stage_info_id' => $project_stage_details[0]['id_project_stage_info'],'keys' => $required_keys));
                for($r=0;$r<count($required_keys);$r++)
                {
                    for($s=0;$s<count($key_details);$s++){
                        $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                    }
                }

                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => ['project_contract_number','project_country']));
                for($r=0;$r<count($required_keys);$r++)
                {
                    for($s=0;$s<count($key_details);$s++){
                        $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                    }
                }

                if(!empty($result_array['risk_factor']))
                {
                    $result_array['risk_factor'] = json_decode($result_array['risk_factor']);
                }


                $result_array['project_background'] = $result_array['project_description'];
                $swot_details = $this->Project_model->getProjectCompanyAssessment(array('crm_project_id' => $data['project_id'],'assessment' => 'swot'));
                $swot_analysis = array();
                for($s=0;$s<count($swot_details);$s++){
                    $swot_analysis[$swot_details[$s]['id_assessment_item']]['name'] = $swot_details[$s]['assessment_item_name'];
                    $swot_analysis[$swot_details[$s]['id_assessment_item']]['color'] = $swot_details[$s]['assessment_color'];
                    $swot_analysis[$swot_details[$s]['id_assessment_item']]['answers']= array();
                    if(isset($swot_details[$s]['id_company_assessment_item_step']) && $swot_details[$s]['id_company_assessment_item_step']!='') {
                        $swot_analysis[$swot_details[$s]['id_assessment_item']]['answers'][] = array(
                            'answer' => $swot_details[$s]['step_title']
                        );
                    }
                }
                $result_array['strength_and_weaknesses_analysis'] = array_values($swot_analysis);
            }
            else if($project_stage_details[0]['stage_key']=='detailed_appraisal'){
                $required_keys = array('project_title','application_date','project_contract_number','project_country','business_and_economic_environment','governance _management','past_performance','ratio_analysis','risk_grading_analysis','financial_projections','security_cover_analysis','other_observations','project_description','project_concept_purpose','project_components_cost_estimates','financing_plan_role_of_eadb','implementation_plan','project_operational_management','procurement_disbursement','market_analysis','project_risks_uncertainties','project_background_other','business_viability','economic_aspects','environmental_social_aspects','eadb_portfolio_analysis','eadb_benefits','eadb_additionalities','eadb_justification_other','conclusions','recommendations');
                $result_array = array('borrower' => '','project_title' => '','project_description' => '','application_date' => '', 'project_contract_number' => '','project_country' => '','business_and_economic_environment' => '','project_location' => '','project_team'=>'','sector'=>'','sub_sector'=>'','focus_area'=>'','facilities' => '','company_nature_of_business' => '','short_character_assessment' => '','company_description' => '','company_business_type' => '','company_ownership_details' => '','company_street_name' => '','company_block_number' => '','company_city' => '','company_region' =>'','company_country' => '','governance_management' => '','past_performance' => '','ratio_analysis'  => '','risk_grading_analysis' => '','financial_projections' => '','security_cover_analysis' => '','other_observations' => '','project_description' => '','project_concept_purpose' => '','project_components_cost_estimates' => '','financing_plan_role_of_eadb' => '','implementation_plan' => '','project_operational_management' => '','procurement_disbursement' => '','market_analysis' => '','project_risks_uncertainties' => '','project_background_other' => '','business_viability' => '','economic_aspects' => '','environmental_social_aspects' => '','eadb_portfolio_analysis' => '','eadb_benefits' => '','eadb_additionalities' => '','eadb_justification_other' => '','conclusions' => '','recommendations' => '');

                $project_details = $this->Crm_model->getProject($data['project_id']);
                //echo "<pre>"; print_r($project_details); exit;
                $result_array['application_date'] = $project_details[0]['created_date_time'];
                $result_array['project_title'] = $project_details[0]['project_title'];
                $result_array['sector'] = $project_details[0]['sector'];
                $result_array['sub_sector'] = $project_details[0]['sub_sector'];

                $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
                $company_keys = array('company_description','company_nature_of_business','company_business_type','company_ownership_details','company_street_name','company_block_number','company_city','company_region','company_country');
                if(!empty($borrower)){
                    $result_array['borrower'] = $borrower[0]['company_name'];
                    $key_data = $this->Project_model->getCrmCompanyDataByKeys(array('crm_company_id' => $borrower[0]['id_crm_company'],'keys' => $company_keys));
                    if(!empty($key_data)){
                        for($sr=0;$sr<count($key_data);$sr++) {
                            $result_array[$key_data[$sr]['field_name']] = $key_data[$sr]['form_field_value'];
                        }
                    }
                }

                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'project_stage_info_id' => $project_stage_details[0]['id_project_stage_info'],'keys' => $required_keys));
                for($s=0;$s<count($key_details);$s++){
                    $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                }

                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => ['project_contract_number','project_country','project_location','focus_area','project_description'.'short_character_assessment']));
                for($s=0;$s<count($key_details);$s++){
                    $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                }

                $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));
                $appraisal_fee_data='';
                for($s=0;$s<count($facilities);$s++)
                {
                    $pricing_array = '';
                    $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'pricing'));
                    for($r=0;$r<count($pricing);$r++){
                        $pricing_array[$r] = array(
                            'name' => $pricing[$r]['field_name'],
                            'payment_type' => $pricing[$r]['payment_type'],
                            'label' => '',
                            'facility_field_value' => $pricing[$r]['facility_field_value'],
                            'facility_field_description' => $pricing[$r]['facility_field_description']
                        );
                        if($pricing[$r]['payment_type']=='interest_as'){
                            $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                            $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                        }
                        else if($pricing[$r]['payment_type']=='fee_as'){
                            $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                            $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                        }
                        else if($pricing[$r]['payment_type']=='fee_amount'){
                            $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                            $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                        }
                    }
                    $pricing_array = array_values($pricing_array);

                    $result_array['facilities'][] = array(
                        'facility_name' => $facilities[$s]['project_facility_name'],
                        'currency' => $facilities[$s]['currency_code'],
                        'amount' => $facilities[$s]['facility_amount'],
                        'facility_maturity_date' => $facilities[$s]['facility_maturity_date'],
                        'pricing' => $pricing_array,
                    );
                }
            }
        }
        if(isset($project_stage_details[0]))
        {
            $project_stage_details[0]['fields'] = $result_array;
            $project_stage_details = $project_stage_details[0];
            require_once './phpdocx/classes/CreateDocx.inc';
            $docx = new CreateDocx();
            $docx->setDefaultFont('Calibri');
            $template_collection_data=$result_array;

            foreach ($template_collection_data as $key => $value)
            {
                if(!is_array($value))
                    $template_collection_data[$key] = preg_replace('/<table (.*?)>/', '<table border="1" style="border-collapse: collapse;border-color: #000000;" >',$value);
            }

            $project_involved_team = '';
            if($team_array != '' && count($team_array)>0)
            {
                for($t=0;$t<count($team_array);$t++)
                {
                    if($t == 0)
                        $project_involved_team.='<tr style="padding: 20;">';
                    else
                        $project_involved_team.='<tr>';
                    $project_involved_team.='<td>'.$team_array[$t]['phase_name'].':</td>';
                    $project_involved_team.='<td>'.$team_array[$t]['user_name'].'</td>';
                    $project_involved_team.='</tr>';
                }
            }
            else
            {
                $project_involved_team = '<tr style="padding: 20;">
                    <td>Task Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Reviewing Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Environmental Specialist:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Legal Counsel:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Appraisal Manager:</td>
                    <td>....................</td>
                    </tr>';
            }

            if($project_stage_details['stage_key']=='concept_note')
            {
                $project_ref_number=$template_collection_data['project_contract_number'];
                $project_name=$template_collection_data['project_title'];
                $project_country=$template_collection_data['project_country'];
                $country=$template_collection_data['country_office'];
                $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
                $borrower=$template_collection_data['borrower'];
                $main_sector=$template_collection_data['main_sector'];
                $sub_sector=$template_collection_data['sub_sector'];
                $requested_loan_data=$template_collection_data['required_loan_amount_currency'];
                //Need to build the phpdocx table

                $load_data=array(array('Name','Amount','Currency'));
                $requested_loan_table='';

                if($requested_loan_data!='' && count($requested_loan_data)>0)
                {
                    for($t=0;$t<count($requested_loan_data);$t++)
                    {
                        $requested_loan_array = (array)$requested_loan_data[$t];
                        $requested_loan_array['facility_amount'] = number_format($requested_loan_array['facility_amount']);
                        if(isset($requested_loan_array['$$hashKey']))
                            unset($requested_loan_array['$$hashKey']);
                        array_push($load_data,array_values($requested_loan_array));
                    }
                    $requested_loan_table=$docx->buildArrayToTable($load_data);
                }
                //End of table build
                $project_description=$template_collection_data['project_description'];
                $project_concept_purpose=$template_collection_data['project_concept_purpose'];
                $status_of_project_implementation=$template_collection_data['status_of_project_implementation'];
                $project_cost_estimates=$template_collection_data['project_cost_financing_plan'];
                $summary_of_available_information=$template_collection_data['summary_of_avalibale_information'];
                $social_and_environmental_concerns=$template_collection_data['social_and_environmental_concerns'];
                $project_comments=$template_collection_data['project_background_other'];

                $options = array(
                    'display' => 'firstPage',
                    'borderWidth' => 12,
                    'borderColor' => '000000'
                );
                $docx->addPageBorders($options);
                $textOptions = array(
                    'textAlign' => 'right',
                    'fontSize' => 12,
                    'color' => '000000',
                );
                $headerText = new WordFragment($docx, 'defaultHeader');
                $headerText->embedHTML('<p style="text-align:right; font-size:9;">Concept Note '.$project_name.'<br>'.$application_date.', '.$project_country.'</p>');
                $first = new WordFragment($docx, 'firstHeader');
                $first->addText('');
                $docx->addHeader(array('default' => $headerText, 'first' => $first));

                $pageNumberOptions = array(
                    'textAlign' => 'center',
                    'fontSize' => 10
                );
                $default = new WordFragment($docx, 'defaultFooter');
                $default->addPageNumber('numerical', $pageNumberOptions);
                $first = new WordFragment($docx, 'firstFooter');
                $first->addPageNumber('numerical', $pageNumberOptions);

                $docx->addFooter(array('default' => $default, 'first' => $first));

                $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));
                $template_1 = $this->load->view('templates/conceptnote_template1.html','',true);
                $template_2 = $this->load->view('templates/conceptnote_template2.html','',true);
                $template_1=sprintf($template_1,$project_ref_number,$project_name,$project_country,$project_involved_team,$application_date);
                $template_2=sprintf($template_2,$project_name,$borrower,$application_date,$country,$main_sector,$sub_sector,$requested_loan_table,$project_description,$project_concept_purpose,$status_of_project_implementation,$project_cost_estimates,$summary_of_available_information,$social_and_environmental_concerns,$project_comments);

                $docx->embedHTML($template_1);
                //$docx->addBreak(array('type' => 'page'));
                $docx->embedHTML($template_2);
                $filename=str_replace(' ','_',$project_name).'_concept_note_'.date("d-m-Y");
                $documentfile='template_docx/'.$filename;
                $returnfilename=REST_API_URL.$documentfile.'.docx';
            }
            elseif($project_stage_details['stage_key']=='pre_appraisal')
            {
                $project_ref_number=$template_collection_data['project_contract_number'];
                $project_name=$template_collection_data['project_title'];
                $project_country=$template_collection_data['project_country'];
                //$country=$template_collection_data['country_office'];
                $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
                $statement_of_the_application=$template_collection_data['statement_of_the_application'];
                $project_background_information=$template_collection_data['project_background'];
                $rationale_of_the_client_to_apply=$template_collection_data['rationale_of_the_client_to_apply'];
                $project_concept_purpose=$template_collection_data['project_concept_purpose'];
                $expected_economic_benefits=$template_collection_data['expected_economic_benefits_and_additionalities'];
                $shareholder_structure=$template_collection_data['shareholder_structure'];
                $sponsors_and_promoters=$template_collection_data['sponsors_and_promoters'];
                $products_operations=$template_collection_data['products_operations'];
                $markets=$template_collection_data['markets'];
                $environmental_and_social_considerations=$template_collection_data['environmental_and_social_considerations'];
                $financial_aspects=$template_collection_data['financial_aspects'];
                $risk_grading=$template_collection_data['risk_grading'];
                $risk_factor_data=(array) $template_collection_data['risk_factor'];
                //Need to build the phpdocx table
                $load_data=array(array('Name','Type','Description','Mitigation','Grade'));
                $risk_factor_table='';
                if($risk_factor_data!='' && count($risk_factor_data)>0)
                {
                    for($t=0;$t<count($risk_factor_data);$t++)
                    {
                        $risk_factor_array = (array)$risk_factor_data[$t];
                        if(isset($risk_factor_array['$$hashKey']))
                            unset($risk_factor_array['$$hashKey']);
                        if(isset($risk_factor_array['parent']))
                        {
                            $risk_factor_array['type'] = $risk_factor_array['parent']->risk_name;
                            unset($risk_factor_array['parent']);
                        }
                        if(isset($risk_factor_array['child']))
                        {
                            $risk_factor_array['type'] .= ' / '.$risk_factor_array['child']->risk_name;
                            unset($risk_factor_array['child']);
                        }
                        if(isset($risk_factor_array['subchild']))
                        {
                            $risk_factor_array['type'] .= ' / '.$risk_factor_array['subchild']->risk_name;
                            unset($risk_factor_array['subchild']);
                        }
                        array_push($load_data,array_values($risk_factor_array));
                    }
                    $risk_factor_table=$docx->buildArrayToTable($load_data);
                }
                //End of table build
                $comments_on_risk_assessment=$template_collection_data['comments_on_risk_assessment'];

                $strength_and_weaknesses_analysis_data=$template_collection_data['strength_and_weaknesses_analysis'];
                $strength_and_weaknesses_list='';
                if($strength_and_weaknesses_analysis_data!='' && count($strength_and_weaknesses_analysis_data)>0)
                {
                    for($st=0;$st<count($strength_and_weaknesses_analysis_data);$st++)
                    {
                        $strength_and_weaknesses_list.='<h3 style="font-size:10;">'.$strength_and_weaknesses_analysis_data[$st]['name'].'</h3>';
                        for($aw=0;$aw<count($strength_and_weaknesses_analysis_data[$st]['answers']);$aw++)
                        {
                            $strength_and_weaknesses_list.='<ul>';
                            $strength_and_weaknesses_list.='<li>'.$strength_and_weaknesses_analysis_data[$st]['answers'][$aw]['answer'].'</li>';
                            $strength_and_weaknesses_list.='</ul>';
                        }
                    }
                }
                $credit_worthiness=$template_collection_data['credit_worthiness'];
                $short_character_assessment=$template_collection_data['short_character_assessment'];
                $recommendations=$template_collection_data['recommendations'];
                $other_recommendations=$template_collection_data['other_recommendations'];

                $options = array(
                    'display' => 'firstPage',
                    'borderWidth' => 12,
                    'borderColor' => '000000'
                );
                $docx->addPageBorders($options);
                $textOptions = array(
                    'textAlign' => 'right',
                    'fontSize' => 12,
                    'color' => '000000',
                );
                $headerText = new WordFragment($docx, 'defaultHeader');
                $headerText->embedHTML('<p style="text-align:right; font-size:9;">Pre-Appraisal Report '.$project_name.'<br>'.$application_date.', '.$project_country.'</p>');
                $first = new WordFragment($docx, 'firstHeader');
                $first->addText('');
                $docx->addHeader(array('default' => $headerText, 'first' => $first));

                $pageNumberOptions = array(
                    'textAlign' => 'center',
                    'fontSize' => 10
                );
                $default = new WordFragment($docx, 'defaultFooter');
                $default->addPageNumber('numerical', $pageNumberOptions);
                $first = new WordFragment($docx, 'firstFooter');
                $first->addPageNumber('numerical', $pageNumberOptions);

                $docx->addFooter(array('default' => $default, 'first' => $first));

                $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));
                $template_1 = $this->load->view('templates/preappraisal_template1.html','',true);
                $template_2 = $this->load->view('templates/preappraisal_template2.html','',true);
                $template_1=sprintf($template_1,$project_ref_number,$project_name,$project_country,$project_involved_team,$application_date);
                $template_2=sprintf($template_2,$statement_of_the_application,$project_background_information,$rationale_of_the_client_to_apply,$project_concept_purpose,$expected_economic_benefits,$shareholder_structure,$sponsors_and_promoters,$products_operations,$markets,$environmental_and_social_considerations,$financial_aspects,$risk_grading,$risk_factor_table,$comments_on_risk_assessment,$strength_and_weaknesses_list,$credit_worthiness,$short_character_assessment,$recommendations,$other_recommendations);
                $docx->embedHTML($template_1);
                $docx->addBreak(array('type' => 'page'));
                $docx->embedHTML($template_2);
                $filename=str_replace(' ','_',$project_name).'_preappraisal_'.date("d-m-Y");
                $documentfile='template_docx/'.$filename;
                $returnfilename=REST_API_URL.$documentfile.'.docx';
            }
            else if($project_stage_details['stage_key']=='detailed_appraisal')
            {
                $project_ref_number = $template_collection_data['project_contract_number'];
                $project_name=$template_collection_data['project_title'];
                $project_country=$template_collection_data['project_country'];
                $project_team = $template_collection_data['project_team'];
                $country=$template_collection_data['country_office'];
                $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
                $project_description = $template_collection_data['project_description'];
                $main_sector=$template_collection_data['sector'];
                $sub_sector=$template_collection_data['sub_sector'];
                $focus_area=$template_collection_data['focus_area'];
                $business_economic_environment = $template_collection_data['business_and_economic_environment'];
                $company_name = $template_collection_data['borrower'];

                $company_address='';
                $flag=0;
                if($company_name!='')
                {
                    $company_address.=$company_name.',<br/>';
                }
                if($template_collection_data['company_street_name']!='')
                {
                    $company_address.=$template_collection_data['company_street_name'];
                    $flag=1;
                }
                if($template_collection_data['company_block_number']!='')
                {
                    if($template_collection_data['company_street_name']!='')
                    {
                        $company_address.=',';
                    }
                    $company_address.=$template_collection_data['company_block_number'];
                    $flag=1;
                }
                if($flag == 1)
                {
                    $company_address.='<br/>';
                    $flag=0;
                }
                if($template_collection_data['company_city']!='')
                {
                    $company_address.=$template_collection_data['company_city'];
                    $flag=1;
                }
                if($template_collection_data['company_region']!='')
                {
                    if($template_collection_data['company_city']!='')
                    {
                        $company_address.=',';
                    }
                    $company_address.=$template_collection_data['company_region'];
                    $flag=1;
                }
                if($flag == 1)
                {
                    $company_address.=',<br/>';
                    $flag=0;
                }
                if($template_collection_data['company_country']!='')
                {
                    $company_address.=$template_collection_data['company_country'];
                }

                $company_description = $template_collection_data['company_description'];
                $company_business_type = $template_collection_data['company_business_type'];
                $company_nature_of_business_data = '';
                if($template_collection_data['company_nature_of_business']!='')
                {
                    $company_nature_of_business_raw = json_decode($template_collection_data['company_nature_of_business'],true);
                    $company_nature_of_business_raw_data = array_keys( $company_nature_of_business_raw, true );
                    $company_nature_of_business_data = implode(', ', $company_nature_of_business_raw_data);
                }

                $company_ownership_details = json_decode($template_collection_data['company_ownership_details'],true);
                //Need to build the phpdocx table
                $load_data=array(array('Name','Ownership Percentage','Number Of Shares','Value Of Shares','Currency'));
                for($t=0;$t<count($company_ownership_details);$t++)
                {
                    $company_ownership_array = (array)$company_ownership_details[$t];
                    if(isset($company_ownership_array['$$hashKey']))
                        unset($company_ownership_array['$$hashKey']);
                    array_push($load_data,array_values($company_ownership_array));
                }
                $company_ownership_table=$docx->buildArrayToTable($load_data,'termsheet');
                //End of table build
                $short_character_assessment = $template_collection_data['short_character_assessment'];
                $governance_management = $template_collection_data['governance_management'];
                $past_performance = $template_collection_data['past_performance'];
                $ratio_analysis = $template_collection_data['ratio_analysis'];
                $risk_grading_analysis = $template_collection_data['risk_grading_analysis'];
                $financial_projections = $template_collection_data['financial_projections'];
                $security_cover_analysis = $template_collection_data['security_cover_analysis'];
                $others_on_client = $template_collection_data['other_observations'];
                $project_concept_purpose = $template_collection_data['project_concept_purpose'];
                $project_components_cost_estimates = $template_collection_data['project_components_cost_estimates'];
                $financing_plan_role_of_eadb = $template_collection_data['financing_plan_role_of_eadb'];
                $implementation_plan = $template_collection_data['implementation_plan'];
                $project_operational_management = $template_collection_data['project_operational_management'];
                $procurement_disbursement = $template_collection_data['procurement_disbursement'];
                $market_analysis = $template_collection_data['market_analysis'];
                $project_risks_uncertainties = $template_collection_data['project_risks_uncertainties'];
                $project_background_other = $template_collection_data['project_background_other'];
                $business_viability = $template_collection_data['business_viability'];
                $economic_aspects = $template_collection_data['economic_aspects'];
                $environmental_social_aspects = $template_collection_data['environmental_social_aspects'];
                $eadb_portfolio_analysis = $template_collection_data['eadb_portfolio_analysis'];
                $eadb_benefits = $template_collection_data['eadb_benefits'];
                $eadb_additionalities = $template_collection_data['eadb_additionalities'];
                $eadb_justification_other = $template_collection_data['eadb_justification_other'];
                $conclusions = $template_collection_data['conclusions'];
                $recommendations = $template_collection_data['recommendations'];

                $facilities = $template_collection_data['facilities'];
                $facility_data = array();
                $pricing_data = array();
                $pricing_list_table='';
                for($fc=0;$fc<count($facilities);$fc++)
                {
                    $facility_data[$fc]['facility_name']=$facilities[$fc]['facility_name'];
                    $facility_data[$fc]['currency']=$facilities[$fc]['currency'];
                    $facility_data[$fc]['amount']=number_format($facilities[$fc]['amount']);
                    $facility_data[$fc]['facility_maturity_date']=$facilities[$fc]['facility_maturity_date'];
                    if($facility_data[$fc]['facility_maturity_date']!='')
                        $facility_data[$fc]['facility_maturity_date'] = date("d-m-Y", strtotime($facility_data[$fc]['facility_maturity_date']));
                    $pricing_data[$fc]['pricing']=$facilities[$fc]['pricing'];

                    $pricing_list_table.='<table border="1" width="100%" style="width: 100%;border-collapse: collapse;border-color: #000000;margin-top: 10px;font-size:10;">';
                    $pricing_list_table.='<tbody>';
                    $pricing_list_table.='<tr style="font-weight:bold;"><td>FACILITY</td><td>'.$facility_data[$fc]['facility_name'].'</td></tr>';
                    for($pdi=0;$pdi<count($pricing_data[$fc]['pricing'][$pdi]);$pdi++)
                    {
                        $pricing_data_items=$pricing_data[$fc]['pricing'][$pdi];
                        if($pricing_data_items['payment_type'] == 'fee_amount')
                            $pricing_list_table.='<tr><td>'.$pricing_data_items['name'].'</td><td>'.number_format($pricing_data_items['facility_field_value']).' '.$pricing_data_items['label'].$pricing_data_items['facility_field_description'].'</td></tr>';
                        else
                            $pricing_list_table.='<tr><td>'.$pricing_data_items['name'].'</td><td>'.$pricing_data_items['facility_field_value'].'% '.$pricing_data_items['label'].$pricing_data_items['facility_field_description'].'</td></tr>';
                    }
                    $pricing_list_table.='</tbody>';
                    $pricing_list_table.='</table>';
                }
                //Need to build the phpdocx table
                $facility_load_data=array(array('Name','Currency','Amount','Maturity Date'));
                for($fd=0;$fd<count($facility_data);$fd++)
                {
                    $facility_array = (array)$facility_data[$fd];
                    if(isset($facility_array['$$hashKey']))
                        unset($facility_array['$$hashKey']);
                    array_push($facility_load_data,array_values($facility_array));
                }
                $facility_data_table=$docx->buildArrayToTable($facility_load_data);

                $main_project_features='<table border="1" width="100%" style="width: 100%;border-collapse: collapse;border-color: #000000;margin-top: 10px;font-size:10;">';
                $main_project_features.='<tbody>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Project Team</td>';
                $main_project_features.='<td>'.$project_team.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Client/Borrower</td>';
                $main_project_features.='<td>'.$company_name.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Legal Status</td>';
                $main_project_features.='<td>'.$company_business_type.$company_ownership_table.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Nature of Business</td>';
                $main_project_features.='<td>';
                $main_project_features.='<ul>';
                $main_project_features.='<li><span style="font-weight:bold">Main Sector: </span>'.$main_sector.'</li>';
                $main_project_features.='<li><span style="font-weight:bold">Sub-Sector: </span>'.$sub_sector.'</li>';
                $main_project_features.='<li><span style="font-weight:bold">Focus Area : </span>'.$focus_area.'</li>';
                $main_project_features.='</ul>';
                $main_project_features.='</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Address Company Office</td>';
                $main_project_features.='<td>'.$company_address.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Project Description</td>';
                $main_project_features.='<td>'.$project_description.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Proposed EADB transaction</td>';
                $main_project_features.='<td>'.$facility_data_table.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='<tr>';
                $main_project_features.='<td>Pricing and Fees</td>';
                $main_project_features.='<td>'.$pricing_list_table.'</td>';
                $main_project_features.='</tr>';
                $main_project_features.='</tbody>';
                $main_project_features.='</table>';

                $template_1 = '<table style="width: 600;font-size:10;"><tr><td style="text-align: left;width:400;">Confidential</td><td style="text-align: left;width:200;">BP. No. '.$project_ref_number.'</td></tr></table>
                <h2 style="color:#000; text-align:center; font-weight:bold;font-size: 14; padding:30px 0;margin-top: 80px">EAST AFRICAN DEVELOPMENT BANK</h2>
                <h3 style="text-align:center; font-weight:bold; padding:20px 0;">'.$project_name.'</h3>
                <p style="text-align:center; padding:20px 0; font-size:16px;">'.$project_country.'</p>
                <h3 style="text-align:center; padding:30px 0;margin-bottom: 300">DETAILED APPRAISAL REPORT</h3>
                <table border="0" style="border:0;width: 600;margin-top: 650;font-size: 10;text-align: left;">
                <tr>
                <td colspan="2" style="font-weight:bold;padding: 20;">PROJECT TEAM:</td>
                </tr>'.$project_involved_team.'<td colspan="2" style="padding-top: 20;">Date: '.$application_date.'</td>
                </tr>
                </table>';

                $template_2 = '<table border="1" width="600" style="width: 100%;border-collapse: collapse;border-color: #000000;margin-top: 10px;font-size:10;" >
                <tbody>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;font-weight: bold;text-align: left;">1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;font-weight: bold;text-align: left;">INTRODUCTION</td>
                <td nowrap="nowrap" style="width:350;height:17px;font-weight:bold;text-align: center;"></td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;text-align: left">1.1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;text-align: left">Main Project Features</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$main_project_features.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">1.2.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project Background</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_description.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">1.3.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Business and Economic Environment</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$business_economic_environment.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;font-weight: bold;">2</td>
                <td nowrap="nowrap" style="width:150;height:17px;font-weight: bold;">CLIENT</td>
                <td nowrap="nowrap" style="width:350;height:17px;"></td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Company Profile</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$company_description.'<p><span style="font-weight:bold;">Nature of Business: </span>'.$company_nature_of_business_data.'</p>'.$company_ownership_table.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.2.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">Borrower`s character assessment and credibility</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$short_character_assessment.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.3.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Governance and Management</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$governance_management.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.4.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Past Performance</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$past_performance.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.5.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Ratio Analysis</div></td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$ratio_analysis.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.6.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Risk Grading Analysis</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$risk_grading_analysis.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.7.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Financial Projections</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$financial_projections.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.8.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Security Cover Analysis</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$security_cover_analysis.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">2.9.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Others on the Client</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$others_on_client.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;font-weight: bold;">3</td>
                <td nowrap="nowrap" style="width:150;height:17px;font-weight: bold;">PROJECT</td>
                <td nowrap="nowrap" style="width:350;height:17px;"></td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project Concept and Rationale</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_concept_purpose.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.2.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project Objectives and Description</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_description.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.3.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project Components and Cost Estimates</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_components_cost_estimates.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.4.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Financing Plan and Role of EADB</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$financing_plan_role_of_eadb.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.5.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">Implementation Plan</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$implementation_plan.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.6.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project and Operational Management</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_operational_management.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.7.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Procurement and Disbursement</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$procurement_disbursement.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.8.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">Market Analysis</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$market_analysis.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.9.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Project Risks and Uncertainties</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_risks_uncertainties.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">3.10.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Others on the Project</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$project_background_other.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;font-weight: bold;">4</td>
                <td nowrap="nowrap" style="width:150;height:17px;font-weight: bold;">JUSTIFICATION OF EADB`s INVOLVEMENT</td>
                <td nowrap="nowrap" style="width:350;height:17px;"></td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Business Viability</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$business_viability.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.2.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">Economic Aspects</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$economic_aspects.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.3.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Environmental and Social Aspects</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$environmental_social_aspects.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.4.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">EADB`s Portfolio Analysis</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$eadb_portfolio_analysis.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.5.</p></td>
                <td nowrap="nowrap" style="width:150;height:17px;">EADB`s Benefits</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$eadb_benefits.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.6.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">EADB`s Additionalities</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$eadb_additionalities.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">4.7.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Others on EADB`s involvement</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$eadb_justification_other.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;font-weight: bold;">5</td>
                <td nowrap="nowrap" style="width:150;height:17px;font-weight: bold;">CONCLUSIONS AND RECOMMENDATIONS</td>
                <td nowrap="nowrap" style="width:350;height:17px;"></td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">5.1.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Conclusions</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$conclusions.'</td>
                </tr>
                <tr>
                <td nowrap="nowrap" style="width:100;height:17px;">5.2.</td>
                <td nowrap="nowrap" style="width:150;height:17px;">Recommendations</td>
                <td nowrap="nowrap" style="width:350;height:17px;">'.$recommendations.'</td>
                </tr>
                </tbody>
                </table>';

                $options = array(
                    'display' => 'firstPage',
                    'borderWidth' => 12,
                    'borderColor' => '000000'
                );
                $docx->addPageBorders($options);
                $textOptions = array(
                    'textAlign' => 'right',
                    'fontSize' => 12,
                    'color' => '000000',
                );
                $headerText = new WordFragment($docx, 'defaultHeader');
                $headerText->embedHTML('<p style="text-align:right; font-size:9;">Detailed Appraisal Report '.$project_name.'<br>'.$application_date.', '.$project_country.'</p>');
                $first = new WordFragment($docx, 'firstHeader');
                $first->addText('');
                $docx->addHeader(array('default' => $headerText, 'first' => $first));

                $pageNumberOptions = array(
                    'textAlign' => 'center',
                    'fontSize' => 10
                );
                $default = new WordFragment($docx, 'defaultFooter');
                $default->addPageNumber('numerical', $pageNumberOptions);
                $first = new WordFragment($docx, 'firstFooter');
                $first->addPageNumber('numerical', $pageNumberOptions);

                $docx->addFooter(array('default' => $default, 'first' => $first));

                $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));
                $docx->embedHTML($template_1);
                $docx->addBreak(array('type' => 'page'));
                $docx->embedHTML($template_2);
                $filename=str_replace(' ','_',$project_name).'_detailed_appraisal_report_'.date("d-m-Y");
                $documentfile='template_docx/'.$filename;
                $returnfilename=REST_API_URL.$documentfile.'.docx';
            }
            $docx->createDocx($documentfile);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$returnfilename);
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function downloadDisbursementRequest_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        /*$this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id_req')));*/
        $this->form_validator->add_rules('disbursement_request_id', array('required'=> $this->lang->line('disbursement_request_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
        $required_keys = array('project_contract_number');
        $result_array = array('project_title' => $project_details[0]['project_title'],'country' => $created_by[0]['branch_name'],'project_contract_number' => '','date' => $project_details[0]['created_date_time']);

        $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
        for($r=0;$r<count($required_keys);$r++)
        {
            for($s=0;$s<count($key_details);$s++){
                $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
            }
        }
        $data['id_disbursement_request'] = $data['disbursement_request_id'];
        $disbursement = $this->Project_model->getDisbursementRequest($data);
        for($s=0;$s<count($disbursement);$s++)
        {
            $result_array['disbursement'] = array(
                'date_of_receipt_of_request' => $disbursement[$s]['date_of_receipt_of_request'],
                'project_no' => $result_array['project_contract_number'],
                'project_title' => $project_details[0]['project_title'],
                'source_of_funds' => $disbursement[$s]['child_name'],
                'request_number' => $disbursement[$s]['request_number'],
                'disbursement_status' => $disbursement[$s]['disbursement_status'],
            );
        }

        $team = $this->Workflow_model->getProjectStageWorkflowDetails(array('project_stage_info_id' => $data['id_project_stage_info']));

        $team_array = array();
        for($s=0;$s<count($team);$s++){
            if($team[$s]['to_phase_name']!='')
                $team_array[] = array(
                    'phase_name' => $team[$s]['to_phase_name'],
                    'user_name' => $team[$s]['to_user_name']
                );
        }
        if(isset($team[count($team)-1]['to_phase_name']) && $team[count($team)-1]['to_phase_name']!='')
            $team_array[] = array(
                'phase_name' => $team[count($team)-1]['to_phase_name'],
                'user_name' => $team[count($team)-1]['to_user_name']
            );

        $team = array();
        foreach($team_array as $i){
            $team[$i['phase_name']] = $i;
        }
        $team_array = array_values($team);

        //echo "<pre>"; print_r($result_array); exit;
        require_once './phpdocx/classes/CreateDocx.inc';
        $docx = new CreateDocx();
        $docx->setDefaultFont('Calibri');
        $template_collection_data=$result_array;

        $project_ref_number=$template_collection_data['project_contract_number'];
        $project_name=$template_collection_data['project_title'];
        $project_country=$template_collection_data['country'];
        $application_date = date("d-m-Y", strtotime($template_collection_data['date']));
        $receipt_date = '';
        if($template_collection_data['disbursement']['date_of_receipt_of_request'])
            $receipt_date = date("d-m-Y", strtotime($template_collection_data['disbursement']['date_of_receipt_of_request']));

        $project_no=$template_collection_data['disbursement']['project_no'];
        $source_of_funds=$template_collection_data['disbursement']['source_of_funds'];
        $request_number=$template_collection_data['disbursement']['request_number'];
        $disbursement_status=$template_collection_data['disbursement']['disbursement_status'];
        //Need to build the phpdocx table

        $project_involved_team = '';
        if($team_array != '' && count($team_array)>0)
        {
            for($t=0;$t<count($team_array);$t++)
            {
                if($t == 0)
                    $project_involved_team.='<tr style="padding: 20;">';
                else
                    $project_involved_team.='<tr>';
                $project_involved_team.='<td>'.$team_array[$t]['phase_name'].':</td>';
                $project_involved_team.='<td>'.$team_array[$t]['user_name'].'</td>';
                $project_involved_team.='</tr>';
            }
        }
        else
        {
            $project_involved_team = '<tr style="padding: 20;">
                    <td>Task Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Reviewing Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Environmental Specialist:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Legal Counsel:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Appraisal Manager:</td>
                    <td>....................</td>
                    </tr>';
        }

        $options = array(
            'display' => 'firstPage',
            'borderWidth' => 12,
            'borderColor' => '000000'
        );
        $docx->addPageBorders($options);
        $textOptions = array(
            'textAlign' => 'right',
            'fontSize' => 12,
            'color' => '000000',
        );
        $headerText = new WordFragment($docx, 'defaultHeader');
        $headerText->embedHTML('<p style="text-align:right; font-size:9;">Concept Note '.$project_name.'<br>'.$application_date.', '.$project_country.'</p>');
        $first = new WordFragment($docx, 'firstHeader');
        $first->addText('');
        $docx->addHeader(array('default' => $headerText, 'first' => $first));

        $pageNumberOptions = array(
            'textAlign' => 'center',
            'fontSize' => 10
        );
        $default = new WordFragment($docx, 'defaultFooter');
        $default->addPageNumber('numerical', $pageNumberOptions);
        $first = new WordFragment($docx, 'firstFooter');
        $first->addPageNumber('numerical', $pageNumberOptions);

        $docx->addFooter(array('default' => $default, 'first' => $first));

        $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));
        $template_1 = $this->load->view('templates/disbursementrequest_template1.html','',true);
        $template_2 = $this->load->view('templates/disbursementrequest_template2.html','',true);
        $template_1=sprintf($template_1,$project_ref_number,$project_name,$project_country,$project_involved_team,$application_date);
        $template_2=sprintf($template_2,$receipt_date,$project_no,$project_name,$source_of_funds,$request_number,$disbursement_status);

        $docx->embedHTML($template_1);
        $docx->addBreak(array('type' => 'page'));
        $docx->embedHTML($template_2);
        $filename=str_replace(' ','_',$project_name).'_disbursement_request_'.date("d-m-Y");
        $documentfile='template_docx/'.$filename;
        $returnfilename=REST_API_URL.$documentfile.'.docx';
        $docx->createDocx($documentfile);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$returnfilename);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function downloadSupervisionReport_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        /*$this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id_req')));*/
        $this->form_validator->add_rules('supervision_assessment_id', array('required'=> $this->lang->line('supervision_assessment_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
        $required_keys = array('project_contract_number');
        $result_array = array('project_title' => $project_details[0]['project_title'],'country' => $created_by[0]['branch_name'],'project_contract_number' => '','date' => $project_details[0]['created_date_time']);

        $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
        for($r=0;$r<count($required_keys);$r++)
        {
            for($s=0;$s<count($key_details);$s++){
                $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
            }
        }
        $attachment_array = array();
        $data['id_supervision_assessment'] = $data['supervision_assessment_id'];
        $supervision = $this->Project_model->getSupervisionAssessment($data);
        for($s=0;$s<count($supervision);$s++)
        {
            $attachments = $this->Crm_model->getDocument(array('uploaded_from_id' => $supervision[$s]['id_supervision_assessment'],'module_type' => 'supervision'));
            for($r=0;$r<count($attachments);$r++){
                $attachment_array[] = getImageUrl($attachments[$r]['document_source'],'file');
            }
            $result_array['supervision'] = array(
                'introduction' => $supervision[$s]['introduction'],
                'missing_findings' => $supervision[$s]['missing_findings'],
                'proposed_action_plan' => $supervision[0]['proposed_action_plan'],
                'attachment_comments' => $supervision[$s]['attachment_comments'],
                'next_supervision_date' => $supervision[$s]['next_supervision_date'],
                'attachments' => $attachment_array
            );
        }

        $team = $this->Workflow_model->getProjectStageWorkflowDetails(array('project_stage_info_id' => $data['id_project_stage_info']));

        $team_array = array();
        for($s=0;$s<count($team);$s++){
            if($team[$s]['to_phase_name']!='')
                $team_array[] = array(
                    'phase_name' => $team[$s]['to_phase_name'],
                    'user_name' => $team[$s]['to_user_name']
                );
        }
        if(isset($team[count($team)-1]['to_phase_name']) && $team[count($team)-1]['to_phase_name']!='')
            $team_array[] = array(
                'phase_name' => $team[count($team)-1]['to_phase_name'],
                'user_name' => $team[count($team)-1]['to_user_name']
            );

        $team = array();
        foreach($team_array as $i){
            $team[$i['phase_name']] = $i;
        }
        $team_array = array_values($team);
        //echo "<pre>"; print_r($result_array); exit;

        require_once './phpdocx/classes/CreateDocx.inc';
        $docx = new CreateDocx();
        $docx->setDefaultFont('Calibri');
        $template_collection_data=$result_array;

        $project_ref_number=$template_collection_data['project_contract_number'];
        $project_name=$template_collection_data['project_title'];
        $project_country=$template_collection_data['country'];
        $application_date = date("d-m-Y", strtotime($template_collection_data['date']));

        $supervision_introduction=$template_collection_data['supervision']['introduction'];
        $supervision_missing_findings=$template_collection_data['supervision']['missing_findings'];
        $supervision_proposed_action_plan=$template_collection_data['supervision']['proposed_action_plan'];
        $supervision_attachments='';
        //attachments
        $supervision_attachments.='<ul>';
        for($aw=0;$aw<count($template_collection_data['supervision']['attachments']);$aw++)
        {
            $supervision_attachments.='<li><a href="'.$template_collection_data['supervision']['attachments'][$aw].'">'.$template_collection_data['supervision']['attachments'][$aw].'</a></li>';
        }
        $supervision_attachments.='</ul>';
        //Need to build the phpdocx table
        $project_involved_team = '';
        if($team_array != '' && count($team_array)>0)
        {
            for($t=0;$t<count($team_array);$t++)
            {
                if($t == 0)
                    $project_involved_team.='<tr style="padding: 20;">';
                else
                    $project_involved_team.='<tr>';
                $project_involved_team.='<td>'.$team_array[$t]['phase_name'].':</td>';
                $project_involved_team.='<td>'.$team_array[$t]['user_name'].'</td>';
                $project_involved_team.='</tr>';
            }
        }
        else
        {
            $project_involved_team = '<tr style="padding: 20;">
                    <td>Task Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Reviewing Manager:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Environmental Specialist:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Legal Counsel:</td>
                    <td>....................</td>
                    </tr>
                    <tr>
                    <td>Appraisal Manager:</td>
                    <td>....................</td>
                    </tr>';
        }
        $options = array(
            'display' => 'firstPage',
            'borderWidth' => 12,
            'borderColor' => '000000'
        );
        $docx->addPageBorders($options);
        $textOptions = array(
            'textAlign' => 'right',
            'fontSize' => 12,
            'color' => '000000',
        );
        $headerText = new WordFragment($docx, 'defaultHeader');
        $headerText->embedHTML('<p style="text-align:right; font-size:9;">Project Supervision Report '.$project_name.'<br>'.$application_date.', '.$project_country.'</p>');
        $first = new WordFragment($docx, 'firstHeader');
        $first->addText('');
        $docx->addHeader(array('default' => $headerText, 'first' => $first));

        $pageNumberOptions = array(
            'textAlign' => 'center',
            'fontSize' => 10
        );
        $default = new WordFragment($docx, 'defaultFooter');
        $default->addPageNumber('numerical', $pageNumberOptions);
        $first = new WordFragment($docx, 'firstFooter');
        $first->addPageNumber('numerical', $pageNumberOptions);

        $docx->addFooter(array('default' => $default, 'first' => $first));

        $docx->modifyPageLayout('A4',array('marginLeft' => '1000','marginRight' => '1000'));
        $template_1 = $this->load->view('templates/projectsupervisionreport_template1.html','',true);
        $template_2 = $this->load->view('templates/projectsupervisionreport_template2.html','',true);
        $template_1=sprintf($template_1,$project_ref_number,$project_name,$project_country,$project_involved_team,$application_date);
        $template_2=sprintf($template_2,$supervision_introduction,$supervision_missing_findings,$supervision_proposed_action_plan,$supervision_attachments);

        $docx->embedHTML($template_1);
        $docx->addBreak(array('type' => 'page'));
        $docx->embedHTML($template_2);
        $filename=str_replace(' ','_',$project_name).'_supervisionreport_'.date("d-m-Y");
        $documentfile='template_docx/'.$filename;
        $returnfilename=REST_API_URL.$documentfile.'.docx';
        $docx->createDocx($documentfile);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$returnfilename);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function offerLetter_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_stage_info', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_stage_details = $this->Project_model->getProjectStageDetails(array('project_stage_info_id' => $data['id_project_stage_info']));
        $data['project_id'] = $project_stage_details[0]['project_id'];
        $data['stage_id'] = $project_stage_details[0]['stage_id'];
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
        $country_office = $created_by[0]['branch_name'];
        $country_name = $created_by[0]['country_name'];
        $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));
        $loan_amount = array_map(function($i){ return $i['facility_usd_amount']; },$facilities);
        $loan_amount = array_sum($loan_amount);
        $contract_id = 0;
        $project_date = $project_details[0]['created_date_time'];
        $project_name = $project_details[0]['project_title'];
        if(!empty($facilities)){ $contract_id = $facilities[0]['contract_id']; }
        $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
        $company_name = ''; $info = array();
        $company_keys = array('company_street_name','company_block_number','company_city','company_region','company_country');
        $info = array('company_street_name' => '','company_block_number' => '','company_city' => '','company_region' =>'','company_country' => '');
        if(!empty($borrower)){
            $company_name = $borrower[0]['company_name'];
            $key_data = $this->Project_model->getCrmCompanyDataByKeys(array('crm_company_id' => $borrower[0]['id_crm_company'],'keys' => $company_keys));
            if(!empty($key_data)){
                for($sr=0;$sr<count($key_data);$sr++) {
                    $info[$key_data[$sr]['field_name']] = $key_data[$sr]['form_field_value'];
                }
            }
        }
        $company_address = $info;
        //$required_keys = array('project_contract_number','project_concept_purpose','project_location','pre_conditions_for_disbursement','procurement_of_goods','insurance','other_conditions','security','representations_and_warranties','information_undertakings','financial_covenants','general_undertakings','events_of_default','special_audit','legal_documentation','costs_and_expenses','applicable_law','dispute_resolution','other');
        $required_keys = ['costs_and_expenses','legal_opinions','legal_documentation','special_audit','events_of_default','financial_covenants','representations_and_warranties','board_of_directors_representation','procurement_of_goods','reporting_obligations','insurance','pre_conditions_for_disbursement','debt_service_reserve_account_agreement','security','procurement_disbursement','project_location','project_contract_number','project_concept_purpose'];
        $result_array = array('project_title' => $project_name,'loan_amount' => $loan_amount,'application_date' => $project_date,'country' => $country_name,'country_office' => $country_office,'company_name' => $company_name,'company_address'=>$company_address,'project_location' =>'','project_concept_purpose' => '', 'project_contract_number' => '','facilities' => '','procurement_disbursement' => '','security' => '','debt_service_reserve_account_agreement' => '','pre_conditions_for_disbursement' => '','insurance' => '','reporting_obligations' => '','procurement_of_goods' => '','board_of_directors_representation' => '','representations_and_warranties' => '','costs_and_expenses' => '','legal_opinions' => '','legal_documentation' => '','special_audit' => '','events_of_default' => '','financial_covenants' => '','waiver_of_privileges_and_immunities' => '', 'others' => '');
        //$result_array_more = ['others' => '---','waiver_of_privileges_and_immunities' => '---', 'country' => $country_name ];
        $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
        //echo "<pre>"; print_r($key_details); exit;
        for($s=0;$s<count($key_details);$s++){
            $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
        }

        for($s=0;$s<count($facilities);$s++)
        {
            $interest_rate = '';
            $loan_type = '---';
            $facility_type_name = '---';
            $type_of_loan = $this->Master_model->getMasterChild(array('child_id' => $facilities[$s]['loan_payment_type']));
            $facility_type = $this->Master_model->getMasterChild(array('child_id' => $facilities[$s]['facility_type']));
            $pricing_array = $terms_condition_array = '';
            $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'pricing'));
            $terms_conditions = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
            //echo "<pre>"; print_r($pricing); exit;
            for($r=0;$r<count($pricing);$r++){
                $pricing_array[$r] = array(
                    'name' => $pricing[$r]['field_name'],
                    'payment_type' => $pricing[$r]['payment_type'],
                    'label' => '',
                    'facility_field_value' => $pricing[$r]['facility_field_value'],
                    'facility_field_description' => $pricing[$r]['facility_field_description']
                );
                if($pricing[$r]['payment_type']=='interest_as'){
                    $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                    $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                }
                else if($pricing[$r]['payment_type']=='fee_as'){
                    $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                    $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                }
                else if($pricing[$r]['payment_type']=='fee_amount'){
                    $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                    $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                }

                if($pricing[$r]['field_key']=='interest_rate'){
                    $interest_rate =  $pricing_array[$r];
                }
            }
            $pricing_array = array_values($pricing_array);
            //echo "<pre>"; print_r($terms_conditions); exit;

            for($r=0;$r<count($terms_conditions);$r++){
                $terms_condition_array[] = array(
                    'name' => $terms_conditions[$r]['field_name'],
                    'description' => $terms_conditions[$r]['facility_field_description']
                );
            }

            if(!empty($type_of_loan)){ $loan_type = $type_of_loan[0]['child_name']; }
            if(!empty($facility_type)){ $facility_type_name = $facility_type[0]['child_name']; }
            $result_array['facilities'][] = array(
                'facility_name' => $facilities[$s]['project_facility_name'],
                'currency' => $facilities[$s]['currency_code'],
                'amount' => $facilities[$s]['facility_amount'],
                'usd_amount' => $facilities[$s]['facility_usd_amount'],
                'type_of_loan' => $loan_type,
                'facility_maturity_date' => $facilities[$s]['facility_maturity_date'],
                'facility_type' => $facility_type_name,
                'facility_purpose' => $facilities[$s]['facility_purpose'],
                'facility_minimum_amount' => $facilities[$s]['facility_amount'],
                'other_facility_modalities' => $facilities[$s]['other_facility_modalities'],
                'security' => $facilities[$s]['facility_security'],
                'maturity_loan_repayment' => $facilities[$s]['facility_tenor'],
                'interest_payment' => $interest_rate,
                'pricing' => $pricing_array,
                'terms_conditions' => $terms_condition_array
            );
        }
        $project_stage_details[0]['fields'] = $result_array;
        $project_stage_details = $project_stage_details[0];

        require_once './phpdocx/classes/CreateDocx.inc';
        $docx = new CreateDocx();
        $docx->setDefaultFont('Calibri');
        //print_r($project_stage_details);exit;
        $template_collection_data = $project_stage_details['fields'];
        $project_ref_number=$template_collection_data['project_contract_number'];
        $project_name=$template_collection_data['project_title'];
        $company_name=$template_collection_data['company_name'];
        $application_date = date("d-m-Y", strtotime($template_collection_data['application_date']));
        $textOptions = array(
            'textAlign' => 'right',
            'fontSize' => 12,
            'color' => '000000'
        );
        $headerText = new WordFragment($docx, 'defaultHeader');
        $headerText->embedHTML('<img style="width: 60;float: right;" src="'.REST_API_URL.'/template_docx/eadb_logo.png"><p style="font-size:9;font-style: italic;text-align: right;font-family: "Times New Roman", serif">"SUBJECT TO CONTRACT"</p>');
        $firstHeader = new WordFragment($docx, 'firstHeader');
        $firstHeader->embedHTML('<div style="font-style:italic;font-family: \"Times New Roman\", Georgia, Serif;"><h3 style="font-size:20;margin-bottom:0;">EAST AFRICAN DEVELOPMENT BANK <img style="width: 80;float: right;padding-right: 90;" src="'.REST_API_URL.'/template_docx/eadb_logo.png"></h3><span style="font-size:14;margin-top: 0;">Head Office</span><p></p></div>');
        $docx->addHeader(array('default' => $headerText, 'first' => $firstHeader));

        $pageNumberOptions = array(
            'textAlign' => 'center',
            'fontSize' => 10
        );
        $defaultFooter = new WordFragment($docx, 'defaultFooter');
        $defaultFooter->addPageNumber('numerical', $pageNumberOptions);
        $firstFooter = new WordFragment($docx, 'firstFooter');
        $firstFooter->embedHTML('<table width="100%" style="width100%;border-collapse:collapse;border-top: 1 solid #000000;font-size: 8;font-style: italic;font-family: \"Times New Roman\", Georgia, Serif;">
        <tr><td colspan="4"></td></tr>
    	<tr>
        	<td style="width: 150">
        	    <span style="font-weight: bold">Country office, Kenya</span><br>
                2nd Floor, Rahimtulla Tower<br>
                Upper Hill Road<br>
                P.O Box 47685, Nairobi<br>
                Tel: 254-20-340642/340656<br>
                Fax: 254-20-2731590<br>
                E-mail:cok@eadb.org
            </td>
            <td style="width: 150">
            	<span style="font-weight: bold">HEADQUARTERS<br>
                Kampala, Uganda</span><br>
                EADB Bldng, 4 Nile Avenue<br>
                P.O Box 7128, Kampala<br>
                Tel: 256-41-4230021/5<br>
                Fax: 256-41-4259763/253585<br>
                E-mail: admin@eadb.org
            </td>
            <td>
            	<span style="font-weight: bold">Country office, Tanzania</span><br>
                7th Floor, NSSF Waterfront House Sokoine Drive<br>
                P.O Box 9401,Dar-es-Salaam<br>
                Tel: 255-22-2113195/2116981<br>
                Fax: 255-22-2113197<br>
                E-mail:cot@eadb.org
            </td>
            <td>
            	<span style="font-weight: bold">Country Office, Rwanda</span><br>
                Kacyiru, Glory House, Grnd Floor<br>
                B.P. 6225, Kigali<br>
                Tel: 0037 570323<br>
                E-mail: cor@eadb.org
            </td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr><td colspan="4"><div style="font-weight: bold;font-style:normal;text-align: center;font-size: 10;background-color: #000000;color: #ffffff">www.eadb.org</div></td></tr>
    </table>');
        $docx->addFooter(array('default' => $defaultFooter, 'first' => $firstFooter));
        $docx->modifyPageLayout('A4',array('marginLeft' => '1200','marginRight' => '1200'));

        $company_address='';
        $flag=0;
        if($company_name!='')
        {
            $company_address.=$company_name;
        }
        if($template_collection_data['company_address']['company_street_name']!='')
        {
            $company_address.=',<br/>';
            $company_address.=$template_collection_data['company_address']['company_street_name'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_block_number']!='')
        {
            if($template_collection_data['company_address']['company_street_name']!='')
            {
                $company_address.=',';
            }
            else
            {
                $company_address.=',<br/>';
            }
            $company_address.=$template_collection_data['company_address']['company_block_number'].',';
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.='<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_city']!='')
        {
            if($template_collection_data['company_address']['company_street_name']=='' && $template_collection_data['company_address']['company_block_number']=='')
            {
                $company_address.='<br/>';
            }
            $company_address.=$template_collection_data['company_address']['company_city'];
            $flag=1;
        }
        if($template_collection_data['company_address']['company_region']!='')
        {
            if($template_collection_data['company_address']['company_city']!='')
            {
                $company_address.=',';
            }
            $company_address.=$template_collection_data['company_address']['company_region'];
            $flag=1;
        }
        if($flag == 1)
        {
            $company_address.=',<br/>';
            $flag=0;
        }
        if($template_collection_data['company_address']['company_country']!='')
        {
            if($template_collection_data['company_address']['company_street_name']=='' && $template_collection_data['company_address']['company_block_number']=='' && $template_collection_data['company_address']['company_city']=='' && $template_collection_data['company_address']['company_region']=='')
            {
                $company_address.='<br/>';
            }
            $company_address.=$template_collection_data['company_address']['company_country'];
        }
        $facility_types=[];
        if($template_collection_data['facilities'])
        {
            for($fc=0;$fc<count($template_collection_data['facilities']);$fc++)
            {
                if (!in_array($template_collection_data['facilities'][$fc]['facility_type'], $facility_types))
                {
                    array_push($facility_types,$template_collection_data['facilities'][$fc]['facility_type']);
                }
            }
            $facility_types= implode(",", $facility_types);
        }
        else
        {
            $facility_types= '';
        }
        $project_concept_purpose = $template_collection_data['project_concept_purpose'];


        $offer_letter_html='<table style="width: 600;font-size:10;"><tr><td style="text-align: left"><span style="font-weight: bold;">Our Ref:</span> '.$project_ref_number.'</td><td style="text-align: right">'.$application_date.'</td></tr></table>
<div style="font-size: 10;">
    <p style="padding: 0;text-align: right;font-weight: bold;">"SUBJECT TO CONTRACT"</p>
    <p style="padding-bottom:15;">'.$company_address.'</p>
    <p>Dear Sirs,</p>
    <p style="font-weight:bold;">RE: APPROVAL OF FINANCING</p>
    <p>We are pleased to inform you that the East African Development Bank has approved your application for a '.$facility_types.' for the project described in outline below on the basis of terms and conditions outlined in this letter. These will form the basis for the transaction documentation but should not be considered exhaustive.</p>
    <p>The approval is subject to completion of all outstanding due diligence and negotiation and execution of appropriate legal documentation on terms satisfactory to the Bank.</p>
</div>';
        $index_flag=1;
        $offer_letter_html.='<ol style="font-weight: bold;font-size:10;">';
        $offer_letter_html.='<li style="margin-bottom:10;padding-bottom:10;">Project Concept <div style="font-weight: normal;margin: 0;padding: 0;">'.$project_concept_purpose.'</div></li>';
        $offer_letter_html.='<li>Outline Terms and conditions<p style="margin: 0;padding: 0;"></p>';
        $offer_letter_html.='<table width="600" border="1" style="width:600;border-collapse:collapse;font-size:10;font-weight:bold;padding-left:20;margin-left:20;">
            <tbody>
            <tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Borrower:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$company_name.' (the "Borrower")</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Bank:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">East African Development Bank ("EADB" or the "Bank")</td>
            </tr>';
        //print_r($template_collection_data['facilities']);exit;
        if($template_collection_data['facilities'])
        {

            for($fc=0;$fc<count($template_collection_data['facilities']);$fc++)
            {
                $facility_pricing = number_format($template_collection_data['facilities'][$fc]['amount']);
                if($template_collection_data['facilities'][$fc]['currency'] != 'USD')
                    $facility_pricing .= ' (USD '.number_format($template_collection_data['facilities'][$fc]['usd_amount']).')';
                $index_flag=$index_flag+1;
                $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">Currency:</td>
                    <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['currency'].'</td>
                </tr>';
                $index_flag=$index_flag+1;
                $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">Facility:</td>
                    <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['facility_name'].'</td>
                </tr>';
                $index_flag=$index_flag+1;
                $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">Amount:</td>
                    <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$facility_pricing.'</td>
                </tr>';
                $index_flag=$index_flag+1;
                $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">Purpose of Loan</td>
                    <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['facility_purpose'].'</td>
                </tr>';
                $index_flag=$index_flag+1;
                $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">Availability Period and Project Completion Date</td>
                    <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['facility_maturity_date'].'</td>
                </tr>';
                //print_r($template_collection_data['facilities'][$fc]['pricing']);exit;
                for($pr=0;$pr<count($template_collection_data['facilities'][$fc]['pricing']);$pr++)
                {
                    $index_flag=$index_flag+1;
                    $offer_letter_html.='<tr>
                    <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                    <td nowrap="nowrap" style="width:60;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['pricing'][$pr]['name'].':</td>';
                    if($template_collection_data['facilities'][$fc]['pricing'][$pr]['payment_type'] == 'fee_amount')
                        $offer_letter_html.='<td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.number_format($template_collection_data['facilities'][$fc]['pricing'][$pr]['facility_field_value']).' '.$template_collection_data['facilities'][$fc]['pricing'][$pr]['label'].$template_collection_data['facilities'][$fc]['pricing'][$pr]['facility_field_description'].'</td>';
                    else
                        $offer_letter_html.='<td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['facilities'][$fc]['pricing'][$pr]['facility_field_value'].'% '.$template_collection_data['facilities'][$fc]['pricing'][$pr]['label'].$template_collection_data['facilities'][$fc]['pricing'][$pr]['facility_field_description'].'</td>';
                    $offer_letter_html.='</tr>';
                }
            }
        }
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Disbursement of Funds:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['procurement_disbursement'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Security:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['security'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Debt Service Reserve Account Agreement:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['debt_service_reserve_account_agreement'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Deed of Subordination:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">All loans from any of the shareholders or directors of the Borrower will be subordinated to the claims of the Bank under the finance documents.</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Conditions Precedent:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['pre_conditions_for_disbursement'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Insurance:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['insurance'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Reporting Obligations:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['reporting_obligations'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Procurement of Goods and Services:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['procurement_of_goods'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Board of Directors Representation:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['board_of_directors_representation'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Representations and Warranties:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['representations_and_warranties'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Covenants of the Borrower:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['financial_covenants'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Events of Default:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['events_of_default'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Special Audit:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['special_audit'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Legal Documentation:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['legal_documentation'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Legal Opinions:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['legal_opinions'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Costs and expenses:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['costs_and_expenses'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Governing law and Dispute resolution:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">
                    All legal documents to be executed shall be governed by the [country] Law.<br/>
                    Dispute resolution shall be by way of mediation under London Court of International Arbitration (`LCIA`) Mediation Rules.<br/>
                    All documentation, correspondence, invoices, receipts and all forms of communication shall be undertaken in the English Language.
                </td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">No Waiver of Privileges and Immunities:</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['waiver_of_privileges_and_immunities'].'</td>
            </tr>';
        $index_flag=$index_flag+1;
        $offer_letter_html.='<tr>
                <td nowrap="nowrap" style="width:40;border-style: dotted;">'.$index_flag.'.</td>
                <td nowrap="nowrap" style="width:60;border-style: dotted;">Others</td>
                <td nowrap="nowrap" style="width:250;font-weight: normal;border-style: dotted;">'.$template_collection_data['others'].'</td>
            </tr>';
        $offer_letter_html.='</tbody></table>';
        $offer_letter_html.='</li></ol>';
        $offer_letter_html.='<div style="font-size:10;">
    <p>In the event of a conflict between the terms of this Letter of Offer and any other Agreement between the Bank and the Borrower including the Indicative Term Sheet signed on the [input manually date],  the terms of this Letter of Offer shall prevail.</p>
    <p style="margin-bottom: 40">Please indicate your acceptance of the above terms by signing and returning to us a copy of this letter within five (5) business days of the date hereof.  Failure to do so will result in the above offer lapsing automatically and the entire appraisal fee forfeited.</p>
    <p style="margin-top: 0;margin-bottom: 10">Yours faithfully,<br>
        <span style="font-weight:bold;">EAST AFRICAN DEVELOPMENT BANK</span>
    </p>
    <table width="600" style="width: 600;">
        <tr>
            <td nowrap="nowrap" style="width:300;">[Input Country Manager Name]</td>
            <td nowrap="nowrap" style="width:300;">[Input Principal Legal Officer]</td>
        </tr>
        <tr style="font-weight: bold;">
            <td nowrap="nowrap" style="width:300;">COUNTRY MANAGER (UGANDA)</td>
            <td nowrap="nowrap" style="width:300;">PRINCIPAL LEGAL OFFICER</td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="200" style="width: 200;">
                    <tr>
                        <td nowrap="nowrap" style="width:40;vertical-align: top;">cc:</td>
                        <td nowrap="nowrap" style="width:160;">Finance Manager,<br/>EADB, <span style="font-weight: bold">KAMPALA</span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 100%;border-top: 3 double #000000;margin: 0;padding: 0;border-spacing: 1;">

            </td>
        </tr>
    </table>
</div>';
        $offer_letter_html.='<div style="width:100%;text-decoration: underline;text-align: center;font-weight: bold;font-size:10;">ACKNOWLEDGEMENT</div>';
        $offer_letter_html.='<p style="font-size:10;line-height: 1.5;">We, (1).............................................................................................................. and (2)
    .............................................................................................................. DULY AUTHORISED on behalf of '.$company_name.' hereby accept the terms and conditions of the Term Loan as provided above.
</p>';
        $offer_letter_html.='<table width="600" style="width: 600;">
    <tr>
        <td nowrap="nowrap" style="width:300;padding-top: 10">------------------------------------------------</td>
        <td nowrap="nowrap" style="width:50;">}</td>
        <td nowrap="nowrap" style="width:250;"></td>
    </tr>
    <tr>
        <td nowrap="nowrap" style="width:300"></td>
        <td nowrap="nowrap" style="width:50">}</td>
        <td nowrap="nowrap" style="width:250;">Authorized Signatories</td>
    </tr>
    <tr>
        <td nowrap="nowrap" style="width:300;padding-top: 10">------------------------------------------------</td>
        <td nowrap="nowrap" style="width:50;">}</td>
        <td nowrap="nowrap" style="width:250;"></td>
    </tr>
</table>';
        $docx->embedHTML($offer_letter_html);
        $filename=str_replace(' ','_',$project_name).'_offer_letter_'.date("d-m-Y");
        $documentfile='template_docx/'.$filename;
        $returnfilename=REST_API_URL.$documentfile.'.docx';
        $docx->createDocx($documentfile);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$returnfilename);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Crm extends REST_Controller
{

    public $allowed_created_by = [];
    public $allowed_company_approval_roles = [];
    public $current_user = 0;
    public $order_data = 0;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Company_model');
        $this->load->model('Activity_model');
        $this->load->model('Project_model');
        $this->load->model('Workflow_model');
        $this->load->model('Facility_model');

        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];
        }

        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if(!empty($user_info) && $user_info[0]['user_role_id']==3 && $user_info[0]['all_projects']==0)
        {
            $this->current_user = $user_id;
        }
        else{
            $this->current_user = 0;
        }

        //$this->current_user = 0;
    }

    public function contactList_get()
    {
        $data = $this->input->get();

        $result = $this->Crm_model->getContactList($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = $this->Crm_model->getContactListCount($data);
        $all_records =$total_records[0]['total'];

        unset($data['search_key']);
        //all records
        if(isset($data['created_by'])){ $created_by = $data['created_by']; unset($data['created_by']); }
        $total_records = $this->Crm_model->getContactListCount($data);
        $all_clients = $total_records[0]['total'];

        //created by user records
        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $my_contacts = $this->Crm_model->getContactListCount($data);
        $my_contacts = $my_contacts[0]['total'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $all_records,'my_clients' => $my_contacts,'all_clients' => $all_clients));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmContactType_get()
    {
        $contact_type = $this->Crm_model->getCrmContactType();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$contact_type);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result=$this->Crm_model->getContact($data['crm_contact_id']);
        $result[0]['grade'] = $this->contactRiskGradeCalculation($result[0]['internal_rating']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result=$this->Crm_model->getCompany($data['crm_company_id']);

        //$rating = $this->getCrmCompanyRiskRating($data['crm_company_id'],$result[0]['company_id']);
        //$rating = explode('@@@',$rating);
        //if($rating[1]){ $result[0]['rating'] = (float)$rating[0]; }
        //else{ $result[0]['rating'] = '---'; }
        $result[0]['grade'] = $this->companyRiskGradeCalculation($result[0]['internal_rating']);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactForms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_contact_id = $data['crm_contact_id'];
        $contact = $this->Crm_model->getContact($data['crm_contact_id']);
        $result=$this->Crm_model->getModuleDetails($data['crm_module_id']);
        $data = array(); $section_id = array();
        $status = 0;
        $contactTypeId = $this->Crm_model->checkRecord('crm_contact_type', array('key_crm_contact_type'=>'borrower_gaurantor'));
        for($s=0;$s<count($result);$s++)
        {
            if($contact[0]['crm_contact_type_id']!=$contactTypeId[0]['id_crm_contact_type'])
            {
                if($result[$s]['form_key']=="client_information" || $result[$s]['form_key']=='client_contact_information')
                {
                    if(!in_array($result[$s]['id_section'],$section_id)){
                        array_push($section_id,$result[$s]['id_section']);
                        $data[$result[$s]['id_section']] = array(
                            'id_section' => $result[$s]['id_section'],
                            'section_name' => $result[$s]['section_name']
                        );
                        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                        if(empty($module_status)) $status = 0;
                        else $status = 1;
                        //getting form updated date time
                        $last_updated_time = $user_name = '';
                        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $crm_contact_id,'project_stage_section_form_id' => $result[$s]['id_form']));
                        if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }

                        $data[$result[$s]['id_section']]['forms'][] = array(
                            'id_form' => $result[$s]['id_form'],
                            'form_name' => $result[$s]['form_name'],
                            'form_template' => $result[$s]['form_template'],
                            'form_class' => $result[$s]['form_class'],
                            'form_key' => $result[$s]['form_key'],
                            'status' => $status,
                            'updated_date_time' => $last_updated_time,
                            'updated_by' => $user_name
                        );
                    }
                    else{
                        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                        if(empty($module_status)) $status = 0;
                        else $status = 1;
                        //getting form updated date time
                        $last_updated_time = $user_name = '';
                        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $crm_contact_id,'project_stage_section_form_id' => $result[$s]['id_form']));
                        if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }

                        $data[$result[$s]['id_section']]['forms'][] = array(
                            'id_form' => $result[$s]['id_form'],
                            'form_name' => $result[$s]['form_name'],
                            'form_template' => $result[$s]['form_template'],
                            'form_class' => $result[$s]['form_class'],
                            'form_key' => $result[$s]['form_key'],
                            'status' => $status,
                            'updated_date_time' => $last_updated_time,
                            'updated_by' => $user_name
                        );
                    }
                }
            }
            else
            {
                if(!in_array($result[$s]['id_section'],$section_id)){
                    array_push($section_id,$result[$s]['id_section']);
                    $data[$result[$s]['id_section']] = array(
                        'id_section' => $result[$s]['id_section'],
                        'section_name' => $result[$s]['section_name']
                    );
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                    if(empty($module_status)) $status = 0;
                    else $status = 1;
                    //getting form updated date time
                    $last_updated_time = $user_name = '';
                    $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $crm_contact_id,'project_stage_section_form_id' => $result[$s]['id_form']));
                    if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }


                    $data[$result[$s]['id_section']]['forms'][] = array(
                        'id_form' => $result[$s]['id_form'],
                        'form_name' => $result[$s]['form_name'],
                        'form_template' => $result[$s]['form_template'],
                        'form_class' => $result[$s]['form_class'],
                        'form_key' => $result[$s]['form_key'],
                        'status' => $status,
                        'updated_date_time' => $last_updated_time,
                        'updated_by' => $user_name
                    );
                }
                else{
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                    if(empty($module_status)) $status = 0;
                    else $status = 1;
                    //getting form updated date time
                    $last_updated_time = $user_name = '';
                    $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $crm_contact_id,'project_stage_section_form_id' => $result[$s]['id_form']));
                    if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }

                    $data[$result[$s]['id_section']]['forms'][] = array(
                        'id_form' => $result[$s]['id_form'],
                        'form_name' => $result[$s]['form_name'],
                        'form_template' => $result[$s]['form_template'],
                        'form_class' => $result[$s]['form_class'],
                        'form_key' => $result[$s]['form_key'],
                        'status' => $status,
                        'updated_date_time' => $last_updated_time,
                        'updated_by' => $user_name
                    );
                }
            }
        }


        $data = array_values($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_get($contact_id,$module_id)
    {
        $contact = $this->Crm_model->getContact($contact_id);
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = $active_forms = array(); $active_form_id = 0;
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));

        $contactTypeId = $this->Crm_model->checkRecord('crm_contact_type', array('key_crm_contact_type'=>'borrower_gaurantor'));
        $sectionInfo = $this->Crm_model->checkRecord('section', array('section_key'=>'basic_information'));

        if($contact[0]['crm_contact_type_id']!=$contactTypeId[0]['id_crm_contact_type']) {
            foreach($unique_id_sections as $key=>$val){
                if($val!=$sectionInfo[0]['id_section']){
                    unset($unique_section_names[$key]);
                    unset($unique_id_sections[$key]);
                }
            }
        }

        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];
            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st= $active_form_id = 0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $last_updated_time = $user_name = '';
                    $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $contact_id,'project_stage_section_form_id' => $result[$r]['id_form']));
                    if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }

                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_key'] = $result[$r]['form_key'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['updated_date_time'] = $last_updated_time;
                    $form[$st]['updated_by'] = $user_name;
                    $st++;
                }
            }

            $data[$s]['form'] = $form;
        }
        $t=0;
        //$form_key = array_values(array_unique(array_map(function ($i) { return $i['form_key']; }, $result)));
        $company_id = $this->Crm_model->checkRecord('crm_contact',array('id_crm_contact'=>$contact_id));
        $company_id = $company_id[0]['company_id'];
        //getting fields data
        $crm_contact_type_based_fields = array('first_name','last_name','phone_number','email');
        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++){
                $form_field_data = $this->Crm_model->getContactFormData($contact_id,$data[$s]['form'][$r]['id_form']);
                //echo "<pre>12"; print_r($contact[0]['crm_contact_type_id']); exit;
                $field_data = array();
                $contact_data='';
                $relation_data='';
                if($data[$s]['form'][$r]['form_key']=='personal_information'){
                    $contact_data = $this->Crm_model->getContactPersonalInformation(array('crm_contact_id'=>$contact_id,'crm_company_id'=>$company_id));
                    $arr = array('crm_contact_id'=>$contact_id,'relation_type'=>"children,spouse",'crm_company_id'=>$company_id);

                    $relation_data = $this->contactRelationInformation_get($arr);

                    $relation_info=array();
                    foreach($relation_data as $key => $val){
                        $relation_info[$key]['first_name']=$val['contact_details']['first_name'];
                        $relation_info[$key]['last_name']=$val['contact_details']['last_name'];
                        $relation_info[$key]['contact_type']=$val['contact_details']['crm_contact_type_name'];
                        $relation_info[$key]['relation_name']=$val['relation_name'];
                    }
                    $personal_info=array();
                    if(!empty($contact_data)){
                        $personal_info['resident_at_current_location_since']=$contact_data['resident_at_current_location_since'];
                        $personal_info['residence']=$contact_data['residence']==1?'Own':'Rented';
                        $personal_info['educationInfo']=$contact_data['educationInfo'];
                        $personal_info['martialInfo']=$contact_data['martialInfo'];
                        $personal_info['occupationInfo']=$contact_data['occupationInfo'];
                        $personal_info['employer']=$contact_data['employer'];
                        $personal_info['employed_since']=$contact_data['employed_since'];
                        $personal_info['monthly_salary']=$contact_data['monthly_salary'];
                        $personal_info['monthly_salary_currency_info']=$contact_data['monthly_salary_currency_info'];
                        $personal_info['monthly_expenses']=$contact_data['monthly_expenses'];
                        $personal_info['monthly_expenses_currency_info']=$contact_data['monthly_expenses_currency_info'];
                    }

                    $field_data['personal_info']=$personal_info;
                    $field_data['relation_info']=$relation_info;
                    /*foreach($relation_data as $k => $v){
                        $field_data[]=$v;
                    }*/

                }
                if($data[$s]['form'][$r]['form_key']=='reference_guarantor'){
                    $contact_data = $this->contactRelationInformation_get(array('crm_contact_id'=>$contact_id,'status'=>1,'relation_type'=>"reference_guarantor"));
                    $reference_guarantor_info=array();
                    foreach($contact_data as $key => $val){
                        $reference_guarantor_info[$key]['first_name']=$val['data']['client_first_name'];
                        $reference_guarantor_info[$key]['last_name']=$val['data']['client_last_name'];
                        $reference_guarantor_info[$key]['client_gender']=$val['data']['client_gender'];
                        $reference_guarantor_info[$key]['client_date_of_birth']=$val['data']['client_date_of_birth'];
                    }
                    $field_data['reference_guarantor']=$reference_guarantor_info;
                }
                if($data[$s]['form'][$r]['form_key']=='assets_and_liabilities'){
                    $contact_data = $this->Crm_model->getContactAssetsAndLiabilities(array('crm_contact_id'=>$contact_id,'status'=>1,'crm_company_id'=>$company_id));
                    $contact_data_income_expenses = $this->Crm_model->getContactIncomeAndExpenses(array('crm_contact_id'=>$contact_id,'status'=>1,'crm_company_id'=>$company_id));
                    $field_data['assets_and_liabilities']=$contact_data;

                    $assets_info=array();
                    foreach($contact_data['assets'] as $key => $val){
                        $assets_info[$key]['type_name']=$val['type_name'];
                        $assets_info[$key]['currency_code']=$val['currency_code'];
                        $assets_info[$key]['value']=$val['value'];
                        $assets_info[$key]['aquisition_date']=$val['aquisition_date'];
                    }
                    $field_data['assets']=$assets_info;
                    $liabilities_info=array();
                    foreach($contact_data['liabilities'] as $key => $val){
                        $liabilities_info[$key]['type_name']=$val['type_name'];
                        $liabilities_info[$key]['currency_code']=$val['currency_code'];
                        $liabilities_info[$key]['amount']=$val['amount'];
                        $liabilities_info[$key]['maturity_date']=$val['maturity_date'];
                    }
                    $field_data['liabilities']=$liabilities_info;

                    $income_info=array();
                    foreach($contact_data_income_expenses['income'] as $key => $val){
                        $income_info[$key]['type_name']=$val['income_expense_type_master'];
                        $income_info[$key]['currency_code']=$val['currency_code'];
                        $income_info[$key]['amount']=$val['amount'];
                        $income_info[$key]['description']=$val['description'];
                    }
                    $field_data['income']=$income_info;

                    $expenses_info=array();
                    foreach($contact_data_income_expenses['expenses'] as $key => $val){
                        $expenses_info[$key]['type_name']=$val['income_expense_type_master'];
                        $expenses_info[$key]['currency_code']=$val['currency_code'];
                        $expenses_info[$key]['amount']=$val['amount'];
                        $expenses_info[$key]['description']=$val['description'];
                    }
                    $field_data['expenses']=$expenses_info;
                    //echo '<pre>';print_r($field_data);exit;
                }
                else {
                    for ($st = 0; $st < count($form_field_data); $st++) {

                        /*if($contact[0]['crm_contact_type_id']!=1){
                            if(in_array($form_field_data[$st]['field_name'],$crm_contact_type_based_fields)){
                                if(trim($form_field_data[$st]['field_type'])=='json'){

                                    $value = json_decode($form_field_data[$st]['form_field_value']);
                                    for($sth=0;$sth<count($value);$sth++)
                                    {
                                        if(empty($value[$sth])){
                                            $value[$sth] = (object)$value[$sth];
                                        }
                                    }
                                    $form_field_data[$st]['form_field_value'] = $value;

                                    $field_data[] = array(
                                        'field_label' => $form_field_data[$st]['field_label'],
                                        'field_name' => $form_field_data[$st]['field_name'],
                                        'field_type' => $form_field_data[$st]['field_type'],
                                        'field_value' => $form_field_data[$st]['form_field_value']
                                    );
                                }
                                else{
                                    if($form_field_data[$st]['field_name']=='date_of_birth' && $form_field_data[$st]['form_field_value']!=''){
                                        $form_field_data[$st]['form_field_value'] = date('Y-m-d',strtotime($form_field_data[$st]['form_field_value']));
                                    }
                                    $field_data[] = array(
                                        'field_label' => $form_field_data[$st]['field_label'],
                                        'field_name' => $form_field_data[$st]['field_name'],
                                        'field_type' => $form_field_data[$st]['field_type'],
                                        'field_value' => $form_field_data[$st]['form_field_value']
                                    );
                                }
                            }
                        }
                        else{*/
                        if (trim($form_field_data[$st]['field_type']) == 'json') {

                            $value = json_decode($form_field_data[$st]['form_field_value']);
                            for ($sth = 0; $sth < count($value); $sth++) {
                                if (empty($value[$sth])) {
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_field_data[$st]['form_field_value'] = $value;

                            $field_data[] = array(
                                'field_label' => $form_field_data[$st]['field_label'],
                                'field_name' => $form_field_data[$st]['field_name'],
                                'field_type' => $form_field_data[$st]['field_type'],
                                'field_value' => $form_field_data[$st]['form_field_value']
                            );
                        } else {
                            if ($form_field_data[$st]['field_name'] == 'date_of_birth' && $form_field_data[$st]['form_field_value'] != '') {
                                $form_field_data[$st]['form_field_value'] = date('Y-m-d', strtotime($form_field_data[$st]['form_field_value']));
                            }
                            if ($form_field_data[$st]['field_name'] == 'client_prefix' && $form_field_data[$st]['form_field_value'] != '') {
                                $maritalStatus = $this->Crm_model->checkRecord('master_child', array('id_child'=>$form_field_data[$st]['form_field_value']));
                                $form_field_data[$st]['form_field_value']=$maritalStatus[0]['child_name'];
                            }
                            if ($form_field_data[$st]['field_name'] == 'client_profession' && $form_field_data[$st]['form_field_value'] != '') {
                                $maritalStatus = $this->Crm_model->checkRecord('master_child', array('id_child'=>$form_field_data[$st]['form_field_value']));
                                $form_field_data[$st]['form_field_value']=$maritalStatus[0]['child_name'];
                            }
                            if ($form_field_data[$st]['field_name'] == 'client_education' && $form_field_data[$st]['form_field_value'] != '') {
                                $maritalStatus = $this->Crm_model->checkRecord('master_child', array('id_child'=>$form_field_data[$st]['form_field_value']));
                                $form_field_data[$st]['form_field_value']=$maritalStatus[0]['child_name'];
                            }
                            if ($form_field_data[$st]['field_name'] == 'client_contact_origin' && $form_field_data[$st]['form_field_value'] != '') {
                                $maritalStatus = $this->Crm_model->checkRecord('master_child', array('id_child'=>$form_field_data[$st]['form_field_value']));
                                $form_field_data[$st]['form_field_value']=$maritalStatus[0]['child_name'];
                            }
                            $field_data[] = array(
                                'field_label' => $form_field_data[$st]['field_label'],
                                'field_name' => $form_field_data[$st]['field_name'],
                                'field_type' => $form_field_data[$st]['field_type'],
                                'field_value' => $form_field_data[$st]['form_field_value']
                            );
                        }
                        /*}*/


                    }
                }
                //echo "<pre>"; print_r($field_data); exit;
                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not
                $active_forms[$t]['id'] = $data[$s]['form'][$r]['id_form'];
                $active_forms[$t]['section_id'] = $data[$s]['id_section'];
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $contact_id, 'crm_module_type' => 'contact', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                else $data[$s]['form'][$r]['status'] = 1;

                $t++;
            }
        }

        $percentage = $this->Crm_model->getFieldPercentage($contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('details' => $contact, 'forms' => $data, 'percentage' => $percentage,'active_forms' => $active_forms));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function module_get($module_id,$crm_id = 0)
    {
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = array();
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));
        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];
            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st=0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['form_class'] = $result[$r]['form_class'];
                    $percentage = $this->Crm_model->getFormPercentage($result[$r]['id_form'],'company',$crm_id);
                    $percentage = explode(',',$percentage);
                    if($percentage[0])
                        $percentage = ($percentage[1]/$percentage[0])*100;
                    else
                        $percentage = 0;
                    $form[$st]['percentage'] = $percentage;
                    $st++;
                }
            }
            $data[$s]['form'] = $form;
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formData_get($contact_id,$form_id,$type,$info = NULL)
    {
        if($type=='relation'){
            $type='edit';
            $contact_details = $this->Crm_model->getContact($contact_id,1);
        }
        else{
            $contact_details = $this->Crm_model->getContact($contact_id);
        }
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getContactFormData($contact_id,$form_id);

        $created_date = '';
        if(!empty($contact_details))
            $created_date = $contact_details[0]['created_date_time'];

        $result = array(); $st=0; $crm_contact_type_based_fields = array('first_name','last_name','phone_number','email');
        /*If data then only have to check else no data avail*/
        /*if(count($form_fields)>0){*/
            $result['form_name'] = $form_fields[0]['form_name'];
            $result['form_key'] = $form_fields[0]['form_key'];

            for($s=0;$s<count($form_fields);$s++){
                for($r=0;$r<count($form_data);$r++){
                    if($form_fields[$s]['id_form_field']==$form_data[$r]['form_field_id']){
                        if($type=='view')
                        {
                            if($contact_details[0]['crm_contact_type_id']==1 || $contact_details[0]['crm_contact_type_id']==2){
                                if(!in_array($form_data[$r]['field_name'],$crm_contact_type_based_fields)){ break; }
                            }
                            if(trim($form_fields[$s]['field_type'])=='json'){

                                $value = json_decode($form_data[$r]['form_field_value']);
                                for($sth=0;$sth<count($value);$sth++)
                                {
                                    if(empty($value[$sth])){
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_data[$r]['form_field_value'] = $value;

                                $result['form_data'][$st] = array(
                                    'field_label' => $form_data[$r]['field_label'],
                                    'field_name' => $form_data[$r]['field_name'],
                                    'field_type' => $form_data[$r]['field_type'],
                                    'field_value' => $form_data[$r]['form_field_value']
                                );
                                $st++;
                            }
                            else{
                                if($form_data[$r]['field_name']=='date_of_birth'){
                                    if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                        $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                                }
                                $result['form_data'][$st] = array(
                                    'field_label' => $form_data[$r]['field_label'],
                                    'field_name' => $form_data[$r]['field_name'],
                                    'field_type' => $form_data[$r]['field_type'],
                                    'field_value' => $form_data[$r]['form_field_value']
                                );
                                $st++;
                            }
                        }
                        else
                        {
                            if(trim($form_fields[$s]['field_type'])=='json')
                            {
                                $value = json_decode($form_data[$r]['form_field_value']);
                                for($sth=0;$sth<count($value);$sth++)
                                {
                                    if(empty($value[$sth])){
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_data[$r]['form_field_value'] = $value;
                                $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                            }
                            else
                            {
                                if($form_data[$r]['field_name']=='date_of_birth'){
                                    if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                        $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                                }
                                $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                            }
                        }
                    }
                }
            }
        /*}*/

        $percentage = $this->Crm_model->getFieldPercentage($contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'contact_details' => $contact_details[0]));
        if(isset($info) && !empty($info)){
            return $result['data'];
        }else{
            $this->response($result, REST_Controller::HTTP_OK);
        }


    }

    public function formFields_get($form_id)
    {
        $result = $this->Crm_model->getFormFieldsByFormId($form_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formData_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo '';print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';
        if(isset($data['personal_financial_information'])){ $data['personal_financial_information'] = json_encode($data['personal_financial_information']); $update_type=1; }
        if(isset($data['other_sources_income'])){ $data['other_sources_income'] = json_encode($data['other_sources_income']); $update_type=1; }
        if(isset($data['non_business_expenditure'])){ $data['non_business_expenditure'] = json_encode($data['non_business_expenditure']); $update_type=1; }
        if(isset($data['previous_experience_institution'])){ $data['previous_experience_institution'] = json_encode($data['previous_experience_institution']); $update_type=1; }
        if(isset($data['client_type_of_id'])){ $data['client_type_of_id'] = json_encode($data['client_type_of_id']); $update_type=0; }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_contact_id'])){
            $check_data = $this->Crm_model->getContactFormData($data['crm_contact_id'],$data['form_id']);
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }
        /*echo '<pre>';print_r($data);
        echo '<br>';print_r($check_data);exit;*/
        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }

        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_fields'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            $contact_data = array();
            if(!isset($data['crm_contact_id'])){
                $contact_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['client_first_name'])){ $contact_data['first_name'] = $data['client_first_name']; }
                if(isset($data['client_last_name'])){ $contact_data['last_name'] = $data['client_last_name']; }
                if(isset($data['client_email'])){ $contact_data['email'] = $data['client_email']; }
                if(isset($data['client_phone_number'])){ $contact_data['phone_number'] = $data['client_phone_number']; }
                if(isset($data['partner_type'])){
                    $crm_contact_type = $this->Master_model->getCrmContactType(array('crm_contact_type_name' => $data['partner_type']));
                    if(!empty($crm_contact_type))
                        $contact_data['crm_contact_type_id'] = $crm_contact_type[0]['id_crm_contact_type'];
                }

                if(isset($data['client_country'])){
                    $country = $this->Master_model->getCountryByName(array('country_name' => $data['client_country']));
                    if(!empty($country))
                        $contact_data['country_id'] = $country[0]['id_country'];
                }

                $crm_contact_id = $this->Crm_model->addCrmContact($contact_data);
            }
            else{
                $crm_contact_id = $data['crm_contact_id'];
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_contact_id'] = $crm_contact_id;
                $form_data[$r]['created_date_time'] = currentDate();
            }
            if(isset($data['client_financial_credit_rating'])){ $contact_data['credit_rating'] = $data['client_financial_credit_rating']; }
            $contact_data['updated_by'] = $data['user_id'];
            $contact_data['updated_date_time'] = currentDate();
            if(isset($data['client_prefix']))
                $contact_data['prefix'] = $data['client_prefix'];
            if(!empty($contact_data)){
                $this->Crm_model->updateCrmContact($contact_data,$crm_contact_id);
            }
            $this->Crm_model->addCrmContactData($form_data);
        }

        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_contact_data'] = $check_data[$s]['id_crm_contact_data'];
                        }
                    }
                }
            }

            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_crm_contact_data'])){
                    $form_data[$sr]['crm_contact_id'] = $data['crm_contact_id'];
                    $form_data[$sr]['created_date_time'] = currentDate();
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            //check mail
            if(isset($data['client_email']) && $data['client_email']!=''){
                $email_check = $this->User_model->check_email($data['client_email'],'crm_contact',$data['company_id']);
                if(!empty($email_check)){
                    if($email_check->id_crm_contact!=$data['crm_contact_id']){
                        $result = array('status'=>FALSE,'error'=>array('client_email'=>$this->lang->line('email_duplicate')),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $contact_data['email'] = $data['client_email'];
            }

            if(!empty($new_form_fields))
                $this->Crm_model->addCrmContactData($new_form_fields);

            $this->Crm_model->updateCrmContactData($form_data,$update_type);

            $crm_contact_id = $data['crm_contact_id'];
            $contact_data = array();
            if(isset($data['client_first_name'])){ $contact_data['first_name'] = $data['client_first_name']; }
            if(isset($data['client_last_name'])){ $contact_data['last_name'] = $data['client_last_name']; }
            if(isset($data['partner_type'])){
                $crm_contact_type = $this->Master_model->getCrmContactType(array('crm_contact_type_name' => $data['partner_type']));
                if(!empty($crm_contact_type))
                    $contact_data['crm_contact_type_id'] = $crm_contact_type[0]['id_crm_contact_type'];
            }
            if(isset($data['client_financial_credit_rating'])){ $contact_data['credit_rating'] = $data['client_financial_credit_rating']; }
            if(isset($data['client_email']) && $data['client_email']!=''){
                $email_check = $this->User_model->check_email($data['client_email'],'crm_contact',$data['company_id']);

                if(!empty($email_check)){
                    if($email_check->id_crm_contact!=$crm_contact_id){
                        $result = array('status'=>FALSE,'error'=>array('client_email'=>$this->lang->line('email_duplicate')),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $contact_data['email'] = $data['client_email'];
            }
            if(isset($data['client_country'])){
                $country = $this->Master_model->getCountryByName(array('country_name' => $data['client_country']));
                if(!empty($country))
                    $contact_data['country_id'] = $country[0]['id_country'];
            }
            if(isset($data['client_phone_number'])){ $contact_data['phone_number'] = $data['client_phone_number']; }
            /*if(!empty($contact_data)){
                $contact_data['updated_by'] = $data['user_id'];
                $contact_data['updated_date_time'] = currentDate();
                $this->Crm_model->updateCrmContact($contact_data,$crm_contact_id);
            }*/
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    if($check_data[$s]['field_name']=='client_email'){
                        if($check_data[$s]['form_field_value'] != $data['client_email']){
                            $contact_data['is_verified'] = 0;
                            $contact_data['verification_date'] = currentDate();
                        }
                    }
                }
            }
            if(isset($data['prefix'])){
                $contact_data['prefix']=$data['prefix'];
            }
            if(isset($data['profession'])){
                $contact_data['profession']=$data['profession'];
            }
            if(!empty($contact_data)){
                $contact_data['updated_by'] = $data['user_id'];
                $contact_data['updated_date_time'] = currentDate();
                $this->Crm_model->updateCrmContact($contact_data,$crm_contact_id);
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($crm_contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'],'crm_module_type' => 'contact','reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));


        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_contact_id' => $data['crm_contact_id'],'project_stage_section_form_id' => $data['form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_contact_id' => $data['crm_contact_id'],'project_stage_section_form_id' => $data['form_id'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'Information saved','module_type' => 'contact','module_id' => $data['crm_contact_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>array('crm_contact_id' => $crm_contact_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_get($company_id,$module_id)
    {
        $contact = $this->Crm_model->getCompany($company_id);
        $contact['sector'] = $contact['subsector'] = '';
        $result = $this->Crm_model->getModuleDetails($module_id);
        //echo "<pre>"; print_r($result); exit;

        $data = $active_forms = array();
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_section_keys = array_values(array_unique(array_map(function ($i) { return $i['section_key']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));
        for($s=0;$s<count($unique_section_names);$s++)
        {
            /*if($unique_section_keys[$s]!='business_assessment' && $unique_section_keys[$s]!='internal_rating')
            {*/
                $data[$s]['id_section'] = $unique_id_sections[$s];
                $data[$s]['section_name'] = $unique_section_names[$s];
                $data[$s]['section_key'] = $unique_section_keys[$s];
                $form = array(); $st=0;
                for($r=0;$r<count($result);$r++){
                    if($unique_section_names[$s]==$result[$r]['section_name']){
                        $last_updated_time = $user_name = '';
                        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $company_id,'project_stage_section_form_id' => $result[$r]['id_form']));
                        if(!empty($last_update)){ $last_updated_time = $last_update[0]['updated_date_time']; $user_name = $last_update[0]['user_name']; }

                        $form[$st]['id_form'] = $result[$r]['id_form'];
                        $form[$st]['form_name'] = $result[$r]['form_name'];
                        $form[$st]['form_template'] = $result[$r]['form_template'];
                        $form[$st]['form_class'] = $result[$r]['form_class'];
                        $form[$st]['form_key'] = $result[$r]['form_key'];
                        $form[$st]['updated_date_time'] = $last_updated_time;
                        $form[$st]['updated_by'] = $user_name;
                        $st++;
                    }
                }
                $data[$s]['form'] = $form;
            /*}*/

        }
        //getting fields data
        $t = 0;
        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++)
            {
                $form_fields = $this->Crm_model->getFormFieldsByFormId($data[$s]['form'][$r]['id_form']);
                $form_data = $this->Crm_model->getCompanyFormData($company_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();

                for($sr=0;$sr<count($form_fields);$sr++){
                    $field_data[] = array(
                        'id_form_field' => $form_fields[$sr]['id_form_field'],
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '---'
                    );
                }

                $st=0;
                for($sr=0;$sr<count($form_fields);$sr++){
                    $field_data[$sr] = array(
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '--'
                    );
                    for($ru=0;$ru<count($form_data);$ru++){
                        if($form_fields[$sr]['id_form_field']==$form_data[$ru]['form_field_id']){

                            if(trim($form_fields[$sr]['field_type'])=='json'){

                                $value = json_decode($form_data[$ru]['form_field_value']);
                                for($sth=0;$sth<count($value);$sth++)
                                {
                                    if(empty($value[$sth])){
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_data[$ru]['form_field_value'] = $value;

                                if($form_data[$ru]['field_name']=='company_ownership_details'){
                                    $form_data[$ru]['form_field_value'] = $this->Crm_model->crmCompanyOwnershipListCompleteView(array('crm_company_id'=>$company_id));
                                }

                                $field_data[$sr] = array(
                                    'field_label' => $form_data[$ru]['field_label'],
                                    'field_name' => $form_data[$ru]['field_name'],
                                    'field_type' => $form_data[$ru]['field_type'],
                                    'field_value' => $form_data[$ru]['form_field_value']
                                );
                                $st++;
                            }
                            else{
                                if($form_data[$ru]['field_name']=='sector' || $form_data[$ru]['field_name']=='subsector'){
                                    $sector = $this->Master_model->getSector(array('id_sector' => $form_data[$ru]['form_field_value']));

                                    if(!empty($sector))
                                        $form_data[$ru]['form_field_value'] = $sector->sector_name;
                                }
                                else if($form_data[$ru]['field_name']=='business_start_date'){
                                    if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='')
                                        $form_data[$ru]['form_field_value'] = date('Y-m-d', strtotime($form_data[$ru]['form_field_value']));
                                }
                                else if($form_data[$ru]['field_name']=='operating_in_this_place'){
                                    if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='')
                                        $form_data[$ru]['form_field_value'] = date('Y-m-d', strtotime($form_data[$ru]['form_field_value']));
                                }
                                else if($form_data[$ru]['field_name']=='business_country'){
                                    if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='') {
                                        $country = $this->Master_model->getCountry(array('id_country' => $form_data[$ru]['form_field_value']));
                                        $form_data[$ru]['form_field_value'] = $country->country_name;
                                    }
                                }


                                $field_data[$sr] = array(
                                    'field_label' => $form_data[$ru]['field_label'],
                                    'field_name' => $form_data[$ru]['field_name'],
                                    'field_type' => $form_data[$ru]['field_type'],
                                    'field_value' => $form_data[$ru]['form_field_value']
                                );
                                $st++;
                            }
                        }
                    }
                }

                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not

                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $company_id, 'crm_module_type' => 'company', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                else $data[$s]['form'][$r]['status'] = 1; $t++;
                if($data[$s]['form'][$r]['form_key']=='exposure_with_eadb') {
                    $facilities = $this->Facility_model->getCompanyFacilities(array('crm_company_id' => $company_id));
                    if(!empty($facilities)){
                        $data[$s]['form'][$r]['status'] = 1;
                    }
                }
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //rating
        /*$rating = $this->getCrmCompanyRiskRating($company_id,$contact[0]['company_id']);
        $rating = explode('@@@',$rating);

        if($rating[1]){ $contact[0]['rating'] = (float)$rating[0]; }
        else{ $contact[0]['rating'] = '---'; }
        $contact[0]['grade'] = $this->companyRiskGradeCalculation($contact[0]['rating']);*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('details' => $contact, 'forms' => $data, 'percentage' => ceil($percentage), 'active_forms' => $active_forms));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyTabInfo_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company = $this->Company_model->getCompanyById($data['company_id']);
        $pipeline = $this->Crm_model->getCompanyTabsCount(array('company_id' => $data['company_id'], 'status' => 'Pending'));
        $submitted = $this->Crm_model->getCompanyTabsCount(array('company_id' => $data['company_id'], 'status' => 'Pre-approved'));
        $approved = $this->Crm_model->getCompanyTabsCount(array('company_id' => $data['company_id'], 'status' => 'Approved'));
        $monitoring = $this->Crm_model->getCompanyTabsCount(array('company_id' => $data['company_id'], 'status' => 'Monitoring'));

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array(
            'pipeline' =>array('DEALS' => $pipeline[0]['records'],$company->currency_code => $pipeline[0]['amount']),
            'submitted' =>array('DEALS' => $submitted[0]['records'],$company->currency_code => $submitted[0]['amount']),
            'approved' =>array('DEALS' => $approved[0]['records'],$company->currency_code => $approved[0]['amount']),
            'monitoring' =>array('DEALS' => $monitoring[0]['records'],$company->currency_code => $monitoring[0]['amount']),
        ));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyList_get()
    {
        $data = $this->input->get();


        if(isset($data['crm_project_id'])){
            $project = $this->Crm_model->getProject($data['crm_project_id']);
            $data['company_id'] = $project[0]['company_id'];
        }
        $result = $this->Crm_model->getCompanyList($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result[$s]['id_crm_project']);
            if(!empty($result[$s]['amount']))
                $result[$s]['amount'] = $result[$s]['amount'][0]['amount'];
            else
                $result[$s]['amount'] = 0;
        }

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = $this->Crm_model->getCompanyListCount($data);
        $all_records =$total_records[0]['total'];
        unset($data['search_key']);
        //all records
        if(isset($data['created_by'])){ $created_by = $data['created_by']; unset($data['created_by']); }
        $total_records = $this->Crm_model->getCompanyListCount($data);
        $all_companies = $total_records[0]['total'];

        //created by user records
        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $my_companies = $this->Crm_model->getCompanyListCount($data);
        $my_companies = $my_companies[0]['total'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $all_records,'all_companies' => $all_companies,'my_companies' => $my_companies));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function crmCompanyList_get()
    {
        $data = $this->input->get();


        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $conditions = $result = array();
        $category = array(
            array('type' => 'pipeline','status' => 'Pending'),
            array('type' => 'submitted','status' => 'pre-approved'),
            array('type' => 'approved','status' => 'Approved'),
            array('type' => 'monitoring','status' => 'monitoring')
        );
        $conditions['company_id'] =$data['company_id'];
        if(isset($data['search']) && !empty($data['search'])){ $data['search'] = (array)json_decode($data['search']); }

        if(isset($data['search']['category']))
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            if(isset($data['offset']))
                $conditions['offset'] = $data['offset'];
            else
                $conditions['offset'] = 0;

            if(isset($data['limit']))
                $conditions['limit'] = $data['limit'];
            else
                $conditions['limit'] = 10;

            for($s=0;$s<count($category);$s++)
            {
                $result[$category[$s]['type']]['data'] = array();
                $result[$category[$s]['type']]['count'] = 0;

                if($category[$s]['type']==$data['search']['category'])
                {
                    $conditions['status'] = $category[$s]['status'];

                    if($category[$s]['status']=='Pending') {
                        $result[$category[$s]['type']]['data'] = $this->Crm_model->getPipelineCompanies($conditions);
                        $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
                        $result[$category[$s]['type']]['count'] = $total[0]['total'];
                    }
                    else{
                        $result[$category[$s]['type']]['data'] = $this->Crm_model->getProjectCompanies($conditions);
                        $total = $this->Crm_model->getProjectCompaniesCount($conditions);
                        $result[$category[$s]['type']]['count'] = $total[0]['total'];
                    }
                }
            }
        }
        else
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];
            $conditions['offset'] = 0;
            $conditions['limit'] = 10;

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            $conditions['status'] = 'Pending';
            $result['pipeline']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['pipeline']['count'] = $total[0]['total'];

            $conditions['limit'] = 10;
            $conditions['status'] = 'Pre-approved';
            $result['submitted']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['submitted']['count'] = $total[0]['total'];

            $conditions['status'] = 'Approved';
            $result['approved']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['approved']['count'] = $total[0]['total'];

            $conditions['status'] = 'Monitoring';
            $result['monitoring']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['monitoring']['count'] = $total[0]['total'];
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }*/

    public function crmCompanyList_get()
    {
        $data = $this->input->get();


        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $conditions = $result = array();
        $category = array(
            array('type' => 'pipeline','status' => 'new'),
            array('type' => 'submitted','status' => 'pre-approved'),
            array('type' => 'approved','status' => 'Approved'),
            array('type' => 'monitoring','status' => 'monitoring')
        );
        $conditions['company_id'] =$data['company_id'];
        if(isset($data['search']) && !empty($data['search'])){ $data['search'] = (array)json_decode($data['search']); }

        if(isset($data['search']['category']))
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            if(isset($data['offset']))
                $conditions['offset'] = $data['offset'];
            else
                $conditions['offset'] = 0;

            if(isset($data['limit']))
                $conditions['limit'] = $data['limit'];
            else
                $conditions['limit'] = 10;

            for($s=0;$s<count($category);$s++)
            {
                $result[$category[$s]['type']]['data'] = array();
                $result[$category[$s]['type']]['count'] = 0;

                if($category[$s]['type']==$data['search']['category'])
                {
                    $conditions['status'] = $category[$s]['status'];

                    if($category[$s]['status']=='new') {
                        $result[$category[$s]['type']]['data'] = $this->Crm_model->getPipelineCompanies($conditions);
                        $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
                        $result[$category[$s]['type']]['count'] = $total[0]['total'];
                    }
                    else{
                        $result[$category[$s]['type']]['data'] = $this->Crm_model->getProjectCompanies($conditions);
                        $total = $this->Crm_model->getProjectCompaniesCount($conditions);
                        $result[$category[$s]['type']]['count'] = $total[0]['total'];
                    }
                }
            }
        }
        else
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];
            $conditions['offset'] = 0;
            $conditions['limit'] = 10;

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            $conditions['status'] = 'new';
            $result['pipeline']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['pipeline']['count'] = $total[0]['total'];

            $conditions['limit'] = 10;
            $conditions['status'] = 'Pre-approved';
            $result['submitted']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['submitted']['count'] = $total[0]['total'];

            $conditions['status'] = 'Approved';
            $result['approved']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['approved']['count'] = $total[0]['total'];

            $conditions['status'] = 'Monitoring';
            $result['monitoring']['data'] = $this->Crm_model->getPipelineCompanies($conditions);
            $total = $this->Crm_model->getPipelineCompaniesCount($conditions);
            $result['monitoring']['count'] = $total[0]['total'];
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function companyForm_get($company_id,$form_id,$type)
    {
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getCompanyFormData($company_id,$form_id);
        $company_details = $this->Crm_model->getCompany($company_id);
        $created_date = '';

        $result = array(); $st=0;
        $result['form_name'] = $form_fields[0]['form_name'];
        for($s=0;$s<count($form_fields);$s++){
            for($r=0;$r<count($form_data);$r++){
                if($form_fields[$s]['id_form_field']==$form_data[$r]['form_field_id']){
                    if($type=='view')
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){

                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;

                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='sector' || $form_data[$r]['field_name']=='subsector'){
                                $sector = $this->Master_model->getSector(array('id_sector' => $form_data[$r]['form_field_value']));
                                if(!empty($sector))
                                    $form_data[$r]['form_field_value'] = $sector->sector_name;
                            }
                            else if($form_data[$r]['field_name']=='business_start_date'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='operating_in_this_place'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='business_country'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='') {
                                    $country = $this->Master_model->getCountry(array('id_country' => $form_data[$r]['form_field_value']));
                                    $form_data[$r]['form_field_value'] = $country->country_name;
                                }
                            }
                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                    }
                    else
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){

                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;
                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='business_start_date'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='operating_in_this_place'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }

                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                    }

                }
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'company_details' => $company_details[0]));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';

        if(isset($data['assessment_questions'])){ $data['assessment_questions'] = json_encode($data['assessment_questions']); $update_type=1; }
        if(isset($data['risks_from_suppliers'])){ $data['risks_from_suppliers'] = json_encode($data['risks_from_suppliers']); $update_type=1; }
        if(isset($data['bargaining_buyers'])){ $data['bargaining_buyers'] = json_encode($data['bargaining_buyers']); $update_type=1; }
        if(isset($data['threats_new_entrants'])){ $data['threats_new_entrants'] = json_encode($data['threats_new_entrants']); $update_type=1; }
        if(isset($data['cash_inflow'])){ $data['cash_inflow'] = json_encode($data['cash_inflow']); $update_type=1; }
        if(isset($data['cash_outflow'])){ $data['cash_outflow'] = json_encode($data['cash_outflow']); $update_type=1; }
        if(isset($data['financial_indicators'])){ $data['financial_indicators'] = json_encode($data['financial_indicators']); $update_type=1; }
        if(isset($data['threats_from_subsitutes'])){ $data['threats_from_subsitutes'] = json_encode($data['threats_from_subsitutes']); $update_type=1; }
        if(isset($data['ownership_details'])){ $data['ownership_details'] = json_encode($data['ownership_details']);  }
        if(isset($data['company_social_networks'])){ $data['company_social_networks'] = json_encode($data['company_social_networks']);  }
        if(isset($data['business_contact_type'])){ $data['business_contact_type'] = json_encode($data['business_contact_type']);  }
        if(isset($data['family_projected_development'])){ $data['family_projected_development'] = json_encode($data['family_projected_development']);  }
        if(isset($data['company_ownership_details'])){ $data['company_ownership_details'] = json_encode($data['company_ownership_details']);  }
        if(isset($data['company_nature_of_business'])){ $data['company_nature_of_business'] = json_encode($data['company_nature_of_business']);  }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_company_id'])){
            $check_data = $this->Crm_model->getCompanyFormData($data['crm_company_id'],$data['form_id']);
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_fields'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_company_data = array();//for updating sector and subsector for project which is assigned to this company

        if(isset($data['subsector']) && ($data['subsector']==NULL || trim($data['subsector'])=='')){
            $data['subsector']=0;
        }
        $main_sector_id = $sub_sector_id = 0;
        if(isset($data['company_sector'])){
            $sector = $this->Master_model->getSectorByName(array('sector_name' => $data['company_sector']));
            if(!empty($sector))
                $main_sector_id = $sector[0]['id_sector'];
        }
        if(isset($data['company_subsector'])){
            $sector = $this->Master_model->getSectorByName(array('sector_name' => $data['company_subsector']));
            if(!empty($sector))
                $sub_sector_id = $sector[0]['id_sector'];
            else{ $sub_sector_id = ''; }
        }
        if($type=='insert')
        {
            if(!isset($data['crm_company_id'])){
                $company_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['company_borrower_name']) && $data['company_borrower_name']!=''){
                    $company_data['company_name'] = $data['company_borrower_name'];
                }
                if(isset($data['company_sector'])){
                    $company_data['sector_id'] = $main_sector_id;
                    $project_company_data['project_main_sector_id'] = $main_sector_id;
                }
                if(isset($data['company_subsector'])){
                    $company_data['sub_sector_id'] = $sub_sector_id;
                    $project_company_data['project_sub_sector_id'] = $sub_sector_id;
                }
                if(isset($data['company_description'])){
                    $company_data['company_description'] = $data['company_description'];
                }
                if(isset($data['company_phone_number'])){
                    $company_data['phone_number'] = $data['company_phone_number'];
                }
                if(isset($data['company_country'])){
                    $country = $this->Master_model->getCountryByName(array('country_name' => $data['company_country']));
                    if(!empty($country))
                        $company_data['country_id'] = $country[0]['id_country'];
                }

                $company_data['created_date_time'] = currentDate();
                $crm_company_id = $this->Crm_model->addCrmCompany($company_data);
            }
            else{
                $crm_company_id = $data['crm_company_id'];
                $company_data = array();
                if(isset($data['company_borrower_name']) && $data['company_borrower_name']!=''){
                    $company_data['company_name'] = $data['company_borrower_name'];
                }
                if(isset($data['company_sector'])){
                    $company_data['sector_id'] = $main_sector_id;
                    $project_company_data['project_main_sector_id'] = $main_sector_id;
                }
                if(isset($data['company_subsector'])){
                    $company_data['sub_sector_id'] = $sub_sector_id;
                    $project_company_data['project_sub_sector_id'] = $sub_sector_id;
                }
                if(isset($data['company_description'])){
                    $company_data['company_description'] = $data['company_description'];
                }
                if(isset($data['company_phone_number'])){
                    $company_data['phone_number'] = $data['company_phone_number'];
                }
                if(isset($data['company_email']) && $data['company_email']!=''){
                    $email_check = $this->User_model->check_email($data['company_email'],'crm_company',$data['company_id']);
                    if(!empty($email_check)){
                        if($email_check->id_crm_company!=$data['crm_company_id']){
                            $result = array('status'=>FALSE,'error'=>array('company_email'=>'Email already exists'),'data'=>'');
                            $this->response($result, REST_Controller::HTTP_OK);
                        }
                    }
                    $company_data['email'] = $data['company_email'];
                }
                if(isset($data['company_country'])){
                    $country = $this->Master_model->getCountryByName(array('country_name' => $data['company_country']));
                    if(!empty($country))
                        $company_data['country_id'] = $country[0]['id_country'];
                }
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_company_id'] = $crm_company_id;
                $form_data[$r]['created_date_time'] = currentDate();
            }
            $this->Crm_model->addCrmCompanyData($form_data);
        }
        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_company_data'] = $check_data[$s]['id_crm_company_data'];
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_crm_company_data'])){
                    $form_data[$sr]['crm_company_id'] = $data['crm_company_id'];
                    $form_data[$sr]['created_date_time'] = currentDate();
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }

            if(!empty($new_form_fields))
                $this->Crm_model->addCrmCompanyData($new_form_fields);

            $this->Crm_model->updateCrmCompanyData($form_data,$update_type);
            $crm_company_id = $data['crm_company_id'];
            $company_data = array();
            if(isset($data['company_borrower_name']) && $data['company_borrower_name']!=''){
                $company_data['company_name'] = $data['company_borrower_name'];
            }
            if(isset($data['company_sector'])){
                $company_data['sector_id'] = $main_sector_id;
                $project_company_data['project_main_sector_id'] = $main_sector_id;
            }
            if(isset($data['company_subsector'])){
                $company_data['sub_sector_id'] = $sub_sector_id;
                $project_company_data['project_sub_sector_id'] = $sub_sector_id;
            }
            if(isset($data['company_description'])){
                $company_data['company_description'] = $data['company_description'];
            }
            if(isset($data['company_phone_number'])){
                $company_data['phone_number'] = $data['company_phone_number'];
            }
            if(isset($data['company_email']) && $data['company_email']!=''){
                $email_check = $this->User_model->check_email($data['company_email'],'crm_company',$data['company_id']);
                if(!empty($email_check)){
                    if($email_check->id_crm_company!=$data['crm_company_id']){
                        $result = array('status'=>FALSE,'error'=>array('company_email'=>'Email already exists'),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $company_data['email'] = $data['company_email'];
            }
            if(isset($data['company_country'])){
                $country = $this->Master_model->getCountryByName(array('country_name' => $data['company_country']));
                if(!empty($country))
                    $company_data['country_id'] = $country[0]['id_country'];
            }
        }

        if(isset($data['company_credit_rating'])){ $company_data['credit_rating'] = $data['company_credit_rating']; }
        $company_data['updated_by'] = $data['user_id'];
        $company_data['updated_date_time'] = currentDate();

        if(!empty($check_data)){
            for($s=0;$s<count($check_data);$s++){
                if($check_data[$s]['field_name']=='company_email'){
                    if($check_data[$s]['form_field_value'] != $data['company_email']){
                        $company_data['is_verified'] = 0;
                        $company_data['verification_date'] = currentDate();
                    }
                }
            }
        }
        if(!empty($company_data)){
            $this->Crm_model->updateCrmcompany($company_data,$crm_company_id);
        }
        if(!empty($project_company_data)){
            $primary_project = $this->Crm_model->getProjectCompany(array('crm_company_id' => $crm_company_id, 'company_type_id' => 1));

            if(!empty($primary_project)){
                $this->Crm_model->updateProject($project_company_data,$primary_project[0]['crm_project_id']);
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($crm_company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $data['form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $data['form_id'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));


        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'Information saved','module_type' => 'company','module_id' =>  $data['crm_company_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_add'), 'data'=>array('crm_company_id' => $crm_company_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_crm_contact',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_contact_id = $data['id_crm_contact'];
        $data['updated_date_time'] = currentDate();
        $this->Crm_model->updateCrmContact($data,$crm_contact_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function quickContact_post($data=NULL)
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        if(empty($data)){
            $data = $this->input->post();
            $post_type = '';
        }else{
            $post_type = 'personal_info';
        }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $prefixRules               = array(
            'required'=> $this->lang->line('prefix_req')
        );
        $firstNameRules               = array(
            'required'=> $this->lang->line('first_name_req'),
            'max_len-100' => $this->lang->line('first_name_len'),
        );
        $lastNameRules               = array(
            'required'=> $this->lang->line('last_name_req'),
            'max_len-100' => $this->lang->line('last_name_len'),
        );
        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            'numeric' =>  $this->lang->line('phone_num_num'),
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-20' => $this->lang->line('phone_num_max_len'),
        );
        $save_create = isset($data['save_create'])?$data['save_create']:1;

        $this->form_validator->add_rules('client_first_name',$firstNameRules);
        //$this->form_validator->add_rules('client_prefix',$prefixRules);
        if($save_create == 1 || ($save_create==0 && isset($data['client_email']) && $data['client_email']!='')){
            $this->form_validator->add_rules('client_email', $emailRules);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id')));
        $this->form_validator->add_rules('crm_contact_type_id', array('required'=> $this->lang->line('contact_type')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $contact_data = array();
        $contact_data['company_id'] = $data['company_id'];
        $contact_data['created_by'] = $data['created_by'];
        $contact_data['crm_contact_type_id'] = $data['crm_contact_type_id'];

        $contact_data['first_name'] = $data['first_name'] = $data['client_first_name'];
        $contact_data['last_name'] = $data['last_name'] = $contact_data['phone_number'] = $data['phone_number'] = '';
        if(isset($data['client_last_name']))
            $contact_data['last_name'] = $data['last_name'] = $data['client_last_name'];
        $contact_data['email'] = $data['email'] = isset($data['client_email'])?$data['client_email']:'';
        if(isset($data['client_phone_number']))
            $contact_data['phone_number'] = $data['phone_number'] = $data['client_phone_number'];
        if(isset($data['client_prefix']))
            $contact_data['prefix'] = $data['client_prefix'];

        if($save_create == 1 || ($save_create==0 && isset($data['client_email']) && $data['client_email']!='')){
            $email_check = $this->User_model->check_email($data['email'],'crm_contact',$data['company_id']);
            if(!empty($email_check)){
                $result = array('status'=>FALSE,'error'=>array('email'=>$this->lang->line('email_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            if(!file_exists('uploads/'.$data['company_id'])){
                mkdir('uploads/'.$data['company_id']);
            }
            $folder = $data['company_id'];
            $path='uploads/';

            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,$folder,'image');
            $contact_data['profile_image'] = $data['profile_image'] = $imageName;
        }
        else
        {
            $contact_data['profile_image'] = $data['profile_image'] = '';
        }
        //adding new company
        if(isset($data['crm_company_name']) && $data['crm_company_name']!=''){
            $company = array(
                'company_id'   => $data['company_id'],
                'created_by'   => $data['created_by'],
                'company_name' => $data['crm_company_name'],
                'created_date_time' => currentDate()
            );
            $contact_data['crm_company_id'] = $data['crm_company_id'] = $this->Crm_model->addCrmCompany($company);
            unset($data['crm_company_name']);
        }

        $map_data = array();
        if(isset($data['crm_company_id']) && $data['crm_company_id']!='' && isset($data['crm_company_designation_id']) && $data['crm_company_designation_id']!='')
        {
            $map_data = array(
                'crm_company_id'                => $data['crm_company_id'],
                'crm_company_designation_id'    => $data['crm_company_designation_id'],
                'created_by'                    => $data['created_by'],
                'is_primary_company'            => '1'
            );

            unset($data['crm_company_id']);
            unset($data['crm_company_designation_id']);
        }
        $data['updated_date_time'] = $contact_data['updated_date_time'] = currentDate();
        $data['created_date_time'] = $contact_data['created_date_time'] = currentDate();
        //echo "<pre>"; print_r($contact_data); exit;
        if($save_create != 1){
            $contact_data['contact_status']=0;
        }
        $crm_contact_id = $this->Crm_model->addQuickContact($contact_data);

        if(!empty($map_data))
        {
            $map_data['crm_contact_id'] = $crm_contact_id;
            $this->Crm_model->mapModule($map_data);
        }
        $data['partner_type'] = $data['crm_contact_type_id'];
        $contact_type = $this->Master_model->getCrmContactType(array('id_crm_contact_type' => $data['crm_contact_type_id']));
        if(!empty($contact_type))
            $data['partner_type'] = $contact_type[0]['crm_contact_type_name'];
        $data_keys = array_keys($data);
        //$form_key_details = $this->Crm_model->getFormByFormKeys($data_keys);
        //$forms = $this->Crm_model->getModuleDetails(1);
        $st=0; $form_data = array();
        //$form_fields = $this->Crm_model->getFormFieldsByFormId($forms[0]['id_form']);
        $data_keys['crm_module_id'] = 1;
        $form_fields = $this->Crm_model->getFormByFormKeys($data_keys);
        unset($data_keys['crm_module_id']);
        $form_ids = array_values(array_unique(array_map(function($i){ return $i['form_id']; },$form_fields)));
        for($sr=0;$sr<count($form_ids);$sr++)
        {
            $form_data = array();
            $form_fields = $this->Crm_model->getFormFieldsByFormId($form_ids[$sr]);
            for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
                for($r=0;$r<count($data_keys);$r++){

                        if ($form_fields[$s]['field_name'] == $data_keys[$r]) {
                            $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                            $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                            $st++;
                            $check_field = 1;
                        }

                }
                if($check_field==0){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    if($form_fields[$s]['field_name']=='client_gender'){
                        $form_data[$st]['form_field_value'] = 'Male';
                    }
                    else{
                        $form_data[$st]['form_field_value'] = '';
                    }
                    $st++;
                }
            }
            //echo "<pre>"; print_r($form_data);
            $form_data = array_values($form_data);
            for($r=0;$r<count($form_data);$r++){
                if(!empty($form_data[$r])) {
                    $form_data[$r]['crm_contact_id'] = $crm_contact_id;
                    $form_data[$r]['created_date_time'] = currentDate();
                }
            }
            //echo "<pre>"; print_r($form_data);
            $this->Crm_model->addCrmContactData($form_data);
        }

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Contact','activity_template' => 'Contact created','module_type' => 'contact','module_id' => $crm_contact_id, 'activity_type' => 'created','activity_reference_id' => $crm_contact_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $crm_contact_id));

        if($post_type == 'personal_info'){
            return $result;
        }else{
            $this->response($result, REST_Controller::HTTP_OK);
        }

    }

    public function quickCompany_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $emailRules = array(
            'required'=> $this->lang->line('email_req'),
            'valid_email' => $this->lang->line('email_invalid')
        );
        $phoneRules  = array(
            'required' => $this->lang->line('phone_num_req'),
            /*'numeric' =>  $this->lang->line('phone_num_num'),*/
            'min_len-7' => $this->lang->line('phone_num_min_len'),
            'max_len-20' => $this->lang->line('phone_num_max_len'),
        );

        $this->form_validator->add_rules('company_borrower_name',array('required'=> $this->lang->line('company_name_req')));
        $this->form_validator->add_rules('company_email', $emailRules);
        $this->form_validator->add_rules('company_phone_number', $phoneRules);
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_data = array();
        $data_keys = array_keys($data);
        $company_data['company_name'] = $data['comapny_name'] = $data['company_borrower_name'];
        $company_data['email'] = $data['email'] = $data['company_email'];
        $company_data['phone_number'] = $data['phone_number'] = $data['company_phone_number'];
        $company_data['company_id'] = $data['company_id'];
        $company_data['created_by'] = $data['created_by'];
        $email_check = $this->User_model->check_email($data['email'],'crm_company',$data['company_id']);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email'=>$this->lang->line('email_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['created_date_time'] = currentDate();
        $crm_company_id = $this->Crm_model->addQuickCompany($company_data);

        if(isset($data['company_name'])){
            $data['business_name'] = $data['company_name'];
        }

        $data_keys['crm_module_id'] = 2;
        $form_fields = $this->Crm_model->getFormByFormKeys($data_keys);
        $form_ids = array_values(array_unique(array_map(function($i){ return $i['form_id']; },$form_fields)));

        //$forms = $this->Crm_model->getModuleDetails(2);
        $st=0;
        for($str=0;$str<count($form_ids);$str++)
        {
            $form_data = array();
            $form_fields = $this->Crm_model->getFormFieldsByFormId($form_ids[$str]);
            for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
                for($r=0;$r<count($data_keys);$r++){
                    if(isset($data_keys[$r])) {
                        if ($form_fields[$s]['field_name'] == $data_keys[$r]) {
                            $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                            $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                            $st++;
                            $check_field = 1;
                        }
                    }
                }
                if($check_field==0){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = '';
                    $st++;
                }
            }
            $form_data = array_values($form_data);

            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_company_id'] = $crm_company_id;
                $form_data[$r]['created_date_time'] = currentDate();
            }
            $this->Crm_model->addCrmCompanyData($form_data);

        }

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Company','activity_template' => 'Company created','module_type' => 'company','module_id' => $crm_company_id, 'activity_type' => 'created','activity_reference_id' => $crm_company_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('company_add'), 'data'=>array('crm_company_id' => $crm_company_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('group_id', array('required'=> $this->lang->line('group_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $id_project_stage_section_form = 0;
        if(isset($data['id_project_stage_section_form'])){
            $id_project_stage_section_form = $data['id_project_stage_section_form'];
        }

        $company_id = $data['company_id'];
        if(!file_exists('uploads/'.$company_id)){
            mkdir('uploads/'.$company_id);
        }

        if($data['module_id']==1){ $data['module_type']='contact'; }
        if($data['module_id']==2){ $data['module_type']='company'; }
        if($data['module_id']==3){ $data['module_type']='project'; }
        if($data['module_id']==4){ $data['module_type']='collateral'; }

        if(isset($_FILES) && !empty($_FILES['file']['name']['excel']) && $data['form_key']=='balance_sheet'){
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(in_array($ext,$allowed) ) {
                $this->financialExcel_post($data);
            }
        }
        if(isset($_FILES) && (!empty($_FILES['file']['name']['attachment']) || !empty($_FILES['file']['name']['excel']))){
            $fl_type='attachment';
            if(!empty($_FILES['file']['name']['attachment']))
                $fl_type='attachment';
            if(!empty($_FILES['file']['name']['excel']))
                $fl_type='excel';
            //$_FILES['file']['type'][$fl_type] = $_FILES['file']['type']['excel'];
            //$_FILES['file']['tmp_name'][$fl_type] = $_FILES['file']['tmp_name']['excel'];

            $file_name = $_FILES['file']['name'][$fl_type];
            $mediaType = $_FILES['file']['type'][$fl_type];
            $imageName = doUpload($_FILES['file']['tmp_name'][$fl_type],$_FILES['file']['name'][$fl_type],'uploads/',$company_id);


            if(!isset($data['module_type'])){ $data['module_type']=''; }
            if(!isset($data['form_key'])){ $data['form_key']=''; }
            if(!isset($data['field_key'])){ $data['field_key']=''; }
            if(!isset($data['description'])){ $data['description']=''; }
            if(!isset($data['document_type'])){ $data['document_type']=0; }

            if(isset($data['document_id']))
            {
                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id'],'crm_document_id' => $data['document_id'],'created_date_time' => currentDate());
                $document_id = $this->Crm_model->addDocumentVersion($document);
            }
            else
            {
                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id'],'module_type' => $data['module_type'],'form_key' => $data['form_key'],'field_key' => $data['field_key'],'created_date_time' => currentDate());
                $document_id = $this->Crm_model->addDocument($document);

                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id'],'crm_document_id' => $document_id,'created_date_time' => currentDate());
                $document_id = $this->Crm_model->addDocumentVersion($document);
            }
            $data['document_id'] = $document_id;
            //saving activity for activity table --start
            /*$module = $this->Crm_model->getModuleByDocumentType($data['document_type']);
            if(!empty($module)){
                if($data['module_id']==1){ $module_type = 'contact'; }
                else if($data['module_id']==2){ $module_type = 'company'; }
                else if($data['module_id']==3){ $module_type = 'project'; }*/
                $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
                $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
                $activity = array(
                    'activity_name' => 'Document',
                    'activity_type' => 'attachment',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $document_id,
                    'module_type' => $data['module_type'],
                    'module_id' => $data['group_id'],
                    'created_by' => $data['user_id'],
                    'created_date_time' => currentDate()
                );
                $this->Activity_model->addActivity($activity);
            //}
            //saving activity --end
        }



        //adding activity
        if($id_project_stage_section_form) {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['group_id'], 'crm_module_type' => $data['module_type'], 'reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['group_id'], 'crm_module_type' => $data['module_type'], 'reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(),'updated_date_time' => currentDate()));

            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['group_id'], 'project_stage_section_form_id' => $id_project_stage_section_form));
            //echo "<pre>"; print_r($last_update); exit;
            if (empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['group_id'], 'project_stage_section_form_id' => $id_project_stage_section_form, 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
        }
        else if(isset($data['form_key'])){
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if(!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['group_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['group_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['group_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                //echo "<pre>"; print_r($last_update); exit;
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['group_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('document_add'), 'data'=>array('document_id' => $document_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_get()
    {
        $data = $this->input->get();
        //echo '<pre>';print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('group_id', array('required'=> $this->lang->line('group_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['module_id']==1){ $data['module_type'] = 'contact'; }
        if($data['module_id']==2){ $data['module_type'] = 'company'; }
        if($data['module_id']==3){ $data['module_type'] = 'project'; }
        if($data['module_id']==4){ $data['module_type'] = 'collateral'; unset($data['module_id']); }

        $result=$this->Crm_model->getDocument($data);
        //echo $this->db->last_query();exit;//echo "<pre>"; print_r($result); exit;
        $versions_data = array();
        for($m=0;$m<count($result);$m++)
        {
            $result[$m]['full_path']=getImageUrl($result[$m]['document_source'],'');
            $result[$m]['size']=get_file_info('uploads/'.$result[$m]['document_source'], 'size');
            $versions = $this->Crm_model->getDocumentVersion(array('crm_document_id' => $result[$m]['id_crm_document']));
            for($n=0;$n<count($versions);$n++)
            {
                $versions[$n]['full_path']=getImageUrl($versions[$n]['document_source'],'');
                $versions[$n]['document_type_name']=$result[$m]['document_type_name'];
                $versions[$n]['size']=get_file_info('uploads/'.$versions[$n]['document_source'], 'size');
                $versions[$n]['id_crm_document'] = $versions[$n]['crm_document_id'];
                if($n==0){
                    $versions_data[$versions[$n]['crm_document_id']] = $versions[$n];
                }
                else{
                    $versions_data[$versions[$n]['crm_document_id']]['versions'][] = $versions[$n];
                }
            }

            $result[$m]['versions']= $versions;
            /*if(!empty($versions)) {
                $versions_data[] = $versions;
            }*/
        }
        $versions_data = array_values($versions_data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$versions_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('document_id', array('required'=> $this->lang->line('document_id_req')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*if($data['type']=='version'){
            //deleting documents for company folder
            $document_details = $this->Crm_model->getDocumentVersion(array('id_crm_document_version' => $data['document_id']));
            if(isset($document_details[0]['document_source']) && file_exists('uploads/'.$document_details[0]['document_source'])){
                unlink('uploads/'.$document_details[0]['document_source']);
            }
            $this->Crm_model->deleteDocumentVersion(array('id_crm_document_version' => $data['document_id']));
        }
        else{
            //deleting documents for company folder
            $document_details = $this->Crm_model->getDocumentVersion(array('crm_document_id' => $data['document_id']));

            for($st=0;$st<count($document_details);$st++){
                if(isset($document_details[$st]['document_source']) && file_exists('uploads/'.$document_details[$st]['document_source'])){
                    unlink('uploads/'.$document_details[$st]['document_source']);
                }
            }
            $document_details = $this->Crm_model->getDocument(array('id_crm_document' => $data['document_id']));
            if(file_exists('uploads/'.$document_details[0]['document_source'])){
                unlink('uploads/'.$document_details[0]['document_source']);
            }

            $this->Crm_model->deleteDocumentVersion(array('crm_document_id' => $data['document_id']));
            $this->Crm_model->deleteDocument(array('id_crm_document' => $data['document_id']));
        }*/

        $document_details = $this->Crm_model->deleteDocumentVersion(array('crm_document_id' => $data['document_id']));

        if(isset($document_details[0]['document_source']) && file_exists('uploads/'.$document_details[0]['document_source'])){
            unlink('uploads/'.$document_details[0]['document_source']);
        }
        $this->Crm_model->deleteDocument(array('id_crm_document' => $data['document_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function documentType_get()
    {
        $data = $this->input->get();
        $documents = $this->Crm_model->getDocumentType($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$documents);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function quickProject_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('project_title',array('required'=> $this->lang->line('project_title_req')));
        $this->form_validator->add_rules('project_description', array('required'=> $this->lang->line('project_description_req')));
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['assigned_to'] = $data['created_by'];
        $crm_company_id = $data['crm_company_id'];
        unset($data['crm_company_id']);

       /* $crm_company_details = $this->Crm_model->getCompany($crm_company_id);
        $data['project_main_sector_id'] = $crm_company_details[0]['sector_id'];
        $data['project_sub_sector_id'] = $crm_company_details[0]['sub_sector_id'];*/
        $data['created_date_time'] = currentDate();
        $data['updated_date_time'] = currentDate();
        $data['project_status'] = 'new';
        $company_user = $this->User_model->getCompanyUser($data['created_by']);
        if(isset($company_user))
        $data['country_id'] = $company_user->branch_country_id;
        //echo "<pre>"; print_r($company_user); exit;
        $crm_project_id = $this->Crm_model->addQuickProject($data);

        //adding crm project company(assigning company to project)
        $this->Crm_model->addProjectCompany(array(
            'created_by' => $data['created_by'],
            'crm_project_id' => $crm_project_id,
            'crm_company_id' =>$crm_company_id,
            'company_type_id' => 1,
            'project_company_status' => 1,
            'created_date_time' => currentDate()
        ));

        $data_keys = array_keys($data);
        $stages = $this->Project_model->getStage();
        if(!empty($stages)){ $project_stage_id = $stages[0]['id_project_stage']; }
        else{ $project_stage_id = 0; }
        $data_keys['stage_id'] = $project_stage_id;
        $form_fields = $this->Project_model->getFormFieldsByKeys($data_keys);
        $section_form_id = array_values(array_unique(array_map(function($i){ return $i['project_stage_section_form_id']; },$form_fields)));
        //echo "<pre>"; print_r($section_form_id); exit;

        $project_stage_info_id = $this->Project_model->addProjectStageInfo(array(
            'project_id' => $crm_project_id,
            'stage_id' => $project_stage_id,
            'version_number' => 1,
            'project_stage_status' => 'in progress',
            'created_by' => $data['created_by'],
            'start_date_time' => currentDate()
        ));
        $this->Crm_model->projectWorkflowDump(['project_id'=>$crm_project_id,'created_by'=>$data['created_by'],'product_id'=>$data['product_id']]);

        $this->Project_model->addProjectStageVersion(['project_stage_info_id' => $project_stage_info_id,'version_number' => 1, 'created_by' => $data['created_by'],'created_date_time' => currentDate()]);

        if(isset($stages[0]['workflow_id']) && $stages[0]['workflow_id'])
        {
            //$workflow_phase = $this->Workflow_model->getWorkflowPhase(['workflow_id' => $stages[0]['workflow_id']]);
            $workflow_phase = $this->Workflow_model->getWorkflowPhase(['workflow_id' => $stages[0]['workflow_id'],'project_id'=>$crm_project_id,'project_stage_id'=>$project_stage_id,'is_start'=>1]);
            if(!empty($workflow_phase)){
                $workflow_phase_id = $workflow_phase[0]['id_workflow_phase'];
            }
            else{
                $workflow_phase_id = 0;
            }
            $this->Workflow_model->addProjectStageWorkflowPhase(array(
                'project_stage_info_id' => $project_stage_info_id,
                'workflow_phase_id' => $workflow_phase_id,
                'assigned_to' => $data['created_by'],
                'parent_workflow_phase_id' => 0,
                'created_date_time' => currentDate(),
                'status' => 'pending'
            ));
        }
       /* if(isset($stages[0]['workflow_id']) && ($stages[0]['workflow_id']==0 || $stages[0]['workflow_id']=='') && ($stages[0]['next_project_stage_id'] > 0))
        {
            $stage_init = $this->checkNextStageForProject(array(
                'project_id' => $crm_project_id,
                'stage_id' => $stages[0]['next_project_stage_id'],
                'version_number' => 1,
                'project_stage_status' => 'in progress',
                'created_by' => $data['created_by']
            ));
        }*/

        for($sru=0;$sru<count($section_form_id);$sru++)
        {
            $form_fields = $this->Project_model->getProjectStageSectionFormFields(array('project_stage_section_form_id' => $section_form_id[$sru]));
            $data['form_id'] = $form_fields[0]['project_form_id'];
            $add = $new =  $update = array();
            //checking form field data with db form fields
            for($s=0;$s<count($form_fields);$s++){
                if(in_array($form_fields[$s]['field_key'],$data_keys)){
                    if($form_fields[$s]['field_type']=='json' || $form_fields[$s]['field_type']=='widget'){
                        $data[$form_fields[$s]['field_key']] = json_encode($data[$form_fields[$s]['field_key']]);
                    }
                    $add[] = array(
                        'project_stage_section_form_id' => $section_form_id[$sru],
                        'project_form_field_id' => $form_fields[$s]['id_project_form_field'],
                        'form_field_value' => $data[$form_fields[$s]['field_key']],
                        'updated_by' => $data['created_by'],
                        'created_date_time' => currentDate(),
                        'updated_date_time' => currentDate(),
                        'project_id' => $crm_project_id
                    );
                }
                else{
                    $add[] = array(
                        'project_stage_section_form_id' => $section_form_id[$sru],
                        'project_form_field_id' => $form_fields[$s]['id_project_form_field'],
                        'form_field_value' => '',
                        'updated_by' => $data['created_by'],
                        'created_date_time' => currentDate(),
                        'updated_date_time' => currentDate(),
                        'project_id' => $crm_project_id
                    );
                }
            }
            for($s=0;$s<count($add);$s++){
                $add[$s]['project_stage_info_id'] = $project_stage_info_id;
            }

            if(!empty($add)){
                $this->Project_model->addProjectStageFormInfo_batch($add);
            }
        }

        $this->Crm_model->updateProject(array('project_stage_info_id' => $project_stage_info_id),$crm_project_id);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Project created','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'created','activity_reference_id' => $crm_project_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('project_add'), 'data'=>array('crm_project_id' => $crm_project_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ProjectTabInfo_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company = $this->Company_model->getCompanyById($data['company_id']);
        $pipeline = $this->Crm_model->getProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'ITS Accepted'));
        $submitted = $this->Crm_model->getProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'approved'));
        $approved = $this->Crm_model->getProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'disbursement'));
        //echo $this->db->last_query(); exit;
        $monitoring = $this->Crm_model->getProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'monitoring'));

        if(empty($pipeline)){ $pipeline[0]['records'] = 0; $pipeline[0]['amount'] = 0; }
        if(empty($submitted)){ $submitted[0]['records'] = 0; $submitted[0]['amount'] = 0; }
        if(empty($approved)){ $approved[0]['records'] = 0; $approved[0]['amount'] = 0; }
        if(empty($monitoring)){ $monitoring[0]['records'] = 0; $monitoring[0]['amount'] = 0; }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array(
            'its_accepted' =>array('DEALS' => $pipeline[0]['records'],$company->currency_code => $pipeline[0]['amount']),
            'approved' =>array('DEALS' => $submitted[0]['records'],$company->currency_code => $submitted[0]['amount']),
            'disbursement' =>array('DEALS' => $approved[0]['records'],$company->currency_code => $approved[0]['amount']),
            'monitoring' =>array('DEALS' => $monitoring[0]['records'],$company->currency_code => $monitoring[0]['amount']),
        ));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmProjectList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getPipelineProjects($data);


        for($s=0;$s<count($result);$s++){

            $user_access = 'edit';
            $result[$s]['user_access'] = $user_access;
            $result[$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result[$s]['id_crm_project']);
            if(!empty($result[$s]['loan_amount']))
                $result[$s]['loan_amount'] = $result[$s]['loan_amount'][0]['amount'];
            else
                $result[$s]['loan_amount'] = 0;

            $outstanding_month =  $this->Crm_model->getCurrentOutStandingMonth();
            $result[$s]['outstanding_amount'] = $this->Crm_model->getProjectOutStanding(['crm_project_id' => $result[$s]['id_crm_project'], 'outstanding_month' => $outstanding_month]);
            if (!empty($result[$s]['outstanding_amount']))
                $result[$s]['outstanding_amount'] = $result[$s]['outstanding_amount'][0]['project_outstanding'];
            else
                $result[$s]['outstanding_amount'] = 0;
        }

        $all_records = $this->Crm_model->getPipelineProjectsCount($data);
        if (!empty($all_records))
            $all_records = $all_records[0]['total'];
        else
            $all_records = 0;

        /*$conditions = $result = array();
        $category = array(
            array('type' => 'its_accepted','status' => 'ITS Accepted'),
            array('type' => 'approved','status' => 'approved'),
            array('type' => 'disbursement','status' => 'disbursement'),
            array('type' => 'monitoring','status' => 'monitoring')
        );
        $conditions['company_id'] =$data['company_id'];

        //if(isset($data['search']) && !empty($data['search'])){ $data['search'] = (array)json_decode($data['search']); }
        //if(!empty($data['search']))
            //$data['search'] = (array)json_decode($data['search']);

        if(isset($data['offset']))
            $conditions['offset'] = $data['offset'];
        else
            $conditions['offset'] = 0;

        if(isset($data['limit']))
            $conditions['limit'] = $data['limit'];
        else
            $conditions['limit'] = 10;

        if(!is_array($data['search']))
        if(isset($data['search'])){ $data['search'] = (array)json_decode($data['search']); }

        /*if(isset($data['search']['category']))
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            if(isset($data['offset']))
                $conditions['offset'] = $data['offset'];
            else
                $conditions['offset'] = 0;

            if(isset($data['limit']))
                $conditions['limit'] = $data['limit'];
            else
                $conditions['limit'] = 10;


            for($s=0;$s<count($category);$s++)
            {
                $result[$category[$s]['type']]['data'] = array();
                $result[$category[$s]['type']]['count'] = 0;

                if($category[$s]['type']==$data['search']['category'])
                {
                    $conditions['status'] = $category[$s]['status'];
                    $result[$category[$s]['type']]['data'] = $this->Crm_model->getPipelineProjects($conditions);
                    //echo "<pre>"; print_r($result[$category[$s]['type']]['data']); exit;
                    //echo $this->db->last_query(); exit;
                    $total = $this->Crm_model->getPipelineProjectsCount($conditions);
                    $result[$category[$s]['type']]['count'] = $total[0]['total'];

                    //echo "<pre>"; print_r($result); exit;
                }
            }
        }
        else
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];
            $conditions['offset'] = 0;
            $conditions['limit'] = 3;

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            $conditions['status'] = 'ITS Accepted';
            $result['its_accepted']['data'] = $this->Crm_model->getPipelineProjects($conditions);
            $total = $this->Crm_model->getPipelineProjectsCount($conditions);
            $result['its_accepted']['count'] = $total[0]['total'];

            //$conditions['limit'] = 3;
            $conditions['status'] = 'approved';
            $result['approved']['data'] = $this->Crm_model->getPipelineProjects($conditions);
            $total = $this->Crm_model->getPipelineProjectsCount($conditions);
            $result['approved']['count'] = $total[0]['total'];

            $conditions['status'] = 'disbursement';
            $result['disbursement']['data'] = $this->Crm_model->getPipelineProjects($conditions);
            $total = $this->Crm_model->getPipelineProjectsCount($conditions);
            $result['disbursement']['count'] = $total[0]['total'];

            $conditions['status'] = 'monitoring';
            $result['monitoring']['data'] = $this->Crm_model->getPipelineProjects($conditions);
            $total = $this->Crm_model->getPipelineProjectsCount($conditions);
            $result['monitoring']['count'] = $total[0]['total'];
        }

        $current_user_id = '';
        if(isset($_SERVER['HTTP_USER'])){
            $current_user_id = $_SERVER['HTTP_USER'];
        }
        $user_access = '';
        for($s=0;$s<count($result['its_accepted']['data']);$s++)
        {
            $user_access = 'edit';
            $result['its_accepted']['data'][$s]['user_access'] = $user_access;
            $result['its_accepted']['data'][$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result['its_accepted']['data'][$s]['id_crm_project']);
            if(!empty($result['its_accepted']['data'][$s]['loan_amount']))
                $result['its_accepted']['data'][$s]['loan_amount'] = $result['its_accepted']['data'][$s]['loan_amount'][0]['amount'];
            else
                $result['its_accepted']['data'][$s]['loan_amount'] = 0;
        }

        for($s=0;$s<count($result['approved']['data']);$s++){
            $user_access = 'edit';
            $result['approved']['data'][$s]['user_access'] = $user_access;
            $result['approved']['data'][$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result['approved']['data'][$s]['id_crm_project']);
            if(!empty($result['approved']['data'][$s]['loan_amount']))
                $result['approved']['data'][$s]['loan_amount'] = $result['approved']['data'][$s]['loan_amount'][0]['amount'];
            else
                $result['approved']['data'][$s]['loan_amount'] = 0;
        }

        for($s=0;$s<count($result['disbursement']['data']);$s++){
            $user_access = 'edit';
            $result['disbursement']['data'][$s]['user_access'] = $user_access;
            $result['disbursement']['data'][$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result['disbursement']['data'][$s]['id_crm_project']);
            if(!empty($result['disbursement']['data'][$s]['loan_amount']))
                $result['disbursement']['data'][$s]['loan_amount'] = $result['disbursement']['data'][$s]['loan_amount'][0]['amount'];
            else
                $result['disbursement']['data'][$s]['loan_amount'] = 0;
        }

        for($s=0;$s<count($result['monitoring']['data']);$s++){

            $user_access = 'edit';
            $result['monitoring']['data'][$s]['user_access'] = $user_access;
            $result['monitoring']['data'][$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result['monitoring']['data'][$s]['id_crm_project']);
            if(!empty($result['supervision']['data'][$s]['loan_amount']))
                $result['monitoring']['data'][$s]['loan_amount'] = $result['monitoring']['data'][$s]['loan_amount'][0]['amount'];
            else
                $result['monitoring']['data'][$s]['loan_amount'] = 0;
        }*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result, 'total_records' => $all_records);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    /*public function project_get($project_id,$module_id,$current_user)
    {
        $project = $this->Crm_model->getProject($project_id);

        $project_company = $this->Crm_model->getProjectCompany(array('crm_project_id' => $project_id));
        $project_company = array_map(function($key){ return $key['company_name']; }, $project_company);
        $project[0]['company_name'] = implode(',',$project_company);

        $project_contact = $this->Crm_model->getProjectContact(array('crm_project_id' => $project_id));
        $project_contact = array_map(function($key){ return $key['first_name']; }, $project_contact);
        $project[0]['contact_name'] = implode(',',$project_contact);

        $project[0]['loan_amount'] = 0;
        $loan = $this->Crm_model->getFacilityLoanAmountByProjectId($project[0]['id_crm_project']);
        if(!empty($loan)){ $project[0]['loan_amount'] = $loan; }
        $project[0]['no_of_loans'] = 0;
        $count = $this->Crm_model->getFacilitiesCount(array('crm_project_id' => $project[0]['id_crm_project']));
        if($count[0]['total']!='')
            $project[0]['no_of_loans']  = $count[0]['total'];
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = $active_forms = array(); $active_form_id = 0;
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));

        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];

            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st= $active_form_id = 0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['form_class'] = $result[$r]['form_class'];
                    $form[$st]['form_key'] = $result[$r]['form_key'];
                    $form[$st]['comments_count'] = $this->Crm_model->getFormCommentsCount(array('form_key' => $result[$r]['form_key'],'reference_type' => 'project','reference_id' => $project_id));
                    $st++;
                }
            }

            $data[$s]['form'] = $form;
        }

        $t=0;
        //getting fields data

        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++){

                $form_fields = $this->Crm_model->getFormFieldsByFormId($data[$s]['form'][$r]['id_form']);
                $form_field_data = $this->Crm_model->getProjectFormData($project_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();

                for($sr=0;$sr<count($form_fields);$sr++)
                {
                    $field_data[$sr] = array(
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '---'
                    );

                    for($st=0;$st<count($form_field_data);$st++){

                        if($form_fields[$sr]['id_form_field']==$form_field_data[$st]['form_field_id']) {

                            if (trim($form_field_data[$st]['field_type']) == 'json') {

                                $value = json_decode($form_field_data[$st]['form_field_value']);
                                for ($sth = 0; $sth < count($value); $sth++) {
                                    if (empty($value[$sth])) {
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_field_data[$st]['form_field_value'] = $value;

                                $field_data[$sr] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }
                            else {
                                if ($form_field_data[$st]['field_name'] == 'project_main_sector_id' || $form_field_data[$st]['field_name'] == 'project_sub_sector_id') {
                                    $sector = $this->Master_model->getSector(array('id_sector' => $form_field_data[$st]['form_field_value']));
                                    if (!empty($sector))
                                        $form_field_data[$st]['form_field_value'] = $sector->sector_name;
                                }
                                $field_data[$sr] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }

                        }
                    }
                }


                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not


                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $project_id, 'crm_module_type' => 'project', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                else $data[$s]['form'][$r]['status'] = 1; $t++;
            }
        }


        $current_user_details = $this->Company_model->getCompanyUser($current_user);
        $reporting_user_id = $current_user_details->reporting_user_id;
        $reporting_user_name = $current_user_details->reporting_user_name;

        $project[0]['approval'] = 0;
        if($project[0]['committee_id']!=0)
        {
            $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => $project[0]['project_sub_sector_id'], 'product_id' => $project[0]['product_id']));

            if(!empty($approval_limit)){
                if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
            }
            else
            {
                $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => $project[0]['project_main_sector_id'], 'product_id' => $project[0]['product_id']));

                if(!empty($approval_limit)){
                    if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
                }
                else
                {
                    $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => 0, 'product_id' => $project[0]['product_id']));

                    if(!empty($approval_limit)){
                        if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
                    }
                }
            }

        }


        $project[0]['rating'] = '---';
        $project[0]['grade'] = '---';

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('details' => $project, 'forms' => $data, 'percentage' => '','active_forms' => $active_forms,'reporting' =>array('reporting_user_id' => $reporting_user_id,'reporting_user_name' => $reporting_user_name)));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function projectList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectList($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result[$s]['id_crm_project']);
            if(!empty($result[$s]['loan_amount']))
                $result[$s]['loan_amount'] = $result[$s]['loan_amount'][0]['amount'];
            else
                $result[$s]['loan_amount'] = 0;
        }


        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }
        $total_records = count($this->Crm_model->getProjectList($data));



        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectForm_get($project_id,$form_id,$type)
    {
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getProjectFormData($project_id,$form_id);
        $project_details = $this->Crm_model->getProject($project_id);
        $created_date = '';

        $result = array(); $st=0;
        $result['form_name'] = $form_fields[0]['form_name'];
        $form_field_data = $this->Crm_model->getProjectFormData($project_id,$form_id);
        $field_data = array();
        for($st=0;$st<count($form_field_data);$st++){
            if($type=='view')
            {
                if(trim($form_field_data[$st]['field_type'])=='json'){

                    $value = json_decode($form_field_data[$st]['form_field_value']);
                    for($sth=0;$sth<count($value);$sth++)
                    {
                        if(empty($value[$sth])){
                            $value[$sth] = (object)$value[$sth];
                        }
                    }
                    $form_field_data[$st]['form_field_value'] = $value;

                    $field_data[] = array(
                        'field_label' => $form_field_data[$st]['field_label'],
                        'field_name' => $form_field_data[$st]['field_name'],
                        'field_type' => $form_field_data[$st]['field_type'],
                        'field_value' => $form_field_data[$st]['form_field_value']
                    );
                }
                else{
                    if($form_field_data[$st]['field_name']=='project_main_sector_id' || $form_field_data[$st]['field_name']=='project_sub_sector_id'){
                        $sector = $this->Master_model->getSector(array('id_sector' => $form_field_data[$st]['form_field_value']));
                        if(!empty($sector))
                            $form_field_data[$st]['form_field_value'] = $sector->sector_name;
                    }
                    $field_data[] = array(
                        'field_label' => $form_field_data[$st]['field_label'],
                        'field_name' => $form_field_data[$st]['field_name'],
                        'field_type' => $form_field_data[$st]['field_type'],
                        'field_value' => $form_field_data[$st]['form_field_value']
                    );
                }
            }
            else
            {
                if(trim($form_field_data[$st]['field_type'])=='json'){
                    $value = json_decode($form_field_data[$st]['form_field_value']);
                    for($sth=0;$sth<count($value);$sth++)
                    {
                        if(empty($value[$sth])){
                            $value[$sth] = (object)$value[$sth];
                        }
                    }
                    $form_field_data[$st]['form_field_value'] = $value;
                    $result[$form_field_data[$st]['field_name']] = $form_field_data[$st]['form_field_value'];
                }
                else{
                    $value = '';
                    if(isset($form_field_data[$st]['form_field_value'])){ $value = $form_field_data[$st]['form_field_value']; }
                    $result[$form_field_data[$st]['field_name']] = $value;
                }
            }
        }

        if($type=='view') $result['form_data'] = $field_data;

        $percentage = $this->Crm_model->getFieldPercentage($project_id,'project');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'project_details' => $project_details[0]));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function project_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';

        if(isset($data['project_cost'])){ $data['project_cost'] = json_encode($data['project_cost']); $update_type=0; }
        if(isset($data['loan_information'])){ $data['loan_information'] = json_encode($data['loan_information']); $update_type=0; }
        if(isset($data['facility_details'])){ $data['facility_details'] = json_encode($data['facility_details']); $update_type=1; }
        if(isset($data['risk_assessment'])){ $data['risk_assessment'] = json_encode($data['risk_assessment']); $update_type=0; }
        if(isset($data['recommended_terms'])){ $data['recommended_terms'] = json_encode($data['recommended_terms']); $update_type=0; }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_project_id'])){
            $check_data = $this->Crm_model->getProjectFormData($data['crm_project_id'],$data['form_id']);
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                }
                $check_field = 1;
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_fields'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            if(!isset($data['crm_project_id'])){
                $project_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['project_title']) && $data['project_title']!=''){
                    $project_data['project_title'] = $data['project_title'];
                }
                if(isset($data['project_main_sector_id'])){
                    $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
                }
                if(isset($data['project_sub_sector_id'])){
                    $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
                }
                if(isset($data['project_description'])){
                    $project_data['project_description'] = $data['project_description'];
                }
                $project_data['created_date_time'] = currentDate();
                $project_data['updated_date_time'] = currentDate();
                $crm_project_id = $this->Crm_model->addCrmProject($project_data);
            }
            else{
                $crm_project_id = $data['crm_project_id'];
                $project_data = array();
                if(isset($data['project_title']) && $data['project_title']!=''){
                    $project_data['project_title'] = $data['project_title'];
                }
                if(isset($data['project_main_sector_id'])){
                    $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
                }
                if(isset($data['project_sub_sector_id'])){
                    $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
                }
                if(isset($data['project_description'])){
                    $project_data['project_description'] = $data['project_description'];
                }
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_project_id'] = $crm_project_id;
                $form_data[$r]['created_date_time'] = currentDate();
            }

            $this->Crm_model->addCrmProjectData($form_data);

            if(!empty($project_data)){
                $project_data['updated_date_time'] = currentDate();
                $this->Crm_model->updateCrmProject($project_data,$crm_project_id);
            }
        }
        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_project_data'] = $check_data[$s]['id_crm_project_data'];
                        }
                    }
                }
            }
            $this->Crm_model->updateCrmProjectData($form_data,$update_type);
            $crm_project_id = $data['crm_project_id'];
            $project_data = array();
            if(isset($data['project_title']) && $data['project_title']!=''){
                $project_data['project_title'] = $data['project_title'];
            }
            if(isset($data['project_main_sector_id'])){
                $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
            }
            if(isset($data['project_sub_sector_id'])){
                $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
            }
            if(isset($data['project_description'])){
                $project_data['project_description'] = $data['project_description'];
            }
        }
        if(!empty($project_data)){
            $project_data['updated_date_time'] = currentDate();
            $this->Crm_model->updateCrmProject($project_data,$crm_project_id);
        }

        //$this->Crm_model->updateCrmProject(array('updated_date_time' => currentDate()),$crm_project_id);

        $percentage = $this->Crm_model->getFieldPercentage($crm_project_id,'project');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(),'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'Information saved','module_type' => 'project','module_id' =>  $data['crm_project_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>array('crm_project_id' => $crm_project_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmListForMapping_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('search_key',array('required'=> $this->lang->line('search_key')));
        $validated = $this->form_validator->validate($data);
        $this->form_validator->clearRules();
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('contact_company_id_req')));
        $validated1 = $this->form_validator->validate($data);
        //$data['company_id']=$data['crm_company_id'];
        if($validated != 1 && $validated1 != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getCrmListForMapping($data);

        if(isset($data['crm_company_id'])){
            for($s=0;$s<count($result);$s++){
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyDesignation_get()
    {
        $result = $this->Crm_model->getCompanyDesignation();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function mapModule_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('crm_company_designation_id', array('required'=> $this->lang->line('company_desi_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $type = $data['type'];
        unset($data['type']);
        if(isset($data['is_primary_company'])){
            $update = array('is_primary_company' => '');
            $this->Crm_model->updateCompanyContactPrimary($data['crm_contact_id'],$update);
        }

        $check_exists = $this->Crm_model->checkCompanyContactExists($data);
        if(!empty($check_exists)){
            $update = array('crm_company_contact_status' => '1');
            if(isset($data['is_primary_company'])){ $update['is_primary_company'] = 1; }
            $this->Crm_model->updateCompanyContact($check_exists->id_crm_company_contact,$update);
            $update = array('crm_company_designation_id' => $data['crm_company_designation_id']);
            $this->Crm_model->updateCompanyContact($check_exists->id_crm_company_contact,$update);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$check_exists->id_crm_company_contact);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }
        $data['created_date_time'] = currentDate();
        $map_id = $this->Crm_model->mapModule($data);
        $activity_template = '';
        if($type=='company'){ $suc_msg = $this->lang->line('contact_to_company_add'); $activity_template='Contact added to company'; $activity_type = 'contact_add_to_company'; }
        else if($type=='contact'){ $suc_msg = $this->lang->line('company_to_contcat_add'); $activity_template='Company added to contact'; $activity_type = 'company_add_to_contact'; }

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Contact','activity_template' => $activity_template,'module_type' => 'contact','module_id' => $data['crm_contact_id'], 'activity_type' => $activity_type,'activity_reference_id' => $map_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyContact_delete($crm_company_contact_id)
    {
        $this->Crm_model->deleteCompanyContact($crm_company_contact_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function mapModule_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('company_contact_id_req')));
        $validated = $this->form_validator->validate($data);
        $this->form_validator->clearRules();
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('company_contact_id_req')));
        $validated1 = $this->form_validator->validate($data);

        if($validated != 1 && $validated1 != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getMapModule($data);

        for($s=0;$s<count($result);$s++){
            if(isset($data['crm_company_id']))
                $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_contact'],'contact');
            else if(isset($data['crm_contact_id']))
                $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_company'],'company');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactType_get()
    {
        $result = $this->Crm_model->getContactType();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> 'Project id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectContact($data);
        for($s=0;$s<count($result);$s++)
        {
            $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_contact'],'contact');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['is_primary_contact'])){
            $update = array('is_primary_contact' => '');
            $this->Crm_model->updateProjectContactPrimary($data['crm_project_id'],$update);
        }

        $check_exists = $this->Crm_model->checkProjectContactExists($data);
        if(!empty($check_exists)){
            $update = array('project_contact_status' => '1','contact_type_id'=>isset($data['contact_type_id'])?$data['contact_type_id']:$check_exists->contact_type_id);
            if(isset($data['is_primary_contact'])){ $update['is_primary_contact'] = 1; }
            $this->Crm_model->updateProjectContact($check_exists->id_project_contact,$update);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$check_exists->id_project_contact);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }
        $data['created_date_time'] = currentDate();
        $map_id = $this->Crm_model->addProjectContact($data);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Contact added to project.','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'contact_add_to_project','activity_reference_id' => $map_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_to_project_add'), 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_delete($crm_project_contact_id)
    {
        $this->Crm_model->deleteProjectContact($crm_project_contact_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyType_get()
    {
        $result = $this->Crm_model->getCompanyType();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> 'Project id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectCompany($data);
        for($s=0;$s<count($result);$s++){
            $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_company'],'company');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('company_type_id', array('required'=> $this->lang->line('company_type_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_exists = $this->Crm_model->checkProjectCompanyExists($data);
        if(!empty($check_exists)){
            $update = array('project_company_status' => '1');
            $this->Crm_model->updateProjectCompany($check_exists->id_project_company,$update);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$check_exists->id_project_company);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }
        $data['created_date_time'] = currentDate();
        $map_id = $this->Crm_model->addProjectCompany($data);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Company added to project.','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'company_add_to_project','activity_reference_id' => $map_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('company_to_project_add'), 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_delete($crm_project_company_id)
    {
        $this->Crm_model->deleteProjectCompany($crm_project_company_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('company_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPointType_get()
    {
        $result = $this->Crm_model->getTouchPointType();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = array();
        if(!isset($data['grade']))
            $data['grade'] = '';
        $result = $this->Crm_model->getTouchPoints($data);
        $reminder_count = count($this->Crm_model->getTouchPointReminder($data));

        $res = array();
        for($s=0;$s<count($result);$s++)
        {
            $touch_point_type_id = explode('@@@',$result[$s]['touch_point_type_id']);
            $touch_point_type_name = explode('@@@',$result[$s]['touch_point_type_name']);
            $touch_point_description = explode('@@@',$result[$s]['touch_point_description']);
            $reminder_date = explode('@@@',$result[$s]['reminder_date']);
            $reminder_time = explode('@@@',$result[$s]['reminder_time']);
            $reminder = explode('@@@',$result[$s]['reminder']);
            $user_name = explode('@@@',$result[$s]['user_name']);
            $created_date = explode('@@@',$result[$s]['created_date']);

            for($r=0;$r<count($touch_point_type_id);$r++)
            {
                $reminder_date_new = $reminder_new = $user_name_new = $reminder_time_new = $created_date_new = '';
                if(isset($reminder_date[$r])){ $reminder_date_new = $reminder_date[$r]; }
                if(isset($reminder_time[$r])){ $reminder_time_new = $reminder_time[$r]; }
                if(isset($reminder[$r])){ $reminder_new = $reminder[$r]; }
                if(isset($user_name[$r])){ $user_name_new = $user_name[$r]; }
                if(isset($created_date[$r])){ $created_date_new = $created_date[$r]; }
                $res[$s][] = array(
                    'touch_point_type_id' => $touch_point_type_id[$r],
                    'touch_point_type_name' => $touch_point_type_name[$r],
                    'touch_point_description' => $touch_point_description[$r],
                    'reminder_date' => $reminder_date_new,
                    'reminder_time' => $reminder_time_new,
                    'reminder' => $reminder_new,
                    'user_name' => $user_name_new,
                    'created_date' => $created_date_new
                );
            }

            $results[] = array(
                'touch_point' => $res[$s],
                'date' => $result[$s]['date']
            );
        }
        $total_records = count($this->Crm_model->getTotalTouchPoints($data));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$results,'total_records' => $total_records,'reminder' => $reminder_count));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPointReminder_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getTouchPointReminder($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo '<pre>';print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('crm_touch_point_type_id', array('required'=> $this->lang->line('touch_point_type_req')));
        $this->form_validator->add_rules('crm_module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('touch_point_description', array('required'=> $this->lang->line('description_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['reminder'])){
            $data['is_reminder'] = $data['reminder'];
            unset($data['reminder']);
        }
        if(!isset($data['grade'])){
            $data['grade'] = 0;
        }
        $data['touch_point_from_id'] = $data['crm_module_id'];
        if($data['crm_module_type']=='contact'){ $data['crm_module_id'] = 1; }
        else if($data['crm_module_type']=='company'){ $data['crm_module_id'] = 2; }
        else if($data['crm_module_type']=='project'){ $data['crm_module_id'] = 3; }
        else if($data['crm_module_type']=='collateral'){ $data['crm_module_id'] = 4; }
        $module_type = $data['crm_module_type'];
        unset($data['crm_module_type']);
        $data['created_date_time'] = currentDate();
        $result = $touch_point_id = $this->Crm_model->addTouchPoint($data);
        //adding activity

        $this->Activity_model->addActivity(array('grade' => $data['grade'],'activity_name' => 'Touch Point','activity_template' => $data['touch_point_description'],'module_type' => $module_type,'module_id' => $data['touch_point_from_id'], 'activity_type' => 'touch_point','activity_reference_id' => $touch_point_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('touch_point_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('touch_point_id',array('required'=> $this->lang->line('touch_point_id_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Activity_model->deleteActivity(array('id_activity' => $data['touch_point_id']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('touch_point_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $product = $this->Crm_model->getProduct($data);
        foreach($product as $k=>$v){
            //$product[$k]['created_by_user_name']='---';
            //$product[$k]['no_of_projects']='---';
            $product[$k]['total_exposure']='---';
            //$product[$k]['last_modified_on']='---';
        }
        unset($data['offset']); unset($data['limit']);
        $total_records = count($this->Crm_model->getProduct($data));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $product,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('product_name', array('required'=> $this->lang->line('product_name_req')));
        //$this->form_validator->add_rules('company_approval_structure_id', array('required'=> $this->lang->line('approval_struc_req')));
        //$this->form_validator->add_rules('company_pre_approval_structure_id', array('required'=> $this->lang->line('pre_approval_struc_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['company_pre_approval_structure_id'])){
            $data['company_pre_approval_structure_id'] = 0;
        }

        if(!isset($data['id_product']))
        {
            $product = array(
                'company_id' => $data['company_id'],
                'product_name' => $data['product_name'],
                //'company_approval_structure_id' => $data['company_approval_structure_id'],
                //'company_pre_approval_structure_id' => $data['company_pre_approval_structure_id'],
                'created_by' => $data['created_by'],
                'created_date' => currentDate(),
                'updated_date' => currentDate()
            );
            $product_id = $this->Crm_model->addProduct($product);
            $stages=$this->Crm_model->getProjectStages();
            foreach($stages as $kp=>$vp) {
                $addProductStageWorkflow = array(
                    'product_id' => $product_id,
                    'project_stage_id' => $vp['id_project_stage'],
                    'workflow_phase_name' => 'Owner',
                    'workflow_phase_status' => 1,
                    'product_workflow_approval_type' => 'User',
                    'product_workflow_approval_process' => 'Parallel',
                    'is_default' => 1
                );
                $addProductStageWorkflow['created_by'] = $data['created_by'];
                $addProductStageWorkflow['created_date_time'] = currentDate();
                $id_product_workflow_phase = $this->Crm_model->addProductStageWorkflow($addProductStageWorkflow);
                $inner_data = '';
                $inner_data['product_workflow_phase_id'] = $id_product_workflow_phase;
                $inner_data['approver_id'] = 0;
                $inner_data['is_approval_mandatory'] = 1;
                $inner_data['created_by'] = $data['created_by'];
                $inner_data['created_date_time'] = currentDate();
                $this->Crm_model->addProductStageWorkflowApprover($inner_data);
            }

            $suc_msg =  $this->lang->line('product_add');
        }
        else
        {
            $product = array(
                'company_id' => $data['company_id'],
                'product_name' => $data['product_name'],
                //'company_approval_structure_id' => $data['company_approval_structure_id'],
                //'company_pre_approval_structure_id' => $data['company_pre_approval_structure_id'],
                'created_by' => $data['created_by'],
                'id_product' => $data['id_product'],
                'updated_date' => currentDate()
            );
            $product_id = $data['id_product'];
            $this->Crm_model->updateProduct($product);
            $suc_msg =  $this->lang->line('product_update');
        }

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$product_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function projectStage_get(){
        $product = $this->Crm_model->getProjectStages();
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $product);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function productStageWorkflow_get(){

        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('product_id', array('required' => $this->lang->line('id_product_req')));
        $this->form_validator->add_rules('project_stage_id', array('required' => $this->lang->line('id_project_stage_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $product = $this->Crm_model->getProductStageWorkflow($data);
        foreach($product as $k=>$v){
            $inner_data='';
            $inner_data['id_product_workflow_phase']=$v['id_product_workflow_phase'];
            $inner_data['product_workflow_approval_type']=$v['product_workflow_approval_type'];
            $product[$k]['approver_info']=$this->Crm_model->getProductStageWorkflowApprover($inner_data);
            $product[$k]['approver_id']=isset($product[$k]['approver_info'][0]['approver_id'])?$product[$k]['approver_info'][0]['approver_id']:NULL;
            $product[$k]['id_product_workflow_phase_approver']=isset($product[$k]['approver_info'][0]['id_product_workflow_phase_approver'])?$product[$k]['approver_info'][0]['id_product_workflow_phase_approver']:NULL;
            $product[$k]['workflow_actions_data'] = $this->Workflow_model->getProductPhaseWorkflowAction(array('id_product_workflow_phase'=>$v['id_product_workflow_phase']));
        }
        unset($data['offset']);
        unset($data['limit']);
        $total_records = count($this->Crm_model->getProductStageWorkflow($data));
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => $product, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function productStageWorkflowActionsSettings_get(){

        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('product_id', array('required' => $this->lang->line('id_product_req')));
        $this->form_validator->add_rules('project_stage_id', array('required' => $this->lang->line('id_project_stage_req')));
        $this->form_validator->add_rules('id_product_workflow_phase', array('required' => $this->lang->line('id_product_workflow_phase_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $inner_data=$data;
        unset($inner_data['id_product_workflow_phase']);
        $product['workflow_phases'] = $this->Crm_model->getProductStageWorkflow($inner_data);
        foreach($product['workflow_phases'] as $k=>$v){
            if($v['id_product_workflow_phase']==$data['id_product_workflow_phase']){
                unset($product['workflow_phases'][$k]);
            }
        }
        $product['workflow_phases']=array_values($product['workflow_phases']);
        $product['workflow_actions'] = $this->Workflow_model->getWorkflowAction();
        $product['workflow_actions_data'] = $this->Workflow_model->getProductPhaseWorkflowAction($data);
        $workflow_next_stage_details = $this->Crm_model->getProjectStages(array('id_project_stage'=>$data['project_stage_id']));
        $product['workflow_next_stage_details']=array();
        if(isset($workflow_next_stage_details[0]['next_project_stage_id']) && $workflow_next_stage_details[0]['next_project_stage_id']>0){
            $product['workflow_next_stage_details'] = $this->Crm_model->getProjectStages(array('id_project_stage'=>$workflow_next_stage_details[0]['next_project_stage_id']));
            if(isset($product['workflow_next_stage_details'][0]['id_project_stage']) && $product['workflow_next_stage_details'][0]['id_project_stage']>0)
                $product['workflow_next_stage_details']=$product['workflow_next_stage_details'][0];
        }
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $product);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function productStageWorkflowActionsSettings_post(){
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_product_workflow_phase', array('required' => $this->lang->line('id_product_workflow_phase')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('workflow_actions_data', array('required' => $this->lang->line('workflow_actions_data_req')));

        //$data['workflow_actions_data']=json_decode($data['workflow_actions_data'],true);
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $add=$update=array();

        foreach ($data['workflow_actions_data'] as $k => $v) {
            $inner_data = '';
            if (isset($v['product_workflow_phase_id']) && $v['product_workflow_phase_id'] != NULL && $v['product_workflow_phase_id'] != '' && $v['product_workflow_phase_id'] > 0) {
                $inner_data['product_workflow_phase_id'] = $v['product_workflow_phase_id'];
            } else {
                $inner_data['product_workflow_phase_id'] = $data['id_product_workflow_phase'];
            }
            if (isset($v['forward_product_workflow_phase_id']) && $v['forward_product_workflow_phase_id'] != NULL && $v['forward_product_workflow_phase_id'] != '' && $v['forward_product_workflow_phase_id'] > 0) {
                $inner_data['forward_product_workflow_phase_id'] = $v['forward_product_workflow_phase_id'];
            }
            if (isset($v['id_workflow_action']) && $v['id_workflow_action'] != NULL && $v['id_workflow_action'] != '' && $v['id_workflow_action'] > 0) {
                $inner_data['workflow_action_id'] = $v['id_workflow_action'];
            }
            if(isset($v['next_project_stage_id']))
                $inner_data['next_project_stage_id'] = $v['next_project_stage_id'];
            if(isset($v['next_project_stage_assign_type']))
                $inner_data['next_project_stage_assign_type'] = $v['next_project_stage_assign_type'];
            if(isset($v['next_project_stage_assign_to']))
                $inner_data['next_project_stage_assign_to'] = $v['next_project_stage_assign_to'];

            if (isset($v['status']))
                $inner_data['status'] = $v['status'];
            if (isset($v['id_product_workflow_phase_action']) && $v['id_product_workflow_phase_action'] != NULL && $v['id_product_workflow_phase_action'] != '' && $v['id_product_workflow_phase_action'] > 0) {
                $inner_data['updated_by'] = $data['created_by'];
                $inner_data['updated_date_time'] = currentDate();
                $inner_data['id_product_workflow_phase_action'] = $v['id_product_workflow_phase_action'];
                $update[] = $inner_data;
            } else {
                $inner_data['created_by'] = $data['created_by'];
                $inner_data['created_date_time'] = currentDate();
                $add[] = $inner_data;
            }
        }

        foreach($add as $kadd=>$vadd){
            $this->Workflow_model->addProductPhaseWorkflowAction($vadd);
        }
        foreach($update as $kupdate=>$vupdate){
            $this->Workflow_model->updateProductPhaseWorkflowAction($vupdate);
        }
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array());
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function productStageWorkflow_post(){
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('product_id', array('required' => $this->lang->line('product_id_req')));
        $this->form_validator->add_rules('project_stage_id', array('required' => $this->lang->line('project_stage_id_req')));
        $this->form_validator->add_rules('workflow_phase_name', array('required' => $this->lang->line('workflow_phase_name_req')));
        $this->form_validator->add_rules('workflow_phase_status', array('required' => $this->lang->line('workflow_phase_status_req')));
        $this->form_validator->add_rules('product_workflow_approval_type', array('required' => $this->lang->line('product_workflow_approval_type_req')));
        $this->form_validator->add_rules('product_workflow_approval_process', array('required' => $this->lang->line('product_workflow_approval_process_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        if($data['product_workflow_approval_type']=='Committee') {
            $this->form_validator->add_rules('approver_info', array('required' => $this->lang->line('approver_info_req')));
        }
        else{
            $this->form_validator->add_rules('approver_id', array('required' => $this->lang->line('approver_info_req')));
        }
        //$data['approver_info']=json_decode($data['approver_info'],true);
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($data['product_workflow_approval_type']=='Committee') {
            $data['product_workflow_approval_process']='Parallel';
        }
        $product = array(
            'product_id' => $data['product_id'],
            'project_stage_id' => $data['project_stage_id'],
            'workflow_phase_name' => $data['workflow_phase_name'],
            'workflow_phase_status' => $data['workflow_phase_status'],
            'product_workflow_approval_type' => $data['product_workflow_approval_type'],
            'product_workflow_approval_process' => $data['product_workflow_approval_process']
        );
        if(isset($data['id_product_workflow_phase']) && $data['id_product_workflow_phase']!=NULL && $data['id_product_workflow_phase']!='' && $data['id_product_workflow_phase']>0){
            $product['updated_by']=$data['created_by'];
            $product['updated_date_time']=currentDate();
            $product['id_product_workflow_phase']=$data['id_product_workflow_phase'];
            $this->Crm_model->updateProductStageWorkflow($product);
            $id_product_workflow_phase=$data['id_product_workflow_phase'];
        }
        else{
            $product['created_by']=$data['created_by'];
            $product['created_date_time']=currentDate();
            $id_product_workflow_phase=$this->Crm_model->addProductStageWorkflow($product);
        }
        $add=$update=array();
        if($data['product_workflow_approval_type']=='Committee') {
            foreach ($data['approver_info'] as $k => $v) {
                $inner_data = '';
                if (isset($v['product_workflow_phase_id']) && $v['product_workflow_phase_id'] != NULL && $v['product_workflow_phase_id'] != '' && $v['product_workflow_phase_id'] > 0) {
                    $inner_data['product_workflow_phase_id'] = $v['product_workflow_phase_id'];
                } else {
                    $inner_data['product_workflow_phase_id'] = $id_product_workflow_phase;
                }
                $inner_data['approver_id'] = $v['approver_id'];
                $inner_data['is_approval_mandatory'] = $v['is_approval_mandatory'];
                if (isset($v['status']))
                    $inner_data['status'] = $v['status'];
                if (isset($v['id_product_workflow_phase_approver']) && $v['id_product_workflow_phase_approver'] != NULL && $v['id_product_workflow_phase_approver'] != '' && $v['id_product_workflow_phase_approver'] > 0) {
                    $inner_data['updated_by'] = $data['created_by'];
                    $inner_data['updated_date_time'] = currentDate();
                    $inner_data['id_product_workflow_phase_approver'] = $v['id_product_workflow_phase_approver'];
                    $update[] = $inner_data;
                } else {
                    $inner_data['created_by'] = $data['created_by'];
                    $inner_data['created_date_time'] = currentDate();
                    $add[] = $inner_data;
                }
            }
        }
        else{
            $add=array();$update=array();
            $inner_data = '';
            $inner_data['product_workflow_phase_id'] = $id_product_workflow_phase;
            $inner_data['approver_id'] = $data['approver_id'];
            $inner_data['is_approval_mandatory'] = 1;
            if (isset($v['status']))
                $inner_data['status'] = $v['status'];
            if (isset($data['id_product_workflow_phase_approver']) && $data['id_product_workflow_phase_approver'] != NULL && $data['id_product_workflow_phase_approver'] != '' && $data['id_product_workflow_phase_approver'] > 0) {
                $inner_data['updated_by'] = $data['created_by'];
                $inner_data['updated_date_time'] = currentDate();
                $inner_data['id_product_workflow_phase_approver'] = $data['id_product_workflow_phase_approver'];
                $update[] = $inner_data;
            } else {
                $inner_data['created_by'] = $data['created_by'];
                $inner_data['created_date_time'] = currentDate();
                $add[] = $inner_data;
            }
        }

        foreach($add as $kadd=>$vadd){
            $this->Crm_model->addProductStageWorkflowApprover($vadd);
        }
        foreach($update as $kupdate=>$vupdate){
            $this->Crm_model->updateProductStageWorkflowApprover($vupdate);
        }
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => array('id_product_workflow_phase'=>$id_product_workflow_phase)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectTeam($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('team_member_id', array('required'=> $this->lang->line('team_member_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_exists = $this->Crm_model->checkProjectTeamExists($data);
        if(!empty($check_exists)){
            $update = array('team_member_status' => '1');
            $this->Crm_model->updateProjectTeam($check_exists->id_project_team,$update);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('team_add'), 'data'=>$check_exists->id_project_team);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }
        $data['created_date_time'] = currentDate();
        $map_id = $this->Crm_model->addProjectTeam($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('team_add'), 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_delete($project_team_id)
    {
        $this->Crm_model->deleteProjectTeam($project_team_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('team_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function intelligence_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=> $this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
        $this->form_validator->add_rules('sector_id', array('required'=> $this->lang->line('sector_id_req')));
        $this->form_validator->add_rules('sub_sector_id', array('required'=> $this->lang->line('sub_sector_id_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if($data['type']!='crm_company' && $data['type']!='crm_project')
        {
            $result = array('status'=>FALSE,'error'=>array('type' => $this->lang->line('Invalid type')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getIntelligence($data);
        $list = $user_list = $users = array();
        for($s=0;$s<count($result);$s++)
        {
            if($data['type']=='crm_company'){
                $list[] = array(
                    'id_crm_company' => $result[$s]['id_crm_company'],
                    'company_id' => $result[$s]['company_id'],
                    'company_name' => $result[$s]['company_name'],
                    'company_description' => $result[$s]['company_description']
                );
            }
            else if($data['type']=='crm_project')
            {
                $list[] = array(
                    'id_crm_project' => $result[$s]['id_crm_project'],
                    'company_id' => $result[$s]['company_id'],
                    'project_title' => $result[$s]['project_title'],
                    'project_description' => $result[$s]['project_description']
                );
            }

            if(!in_array($result[$s]['id_user'],$users)){
                array_push($users,$result[$s]['id_user']);
                $user_list[] = array(
                    'username' => $result[$s]['username'],
                    'email_id' => $result[$s]['email_id'],
                    'phone_number' => $result[$s]['phone_number'],
                    'profile_image' => getImageUrl($result[$s]['profile_image'],'profile'),
                    'branch_name' => $result[$s]['legal_name'],
                    'approval_name' => $result[$s]['approval_name']
                );
            }
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=>array('list' => $list, 'user_list' => $user_list));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledge_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if($data['type']!='crm_contact' && $data['type']!='crm_company' && $data['type']!='crm_project')
        {
            $result = array('status'=>FALSE,'error'=>array('type' => $this->lang->line('invalid_type')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['tags']))
            $tag = $data['tags'];
        else $tag = 0;

        $data['tags'] = array();

        if($tag)
            $data['tags'][] = $tag;
        if($data['type']=='crm_company'){
            $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $crm_details = $this->Crm_model->getCompany($data['crm_company_id']);
                $data['tags'][] =  $crm_details[0]['sector'];
                $data['tags'][] =  $crm_details[0]['sub_sector'];
            }
        }
        else if($data['type']=='crm_project'){
            $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $crm_details = $this->Crm_model->getProject($data['crm_project_id']);
                $data['tags'][] =  $crm_details[0]['sector'];
                $data['tags'][] =  $crm_details[0]['sub_sector'];
            }
        }
        $tags = '(';
        for($s=0;$s<count($data['tags']);$s++)
        {   if($s>0){ $tags .=','; }
            $tags .='"'.$data['tags'][$s].'"';
        }
        $tags .= ')';
        $data['tags'] = $tags;
        $result = $this->Crm_model->getKnowledgeDocuments(array('search_key' => $data['tags'], 'company_id' => $data['company_id'], 'knowledge_document_status' => 1));
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['document_type']=='document'){
                $result[$s]['document_source'] = getImageUrl($result[$s]['document_source'],'file');
            }
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeCount_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_knowledge_document', array('required'=> $this->lang->line('knowledge_document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->addKnowledgeDocumentViewCount($data);

        $result = array('status'=>TRUE, 'message' => '', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTerms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        $result = $this->Company_model->getProductTerms($data);

        for($sr=0;$sr<count($result);$sr++)
        {
            if(trim($result[$sr]['product_term_type'])=='item')
            {
                $result[$sr]['data'] = $this->Company_model->getProductTermItems(array('product_term_id' => $result[$sr]['id_product_term']));
                for($s=0;$s<count($result[$sr]['data']);$s++)
                {
                    if(trim(strtolower($result[$sr]['data'][$s]['product_term_item_name']))=='facility information')
                    {
                        $form = $this->Crm_model->getFormFieldValueByFieldName(array('form_name' => trim($result[$sr]['data'][$s]['product_term_item_name']), 'crm_project_id' => $data['crm_project_id']));
                        if(!empty($form)) $result[$sr]['data'][$s]['answer'] = json_decode($form[0]['form_field_value']);
                        else  $result[$sr]['data'][$s]['answer'] = '';

                    }
                    else if(trim($result[$sr]['data'][$s]['item_type'])=='Covenants')
                    {
                        if($result[$sr]['data'][$s]['product_term_item_name']=='Positive Covenants'){ $covenant_type_key = 'positive_covenant'; }
                        if($result[$sr]['data'][$s]['product_term_item_name']=='Negative Covenants'){ $covenant_type_key = 'negative_covenant'; }
                        if($result[$sr]['data'][$s]['product_term_item_name']=='Representations & Warranties'){ $covenant_type_key = 'representations_warranties'; }
                        if($result[$sr]['data'][$s]['product_term_item_name']=='Events of Defaults'){ $covenant_type_key = 'events_of_defaults'; }
                        $result[$sr]['data'][$s]['category'] = $this->Company_model->getCovenantCategoryList(array('company_id' => $data['company_id'], 'covenant_type_key' => $covenant_type_key));
                        for($sth=0;$sth<count($result[$sr]['data'][$s]['category']);$sth++){
                            $result[$sr]['data'][$s]['category'][$sth]['covenant'] = $this->Crm_model->getSelectedProjectCovenants(array('crm_project_id' => $data['crm_project_id'], 'covenant_category_id' => $result[$sr]['data'][$s]['category'][$sth]['id_covenant_category']));
                        }
                    }
                    else if(trim($result[$sr]['data'][$s]['product_term_item_key'])=='approval_and_comments')
                    {
                        $comments = $this->Crm_model->getApprovalComments(['crm_project_id' => $data['crm_project_id']]);
                        for($cm=0;$cm<count($comments);$cm++)
                        {
                            $comments[$cm]['profile_image'] = getImageUrl($comments[$cm]['profile_image'],'profile');
                        }
                        $result[$sr]['data'][$s]['answer'] = $comments;
                    }
                    else if(trim($result[$sr]['data'][$s]['product_term_item_key'])=='key_risk_factors')
                    {
                        $result[$sr]['data'][$s]['answer'] = $this->Crm_model->getProjectRiskItems(array('crm_project_id' => $data['crm_project_id']));
                        if(!empty($result[$sr]['data'][$s]['answer'])){
                            $result[$sr]['data'][$s]['answer']= $result[$sr]['data'][$s]['answer'][0];
                            if(isset($result[$sr]['data'][$s]['answer']['risk_items'])){
                                $result[$sr]['data'][$s]['answer'] = json_decode($result[$sr]['data'][$s]['answer']['risk_items']);
                            }
                        }
                        else{ $result[$sr]['data'][$s]['answer'] = new stdClass(); }
                    }

                    else
                    {
                        $answer = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $result[$sr]['data'][$s]['id_product_term_item']));
                        if(empty($answer)){ $result[$sr]['data'][$s]['answer'] = ''; }
                        else { $result[$sr]['data'][$s]['answer'] = $answer[0]['item_data']; }
                    }

                    if(trim($result[$sr]['data'][$s]['product_term_item_name'])=="Expiry"){
                        if(isset($result[$sr]['data'][$s]['answer']) && !empty($result[$sr]['data'][$s]['answer'])){
                            $result[$sr]['data'][$s]['answer']=date("Y-m-d",strtotime($result[$sr]['data'][$s]['answer']));
                        }else{
                            $result[$sr]['data'][$s]['answer']="";
                        }
                    }

                }
            }
            else if($result[$sr]['product_term_type']==''){
                $result[$sr]['data'] = array();
            }
            else if($result[$sr]['product_term_type']=='question')
            {
                $result[$sr]['data'] = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => 'pre_disbursement_checklist'));
                for($s=0;$s<count($result[$sr]['data']);$s++)
                {
                    $result[$sr]['data'][$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$sr]['data'][$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $project_details[0]['project_main_sector_id'], 'sub_sector_id' => $project_details[0]['project_sub_sector_id'], 'product_id' => $project_details[0]['product_id'] ));
                    for($r=0;$r<count($result[$sr]['data'][$s]['category']);$r++)
                    {
                        $result[$sr]['data'][$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$sr]['data'][$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                        for($sru=0;$sru<count($result[$sr]['data'][$s]['category'][$r]['question']);$sru++)
                        {
                            if(isset($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']) && $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']!=''){
                                $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option'] = json_decode($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']);
                            }
                            $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['id_assessment_question'], 'crm_project_id' => $data['crm_project_id']));

                            if(isset($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer) && $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer!=''){
                                if($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_type']=='checkbox')
                                    $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer = json_decode($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer);
                                else if($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_type']=='file')
                                    $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer = getImageUrl($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer,'file');
                            }
                        }
                    }
                }
            }
            else if($result[$sr]['product_term_type']=='risk'){
                $result[$sr]['data']  = $this->Crm_model->getProjectRiskItems(array('crm_project_id' => $data['crm_project_id']));
                if(!empty($result[$sr]['data'])){
                    $result[$sr]['data']= $result[$sr]['data'][0];
                    if(isset($result[$sr]['data']['risk_items'])){
                        $result[$sr]['data']['risk_items'] = json_decode($result[$sr]['data']['risk_items']);
                    }
                }
                else{ $result[$sr]['data'] = new stdClass(); }
            }
            else if($result[$sr]['product_term_type']=='collateral'){
                $result[$sr]['data']  = $this->Crm_model->getCollateralList(array('crm_project_id' => $data['crm_project_id']));
            }
            else if($result[$sr]['product_term_type']=='monitoring'){
                $item_data = $this->Crm_model->getProductTermItemData(array('product_term_id' => $result[$sr]['id_product_term'],'crm_project_id' => $data['crm_project_id']));
                $monitoring = array();

                for($p=0;$p<count($item_data);$p++)
                {
                    if($item_data[$p]['product_term_item_key']=='title'){
                        $monitoring[$item_data[$p]['item_order']]['item_order'] = $item_data[$p]['item_order'];
                        $monitoring[$item_data[$p]['item_order']]['title'] = $item_data[$p]['item_data'];
                        $monitoring[$item_data[$p]['item_order']]['username'] = $item_data[$p]['username'];
                    }
                    if($item_data[$p]['product_term_item_key']=='date_of_the_verification_visit'){
                        $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                    }
                    if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit'){
                        $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                    }
                    if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit_client_interaction'){
                        $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                    }
                    if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit'){
                        $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                    }
                    if($item_data[$p]['product_term_item_key']=='grading'){
                        $monitoring[$item_data[$p]['item_order']]['grading'] = $item_data[$p]['item_data'];
                    }
                    $monitoring[$item_data[$p]['item_order']]['status'] = 'current';
                }

                $result[$sr]['data'] = array_values($monitoring);
            }
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function terms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'crm project id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));

        $this->form_validator->add_rules('product_term_key', array('required'=> 'Term key required'));
        $this->form_validator->add_rules('product_term_type', array('required'=> 'Term Type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = array();
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        if(!isset($data['product_term_id']))
        {
            $product_term_details = $this->Company_model->getProductTerms($data);
            $data['product_term_id'] = $product_term_details[0]['id_product_term'];
        }

        if($data['product_term_type']=='item' || $data['product_term_type']=='item')
        {
            $result = $this->Company_model->getProductTermItems($data);
            for($s=0;$s<count($result);$s++)
            {
                if(trim(strtolower($result[$s]['product_term_item_name']))=='facility information')
                {
                    $form = $this->Crm_model->getFormFieldValueByFieldName(array('form_name' => trim($result[$s]['product_term_item_name']), 'crm_project_id' => $data['crm_project_id']));
                    if(!empty($form)) $result[$s]['answer'] = json_decode($form[0]['form_field_value']);
                    else  $result[$s]['answer'] = '';
                }
                else
                {
                    $result[$s]['answer'] = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $result[$s]['id_product_term_item']));
                    if(!empty($result[$s]['answer'])){ $result[$s]['answer'] = $result[$s]['answer'][0]['item_data']; }
                    else { $result[$s]['answer'] = ''; }
                }
            }
        }
        else if($data['product_term_type']=='question')
        {
            $result = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['product_term_key']));
            for($s=0;$s<count($result);$s++)
            {
                if(isset($data['product_term_key']) && ($data['product_term_key'] =='pre_disbursement_checklist' || $data['product_term_key'] =='restriction_limit')){
                    $data['product_id'] = $project_details[0]['product_id'];
                }
                else{ $data['product_id'] = 0; }
                $order = 'DESC';
                if($data['product_term_key']=='pre_disbursement_checklist'){ $order = 'ASC'; }
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $project_details[0]['project_main_sector_id'], 'sub_sector_id' => $project_details[0]['project_sub_sector_id'], 'product_id' => $data['product_id'], 'order' => $order));

                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                        $result[$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_project_id' => $data['crm_project_id']));

                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                            if($result[$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                            else if($result[$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer,'file');
                        }
                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->comment_file) && $result[$s]['category'][$r]['question'][$sr]['answer']->comment_file!=''){
                            $result[$s]['category'][$r]['question'][$sr]['answer']->comment_file = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->comment_file,'file');
                        }
                    }
                }
            }
        }
        else if($data['product_term_type']=='monitoring'){
            $item_data = $this->Crm_model->getProductTermItemData(array('product_term_id' => $data['product_term_id'],'crm_project_id' => $data['crm_project_id']));
            $monitoring = array();
            for($p=0;$p<count($item_data);$p++)
            {

                if($item_data[$p]['product_term_item_key']=='title'){
                    $monitoring[$item_data[$p]['item_order']]['item_order'] = $item_data[$p]['item_order'];
                    $monitoring[$item_data[$p]['item_order']]['title'] = $item_data[$p]['item_data'];
                    $monitoring[$item_data[$p]['item_order']]['username'] = $item_data[$p]['username'];
                }
                if($item_data[$p]['product_term_item_key']=='date_of_the_verification_visit'){
                    $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                }
                if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit'){
                    $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                }
                if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit_client_interaction'){
                    $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                }
                if($item_data[$p]['product_term_item_key']=='date_of_the_last_visit'){
                    $monitoring[$item_data[$p]['item_order']]['date'] = $item_data[$p]['item_data'];
                }
                if($item_data[$p]['product_term_item_key']=='grading'){
                    $monitoring[$item_data[$p]['item_order']]['grading'] = $item_data[$p]['item_data'];
                }
                $monitoring[$item_data[$p]['item_order']]['status'] = 'current';
            }
            $monitoring = array_values($monitoring);
            $result= $monitoring;
        }


        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTermItemData_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('product_term_item_id', array('required'=> $this->lang->line('product_term_item_id_req')));
        $this->form_validator->add_rules('item_data', array('required'=> $this->lang->line('data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_item_data = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $data['product_term_item_id']));

        if(!empty($check_item_data))
        {
            $this->Crm_model->updateProductTermItemData($data);
            $suc_msg = $this->lang->line('info_update');
        }
        else
        {
            $data['created_date_time'] = currentDate();
            $this->Crm_model->addProductTermItemData($data);
            $suc_msg = $this->lang->line('info_add');
        }

        //adding module status for financial sheets
        $term = $this->Company_model->getProductTermItems(array('id_product_term_item' => $data['product_term_item_id']));
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $term[0]['product_term_id'], 'reference_type' => 'term'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $term[0]['product_term_id'], 'reference_type' => 'term', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $product_term_item_details = $this->Crm_model->getProductTermItem(array('id_product_term_item' => $data['product_term_item_id']));
        $this->Activity_model->addActivity(array('activity_name' => $product_term_item_details[0]['product_term_item_name'],'activity_template' => 'Information updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'term','activity_reference_id' => $term[0]['product_term_id'],'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //risk changed to crm company wise
    public function riskCategory_get()
    {
        $data = $this->input->get();
        $data['type']=isset($data['type'])?$data['type']:'company';
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        if(!isset($data['crm_company_id']) && !isset($data['crm_project_id']) && !isset($data['crm_contact_id']))
            $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $sector_id = $sub_sector_id = '';

        if(isset($data['crm_company_id'])) {
            $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
            $sector_id = $crm_company_details[0]['sector_id'];
            $sub_sector_id = $crm_company_details[0]['sub_sector_id'];
        }
        else if(isset($data['crm_project_id'])) {
            $crm_project_details = $this->Crm_model->getProject($data['crm_project_id']);
            $sector_id = $crm_project_details[0]['project_main_sector_id'];
            $sub_sector_id = $crm_project_details[0]['project_sub_sector_id'];
        }
        else if(isset($data['crm_contact_id'])) {

            $sector_id = 0;
            $sub_sector_id = 0;
        }

        $result = [];
        if($sub_sector_id > 0)
            $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'sector_id' => $sub_sector_id,'type'=>$data['type']));
        if(empty($result) && $sector_id > 0)
            $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'sector_id' => $sector_id,'type'=>$data['type']));
        if(empty($result))
            $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'sector_id' => 0,'type'=>$data['type']));
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type'=>$data['type']));
            for($r=0;$r<count($result[$s]['sub_category']);$r++)
            {
                $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type'=>$data['type']));
                for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                {
                    if(isset($data['crm_company_id']))
                    {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithCompanyItems(array('crm_company_id'=>$data['crm_company_id'],'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'],'type'=>$data['type']));
                    }
                    else if(isset($data['crm_project_id']))
                    {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithProjectItems(array('crm_project_id'=>$data['crm_project_id'],'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'],'type'=>$data['type']));
                    }
                    else if(isset($data['crm_contact_id']))
                    {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithContactItems(array('crm_contact_id'=>$data['crm_contact_id'],'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'],'type'=>$data['type']));
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectRiskCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_items = $data['data']; $add = $update = $risk_category_ids = array();
        for($s=0;$s<count($project_items);$s++)
        {
            if(!in_array($project_items[$s]['id_risk_category'],$risk_category_ids)){ array_push($risk_category_ids,$project_items[$s]['id_risk_category']); }
            $check_data = $this->Crm_model->getProjectRiskCategoryItem(array('risk_category_item_id' => $project_items[$s]['id_risk_category_item'],'crm_project_id' => $data['crm_project_id']));
            if(empty($check_data))
            {
                $add[] = array(
                    'risk_category_id' => $project_items[$s]['id_risk_category'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'project_risk_category_item_grade' => $project_items[$s]['grade'],
                    'user_id' => $data['user_id'],
                    'crm_project_id' => $data['crm_project_id'],
                    'is_active' => 1,
                    'created_date_time' => currentDate()
                );
            }
            else{
                $update[] = array(
                    'id_project_risk_category_item' => $check_data[0]['id_project_risk_category_item'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'project_risk_category_item_grade' => $project_items[$s]['grade'],
                    'is_active' => 1
                );
            }
        }
        //update all project risk category items as is_active=0
        for($s=0;$s<count($risk_category_ids);$s++)
        {
            $this->Crm_model->updateProjectRiskCategory(array('is_active' => 0, 'risk_category_id' => $risk_category_ids[$s]));
        }

        if(!empty($add)){ $this->Crm_model->addProjectRiskCategory($add); }
        if(!empty($update)){ $this->Crm_model->updateProjectRiskCategory($update); }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>'', 'reference_type' => 'risk'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>'', 'reference_type' => 'risk', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
        $this->Activity_model->addActivity(array('activity_name' => 'Risk Assessment','activity_template' => 'Risk assessment information updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'risk','activity_reference_id' => '','created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('risk_assessment_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCrmProjectRiskRating($crm_project_id,$company_id)
    {
        $check_project_rating = $this->Crm_model->checkProjectContainsProjectRiskAssessment($crm_project_id);
        if(empty($check_project_rating))
        {
            $rating = 0;
            $status = 0;
        }
        else
        {
            $result = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => 0,'type'=>'company'));
            $rating = array();
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type'=>'company'));
                $sub_category = array();
                for($r=0;$r<count($result[$s]['sub_category']);$r++)
                {
                    $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type'=>'company'));
                    $attributes = array();
                    for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                    {
                        $crm_project_details = $this->Crm_model->getProject($crm_project_id);
                        $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $this->Crm_model->getCrmProjectRiskCategoryAttributesItemsGrade(array('crm_project_id'=>$crm_project_id,'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $crm_project_details[0]['project_main_sector_id'],'sub_sector_id' => $crm_project_details[0]['project_sub_sector_id']));

                        if(isset($result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade']))
                            $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade'];
                        else
                            $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = 0;
                    }

                    $attributes = $result[$s]['sub_category'][$r]['attributes'];
                    $attributes = array_map(function ($i) { return $i['project_grade']; }, $attributes);
                    $remove = array(0);
                    $attributes = array_diff($attributes, $remove);
                    if(count($attributes)>0)
                        $attributes = (array_sum($attributes)/count($attributes));
                    else $attributes = 0;
                    $result[$s]['sub_category'][$r]['final_grade'] =  (($attributes*$result[$s]['sub_category'][$r]['risk_percentage'])/100);

                }
                $sub_category = $result[$s]['sub_category'];
                $sub_category = array_map(function ($i) { return $i['final_grade']; }, $sub_category);
                $sub_category = array_sum($sub_category);

                $result[$s]['final_grade'] =  (($sub_category*$result[$s]['risk_percentage'])/100);

            }
            $rating = $result;
            $rating = array_map(function ($i) { return $i['final_grade']; }, $rating);

            $rating = array_sum($rating);
            $rating = number_format($rating,2);
            $status = 1;
        }

        return $rating.'@@@'.$status;
    }

    public function companyRiskCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
        $project_items = $data['data']; $add = $update = $risk_category_ids = array();
        for($s=0;$s<count($project_items);$s++)
        {
            if(!in_array($project_items[$s]['id_risk_category'],$risk_category_ids)){ array_push($risk_category_ids,$project_items[$s]['id_risk_category']); }
            $check_data = $this->Crm_model->getCompanyRiskCategoryItem(array('risk_category_item_id' => $project_items[$s]['id_risk_category_item'],'crm_company_id' => $data['crm_company_id']));
            if(empty($check_data))
            {
                $add[] = array(
                    'risk_category_id' => $project_items[$s]['id_risk_category'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'company_risk_category_item_grade' => $project_items[$s]['grade'],
                    'user_id' => $data['user_id'],
                    'crm_company_id' => $data['crm_company_id'],
                    'is_active' => 1,
                    'created_date_time' => currentDate()
                );
            }
            else{
                $update[] = array(
                    'id_company_risk_category_item' => $check_data[0]['id_company_risk_category_item'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'company_risk_category_item_grade' => $project_items[$s]['grade'],
                    'is_active' => 1
                );
            }
        }
        //update all project risk category items as is_active=0
        for($s=0;$s<count($risk_category_ids);$s++)
        {
            $this->Crm_model->updateCompanyRiskCategory(array('is_active' => 0, 'risk_category_id' => $risk_category_ids[$s]));
        }

        if(!empty($add)){ $this->Crm_model->addCompanyRiskCategory($add); }
        if(!empty($update)){ $this->Crm_model->updateCompanyRiskCategory($update); }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' =>$form_details[0]['id_form'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' =>$form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
        $this->Activity_model->addActivity(array('activity_name' => 'Risk Assessment','activity_template' => 'Risk assessment information updated','module_type' => 'company','module_id' => $data['crm_company_id'], 'activity_type' => 'risk','activity_reference_id' => '','created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));

        $internal_rating = $this->getCrmCompanyRiskRating($data['crm_company_id'],$data['company_id']);
        $this->Crm_model->updateCrmCompany(array('internal_rating' => $internal_rating),$data['crm_company_id']);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('risk_assessment_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCrmCompanyRiskRating($crm_company_id,$company_id)
    {
        $check_company_rating = $this->Crm_model->checkCompanyContainsCompanyRiskAssessment($crm_company_id);
        if(empty($check_company_rating))
        {
            $rating = 1;
            $status = 0;
        }
        else
        {
            $crm_company_details = $this->Crm_model->getCompany($crm_company_id);
            $result = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => 0,'sector_id' => $crm_company_details[0]['sector_id'],'sub_sector_id' => $crm_company_details[0]['sub_sector_id'],'type' => 'company'));
            $rating = array();
            $rating_grades = [];
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type' => 'company'));
                $sub_category = array();
                for($r=0;$r<count($result[$s]['sub_category']);$r++)
                {
                    $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type' => 'company'));
                    $attributes = array();
                    for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                    {

                        $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = $this->Crm_model->getCrmCompanyRiskCategoryAttributesItemsGrade(array('crm_company_id'=>$crm_company_id,'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'],'type' => 'company'));

                        if(isset($result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'][0]['company_risk_category_item_grade']))
                            $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'][0]['company_risk_category_item_grade'];
                        else
                            $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = 0;
                    }

                    $attributes = $result[$s]['sub_category'][$r]['attributes'];
                    $attributes = array_map(function ($i) { return $i['company_grade']; }, $attributes);
                    $remove = array(0);
                    $attributes = array_diff($attributes, $remove);
                    if(count($attributes)>0)
                        $attributes = (array_sum($attributes)/count($attributes));
                    else $attributes = 0;
                    $result[$s]['sub_category'][$r]['final_grade'] =  (($attributes*$result[$s]['sub_category'][$r]['risk_percentage'])/100);

                }
                $sub_category = $result[$s]['sub_category'];
                $sub_category = array_map(function ($i) { return $i['final_grade']; }, $sub_category);
                $sub_category = array_sum($sub_category);

                $result[$s]['final_grade'] =  (($sub_category*$result[$s]['risk_percentage'])/100);
                $rating_grades[] = $sub_category;
                //echo $result[$s]['final_grade'].'--'.$result[$s]['risk_category_name'].'--'.$sub_category.'--'.$result[$s]['risk_percentage'].'<br/>';
            }
            if(max($rating_grades) >= 5) $rating = max($rating_grades);
            else {
                $rating = $result;
                $rating = array_map(function ($i) { return $i['final_grade']; }, $rating);
                $rating = array_sum($rating);
                $rating = number_format($rating,2);
            }
            $rating = $rating+1;
            $status = 1;
        }

        return $rating;
    }

    public function contactRiskCategory_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
        $project_items = $data['data']; $add = $update = $risk_category_ids = array();
        for($s=0;$s<count($project_items);$s++)
        {
            if(!in_array($project_items[$s]['id_risk_category'],$risk_category_ids)){ array_push($risk_category_ids,$project_items[$s]['id_risk_category']); }
            $check_data = $this->Crm_model->getContactRiskCategoryItem(array('risk_category_item_id' => $project_items[$s]['id_risk_category_item'],'crm_contact_id' => $data['crm_contact_id']));
            if(empty($check_data))
            {
                $add[] = array(
                    'risk_category_id' => $project_items[$s]['id_risk_category'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'contact_risk_category_item_grade' => $project_items[$s]['grade'],
                    'user_id' => $data['user_id'],
                    'crm_contact_id' => $data['crm_contact_id'],
                    'is_active' => 1,
                    'created_date_time' => currentDate()
                );
            }
            else{
                $update[] = array(
                    'id_contact_risk_category_item' => $check_data[0]['id_contact_risk_category_item'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'contact_risk_category_item_grade' => $project_items[$s]['grade'],
                    'is_active' => 1
                );
            }
        }
        //update all project risk category items as is_active=0
        for($s=0;$s<count($risk_category_ids);$s++)
        {
            $this->Crm_model->updateContactRiskCategory(array('is_active' => 0, 'risk_category_id' => $risk_category_ids[$s]));
        }

        if(!empty($add)){ $this->Crm_model->addContactRiskCategory($add); }
        if(!empty($update)){ $this->Crm_model->updateContactRiskCategory($update); }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' =>$form_details[0]['id_form'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' =>$form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
        $this->Activity_model->addActivity(array('activity_name' => 'Internal Rating','activity_template' => 'Internal rating updated','module_type' => 'contact','module_id' => $data['crm_contact_id'], 'activity_type' => 'risk','activity_reference_id' => '','created_by' => $data['user_id'],'created_date_time' => currentDate()));

        /*$last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'],'project_stage_section_form_id' => $form_details[0]['id_form'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));*/

        $internal_rating = $this->getCrmContactRiskRating($data['crm_contact_id'],$data['company_id']);
        $this->Crm_model->updateCrmContact(array('internal_rating' => $internal_rating),$data['crm_contact_id']);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('internal_rating_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCrmContactRiskRating($crm_contact_id,$company_id)
    {
        $check_company_rating = $this->Crm_model->checkContactContainsCompanyRiskAssessment($crm_contact_id);
        if(empty($check_company_rating))
        {
            $rating = 1;
            $status = 0;
        }
        else
        {

            $result = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => 0,'type' => 'contact'));
            $rating = array();
            $rating_grades = [];
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type' => 'contact'));
                $sub_category = array();
                for($r=0;$r<count($result[$s]['sub_category']);$r++)
                {
                    $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type' => 'contact'));
                    $attributes = array();
                    for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                    {

                        $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = $this->Crm_model->getCrmContactRiskCategoryAttributesItemsGrade(array('crm_contact_id'=>$crm_contact_id,'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));

                        if(isset($result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'][0]['contact_risk_category_item_grade']))
                            $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'][0]['contact_risk_category_item_grade'];
                        else
                            $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = 0;
                    }

                    $attributes = $result[$s]['sub_category'][$r]['attributes'];
                    $attributes = array_map(function ($i) { return $i['contact_grade']; }, $attributes);
                    $remove = array(0);
                    $attributes = array_diff($attributes, $remove);
                    if(count($attributes)>0)
                        $attributes = (array_sum($attributes)/count($attributes));
                    else $attributes = 0;
                    $result[$s]['sub_category'][$r]['final_grade'] =  (($attributes*$result[$s]['sub_category'][$r]['risk_percentage'])/100);

                }
                $sub_category = $result[$s]['sub_category'];
                $sub_category = array_map(function ($i) { return $i['final_grade']; }, $sub_category);
                $sub_category = array_sum($sub_category);

                $result[$s]['final_grade'] =  (($sub_category*$result[$s]['risk_percentage'])/100);
                $rating_grades[] = $sub_category;
                //echo $result[$s]['final_grade'].'--'.$result[$s]['risk_category_name'].'--'.$sub_category.'--'.$result[$s]['risk_percentage'].'<br/>';
            }
            if(max($rating_grades) >= 5) $rating = max($rating_grades);
            else {
                $rating = $result;
                $rating = array_map(function ($i) { return $i['final_grade']; }, $rating);
                $rating = array_sum($rating);
                $rating = number_format($rating,2);
            }
            $rating = $rating+1;
            $status = 1;
        }

        return $rating;
    }
    public function contactRiskGradeCalculation($rating)
    {
        $grade = array();
        if(!is_numeric($rating) && !is_float($rating)){ $grade = array(); }
        else {
            $internal_rating = $this->Crm_model->getInternalRatingGrade(array('rating' => $rating));
            if(!empty($internal_rating)){
                $grade = array(
                    'grade' => $internal_rating[0]['internal_rating'],
                    'rating_text' => $internal_rating[0]['rating_text'],
                    'risk_margin' => $internal_rating[0]['risk_margin'],
                );
            }
        }
        return $grade;
    }
    public function companyRiskGradeCalculation($rating)
    {
        /*$grade = '';
        if(!is_numeric($rating) && !is_float($rating)){ $grade = '---'; }
        else if(0<=$rating && $rating<1.25){ $grade = 'A'; }
        else if(1.25<=$rating && $rating<2.25){ $grade = 'B'; }
        else if(2.25<=$rating && $rating<3.25){ $grade = 'C'; }
        else if(3.25<=$rating && $rating<4.25){ $grade = 'D'; }
        else if(4.25<=$rating && $rating<5.25){ $grade = 'E'; }
        else if(5.25<=$rating && $rating<6.25){ $grade = 'F'; }
        else if(6.25<=$rating){ $grade = 'G'; }
        return $grade;*/
        $grade = array();
        if(!is_numeric($rating) && !is_float($rating)){ $grade = array(); }
        else {
            $internal_rating = $this->Crm_model->getInternalRatingGrade(array('rating' => $rating));
            if(!empty($internal_rating)){
                $grade = array(
                    'grade' => $internal_rating[0]['internal_rating'],
                    'rating_text' => $internal_rating[0]['rating_text'],
                    'risk_margin' => $internal_rating[0]['risk_margin'],
                );
            }
        }
        return $grade;
    }

    public function companyInternalRisk_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_details = $this->Crm_model->getCompany($data['crm_company_id']);
        //$internal_rating = $this->Company_model->getInternalRating($data);

        //for($sr=0;$sr<count($internal_rating);$sr++) {
            $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'type'=>'company'));
            for ($s = 0; $s < count($result); $s++) {
                $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type'=>'company'));
                $sub_category = array();
                for ($r = 0; $r < count($result[$s]['sub_category']); $r++) {
                    $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type'=>'company'));
                    $attributes = array();
                    for ($u = 0; $u < count($result[$s]['sub_category'][$r]['attributes']); $u++) {

                        $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithCompanyItems(array('crm_company_id' => $data['crm_company_id'], 'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $company_details[0]['sector_id'], 'sub_sector_id' => $company_details[0]['sub_sector_id'],'type'=>'company'));
                        $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = $this->Crm_model->getCrmCompanyRiskCategoryAttributesItemsGrade(array('crm_company_id' => $data['crm_company_id'], 'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $company_details[0]['sector_id'], 'sub_sector_id' => $company_details[0]['sub_sector_id'],'type'=>'company'));
                        if (isset($result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'][0]['company_risk_category_item_grade'])) {
                            $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'][0]['company_risk_category_item_grade'];
                        }
                        else
                            $result[$s]['sub_category'][$r]['attributes'][$u]['company_grade'] = 0;
                    }

                    $attributes = $result[$s]['sub_category'][$r]['attributes'];

                    $attributes = array_map(function ($i) {
                        return $i['company_grade'];
                    }, $attributes);
                    $remove = array(0);
                    $attributes = array_diff($attributes, $remove);
                    if (count($attributes) > 0)
                        $attributes = (array_sum($attributes) / count($attributes));
                    else $attributes = 0;
                    $result[$s]['sub_category'][$r]['final_grade'] = number_format((($attributes * $result[$s]['sub_category'][$r]['risk_percentage']) / 100), 2);
                }

                $sub_category = $result[$s]['sub_category'];
                $sub_category = array_map(function ($i) {
                    return $i['final_grade'];
                }, $sub_category);
                $sub_category = array_sum($sub_category);
                $result[$s]['final_grade'] = number_format((($sub_category * $result[$s]['risk_percentage']) / 100), 2);
            }
        //}

        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contactInternalRating_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('crm_contact_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$internal_rating = $this->Company_model->getInternalRating($data);

        //for($sr=0;$sr<count($internal_rating);$sr++) {
        $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0,'type'=>'contact'));
        for ($s = 0; $s < count($result); $s++) {
            $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category'],'type'=>'contact'));
            $sub_category = array();
            for ($r = 0; $r < count($result[$s]['sub_category']); $r++) {
                $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category'],'type'=>'contact'));
                $attributes = array();
                for ($u = 0; $u < count($result[$s]['sub_category'][$r]['attributes']); $u++) {

                    $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithContactItems(array('crm_contact_id' => $data['crm_contact_id'], 'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));
                    $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = $this->Crm_model->getCrmContactRiskCategoryAttributesItemsGrade(array('crm_contact_id' => $data['crm_contact_id'], 'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));
                    if (isset($result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'][0]['contact_risk_category_item_grade'])) {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'][0]['contact_risk_category_item_grade'];
                    }
                    else
                        $result[$s]['sub_category'][$r]['attributes'][$u]['contact_grade'] = 0;
                }

                $attributes = $result[$s]['sub_category'][$r]['attributes'];

                $attributes = array_map(function ($i) {
                    return $i['contact_grade'];
                }, $attributes);
                $remove = array(0);
                $attributes = array_diff($attributes, $remove);
                if (count($attributes) > 0)
                    $attributes = (array_sum($attributes) / count($attributes));
                else $attributes = 0;
                $result[$s]['sub_category'][$r]['final_grade'] = number_format((($attributes * $result[$s]['sub_category'][$r]['risk_percentage']) / 100), 2);
            }

            $sub_category = $result[$s]['sub_category'];
            $sub_category = array_map(function ($i) {
                return $i['final_grade'];
            }, $sub_category);
            $sub_category = array_sum($sub_category);
            $result[$s]['final_grade'] = number_format((($sub_category * $result[$s]['risk_percentage']) / 100), 2);
        }
        //}

        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function statusFlow_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProjectList(array('crm_project_id' => $data['crm_project_id'], 'company_id' => $data['company_id']));
        $result = $this->Crm_model->statusFlow($data);

        if(empty($result)){ }
        else
        {
            for($s=0;$s<count($result);$s++)
            {
                $meeting = $this->Crm_model->projectApprovalMeeting(array('project_approval_id' => $result[$s]['id_project_approval']));
                for($st=0;$st<count($meeting);$st++){
                    if($meeting[$st]['from_time']!=0 && $meeting[$st]['to_time']!=0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['meeting_time'];
                    else if($meeting[$st]['from_time']!=0 && $meeting[$st]['to_time']==0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['from_time'];
                    else if($meeting[$st]['from_time']==0 && $meeting[$st]['to_time']!=0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['to_time'];

                    $email = $this->Crm_model->getMeetingGuest(array('meeting_id' => $meeting[$st]['id_meeting']));
                    $meeting[$st]['guest'] = $email[0]['guest'];
                }
                if(!empty($meeting)){
                    $result[$s]['meeting'] = $meeting;
                }
                else{
                    $result[$s]['meeting'] = array();
                }

                //getting facilities
                if($result[$s]['id_project_approval']==0) {
                    $result[$s]['facilities'] = $this->Crm_model->getFacilityActivityInitial(array('crm_project_id' => $data['crm_project_id']));
                }
                else {
                    $result[$s]['facilities'] = $this->Crm_model->getFacilityActivity(array('project_approval_id' => $result[$s]['id_project_approval']));

                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        //$this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_project = $this->Crm_model->getProject($data['crm_project_id']);
        if(!isset($data['stage_id']))
        {
            //$data['project_facility_status'] = 'active'; //
            $result = $this->Crm_model->getFacilitiesList($data);
        }
        else
        {
            $stage_info_details = $this->Project_model->getProjectStageInfo(array('project_id' => $data['crm_project_id'],'stage_id' => $data['stage_id']));
            if($crm_project[0]['project_stage_info_id']==$stage_info_details[0]['id_project_stage_info'])
            {
                //$data['project_facility_status'] = 'active';
                $result = $this->Crm_model->getFacilitiesList($data);
            }
            else
            {
                //$url = MONGO_SERVICE_URL.'findFacilityDetails/?project_stage_info_id='.$stage_info_details[0]['id_project_stage_info'].'&project_id='.$data['crm_project_id'];
                $url = MONGO_SERVICE_PHP_URL.'findFacilityDetails.php?project_stage_info_id='.$stage_info_details[0]['id_project_stage_info'].'&project_id='.$data['crm_project_id'];
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                $output=curl_exec($ch);
                curl_close($ch);
                $result = json_decode($output);
                if(!empty($result) && isset($result->data[0])){ $result = $result->data[0]->facilities; }
                else{ $result = ''; }
            }
        }
        if($result != '')
        {
            for($fc=0;$fc<count($result);$fc++)
            {
                $result[$fc] = (array)$result[$fc];
                $result[$fc]['facility_transactions'] = $this->Crm_model->getFacilityTransactionList($result[$fc]['contract_id']);
            }
        }
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('id_project_facility', array('required'=> $this->lang->line('project_facility_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$data['project_facility_status'] = 'active';
        $crm_project = $this->Crm_model->getProject($data['crm_project_id']); //getting project information
        $stage_info_details = $this->Project_model->getProjectStageInfo(array('project_id' => $data['crm_project_id'],'stage_id' => $data['stage_id']));//getting stage info

        if($crm_project[0]['project_stage_info_id']==$stage_info_details[0]['id_project_stage_info'])
        {
            $data['type']='workflow';
            $result = $this->Crm_model->getFacilities($data);
            for($s=0;$s<count($result);$s++){
                $result[$s]['pricing'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'pricing'));
                $result[$s]['terms_conditions'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
            }
        }
        else
        {
            //$url = MONGO_SERVICE_URL.'findFacilityDetails/?project_stage_info_id='.$stage_info_details[0]['id_project_stage_info'].'&project_id='.$data['crm_project_id'].'&id_project_facility='.$data['id_project_facility'];
            $url = MONGO_SERVICE_PHP_URL.'findFacilityDetails.php?project_stage_info_id='.$stage_info_details[0]['id_project_stage_info'].'&project_id='.$data['crm_project_id'].'&id_project_facility='.$data['id_project_facility'];
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);
            $result = json_decode($output);
            if(!empty($result))
            {
                $result = $result->data[0]->facilities;
                /*$result[0]->facility_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_type))[0]['child_name'];
                $result[0]->facility_sub_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_sub_type))[0]['child_name'];
                $result[0]->facility_term_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_term))[0]['child_name'];
                $result[0]->loan_payment_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->loan_payment_type))[0]['child_name'];
                $result[0]->payment_modalities_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->payment_modalities))[0]['child_name'];
                $result[0]->facility_tenor_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_tenor))[0]['child_name'];*/

                /*$result[0]->facility_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_type));
                if(!empty($result[0]->facility_type_name)){ $result[0]->facility_type_name = $result[0]->facility_type_name[0]['child_name']; }
                else{ $result[0]->facility_tenor_name = ''; }

                $result[0]->facility_sub_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_sub_type));
                if(!empty($result[0]->facility_sub_type_name)){ $result[0]->facility_sub_type_name = $result[0]->facility_sub_type_name[0]['child_name']; }
                else{ $result[0]->facility_sub_type_name = ''; }

                $result[0]->facility_term_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_term));
                if(!empty($result[0]->facility_term_name)){ $result[0]->facility_term_name = $result[0]->facility_term_name[0]['child_name']; }
                else{ $result[0]->facility_term_name = ''; }

                $result[0]->loan_payment_type_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->loan_payment_type));
                if(!empty($result[0]->loan_payment_type_name)){ $result[0]->loan_payment_type_name = $result[0]->loan_payment_type_name[0]['child_name']; }
                else{ $result[0]->loan_payment_type_name = ''; }

                $result[0]->payment_modalities_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->payment_modalities));
                if(!empty($result[0]->payment_modalities_name)){ $result[0]->payment_modalities_name = $result[0]->payment_modalities_name[0]['child_name']; }
                else{ $result[0]->payment_modalities_name = ''; }

                $result[0]->facility_tenor_name = $this->Master_model->getMasterChild(array('child_id' => $result[0]->facility_tenor));
                if(!empty($result[0]->facility_tenor_name)){ $result[0]->facility_tenor_name = $result[0]->facility_tenor_name[0]['child_name']; }
                else{ $result[0]->facility_tenor_name = ''; }*/
            }
        }
        if(isset($data['id_project_facility']) && !empty($result)){
            if(isset($result[0]))
                $result = $result[0];
        }
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function facility_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('facility_name', array('required'=> $this->lang->line('facility_name_req')));
        $this->form_validator->add_rules('currency_id', array('required'=> $this->lang->line('currency_req')));
        $this->form_validator->add_rules('loan_amount', array('required'=> $this->lang->line('loan_amount_req'),'float' => $this->lang->line('loan_amount_invalid')));
        $this->form_validator->add_rules('expected_start_date', array('required'=> $this->lang->line('start_date_req')));
        $this->form_validator->add_rules('expected_maturity', array('required'=> $this->lang->line('maturity_req')));
        $this->form_validator->add_rules('expected_maturity_type', array('required'=> $this->lang->line('maturity_type_req')));
        $this->form_validator->add_rules('expected_interest_rate', array('required'=> $this->lang->line('interest_rate_req'),'float' => $this->lang->line('interest_rate_invalid')));
        $this->form_validator->add_rules('project_loan_type_id', array('required'=> $this->lang->line('loan_type_req')));
        $this->form_validator->add_rules('project_payment_type_id', array('required'=> $this->lang->line('payment_type_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $facility_comments = ''; $project_approval_id = 0;
        if(!isset($data['project_facility_status'])){ $data['project_facility_status'] = 'New'; }

        $project_details = $this->Crm_model->getProject($data['crm_project_id']);
        $company_details = $this->Company_model->getCompany(array('id_company' => $project_details[0]['company_id']));
        $currency_value = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));

        if(isset($data['project_approval_id']))
        {
            if($project_details[0]['currency_id']!='' || $project_details[0]['currency_id']!=0){ $company_details->currency_id = $project_details[0]['currency_id']; }
            else
            {
                $this->Crm_model->updateProject(array('currency_id' => $company_details->currency_id),$data['crm_project_id']);
            }
            if(isset($data['project_approval_id'])){ $project_approval_id = $data['project_approval_id']; unset($data['project_approval_id']); }
            if(isset($data['facility_comments'])){ $facility_comments = $data['facility_comments']; unset($data['facility_comments']); }
            $prev_facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id'], 'id_project_facility' => $data['id_project_facility']));
            $new_currency_value = $prev_facility[0]['currency_value'];
            $result = array();
            if(isset($data['id_project_facility']))
            {
                $result = $this->Crm_model->updateFacilities(array('id_project_facility' => $data['id_project_facility'], 'loan_amount' => $data['loan_amount'], 'project_facility_status' => $data['project_facility_status']));
                $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $data['id_project_facility'],'created_by' => $data['created_by'], 'type' => 'update','currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $new_currency_value,'created_date_time' => currentDate()));

                $this->Crm_model->addFacilityActivity(array('created_by' => $data['created_by'], 'project_facility_id' => $data['id_project_facility'], 'project_approval_id' => $project_approval_id, 'facility_amount' => $data['loan_amount'], 'facility_status' => $data['project_facility_status'],'facility_comments' => $facility_comments,'created_date_time' => currentDate()));
            }

            $final_amount = 0;
            $facility_list = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));

            for($s=0;$s<count($facility_list);$s++)
            {
                if($facility_list[$s]['currency_id']==$company_details->currency_id)
                {
                    $final_amount = $final_amount + $facility_list[$s]['loan_amount'];
                }
                else
                {
                    $final_amount = $final_amount + ($facility_list[$s]['loan_amount']*$facility_list[$s]['currency_value']);
                }
            }
            $this->Crm_model->updateProject(array('amount' => $final_amount),$data['crm_project_id']);

            $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($company_details->currency_id!=$data['currency_id']){
            if(empty($currency_value)){
                $result = array('status'=>FALSE,'error'=>array('config' => $this->lang->line('config_currency')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if($project_details[0]['currency_id']!='' || $project_details[0]['currency_id']!=0){ $company_details->currency_id = $project_details[0]['currency_id']; }
        else
        {
            $this->Crm_model->updateProject(array('currency_id' => $company_details->currency_id),$data['crm_project_id']);
        }

        $amount = $result = '';

        if(isset($data['id_project_facility']))
        {
            $prev_facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id'], 'id_project_facility' => $data['id_project_facility']));

            if($company_details->currency_id==$data['currency_id']){
                $amount = $project_details[0]['amount']+$data['loan_amount']-$prev_facility[0]['loan_amount'];
                $new_currency_value = $prev_facility[0]['currency_value'];
                $data['currency_value'] = 0;
            }
            else{
                if($prev_facility[0]['currency_id']==$data['currency_id'])
                {
                    $new_currency_value = $prev_facility[0]['currency_value'];
                    $amount = ($data['loan_amount']*$prev_facility[0]['currency_value']);
                    $amount = $project_details[0]['amount']+$amount-$prev_facility[0]['loan_amount'];
                }
                else
                {
                    $company_currency = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));
                    if(!empty($company_currency))
                    {
                        $amount = ($data['loan_amount']*$company_currency[0]['company_currency_value']);
                        $amount = $project_details[0]['amount']+$amount-$prev_facility[0]['loan_amount'];
                        $new_currency_value = $company_currency[0]['company_currency_value'];
                    }
                    $data['currency_value'] = $company_currency[0]['company_currency_value'];
                }

            }
            $result = $this->Crm_model->updateFacilities($data);
            $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $data['id_project_facility'],'created_by' => $data['created_by'], 'type' => 'update','currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $new_currency_value,'created_date_time' => currentDate()));
        }
        else
        {
            if($company_details->currency_id==$data['currency_id']){
                $amount = $project_details[0]['amount']+$data['loan_amount'];
                $data['currency_value'] = 0;
            }
            else{
                $currency_value = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));

                if(!empty($currency_value)){
                    $amount = ($data['loan_amount']*$currency_value[0]['company_currency_value']+$project_details[0]['amount']);
                }
                if(isset($currency_value[0]['company_currency_value']))
                    $data['currency_value'] = $currency_value[0]['company_currency_value'];
                else
                    $data['currency_value'] = 0;
            }
            $data['created_date_time'] = currentDate();
            $id_project_facility = $this->Crm_model->addFacilities($data);

            $company_currency_value = 0;
            if(isset($currency_value[0]['company_currency_value'])){ $company_currency_value = $currency_value[0]['company_currency_value']; }
            $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $id_project_facility,'created_by' => $data['created_by'], 'type' => 'add', 'currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $company_currency_value,'created_date_time' => currentDate()));
        }

        $final_amount = 0;
        $facility_list = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));

        for($s=0;$s<count($facility_list);$s++)
        {
            if($facility_list[$s]['currency_id']==$company_details->currency_id)
            {
                $final_amount = $final_amount + $facility_list[$s]['loan_amount'];
            }
            else
            {
                $final_amount = $final_amount + ($facility_list[$s]['loan_amount']*$facility_list[$s]['currency_value']);
            }
        }

        $this->Crm_model->updateProject(array('amount' => $final_amount),$data['crm_project_id']);

        //adding module status for financial sheets
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => '', 'reference_type' => 'facility'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => '', 'reference_type' => 'facility', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $data['facility_name'].' facility added','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by'],'created_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function companyFacility_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('company_facility_id', array('required'=> $this->lang->line('company_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result['general'] = $this->Company_model->getCompanyFacility(array('id_company_facility' => $data['company_facility_id']));
        $result['general'] = $result['general'][0];
        $result['pricing'] = $this->Company_model->getCompanyFacilityFields(array('company_facility_id' => $data['company_facility_id'],'field_section' => 'pricing'));
        $result['terms_conditions'] = $this->Company_model->getCompanyFacilityFields(array('company_facility_id' => $data['company_facility_id'],'field_section' => 'terms_conditions'));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function quickFacility_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('id_company_facility', array('required'=> $this->lang->line('company_facility_id_req')));
        $this->form_validator->add_rules('project_facility_name', array('required'=> $this->lang->line('project_facility_name_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $id_project_stage_section_form = 0;
        if(isset($data['id_project_stage_section_form'])) {
            $id_project_stage_section_form = $data['id_project_stage_section_form'];
            unset($data['id_project_stage_section_form']);
        }

        if(isset($data['id_company_facility'])) {
            $data['company_facility_id'] = $data['id_company_facility'];
            unset($data['id_company_facility']);
        }
        //getting company facility details
        $company_facility_details = $this->Company_model->getCompanyFacility(array('id_company_facility' => $data['company_facility_id']));
        if(!empty($company_facility_details)){ $facility_name = $company_facility_details[0]['facility_name']; }
        else{ $facility_name = '----'; }

        //getting current stage details
        $stage_info_details = $this->Project_model->getProjectStageInfo(array('project_id' => $data['crm_project_id'],'stage_id' => $data['stage_id']));
        $data['project_stage_info_id'] = $stage_info_details[0]['id_project_stage_info'];

        if(!isset($data['contract_id'])){ $data['contract_id'] = ''; }
        $insert_data = array(
            /*'crm_project_id' => $data['crm_project_id'],
            'contract_id' => $data['contract_id'],
            'company_facility_id' => $data['company_facility_id'],
            'project_facility_name' => $data['project_facility_name'],
            'facility_description' => $company_facility_details[0]['facility_description'],
            'no_of_terms' => $company_facility_details[0]['no_of_terms'],
            'currency_id' => $company_facility_details[0]['currency_id'],
            'facility_amount' => $company_facility_details[0]['facility_amount'],
            'facility_tenor' => $company_facility_details[0]['facility_tenor'],
            'facility_tenor_description' => $company_facility_details[0]['facility_tenor_description'],
            'facility_purpose' => $company_facility_details[0]['facility_purpose'],
            'facility_source_of_payment' => $company_facility_details[0]['facility_source_of_payment'],
            'facility_security' => $company_facility_details[0]['facility_security'],
            'facility_expected_start_date' => $company_facility_details[0]['facility_expected_start_date'],
            'facility_maturity_date' => $company_facility_details[0]['facility_maturity_date'],
            'facility_term_type' => $company_facility_details[0]['facility_term_type'],
            'withdrawl_expiry_date' => $company_facility_details[0]['withdrawl_expiry_date']*/
                'crm_project_id' => $data['crm_project_id'],
                'contract_id' => $data['contract_id'],
                'company_facility_id' => $data['company_facility_id'],
                'project_facility_name' => $data['project_facility_name'],
                'facility_description' => $company_facility_details[0]['facility_description'],
                'facility_type' => $company_facility_details[0]['facility_type'],
                'facility_type_description' => $company_facility_details[0]['facility_type_description'],
                'facility_sub_type' => $company_facility_details[0]['facility_sub_type'],
                'facility_term' => $company_facility_details[0]['facility_term'],
                'loan_payment_type' => $company_facility_details[0]['loan_payment_type'],
                'payment_modalities' => $company_facility_details[0]['payment_modalities'],
                'currency_id' => $company_facility_details[0]['currency_id'],
                'facility_amount' => $company_facility_details[0]['facility_amount'],
                'amount_description' => $company_facility_details[0]['amount_description'],
                'facility_tenor' => $company_facility_details[0]['facility_tenor'],
                'facility_tenor_description' => $company_facility_details[0]['facility_tenor_description'],
                'facility_purpose' => $company_facility_details[0]['facility_purpose'],
                'facility_source_of_payment' => $company_facility_details[0]['facility_source_of_payment'],
                'lender_type' => $company_facility_details[0]['lender_type'],
                'credit_risk' => $company_facility_details[0]['credit_risk'],
                'guarantee_type' => $company_facility_details[0]['guarantee_type'],
                'other_facility_modalities' => $company_facility_details[0]['other_facility_modalities'],
                'facility_security' => $company_facility_details[0]['facility_security'],
                'facility_expected_start_date' => $company_facility_details[0]['facility_expected_start_date'],
                'facility_maturity_date' => $company_facility_details[0]['facility_maturity_date'],
                'facility_term_type' => $company_facility_details[0]['facility_term_type'],
                'drawdown_expiry_date' => $company_facility_details[0]['drawdown_expiry_date'],
                'lender_percentage' => $company_facility_details[0]['lender_percentage'],
                'project_stage_info_id' => $data['project_stage_info_id']
        );
        $insert_data['updated_by'] = $data['created_by'];
        $insert_data['updated_date_time'] = currentDate();
        $insert_data['created_by'] = $data['created_by'];
        $insert_data['created_date_time'] = currentDate();
        $project_facility_id = $result = $this->Crm_model->addFacilities($insert_data);
        $msg = $this->lang->line('facility_add');

        $fields = $this->Company_model->getCompanyFacilityFields(array('field_section' => 'pricing','company_facility_id' => $data['company_facility_id']));
        $fields1 = $this->Company_model->getCompanyFacilityFields(array('field_section' => 'terms_conditions','company_facility_id' => $data['company_facility_id']));
        $fields = array_merge($fields,$fields1);
        for($s=0;$s<count($fields);$s++){
            $add[] = array(
                'project_facility_id' => $project_facility_id,
                'facility_field_id' => $fields[$s]['facility_field_id'],
                'currency_id' => $fields[$s]['currency_id'],
                'time_period_id' => $fields[$s]['time_period_id'],
                'facility_amount_type' => $fields[$s]['facility_amount_type'],
                'facility_field_value' => $fields[$s]['facility_field_value'],
                'facility_field_description' => $fields[$s]['facility_field_description'],
                'created_date_time' => currentDate(),
                'updated_date_time' => currentDate()
            );
        }
        if(!empty($add)){ $this->Crm_model->addProjectFacilityData($add); }
        $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_name.' facility added','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => $project_facility_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $form_details = $this->Crm_model->getFormDetails(array('form_key' => 'facility_information'));
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>$id_project_stage_section_form, 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>$id_project_stage_section_form, 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));


        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'],'project_stage_section_form_id' => $id_project_stage_section_form));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'],'project_stage_section_form_id' => $id_project_stage_section_form,'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$project_facility_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('field_section', array('required'=> $this->lang->line('field_section_req')));
        $this->form_validator->add_rules('id_company_facility', array('required'=> $this->lang->line('company_facility_id_req')));
        if($data['field_section']=='general') {
            $this->form_validator->add_rules('facility_description', array('required' => $this->lang->line('project_facility_description_req')));
            $this->form_validator->add_rules('currency_id', array('required' => $this->lang->line('currency_req')));
            $this->form_validator->add_rules('facility_amount', array('required' => $this->lang->line('project_facility_amount_req')));
            $this->form_validator->add_rules('facility_usd_amount', array('required' => $this->lang->line('project_facility_usd_amount_req')));
            $this->form_validator->add_rules('project_facility_name', array('required' => $this->lang->line('project_facility_name_req')));

        }
        else{
            $this->form_validator->add_rules('id_company_facility', array('required'=> $this->lang->line('company_facility_id_req')));
        }
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result_data = array();
        //checking facility already exists or not
        /*$check_facility = $this->Crm_model->getProjectFacility(array('crm_project_id' => $data['crm_project_id'],'company_facility_id' => $data['id_company_facility']));
        if(!empty($check_facility)){
            if(isset($data['id_project_facility'])){
                if($check_facility[0]['id_project_facility']!=$data['id_project_facility']){
                    $result = array('status'=>FALSE,'error'=>array('id_company_facility' => $this->lang->line('company_facility_id_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else{
                $result = array('status'=>FALSE,'error'=>array('id_company_facility' => $this->lang->line('company_facility_id_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

        }*/
        $id_project_stage_section_form = 0;
        if(isset($data['id_project_stage_section_form'])) {
            $id_project_stage_section_form = $data['id_project_stage_section_form'];
            unset($data['id_project_stage_section_form']);
        }
        if(isset($data['id_company_facility'])) {
            $data['company_facility_id'] = $data['id_company_facility'];
            unset($data['id_company_facility']);
        }
        //getting company facility details
        $company_facility_details = $this->Company_model->getCompanyFacility(array('id_company_facility' => $data['company_facility_id']));
        if(!empty($company_facility_details)){ $facility_name = $company_facility_details[0]['facility_name']; }
        else{ $facility_name = '----'; }

        $project_facility_id = 0;
        if(isset($data['id_project_facility'])){
            $project_facility_id = $data['id_project_facility'];
        }
        if(!isset($data['contract_id'])){ $data['contract_id'] = ''; }
        if(!isset($data['lender_percentage'])){ $data['lender_percentage'] = ''; }
        if(!isset($data['guarantee_type'])){ $data['guarantee_type'] = ''; }
        if(!isset($data['facility_term'])){ $data['facility_term'] = ''; }
        if(!isset($data['facility_sub_type'])){ $data['facility_sub_type'] = ''; }
        if(isset($data['facility_amount']))
            if(!isset($data['facility_usd_amount'])){ $data['facility_usd_amount'] = $data['facility_amount']; }

        if($data['field_section']=='general')
        {
            $insert_data = array(
                'crm_project_id' => $data['crm_project_id'],
                'contract_id' => $data['contract_id'],
                'company_facility_id' => $data['company_facility_id'],
                'project_facility_name' => $data['project_facility_name'],
                'facility_description' => $data['facility_description'],
                'facility_type' => $data['facility_type'],
                'facility_type_description' => $data['facility_type_description'],
                'facility_sub_type' => $data['facility_sub_type'],
                'facility_term' => $data['facility_term'],
                'loan_payment_type' => $data['loan_payment_type'],
                /*'payment_modalities' => $data['payment_modalities'],*/
                'currency_id' => $data['currency_id'],
                'facility_amount' => $data['facility_amount'],
                'amount_description' => $data['amount_description'],
                'facility_tenor' => $data['facility_tenor'],
                'facility_tenor_description' => $data['facility_tenor_description'],
                'facility_purpose' => $data['facility_purpose'],
                'facility_source_of_payment' => $data['facility_source_of_payment'],
                'lender_type' => $data['lender_type'],
                'credit_risk' => $data['credit_risk'],
                'guarantee_type' => $data['guarantee_type'],
                'other_facility_modalities' => $data['other_facility_modalities'],
                'facility_security' => $data['facility_security'],
                'facility_expected_start_date' => $data['facility_expected_start_date'],
                'facility_maturity_date' => $data['facility_maturity_date'],
                'facility_term_type' => $data['facility_term_type'],
                'drawdown_expiry_date' => $data['drawdown_expiry_date'],
                'lender_percentage' => $data['lender_percentage'],
                'facility_usd_amount' => $data['facility_usd_amount'],
                'source_of_payment' => $data['source_of_payment']
            );
            $insert_data['updated_by'] = $data['created_by'];
            $insert_data['updated_date_time'] = currentDate();

            if(!isset($data['id_project_facility'])) { //inserting general and remaining facility section (admin side) data
                $insert_data['created_by'] = $data['created_by'];
                $insert_data['created_date_time'] = currentDate();
                $project_facility_id = $result = $this->Crm_model->addFacilities($insert_data);
                $msg = $this->lang->line('facility_add');

                $fields = $this->Company_model->getCompanyFacilityFields(array('field_section' => 'pricing','company_facility_id' => $data['company_facility_id']));
                $fields1 = $this->Company_model->getCompanyFacilityFields(array('field_section' => 'terms_conditions','company_facility_id' => $data['company_facility_id']));
                $fields = array_merge($fields,$fields1);
                for($s=0;$s<count($fields);$s++){
                    $add[] = array(
                        'project_facility_id' => $project_facility_id,
                        'facility_field_id' => $fields[$s]['facility_field_id'],
                        'currency_id' => $fields[$s]['currency_id'],
                        'time_period_id' => $fields[$s]['time_period_id'],
                        'facility_amount_type' => $fields[$s]['facility_amount_type'],
                        'facility_field_value' => $fields[$s]['facility_field_value'],
                        'facility_field_description' => $fields[$s]['facility_field_description'],
                        'created_date_time' => currentDate(),
                        'updated_date_time' => currentDate()
                    );
                }
                if(!empty($add)){ $this->Crm_model->addProjectFacilityData($add); }
                $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_name.' facility added','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => $project_facility_id,'created_by' => $data['created_by'],'created_date_time' => currentDate()));

                //getting current facility details for first time insert to return application for getting project facility field data ids
                $result_data = $this->Crm_model->getFacilities(array('id_project_facility' => $project_facility_id,'project_facility_status' => 'active'));
                for($s=0;$s<count($result);$s++){
                    $result_data[$s]['pricing'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result_data[$s]['id_project_facility'],'field_section' => 'pricing'));
                    $result_data[$s]['terms_conditions'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result_data[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
                }
                $result_data = $result_data[0];
            }
            else{
                //facility update
                $project_facility_id = $insert_data['id_project_facility'] = $data['id_project_facility'];
                //echo "<pre>"; print_r($insert_data); exit;

                $result = $this->Crm_model->updateFacilities($insert_data);
                $msg = $this->lang->line('facility_update');
                $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_name.' facility updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by'],'created_date_time' => currentDate()));
            }
        }
        else
        {
            //getting fields to compare app data
            $fields = $this->Company_model->getCompanyFacilityFields(array('field_section' => $data['field_section'],'company_facility_id' => $data['company_facility_id']));
            $project_facility_field_data = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $project_facility_id));
            if(!empty($project_facility_field_data)){
                $project_facility_field_data_id = array_map(function($i){ return $i['id_project_facility_field_data']; },$project_facility_field_data);
            }
            else{
                $project_facility_field_data_id = array();
            }
            $add = $update = array();
            $data1 = $data['data'];
            //echo "<pre>"; print_r($data1); exit;
            for($s=0;$s<count($fields);$s++){
                for($r=0;$r<count($data1);$r++){
                    if($fields[$s]['facility_field_id']==$data1[$r]['id_facility_field']){
                        //echo $data1[$r]['facility_field_id'].'--';
                        if(!isset($data1[$r]['currency_id'])){ $data1[$r]['currency_id']=''; }
                        if(!isset($data1[$r]['time_period_id'])){ $data1[$r]['time_period_id']=''; }
                        if(!isset($data1[$r]['facility_amount_type'])){ $data1[$r]['facility_amount_type']=''; }

                        if(isset($data1[$r]['id_project_facility_field_data']) && in_array($data1[$r]['id_project_facility_field_data'],$project_facility_field_data_id)) {
                            $update[] = array(
                                'id_project_facility_field_data' => $data1[$r]['id_project_facility_field_data'],
                                'currency_id' => $data1[$r]['currency_id'],
                                'time_period_id' => $data1[$r]['time_period_id'],
                                'facility_amount_type' => $data1[$r]['facility_amount_type'],
                                'facility_field_value' => $data1[$r]['facility_field_value'],
                                'facility_field_description' => $data1[$r]['facility_field_description'],
                                'updated_date_time' => currentDate()
                            );
                        }
                        else{

                            $add[] = array(
                                'project_facility_id' => $project_facility_id,
                                'facility_field_id' => $fields[$s]['facility_field_id'],
                                'currency_id' => $data1[$r]['currency_id'],
                                'time_period_id' => $data1[$r]['time_period_id'],
                                'facility_amount_type' => $data1[$r]['facility_amount_type'],
                                'facility_field_value' => $data1[$r]['facility_field_value'],
                                'facility_field_description' => $data1[$r]['facility_field_description'],
                                'created_date_time' => currentDate(),
                                'updated_date_time' => currentDate()
                            );
                        }
                    }
                }
            }
            //echo "<pre>"; print_r($add); echo "<pre>"; print_r($update); exit;
            if(!empty($add)){ $this->Crm_model->addProjectFacilityData($add); }
            if(!empty($update)){ $this->Crm_model->updateProjectFacilityData($update); }

            $msg = $this->lang->line('info_update');
            $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_name.' facility updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by'],'created_date_time' => currentDate()));
        }
        //$form_details = $this->Crm_model->getFormDetails(array('form_key' => 'facility_information'));
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>$id_project_stage_section_form, 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>$id_project_stage_section_form, 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'],'project_stage_section_form_id' => $id_project_stage_section_form));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'],'project_stage_section_form_id' => $id_project_stage_section_form,'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$result_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('project_facility_id', array('required'=> $this->lang->line('project_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $facility_data = array(
            'crm_project_id' => $data['crm_project_id'],
            'id_project_facility' => $data['project_facility_id'],
            'project_facility_status' => 'inactive'
        );

        $this->Crm_model->updateFacilities($facility_data);

        $this->Activity_model->deleteTouchPoint(array(
            'activity_type' => 'facility',
            'activity_reference_id' => $data['project_facility_id']
        ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('facility_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentComment_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> $this->lang->line('document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getKnowledgeDocumentComments($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentComment_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> $this->lang->line('document_id_req')));
        $this->form_validator->add_rules('commented_by', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('comment', array('required'=> $this->lang->line('comment_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_knowledge_document_comment']))
            $result = $this->Crm_model->updateKnowledgeDocumentComments($data);
        else {
            $data['created_date_time'] = currentDate();
            $result = $this->Crm_model->addKnowledgeDocumentComments($data);
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentDetails_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> $this->lang->line('document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getKnowledgeDocuments(array('knowledge_document_id' => $data['knowledge_document_id']));
        for($s=0;$s<count($result);$s++)
        {
            $file = explode('/',$result[$s]['document_source']);
            if($result[$s]['document_type']=='document'){
                $result[$s]['document_source'] = getImageUrl($result[$s]['document_source'],'file');
                $result[$s]['size']=get_file_info($result[$s]['document_source'], 'size');
            }
            $file_name = '';
            if(isset($file[1])){ $file_name = $file[1]; }
            $result[$s]['file_name'] = $file_name;
            $result[$s]['comments'] = $this->Crm_model->getKnowledgeDocumentComments(array('knowledge_document_id' => $data['knowledge_document_id']));
        }
        if(!empty($result[0]))
            $result = $result[0];
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgePublish_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> $this->lang->line('document_id_req')));
        $this->form_validator->add_rules('knowledge_document_status', array('required'=> $this->lang->line('status_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->updateKnowledgeDocument(array('id_knowledge_document' => $data['knowledge_document_id'], 'knowledge_document_status' => $data['knowledge_document_status']));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        $result = $this->Crm_model->getCurrency($data);
        array_push($result,array('id_currency' => $company_details->currency_id, 'currency_code' => $company_details->currency_code));
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoint_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('crm_module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('offset', array('required'=> $this->lang->line('offset_req')));
        $this->form_validator->add_rules('limit', array('required'=> $this->lang->line('limit_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(($data['crm_module_type']!='contact') && ($data['crm_module_type']!='company') && ($data['crm_module_type']!='project') && ($data['crm_module_type']!='collateral'))
        {
            $result = array('status'=>FALSE,'error'=>array('module_type' => 'Invalid module type'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['offset'])){ $data['offset'] = 0; }
        if(!isset($data['limit'])){ $data['limit'] = 5; }

        if($data['crm_module_type']=='project')
        {
            $project_details = $this->Crm_model->getProject($data['crm_module_id']);
            if($project_details[0]['sub_sector']!=''){ $data['sector'] = $project_details[0]['sub_sector']; }
            else if($project_details[0]['sector']!=''){ $data['sector'] = $project_details[0]['sector']; }
            else{ $data['sector']=''; }
        }
        else if($data['crm_module_type']=='company')
        {
            $company_details = $this->Crm_model->getCompany($data['crm_module_id']);
            if($company_details[0]['sub_sector']!=''){ $data['sector'] = $company_details[0]['sub_sector']; }
            else if($company_details[0]['sector']!=''){ $data['sector'] = $company_details[0]['sector']; }
            else{ $data['sector']=''; }
        }
        else if($data['crm_module_type']=='contact' || $data['crm_module_type']=='collateral')
        {
            $data['sector']='';
        }
        if(!isset($data['grade']))
            $data['grade'] = '';
        $result = $this->Company_model->getTouchPoints($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $this->input->post();
        if(isset($data['id_collateral'])) {
            $this->form_validator->add_rules('project_facility_id', array('required' => $this->lang->line('facility_req')));
            $this->form_validator->add_rules('id_collateral', array('required' => $this->lang->line('collateral_req')));

        }
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_project_id = $data['crm_project_id'];
        if(isset($data['crm_project_id']))
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'collateral'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'collateral', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        }


        unset($data['crm_project_id']);

        if(!isset($data['id_collateral']))
        {
            $data['created_date_time'] = currentDate();
            $result = $collateral_id = $this->Crm_model->addCollateral($data);
            if(isset($data['project_facility_id'])){
                $this->Crm_model->addFacilityCollateral(array('facility_id' => $data['project_facility_id'], 'collateral_id' => $collateral_id,'created_date_time' => currentDate()));
                $facility_collateral_details = $this->Crm_model->getFacilityCollateral(array('facility_id' => $data['project_facility_id'],'collateral_id' => $collateral_id));

                $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_collateral_details[0]['collateral_code'].' collateral has been added to '.$facility_collateral_details[0]['facility_name'].' facility.','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by'],'created_date_time' => currentDate()));
            }
            $message = $this->lang->line('collateral_add');
        }
        else
        {
            if(isset($data['project_facility_id'])){
                $this->Crm_model->addFacilityCollateral(array('facility_id' => $data['project_facility_id'], 'collateral_id' => $data['id_collateral'],'created_date_time' => currentDate()));
                $facility_collateral_details = $this->Crm_model->getFacilityCollateral(array('facility_id' => $data['project_facility_id'],'collateral_id' => $data['id_collateral']));

                $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => $facility_collateral_details[0]['collateral_code'].' collateral has been added to '.$facility_collateral_details[0]['facility_name'].' facility.','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by'],'created_date_time' => currentDate()));
            }
            $result = $this->Crm_model->updateCollateral($data);
            $message = $this->lang->line('collateral_update');
        }

        $result = array('status'=>TRUE, 'message' => $message, 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('facility_id', array('required'=> $this->lang->line('facility_req')));
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Crm_model->deleteFacilityCollateral($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_collateral', array('required'=> $this->lang->line('collateral_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_details = $this->Crm_model->getCollateralDetails($data['id_collateral']);
        if(!empty($collateral_details)) $collateral_details = $collateral_details[0];
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$collateral_details);
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function collateralStageFormData_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_stage = $this->Crm_model->getCollateralTypeStages($data);
        $dates = array();
        for($cs = 0; $cs < count($collateral_stage); $cs++)
        {
            $data['collateral_stage_id'] = $collateral_stage[$cs]['id_collateral_stage'];
            $collateral_stage[$cs]['form_data'] = array();

            $collateral_stage_fields = $this->Crm_model->getCollateralStageFieldData($data);

            $collateral_stage_field_data = '';
            for($csf=0; $csf < count($collateral_stage_fields); $csf++)
            {
                if($collateral_stage_fields[$csf]['collateral_stage_field_value']!=''){ $collateral_stage_fields[$csf]['collateral_stage_field_value'] = json_decode($collateral_stage_fields[$csf]['collateral_stage_field_value']); }
                else{ $collateral_stage_fields[$csf]['collateral_stage_field_value'] = array(); }
                $collateral_stage_field_data[$collateral_stage_fields[$csf]['collateral_stage_filed_name']] = $collateral_stage_fields[$csf]['collateral_stage_field_value'];
            }

            if(empty($collateral_stage_field_data)){
                $collateral_stage[$cs]['form_data'] = new stdClass();
            }
            else{ $collateral_stage[$cs]['form_data'] = $collateral_stage_field_data; }
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=> $collateral_stage);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFormData_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('date', array('required'=> $this->lang->line('date_req')));
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> $this->lang->line('collateral_stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Crm_model->deleteCollateralStageFormData($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getCollateralList($data);
        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }
        $total_records = count($this->Crm_model->getCollateralList($data));

        if(isset($data['status']) and $data['status']=='pending')
            $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        else
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function collateralTypeDocuments_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getCollateralTypeCategories($data);
        for($r=0;$r<count($result);$r++)
        {
            $result[$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$r]['id_assessment_question_category'], 'question_status' => 1));
            for($sr=0;$sr<count($result[$r]['question']);$sr++)
            {
                if(isset($result[$r]['question'][$sr]['question_option']) && $result[$r]['question'][$sr]['question_option']!=''){
                    $result[$r]['question'][$sr]['question_option'] = json_decode($result[$r]['question'][$sr]['question_option']);
                }
                $result[$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$r]['question'][$sr]['id_assessment_question'], 'collateral_id' => $data['collateral_id']));
                if(isset($result[$r]['question'][$sr]['answer']->assessment_answer) && $result[$r]['question'][$sr]['answer']->assessment_answer!=''){
                    if($result[$r]['question'][$sr]['question_type']=='checkbox')
                        $result[$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$r]['question'][$sr]['answer']->assessment_answer);
                    else if($result[$r]['question'][$sr]['question_type']=='file')
                        $result[$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$r]['question'][$sr]['answer']->assessment_answer,'file');
                }
                if(isset($result[$r]['question'][$sr]['answer']->comment_file) && $result[$r]['question'][$sr]['answer']->comment_file!=''){
                    $result[$r]['question'][$sr]['answer']->comment_file = getImageUrl($result[$r]['question'][$sr]['answer']->comment_file,'file');
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFormData_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> $this->lang->line('collateral_stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getCollateralStageFormFields($data);
        $collateral_stage_form_data = $this->Crm_model->getCollateralStageFormData($data);
        if(empty($collateral_stage_form_data)) $type = 'insert'; else $type = 'update';
        $collateralId = $data['collateral_id'];
        unset($data['collateral_id']);
        $collateralStageId = $data['collateral_stage_id'];
        unset($data['collateral_stage_id']);
        $data_keys = array_keys($data);
        for($s=0;$s<count($form_fields);$s++){
            $check_field = 0;
            $form_data[$st]['collateral_id'] = $collateralId;
            $form_data[$st]['collateral_stage_id'] = $collateralStageId;
            $form_data[$st]['collateral_stage_field_id'] = $form_fields[$s]['id_collateral_stage_field'];
            $form_data[$st]['created_date_time'] = currentDate();
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['collateral_stage_filed_name']==$data_keys[$r]){
                    $form_data[$st]['collateral_stage_field_value'] = json_encode($data[$data_keys[$r]]);
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['collateral_stage_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            $this->Crm_model->addCollateralStageFormData($form_data);
        }
        else
        {
            if(!empty($collateral_stage_form_data)){
                for($s=0;$s<count($collateral_stage_form_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($collateral_stage_form_data[$s]['collateral_stage_field_id']==$form_data[$r]['collateral_stage_field_id']){
                            $form_data[$r]['id_collateral_stage_field_data'] = $collateral_stage_form_data[$s]['id_collateral_stage_field_data'];
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_collateral_stage_field_data'])){
                    $form_data[$sr]['created_date_time'] = currentDate();
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            if(!empty($new_form_fields))
                $this->Crm_model->addCollateralStageFormData($new_form_fields);
            $this->Crm_model->updateCollateralStageFormData($form_data);
        }
        $this->Activity_model->addActivity(array('activity_name' => 'Collateral Valuation','activity_template' => 'Collateral Valuation has been Updated','module_type' => 'collateral','module_id' =>  $collateralId, 'activity_type' => 'form','created_by' => $data['user_id'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> []);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project = $this->Crm_model->getProject($data['crm_project_id']);
        $data['sector_id']=0;
        if($project[0]['project_sub_sector_id']!='' && $project[0]['project_sub_sector_id']!=0){ $data['sector_id'] = $project[0]['project_sub_sector_id']; }
        else { $data['sector_id'] = $project[0]['project_main_sector_id']; }

        $covenant = $this->Crm_model->getProjectCovenant($data);

        $result = array();
        $covenant_category_id = $covenant_id = $project_covenant_id = array();
        for($s=0;$s<count($covenant);$s++)
        {
            if(!in_array($covenant[$s]['id_covenant_category'],$covenant_category_id))
            {
                //getting previous category covenants like objects
                if(isset($covenant[$s-1])){
                    $result[$covenant[$s-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[$s-1]['id_covenant_category']]['covenant']);
                }

                array_push($covenant_category_id,$covenant[$s]['id_covenant_category']);
                $result[$covenant[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $covenant[$s]['id_covenant_category'],
                    'covenant_category' => $covenant[$s]['covenant_category'],
                    'covenant' => array()
                );

                if($covenant[$s]['id_covenant']!='')
                {
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by'],
                        'is_checked' => $covenant[$s]['is_checked']
                    );
                }
            }
            else
            {
                if($covenant[$s]['id_covenant']!=''){
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by'],
                        'is_checked' => $covenant[$s]['is_checked']
                    );
                }
            }
        }

        if(isset($covenant[count($covenant)-1])){
            $result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant']);
        }

        //getting covenant category like object
        $result = array_values($result);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function projectCovenant_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $categories = $this->Company_model->getCovenantCategoryList($data);
        $covenant = array();
        for($s=0;$s<count($categories);$s++)
        {
            $data['covenant_category_id'] = $categories[$s]['id_covenant_category'];
            $covenant[] = array(
                'id_covenant_category' => $categories[$s]['id_covenant_category'],
                'covenant_category' => $categories[$s]['covenant_category'],
                'covenant_type_id' => $categories[$s]['covenant_type_id'],
                'sector' => $categories[$s]['sector'],
                'company_id' => $categories[$s]['company_id'],
                'covenant' => $this->Crm_model->getSelectedProjectCovenants($data)
            );

        }

        $result = array();
        $covenant_category_id = $covenant_id = $project_covenant_id = array();
        for($s=0;$s<count($covenant);$s++)
        {
            if(!in_array($covenant[$s]['id_covenant_category'],$covenant_category_id))
            {
                //getting previous category covenants like objects
                if(isset($covenant[$s-1])){
                    $result[$covenant[$s-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[$s-1]['id_covenant_category']]['covenant']);
                }

                array_push($covenant_category_id,$covenant[$s]['id_covenant_category']);
                $result[$covenant[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $covenant[$s]['id_covenant_category'],
                    'covenant_category' => $covenant[$s]['covenant_category'],
                    'covenant' => array()
                );

                if(isset($covenant[$s]['id_covenant']) && $covenant[$s]['id_covenant']!='')
                {
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by']
                    );
                }
            }
            else
            {
                if(isset($covenant[$s]['id_covenant']) && $covenant[$s]['id_covenant']!=''){
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by']
                    );
                }
            }
        }

        if(isset($covenant[count($covenant)-1])){
            $result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant']);
        }

        //getting covenant category like object
        $result = array_values($result);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $covenant);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('id_covenant_category', array('required'=> $this->lang->line('covenant_category_id_req')));
        //$this->form_validator->add_rules('covenant_ids', array('required'=> $this->lang->line('covenant_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $covenant_ids = array();
        if(isset($data['covenant_ids']) && $data['covenant_ids']!='')
            $covenant_ids = explode(',',$data['covenant_ids']);

        $add = $update = $delete = array();
        $prev_data = $this->Crm_model->getProjectCovenantByProjectId(array('crm_project_id' => $data['crm_project_id'],'covenant_type_key' => $data['covenant_type_key'],'covenant_category_id' => $data['id_covenant_category']));
        if(!empty($prev_data))
            $prev_covenant_id = array_values(array_unique(array_map(function ($i) { return $i['covenant_id']; }, $prev_data)));
        else $prev_covenant_id = array();

        if(!empty($covenant_ids))
            for($s=0;$s<count($covenant_ids);$s++)
            {
                if(!in_array($covenant_ids[$s],$prev_covenant_id)){
                    $add[] = array(
                        'project_id' => $data['crm_project_id'],
                        'covenant_id' => $covenant_ids[$s],
                        'created_by' => $data['created_by'],
                        'created_date_time' => currentDate()
                    );
                }
            }

        for($s=0;$s<count($prev_data);$s++)
        {
            if(!in_array($prev_data[$s]['covenant_id'],$covenant_ids)){
                $this->Crm_model->deleteProjectCovenant(array('covenant_id' => $prev_data[$s]['covenant_id'],'project_id' => $data['crm_project_id']));
            }
        }

        if(!empty($add)){ $this->Crm_model->addProjectCovenant($add); }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('covenant_id', array('required'=> $this->lang->line('covenant_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->deleteProjectCovenant(array('covenant_id' => $data['covenant_id'],'project_id' => $data['crm_project_id']));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getChildNodes($data,$parent_id)
    {
        for($s=0;$s<count($data);$s++){
            if($data[$s]['parent_module_id']==$parent_id){
                if( $data[$s]['sub_module']==0){
                    for($st=0;$st<count($data);$st++){
                        if($data[$s]['id_module']==$data[$st]['module_id']){
                            $this->order_data[] = array(
                                $data[$st]['action_key'] => ($data[$st]['checked']==1)? true : false
                            );
                        }
                    }
                }
                $this->getChildNodes($data,$data[$s]['id_module']);
            }
        }
        return $this->order_data;
    }

    public function moduleAccess_get()
    {
        $data = $this->input->get();
        //$this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_key', array('required'=> $this->lang->line('module_key_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $applicationUserRole = $this->Company_model->getApplicationUserRole($data);

        if(!empty($applicationUserRole)){
            $data['application_role_id'] = $applicationUserRole[0]['application_role_id'];
        }
        else{

            //$company_approval_role = $this->Company_model->getCompanyUsers(array('company_id' => $data['company_id'],'user_id' => $data['user_id']));
            $company_approval_role = $this->Company_model->getCompanyUsers(array('user_id' => $data['user_id']));
            $data['company_approval_role_id'] = $company_approval_role[0]['company_approval_role_id'];
            unset($data['user_id']);
            $applicationUserRole = $this->Company_model->getApplicationUserRole($data);
            if(!empty($applicationUserRole))
                $data['application_role_id'] = $applicationUserRole[0]['application_role_id'];
            else{
                $data['application_role_id'] = 0;
            }
        }
        $result = $this->Company_model->getModules($data);
        $module_data = $this->order_data = array();
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['module_key']==$data['module_key'])
            {
                if( $result[$s]['sub_module']==0){
                    for($st=0;$st<count($result);$st++){
                        if($result[$s]['id_module']==$result[$st]['module_id']){
                            $this->order_data[] = array(
                                $result[$st]['action_key'] => ($result[$st]['checked']==1)? true : false
                            );
                        }
                    }
                }
                $this->getChildNodes($result,$result[$s]['id_module']);
                break;
            }
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$this->order_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getAllCompaniesList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getAllCompaniesList($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyProjectFacilities_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $current_project_details = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['crm_project_id']));
        if(!empty($current_project_details))
            $project_company_id = $current_project_details[0]['crm_company_id'];
        else
            $project_company_id = 0;

        $other_projects = $this->Crm_model->getProjectCompanyList(array('crm_company_id' => $project_company_id));
        $facilities = array();
        $currency_code = $national_amount = $outstanding_amount = 0;
        if(!empty($other_projects)){
            $other_project_ids = explode(',',$other_projects[0]['crm_project_id']);
            foreach($other_project_ids as $id){
                if($data['crm_project_id']!=$id) {
                    $fac = $this->Crm_model->getExistingFacilitiesByProjectId(array('crm_project_id' => $id));
                    foreach ($fac as $item) {
                        array_push($facilities, $item);
                        $currency_code = $item['currency_code'];
                        $national_amount = $national_amount+$item['notional'];
                        $outstanding_amount = $outstanding_amount+$item['outstanding'];
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>array('data' => $facilities,'total' => array('currency' => $currency_code,'notional' => $national_amount, 'outstanding' => $outstanding_amount)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectRiskFactors_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectRiskItems($data);

        if(!empty($result)){
            $result= $result[0];
            if(isset($result['risk_items'])){
                $result['risk_items'] = json_decode($result['risk_items']);
            }
        }
        else{ $result = new stdClass(); }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectRiskFactors_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('user_req')));
        $this->form_validator->add_rules('risk_items', array('required'=> $this->lang->line('risk_factors_req')));
        $this->form_validator->add_rules('risk_comment', array('required'=> $this->lang->line('risk_comment_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['risk_items'])){ $data['risk_items'] = json_encode($data['risk_items']); }

        if(isset($data['id_project_risk_item']))
            $this->Crm_model->updateProjectRiskItems($data);
        else {
            $data['created_date_time'] = currentDate();
            $this->Crm_model->addProjectRiskItems($data);
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'risk'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'risk', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        if(isset($data['id_project_risk_item'])){
            $id_project_risk_item = $data['id_project_risk_item'];
            $msg = $this->lang->line('risk_assessment_info_update'); }
        else{
            $id_project_risk_item = 0;
            $msg = $this->lang->line('risk_assessment_info_add');
        }
        $this->Activity_model->addActivity(array('activity_name' => 'Risk Assessment','activity_template' => $msg,'module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'term','activity_reference_id' =>$id_project_risk_item,'created_by' => $data['created_by'],'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' =>$msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralProjectTabInfo_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company = $this->Company_model->getCompanyById($data['company_id']);
        $pipeline = $this->Crm_model->getCollateralProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'Pending'));
        $submitted = $this->Crm_model->getCollateralProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'pre-approved'));
        $approved = $this->Crm_model->getCollateralProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'Approved'));
        $monitoring = $this->Crm_model->getCollateralProjectTabsCount(array('company_id' => $data['company_id'], 'status' => 'monitoring'));

        if(empty($pipeline)){ $pipeline[0]['records'] = 0; $pipeline[0]['amount'] = 0; }
        if(empty($submitted)){ $submitted[0]['records'] = 0; $submitted[0]['amount'] = 0; }
        if(empty($approved)){ $approved[0]['records'] = 0; $approved[0]['amount'] = 0; }
        if(empty($monitoring)){ $monitoring[0]['records'] = 0; $monitoring[0]['amount'] = 0; }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array(
            'pipeline' =>array('DEALS' => $pipeline[0]['records'],$company->currency_code => $pipeline[0]['amount']),
            'submitted' =>array('DEALS' => $submitted[0]['records'],$company->currency_code => $submitted[0]['amount']),
            'approved' =>array('DEALS' => $approved[0]['records'],$company->currency_code => $approved[0]['amount']),
            'monitoring' =>array('DEALS' => $monitoring[0]['records'],$company->currency_code => $monitoring[0]['amount']),
        ));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralProjectList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $conditions = $result = array();
        $category = array(
            array('type' => 'pipeline','status' => 'Pending'),
            array('type' => 'submitted','status' => 'pre-approved'),
            array('type' => 'approved','status' => 'Approved'),
            array('type' => 'monitoring','status' => 'monitoring')
        );
        $conditions['company_id'] =$data['company_id'];
        if(isset($data['search'])){ $data['search'] = (array)json_decode($data['search']); }

        if(isset($data['search']['category']))
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            if(isset($data['offset']))
                $conditions['offset'] = $data['offset'];
            else
                $conditions['offset'] = 0;

            if(isset($data['limit']))
                $conditions['limit'] = $data['limit'];
            else
                $conditions['limit'] = 10;

            for($s=0;$s<count($category);$s++)
            {
                $result[$category[$s]['type']]['data'] = array();
                $result[$category[$s]['type']]['count'] = 0;

                if($category[$s]['type']==$data['search']['category'])
                {
                    $conditions['status'] = $category[$s]['status'];
                    $result[$category[$s]['type']]['data'] = $this->Crm_model->getCollateralProjectList($conditions);
                    $total = $this->Crm_model->getCollateralProjectCount($conditions);
                    $result[$category[$s]['type']]['count'] = $total[0]['total'];
                }
            }
        }
        else
        {
            if(isset($data['search']['search_key']))
                $conditions['search_key'] = $data['search']['search_key'];
            $conditions['offset'] = 0;
            $conditions['limit'] = 3;

            if(isset($data['search']['sector']))
                $conditions['sector_id'] = $data['search']['sector'];

            $conditions['status'] = 'Pending';
            $result['pipeline']['data'] = $this->Crm_model->getCollateralProjectList($conditions);
            $result['pipeline']['count'] = count($result['pipeline']['data']);

            $conditions['limit'] = 3;
            $conditions['status'] = 'pre-approved';
            $result['submitted']['data'] = $this->Crm_model->getCollateralProjectList($conditions);
            $result['submitted']['count'] = count($result['submitted']['data']);

            $conditions['status'] = 'Approved';
            $result['approved']['data'] = $this->Crm_model->getCollateralProjectList($conditions);
            $result['approved']['count'] = count($result['approved']['data']);

            $conditions['status'] = 'Monitoring';
            $result['monitoring']['data'] = $this->Crm_model->getCollateralProjectList($conditions);
            $result['monitoring']['count'] = count($result['monitoring']['data']);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function monitoring_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('product_term_id', array('required'=> $this->lang->line('product_term_id_req')));
        $this->form_validator->add_rules('item_order', array('required'=> $this->lang->line('item_order_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['item_order']==0){ $type = 'insert'; }
        else{ $type = 'update'; }

        $crm_project_id = $data['crm_project_id'];
        $created_by = $data['created_by'];
        $data_keys = array_keys($data);
        $form_data = array(); $st=0;
        $form_fields = $this->Company_model->getProductTermItems(array('product_term_id' => $data['product_term_id']));

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['product_term_item_key']==$data_keys[$r]){
                    $form_data[$st]['product_term_item_id'] = $form_fields[$s]['id_product_term_item'];
                    if($form_fields[$s]['item_type']=='json')
                        $form_data[$st]['item_data'] = json_encode($data[$data_keys[$r]]);
                    else
                        $form_data[$st]['item_data'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['product_term_item_id'] = $form_fields[$s]['id_product_term_item'];
                $form_data[$st]['item_data'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_fields'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($type=='insert')
        {
            $item_order = $this->Crm_model->getProductTermItemDataMaxItemOrder(array('crm_project_id' => $crm_project_id));
            if(!empty($item_order))
                $item_order = $item_order[0]['item_order']+1;
            else
                $item_order = 1;

            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_project_id'] = $crm_project_id;
                $form_data[$r]['created_by'] = $created_by;
                $form_data[$r]['item_order'] = $item_order;
                $form_data[$r]['created_date_time'] = currentDate();
            }

            $this->Crm_model->addProductTermItemData_batch($form_data);
            $msg = $this->lang->line('item_data_add');
        }
        if($type=='update')
        {
            $check_data = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $crm_project_id, 'product_term_id' => $data['product_term_id'], 'item_order' => $data['item_order']));

            for($s=0;$s<count($check_data);$s++){
                for($r=0;$r<count($form_data);$r++){
                    if($check_data[$s]['product_term_item_id']==$form_data[$r]['product_term_item_id']){
                        $form_data[$r]['id_product_term_item_data'] = $check_data[$s]['id_product_term_item_data'];
                    }
                }
            }

            $this->Crm_model->updateMonitoring($form_data);
            $msg = $this->lang->line('item_data_update');
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'],'crm_module_type' => 'project','reference_id' => $data['product_term_id'], 'reference_type' => 'monitoring'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['product_term_id'], 'reference_type' => 'monitoring', 'created_by' => $created_by, 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $product_term_details = $this->Company_model->getProductTerms(array('id_product_term' => $data['product_term_id']));
        $this->Activity_model->addActivity(array('activity_name' => $product_term_details[0]['product_term_name'],'activity_template' => 'Information updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'term','activity_reference_id' => $product_term_details[0]['id_product_term'],'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function monitoring_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('product_term_id', array('required'=> $this->lang->line('product_term_id')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('item_order', array('required'=> $this->lang->line('item_order_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $item_data = $this->Crm_model->getProductTermItemData(array('product_term_id' => $data['product_term_id'], 'crm_project_id' => $data['crm_project_id'], 'item_order' => $data['item_order']));
        $result = array();
        for($s=0;$s<count($item_data);$s++)
        {
            if($item_data[$s]['item_type']=='json')
                $result[$item_data[$s]['product_term_item_key']] = json_decode($item_data[$s]['item_data']);
            else
                $result[$item_data[$s]['product_term_item_key']] = $item_data[$s]['item_data'];

            $result['item_order'] = $item_data[$s]['item_order'];
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formComments_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $this->form_validator->add_rules('reference_type', array('required'=> $this->lang->line('reference_type_req')));
        $this->form_validator->add_rules('reference_id', array('required'=> $this->lang->line('reference_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getFormComments($data);

        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'], 'profile');
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formComments_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('comment_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $this->form_validator->add_rules('reference_type', array('required'=> $this->lang->line('reference_type_req')));
        $this->form_validator->add_rules('reference_id', array('required'=> $this->lang->line('reference_id_req')));
        $this->form_validator->add_rules('comment', array('required'=> $this->lang->line('comment_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['created_date_time'] = currentDate();
        $this->Crm_model->addFormComments($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralDocument_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getCollateralDocument($data);

        for($s=0;$s<count($result);$s++){
            $result[$s]['document'] = json_decode($result[$s]['document']);
            $result[$s]['suggest'] = json_decode($result[$s]['suggest']);
        }
        if(!empty($result)){ $result = $result[0]; }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralDocument_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $user_id = $data['user_id'];
        unset($data['user_id']);

        if(isset($data['post_type']) && $data['post_type']=='file_upload')
        {
            $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
            $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_id_req')));
            $this->form_validator->add_rules('date', array('required'=> $this->lang->line('date_req')));
            $this->form_validator->add_rules('due_date', array('required'=> $this->lang->line('due_date_reg')));
            $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
            $this->form_validator->add_rules('comment', array('required'=> $this->lang->line('comment_req')));
            $validated = $this->form_validator->validate($data['data']);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $path='uploads/';
            if(isset($_FILES) && !empty($_FILES['file']['name']))
            {
                $imageName = doUpload($_FILES['file']['tmp_name'],$_FILES['file']['name'],$path,$data['data']['company_id'],'image');

                $data['data']['attachment'] = getExactImageUrl($imageName);
            }
            else{
                $data['data']['attachment'] = '';
            }
            unset($data['company_id']);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data['data']);
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
            $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_id_req')));
            $this->form_validator->add_rules('document', array('required'=> $this->lang->line('document_req')));
            $this->form_validator->add_rules('suggest', array('required'=> $this->lang->line('suggest_reg')));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            unset($data['company_id']);

            $check_date = $this->Crm_model->getCollateralDocument(array('collateral_id' => $data['collateral_id']));

            $data['document'] = json_encode($data['document']);
            $data['suggest'] = json_encode($data['suggest']);
            if(empty($check_date)) {
                $data['created_date_time'] = currentDate();
                $this->Crm_model->addCollateralDocument($data);
            }
            else {
                $data['id_collateral_document'] = $check_date[0]['id_collateral_document'];
                $this->Crm_model->updateCollateralDocument($data);
            }
        }

        $this->Activity_model->addActivity(array('activity_name' => 'Collateral Documentation','activity_template' => 'Collateral Documentation has been Updated','module_type' => 'collateral','module_id' =>  $data['collateral_id'], 'activity_type' => 'form','created_by' => $user_id,'created_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function existingFacility_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('facility_id', array('required'=> $this->lang->line('facility_id_reg')));
        $this->form_validator->add_rules('status', array('required'=> $this->lang->line('status_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check = $this->Crm_model->getExistingFacility($data);
        if($data['status']=='yes'){
            if(empty($check)) $this->Crm_model->addExistingFacility(array('facility_id' => $data['facility_id'], 'status' => 1,'created_date_time' => currentDate()));
            else{ $this->Crm_model->updateExistingFacility(array('id_project_existing_facility' => $check[0]['id_project_existing_facility'], 'status' => 1)); }
        }
        else{
            $this->Crm_model->deleteExistingFacility(array('id_project_existing_facility' => $check[0]['id_project_existing_facility']));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmProject_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_project_id = $data['crm_project_id'];
        unset($data['crm_project_id']);
        $data['created_date_time'] = currentDate();
        $this->Crm_model->updateProject($data,$crm_project_id);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalComments_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getApprovalComments($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function checkNextStageForProject($data)
    {
        $project_stage_info_id = $this->Project_model->addProjectStageInfo(array(
            'project_id' => $data['project_id'],
            'stage_id' => $data['stage_id'],
            'version_number' => 1,
            'project_stage_status' => 'in progress',
            'created_by' => $data['created_by'],
            'start_date_time' => currentDate()
        ));
        $this->Crm_model->updateProject(array('project_stage_info_id' => $project_stage_info_id),$data['project_id']);
        $this->Project_model->addProjectStageVersion(['project_stage_info_id' => $project_stage_info_id,'version_number' => 1, 'created_by' => $data['created_by'],'created_date_time' => currentDate()]);

        $stage_details = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['stage_id']));
        if(!empty($stage_details) && $stage_details[0]['workflow_id']==0 && $stage_details[0]['next_id_project_stage'] && $stage_details[0]['next_id_project_stage']!=''){
            $this->checkNextStageForProject(array(
                'project_id' => $data['project_id'],
                'stage_id' => $stage_details[0]['next_id_project_stage'],
                'version_number' => 1,
                'project_stage_status' => 'in progress',
                'created_by' => $data['created_by']
            ));
        }

        return 1;
    }

    /*public function fromDocument_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $this->form_validator->add_rules('field_key', array('required'=> $this->lang->line('field_key_req')));
        $this->form_validator->add_rules('group_id', array('required'=> $this->lang->line('group_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result=$this->Crm_model->getFormDocument($data);
        for($m=0;$m<count($result);$m++)
        {
            $result[$m]['versions']=$this->Crm_model->getDocumentVersion(array('crm_document_id' => $result['id_crm_document']));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formDocument_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id',array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $this->form_validator->add_rules('field_key', array('required'=> $this->lang->line('field_key_req')));
        $this->form_validator->add_rules('uploaded_from_id', array('required'=> $this->lang->line('group_id_req')));
        $this->form_validator->add_rules('uploaded_by', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $data['company_id'];
        if(!isset($data['description'])){ $data['description'] = ''; }
        $document_id = '';
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            $file_name = $_FILES['file']['name']['attachment'];
            $mediaType = $_FILES['file']['type']['attachment'];
            $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],'uploads/',$company_id);

            if($imageName===0){
                $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if(isset($data['document_id']))
            {
                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id'],'crm_document_id' => $data['document_id']);
                $document_id = $this->Crm_model->addDocumentVersion($document);
            }
            else
            {
                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id'],'module_type' => $data['module_type'],'form_key' => $data['form_key'],'field_key' => $data['field_key']);
                $document_id = $this->Crm_model->addDocument($document);
            }

            //saving activity for activity table --start
            $module = $this->Crm_model->getModuleByDocumentType($data['document_type']);
            if(!empty($module)){
                $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
                $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
                $activity = array(
                    'activity_name' => 'Document',
                    'activity_type' => 'attachment',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $document_id,
                    'module_type' => $data['module_type'],
                    'module_id' => $data['group_id'],
                    'created_by' => $data['uploaded_by']
                );
                $this->Activity_model->addActivity($activity);
            }
            //saving activity --end
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('document_add'), 'data'=>array('document_id' => $document_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function contact_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->updateContact(array('id_crm_contact' => $data['crm_contact_id'],'contact_status' => 0));

        $this->Activity_model->deleteTouchPoint(array(
            'module_id' => $data['crm_contact_id'],
            'module_type' => 'contact'
        ));
        $this->Activity_model->deleteNotification(array(
            'module_id' => $data['crm_contact_id'],
            'module_type' => 'contact'
        ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('contact_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->updateCompany(array('id_crm_company' => $data['crm_company_id'],'company_status' => 0));

        $this->Activity_model->deleteTouchPoint(array(
            'module_id' => $data['crm_company_id'],
            'module_type' => 'company'
        ));
        $this->Activity_model->deleteNotification(array(
            'module_id' => $data['crm_company_id'],
            'module_type' => 'company'
        ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('company_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function project_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->updateProject(array('project_status' => 'deleted'),$data['crm_project_id']);

        $this->Activity_model->deleteTouchPoint(array(
            'module_id' => $data['crm_project_id'],
            'module_type' => 'project'
        ));
        $this->Activity_model->deleteNotification(array(
            'module_id' => $data['crm_project_id'],
            'module_type' => 'project'
        ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('project_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function menu_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $applicationUserRole = $this->Company_model->getApplicationUserRole($data);

        if(empty($applicationUserRole)){
            $company_approval_role = $this->Company_model->getCompanyUsers(array('user_id' => $data['user_id'],'user_type' => ''));
            $data['company_approval_role_id'] = $company_approval_role[0]['company_approval_role_id'];
            unset($data['user_id']);
            $applicationUserRole = $this->Company_model->getApplicationUserRole($data);
        }

        $menu = 0;
        if(!empty($applicationUserRole)){
            $application_role_id = $applicationUserRole[0]['application_role_id'];
            $menu = $this->Company_model->getMenu(array('application_role_id' => $application_role_id));
            $menu_array = array();
            //echo "<pre>"; print_r($menu); exit;
            for($s=0;$s<count($menu);$s++)
            {
                if($menu[$s]['menu_child']==1) {
                    if(!isset($menu_array[$menu[$s]['id_module']]))
                        $menu_array[$menu[$s]['id_module']] = array(
                            'module_name' => $menu[$s]['menu_label'],
                            'module_icon' => $menu[$s]['module_icon'],
                            'module_url' => $menu[$s]['module_url']
                        );
                    $menu_array[$menu[$s]['id_module']]['childs'][] = array(
                        'child_name' => $menu[$s]['child_label'],
                        'child_icon' => $menu[$s]['child_icon'],
                        'url' => $menu[$s]['child_module_url']
                    );
                }
                else
                    $menu_array[$menu[$s]['id_module']] = array(
                        'module_name' => $menu[$s]['child_label'],
                        'module_icon' => $menu[$s]['module_icon'],
                        'module_url' => $menu[$s]['child_module_url'],
                        'childs' => array()
                    );
            }
            //exit;
            $menu = array_values($menu_array);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$menu);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getDownloadedFile_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getDownloadedFile($data);
        if(count($result)>0){
            $res = array('url'=>REST_API_URL.'uploads/'.$result[0]['document_source'],
                'file' => $result[0]['document_name']);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$res);
            $this->response($result, REST_Controller::HTTP_OK);
        }else{
            $result = array('status'=>false, 'message' => $this->lang->line('error'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    /* * * 24-05-2017 * * */

    /*public function contactPersonalInformation_post(){

        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('martial_status', array('required'=> $this->lang->line('martial_status_req')));
        $this->form_validator->add_rules('education', array('required'=> $this->lang->line('education_req')));
        $this->form_validator->add_rules('residence', array('required'=> $this->lang->line('residence_req')));
        $this->form_validator->add_rules('resident_at_current_location_since', array('required'=> $this->lang->line('resident_at_current_location_since_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //adding new record
        if(isset($data['id_contact_personal_information'])){
            $personalInfoUpdate = array(
                'spouse_name'   => isset($data['spouse_name'])?$data['spouse_name']:'',
                'education' => $data['education'],
                'residence' => $data['residence'],
                'resident_at_current_location_since' => $data['resident_at_current_location_since'],
                'created_on' => currentDate()
            );
            $this->Crm_model->updateRecord('contact_personal_information',$personalInfoUpdate,'id_contact_personal_information', $data['id_contact_personal_information']);
        }else{
            $personalInfo = array(
                'spouse_name'   => isset($data['spouse_name'])?$data['spouse_name']:'',
                'education' => $data['education'],
                'residence' => $data['residence'],
                'resident_at_current_location_since' => $data['resident_at_current_location_since'],
                'created_by' => $_SERVER['HTTP_USER'],
                'created_on' => currentDate()
            );
            $contact_personal_information_id = $this->Crm_model->addNewRecord('contact_personal_information',$personalInfo);
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['contact'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactChildInformation_post(){

        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('id_contact_personal_information',array('required'=> $this->lang->line('contact_parent_id_req')));
        $this->form_validator->add_rules('name', array('required'=> $this->lang->line('name_req')));
        $this->form_validator->add_rules('gender', array('required'=> $this->lang->line('gender_req')));
        $this->form_validator->add_rules('age', array('required'=> $this->lang->line('age_req')));
        $this->form_validator->add_rules('occupation', array('required'=> $this->lang->line('occupation_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //adding new record
        if(isset($data['Id_contact_children'])){
            $childPersonalUpdate = array(
                'contact_children_id' => $data['education'],
                'residence' => $data['residence'],
                'resident_at_current_location_since' => $data['resident_at_current_location_since'],
                'created_on' => currentDate()
            );
            $this->Crm_model->updateRecord('contact_children',$childPersonalUpdate,'Id_contact_children', $data['Id_contact_children']);
        }else{
            $childPersonalInfo = array(
                'name'   => $data['name'],
                'gender' => $data['gender'],
                'age' => $data['age'],
                'occupation' => $data['occupation'],
                'created_by' => $_SERVER['HTTP_USER'],
                'created_on' => currentDate()
            );
            $contact_children_id = $this->Crm_model->addNewRecord('contact_children',$childPersonalInfo);
        }

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    /*public function contactPersonalInformation_post(){

        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //adding new record
        if(isset($data['id_contact_personal_information']) && isset($data['Id_contact_children'])){
            $childPersonalUpdate = array(
                'name'   => $data['name'],
                'gender' => $data['gender'],
                'age' => $data['age'],
                'occupation' => $data['occupation'],
                'updated_by' => $_SERVER['HTTP_USER'],
                'updated_on' => currentDate()
            );
            $this->Crm_model->updateRecord('contact_children',$childPersonalUpdate,'Id_contact_children', $data['Id_contact_children']);
        }else{
            if(isset($data['id_contact_personal_information'])){
                $childPersonalInfo = array(
                    'name'   => $data['name'],
                    'gender' => $data['gender'],
                    'age' => $data['age'],
                    'occupation' => $data['occupation'],
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_on' => currentDate()
                );
                $contact_children_id = $this->Crm_model->addNewRecord('contact_children',$childPersonalInfo);

                $childParentData = array(
                    'contact_personal_information_id'   => $data['id_contact_personal_information'],
                    'contact_children_id' => $contact_children_id,
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_on' => currentDate()
                );
                $this->Crm_model->addNewRecord('contact_parent_children_relation',$childParentData);
            }else{
                $WHERE = array('child_key'=>'marital_status_single');
                $maritalStatus = $this->Crm_model->checkRecord('master_child', $WHERE);
                if(!empty($maritalStatus[0]['id_child']==$data['martial_status']))
                    $spouse = $data['spouse_name'];
                else
                    $spouse = '';

                $personalInfo = array(
                    'company_id' => $data['crm_company_id'],
                    'contact_id' => $data['crm_contact_id'],
                    'spouse_name'   => $spouse,
                    'education' => $data['education'],
                    'martial_status' => $data['martial_status'],
                    'residence' => $data['residence'],
                    'resident_at_current_location_since' => $data['resident_at_current_location_since'],
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_on' => currentDate()
                );
                $this->Crm_model->addNewRecord('contact_personal_information',$personalInfo);
            }
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['contact'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    /*public function contactPersonalInformation_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_contact_personal_information']) && isset($data['Id_contact_children'])){
            $childPersonalUpdate = array(
                'name'   => $data['name'],
                'gender' => $data['gender'],
                'age' => $data['age'],
                'occupation' => $data['occupation'],
                'updated_by' => $_SERVER['HTTP_USER'],
                'updated_on' => currentDate()
            );
            $this->Crm_model->updateRecord('contact_children',$childPersonalUpdate,'Id_contact_children', $data['Id_contact_children']);
        }else{
            if(!isset($data['id_contact_personal_information'])){
                $WHERE = array('child_key'=>'marital_status_single');
                $maritalStatus = $this->Crm_model->checkRecord('master_child', $WHERE);
                //if(!empty($maritalStatus[0]['id_child']) && !empty($data['martial_status']) && $maritalStatus[0]['id_child']==$data['martial_status'])
                if(!empty($maritalStatus) && $maritalStatus[0]['id_child']==$data['martial_status'])
                    $spouse = '';
                else
                    $spouse = $data['spouse_name'];

                $personalInfo = array(
                    'company_id' => $data['crm_company_id'],
                    'contact_id' => $data['crm_contact_id'],
                    'spouse_name'   => $spouse,
                    'education' => isset($data['education'])?$data['education']:'',
                    'martial_status' => isset($data['martial_status'])?$data['martial_status']:'',
                    'residence' => isset($data['residence'])?$data['residence']:'',
                    'resident_at_current_location_since' => isset($data['resident_at_current_location_since'])?$data['resident_at_current_location_since']:'',
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_on' => currentDate()
                );
                $this->Crm_model->updateRecordWhere('contact_personal_information',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));
                $contact_parent_ID = $this->Crm_model->addNewRecord('contact_personal_information',$personalInfo);
            }
            if(isset($data['id_contact_personal_information'])){
                $contact_parent_ID = $data['id_contact_personal_information'];
            }
            $children = $data['children'];
            if(isset($contact_parent_ID) && !empty($children)){
                $childPersonalInfo = array();
                for($s=0;$s<count($children);$s++){
                    if(isset($children['Id_contact_children'])){
                        //$checkChildren = $this->Crm_model->checkRecord('contact_children', array('Id_contact_children' => $children['Id_contact_children']));
                    }

                    if(!isset($children[$s]['Id_contact_children'])){
                        $childPersonalInfo['name'] = isset($children[$s]['name'])?$children[$s]['name']:'';
                        $childPersonalInfo['age'] = isset($children[$s]['age'])?$children[$s]['age']:'';
                        $childPersonalInfo['gender'] = isset($children[$s]['gender'])?$children[$s]['gender']:'';
                        $childPersonalInfo['occupation'] = isset($children[$s]['occupation'])?$children[$s]['occupation']:'';
                        $childPersonalInfo['created_by'] = $_SERVER['HTTP_USER'];
                        $childPersonalInfo['created_on'] = currentDate();
                        $contact_children_id = $this->Crm_model->addNewRecord('contact_children',$childPersonalInfo);
                    }else{
                        $contact_children_id = $children[$s]['Id_contact_children'];
                    }

                    if(isset($contact_children_id)){
                        $childParentData = array(
                            'contact_personal_information_id'   => $contact_parent_ID,
                            'contact_children_id' => $contact_children_id,
                            'created_by' => $_SERVER['HTTP_USER'],
                            'created_on' => currentDate()
                        );
                        $this->Crm_model->addNewRecord('contact_parent_children_relation',$childParentData);
                    }
                }
            }
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/



    public function contactPersonalInformation_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getContactPersonalInformation($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function contactReferenceGuarantor_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $info = array(
            'company_id' => $data['crm_company_id'],
            'contact_id' => $data['crm_contact_id'],
            'type'   => isset($data['type'])?$data['type']:'',
            'first_name' => isset($data['first_name'])?$data['first_name']:'',
            'last_name' => isset($data['last_name'])?$data['last_name']:'',
            'address' => isset($data['address'])?$data['address']:'',
            'comments' => isset($data['comments'])?$data['comments']:'',
            'created_by' => $_SERVER['HTTP_USER'],
            'created_date' => currentDate()
        );
        $this->Crm_model->updateRecordWhere('contact_refrence_guarantor',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));
        $inserted_id = $this->Crm_model->addNewRecord('contact_refrence_guarantor',$info);

        $id_project_stage_section_form = 0;
        if(isset($data['id_project_stage_section_form'])){
            $id_project_stage_section_form = $data['id_project_stage_section_form'];
        }

        $company_id = $data['crm_company_id'];
        if(!file_exists('uploads/'.$company_id)){
            mkdir('uploads/'.$company_id);
        }
        //echo'<pre>';print_r($_FILES);exit;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            $file_name = $_FILES['file']['name']['attachment'];
            $mediaType = $_FILES['file']['type']['attachment'];
            $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],'uploads/',$company_id);

            $data['module_type']='contact';
            if(!isset($data['form_key'])){ $data['form_key']=''; }
            if(!isset($data['field_key'])){ $data['field_key']=''; }
            if(!isset($data['description'])){ $data['description']=''; }
            if(!isset($data['document_type'])){ $data['document_type']=0; }

            $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['crm_contact_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'],'module_type' => $data['module_type'],'form_key' => $data['form_key'],'field_key' => $data['field_key'],'created_date_time' => currentDate());
            $document_id = $this->Crm_model->addDocument($document);

            $document = array('document_source' => $imageName, 'document_name' => $file_name, 'uploaded_from_id' => $data['crm_contact_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'],'crm_document_id' => $document_id,'created_date_time' => currentDate());
            $document_id = $this->Crm_model->addDocumentVersion($document);

            $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
            $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
            $activity = array(
                'activity_name' => 'Document',
                'activity_type' => 'attachment',
                'activity_template' =>$activityTemplate,
                'activity_reference_id' => $document_id,
                'module_type' => $data['module_type'],
                'module_id' => $data['crm_contact_id'],
                'created_by' => $_SERVER['HTTP_USER'],
                'created_date_time' => currentDate()
            );
            $this->Activity_model->addActivity($activity);
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_reference_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function contactReferenceGuarantor_get(){
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result=$this->Crm_model->getReferenceGuarantor($data);
        $data['module_id'] = 1;
        // get Document
        
        /*$result=$this->Crm_model->getDocument($data);
        $versions_data = array();
        for($m=0;$m<count($result);$m++)
        {
            $result[$m]['full_path']=getImageUrl($result[$m]['document_source'],'');
            $result[$m]['size']=get_file_info('uploads/'.$result[$m]['document_source'], 'size');
            $versions = $this->Crm_model->getDocumentVersion(array('crm_document_id' => $result[$m]['id_crm_document']));
            for($n=0;$n<count($versions);$n++)
            {
                $versions[$n]['full_path']=getImageUrl($versions[$n]['document_source'],'');
                $versions[$n]['size']=get_file_info('uploads/'.$versions[$n]['document_source'], 'size');
                $versions[$n]['id_crm_document'] = $versions[$n]['crm_document_id'];
                if($n==0){
                    $versions_data[$versions[$n]['crm_document_id']] = $versions[$n];
                }
                else{
                    $versions_data[$versions[$n]['crm_document_id']]['versions'][] = $versions[$n];
                }
            }

            $result[$m]['versions']= $versions;
        }
        $versions_data = array_values($versions_data);*/
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactReferenceGuarantor_delete($contact_refrence_guarantor_id){
        $this->Crm_model->deletRecord('contact_refrence_guarantor','id_contact_refrence_guarantor',$contact_refrence_guarantor_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactAssets_post(){
        $data = $this->input->post();
//echo '<pre>';print_r($data);exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('type_of_asset',array('required'=> $this->lang->line('type_of_asset_req')));
        $this->form_validator->add_rules('aquisition_date',array('required'=> $this->lang->line('aquisition_date_req')));
        $this->form_validator->add_rules('currency_id',array('required'=> $this->lang->line('currency_id_req')));
        $this->form_validator->add_rules('value',array('required'=> $this->lang->line('value_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_tangible_assets'])) {
            $AssetsInfo = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type_of_asset' => isset($data['type_of_asset']) ? $data['type_of_asset'] : NULL,
                'aquisition_date' => isset($data['aquisition_date']) ? $data['aquisition_date'] : NULL,
                'value' => isset($data['value']) ? $data['value'] : NULL,
                'comment' => isset($data['comment']) ? $data['comment'] : '',
                'in_collateral_database' => isset($data['in_collateral_database']) ? $data['in_collateral_database'] : NULL,
                'picture' => isset($data['picture']) ? $data['picture'] : NULL,
                'updated_by' => $_SERVER['HTTP_USER'],
                'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : NULL,
                'updated_on' => currentDate()
            );
        }
        else{
            $AssetsInfo = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type_of_asset' => isset($data['type_of_asset']) ? $data['type_of_asset'] : NULL,
                'aquisition_date' => isset($data['aquisition_date']) ? $data['aquisition_date'] : NULL,
                'value' => isset($data['value']) ? $data['value'] : NULL,
                'comment' => isset($data['comment']) ? $data['comment'] : '',
                'in_collateral_database' => isset($data['in_collateral_database']) ? $data['in_collateral_database'] : NULL,
                'picture' => isset($data['picture']) ? $data['picture'] : NULL,
                'created_by' => $_SERVER['HTTP_USER'],
                'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : NULL,
                'created_on' => currentDate()
            );

        }

        if(isset($data['id_tangible_assets'])){

            $this->Crm_model->updateRecordWhere('contact_tangible_assets',$AssetsInfo,array('id_tangible_assets' => $data['id_tangible_assets']));
            $asset_id = $data['id_tangible_assets'];
            $status = 'asset_update';
        }
        else{
            $asset_id = $this->Crm_model->addNewRecord('contact_tangible_assets',$AssetsInfo);
            $status = 'asset_add';
        }

        $data['user_id'] = $_SERVER['HTTP_USER'];
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
            $totalFilesCount = count($_FILES['file']['name']['attachment']);

        $path='uploads/';
        if(!is_dir($path.$data['crm_company_id'])){ mkdir($path.$data['crm_company_id']); }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']) && count($totalFilesCount)>0)
        {
            for($i=0;$i<$totalFilesCount;$i++){

                $file_name = $_FILES['file']['name']['attachment'][$i];
                $mediaType = $_FILES['file']['type']['attachment'][$i];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachment'][$i],$_FILES['file']['name']['attachment'][$i],'uploads/',$data['crm_company_id']);

                $data['module_type']='contact';
                if(!isset($data['form_key'])){ $data['form_key']=''; }
                if(!isset($data['field_key'])){ $data['field_key']=''; }
                if(!isset($data['description'])){ $data['description']=''; }
                if(!isset($data['document_type'])){ $data['document_type']=0; }

                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $asset_id, 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'],'module_type' => $data['module_type'],'form_key' => $data['form_key'],'field_key' => $data['field_key'],'created_date_time' => currentDate());
                $document_id = $this->Crm_model->addDocument($document);

                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'uploaded_from_id' => $asset_id, 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'],'crm_document_id' => $document_id,'created_date_time' => currentDate());
                $document_id = $this->Crm_model->addDocumentVersion($document);

                $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
                $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
                $activity = array(
                    'activity_name' => 'Document',
                    'activity_type' => 'attachment',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $document_id,
                    'module_type' => $data['module_type'],
                    'module_id' => $data['crm_contact_id'],
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_date_time' => currentDate()
                );
                $this->Activity_model->addActivity($activity);
            }
        }
        if(isset($data['delete_documents']) && $data['delete_documents']!=''){
            $document_details = $this->Crm_model->getDocumentVersion(array('id_crm_document_version' => $data['document_id']));
            if(isset($document_details[0]['document_source']) && file_exists('uploads/'.$document_details[0]['document_source'])){
                unlink('uploads/'.$document_details[0]['document_source']);
            }
            $this->Crm_model->deleteDocumentVersion(array('id_crm_document_version' => $data['document_id']));
        }
        /*$this->Crm_model->updateRecordWhere('contact_tangible_assets',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));*/


        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line($status), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactAssets_delete($contactAssets_id){
        $this->Crm_model->deletRecord('contact_tangible_assets','id_tangible_assets',$contactAssets_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactLiabilities_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('type_of_liabilities',array('required'=> $this->lang->line('type_of_liabilities_req')));
        $this->form_validator->add_rules('maturity_date',array('required'=> $this->lang->line('maturity_date_req')));
        $this->form_validator->add_rules('currency_id',array('required'=> $this->lang->line('currency_id_req')));
        $this->form_validator->add_rules('amount',array('required'=> $this->lang->line('amount_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_liabilities'])) {
            $AssetsInfo = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type_of_liabilities' => isset($data['type_of_liabilities']) ? $data['type_of_liabilities'] : NULL,
                'amount' => isset($data['amount']) ? $data['amount'] : NULL,
                'maturity_date' => isset($data['maturity_date']) ? $data['maturity_date'] : NULL,
                'comment' => isset($data['comment']) ? $data['comment'] : '',
                'updated_by' => $_SERVER['HTTP_USER'],
                'updated_on' => currentDate()
            );
        }
        else{
            $AssetsInfo = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type_of_liabilities' => isset($data['type_of_liabilities']) ? $data['type_of_liabilities'] : NULL,
                'amount' => isset($data['amount']) ? $data['amount'] : NULL,
                'maturity_date' => isset($data['maturity_date']) ? $data['maturity_date'] : NULL,
                'comment' => isset($data['comment']) ? $data['comment'] : '',
                'created_by' => $_SERVER['HTTP_USER'],
                'created_on' => currentDate()
            );
        }
        /*$this->Crm_model->updateRecordWhere('contact_liabilities',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));*/
        if(isset($data['id_liabilities'])){
            $this->Crm_model->updateRecordWhere('contact_liabilities',$AssetsInfo,array('id_liabilities' => $data['id_liabilities']));
            $status = 'liability_update';
        }
        else{
            $this->Crm_model->addNewRecord('contact_liabilities',$AssetsInfo);
            $status = 'liability_add';
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line($status), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactLiabilities_delete($contactLiabilities_id){
        $this->Crm_model->deletRecord('contact_liabilities','id_liabilities',$contactLiabilities_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactAssetsAndLiabilities_get(){
        $data = $this->input->get();
        //echo '<pre>';print_r($data);exit;
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result=$this->Crm_model->getContactAssetsAndLiabilities($data);
        $data['uploaded_from_id']=$data['crm_contact_id'];
        foreach($result['assets'] as $k=>$v){
            $result['assets'][$k]['attachment'] = $this->Crm_model->getDocument(array('group_id'=>$v['id_tangible_assets'], 'module_type'=>'contact', 'form_key'=>'assets_and_liabilities'));
        }


        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }



    /* * * 29-05-2017 * * */

    public function contactRelationSearch_get(){
        $data = $this->input->get();
        if(isset($_SERVER['HTTP_USER'])){
            $this->current_user = $_SERVER['HTTP_USER'];
        }
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('search_key',array('required'=> $this->lang->line('search_key')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $search_array=array('crm_contact_id' => $data['crm_contact_id'],'search_key' => $data['search_key']);
        if(isset($data['relation_type']))
            $search_array['relation_type']=$data['relation_type'];
        $result = $this->Crm_model->getContactRelationSearch($search_array);
        foreach($result as $k=>$v){
            $formdetails= $this->formData_get($v['id_crm_contact'],1,'edit','INFO');
            $result[$k]['data']=array();
            if(isset($formdetails['data']))
                $result[$k]['data']=$formdetails['data'];
        }
        //$crm_contact_id = $data['crm_contact_id'];
        //$result = $this->Crm_model->getContactRelationSearch(array('crm_contact_id' => $crm_contact_id,'search_key' => $data['search_key']));
        //echo '<pre>';print_r($result);exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactRelationalInformation_post(){
        $data = $this->input->post();
        //echo '<pre>';print_r($data);exit;

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        if(!isset($data['child_contact_id'])) {
            $this->form_validator->add_rules('client_first_name', array('required' => $this->lang->line('first_name_req')));
            $this->form_validator->add_rules('client_last_name', array('required' => $this->lang->line('last_name_req')));
        }
        $this->form_validator->add_rules('crm_contact_type_id',array('required'=> $this->lang->line('crm_contact_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data['company_id'] = $data['crm_company_id'];
        /*$data['client_first_name'] = $data['first_name'];
        $data['client_last_name'] = $data['last_name'];
        $data['client_email'] = $data['email'];*/
        $data['created_by'] = $_SERVER['HTTP_USER'];

        if(!isset($data['child_contact_id'])){
            $addContact = $this->quickContact_post($data);
            $crm_contact_id = isset($addContact['data']['crm_contact_id'])?$addContact['data']['crm_contact_id']:0;
        }else{
            $crm_contact_id = $data['child_contact_id'];
        }
        if($crm_contact_id>0) {
            $relation = '';
            if (isset($data['relation_type']) && $data['relation_type'] == 'children') {
                if (isset($data['occupation']) || isset($data['education'])) {
                    $data_pinfo = $data;
                    $data_pinfo['crm_contact_id'] = $crm_contact_id;
                    $addContact = $this->contactPersonalInformation_post($data_pinfo);
                }
                $relation = 'children';
            } elseif (isset($data['relation_type']) && $data['relation_type'] == 'reference_guarantor') {
                $relation = 'reference_guarantor';
            } elseif (isset($data['relation_type']) && $data['relation_type'] == 'spouse') {
                $checkSpouse = $this->Crm_model->checkRecord('crm_contact_relation', array('parent_contact_id' => $data['crm_contact_id'],'status'=>1));
                if (!empty($checkSpouse)) {
                    //$this->Crm_model->updateRecordWhere('crm_contact_relation', array('status' => 0, 'updated_by' => $_SERVER['HTTP_USER'], 'updated_on' => currentDate()), array('id_crm_contact_relation' => $checkSpouse[0]['id_crm_contact_relation']));
                }
                $relation = 'spouse';
            }
            else{
                $relation=$data['relation_type'];
            }
            //$contact_relation_id = $data['contact_relation_id'];
            $relationType = $this->Crm_model->checkRecord('master_child', array('child_key' => $relation));
            if(isset($data['id_crm_contact_relation']) && $data['id_crm_contact_relation']>0){
                $dataRelation = array(
                    'crm_contact_id' => $crm_contact_id,
                    'parent_contact_id' => $data['crm_contact_id'],
                    'relation_id' => $relationType[0]['id_child'],
                    'updated_by' => $_SERVER['HTTP_USER'],
                    'updated_on' => currentDate(),
                    'comments' => isset($data['comments']) ? $data['comments'] : NULL
                );
                $relation_id=$data['id_crm_contact_relation'];
                $this->Crm_model->updateRecordWhere('crm_contact_relation',$dataRelation,array('id_crm_contact_relation' => $data['id_crm_contact_relation']));
            }
            else {
                $dataRelation = array(
                    'crm_contact_id' => $crm_contact_id,
                    'parent_contact_id' => $data['crm_contact_id'],
                    'relation_id' => $relationType[0]['id_child'],
                    'created_by' => $_SERVER['HTTP_USER'],
                    'created_date_time' => currentDate(),
                    'comments' => isset($data['comments']) ? $data['comments'] : NULL
                );
                $relation_id = $this->Crm_model->addNewRecord('crm_contact_relation', $dataRelation);
            }

            $id_project_stage_section_form = 0;
            if (isset($data['id_project_stage_section_form'])) {
                $id_project_stage_section_form = $data['id_project_stage_section_form'];
            }

            //echo'<pre>';print_r($_FILES);exit;
            $data['user_id'] = $_SERVER['HTTP_USER'];
            if (isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
                $totalFilesCount = count($_FILES['file']['name']['attachments']);

            $path = 'uploads/';
            if (!is_dir($path . $data['crm_company_id'])) {
                mkdir($path . $data['crm_company_id']);
            }
            if (isset($_FILES) && !empty($_FILES['file']['name']['attachments']) && count($totalFilesCount) > 0) {
                for ($i = 0; $i < $totalFilesCount; $i++) {

                    $file_name = $_FILES['file']['name']['attachments'][$i];
                    $mediaType = $_FILES['file']['type']['attachments'][$i];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$i], $_FILES['file']['name']['attachments'][$i], 'uploads/', $data['crm_company_id']);

                    $data['module_type'] = 'contact';
                    if (!isset($data['form_key'])) {
                        $data['form_key'] = '';
                    }
                    if (!isset($data['field_key'])) {
                        $data['field_key'] = '';
                    }
                    if (!isset($data['description'])) {
                        $data['description'] = '';
                    }
                    if (!isset($data['document_type'])) {
                        $data['document_type'] = 0;
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $relation_id, 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'], 'module_type' => $data['module_type'], 'form_key' => $data['form_key'], 'field_key' => $data['field_key'], 'created_date_time' => currentDate());
                    $document_id = $this->Crm_model->addDocument($document);

                    $document = array('document_source' => $imageName, 'document_name' => $file_name, 'uploaded_from_id' => $relation_id, 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'], 'crm_document_id' => $document_id, 'created_date_time' => currentDate());
                    $document_id = $this->Crm_model->addDocumentVersion($document);

                    $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
                    $activityTemplate = '<p class="f12"><span class="blue">' . ucfirst($uploaded_user->first_name) . ' ' . ucfirst($uploaded_user->last_name) . '</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="' . $document_id . '">' . ucfirst($file_name) . '</a></p>';
                    $activity = array(
                        'activity_name' => 'Document',
                        'activity_type' => 'attachments',
                        'activity_template' => $activityTemplate,
                        'activity_reference_id' => $document_id,
                        'module_type' => $data['module_type'],
                        'module_id' => $data['crm_contact_id'],
                        'created_by' => $_SERVER['HTTP_USER'],
                        'created_date_time' => currentDate()
                    );
                    $this->Activity_model->addActivity($activity);
                }
            }
            $data['form_id']=isset($data['form_id'])?$data['form_id']:'';
            //adding status for each form fill or not
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
            if (empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

            // Last Updated
            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id']));
            if (empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

            //adding activity
            $form_info = $this->Crm_model->getFormInformation($data['form_id']);
            $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
            $result = array('status' => TRUE, 'message' => $this->lang->line('relation_add'), 'data' => array('crm_contact_id' => $crm_contact_id, 'contact_relation_id' => $relation_id));
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            $result = array('status' => TRUE, 'message' => 'Form submit failed.');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function contactPersonalInformation_post($data=NULL){
        if(isset($data) && $data!=NULL){
            $is_data = 1;
        }else{
            $data = $this->input->post();
            $is_data = 0;
        }
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $WHERE = array('child_key'=>'marital_status_single');
        $maritalStatus = $this->Crm_model->checkRecord('master_child', $WHERE);
        //if(!empty($maritalStatus[0]['id_child']) && !empty($data['martial_status']) && $maritalStatus[0]['id_child']==$data['martial_status'])
        if(!empty($maritalStatus) && isset($data['martial_status']) && $maritalStatus[0]['id_child']==$data['martial_status'])
            $spouse = NULL;
        else
            $spouse = isset($data['spouse_name'])?$data['spouse_name']:NULL;

        $personalInfo = array(
            'company_id' => $data['crm_company_id'],
            'contact_id' => $data['crm_contact_id'],
            'spouse'   => $spouse,
            'education' => isset($data['education'])?$data['education']:'',
            'occupation' => isset($data['occupation'])?$data['occupation']:NULL,
            'martial_status' => isset($data['martial_status'])?$data['martial_status']:'',
            //'residence' => isset($data['residence'])?$data['residence']:'',
            //'resident_at_current_location_since' => isset($data['resident_at_current_location_since'])?$data['resident_at_current_location_since']:'',
            'employer' => isset($data['employer'])?$data['employer']:NULL,
            'employed_since' => isset($data['employed_since'])?$data['employed_since']:NULL,
            'monthly_salary' => isset($data['monthly_salary'])?$data['monthly_salary']:NULL,
            'monthly_salary_currency_id' => isset($data['monthly_salary_currency_id'])?$data['monthly_salary_currency_id']:NULL,
            'monthly_expenses' => isset($data['monthly_expenses'])?$data['monthly_expenses']:NULL,
            'monthly_expenses_currency_id' => isset($data['monthly_expenses_currency_id'])?$data['monthly_expenses_currency_id']:NULL,
            'created_by' => $_SERVER['HTTP_USER'],
            'created_on' => currentDate()
        );
        $this->Crm_model->updateRecordWhere('contact_personal_information',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));
        $contact_parent_ID = $this->Crm_model->addNewRecord('contact_personal_information',$personalInfo);


        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('contact_add'), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        if($is_data==1){
            return $result['data']['crm_contact_id'];
        }else{
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function contactReferenceGuarantor_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('contact_relation_id',array('required'=> $this->lang->line('contact_relation_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $contact_relation_id = $data['contact_relation_id'];

        $id_project_stage_section_form = 0;
        if(isset($data['id_project_stage_section_form'])){
            $id_project_stage_section_form = $data['id_project_stage_section_form'];
        }

        $company_id = $data['crm_company_id'];
        if(!file_exists('uploads/'.$company_id)){
            mkdir('uploads/'.$company_id);
        }
        //echo'<pre>';print_r($_FILES);exit;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            $file_name = $_FILES['file']['name']['attachment'];
            $mediaType = $_FILES['file']['type']['attachment'];
            $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],'uploads/',$company_id);

            $data['module_type']='contact';
            if(!isset($data['form_key'])){ $data['form_key']=''; }
            if(!isset($data['field_key'])){ $data['field_key']=''; }
            if(!isset($data['description'])){ $data['description']=''; }
            if(!isset($data['document_type'])){ $data['document_type']=0; }

            $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $contact_relation_id, 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $_SERVER['HTTP_USER'],'module_type' => $data['module_type'],'form_key' => $data['form_key'],'field_key' => $data['field_key'],'created_date_time' => currentDate());
            $document_id = $this->Crm_model->addDocument($document);

            $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
            $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
            $activity = array(
                'activity_name' => 'Document',
                'activity_type' => 'attachment',
                'activity_template' =>$activityTemplate,
                'activity_reference_id' => $document_id,
                'module_type' => $data['module_type'],
                'module_id' => $data['crm_contact_id'],
                'created_by' => $_SERVER['HTTP_USER'],
                'created_date_time' => currentDate()
            );
            $this->Activity_model->addActivity($activity);
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('document_add'), 'data'=>array('document_id' => $document_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactRelation_delete($id_crm_contact_relation){
        /*$data = $this->input->get();
        if(isset($_SERVER['HTTP_USER'])){
            $this->current_user = $_SERVER['HTTP_USER'];
        }
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/
        $this->Crm_model->updateRecordWhere('crm_contact_relation',array('status'=>0,'updated_by'=>$_SERVER['HTTP_USER'],'updated_on'=>currentDate()),array('id_crm_contact_relation' => $id_crm_contact_relation));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactRelationInformation_get($data=NULL){
        $dataType = 'old';

        if(empty($data)){
            $data = $this->input->get();
            $dataType = 'new';
        }

        //$this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        //$this->form_validator->add_rules('contact_relation_id',array('required'=> $this->lang->line('contact_relation_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        // get relations
        $resultChildren='';

        //$maritalStatus = $this->Crm_model->checkRecord('master_child', array('child_key'=>$data['relation_type']));
        //$data['child_id'] = $maritalStatus[0]['id_child'];

        if(strtolower($data['relation_type'])=='all'){
            $data['child_id']='all';
        }
        else{
            $relation_types=explode(',',$data['relation_type']);
            $relation_type_ids=array();
            foreach($relation_types as $k=>$v){
                $maritalStatus = $this->Crm_model->checkRecord('master_child', array('child_key'=>$v));
                $relation_type_ids[]=$maritalStatus[0]['id_child'];
            }
            $data['child_id'] = implode(',',$relation_type_ids);
        }
        $crm_contact_relations = $this->Crm_model->getRelationshipContacts($data);
        $crm_contact_id = $crm_contact_relations;
        $crm_contact_id = array_map(function($i){ return $i['contact_id']; },$crm_contact_id);
        array_push($crm_contact_id,$data['crm_contact_id']);
        $crm_contact_id = array_values($crm_contact_id);
        /*echo '<pre>';print_r($crm_contact_id);*/
        $resultChildren = array();
        $a=0;
            foreach($crm_contact_relations as $k=>$v){
                if($v['contact_id'] != $data['crm_contact_id']){

                    $resultChildren[$a] = $this->formData_get($v['contact_id'],1,'relation','INFO');
                    //get Documents
                    $id_relation = $this->Crm_model->checkRecord('crm_contact_relation', array('crm_contact_id'=>$v['contact_id']));
                    $data['uploaded_from_id']=$v['id_crm_contact_relation'];
                    $data['form_key']='reference_guarantor';
                    $resultChildren[$a]['documents']=$resultDocuments=$this->Crm_model->getDocument($data);
                    $resultChildren[$a]['id_crm_contact_relation']=$v['id_crm_contact_relation'];
                    $maritalStatus = $this->Crm_model->checkRecord('master_child', array('id_child'=>$v['relation_id']));
                    $resultChildren[$a]['relation_name']=$maritalStatus[0]['child_key'];
                    $a=$a+1;
                }
        }
        $result['relations']=$resultChildren;

        //echo '<br>';print_r($resultChildren);exit;
        if($dataType == 'old'){
            return $result['relations'];
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function mappingRelationType_get(){
        $data = $this->input->get();
        $this->form_validator->add_rules('map_type',array('required'=> $this->lang->line('map_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getMappingRelationType($data);
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /* * * * * * * * * * * * * * * * */

    public function financialExcel_post()
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();


        //$data = $data['financeData'];

        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('crm_company_id')));
        //$this->form_validator->add_rules('financial_statement_id', array('required'=> $this->lang->line('financial_statement_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($_FILES) && !empty($_FILES['file']['name'])){
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(!isset($data['financial_statement_comments'])){
            $data['financial_statement_comments'] = '';
        }
        //
        $financial_statement_details = $this->Company_model->getFinancialStatements(array('statement_key' => $data['form_key']));
        $data['financial_statement_id'] = $financial_statement_details[0]['id_financial_statement'];


        $financial_statement_id = $data['financial_statement_id'];
        $crm_company_id = $data['company_id'];
        $user_id = $data['user_id'];
        $financial_statement_comments = $data['financial_statement_comments'];
        //echo '<pre>';print_r(excelHeadersBySheetName('Sum', $_FILES['file']['tmp_name']['excel'], true));exit;

        //$excel_headers = array_values(excelHeadersBySheetName('Sum', $_FILES['file']['tmp_name']['excel'], true));

        //$excel_headers = array_values(excelHeaders($_FILES['file']['tmp_name']['excel'], true));


        $data = excelToArray($_FILES['file']['tmp_name']['excel'], true);
        $main='';
        $inc=0;
        $temp_head='';
        foreach($data as $kd=>$vd){
            $temp=array_values($vd);
            if($temp[1]!=NULL || trim($temp[1])!=''){
                if($vd['is_formulae']==1){
                    // no formulae
                    $inc=$inc+1;
                    $th=array_values($vd);
                    $main[$inc]['header']['header_name']=$th[1];
                    foreach($vd as $k1=>$v1){
                        $temp_head[preg_replace('/[0-9]+/', '', $k1)]=$v1;
                    }
                }
                else{
                    //formulae
                    $inner_vd=array();
                    $tc=0;
                    foreach($vd as $kk=>$vv){
                        $tc=$tc+1;
                        if($tc==2)
                            $inner_vd['item_name']=$vv;
                        else if($tc>2){
                            //$inner_vd['item_values'][$tc][$temp_head[str_replace(str_split(' 0123456789'), '', $kk)]]=$vv;
                            $inner_vd['item_values'][$tc][$temp_head[preg_replace('/[0-9]+/', '', $kk)]]=$vv;
                        }
                    }
                    $inner_vd['item_values']=array_values($inner_vd['item_values']);
                    $main[$inc]['rows'][]=$inner_vd;
                }
            }
            else{
                unset($data[$kd]);
            }

        }
        $data=array_values($data);
       // echo '<pre>';print_r($main);exit;
        //echo '<pre>';print_r($temp_head);
        //echo '<pre>';print_r($data);
       // echo '<pre>';print_r($main);exit;
          /*$data = getSheetByName('Sum', $_FILES['file']['tmp_name']['excel'], true);
        /*echo '<pre>';print_r($data);die('asdasd');*/

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('excel_empty'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_id = $this->Crm_model->checkRecord('financial_statement', array('statement_key'=>'ratio_analysis'));
        $financial_statement_id = $financial_statement_id[0]['id_financial_statement'];

        $company_financial_statement_data = array('crm_company_id' => $crm_company_id, 'created_by' => $user_id, 'financial_statement_id' => $financial_statement_id, 'financial_statement_comments' => $financial_statement_comments);
        $company_financial_statement_id = $this->Company_model->addCompanyFinancialStatement($company_financial_statement_data);

        $financial_statement_header_item_id = $financial_statement_final_header = $header_temp_var = $temp = 0; $financial_statement_header_id = array();
        $item_type = '';
        /*$excelData = array();
        for($x=0;$x<count($data);$x++){
            foreach($data[$x] as $key => $val){
                $excelData = array(
                    'company_financial_statement_id'=>$company_financial_statement_id,
                    'cell' => $key,
                    'value' => (isset($val))?$val:'',
                    'created_time'=>currentDate()
                );
                $inserted_id = $this->Crm_model->addNewRecord('company_financial_statement_data',$excelData);
            }
        }*/

        $count = count($main)+1;
        for($s=1;$s<$count;$s++){
            if(isset($main[$s]['header'])){
                $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(
                    array(
                        'company_financial_statement_id' => $company_financial_statement_id,
                        'header_name' => $main[$s]['header']['header_name'],
                        'parent_header_id' => 0
                    )
                );
            }
            if(isset($main[$s]['rows'])){
                for($s1=0;$s1<count($main[$s]['rows']);$s1++){
                    $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(
                        array(
                            'financial_statement_header_id' => $financial_statement_final_header,
                            'header_item_name' => $main[$s]['rows'][$s1]['item_name'],
                            'item_type' => ''
                        )
                    );
                    $percentage = 0;
                    if (strstr($main[$s]['rows'][$s1]['item_name'], '%')){
                        $percentage = 1;
                    }
                    for($s2=0;$s2<count($main[$s]['rows'][$s1]['item_values']);$s2++){
                        foreach($main[$s]['rows'][$s1]['item_values'][$s2] as $key1 => $val1){
                            if($percentage == 1){
                                $price = $val1*100;
                            }else{
                                $price = $val1;
                            }
                            $this->Company_model->addFinancialItemData(array(
                                'financial_statement_header_item_id' => $financial_statement_header_item_id,
                                'column_header' => $key1,
                                'price' => str_replace(',','',$price)
                            ));
                        }
                    }
                }

            }
        }

        /*for($s=0;$s<count($data);$s++)
        {
            for($r=0;$r<count($excel_headers);$r++)
            {
                //echo '<pre>';print_r($data[$s][$excel_headers[$r]]);exit;
                $final_header = '';
                $data[$s][$excel_headers[$r]] = strtolower(trim($data[$s][$excel_headers[$r]]));

                if($data[$s][$excel_headers[$r]]!=''){
                    if($r==0){
                        $header = trim(strtolower($data[$s][$excel_headers[$r]]));
                        $header = explode('-',$header);

                        if($temp){
                            if(is_numeric($header[0]) || is_float($header[0])){
                                if (strpos($header[0], $temp.'.') !== false) {
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => $financial_statement_header_id[$temp]));
                                }else if(isset($header[0]) && isset($header[1])){
                                    $temp = $header[0];
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                                    $financial_statement_final_header = 0;
                                }
                            }else{
                                $item_type = '';
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $final_header = $header[0];
                                if(!$financial_statement_final_header)
                                {
                                    if(isset($financial_statement_header_id[$temp]))
                                    {
                                        $financial_statement_final_header = $financial_statement_header_id[$temp]; $header_temp_var = 1;
                                    }else{
                                        $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => '', 'parent_header_id' => 0));
                                    }
                                }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_final_header, 'header_item_name' => $final_header, 'item_type' => $item_type));
                                if($header_temp_var==1){ $financial_statement_final_header = 0; $header_temp_var = 0; }
                            }
                        }else{
                            $temp = $header[0];
                            for($sr=1;$sr<count($header);$sr++){
                                if($sr!=1){ $final_header .= '-'; }
                                $final_header .= $header[$sr];
                            }
                            $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                            if(!is_numeric($header[0]))
                            {
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_header_id[$temp], 'header_item_name' => $header[0], 'item_type' => $item_type));
                            }
                        }
                    }else{
                        $this->Company_model->addFinancialItemData(array(
                            'financial_statement_header_item_id' => $financial_statement_header_item_id,
                            'column_header' => $excel_headers[$r],
                            'price' => str_replace(',','',$data[$s][$excel_headers[$r]])
                        ));
                    }
                }
            }
        }*/
    }

    public function financialInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        //$this->form_validator->add_rules('financial_statement_id', array('required'=> $this->lang->line('financial_statement_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_details = $this->Company_model->getFinancialStatements(array('statement_key' => $data['form_key']));

        $financial_statement = $this->Company_model->getCompanyFinancialStatement(array('financial_statement_id' => $financial_statement_details[0]['id_financial_statement'], 'crm_company_id' => $data['crm_company_id']));

        $template = '';
        if($financial_statement_details[0]['statement_key']=='balance_sheet'){
            $template = REST_API_URL.'templates/balance_sheet.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='income_statement'){
            $template = REST_API_URL.'templates/income_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='cash_flow_statement'){
            $template = REST_API_URL.'templates/cash_flow_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='ratio_analysis'){
            $template = REST_API_URL.'templates/ratio_analysis.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='financial_projection_analysis'){
            $template = REST_API_URL.'templates/financial_projection_analysis.xlsx';
        }

        if(empty($financial_statement))
        {
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => '','template' => $template, 'financial_statement_comments' => ''));
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $financial_statement_comments = $financial_statement[0]['financial_statement_comments'];
        $header_names = $this->Company_model->getFinancialHeaders(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));

        $result = $this->Company_model->getFinancialInformation(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));
        $headers = array_keys($result[0]);

        $html = '<div class="col-sm-12 bal-sheet-wrap table-responsive pb20">';
        $html .= '<div class="bal-sheet-header">';
        $html .= '<h4 class="f13 bold dark-blue pt5 pb5 text-uppercase">'.$financial_statement[0]['statement_name'].'</h4>';
        $html .= '</div>';
        $html .= '<table class="table bal-sheet-table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">';
        $html .= '<thead><tr><th></th>';
        for($s=2;$s<count($headers);$s++){
            $html .='<th>'.$headers[$s].'</th>';
        }
        $html .='</tr></thead><tbody>';
        $sr=0;
        for($s=0;$s<count($header_names);$s++)
        {
            if($header_names[$s]['parent_header_id']){ $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><span class="f14 dark-blue text-uppercase" >'.$header_names[$s]['header_name'].'</span></td></tr>'; }
            else { $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><h4 ';
                if(trim($header_names[$s]['header_name'])!=''){
                    $html .=' class="f13 bold darkblue-bg white-color pt5 pb5 text-uppercase text-center" ';
                }
                $html .='>'.$header_names[$s]['header_name'].'</h4></td></tr>'; }

            $percentage = '';
            for($r=0;$r<$header_names[$s]['loop_count'];$r++)
            {
                if(isset($result[$sr]))
                {
                    $html .='<tr';
                    if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="mod-total" '; }
                    $html .='>';
                    for($u=0;$u<count($headers);$u++)
                    {
                        $percentage = 0;
                        if (strstr($result[$sr][$headers[0]], '%')){
                            $percentage = 1;
                        }
                        if($u==0){
                            $html .= '<td';
                            if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="text-uppercase" '; }
                            $html .=' title ="'.$result[$sr][$headers[$u]].'" >'.$result[$sr][$headers[$u]].'</td>';
                        }
                        else if($u==1){  }
                        else{
                           // $html .='<td>'.''.number_format($result[$sr][$headers[$u]],2).'</td>';
                            if ($percentage == 1){
                               // $perVal = $result[$sr][$headers[$u]] * 100;
                                $perVal = $result[$sr][$headers[$u]];
                                $html .='<td>'.''.number_format($perVal,2).'%'.'</td>';
                            }else{
                                $html .='<td>'.''.number_format($result[$sr][$headers[$u]],0).'</td>';
                            }
                        }

                    }
                    $html .='</tr>';
                    $sr++;
                }
            }
        }
        $html .='</tbody></table>';
//echo '<pre>';print_r($html);exit;
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $html,'template' => $template, 'financial_statement_comments' => $financial_statement_comments));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contactIncomeAndExpenses_get(){
        $data = $this->input->get();
        //echo '<pre>';print_r($data);exit;
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result=$this->Crm_model->getContactIncomeAndExpenses($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contactIncomeAndExpenses_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('contact_company_id_req')));
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('type',array('required'=> $this->lang->line('type_of_income_expense_req')));
        $this->form_validator->add_rules('income_expense_type',array('required'=> $this->lang->line('income_expense_type_req')));
        $this->form_validator->add_rules('description',array('required'=> $this->lang->line('description_req')));
        $this->form_validator->add_rules('currency_id',array('required'=> $this->lang->line('currency_id_req')));
        $this->form_validator->add_rules('amount',array('required'=> $this->lang->line('amount_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_contact_income_expense_id'])) {
            $contact_income_expense_info = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type' => isset($data['type']) ? $data['type'] : NULL,
                'income_expense_type' => isset($data['income_expense_type']) ? $data['income_expense_type'] : NULL,
                'amount' => isset($data['amount']) ? $data['amount'] : NULL,
                'description' => isset($data['description']) ? $data['description'] : '',
                'updated_by' => $_SERVER['HTTP_USER'],
                'updated_on' => currentDate()
            );
        }
        else{
            $contact_income_expense_info = array(
                'company_id' => $data['crm_company_id'],
                'contact_id' => $data['crm_contact_id'],
                'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : NULL,
                'type' => isset($data['type']) ? $data['type'] : NULL,
                'income_expense_type' => isset($data['income_expense_type']) ? $data['income_expense_type'] : NULL,
                'amount' => isset($data['amount']) ? $data['amount'] : NULL,
                'description' => isset($data['description']) ? $data['description'] : '',
                'created_by' => $_SERVER['HTTP_USER'],
                'created_on' => currentDate()
            );
        }
        /*$this->Crm_model->updateRecordWhere('contact_liabilities',array('status'=>0),array('company_id' => $data['crm_company_id'],'contact_id' => $data['crm_contact_id']));*/
        if(isset($data['id_contact_income_expense_id'])){
            $this->Crm_model->updateRecordWhere('contact_income_expense',$contact_income_expense_info,array('id_contact_income_expense_id' => $data['id_contact_income_expense_id']));
            $status = ($data['type']=='income')?'income_update':'expense_update';
        }
        else{
            $this->Crm_model->addNewRecord('contact_income_expense',$contact_income_expense_info);
            $status = ($data['type']=='income')?'income_add':'expense_add';
        }

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if (empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        // Last Updated
        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_contact_id'], 'project_stage_section_form_id' => $data['form_id']));
        if (empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate(),'crm_contact_id'=>$data['crm_contact_id']));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $_SERVER['HTTP_USER'], 'updated_date_time' => currentDate()));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'contact', 'module_id' => $data['crm_contact_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['form_id'], 'created_by' => $_SERVER['HTTP_USER'], 'created_date_time' => currentDate()));
        $result = array('status'=>TRUE, 'message' => $this->lang->line($status), 'data'=>array('crm_contact_id' => $data['crm_contact_id']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactIncomeAndExpenses_delete($contactIncomeExpense_id){
        $this->Crm_model->deletRecord('contact_income_expense','id_contact_income_expense_id',$contactIncomeExpense_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveCompanyCustomer_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        //$this->form_validator->add_rules('whole_sale_customer_percentage',array('required'=> $this->lang->line('whole_sale_customer_percentage_req')));
        //$this->form_validator->add_rules('retail_customer_percentage',array('required'=> $this->lang->line('retail_customer_percentage_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_company_customer_master']) && $data['id_crm_company_customer_master']!=NULL && $data['id_crm_company_customer_master']>0) {
            $contact_company_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'whole_sale_customer_percentage' => isset($data['whole_sale_customer_percentage'])?$data['whole_sale_customer_percentage']:NULL,
                'retail_customer_percentage' => isset($data['retail_customer_percentage'])?$data['retail_customer_percentage']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_company_customer_master',$contact_company_info,array('id_crm_company_customer_master' => $data['id_crm_company_customer_master']));
            $insert_id=$data['id_crm_company_customer_master'];
        }
        else{
            $contact_company_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'whole_sale_customer_percentage' => isset($data['whole_sale_customer_percentage'])?$data['whole_sale_customer_percentage']:NULL,
                'retail_customer_percentage' => isset($data['retail_customer_percentage'])?$data['retail_customer_percentage']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_company_customer_master',$contact_company_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();
        //echo '<pre>';print_r($child_data);exit;
        if(isset($child_data)){
            foreach($child_data as $k=>$v){
               //echo 'v'.$k;
               //echo '<pre>';print_r($v->crm_company_customer_master_id);exit;
                $inner_data=array();
                $inner_data['crm_company_customer_master_id']=$insert_id;
                $inner_data['name']=isset($v['name'])?$v['name']:NULL;
                $inner_data['contact_number']=isset($v['contact_number'])?$v['contact_number']:NULL;
                $inner_data['contact_number_isd']=isset($v['contact_number_isd'])?$v['contact_number_isd']:NULL;
                $inner_data['address']=isset($v['address'])?$v['address']:NULL;
                $inner_data['percentage_of_sales_in_cash']=isset($v['percentage_of_sales_in_cash'])?$v['percentage_of_sales_in_cash']:NULL;
                $inner_data['percentage_of_sales_in_debt']=isset($v['percentage_of_sales_in_debt'])?$v['percentage_of_sales_in_debt']:NULL;
                $inner_data['monthly_sales']=isset($v['monthly_sales'])?$v['monthly_sales']:NULL;
                $inner_data['monthly_sales_currency_id']=isset($v['monthly_sales_currency_id'])?$v['monthly_sales_currency_id']:NULL;
                $inner_data['customer_since']=isset($v['customer_since'])?$v['customer_since']:NULL;
                $inner_data['comments']=isset($v['comments'])?$v['comments']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_company_customer_child']) && $v['id_crm_company_customer_child']!=NULL && $v['id_crm_company_customer_child']>0){
                    $inner_data['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $inner_data['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_company_customer_child',$inner_data,array('id_crm_company_customer_child' => $v['id_crm_company_customer_child']));
                }else{
                    $inner_data['created_date_time'] = currentDate();
                    $inner_data['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    $this->Crm_model->addNewRecord('crm_company_customer_child',$inner_data);
                }
            }
        }
        if(isset($data['form_key'])) {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if (!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
                $this->Activity_model->addActivity(array('activity_name' => 'Customers', 'activity_template' => 'Customers information updated', 'module_type' => 'company', 'module_id' => $data['crm_company_id'], 'activity_type' => 'Customers', 'activity_reference_id' => '', 'created_by' => $data['user_id'], 'created_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getcompanycustomer_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_company_customer_master',array('crm_company_id'=>$data['crm_company_id']));
        foreach($result['master'] as $k => $v){
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_company_customer_child',array('crm_company_customer_master_id'=>$v['id_crm_company_customer_master'],'status'=>1));
            foreach($result['master'][$k]['child_data'] as $kc=>$vc) {
                $result['master'][$k]['child_data'][$kc]['monthly_sales_currency_code']=NULL;
                if($vc['monthly_sales_currency_id']>0) {
                    $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $vc['monthly_sales_currency_id']));
                    $result['master'][$k]['child_data'][$kc]['monthly_sales_currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
                }
            }
            if(!isset($result['master'][$k]['child_data'])){
                $result['master'][$k]['child_data']=array();
            }
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveCompanyCompetitor_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('estimated_no_of_competitors',array('required'=> $this->lang->line('estimated_no_of_competitors_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_company_competitor_master']) && $data['id_crm_company_competitor_master']!=NULL && $data['id_crm_company_competitor_master']>0) {
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'estimated_no_of_competitors' => isset($data['estimated_no_of_competitors'])?$data['estimated_no_of_competitors']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_company_competitor_master',$company_supplier_info,array('id_crm_company_competitor_master' => $data['id_crm_company_competitor_master']));
            $insert_id=$data['id_crm_company_competitor_master'];
        }
        else{
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'estimated_no_of_competitors' => isset($data['estimated_no_of_competitors'])?$data['estimated_no_of_competitors']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_company_competitor_master',$company_supplier_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();

        if(isset($child_data)){
            foreach($child_data as $k=>$v){
                $inner_data=array();
                $inner_data['crm_company_competitor_master_id']=$insert_id;
                $inner_data['name']=isset($v['name'])?$v['name']:NULL;
                $inner_data['contact_number']=isset($v['contact_number'])?$v['contact_number']:NULL;
                $inner_data['contact_number_isd']=isset($v['contact_number_isd'])?$v['contact_number_isd']:NULL;
                $inner_data['address']=isset($v['address'])?$v['address']:NULL;
                $inner_data['price']=isset($v['price'])?$v['price']:NULL;
                $inner_data['brand']=isset($v['brand'])?$v['brand']:NULL;
                $inner_data['size']=isset($v['size'])?$v['size']:NULL;
                $inner_data['comments']=isset($v['comments'])?$v['comments']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_company_competitor_child']) && $v['id_crm_company_competitor_child']!=NULL && $v['id_crm_company_competitor_child']>0){
                    $inner_data['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $inner_data['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_company_competitor_child',$inner_data,array('id_crm_company_competitor_child' => $v['id_crm_company_competitor_child']));
                }else{
                    $inner_data['created_date_time'] = currentDate();
                    $inner_data['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    if($inner_data['status']==1)
                        $this->Crm_model->addNewRecord('crm_company_competitor_child',$inner_data);
                }
            }
        }
        if(isset($data['form_key'])) {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if (!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
                $this->Activity_model->addActivity(array('activity_name' => 'Competitor', 'activity_template' => 'Competitors information updated', 'module_type' => 'company', 'module_id' => $data['crm_company_id'], 'activity_type' => 'Competitors', 'activity_reference_id' => '', 'created_by' => $data['user_id'], 'created_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCompanyCompetitor_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_company_competitor_master',array('crm_company_id'=>$data['crm_company_id']));
        foreach($result['master'] as $k => $v){
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_company_competitor_child',array('crm_company_competitor_master_id'=>$v['id_crm_company_competitor_master'],'status'=>1));
            $result['master'][$k]['estimated_no_of_competitors_text']=NULL;
            if($v['estimated_no_of_competitors']>0) {
                $master_child = $this->Crm_model->checkRecord('master_child', array('id_child' => $v['estimated_no_of_competitors']));
                $result['master'][$k]['estimated_no_of_competitors_text'] = isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL;
            }
            foreach($result['master'][$k]['child_data'] as $kc=>$vc) {
                $result['master'][$k]['child_data'][$kc]['price_text']=NULL;
                $result['master'][$k]['child_data'][$kc]['brand_text']=NULL;
                $result['master'][$k]['child_data'][$kc]['size_text']=NULL;
                //$result['master'][$k]['child_data'][$kc]['contact_number_isd_code']=NULL;
                if($vc['price']>0) {
                    $master_child = $this->Crm_model->checkRecord('master_child', array('id_child' => $vc['price']));
                    $result['master'][$k]['child_data'][$kc]['price_text'] = isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL;
                }
                if($vc['brand']>0) {
                    $master_child = $this->Crm_model->checkRecord('master_child', array('id_child' => $vc['brand']));
                    $result['master'][$k]['child_data'][$kc]['brand_text'] = isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL;
                }
                if($vc['size']>0) {
                    $master_child = $this->Crm_model->checkRecord('master_child', array('id_child' => $vc['size']));
                    $result['master'][$k]['child_data'][$kc]['size_text'] = isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL;
                }
                /*if($vc['contact_number_isd']>0) {
                    $master_child = $this->Crm_model->checkRecord('country', array('id_country' => $vc['contact_number_isd']));
                    $result['master'][$k]['child_data'][$kc]['contact_number_isd_code'] = isset($master_child[0]['isd_code']) ? $master_child[0]['isd_code'] : NULL;
                }*/
            }
            if(!isset($result['master'][$k]['child_data'])){
                $result['master'][$k]['child_data']=array();
            }
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function savecompanysupplier_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('percentage_of_geo_distribution_same_city',array('required'=> $this->lang->line('percentage_of_geo_distribution_same_city_req')));
        $this->form_validator->add_rules('percentage_of_geo_distribution_regional',array('required'=> $this->lang->line('percentage_of_geo_distribution_regional_req')));
        $this->form_validator->add_rules('percentage_of_geo_distribution_national',array('required'=> $this->lang->line('percentage_of_geo_distribution_national_req')));
        $this->form_validator->add_rules('percentage_of_geo_distribution_international',array('required'=> $this->lang->line('percentage_of_geo_distribution_international_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_company_supplier_master']) && $data['id_crm_company_supplier_master']!=NULL && $data['id_crm_company_supplier_master']>0) {
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'percentage_of_geo_distribution_same_city' => isset($data['percentage_of_geo_distribution_same_city'])?$data['percentage_of_geo_distribution_same_city']:NULL,
                'percentage_of_geo_distribution_regional' => isset($data['percentage_of_geo_distribution_regional'])?$data['percentage_of_geo_distribution_regional']:NULL,
                'percentage_of_geo_distribution_national' => isset($data['percentage_of_geo_distribution_national'])?$data['percentage_of_geo_distribution_national']:NULL,
                'percentage_of_geo_distribution_international' => isset($data['percentage_of_geo_distribution_international'])?$data['percentage_of_geo_distribution_international']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_company_supplier_master',$company_supplier_info,array('id_crm_company_supplier_master' => $data['id_crm_company_supplier_master']));
            $insert_id=$data['id_crm_company_supplier_master'];
        }
        else{
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'percentage_of_geo_distribution_same_city' => isset($data['percentage_of_geo_distribution_same_city'])?$data['percentage_of_geo_distribution_same_city']:NULL,
                'percentage_of_geo_distribution_regional' => isset($data['percentage_of_geo_distribution_regional'])?$data['percentage_of_geo_distribution_regional']:NULL,
                'percentage_of_geo_distribution_national' => isset($data['percentage_of_geo_distribution_national'])?$data['percentage_of_geo_distribution_national']:NULL,
                'percentage_of_geo_distribution_international' => isset($data['percentage_of_geo_distribution_international'])?$data['percentage_of_geo_distribution_international']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_company_supplier_master',$company_supplier_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();

        if(isset($child_data)){
            foreach($child_data as $k=>$v){
                $inner_data=array();
                $inner_data['crm_company_supplier_master_id']=$insert_id;
                $inner_data['name']=isset($v['name'])?$v['name']:NULL;
                $inner_data['contact_number']=isset($v['contact_number'])?$v['contact_number']:NULL;
                $inner_data['contact_number_isd']=isset($v['contact_number_isd'])?$v['contact_number_isd']:NULL;
                $inner_data['address']=isset($v['address'])?$v['address']:NULL;
                $inner_data['monthly_purchase_cash']=isset($v['monthly_purchase_cash'])?$v['monthly_purchase_cash']:NULL;
                $inner_data['monthly_purchase_cash_currency_id']=isset($v['monthly_purchase_cash_currency_id'])?$v['monthly_purchase_cash_currency_id']:NULL;
                $inner_data['monthly_purchase_debt']=isset($v['monthly_purchase_debt'])?$v['monthly_purchase_debt']:NULL;
                $inner_data['monthly_purchase_debt_currency_id']=isset($v['monthly_purchase_debt_currency_id'])?$v['monthly_purchase_debt_currency_id']:NULL;
                $inner_data['length_of_relationship']=isset($v['length_of_relationship'])?$v['length_of_relationship']:NULL;
                $inner_data['comments']=isset($v['comments'])?$v['comments']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_company_supplier_child']) && $v['id_crm_company_supplier_child']!=NULL && $v['id_crm_company_supplier_child']>0){
                    $inner_data['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $inner_data['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_company_supplier_child',$inner_data,array('id_crm_company_supplier_child' => $v['id_crm_company_supplier_child']));
                }else{
                    $inner_data['created_date_time'] = currentDate();
                    $inner_data['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    if($inner_data['status']==1)
                        $this->Crm_model->addNewRecord('crm_company_supplier_child',$inner_data);
                }
            }
        }
        if(isset($data['form_key'])) {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if (!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
                $this->Activity_model->addActivity(array('activity_name' => 'Suppliers', 'activity_template' => 'Suppliers information updated', 'module_type' => 'company', 'module_id' => $data['crm_company_id'], 'activity_type' => 'Suppliers', 'activity_reference_id' => '', 'created_by' => $data['user_id'], 'created_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCompanySupplier_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_company_supplier_master',array('crm_company_id'=>$data['crm_company_id']));
        foreach($result['master'] as $k => $v){
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_company_supplier_child',array('crm_company_supplier_master_id'=>$v['id_crm_company_supplier_master'],'status'=>1));
            foreach($result['master'][$k]['child_data'] as $kc=>$vc) {
                $result['master'][$k]['child_data'][$kc]['monthly_purchase_cash_currency_code']=NULL;
                $result['master'][$k]['child_data'][$kc]['monthly_purchase_debt_currency_code']=NULL;
                //$result['master'][$k]['child_data'][$kc]['contact_number_isd_code']=NULL;
                if($vc['monthly_purchase_cash_currency_id']>0) {
                    $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $vc['monthly_purchase_cash_currency_id']));
                    $result['master'][$k]['child_data'][$kc]['monthly_purchase_cash_currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
                }
                if($vc['monthly_purchase_debt_currency_id']>0) {
                    $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $vc['monthly_purchase_debt_currency_id']));
                    $result['master'][$k]['child_data'][$kc]['monthly_purchase_debt_currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
                }
                /*if($vc['contact_number_isd']>0) {
                    $master_child = $this->Crm_model->checkRecord('country', array('id_country' => $vc['contact_number_isd']));
                    $result['master'][$k]['child_data'][$kc]['contact_number_isd_code'] = isset($master_child[0]['isd_code']) ? $master_child[0]['isd_code'] : NULL;
                }*/
            }
            if(!isset($result['master'][$k]['child_data'])){
                $result['master'][$k]['child_data']=array();
            }
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveCompanyOverheadCostEstimate_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_company_overhead_cost_estimate_master']) && $data['id_crm_company_overhead_cost_estimate_master']!=NULL && $data['id_crm_company_overhead_cost_estimate_master']>0) {
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_company_overhead_cost_estimate_master',$company_supplier_info,array('id_crm_company_overhead_cost_estimate_master' => $data['id_crm_company_overhead_cost_estimate_master']));
            $insert_id=$data['id_crm_company_overhead_cost_estimate_master'];
        }
        else{
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_company_overhead_cost_estimate_master',$company_supplier_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();

        if(isset($child_data)){
            foreach($child_data as $k=>$v){
                $inner_data=array();
                $inner_data['crm_company_overhead_cost_estimate_master_id']=$insert_id;
                $inner_data['description']=isset($v['description'])?$v['description']:NULL;
                $inner_data['category_id']=isset($v['category_id'])?$v['category_id']:NULL;
                $inner_data['monthly_cost']=isset($v['monthly_cost'])?$v['monthly_cost']:NULL;
                $inner_data['monthly_cost_currency_id']=isset($v['monthly_cost_currency_id'])?$v['monthly_cost_currency_id']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_company_overhead_cost_estimate_child']) && $v['id_crm_company_overhead_cost_estimate_child']!=NULL && $v['id_crm_company_overhead_cost_estimate_child']>0){
                    $v['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $v['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_company_overhead_cost_estimate_child',$inner_data,array('id_crm_company_overhead_cost_estimate_child' => $v['id_crm_company_overhead_cost_estimate_child']));
                }else{
                    $v['created_date_time'] = currentDate();
                    $v['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    if($inner_data['status']==1)
                        $this->Crm_model->addNewRecord('crm_company_overhead_cost_estimate_child',$inner_data);
                }
            }
        }
        if(isset($data['form_key'])) {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if (!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
                $this->Activity_model->addActivity(array('activity_name' => 'Overhead Cost Estimates', 'activity_template' => 'Overhead Cost Estimates updated', 'module_type' => 'company', 'module_id' => $data['crm_company_id'], 'activity_type' => 'Overhead Cost Estimates', 'activity_reference_id' => '', 'created_by' => $data['user_id'], 'created_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCompanyOverheadCostEstimate_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_company_overhead_cost_estimate_master',array('crm_company_id'=>$data['crm_company_id']));
        foreach($result['master'] as $k => $v){
            $summary=array();
            $result['master'][$k]['currency_code']=NULL;
            if($v['currency_id']>0) {
                $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $v['currency_id']));
                $result['master'][$k]['currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
            }
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_company_overhead_cost_estimate_child',array('crm_company_overhead_cost_estimate_master_id'=>$v['id_crm_company_overhead_cost_estimate_master'],'status'=>1));
            foreach($result['master'][$k]['child_data'] as $kc=>$vc){
                $master_child=$this->Crm_model->checkRecord('master_child', array('id_child' => $vc['category_id']));
                $result['master'][$k]['child_data'][$kc]['category_id_text']=isset($master_child[0]['child_name'])?$master_child[0]['child_name']:NULL;
                $summary[$vc['category_id']]['monthly_cost']=(isset($summary[$vc['category_id']]['monthly_cost'])?$summary[$vc['category_id']]['monthly_cost']:0)+$vc['monthly_cost'];
                $summary[$vc['category_id']]['category_id_text']=(isset($summary[$vc['category_id']]['category_id_text'])?$summary[$vc['category_id']]['category_id_text']:$result['master'][$k]['child_data'][$kc]['category_id_text']);
            }
            if(!isset($result['master'][$k]['child_data'])){
                $result['master'][$k]['child_data']=array();
            }
            $summary=array_values($summary);
            $result['master'][$k]['summary']=$summary;
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveCompanySalesDirectCostEstimate_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('sales_estimates',array('required'=> $this->lang->line('sales_estimates_req')));
        $this->form_validator->add_rules('currency_id',array('required'=> $this->lang->line('currency_id_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_company_sales_directcost_estimate_master']) && $data['id_crm_company_sales_directcost_estimate_master']!=NULL && $data['id_crm_company_sales_directcost_estimate_master']>0) {
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'sales_estimates' => json_encode(isset($data['sales_estimates'])?$data['sales_estimates']:array()),
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_company_sales_directcost_estimate_master',$company_supplier_info,array('id_crm_company_sales_directcost_estimate_master' => $data['id_crm_company_sales_directcost_estimate_master']));
            $insert_id=$data['id_crm_company_sales_directcost_estimate_master'];
        }
        else{
            $company_supplier_info = array(
                'crm_company_id' => $data['crm_company_id'],
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'sales_estimates' => json_encode(isset($data['sales_estimates'])?$data['sales_estimates']:array()),
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_company_sales_directcost_estimate_master',$company_supplier_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();

        if(isset($child_data)){
            foreach($child_data as $k=>$v){
                $inner_data=array();
                $inner_data['crm_company_sales_directcost_estimate_master_id']=$insert_id;
                $inner_data['product_name']=isset($v['product_name'])?$v['product_name']:NULL;
                $inner_data['units_sold']=isset($v['units_sold'])?$v['units_sold']:NULL;
                $inner_data['price_per_unit']=isset($v['price_per_unit'])?$v['price_per_unit']:NULL;
                $inner_data['price_per_unit_currency_id']=isset($v['price_per_unit_currency_id'])?$v['price_per_unit_currency_id']:NULL;
                $inner_data['total_sales']=isset($v['total_sales'])?$v['total_sales']:NULL;
                $inner_data['total_sales_percentage']=isset($v['total_sales_percentage'])?$v['total_sales_percentage']:NULL;
                $inner_data['comments']=isset($v['comments'])?$v['comments']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_company_sales_directcost_estimate_child']) && $v['id_crm_company_sales_directcost_estimate_child']!=NULL && $v['id_crm_company_sales_directcost_estimate_child']>0){
                    $inner_data['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $inner_data['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_company_sales_directcost_estimate_child',$inner_data,array('id_crm_company_sales_directcost_estimate_child' => $v['id_crm_company_sales_directcost_estimate_child']));
                }else{
                    $inner_data['created_date_time'] = currentDate();
                    $inner_data['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    if($inner_data['status']==1)
                        $this->Crm_model->addNewRecord('crm_company_sales_directcost_estimate_child',$inner_data);
                }
            }
        }
        if(isset($data['form_key'])) {
            $form_details = $this->Crm_model->getFormDetails(array('form_key' => $data['form_key']));
            if (!empty($form_details)) {
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form'));
                if (empty($module_status))
                    $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $form_details[0]['id_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
                $this->Activity_model->addActivity(array('activity_name' => 'Sales & Direct Cost Estimates', 'activity_template' => 'Sales & Direct Cost Estimates updated', 'module_type' => 'company', 'module_id' => $data['crm_company_id'], 'activity_type' => 'Sales & Direct Cost Estimates', 'activity_reference_id' => '', 'created_by' => $data['user_id'], 'created_date_time' => currentDate()));

                $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form']));
                if (empty($last_update))
                    $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $form_details[0]['id_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
                else
                    $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCompanySalesDirectCostEstimate_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id',array('required'=> $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_company_sales_directcost_estimate_master',array('crm_company_id'=>$data['crm_company_id']));
        foreach($result['master'] as $k => $v){
            $result['master'][$k]['currency_code']=NULL;
            $result['master'][$k]['sales_estimates']=json_decode($result['master'][$k]['sales_estimates']);
            if($v['currency_id']>0) {
                $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $v['currency_id']));
                $result['master'][$k]['currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
            }
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_company_sales_directcost_estimate_child',array('crm_company_sales_directcost_estimate_master_id'=>$v['id_crm_company_sales_directcost_estimate_master'],'status'=>1));
            foreach($result['master'][$k]['child_data'] as $kc=>$vc) {
                $result['master'][$k]['child_data'][$kc]['price_per_unit_currency_code']=NULL;
                if($vc['price_per_unit_currency_id']>0) {
                    $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $vc['price_per_unit_currency_id']));
                    $result['master'][$k]['child_data'][$kc]['price_per_unit_currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
                }
            }
            if(!isset($result['master'][$k]['child_data'])){
                $result['master'][$k]['child_data']=array();
            }
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    /*For ownership details*/
    public function crmCompanyOwnershipList_get()
    {
        $data = $this->input->get();

        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $list = $this->Crm_model->crmCompanyOwnershipList($data);
        /*foreach ($list as $k => $v) {
            if ($v['module_type'] == 'contact') {
                $result = $this->Crm_model->getContact($v['module_id']);
                if (count($result) > 0) {
                    $list[$k]['shareholder_name'] = $result[0]['first_name'];
                    $list[$k]['shareholder_email_id'] = $result[0]['email'];
                }
            } else if ($v['module_type'] == 'company') {
                $result = $this->Crm_model->getCompany($v['module_id']);
                if (count($result) > 0) {
                    $list[$k]['shareholder_name'] = $result[0]['company_name'];
                    $list[$k]['shareholder_email_id'] = $result[0]['email'];
                }
            }
        }*/

        $result = array('status' => TRUE,
            'message' => $this->lang->line('success'),
            'data' => $list);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function crmCompanyOwnership_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $emailRules = array(
            /*'required' => $this->lang->line('email_req'),*/
            'valid_email' => $this->lang->line('email_invalid')
        );
        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_type', array('required' => $this->lang->line('module_type_id_req')));
        $this->form_validator->add_rules('shareholder_name', array('required' => $this->lang->line('shareholder_name_req')));
        $this->form_validator->add_rules('shareholder_email_id', $emailRules);
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);

        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $currentDate = currentDate();

        /*Company form data*/
        $sponsers = array();
        foreach($data as $k=>$v){
            if(in_array($k , array('shareholder_name', 'no_of_shares', 'percentage_share', 'value_of_shares', 'currency'))){
                if($k == 'percentage_share'){
                    $res['percentage_share'] = $v;
                }else{
                    $res[$k] = $v;
                }
            }
        }
        $sponsers[] = $res;
        $update_type = '2';

        $fdata = array();
        $fdata['company_borrower_name'] = $data['shareholder_name'];
        $fdata['form_id'] = '7';
        $fdata['form_name'] = 'Basic Company Information';
        $fdata['field_name'] = 'company_ownership_details';
        $fdata['crm_company_id'] = $data['crm_company_id'];
        $fdata['company_id'] = $data['company_id'];
        $data['module_id'] = $data['crm_company_id'];

        unset($data['company_id']);
        if (isset($data['id_crm_company_shareholder_details'])) {
            $this->Crm_model->deleteCrmCompanyOwnership(array('id_crm_company_shareholder_details' => $data['id_crm_company_shareholder_details']));
        }

        /*$isPresent = false;
        $isPresent = $this->Crm_model->isPresentCrmCompanyOwnership(array('crm_company_id' => $data['crm_company_id'], 'module_type' => $data['module_type'], 'module_id' => $data['module_id'], 'shareholder_name' => $data['shareholder_name']));
        if (count($isPresent) > 0) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('company_relation_exist'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/

        $this->Crm_model->saveCrmShareholderData(array(
            'crm_company_id' => $data['crm_company_id'],
            'module_type' => $data['module_type'],
            'module_id' => $data['module_id'],
            'shareholder_email_id' => isset($data['shareholder_email_id']) ? $data['shareholder_email_id'] : '',
            'shareholder_name' => $data['shareholder_name'],
            'no_of_shares' => $data['no_of_shares'],
            'percentage_share' => $data['percentage_share'],
            'value_of_shares' => $data['value_of_shares'],
            'currency' => $data['currency']
        ));

        $newdata = $this->Crm_model->crmCompanyOwnershipList(array('crm_company_id' => $data['crm_company_id']));

        $check_data = $this->Crm_model->getCompanyFormDataByFieldName($fdata);

        if (isset($fdata['crm_company_id'])) {
            if (empty($check_data)) {
                $sponsers[0]['created_date_time'] = $currentDate;
                $this->Crm_model->addCrmCompanyData(json_encode($sponsers));
            } else {
                $result = array();
                foreach($newdata as $k=>$v){
                    foreach($v as $k1=>$v1){
                        if(in_array($k1 , array('shareholder_name', 'no_of_shares', 'percentage_share', 'value_of_shares', 'currency'))){
                            $result[$k][$k1] = $v1;
                        }
                    }
                }

                $res = JSON_decode(JSON_encode($result));
                /*$result = array();
                if(is_array($res)){
                    foreach($res as $k=>$v){
                        $result[] = $v;
                    }
                }
                $result[] = $sponsers[0];*/
                $this->Crm_model->updateCrmCompanyDataByCrmCompanyDataId(array('form_field_value' => json_encode($res), 'id_crm_company_data' => $check_data[0]['id_crm_company_data']));
            }
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function crmCompanyOwnership_delete()
    {
        $data = $this->input->get();

        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => $this->lang->line('invalid_data'), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->deleteCrmCompanyOwnership($data);
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
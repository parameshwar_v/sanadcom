<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Facility extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('Facility_model');

        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];
        }

        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if(!empty($user_info) && $user_info[0]['user_role_id']==3 && $user_info[0]['all_projects']==0)
        {
            $this->current_user = $user_id;
        }
        else{
            $this->current_user = 0;
        }
    }

    public function facilityList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Facility_model->getFacilityList($data);
        /*for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }*/

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = $this->Facility_model->getFacilityListCount($data);
        $all_records = array_sum(array_map(function($item) {
                                    return $item['total'];
                                }, $total_records));

        unset($data['search_key']);
        //all records
        if(isset($data['created_by'])){ $created_by = $data['created_by']; unset($data['created_by']); }
        $total_records = $this->Facility_model->getFacilityListCount($data);
        $all_facilities = array_sum(array_map(function($item) {
                                        return $item['total'];
                                    }, $total_records));

        //created by user records
        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $total_records = $this->Facility_model->getFacilityListCount($data);
        $my_facilities = array_sum(array_map(function($item) {
                                        return $item['total'];
                                    }, $total_records));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'),  'data'=>array('data' =>$result,'total_records' => $all_records,'all_facilities' => $all_facilities,'my_facilities' => $my_facilities));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFacilities_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('crm_company_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Facility_model->getCompanyFacilities($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function clientFacilities_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Facility_model->getClientFacilities($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function clientGuaranteeFacilities_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id', array('required'=> $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Facility_model->getClientGuaranteeFacilities($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityDetails_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('project_facility_id', array('required'=> $this->lang->line('project_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $where = ['pf.id_project_facility' => $data['project_facility_id']];
        $facilities_array = $this->Project_model->getStageFacilities($where);
        for($f=0;$f<count($facilities_array);$f++) {
            $pricing_array = '';
            $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities_array[$f]['id_project_facility'], 'field_section' => 'pricing'));
            for ($r = 0; $r < count($pricing); $r++) {
                $pricing_array[$r] = array(
                    'field_name' => $pricing[$r]['field_name'],
                    'payment_type' => $pricing[$r]['payment_type'],
                    'label' => '',
                    'facility_field_value' => $pricing[$r]['facility_field_value'],
                    'facility_field_description' => $pricing[$r]['facility_field_description']
                );
                if ($pricing[$r]['payment_type'] == 'interest_as') {
                    $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                    $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                } else if ($pricing[$r]['payment_type'] == 'fee_as') {
                    $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                    $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                } else if ($pricing[$r]['payment_type'] == 'fee_amount') {
                    $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                    $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                }
            }
            $pricing_array = array_values($pricing_array);
            $facilities_array[$f]['pricing'] = $pricing_array;
            $terms_and_conditions_array = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities_array[$f]['id_project_facility'], 'field_section' => 'terms_conditions'));
            $facilities_array[$f]['terms_conditions'] = $terms_and_conditions_array;
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$facilities_array);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_post()
    {
        $data = $this->input->post();
        $this->form_validator->add_rules('id_project_facility', array('required'=> $this->lang->line('project_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->updateFacilities($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/third_party/mailer/mailer.php';

class Cron extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Crm_model');
        $this->load->model('Covenant_model');
        $this->load->model('Notification_model');
        $this->load->model('Project_model');
        $this->load->model('Collateral_model');
        $this->lang->load('rest_controller_lang');
    }

    public function index()
    {
        //echo md5('Eadb@123'); exit;
        $data = $this->Covenant_model->getModuleCovenants(array('notification_date' => date('Y-m-d',strtotime(currentDate()))));
        //echo "<pre>"; print_r($data); exit;
        if(!empty($data)){


        }
        for($s=0;$s<count($data);$s++)
        {
            $project_details = array();
            if($data[$s]['module_type']=='project'){
                $project_details = $this->Project_model->getProject(array('crm_project_id' => $data[$s]['module_id']));
            }
            $this->Covenant_model->addCovenantTask(array(
                'module_covenant_id' => $data[$s]['id_module_covenant'],
                'task_due_date' => $data[$s]['next_due_date'],
                'task_condition' => '',
                'task_comments' => '',
                'created_date_time' => currentDate()
            ));
            $assigned_to = $assigned_to_mail = $assigned_to_name = array();

            if($data[$s]['assigned_to']) {
                $assigned_to = explode(',', $data[$s]['assigned_to']);
                $assigned_to_mail = explode(',', $data[$s]['email']);
                $assigned_to_name = explode(',', $data[$s]['assigned_to_name']);
            }

            for($r=0;$r<count($assigned_to);$r++)
            {
                $notification_subject = 'General'; $notification = '';
                if($data[$s]['module_type']=='project')
                {
                    $project_details = $this->Crm_model->getProject($data[$s]['module_id']);
                    $link = '<a class="sky-blue" href="'.WEB_BASE_URL.'#/covenant/list?covenantId='.base64_encode($data[$s]['id_module_covenant']).'">here</a>';
                    $notification = str_replace(array('{frequency}','{covenant}','{project}','{module}','{date}','{here}'),array($data[$s]['frequency_name'],$data[$s]['covenant_name'],'project '.$project_details[0]['project_title'],$data[$s]['module_type'],date('F d, Y',strtotime($data[$s]['covenant_start_date'])),$link),$this->lang->line('covenant_notification_message'));

                    $notification_subject = $project_details[0]['project_title'];
                    $mail_link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/covenant/list?id='.base64_encode($data[$s]['id_module_covenant']).'">here</a>';
                    $message = str_replace(array('{name}','{frequency}','{covenant}','{project}','{module}','{date}','{here}'),array($assigned_to_name[$r],$data[$s]['frequency_name'],$data[$s]['covenant_name'],$project_details[0]['project_title'],$data[$s]['module_type'],date('F d, Y',strtotime($data[$s]['covenant_start_date'])),$mail_link),$this->lang->line('covenant_notification_mail'));
                }
                else if($data[$s]['module_type']=='collateral')
                {
                    $collateral = $this->Collateral_model->getCollateral(array('id_collateral' => $data[$s]['module_id']));

                    if(isset($collateral[0]['collateral_status']) && $collateral[0]['collateral_status'] === 0){
                        break;
                    }

                    if($data[$s]['reference_type']=='documents')
                    {
                        $collateral_documents = $this->Collateral_model->getCollateralDocument(array('id_collateral_document' => $data[$s]['reference_id']));
                        $data[$s]['covenant_name'] = $collateral_documents[0]['document_name'].' documents';
                    }
                    else if($data[$s]['reference_type']=='insurance')
                    {
                        $collateral_insurance = $this->Collateral_model->getInsuranceDetails(array('id_insurance' => $data[$s]['reference_id']));
                        $data[$s]['covenant_name'] = 'insurance number '.$collateral_insurance[0]['insurance_number'];
                    }

                    $link = '<a class="sky-blue" href="'.WEB_BASE_URL.'#/covenant/list?covenantId='.base64_encode($data[$s]['id_module_covenant']).'">here</a>';
                    $notification = str_replace(array('{frequency}','{covenant}','{project}','{module}','{date}','{here}'),array($data[$s]['frequency_name'],$data[$s]['covenant_name'],'collateral '.$collateral[0]['collateral_number'],$data[$s]['module_type'],date('F d, Y',strtotime($data[$s]['covenant_start_date'])),$link),$this->lang->line('covenant_notification_message'));

                    $notification_subject = $collateral[0]['collateral_number'];
                    $mail_link = '<a style="color: #22bcf2" href="'.WEB_BASE_URL.'#/covenant/list?id='.base64_encode($data[$s]['id_module_covenant']).'">here</a>';
                    $message = str_replace(array('{name}','{frequency}','{covenant}','{project}','{module}','{date}','{here}'),array($assigned_to_name[$r],$data[$s]['frequency_name'],$data[$s]['covenant_name'],$collateral[0]['collateral_number'],$data[$s]['module_type'],date('F d, Y',strtotime($data[$s]['covenant_start_date'])),$mail_link),$this->lang->line('covenant_notification_mail'));
                }

                $this->Notification_model->addNotification(array(
                    'assigned_to' => $assigned_to[$r],
                    'module_reference_id' => $data[$s]['id_module_covenant'],
                    'module_reference_type' => 'covenant_task',
                    'notification_subject' => $notification_subject,
                    'notification_template' => $notification,
                    'notification_link' => $link,
                    'notification_comments' => '',
                    'notification_type' => 'alert',
                    'created_date_time' => currentDate(),
                    'module_id' => $data[$s]['module_id'],
                    'module_type' => $data[$s]['module_type']
                ));

                //mails
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'base_url' => REST_API_URL,
                    'to_username' => $assigned_to_name[$r],
                    'message' => $message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );

                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                sendmail($assigned_to_mail[$r],str_replace(array('{title}'),array($data[$s]['covenant_name']),$this->lang->line('covenant_notification_subject')),$template_data);

            }

            $due_date = '';
            if(strtolower($data[$s]['frequency_key'])=='yearly'){
                $due_date = date('Y-m-d', strtotime('+1 year', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='half_yearly'){
                $due_date = date('Y-m-d', strtotime('+6 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='quarterly'){
                $due_date = date('Y-m-d', strtotime('+3 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='monthly'){
                $due_date = date('Y-m-d', strtotime('+1 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='weekly'){
                $due_date = date('Y-m-d', strtotime('+7 day', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='daily'){
                $due_date = date('Y-m-d', strtotime('+1 day', strtotime($data[$s]['next_due_date'])));
            }
            $notification_date = date('Y-m-d',strtotime('-'.$data[$s]['notice_days'].' days',strtotime($due_date)));

            $this->Covenant_model->updateModuleCovenant(array(
                'id_module_covenant' => $data[$s]['id_module_covenant'],
                'next_due_date' => $due_date,
                'notification_date' => $notification_date,
                'module_covenant_status' => 'in progress'
            ));
        }
    }
}
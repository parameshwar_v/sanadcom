<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Cleanup extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cleanup_model');
    }

    public function updateContact_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('contact_id', array('required' => $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['update_data'] = array(
            'created_by' => $data['created_by']
        );
        $this->Cleanup_model->updateContact($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateCompany_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['update_data'] = array(
            'created_by' => $data['created_by']
        );
        $this->Cleanup_model->updateCompany($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateProject_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required' => $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['update_data'] = array(
            'created_by' => $data['created_by']
        );
        $this->Cleanup_model->updateProject($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['crm_project_id']) && !isset($data['crm_company_id']) && !isset($data['crm_contact_id']))
        {
            $this->form_validator->add_rules('crm_project_id', array('required' => $this->lang->line('project_id_req')));
        }

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['crm_project_id']))
        {
            $this->Cleanup_model->deleteTouchPoints(array('module_id' => $data['crm_project_id'],'module_type' => 'project'));
        }
        else if(isset($data['crm_company_id']))
        {
            $this->Cleanup_model->deleteTouchPoints(array('module_id' => $data['crm_company_id'],'module_type' => 'company'));
        }
        else if(isset($data['crm_contact_id']))
        {
            $this->Cleanup_model->deleteTouchPoints(array('module_id' => $data['crm_contact_id'],'module_type' => 'contact'));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /* * * Delete Contact/Company/Project * * */

    public function project_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required' => $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Cleanup_model->deleteProjectData($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Cleanup_model->deleteCompanyData($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id', array('required' => $this->lang->line('crm_contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Cleanup_model->deleteContactData($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

}
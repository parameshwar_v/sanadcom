<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Mismaster extends REST_Controller
{

    public $ordered_committee_data      = array();
    public $forward_to                  = '';
    public $ordered_data                = '';
    public $last_node                   = '';
    public $count                       = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mismaster_model');
        $this->load->model('User_model');
        $this->load->model('Company_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('Project_model');
        $this->load->model('Collateral_model');

        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];
        }

        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if(!empty($user_info) && $user_info[0]['user_role_id']==3 && $user_info[0]['all_projects']==0)
        {
            $this->current_user = $user_id;
        }
        else{
            $this->current_user = 0;
        }
    }

    public function uploadFacilityPlans_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($_FILES) && !empty($_FILES['file']['name']['excel']))
        {
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) )
            {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['file']['size']['excel']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>str_replace('MUZ',EXCEL_UPLOAD_SIZE,$this->lang->line('max_upload_size')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        switch ($data['financeData']['type'])
        {
            case 'disbursement':
                $headers = array('CONTRACT_REF_NO', 'VALUE_DT', 'TRN_DT', 'DISBURSED_CCY', 'AMOUNT', 'EXCH_RATE', 'USD_EQUIV');
                $dateColumn_1 = 'VALUE_DT';
                $dateColumn_2 = 'TRN_DT';
                break;
            case 'collection':
                $headers = array('CONTRACT_REF_NO','CUSTOMER_NAME', 'USER_REF_NO', 'AC_CCY', 'PAID_DATE', 'EVENT', 'EXCH_RATE', 'AMOUNT', 'USD_EQUIV');
                $dateColumn_1 = 'PAID_DATE';
                break;
            case 'repayment':
                $headers = array('CONTRACT_REF_NO','DUE_DATE', 'PRINCIPAL', 'INTEREST_DUE', 'MGT_FEE', 'DEFAULT_INTEREST', 'TOTAL_DUE', 'BALANCE','CURRENCY','USD_EQUIV');
                $dateColumn_1 = 'DUE_DATE';
                break;
            default:
                $result = array('status'=>FALSE,'error'=>$this->lang->line('Invalid data'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
                break;
        }
        $data_value = array();
        $objPHPExcel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']['excel']);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        $colArray = array();
        $currentRow = 0;
        foreach ($cell_collection as $cell)
        {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue());
            //Finds the columns is present, then pushed to array
            if(in_array($temp_value , $headers))
            {
                array_push($colArray, $column);
                $currentRow = $row;
            }
        }

        if(count($colArray)>0 && count($headers) == count($colArray))
        {
            $tempRow = $currentRow;
            $tempRow++;
            $break = 0;
            $flag_index = 0;
            //loop to get data after column is matched
            while($break == 0)
            {
                $temp = [];
                foreach($colArray as $key=>$cell)
                {
                    $flag_index++;
                    if($data['financeData']['type'] == 'disbursement')
                    {
                        if($dateColumn_1 == $headers[$key] || $dateColumn_2 == $headers[$key])
                        {
                            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getFormattedValue());
                        }
                        else
                        {
                            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getValue());
                        }
                    }
                    elseif($data['financeData']['type'] == 'collection' || $data['financeData']['type'] == 'repayment')
                    {
                        if($dateColumn_1 == $headers[$key])
                        {
                            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getFormattedValue());
                        }
                        else
                        {
                            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getValue());
                        }
                    }
                    if(($temp_value == '' || $temp_value == null || $temp_value == 'Sum:') && ($key == 0))
                    {
                        $break = 1;
                    }
                    else
                    {
                        $temp[$headers[$key]] = $temp_value;
                    }
                }
                $tempRow++;
                if($break==0)
                    $data_value[] = $temp;
            }
        }
        if(count($headers) != count($colArray))
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_format'), 'data'=>$data_value);
        }
        elseif(count($colArray)==0)
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records'), 'data'=>$data_value);
        }
        else
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('upload_success'), 'data'=>$data_value);
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveFacilityPlans_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $mis_type=$data['type'];
        $company_id=$data['id_company'];
        $created_by=$data['created_by'];
        $facility_data=$data['facility_data'];
        foreach ( $facility_data as $k=>$v )
        {
            $facility_data[$k] ['id_company'] = $company_id;
            $facility_data[$k] ['created_by'] = $created_by;
            $facility_data[$k] ['created_on'] = currentDate();
            $facility_data[$k] ['contract_id'] = $facility_data[$k] ['CONTRACT_REF_NO'];
            unset($facility_data[$k]['CONTRACT_REF_NO']);
            if($mis_type == 'disbursement')
            {
                $facility_data[$k] ['VALUE_DT'] = date("Y-m-d", strtotime($facility_data[$k] ['VALUE_DT']));
                $facility_data[$k] ['TRN_DT'] = date("Y-m-d", strtotime($facility_data[$k] ['TRN_DT']));

                $facility_data[$k] ['AMOUNT'] = str_replace(',','',$facility_data[$k] ['AMOUNT']);
                $facility_data[$k] ['EXCH_RATE'] = str_replace(',','',$facility_data[$k] ['EXCH_RATE']);
                $facility_data[$k] ['USD_EQUIV'] = str_replace(',','',$facility_data[$k] ['USD_EQUIV']);
            }
            elseif($mis_type == 'repayment')
            {
                $facility_data[$k] ['DUE_DATE'] = date("Y-m-d", strtotime($facility_data[$k] ['DUE_DATE']));

                $facility_data[$k] ['PRINCIPAL'] = str_replace(',','',$facility_data[$k] ['PRINCIPAL']);
                $facility_data[$k] ['INTEREST_DUE'] = str_replace(',','',$facility_data[$k] ['INTEREST_DUE']);
                $facility_data[$k] ['MGT_FEE'] = str_replace(',','',$facility_data[$k] ['MGT_FEE']);
                $facility_data[$k] ['TOTAL_DUE'] = str_replace(',','',$facility_data[$k] ['TOTAL_DUE']);
                $facility_data[$k] ['BALANCE'] = str_replace(',','',$facility_data[$k] ['BALANCE']);
                $facility_data[$k] ['USD_EQUIV'] = str_replace(',','',$facility_data[$k] ['USD_EQUIV']);
                //unset($facility_data[$k]['TOTAL_DUE']);
            }
            elseif($mis_type == 'collection')
            {
                $facility_data[$k] ['PAID_DATE'] = date("Y-m-d", strtotime($facility_data[$k] ['PAID_DATE']));

                $facility_data[$k] ['AMOUNT'] = str_replace(',','',$facility_data[$k] ['AMOUNT']);
                $facility_data[$k] ['EXCH_RATE'] = str_replace(',','',$facility_data[$k] ['EXCH_RATE']);
                $facility_data[$k] ['USD_EQUIV'] = str_replace(',','',$facility_data[$k] ['USD_EQUIV']);

            }


            $facility_data[$k]=array_change_key_case($facility_data[$k],CASE_LOWER);
        }
        $facility_response_data = $this->Mismaster_model->saveFacilityPlans($facility_data,$mis_type,$created_by);
        if($facility_response_data)
        {
            $responsedata['type']=$mis_type;
            $responsedata['facilityData']='';
            if($mis_type == 'collection' && is_array($facility_response_data))
            {
                $responsedata['facilityData']=$facility_response_data;
            }
            $result = array('status'=>TRUE, 'message' => $this->lang->line('save_success'), 'data'=>$responsedata);
        }
        else
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('save_failed'), 'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateFacilityPlans_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $mis_type=$data['type'];
        $company_id=$data['id_company'];
        $created_by=$data['created_by'];
        $facility_data=$data['facility_data'];
        foreach ( $facility_data as $k=>$v )
        {
            $facility_data[$k] ['contract_id'] = $facility_data[$k] ['CONTRACT_REF_NO'];
            unset($facility_data[$k]['CONTRACT_REF_NO']);
            $facility_data[$k] ['id_company'] = $company_id;
            $facility_data[$k] ['created_by'] = $created_by;
            $facility_data[$k] ['created_on'] = currentDate();
            if($facility_data[$k] ['STATUS'])
            {
                if($mis_type == 'collection')
                {
                    $facility_data[$k] ['PAID_DATE'] = date("Y-m-d", strtotime($facility_data[$k] ['PAID_DATE']));

                    $facility_data[$k] ['AMOUNT'] = str_replace(',','',$facility_data[$k] ['AMOUNT']);
                    $facility_data[$k] ['EXCH_RATE'] = str_replace(',','',$facility_data[$k] ['EXCH_RATE']);
                    $facility_data[$k] ['USD_EQUIV'] = str_replace(',','',$facility_data[$k] ['USD_EQUIV']);
                    $facility_data[$k]=array_change_key_case($facility_data[$k],CASE_LOWER);
                }
            }
            else
            {
                unset($facility_data[$k]);
            }
        }
        $facility_data=array_values($facility_data);
        $facility_response_data = $this->Mismaster_model->insertDuplicateFacilityCollections($facility_data,$mis_type,$created_by);
        if($facility_response_data)
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('save_success'), 'data'=>'');
        }
        else
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('save_failed'), 'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadAgingAnalysis_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($_FILES) && !empty($_FILES['file']['name']['excel']))
        {
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) )
            {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['file']['size']['excel']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>str_replace('MUZ',EXCEL_UPLOAD_SIZE,$this->lang->line('max_upload_size')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $headers = array('CLIENT', 'REF_NO', 'Status', 'CCY', 'NORMAL{0-30Days}', 'WATC{31-90Days}', 'SUBS{91-180Days}', 'DOUB{181-360Days}', 'LOSS{Above_361Days}', 'Total_Arrears', 'LCY_Arrears','Proposed');
        $data_value = array();
        $objPHPExcel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']['excel']);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        $colArray = array();
        $currentRow = 0;
        foreach ($cell_collection as $cell)
        {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue());
            //Finds the columns is present, then pushed to array
            if(in_array($temp_value , $headers))
            {
                array_push($colArray, $column);
                $currentRow = $row;
            }
        }

        if(count($colArray)>0 && count($headers) == count($colArray))
        {
            $tempRow = $currentRow;
            $tempRow++;
            $break = 0;
            $flag_index = 0;
            //loop to get data after column is matched
            while($break == 0)
            {
                $temp = [];
                foreach($colArray as $key=>$cell)
                {
                    $flag_index++;
                    //if($data['financeData']['type'] == 'disbursement')
                    {
                        $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getValue());
                    }
                    if(($temp_value == '' || $temp_value == null || $temp_value == 'Sum:') && ($key == 0))
                    {
                        $break = 1;
                    }
                    else
                    {
                        $temp[$headers[$key]] = $temp_value;
                    }
                }
                $tempRow++;
                if($break==0)
                    $data_value[] = $temp;
            }
        }
        if(count($headers) != count($colArray))
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_format'), 'data'=>$data_value);
        }
        elseif(count($colArray)==0)
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records'), 'data'=>$data_value);
        }
        else
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('upload_success'), 'data'=>$data_value);
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveAgingAnalysis_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $created_by=$data['created_by'];
        $mis_type=$data['type'];
        $date=date_create($data['analysis_month']);
        $analysis_month=date_format($date,"Y-m-d");
        $aging_data=json_decode($data['aging_data'],true);
        foreach ( $aging_data as $k=>$v )
        {
            $aging_data[$k] ['created_by'] = $created_by;
            $aging_data[$k] ['created_on'] = currentDate();
            $aging_data[$k] ['client'] = $aging_data[$k] ['CLIENT'];
            $aging_data[$k] ['contract_id'] = $aging_data[$k] ['REF_NO'];
            $aging_data[$k] ['analysis_month'] = $analysis_month;
            unset($aging_data[$k]['REF_NO']);
            $aging_data[$k]=array_change_key_case($aging_data[$k],CASE_LOWER);
        }
        $facility_response_data = $this->Mismaster_model->saveFacilityPlans($aging_data,$mis_type,$created_by);
        if($facility_response_data)
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('save_success'), 'data'=>'');
        }
        else
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('save_failed'), 'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadOutstandingAmount_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($_FILES) && !empty($_FILES['file']['name']['excel']))
        {
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) )
            {
                $result = array('status'=>FALSE,'error'=>$this->lang->line('xls_valid'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('upload_excel'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['file']['size']['excel']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>str_replace('MUZ',EXCEL_UPLOAD_SIZE,$this->lang->line('max_upload_size')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $headers = array('CLIENT','REF_NO','CCY','AMOUNT','USD_EQUIV','ARREARS','TOTAL_SPEC_PROV_CF','PROV_ON_NPL_INCOME','GEN_PROV','TOTAL_PROVISION','NET_EXP');
        $data_value = array();
        $objPHPExcel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']['excel']);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        $colArray = array();
        $currentRow = 0;
        foreach ($cell_collection as $cell)
        {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue());
            //Finds the columns is present, then pushed to array
            if(in_array($temp_value , $headers))
            {
                array_push($colArray, $column);
                $currentRow = $row;
            }
        }

        if(count($colArray)>0 && count($headers) == count($colArray))
        {
            $tempRow = $currentRow;
            $tempRow++;
            $break = 0;
            $flag_index = 0;
            //loop to get data after column is matched
            while($break == 0)
            {
                $temp = [];
                foreach($colArray as $key=>$cell)
                {
                    $flag_index++;
                    $temp_value = trim($objPHPExcel->getActiveSheet()->getCell($cell.''.$tempRow)->getValue());
                    if(($temp_value == '' || $temp_value == null || $temp_value == 'Sum:') && ($key == 0))
                    {
                        $break = 1;
                    }
                    else
                    {
                        $temp[$headers[$key]] = $temp_value;
                    }
                }
                $tempRow++;
                if($break==0)
                    $data_value[] = $temp;
            }
        }
        if(count($headers) != count($colArray))
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_format'), 'data'=>$data_value);
        }
        elseif(count($colArray)==0)
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('no_records'), 'data'=>$data_value);
        }
        else
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('upload_success'), 'data'=>$data_value);
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function saveOutstandingAmount_post()
    {
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $created_by=$data['created_by'];
        $mis_type=$data['type'];
        $date=date_create($data['outstanding_month']);
        $outstanding_date=date_format($date,"Y-m-d");
        $outstanding_data=json_decode($data['outstanding_data'],true);
        foreach ( $outstanding_data as $k=>$v )
        {
            $outstanding_data[$k] ['created_by'] = $created_by;
            $outstanding_data[$k] ['created_on'] = currentDate();
            $outstanding_data[$k] ['AMOUNT'] = str_replace(',','',$outstanding_data[$k] ['AMOUNT']);
            $outstanding_data[$k] ['USD_EQUIV'] = str_replace(',','',$outstanding_data[$k] ['USD_EQUIV']);
            $outstanding_data[$k] ['client'] = $outstanding_data[$k] ['CLIENT'];
            $outstanding_data[$k] ['contract_id'] = $outstanding_data[$k] ['REF_NO'];
            $outstanding_data[$k] ['outstanding_date'] = $outstanding_date;
            unset($outstanding_data[$k]['REF_NO']);
            $outstanding_data[$k]=array_change_key_case($outstanding_data[$k],CASE_LOWER);
        }
        $facility_response_data = $this->Mismaster_model->saveFacilityPlans($outstanding_data,$mis_type,$created_by);
        if($facility_response_data)
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('save_success'), 'data'=>'');
        }
        else
        {
            $result = array('status'=>FALSE, 'error' => $this->lang->line('save_failed'), 'data'=>'');
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getModuleItems_get()
    {
        $data = $this->input->get();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $module_name = $data['module_name'];
        $module['company_id'] = $data['company_id'];
        if($module_name == 'client')
            $result = $this->Crm_model->getContactList($module);
        elseif($module_name == 'company')
            $result = $this->Crm_model->getCompanyList($module);
        elseif($module_name == 'project')
        {
            $module['all'] = 1;
            $result = $this->Crm_model->getProjectList($module);
        }
        elseif($module_name == 'collateral')
        {
            $module['all'] = 1;
            $result = $this->Collateral_model->getCollateralList($module);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function getAuditLogItems_get()
    {
        $data = $this->input->get();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $module_name = $data['module_name'];
        $search_date = '';
        $todate='';
        //if(isset($data['search_date']) && empty($data['search_date']))
        //echo $data['search_date'];exit;
        if($data['search_date'] && $data['search_date']!='' && $data['search_date']!=NULL)
        {
            $search_date = strtotime($data['search_date']);
            $todate=date('d-m-Y',strtotime($data['search_date'].' +1 day'));
            $search_date = date('d-m-Y', $search_date );
        }
        /*if($module_name == 'client')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=crm_contact_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$search_date;
        elseif($module_name == 'company')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=crm_company_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$search_date;
        elseif($module_name == 'project')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=project_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$search_date;
        elseif($module_name == 'collateral')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=id_collateral&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$search_date;*/

        if($module_name == 'client')
            $url = MONGO_SERVICE_PHP_URL.'getAuditLog.php?key=crm_contact_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$todate;
        elseif($module_name == 'company')
            $url = MONGO_SERVICE_PHP_URL.'getAuditLog.php?key=crm_company_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$todate;
        elseif($module_name == 'project')
            $url = MONGO_SERVICE_PHP_URL.'getAuditLog.php?key=project_id&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$todate;
        elseif($module_name == 'collateral')
            $url = MONGO_SERVICE_PHP_URL.'getAuditLog.php?key=id_collateral&value='.$data['module_id'].'&fromdate='.$search_date.'&todate='.$todate;


        /*if($module_name == 'client')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=crm_contact_id&value='.$data['module_id'];
        elseif($module_name == 'company')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=crm_company_id&value='.$data['module_id'];
        elseif($module_name == 'project')
            $url = MONGO_SERVICE_URL.'getAuditLog/?key=project_id&value='.$data['module_id'];*/
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);
        $result = '';
        if($output !='' )
        {
            if(isset(json_decode($output)->rows))
            {
                $result = json_decode($output)->rows;
                if($result != '')
                {
                    for($fc=0;$fc<count($result);$fc++)
                    {
                        $userdata['id'] = $result[$fc]->updated_by;
                        $user_details = $this->Company_model->getCompanyUser($userdata['id']);
                        $result[$fc]->updated_by = $user_details->first_name.' '.$user_details->last_name;
                        $result[$fc]->branch_name = $user_details->branch_name;
                    }
                }
            }

        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

class Notifications extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Covenant_model');
        $this->load->model('Notification_model');
        $this->lang->load('rest_controller_lang');
    }

    public function cron()
    {
        $data = $this->Covenant_model->getModuleCovenants(array('notification_date' => date('Y-m-d',strtotime(currentDate()))));
        //echo "<pre>"; print_r($data); exit;
        for($s=0;$s<count($data);$s++)
        {
            $this->Covenant_model->addCovenantTask(array(
                'module_covenant_id' => $data[$s]['id_module_covenant'],
                'task_due_date' => $data[$s]['next_due_date'],
                'task_condition' => '',
                'task_comments' => '',
                'created_date_time' => currentDate()
            ));
            $assigned_to = $assigned_to_mail = $assigned_to_name = array();

            if($data[$s]['assigned_to']) {
                $assigned_to = explode(',', $data[$s]['assigned_to']);
                //$assigned_to_mail = explode(',', $data[$s]['email']);
                //$assigned_to_name = explode(',', $data[$s]['assigned_to_name']);
            }

            for($r=0;$r<count($assigned_to);$r++)
            {
                $this->Notification_model->addNotification(array(
                    'assigned_to' => $assigned_to[$r],
                    'module_reference_id' => $data[$s]['id_module_covenant'],
                    'module_reference_type' => 'covenant_task',
                    'notification_template' => 'covenant_notification_message',
                    'notification_link' => WEB_BASE_URL.'#/project/project-covenant/'.base64_encode($data[$s]['module_id']),
                    'notification_comments' => '',
                    'notification_type' => 'notification',
                    'created_date_time' => currentDate(),
                ));

                //mails
                /*$template_data = array(
                    'base_url' => REST_API_URL,
                    'link' => WEB_BASE_URL.'html/index.html#/project/project-covenant/'.base64_encode($data[$s]['module_id']),
                    'message' => str_replace(array('{name}','{label}'),array($assigned_to_name[$r],$data[$s]['covenant_name']),$this->lang->line('covenant_notification_mail')),
                'mail_footer' => $this->lang->line('mail_footer')
                );
                $template_data = $this->parser->parse('templates/Notification.html', $template_data);
                sendmail($assigned_to_mail[$r],$this->lang->line('covenant_notification_subject'),$template_data);*/
            }

            $due_date = '';
            if(strtolower($data[$s]['frequency_key'])=='yearly'){
                $due_date = date('Y-m-d', strtotime('+1 year', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='half_yearly'){
                $due_date = date('Y-m-d', strtotime('+6 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='quarterly'){
                $due_date = date('Y-m-d', strtotime('+3 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='monthly'){
                $due_date = date('Y-m-d', strtotime('+1 month', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='weekly'){
                $due_date = date('Y-m-d', strtotime('+7 day', strtotime($data[$s]['next_due_date'])));
            }
            if(strtolower($data[$s]['frequency_key'])=='daily'){
                $due_date = date('Y-m-d', strtotime('+1 day', strtotime($data[$s]['next_due_date'])));
            }
            $notification_date = date('Y-m-d',strtotime('-'.$data[$s]['notice_days'].' days',strtotime($due_date)));

            $this->Covenant_model->updateModuleCovenant(array(
                'id_module_covenant' => $data[$s]['id_module_covenant'],
                'next_due_date' => $due_date,
                'notification_date' => $notification_date,
                'module_covenant_status' => 'in progress'
            ));
        }

    }

    public function getList_get()
    {
        $data = $this->input->get();

        if(empty($data)){
            $result = array('status'=>FALSE,'message'=>$this->lang->line('invalid_data'),'data'=>'');
            echo json_encode($result); exit;
        }

        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            echo json_encode($result);exit;
        }

        $result = $this->Notification_model->getNotifications($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'], 'profile');
            $result[$s]['module_reference_type'] = ucwords(str_replace('_',' ',$result[$s]['module_reference_type']));
        }
        $filters = array('all','new','workflow','task','alert','meeting');
        $filterCount = [];
        for($fl=0;$fl<count($filters);$fl++)
        {

            $filterCount[$filters[$fl]] = $this->Notification_model->getNotificationsCount(array(
                'user_id' => $data['user_id'],
                'filter' => $filters[$fl]
            ));
            $filterCount[$filters[$fl]]  = $filterCount[$filters[$fl]][0]['total'];
        }
        $total = $this->Notification_model->getNotificationsCount($data);
        $total_records = $total[0]['total'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records,'filter_count' => $filterCount));
        echo json_encode($result);exit;
    }

    public function notificationCount_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'message'=>$this->lang->line('invalid_data'),'data'=>'');
            echo json_encode($result); exit;
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            echo json_encode($result);exit;
        }

        $result = $this->Notification_model->getNotificationsCount(array('user_id' => $data['user_id'],'notification_status' => 'unread'));
        $result = $result[0]['total'];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        echo json_encode($result);exit;
    }

    public function notification_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_notification', array('required'=> $this->lang->line('notification_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data['updated_date_time'] = currentDate();
        $this->Notification_model->updateNotification($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function module_get()
    {
        $data = $this->input->get();

        if(empty($data)){
            $result = array('status'=>FALSE,'message'=>$this->lang->line('invalid_data'),'data'=>'');
            echo json_encode($result); exit;
        }

        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            echo json_encode($result);exit;
        }

        $result = $this->Notification_model->getNotifications($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'], 'profile');
            $result[$s]['module_reference_type'] = ucwords(str_replace('_',' ',$result[$s]['module_reference_type']));
        }

        $total_records = $this->Notification_model->getNotificationsCount($data);
        $total_records = $total_records[0]['total'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' => $result,'total_records' => $total_records));
        echo json_encode($result);exit;
    }

}
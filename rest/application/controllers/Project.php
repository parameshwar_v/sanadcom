<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Project extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('Workflow_model');
    }

    public function info_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project = $this->Project_model->getProject(array('crm_project_id' =>$data['crm_project_id']));
        //getting project companies
        $project_company = $this->Project_model->getProjectCompany(array('crm_project_id' => $data['crm_project_id']));
        $project_company = array_map(function($key){ return $key['company_name']; }, $project_company);
        $project[0]['company_name'] = implode(',',$project_company);
        //getting project contacts
        $project_contact = $this->Project_model->getProjectContact(array('crm_project_id' => $data['crm_project_id']));
        $project_contact = array_map(function($key){ return $key['first_name']; }, $project_contact);
        $project[0]['contact_name'] = implode(',',$project_contact);

        $project[0]['rating'] = '---';
        $project[0]['grade'] = '---';

        $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $project[0]['id_crm_project']));
        $project[0]['no_of_loans'] = count($facilities);
        $loan_amount = array(); $unique_currency_code = array(); $usd_loan_amount = $project[0]['intermediary_loan'] = 0;
        //echo "<pre>"; print_r($facilities); exit;
        for($s=0;$s<count($facilities);$s++){
            //if(!in_array($facilities[$s]['currency_code'],$unique_currency_code)){
                array_push($unique_currency_code,$facilities[$s]['currency_code']);
                $loan_amount[$facilities[$s]['currency_code']] = array(
                    'currency_code' => $facilities[$s]['currency_code'],
                    'amount' => $facilities[$s]['facility_amount']
                );
                $usd_loan_amount = $usd_loan_amount+$facilities[$s]['facility_usd_amount'];
            //}
            //else{
              //  $loan_amount[$facilities[$s]['currency_code']]['amount'] = $loan_amount[$facilities[$s]['currency_code']]['amount'] + $facilities[$s]['facility_amount'];
            //}

            if($facilities[$s]['loan_payment_type_key_name']=='intermediary_loan'){
                $project[0]['intermediary_loan'] = 1;
            }

        }
        //sum of usd amount
        $loan_amount = array();
        $loan_amount[] = array(
            'currency_code' => 'USD',
            'amount' => $usd_loan_amount
        );

        $project[0]['loan_amount'] = array_values($loan_amount);
        $project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $project[0]['project_stage_info_id']));
        $project[0]['stage_id'] = $project_stage_info[0]['stage_id'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$project[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function forms_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        //$project_details = $this->Crm_model->getProject($data['crm_project_id']);
        $result = $this->Project_model->getStageForms(array('crm_project_id' => $data['crm_project_id']));
        $project_stage_info = $this->Project_model->getProjectStageInfo(array('project_id' => $data['crm_project_id']));

        $stage_info = array();
        for($s=0;$s<count($project_stage_info);$s++){
            $stage_info[$project_stage_info[$s]['stage_id']] = $project_stage_info[$s];
        }
        $result_array = array();
        //echo "<pre>"; print_r($result); exit;
        $most_recent_updated_date = $most_recent_updated_by = 0;
        for($s=0;$s<count($result);$s++)
        {
            if(!isset($result_array[$result[$s]['id_project_stage']]['updated_date_time'])){
                $most_recent_updated_date = $most_recent_updated_by = 0;
            }
            if(isset($stage_info[$result[$s]['id_project_stage']])){
                $result_array[$result[$s]['id_project_stage']]['version_number'] = $stage_info[$result[$s]['id_project_stage']]['version_number'];
                $result_array[$result[$s]['id_project_stage']]['user_name'] = $stage_info[$result[$s]['id_project_stage']]['user_name'];
            }
            if($result[$s]['end_date_time']==''){ $result[$s]['end_date_time'] = currentDate(); }
            $time_taken = date_diff(date_create($result[0]['end_date_time']),date_create($result[0]['start_date_time']));
            $result_array[$result[$s]['id_project_stage']]['time_taken'] = $time_taken->days;
            $result_array[$result[$s]['id_project_stage']]['created_date_time'] = $result[$s]['start_date_time'];
            $result_array[$result[$s]['id_project_stage']]['id_project_stage_info'] = $result[$s]['id_project_stage_info'];
            $result_array[$result[$s]['id_project_stage']]['id_project_stage'] = $result[$s]['id_project_stage'];
            $result_array[$result[$s]['id_project_stage']]['stage_name'] = $result[$s]['stage_name'];
            $result_array[$result[$s]['id_project_stage']]['stage_key'] = $result[$s]['stage_key'];
            $result_array[$result[$s]['id_project_stage']]['stage_class'] = $result[$s]['stage_class'];
            $result_array[$result[$s]['id_project_stage']]['project_stage_status'] = $result[$s]['project_stage_status'];
            $result_array[$result[$s]['id_project_stage']]['project_stage_versions'] = $this->Project_model->getProjectStageVersions(array('project_stage_info_id' => $result[$s]['id_project_stage_info']));;
            if(isset($result[$s]['project_section_id']) && $result[$s]['project_section_id']!=''){
                $result_array[$result[$s]['id_project_stage']]['section'][$result[$s]['project_section_id']]['project_section_id'] = $result[$s]['project_section_id'];
                $result_array[$result[$s]['id_project_stage']]['section'][$result[$s]['project_section_id']]['section_name'] = $result[$s]['section_name'];
                $result_array[$result[$s]['id_project_stage']]['section'][$result[$s]['project_section_id']]['section_key'] = $result[$s]['section_key'];
                if(isset($result[$s]['project_form_id']) && $result[$s]['project_form_id']!=''){
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_project_stage_section_form'], 'reference_type' => 'form'));
                    if(!empty($module_status)){ $status = 1; }else{ $status = 0; }
                    $last_updated_time = $user_name = '';
                    $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'],'project_stage_section_form_id' => $result[$s]['id_project_stage_section_form']));
                    if(!empty($last_update)){
                        $last_updated_time = $last_update[0]['updated_date_time'];
                        $user_name = $last_update[0]['user_name'];
                        $curDate = strtotime($last_updated_time);
                        if($curDate>$most_recent_updated_date){
                            $most_recent_updated_date = $curDate;
                            $most_recent_updated_by = $user_name;
                        }
                    }
                    $result_array[$result[$s]['id_project_stage']]['section'][$result[$s]['project_section_id']]['form'][] = array(
                        'project_form_id' => $result[$s]['project_form_id'],
                        'form_name' => $result[$s]['form_name'],
                        'form_key' => $result[$s]['form_key'],
                        'form_template' => $result[$s]['form_template'],
                        'form_class' => $result[$s]['form_class'],
                        'id_project_stage_section_form' => $result[$s]['id_project_stage_section_form'],
                        'status' => $status,
                        'comments_count' => $this->Project_model->getFormCommentsCount(array('reference_id' => $data['crm_project_id'],'reference_type' => 'project','project_stage_section_form_id' => $result[$s]['id_project_stage_section_form'])),
                        'updated_time' => $last_updated_time,
                        'updated_by' => $user_name
                    );
                }
            }
            if($most_recent_updated_date) {
                $result_array[$result[$s]['id_project_stage']]['updated_date_time'] = date('Y-m-d H:i:s', $most_recent_updated_date);
                $result_array[$result[$s]['id_project_stage']]['updated_by'] = $most_recent_updated_by;
            }
            else{
                $result_array[$result[$s]['id_project_stage']]['updated_date_time'] = '';
                $result_array[$result[$s]['id_project_stage']]['updated_by'] = '';
            }
        }
        //echo "<pre>"; print_r($result_array); exit;
        $result_array = array_values($result_array);
        for($s=0;$s<count($result_array);$s++)
        {
            if(isset($result_array[$s]['section']))
                $result_array[$s]['section'] = array_values($result_array[$s]['section']);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data' => $result_array);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formFields_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id_req')));
        $this->form_validator->add_rules('project_stage_id', array('required'=> $this->lang->line('project_stage_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_stage_info = $this->Project_model->getProjectStageInfo(array('project_id' => $data['crm_project_id'],'stage_id' => $data['project_stage_id']));

        //$project_field_data = $this->Project_model->getProjectFormInfo($data);

        $result = $this->Project_model->getProjectStageSectionForm(array('project_stage_section_form_id' => $data['project_stage_section_form_id'], 'project_id' => $data['crm_project_id']));

        $result = $result[0];
        $stage_details = $this->Project_model->getStage(array('id_project_stage' => $data['project_stage_id']));

        if(!empty($stage_details)) {
            $result['stage_name'] = $stage_details[0]['stage_name'];
            $result['stage_key'] = $stage_details[0]['stage_key'];
        }
        //getting section name
        $section = $this->Project_model->getProjectStageSection(array('id_project_stage_section' => $result['project_stage_section_id']));
        $result['section_name'] = $section[0]['section_name'];

        $result['fields'] = array();

        if(empty($project_stage_info))
        {
            $result['stage_name'] = $project_stage_info[0]['stage_name'];
            $result['fields'] = $this->Project_model->getFormFields($data);
        }
        else
        {
            $project_field_data = $this->Project_model->getProjectFormInfo($data);

            if(empty($project_field_data)){
                $result['fields'] = $this->Project_model->getFormFields($data);
                //echo $this->db->last_query(); exit;
                //echo '<pre>';print_r($result);exit;
            }
            else {
                $data['project_stage_info_id'] = $project_stage_info[0]['id_project_stage_info'];
                $result['fields'] = $this->Project_model->getProjectFormFieldData($data);
            }
        }

        if(isset($data['version_number']) && $data['version_number']!=$project_stage_info[0]['version_number'] && $data['version_number']<=$project_stage_info[0]['version_number'])
        {
            //getting mongo bd data if user request previous version data
            //$url = MONGO_SERVICE_URL.'findProjectStageVersionLog/?project_stage_info_id='.$project_stage_info[0]['id_project_stage_info'].'&version_number='.$data['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $url = MONGO_SERVICE_PHP_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$project_stage_info[0]['id_project_stage_info'].'&version_number='.$data['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);

            if(isset($output->data[0]->forms[0]->field_data))
                $output = $output->data[0]->forms[0]->field_data;
            else
                $output = array();

            $mongo_data = array();
            for($r=0;$r<count($output);$r++)
            {
                $mongo_data[$output[$r]->field_key] = (array)$output[$r]; //Building array with field key as key
            }

            for($s=0;$s<count($result['fields']);$s++)
            {
                if(isset($mongo_data[$result['fields'][$s]['field_key']])){ //replacing field value with mongo data if field key matches
                    $result['fields'][$s]['form_field_value'] = $mongo_data[$result['fields'][$s]['field_key']]['form_field_value'];
                }
                else{
                    $result['fields'][$s]['form_field_value'] = '';
                }
            }
        }

        //echo "<pre>"; print_r($result['fields']); exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilities_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getProjectFacilities($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formFields_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('id_project_stage_section_form', array('required'=> $this->lang->line('project_stage_section_form_id_req')));
        $this->form_validator->add_rules('data', array('required'=> $this->lang->line('data_req')));

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $values = $data['data'];
        $data_keys = array();
        foreach($values as $i){
            $data[$i['field_key']] = $i['form_field_value'];
            $data_keys[] = $i['field_key'];
        }

        $form_fields = $this->Project_model->getProjectStageSectionFormFields(array('project_stage_section_form_id' => $data['id_project_stage_section_form']));
        $data['form_id'] = $form_fields[0]['project_form_id'];
        $add = $new =  $update = array(); $focus_area = '';
        //checking form field data with db form fields
        for($s=0;$s<count($form_fields);$s++){
            if(in_array($form_fields[$s]['field_key'],$data_keys)){
                if($form_fields[$s]['field_type']=='json' || $form_fields[$s]['field_type']=='widget'){
                    $data[$form_fields[$s]['field_key']] = json_encode($data[$form_fields[$s]['field_key']]);
                }
                $add[] = array(
                    'project_stage_section_form_id' => $data['id_project_stage_section_form'],
                    'project_form_field_id' => $form_fields[$s]['id_project_form_field'],
                    'form_field_value' => $data[$form_fields[$s]['field_key']],
                    'updated_by' => $data['created_by'],
                    'created_date_time' => currentDate(),
                    'updated_date_time' => currentDate(),
                    'project_id' => $data['project_id']
                );
            }
            else{
                $add[] = array(
                    'project_stage_section_form_id' => $data['id_project_stage_section_form'],
                    'project_form_field_id' => $form_fields[$s]['id_project_form_field'],
                    'form_field_value' => '',
                    'updated_by' => $data['created_by'],
                    'created_date_time' => currentDate(),
                    'updated_date_time' => currentDate(),
                    'project_id' => $data['project_id']
                );
            }
        }
        $project_stage_info = $this->Project_model->getProjectStageInfo($data);
        //getting existing data for current stage
        $previous_info = $this->Project_model->getProjectFormInfo(array('project_id' => $data['project_id'],'project_stage_section_form_id' => $data['id_project_stage_section_form']));
        //echo "<pre>"; print_r($previous_info); exit;
        //adding data if not exists
        if(empty($previous_info))
        {
            if(empty($project_stage_info)){
                $project_stage_info_id = $this->Project_model->addProjectStageInfo(array(
                    'project_id' => $data['project_id'],
                    'stage_id' => $data['stage_id'],
                    'version_number' => 1,
                    'created_by' => $data['created_by'],
                    'start_date_time' => currentDate()
                ));
            }
            else{
                $project_stage_info_id = $project_stage_info[0]['id_project_stage_info'];
            }


            for($s=0;$s<count($add);$s++){
                $add[$s]['project_stage_info_id'] = $project_stage_info_id;
            }

            if(!empty($add)){
                $this->Project_model->addProjectStageFormInfo_batch($add);
            }
        }
        else
        {
            //comparing existing data with post form fields
            $existing_form_fields = $previous_info;

            for($s=0;$s<count($add);$s++){ $check_field = 0;
                for($r=0;$r<count($existing_form_fields);$r++){
                    if($add[$s]['project_form_field_id']==$existing_form_fields[$r]['project_form_field_id']){
                        $add[$s]['id_project_form_info'] = $existing_form_fields[$r]['id_project_form_info'];
                        unset($add[$s]['created_date_time']);
                        $update[] = $add[$s];
                        $check_field = 1;
                    }
                }
                if($check_field==0){
                    $add[$s]['project_stage_info_id'] = $project_stage_info[0]['id_project_stage_info'];
                    $new[] = $add[$s];
                }
            }

            if(!empty($update)){
                $this->Project_model->updateProjectStageFormInfo_batch($update);
            }
            if(!empty($new)){
                $this->Project_model->addProjectStageFormInfo_batch($new);
            }
        }

        $crm_project_data = array();
        if(isset($data['project_title'])){ $crm_project_data['project_title'] = $data['project_title']; }
        if(isset($data['project_main_sector'])){ $crm_project_data['project_main_sector_id'] = $data['project_main_sector']; }
        if(isset($data['project_sub_sector'])){ $crm_project_data['project_sub_sector_id'] = $data['project_sub_sector']; }
        if(isset($data['focus_area'])){
            $focus_area = $this->Master_model->getMasterChild(array('child_name' => $data['focus_area']));
            //echo "<pre>"; print_r($focus_area); exit;
            if(!empty($focus_area))
                $crm_project_data['focus_area'] = $focus_area[0]['id_child'];
        }
        else{
            $crm_project_data['focus_area'] = $focus_area;
        }

        if(isset($data['project_country'])){
            $country = $this->Master_model->getCountryByName(array('country_name' => $data['project_country']));
            if(!empty($country))
                $crm_project_data['country_id'] = $country[0]['id_country'];
        }

        if(!empty($crm_project_data)){
            $crm_project_data['id_crm_project'] = $data['project_id'];
            $this->Project_model->updateProject($crm_project_data);
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['id_project_stage_section_form']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['id_project_stage_section_form'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));

        $form_info = $this->Project_model->getProjectStageSectionForm(array('project_stage_section_form_id' => $data['id_project_stage_section_form']));
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'Information saved','module_type' => 'project','module_id' => $data['project_id'], 'activity_type' => 'form','activity_reference_id' => $data['id_project_stage_section_form'],'created_by' => $data['created_by'],'created_date_time' => currentDate()));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityDisbursement_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('contract_id', array('required'=> $this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getFacilityDisbursement($data['contract_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityLoanReceipts_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('contract_id', array('required'=> $this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getFacilityLoanReceipts($data['contract_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilityLoanRepayment_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('contract_id', array('required'=> $this->lang->line('contract_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getFacilityLoanRepayment($data['contract_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function view_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getProjectFormFields($data);
        $result_array = array();
        for($s=0;$s<count($result);$s++)
        {
            $result_array[$result[$s]['project_section_id']]['section_name'] = $result[$s]['section_name'];
            if(isset($result[$s]['project_form_id']) && $result[$s]['project_form_id']!=''){
                $result_array[$result[$s]['project_section_id']]['form'][$result[$s]['project_form_id']]['form_id'] = $result[$s]['project_form_id'];
                $result_array[$result[$s]['project_section_id']]['form'][$result[$s]['project_form_id']]['form_name'] = $result[$s]['form_name'];
                if(isset($result[$s]['id_project_form_field']) && $result[$s]['id_project_form_field']!='') {
                    $result_array[$result[$s]['project_section_id']]['form'][$result[$s]['project_form_id']]['fields'][] = array(
                        'field_label' => $result[$s]['field_label'],
                        'field_value' => $result[$s]['form_field_value']
                    );
                }
            }
        }
        $result_array = array_values($result_array);
        for($s=0;$s<count($result_array);$s++)
        {
            $result_array[$s]['form'] = array_values($result_array[$s]['form']);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_array);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stageFacilities_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $where = ['pf.project_stage_info_id' => $data['project_stage_info_id']];
        $facilities_array = $this->Project_model->getStageFacilities($where);
        for($f=0;$f<count($facilities_array);$f++)
        {
            $pricing_array = '';
            $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities_array[$f]['id_project_facility'],'field_section' => 'pricing'));
            for($r=0;$r<count($pricing);$r++){
                $pricing_array[$r] = array(
                    'field_name' => $pricing[$r]['field_name'],
                    'payment_type' => $pricing[$r]['payment_type'],
                    'label' => '',
                    'facility_field_value' => $pricing[$r]['facility_field_value'],
                    'facility_field_description' => $pricing[$r]['facility_field_description']
                );
                if($pricing[$r]['payment_type']=='interest_as'){
                    $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                    $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                }
                else if($pricing[$r]['payment_type']=='fee_as'){
                    $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                    $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                }
                else if($pricing[$r]['payment_type']=='fee_amount'){
                    $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                    $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                }
            }
            $pricing_array = array_values($pricing_array);
            $facilities_array[$f]['pricing'] = $pricing_array;
            $terms_and_conditions_array = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities_array[$f]['id_project_facility'],'field_section' => 'terms_conditions'));
            $facilities_array[$f]['terms_conditions'] = $terms_and_conditions_array;
        }

        if(empty($facilities_array))
        {
            //$url = MONGO_SERVICE_URL.'findFacilityDetails/?project_stage_info_id='.$data['project_stage_info_id'];
            $url = MONGO_SERVICE_PHP_URL.'findFacilityDetails.php?project_stage_info_id='.$data['project_stage_info_id'];
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);
            $result = json_decode($output,true);
            if(!empty($result)){ $facilities_array = $result['data'][0]['facilities']; }
            for($f=0;$f<count($facilities_array);$f++)
            {
                $facility_type_name = '';
                $facility_sub_type_name = '';
                $facility_term_name = '';
                $facility_source_of_funds = '';
                $data_array = $this->Master_model->getMasterChild(['child_id' => $facilities_array[$f]['facility_type']]);
                if(isset($data_array[0])) $facility_type_name = $data_array[0]['child_name'];
                $facilities_array[$f]['facility_type_name'] = $facility_type_name;

                $data_array = $this->Master_model->getMasterChild(['child_id' => $facilities_array[$f]['facility_sub_type']]);
                if(isset($data_array[0])) $facility_sub_type_name = $data_array[0]['child_name'];
                $facilities_array[$f]['facility_sub_type_name'] = $facility_sub_type_name;

                $data_array = $this->Master_model->getMasterChild(['child_id' => $facilities_array[$f]['facility_term']]);
                if(isset($data_array[0])) $facility_term_name = $data_array[0]['child_name'];
                $facilities_array[$f]['facility_term_name'] = $facility_term_name;

                $data_array = $this->Master_model->getMasterChild(['child_id' => $facilities_array[$f]['facility_source_of_payment']]);
                if(isset($data_array[0])) $facility_source_of_funds = $data_array[0]['child_name'];
                $facilities_array[$f]['facility_source_of_funds'] = $facility_source_of_funds;
                $pricing = $facilities_array[$f]['pricing'];
                for($r=0;$r<count($pricing);$r++){
                    if($pricing[$r]['payment_type']=='interest_as'){
                        $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                        $pricing[$r]['label'] = $time_period[0]['period_name'];
                    }
                    else if($pricing[$r]['payment_type']=='fee_as'){
                        $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                        $pricing[$r]['label'] = $fee_as[0]['facility_amount_type'];
                    }
                    else if($pricing[$r]['payment_type']=='fee_amount'){
                        $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                        $pricing[$r]['label'] = $currency[0]['currency_code'];
                    }
                }
                $facilities_array[$f]['pricing'] = $pricing;
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$facilities_array);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function stageView_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_stage_info', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_stage_details = $this->Project_model->getProjectStageDetails(array('project_stage_info_id' => $data['id_project_stage_info']));
        //echo "<pre>"; print_r($project_stage_details); exit;
        $data['project_id'] = $project_stage_details[0]['project_id'];
        $data['stage_id'] = $project_stage_details[0]['stage_id'];
        $result_array = array();

        $team = $this->Workflow_model->getProjectStageWorkflowDetails(array('project_stage_info_id' => $data['id_project_stage_info']));

        $team_array = array();
        for($s=0;$s<count($team);$s++){
            if($team[$s]['to_phase_name']!='')
            $team_array[] = array(
                'phase_name' => $team[$s]['to_phase_name'],
                'user_name' => $team[$s]['to_user_name']
            );
        }
        if(isset($team[count($team)-1]['to_phase_name']) && $team[count($team)-1]['to_phase_name']!='')
        $team_array[] = array(
            'phase_name' => $team[count($team)-1]['to_phase_name'],
            'user_name' => $team[count($team)-1]['to_user_name']
        );

        $team = array();
        foreach($team_array as $i){
            $team[$i['phase_name']] = $i;
        }
        $team_array = array_values($team);

        if(!empty($project_stage_details))
        {
            if($project_stage_details[0]['stage_key']=='indicative_term_sheet' || $project_stage_details[0]['stage_key']=='disbursement_process' || $project_stage_details[0]['stage_key']=='monitoring' ){

                $required_keys = array('project_title','borrower','application_date','country_office','main_sector','sub_sector','required_loan_amount_currency','project_description','project_concept_purpose','status_of_project_implementation','project_cost_financing_plan','summary_of_available_information','social_and_environmental_concerns','project_contract_number','project_country', 'project_background_other');
                $result_array = array('project_title' => '','borrower' => '','application_date' => '','country_office' => '','main_sector' => '','sub_sector' => '','required_loan_amount_currency' => '','project_description' => '','project_concept_purpose' => '','status_of_project_implementation' => '','project_cost_financing_plan' => '','summary_of_available_information' => '','social_and_environmental_concerns' => '','project_contract_number' => '', 'project_country' => '', 'project_background_other' => '');
                $project_details = $this->Crm_model->getProject($data['project_id']);
                $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
                $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
                //echo "<pre>"; print_r($borrower); exit;
                $result_array['application_date'] = $project_details[0]['created_date_time'];
                $result_array['main_sector'] = $project_details[0]['sector'];
                $result_array['sub_sector'] = $project_details[0]['sub_sector'];
                $result_array['country_office'] = $created_by[0]['branch_name'];
                $result_array['borrower'] = $borrower[0]['company_name'];
                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
                //echo "<pre>"; print_r($key_details); exit;
                for($r=0;$r<count($required_keys);$r++)
                {
                    for($s=0;$s<count($key_details);$s++){
                        $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                    }
                }

                $facility_details = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));

                for($s=0;$s<count($facility_details);$s++)
                {
                    $result_array['required_loan_amount_currency'][] = array(
                        'id_project_facility' => $facility_details[$s]['id_project_facility'],
                        'project_facility_name' => $facility_details[$s]['project_facility_name'],
                        'facility_amount' => $facility_details[$s]['facility_amount'],
                        'currency' => $facility_details[$s]['currency_code']
                    );
                }

            }
            else if($project_stage_details[0]['stage_key']=='facility_approval_memo')
            {
                $data['project_id'] = $project_stage_details[0]['project_id'];
                $data['stage_id'] = $project_stage_details[0]['stage_id'];
                $project_details = $this->Crm_model->getProject($data['project_id']);

                $created_by = $this->Company_model->getCompanyUsers(array('user_id' => $project_details[0]['created_by'],'company_id' => $project_details[0]['company_id']));
                $country_office = $created_by[0]['branch_name'];
                $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $data['project_id']));
                $loan_amount = array_map(function($i){ return $i['facility_usd_amount']; },$facilities);
                $loan_amount = array_sum($loan_amount);
                $contract_id = 0;
                $project_date = $project_details[0]['created_date_time'];
                $project_name = $project_details[0]['project_title'];
                $background_of_customer = $project_details[0]['background_of_customer'];
                if(!empty($facilities)){ $contract_id = $facilities[0]['contract_id']; }
                $borrower = $this->Crm_model->getProjectCompany(array('crm_project_id' => $data['project_id']));
                $company_name = $share_holder_info = ''; $info = array();
                $company_keys = array('company_ownership_details','company_street_name','company_block_number','company_city','company_region','company_country','company_business_history','company_corporate_structure_and_organization','company_justification_for_request_consideration','company_external_market_data','company_financial_statement_comments');
                $info = array('company_ownership_details' => '','company_street_name' => '','company_block_number' => '','company_city' => '','company_region' =>'','company_country' => '','company_business_history' => '','company_corporate_structure_and_organization' => '','company_justification_for_request_consideration' => '','company_external_market_data' => '','company_financial_statement_comments'=> '');
                if(!empty($borrower)){
                    $company_name = $borrower[0]['company_name'];
                    $key_data = $this->Project_model->getCrmCompanyDataByKeys(array('crm_company_id' => $borrower[0]['id_crm_company'],'keys' => $company_keys));
                    if(!empty($key_data)){
                        for($sr=0;$sr<count($key_data);$sr++) {
                            $info[$key_data[$sr]['field_name']] = $key_data[$sr]['form_field_value'];
                        }
                    }
                }

                $share_holder_info = $info['company_ownership_details'];
                unset($info['company_ownership_details']);
                $company_address = $info;
                //$required_keys = array('project_contract_number','project_basic_information_other','project_location','pre_conditions_for_disbursement','procurement_of_goods','insurance','other_conditions','security','representations_and_warranties','information_undertakings','financial_covenants','general_undertakings','events_of_default','special_audit','legal_documentation','costs_and_expenses','applicable_law','dispute_resolution','other');
                $required_keys = array('approval_request','critical_credit_issues','risk_and_relationship_strategy','transaction_dynamics','ways_out_analysis','banking_relationships','risk_analysis','country_overview_industry_risk');
                $result_array = array('project_title' => $project_name,'background_of_customer' => $background_of_customer ,'loan_amount' => $loan_amount,'application_date' => $project_date,'country_office' => $country_office,'project_contract_number' => '','company_name' => $company_name,'company_address'=>$company_address,'share_holder_info' => $share_holder_info,'project_location' =>'','project_basic_information_other' => '', 'interest_rate' => '','facilities' => '','pre_conditions_for_disbursement' => '','procurement_of_goods' => '','insurance' => '','other_conditions' => '','security' => '','representations_and_warranties' => '','information_undertakings' => '','financial_covenants' => '','general_undertakings' => '','events_of_default' => '','special_audit' => '','legal_documentation' => '','costs_and_expenses' => '','applicable_law' => '','dispute_resolution' => '','other' => '');
                $key_details = $this->Project_model->getProjectFormFieldValuesByKeys(array('project_id' => $data['project_id'],'keys' => $required_keys));
                //echo "<pre>"; print_r($key_details); exit;
                for($s=0;$s<count($key_details);$s++){
                    $result_array[$key_details[$s]['field_key']] = $required_keys[$key_details[$s]['field_key']] = $key_details[$s]['form_field_value'];
                }

                //contact details

                $client_details = $this->Crm_model->getProjectContact(array('crm_project_id' => $data['project_id']));
                $client_required_keys = array('client_information','client_personality','client_education','client_eployeement');
                for($cd=0;$cd<count($client_details);$cd++)
                {
                    $client_data=$client_details[$cd];
                    $client_key_details = $this->Crm_model->getCrmProjectContactDataByKeys(array('crm_contact_id' => $client_data['id_crm_contact'],'keys' => $client_required_keys));
                    $client_details[$cd]['Additional_data'] = $client_key_details;
                }
                $result_array['client_details'] = $client_details;
                $result_array['project_conditions_checklist'] = $this->Project_model->getProjectChecklistByType(array('project_id' => $data['project_id'],'child_key' => 'conditions_precedent_to_drawdown'));
                $result_array['project_others_checklist'] = $this->Project_model->getProjectChecklistByType(array('project_id' => $data['project_id'],'child_key' => 'other_provisions'));

                $appraisal_fee = '';
                for($s=0;$s<count($facilities);$s++)
                {
                    $interest_rate = '';
                    $loan_type = '---';
                    $type_of_loan = $this->Master_model->getMasterChild(array('child_id' => $facilities[$s]['loan_payment_type']));
                    $pricing_array = $terms_condition_array = $facility_checklist_array = '';
                    $pricing = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'pricing'));
                    $terms_conditions = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $facilities[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
                    $facility_checklist = $this->Crm_model->getProjectChecklistByFacilityId(array('project_facility_id' => $facilities[$s]['id_project_facility']));

                    for($r=0;$r<count($facility_checklist);$r++)
                    {
                        $facility_checklist_array[$r] = $facility_checklist[$r]['assessment_question'];
                    }
                    for($r=0;$r<count($pricing);$r++){
                        $pricing_array[$r] = array(
                            'name' => $pricing[$r]['field_name'],
                            'payment_type' => $pricing[$r]['payment_type'],
                            'label' => '',
                            'field_key' => $pricing[$r]['field_key'],
                            'facility_field_value' => $pricing[$r]['facility_field_value'],
                            'facility_field_description' => $pricing[$r]['facility_field_description']
                        );
                        if($pricing[$r]['payment_type']=='interest_as'){
                            $time_period = $this->Company_model->getTimePeriod(array('id_time_period' => $pricing[$r]['time_period_id']));
                            $pricing_array[$r]['label'] = $time_period[0]['period_name'];
                        }
                        else if($pricing[$r]['payment_type']=='fee_as'){
                            $fee_as = $this->Company_model->getFacilityAmountType(array('id_facility_amount_type' => $pricing[$r]['facility_amount_type']));
                            $pricing_array[$r]['label'] = $fee_as[0]['facility_amount_type'];
                        }
                        else if($pricing[$r]['payment_type']=='fee_amount'){
                            $currency = $this->Company_model->getCompanyCurrencyDetails(array('currency_id' => $pricing[$r]['currency_id']));
                            $pricing_array[$r]['label'] = $currency[0]['currency_code'];
                        }

                        if($pricing[$r]['field_key']=='interest_rate'){
                            $interest_rate =  $pricing_array[$r];
                        }
                        if($pricing[$r]['field_key'] == 'apraisal_fee_commission')
                        {
                            $appraisal_fee = $pricing[$r]['facility_field_value'].'% '.$pricing_array[$r]['label'];
                        }
                    }
                    //echo $appraisal_fee;exit;
                    $pricing_array = array_values($pricing_array);
                    //echo "<pre>"; print_r($terms_conditions); exit;

                    for($r=0;$r<count($terms_conditions);$r++){
                        $terms_condition_array[] = array(
                            'name' => $terms_conditions[$r]['field_name'],
                            'key' => $terms_conditions[$r]['field_key'],
                            'description' => $terms_conditions[$r]['facility_field_description']
                        );
                    }

                    if(!empty($type_of_loan)){ $loan_type = $type_of_loan[0]['child_name']; }

                    $result_array['facilities'][] = array(
                        'facility_name' => $facilities[$s]['project_facility_name'],
                        'facility_description' => $facilities[$s]['facility_description'],
                        'currency' => $facilities[$s]['currency_code'],
                        'amount' => $facilities[$s]['facility_amount'],
                        'usd_amount' => $facilities[$s]['facility_usd_amount'],
                        'type_of_loan' => $loan_type,
                        'facility_purpose' => $facilities[$s]['facility_purpose'],
                        'facility_tenor' => $facilities[$s]['facility_tenor'],
                        'source_of_payment' => $facilities[$s]['source_of_payment'],
                        'facility_minimum_amount' => $facilities[$s]['facility_amount'],
                        'other_facility_modalities' => $facilities[$s]['other_facility_modalities'],
                        'security' => $facilities[$s]['facility_security'],
                        'maturity_loan_repayment' => $facilities[$s]['facility_tenor'],
                        'interest_payment' => $interest_rate,
                        'pricing' => $pricing_array,
                        'terms_conditions' => $terms_condition_array,
                        'facility_checklist' =>$facility_checklist_array
                    );
                }
            }
        }
        $result_array['team'] = $team_array;
        if(isset($project_stage_details[0]))
        {
            $project_stage_details[0]['fields'] = $result_array;
            $project_stage_details = $project_stage_details[0];
        }
        else{
            $project_stage_details = array();
        }
        //echo "<pre>"; print_r($project_stage_details); exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$project_stage_details);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectStageInfoDetail_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['offset'] = 0;
        $data['limit'] = 1;
        $result = $this->Project_model->getProjectStageInfoDetail($data);
        if(!empty($result)){ $result = $result[0]; }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stageComments_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getFormComments(array('reference_type' => 'project-stage','reference_id' => $data['project_stage_info_id']));

        if(!empty($result)){ $result = $result[0]; }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectStageVersion_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $data['project_stage_info_id']));
        $stage_forms = $this->Project_model->getStageForms(array('crm_project_id' => $project_stage_info[0]['project_id'],'project_stage_id' => $project_stage_info[0]['stage_id'],'project_stage_info_id' => $data['project_stage_info_id']));
        $new_version = ($project_stage_info[0]['version_number']+1);
        $this->Project_model->updateProjectStageInfo(array('id_project_stage_info' => $project_stage_info[0]['id_project_stage_info'],'version_number' => $new_version));
        $this->Project_model->addProjectStageVersion(['project_stage_info_id' => $project_stage_info[0]['id_project_stage_info'],'version_number' => $new_version, 'created_by' => $data['created_by'],'created_date_time' => currentDate()]);

        $form_data = array(
            'log_auth_key' => LOG_AUTH_KEY,
            'project_id' => $project_stage_info[0]['project_id'],
            'stage_id' => $project_stage_info[0]['stage_id'],
            'project_stage_info_id' => $project_stage_info[0]['id_project_stage_info'],
            'version_number' => $project_stage_info[0]['version_number'],
            'created_by' => $data['created_by'],
            'created_date_time' => currentDate(),
            'forms' => ''
        );

        for($r=0;$r<count($stage_forms);$r++)
        {
            $form_data['forms'][] = array(
                'project_form_id' => $stage_forms[$r]['project_form_id'],
                'form_name' => $stage_forms[$r]['form_name'],
                'project_stage_section_form_id' => $stage_forms[$r]['id_project_stage_section_form'],
                'field_data' => $this->Project_model->getProjectFormFieldData(array(
                                    'crm_project_id' => $project_stage_info[0]['project_id'],
                                    'project_stage_info_id' => $project_stage_info[0]['id_project_stage_info'],
                                    'project_stage_section_form_id' => $stage_forms[$r]['id_project_stage_section_form']
                                ))
            );
        }

        $mongo_data = json_encode($form_data);
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_URL.'addProjectStageVersionLog');
        curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_PHP_URL.'addProjectStageVersionLog.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $mongo_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($mongo_data))
        );
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        $projectStageVersions = $this->Project_model->getProjectStageVersions(array('project_stage_info_id' => $project_stage_info[0]['id_project_stage_info']));;

        $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
        $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has created new revision for '.$stage_forms[0]['stage_name'].' stage </p>';
        $activity = array(
            'activity_name' => 'Revision',
            'activity_type' => 'revision',
            'activity_template' =>$meetingTemplate,
            'activity_reference_id' => $project_stage_info[0]['id_project_stage_info'],
            'module_type' => 'project',
            'module_id' => $project_stage_info[0]['project_id'],
            'created_by' => $data['created_by'],
            'created_date_time' => currentDate()
        );
        $this->Activity_model->addActivity($activity);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('version_create_success'), 'data'=> ['project_stage_versions' => $projectStageVersions]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectProcess_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_stage = $this->Project_model->getProjectStageWorkflowPhase();
        $project_stage_workflow = $this->Project_model->getProjectStageWorkflowProcess(array('project_id' => $data['project_id']));

        $stage_info = $project_stage_info = array();
        for($s=0;$s<count($project_stage);$s++)
        {
            $stage_info[$project_stage[$s]['id_project_stage']]['id_project_stage'] = $project_stage[$s]['id_project_stage'];
            $stage_info[$project_stage[$s]['id_project_stage']]['stage_name'] = $project_stage[$s]['stage_name'];
            $stage_info[$project_stage[$s]['id_project_stage']]['stage_key'] = $project_stage[$s]['stage_key'];
            $stage_info[$project_stage[$s]['id_project_stage']]['stage_class'] = $project_stage[$s]['stage_class'];
            $stage_info[$project_stage[$s]['id_project_stage']]['phases'] = array();
        }
        $stage_info = array_values($stage_info);
        for($s=0;$s<count($project_stage_workflow);$s++)
        {
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['id_project_stage'] = $project_stage_workflow[$s]['id_project_stage'];
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['stage_name'] = $project_stage_workflow[$s]['stage_name'];
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['stage_key'] = $project_stage_workflow[$s]['stage_key'];
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['start_date_time'] = $project_stage_workflow[$s]['start_date_time'];
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['end_date_time'] = $project_stage_workflow[$s]['end_date_time'];
            $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['phases'][$project_stage_workflow[$s]['id_project_stage_workflow_phase']] = array(
                'id_workflow_phase' => $project_stage_workflow[$s]['workflow_phase_id'],
                'workflow_phase_name' => $project_stage_workflow[$s]['workflow_phase_name'],
                'days' => $project_stage_workflow[$s]['days'],
                'status' => $project_stage_workflow[$s]['status'],
                'action_name' => $project_stage_workflow[$s]['workflow_action_key']
            );
            if(!isset($project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['phase_count'])){
                $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['phase_count'] = 0;
            }
            if($project_stage_workflow[$s]['status']=='completed'){
                $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['phase_count'] = $project_stage_info[$project_stage_workflow[$s]['id_project_stage']]['phase_count']+1;
            }
        }

        for($s=0;$s<count($stage_info);$s++){
            if(isset($project_stage_info[$stage_info[$s]['id_project_stage']])) {
                $stage_info[$s]['phases'] = array_values($project_stage_info[$stage_info[$s]['id_project_stage']]['phases']);
                if (isset($project_stage_info[$stage_info[$s]['id_project_stage']]['phases'])) {
                    $stage_info[$s]['start_date'] = $project_stage_info[$stage_info[$s]['id_project_stage']]['start_date_time'];
                    $stage_info[$s]['end_date'] = $project_stage_info[$stage_info[$s]['id_project_stage']]['end_date_time'];
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $stage_info);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectLegalCheckList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Master_model->getMasterChild(array('child_key' => $data['form_key']));
        $resultArray = array();
        if(!empty($result)) {
            $resultArray = $this->Company_model->getAssessmentQuestionCategory(array('operational_checklist_type' => $result[0]['id_child']));
            for ($r = 0; $r < count($resultArray); $r++) {
                $resultArray[$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $resultArray[$r]['id_assessment_question_category'], 'question_status' => 1));
                for ($sr = 0; $sr < count($resultArray[$r]['question']); $sr++) {
                    if (isset($resultArray[$r]['question'][$sr]['question_option']) && $resultArray[$r]['question'][$sr]['question_option'] != '') {
                        $resultArray[$r]['question'][$sr]['question_option'] = json_decode($resultArray[$r]['question'][$sr]['question_option']);
                    }
                    $resultArray[$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $resultArray[$r]['question'][$sr]['id_assessment_question']));

                    if (isset($resultArray[$r]['question'][$sr]['answer']->assessment_answer) && $resultArray[$r]['question'][$sr]['answer']->assessment_answer != '') {
                        if ($resultArray[$r]['question'][$sr]['question_type'] == 'checkbox')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = json_decode($resultArray[$r]['question'][$sr]['answer']->assessment_answer);
                        else if ($resultArray[$r]['question'][$sr]['question_type'] == 'file')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($resultArray[$r]['question'][$sr]['answer']->assessment_answer);
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $resultArray);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function supervisionAssessment_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //$project = $this->Project_model->getProject(array('crm_project_id' =>$data['project_id']));
        //$project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $data['project_stage_info_id']));
        $result = $this->Project_model->getSupervisionAssessment($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_supervision_assessment'],'module_type' => 'supervision'));
        }
        /*if($project[0]['project_stage_info_id']==$data['project_stage_info_id'])
        {
            $result = $this->Project_model->getSupervisionAssessment($data);
        }
        else
        {
            //$url = MONGO_SERVICE_URL.'findProjectStageVersionLog/?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $url = MONGO_SERVICE_PHP_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);

            if(isset($output->data[0]->forms[0]->field_data))
                $output = $output->data[0]->forms[0]->field_data;
            else
                $output = array();
        }*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function supervisionAssessment_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();

        $supervision_data['introduction'] = isset($data['introduction'])?$data['introduction']:'';
        $supervision_data['missing_findings'] = isset($data['missing_findings'])?$data['missing_findings']:'';
        $supervision_data['proposed_action_plan'] = isset($data['proposed_action_plan'])?$data['proposed_action_plan']:'';
        $supervision_data['attachment_comments'] = isset($data['attachment_comments'])?$data['attachment_comments']:'';
        $supervision_data['next_supervision_date'] = isset($data['next_supervision_date'])?$data['next_supervision_date']:'';


        if(isset($data['id_supervision_assessment'])){
            $supervision_data['id_supervision_assessment'] = $data['id_supervision_assessment'];
            $supervision_assessment_id = $data['id_supervision_assessment'];
            $this->Project_model->updateSupervisionAssessment($supervision_data);
        }
        else{
            $supervision_data['project_id'] = $data['project_id'];
            $supervision_data['project_stage_info_id'] = $data['project_stage_info_id'];
            $supervision_data['project_stage_section_form_id'] = $data['project_stage_section_form_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();

            $supervision_assessment_id = $this->Project_model->addSupervisionAssessment($supervision_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $document_type = $this->Crm_model->getDocumentType(array('module_id' => 3));
        $data['document_type'] = 0;
        if(!empty($document_type)){ $data['document_type'] = $document_type[0]['id_crm_document_type']; }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $supervision_assessment_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'supervision','form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $supervision_assessment_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'supervision','form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function supervisionAssessment_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_supervision_assessment', array('required'=> $this->lang->line('supervision_assessment_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteSupervisionAssessment($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function supervisionProjectCommittee_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        //$this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //$project = $this->Project_model->getProject(array('crm_project_id' =>$data['project_id']));
        //$project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $data['project_stage_info_id']));
        $result = $this->Project_model->getSupervisionProjectCommittee($data);
        /*if($project[0]['project_stage_info_id']==$data['project_stage_info_id'])
        {
            $result = $this->Project_model->getSupervisionAssessment($data);
        }
        else
        {
            //$url = MONGO_SERVICE_URL.'findProjectStageVersionLog/?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $url = MONGO_SERVICE_PHP_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);

            if(isset($output->data[0]->forms[0]->field_data))
                $output = $output->data[0]->forms[0]->field_data;
            else
                $output = array();
        }*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function supervisionProjectCommittee_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();
        $supervision_data['minutes'] = isset($data['minutes'])?$data['minutes']:'';
        $supervision_data['taken_actions'] = isset($data['taken_actions'])?$data['taken_actions']:'';
        $supervision_data['date'] = isset($data['date'])?$data['date']:'';

        if(isset($data['id_supervision_project_committee'])){
            $supervision_data['id_supervision_project_committee'] = $data['id_supervision_project_committee'];
            $this->Project_model->updateSupervisionProjectCommittee($supervision_data);
        }
        else{
            $supervision_data['project_id'] = $data['project_id'];
            $supervision_data['project_stage_info_id'] = $data['project_stage_info_id'];
            $supervision_data['project_stage_section_form_id'] = $data['project_stage_section_form_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();
            $this->Project_model->addSupervisionProjectCommittee($supervision_data);
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function supervisionProjectCommittee_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_supervision_project_committee', array('required'=> $this->lang->line('supervision_project_committee_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteSupervisionProjectCommittee($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function disbursementRequest_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //$project = $this->Project_model->getProject(array('crm_project_id' =>$data['project_id']));
        //$project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $data['project_stage_info_id']));
        $result = $this->Project_model->getDisbursementRequest($data);
        /*if($project[0]['project_stage_info_id']==$data['project_stage_info_id'])
        {
            $result = $this->Project_model->getSupervisionAssessment($data);
        }
        else
        {
            //$url = MONGO_SERVICE_URL.'findProjectStageVersionLog/?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $url = MONGO_SERVICE_PHP_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);

            if(isset($output->data[0]->forms[0]->field_data))
                $output = $output->data[0]->forms[0]->field_data;
            else
                $output = array();
        }*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function disbursementRequest_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('project_facility_id', array('required'=> $this->lang->line('project_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();
        $supervision_data['date_of_receipt_of_request'] = isset($data['date_of_receipt_of_request'])?$data['date_of_receipt_of_request']:'';
        /*$supervision_data['project_no'] = isset($data['project_no'])?$data['project_no']:'';
        $supervision_data['source_of_funds'] = isset($data['source_of_funds'])?$data['source_of_funds']:'';*/
        $supervision_data['request_number'] = isset($data['request_number'])?$data['request_number']:'';
        $supervision_data['disbursement_status'] = isset($data['disbursement_status'])?$data['disbursement_status']:'';
        $supervision_data['project_facility_id'] = $data['project_facility_id'];
        $supervision_data['amount'] = isset($data['amount'])?$data['amount']:'';
        $supervision_data['facility_usd_amount'] = isset($data['facility_usd_amount'])?$data['facility_usd_amount']:'';

        if(isset($data['id_disbursement_request'])){
            $supervision_data['id_disbursement_request'] = $data['id_disbursement_request'];
            $this->Project_model->updateDisbursementRequest($supervision_data);
        }
        else{
            $supervision_data['project_id'] = $data['project_id'];
            $supervision_data['project_stage_info_id'] = $data['project_stage_info_id'];
            $supervision_data['project_stage_section_form_id'] = $data['project_stage_section_form_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();
            $this->Project_model->addDisbursementRequest($supervision_data);
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function disbursementRequest_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_disbursement_request', array('required'=> $this->lang->line('disbursement_request_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteDisbursementRequest($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function esGrade_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getEsGrade($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function esGrade_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('grade', array('required'=> $this->lang->line('grade_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $es_grade_data = array(
            'project_id' => $data['project_id'],
            'expected_no_of_jobs' => isset($data['expected_no_of_jobs'])?$data['expected_no_of_jobs']:'',
            'expected_no_of_women_jobs' => isset($data['expected_no_of_women_jobs'])?$data['expected_no_of_women_jobs']:'',
            'income_from_exports' => isset($data['income_from_exports'])?$data['income_from_exports']:'',
            'expected_tax_to_be_paid' => isset($data['expected_tax_to_be_paid'])?$data['expected_tax_to_be_paid']:'',
            'comments' => isset($data['comments'])?$data['comments']:'',
            'grade' => $data['grade']
        );

        /*if(isset($data['id_project_es_grade']))
        {
            $es_grade_data['id_project_es_grade'] = $data['id_project_es_grade'];
            $es_grade_data['updated_by'] = $data['created_by'];
            $es_grade_data['updated_date_time'] = currentDate();
            $result = $this->Project_model->updateEsGrade($es_grade_data);
        }
        else
        {*/
            $es_grade_data['created_by'] = $data['created_by'];
            $es_grade_data['created_date_time'] = currentDate();
            $result = $this->Project_model->addEsGrade($es_grade_data);
        /*}*/

        //getting last form last update
        if(isset($data['form_id']))
        {
            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'], 'project_stage_section_form_id' => $data['form_id']));
            if (empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));

            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['form_id'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('es_grade_add'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function esGrade_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_project_es_grade', array('required'=> $this->lang->line('project_es_grade_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->updateEsGrade(array(
            'id_project_es_grade' => $data['id_project_es_grade'],
            'es_grade_status' => 0
        ));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function team_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteDisbursementRequest($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stageVersion_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('stage_id', array('required'=> $this->lang->line('stage_id_req')));
        $this->form_validator->add_rules('version_number', array('required'=> $this->lang->line('version_num_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Project_model->getVersionNumber($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function initialSupervisionReport_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getInitialSupervisionReport($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_supervision_report'],'module_type' => 'supervision_report'));
            for($r=0;$r<count($result[$s]['document']);$r++){
                $result[$s]['document'][$r]['full_path'] = getImageUrl($result[$s]['document'][$r]['document_source'],'');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function initialSupervisionReport_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($_FILES); exit;
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();

        $supervision_data['visit_date'] = isset($data['visit_date'])?date('Y-m-d',strtotime($data['visit_date'])):'';
        $supervision_data['use_of_funds'] = isset($data['use_of_funds'])?$data['use_of_funds']:'';
        $supervision_data['borrower_performance_comments'] = isset($data['borrower_performance_comments'])?$data['borrower_performance_comments']:'';
        $supervision_data['observations'] = isset($data['observations'])?$data['observations']:'';
        $supervision_data['grading'] = isset($data['grading'])?$data['grading']:'';
        $supervision_data['status'] = isset($data['status'])?$data['status']:'';


        if(isset($data['id_supervision_report'])){
            $supervision_data['id_supervision_report'] = $data['id_supervision_report'];
            $supervision_report_id = $data['id_supervision_report'];
            $this->Project_model->updateInitialSupervisionReport($supervision_data);
        }
        else{
            $supervision_data['crm_project_id'] = $data['project_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();

            $supervision_report_id = $this->Project_model->addInitialSupervisionReport($supervision_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $document_type = $this->Crm_model->getDocumentType(array('module_id' => 3));
        $data['document_type'] = 0;
        if(!empty($document_type)){ $data['document_type'] = $document_type[0]['id_crm_document_type']; }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $supervision_report_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'supervision_report','form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $supervision_report_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'supervision_report','form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function initialSupervisionReport_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_supervision_report', array('required'=> $this->lang->line('supervision_report_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteInitialSupervisionReport($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function annualReview_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getAnnualReview($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_annual_review'],'module_type' => 'annual_review'));
            for($r=0;$r<count($result[$s]['document']);$r++){
                $result[$s]['document'][$r]['full_path'] = getImageUrl($result[$s]['document'][$r]['document_source'],'');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function annualReview_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();

        $supervision_data['visit_date'] = isset($data['visit_date'])?date('Y-m-d',strtotime($data['visit_date'])):'';
        $supervision_data['company_notes'] = isset($data['company_notes'])?$data['company_notes']:'';
        $supervision_data['ownership_notes'] = isset($data['ownership_notes'])?$data['ownership_notes']:'';
        $supervision_data['exposure_notes'] = isset($data['exposure_notes'])?$data['exposure_notes']:'';
        $supervision_data['assessment_notes'] = isset($data['assessment_notes'])?$data['assessment_notes']:'';
        $supervision_data['assessment_summary'] = isset($data['assessment_summary'])?$data['assessment_summary']:'';
        $supervision_data['status'] = isset($data['status'])?$data['status']:'';
        $supervision_data['grading'] = isset($data['grading'])?$data['grading']:'';


        if(isset($data['id_annual_review'])){
            $supervision_data['id_annual_review'] = $data['id_annual_review'];
            $annual_review_id = $data['id_annual_review'];
            $this->Project_model->updateAnnualReview($supervision_data);
        }
        else{
            $supervision_data['crm_project_id'] = $data['project_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();

            $annual_review_id = $this->Project_model->addAnnualReview($supervision_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $document_type = $this->Crm_model->getDocumentType(array('module_id' => 3));
        $data['document_type'] = 0;
        if(!empty($document_type)){ $data['document_type'] = $document_type[0]['id_crm_document_type']; }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $annual_review_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'annual_review','form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $annual_review_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'annual_review','form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function annualReview_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_annual_review', array('required'=> $this->lang->line('annual_review_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteAnnualReview($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ongoingMonitoring_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getOngoingMonitoring($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_ongoing_monitoring'],'module_type' => 'ongoing_monitoring'));
            for($r=0;$r<count($result[$s]['document']);$r++){
                $result[$s]['document'][$r]['full_path'] = getImageUrl($result[$s]['document'][$r]['document_source'],'');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ongoingMonitoring_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();

        $supervision_data['visit_date'] = isset($data['visit_date'])?date('Y-m-d',strtotime($data['visit_date'])):'';
        $supervision_data['comments'] = isset($data['comments'])?$data['comments']:'';
        $supervision_data['status'] = isset($data['status'])?$data['status']:'';
        $supervision_data['grading'] = isset($data['grading'])?$data['grading']:'';

        if(isset($data['id_ongoing_monitoring'])){
            $supervision_data['id_ongoing_monitoring'] = $data['id_ongoing_monitoring'];
            $ongoing_monitoring_id = $data['id_ongoing_monitoring'];
            $this->Project_model->updateOngoingMonitoring($supervision_data);
        }
        else{
            $supervision_data['crm_project_id'] = $data['project_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();

            $ongoing_monitoring_id = $this->Project_model->addOngoingMonitoring($supervision_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $document_type = $this->Crm_model->getDocumentType(array('module_id' => 3));
        $data['document_type'] = 0;
        if(!empty($document_type)){ $data['document_type'] = $document_type[0]['id_crm_document_type']; }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $ongoing_monitoring_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'ongoing_monitoring','form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $ongoing_monitoring_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'ongoing_monitoring','form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ongoingMonitoring_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_ongoing_monitoring', array('required'=> $this->lang->line('ongoing_monitoring_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteOngoingMonitoring($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function otherMonitoring_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Project_model->getOtherMonitoring($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_other_monitoring'],'module_type' => 'other_monitoring'));
            for($r=0;$r<count($result[$s]['document']);$r++){
                $result[$s]['document'][$r]['full_path'] = getImageUrl($result[$s]['document'][$r]['document_source'],'');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function otherMonitoring_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_section_form_id', array('required'=> $this->lang->line('project_stage_section_form_id')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $supervision_data = array();

        $supervision_data['updated_by'] = $data['created_by'];
        $supervision_data['updated_date_time'] = currentDate();

        $supervision_data['visit_date'] = isset($data['visit_date'])?date('Y-m-d',strtotime($data['visit_date'])):'';
        $supervision_data['comments'] = isset($data['comments'])?$data['comments']:'';
        $supervision_data['status'] = isset($data['status'])?$data['status']:'';
        $supervision_data['grading'] = isset($data['grading'])?$data['grading']:'';

        if(isset($data['id_other_monitoring'])){
            $supervision_data['id_other_monitoring'] = $data['id_other_monitoring'];
            $other_monitoring_id = $data['id_other_monitoring'];
            $this->Project_model->updateOtherMonitoring($supervision_data);
        }
        else{
            $supervision_data['crm_project_id'] = $data['project_id'];
            $supervision_data['created_by'] = $data['created_by'];
            $supervision_data['created_date_time'] = currentDate();

            $other_monitoring_id = $this->Project_model->addOtherMonitoring($supervision_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $document_type = $this->Crm_model->getDocumentType(array('module_id' => 3));
        $data['document_type'] = 0;
        if(!empty($document_type)){ $data['document_type'] = $document_type[0]['id_crm_document_type']; }
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $other_monitoring_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'other_monitoring','form_key' => '');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => $this->lang->line('upload_error'), 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $other_monitoring_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'other_monitoring','form_key' => '');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['project_id'],'crm_module_type' => 'project','reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['project_stage_section_form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

        $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id']));
        if(empty($last_update))
            $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['project_id'],'project_stage_section_form_id' => $data['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
        else
            $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['project_stage_section_form_id'],'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function otherMonitoring_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_other_monitoring', array('required'=> $this->lang->line('other_monitoring_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Project_model->deleteOtherMonitoring($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function riskAssessment_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Project_model->getRiskAssessment($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskAssessment_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('risk', array('required'=> $this->lang->line('risk_title_req')));
        $this->form_validator->add_rules('risk_type', array('required'=> $this->lang->line('risk_type_req')));
        $this->form_validator->add_rules('risk_grade', array('required'=> $this->lang->line('risk_grade_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $insert_data = array(
            'crm_project_id' => $data['project_id'],
            'risk' => $data['risk'],
            'risk_type' => $data['risk_type'],
            'risk_sub_type1' => isset($data['risk_sub_type1'])?$data['risk_sub_type1']:'',
           // 'risk_sub_type2' => isset($data['risk_sub_type2'])?$data['risk_sub_type2']:'',
            'description' => isset($data['description'])?$data['description']:'',
            'mitigation' => isset($data['mitigation'])?$data['mitigation']:'',
            'net_risk' => isset($data['net_risk'])?$data['net_risk']:'',
            'risk_grade' => isset($data['risk_grade'])?$data['risk_grade']:''
        );

        if(isset($data['id_risk_assessment'])){
            $insert_data['id_risk_assessment'] = $data['id_risk_assessment'];
            $insert_data['updated_by'] = $data['created_by'];
            $insert_data['updated_date_time'] = currentDate();
            $this->Project_model->updateRiskAssessment($insert_data);
            $msg = $this->lang->line('risk_update');
        }
        else{
            $insert_data['created_by'] = $data['created_by'];
            $insert_data['created_date_time'] = currentDate();
            $this->Project_model->addRiskAssessment($insert_data);
            $msg = $this->lang->line('risk_add');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskAssessment_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_risk_assessment', array('required'=> $this->lang->line('risk_assessment_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Project_model->deleteRiskAssessment($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('succ_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function saveProjectFinancials_post(){
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('currency_id',array('required'=> $this->lang->line('currency_id_req')));
        //$this->form_validator->add_rules('child_data',array('required'=> $this->lang->line('child_data_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_crm_project_financials_master']) && $data['id_crm_project_financials_master']!=NULL && $data['id_crm_project_financials_master']>0) {
            $company_supplier_info = array(
                'crm_project_id' => $data['crm_project_id'],
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'is_product_quotes_received' => isset($data['is_product_quotes_received'])?$data['is_product_quotes_received']:NULL,
                'can_loan_disbursed_to_supplier' => isset($data['can_loan_disbursed_to_supplier'])?$data['can_loan_disbursed_to_supplier']:NULL,
                'can_loan_disbursed_to_supplier_comments' => isset($data['can_loan_disbursed_to_supplier_comments'])?$data['can_loan_disbursed_to_supplier_comments']:NULL,
                'easy_borrower_pay' => isset($data['easy_borrower_pay'])?$data['easy_borrower_pay']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'updated_date_time' => currentDate(),
                'updated_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $this->Crm_model->updateRecordWhere('crm_project_financials_master',$company_supplier_info,array('id_crm_project_financials_master' => $data['id_crm_project_financials_master']));
            $insert_id=$data['id_crm_project_financials_master'];
        }
        else{
            $company_supplier_info = array(
                'crm_project_id' => $data['crm_project_id'],
                'currency_id' => isset($data['currency_id'])?$data['currency_id']:NULL,
                'is_product_quotes_received' => isset($data['is_product_quotes_received'])?$data['is_product_quotes_received']:NULL,
                'can_loan_disbursed_to_supplier' => isset($data['can_loan_disbursed_to_supplier'])?$data['can_loan_disbursed_to_supplier']:NULL,
                'can_loan_disbursed_to_supplier_comments' => isset($data['can_loan_disbursed_to_supplier_comments'])?$data['can_loan_disbursed_to_supplier_comments']:NULL,
                'easy_borrower_pay' => isset($data['easy_borrower_pay'])?$data['easy_borrower_pay']:NULL,
                'comments' => isset($data['comments'])?$data['comments']:NULL,
                'created_date_time' => currentDate(),
                'created_by' => isset($data['user_id'])?$data['user_id']:NULL
            );
            $insert_id=$this->Crm_model->addNewRecord('crm_project_financials_master',$company_supplier_info);
        }
        //$child_data = json_decode($data['child_data'],true);
        $child_data = isset($data['child_data'])?$data['child_data']:array();
        $child_data_inner = array();
        if(isset($child_data)){

            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_source_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                $result['master'][0]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
                if(isset($child_data[$fund_type_master_text][$fund_type_text])){
                    foreach($child_data[$fund_type_master_text][$fund_type_text] as $k=>$v)
                    $child_data_inner[]=$v;
                }
            }
            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_use_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                if(isset($child_data[$fund_type_master_text][$fund_type_text])){
                    //$child_data_inner[]=$child_data[$fund_type_master_text][$fund_type_text];
                    foreach($child_data[$fund_type_master_text][$fund_type_text] as $k=>$v)
                        $child_data_inner[]=$v;
                }
            }
        }

        if(isset($child_data_inner)){
            foreach($child_data_inner as $k=>$v){
                $inner_data=array();
                $inner_data['crm_project_financials_master_id']=$insert_id;
                $inner_data['fund_type_id']=isset($v['fund_type_id'])?$v['fund_type_id']:NULL;
                $inner_data['requested_amount']=isset($v['requested_amount'])?$v['requested_amount']:NULL;
                $inner_data['proposed_amount']=isset($v['proposed_amount'])?$v['proposed_amount']:NULL;
                $inner_data['description']=isset($v['description'])?$v['description']:NULL;
                $inner_data['status']=isset($v['status'])?$v['status']:1;
                if(isset($v['id_crm_project_financials_child']) && $v['id_crm_project_financials_child']!=NULL && $v['id_crm_project_financials_child']>0){
                    $inner_data['updated_by'] =  isset($data['user_id'])?$data['user_id']:NULL;
                    $inner_data['updated_date_time'] = currentDate();
                    $this->Crm_model->updateRecordWhere('crm_project_financials_child',$inner_data,array('id_crm_project_financials_child' => $v['id_crm_project_financials_child']));
                }else{
                    $inner_data['created_date_time'] = currentDate();
                    $inner_data['created_by'] = isset($data['user_id'])?$data['user_id']:NULL;
                    if($inner_data['status']==1)
                        $this->Crm_model->addNewRecord('crm_project_financials_child',$inner_data);
                }
            }
        }
        if(isset($data['id_project_stage_section_form'])) {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form'));
            if (empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['id_project_stage_section_form'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));

            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'], 'project_stage_section_form_id' => $data['id_project_stage_section_form']));
            if (empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_project_id' => $data['crm_project_id'], 'project_stage_section_form_id' => $data['id_project_stage_section_form'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['user_id'], 'updated_date_time' => currentDate()));

            $form_info = $this->Project_model->getProjectStageSectionForm(array('project_stage_section_form_id' => $data['id_project_stage_section_form']));
            $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'], 'activity_template' => 'Information saved', 'module_type' => 'project', 'module_id' => $data['crm_project_id'], 'activity_type' => 'form', 'activity_reference_id' => $data['id_project_stage_section_form'], 'created_by' => $data['created_by'], 'created_date_time' => currentDate()));
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getProjectFinancials_get(){
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id',array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result['master'] = $this->Crm_model->checkRecord('crm_project_financials_master',array('crm_project_id'=>$data['crm_project_id']));
        foreach($result['master'] as $k => $v){
            $result['master'][$k]['currency_code']=NULL;
            if($v['currency_id']>0) {
                $master_child = $this->Crm_model->checkRecord('currency', array('id_currency' => $v['currency_id']));
                $result['master'][$k]['currency_code'] = isset($master_child[0]['currency_code']) ? $master_child[0]['currency_code'] : NULL;
            }
            $result['master'][$k]['child_data'] = $this->Crm_model->checkRecord('crm_project_financials_child',array('crm_project_financials_master_id'=>$v['id_crm_project_financials_master'],'status'=>1));
            foreach($result['master'][$k]['child_data'] as $kc=>$vc) {
                $result['master'][$k]['child_data'][$kc]['fund_type_text']=NULL;
                if($vc['fund_type_id']>0) {
                    $master_child = $this->Crm_model->checkRecord('master_child', array('id_child' => $vc['fund_type_id']));
                    $result['master'][$k]['child_data'][$kc]['fund_type_text'] = isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL;
                    $fund_type_text=str_replace(' ','_',strtolower(isset($master_child[0]['child_name']) ? $master_child[0]['child_name'] : NULL));
                    $fund_type_master=$this->Crm_model->checkRecord('master', array('id_master' => $master_child[0]['master_id']));
                    $fund_type_master_text=str_replace(' ','_',strtolower(isset($fund_type_master[0]['master_name']) ? $fund_type_master[0]['master_name'] : NULL));
                    $result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text][]=$result['master'][$k]['child_data'][$kc];
                }
            }

            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_source_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                if(!isset($result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]))
                    $result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
            }
            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_use_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                if(!isset($result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]))
                    $result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
            }

            if(!isset($result['master'][$k]['child_data']) || (isset($result['master'][$k]['child_data']) && count($result['master'][$k]['child_data'])==0)){
                $result['master'][$k]['child_data']=array();
                //$result['master'][$k]['child_data_final']=array();
                /*$result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_source_of_funds'));
                foreach($result1 as $kc1=>$vc1) {
                    $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                    $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                    $result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
                }
                $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_use_of_funds'));
                foreach($result1 as $kc1=>$vc1) {
                    $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                    $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                    $result['master'][$k]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
                }*/
            }
        }
        if(!isset($result['master'][0]['child_data'])){
            $result['master'][0]['child_data']=array();
            $result['master'][0]['child_data_final']=array();
            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_source_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                $result['master'][0]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
            }
            $result1 = $this->Master_model->getMaster(array('master_key'=>'project_financials_use_of_funds'));
            foreach($result1 as $kc1=>$vc1) {
                $fund_type_text=str_replace(' ','_',strtolower(isset($vc1['name']) ? $vc1['name'] : NULL));
                $fund_type_master_text=str_replace(' ','_',strtolower(isset($vc1['master_name']) ? $vc1['master_name'] : NULL));
                $result['master'][0]['child_data_final'][$fund_type_master_text][$fund_type_text]=array();
            }
        }
        $result['master'][0]['child_data']=($result['master'][0]['child_data_final']);
        unset($result['master'][0]['child_data_final']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function guarantee_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project = $this->Project_model->getProject(array('crm_project_id' =>$data['crm_project_id']));
        //getting project companies
        $project_company = $this->Project_model->getProjectCompany(array('crm_project_id' => $data['crm_project_id']));
        $project_company = array_map(function($key){ return $key['company_name']; }, $project_company);
        $project[0]['company_name'] = implode(',',$project_company);
        //getting project contacts
        $project_contact = $this->Project_model->getProjectContact(array('crm_project_id' => $data['crm_project_id']));


        $maritalStatus = $this->Crm_model->checkRecord('master_child', array('child_key'=>'reference_guarantor'));
        $relation_type_ids=$maritalStatus[0]['id_child'];
        foreach($project_contact as $kc=>$vc){
            $crm_contact_relations = $this->Crm_model->getRelationshipContacts(array('child_id'=>$relation_type_ids,'crm_contact_id'=>$vc['id_crm_contact']));
            $project_contact[$kc]['reference_guarantor']=array();
            $cnt=0;
            foreach($crm_contact_relations as $kr=>$vr){
                $getContact=$this->Crm_model->getContact($vr['contact_id'],1);
                $project_contact[$kc]['reference_guarantor'][$cnt]=$getContact[0];
                $cnt++;
            }
        }
        $project[0]['contacts'] = $project_contact;
        $project_contact = array_map(function($key){ return $key['first_name']; }, $project_contact);
        $project[0]['contact_name'] = implode(',',$project_contact);

        $project[0]['rating'] = '---';
        $project[0]['grade'] = '---';

        $facilities = $this->Project_model->getFacilityDetails(array('crm_project_id' => $project[0]['id_crm_project']));
        $project[0]['no_of_loans'] = count($facilities);
        $loan_amount = array(); $unique_currency_code = array(); $usd_loan_amount = $project[0]['intermediary_loan'] = 0;
        //echo "<pre>"; print_r($facilities); exit;
        for($s=0;$s<count($facilities);$s++){
            $this->load->model('Collateral_model');
            $facilities[$s]['collaterals']=$this->Collateral_model->getProjectFacilityCollateral(array('crm_project_id' => $project[0]['id_crm_project'],'project_facility_id'=>$facilities[$s]['id_project_facility']));
            array_push($unique_currency_code,$facilities[$s]['currency_code']);
            $loan_amount[$facilities[$s]['currency_code']] = array(
                'currency_code' => $facilities[$s]['currency_code'],
                'amount' => $facilities[$s]['facility_amount']
            );
            $usd_loan_amount = $usd_loan_amount+$facilities[$s]['facility_usd_amount'];
            if($facilities[$s]['loan_payment_type_key_name']=='intermediary_loan'){
                $project[0]['intermediary_loan'] = 1;
            }

        }
        $loan_amount = array();
        $loan_amount[] = array(
            'currency_code' => 'USD',
            'amount' => $usd_loan_amount
        );

        $project[0]['loan_amount'] = array_values($loan_amount);
        $project[0]['facilities'] = $facilities;
        $project_stage_info = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $project[0]['project_stage_info_id']));
        $project[0]['stage_id'] = $project_stage_info[0]['stage_id'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$project[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

}
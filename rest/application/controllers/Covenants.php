<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Covenants extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Crm_model');
        $this->load->model('Covenant_model');
    }

    public function covenantList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Covenant_model->getCovenantsList($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['last_review_date'] = '';
            $covenant_task = $this->Covenant_model->getModuleCovenantLastReviewDate(array('module_covenant_id' => $result[$s]['id_module_covenant']));
            if(!empty($covenant_task)){
                $result[$s]['last_review_date'] = $covenant_task[0]['review_date'];
            }
        }

        $count = $this->Covenant_model->getCovenantsListCount($data);
        $count = array_sum(array_map(function($item) {
            return $item['total'];
        }, $count));
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => $result, 'total_records' => $count));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function list_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Covenant_model->getCovenants($data);
        $count = $this->Covenant_model->covenantCount($data);
        if(!empty($count))
            $count = $count[0]['total'];
        else
            $count = 0;
        //if(isset($data['id_covenant']))
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['sector_name'] = '';
            //covenant sector
            $result[$s]['sector'] = $this->Covenant_model->getCovenantSector(array('covenant_id' => $result[$s]['id_covenant']));
            if(isset($data['id_covenant']))
                $result[$s]['sector'] = array_map(function($i){ return $i['sector_id']; },$result[$s]['sector']);
            else
                $result[$s]['sector_name'] = implode(',',array_map(function($i){ return $i['sector_name']; },$result[$s]['sector']));
            //covenant category
            $result[$s]['category'] = $this->Covenant_model->getCovenantCategory(array('covenant_id' => $result[$s]['id_covenant']));
            if(isset($data['id_covenant']))
                $result[$s]['category'] = array_map(function($i){ return $i['category_id']; },$result[$s]['category']);
        }
        if(isset($data['id_covenant'])) {
            $result = $result[0];
            $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $result);
        }
        else{
            $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => $result, 'total_records' => $count));
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addCovenant_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_name', array('required'=> $this->lang->line('covenant_name_req')));
        $this->form_validator->add_rules('covenant_type_id', array('required'=> $this->lang->line('covenant_type_id_req')));
        $this->form_validator->add_rules('covenant_mandatory', array('required'=> $this->lang->line('covenant_mandatory_req')));
        $this->form_validator->add_rules('frequency_id', array('required'=> $this->lang->line('frequency_id_req')));
        $this->form_validator->add_rules('notice_days', array('required'=> $this->lang->line('notice_days_req')));
        $this->form_validator->add_rules('grace_days', array('required'=> $this->lang->line('grace_days_req')));
        $this->form_validator->add_rules('covenant_tags', array('required'=> $this->lang->line('covenant_tags_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$data['covenant_tags'] = implode(',',(array)$data['covenant_tags']);
        if(isset($data['id_covenant']))
        {
            $this->Covenant_model->updateCovenant(array(
                'id_covenant' => $data['id_covenant'],
                'covenant_name' => $data['covenant_name'],
                'covenant_description' => $data['covenant_description'],
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_mandatory' => $data['covenant_mandatory'],
                'frequency_id' => $data['frequency_id'],
                'notice_days' => $data['notice_days'],
                'grace_days' => $data['grace_days'],
                'covenant_tags' => $data['covenant_tags']
            ));
            $covenant_id = $data['id_covenant'];
            //update sector
            $sectors = $this->Covenant_model->getCovenantSector(array('covenant_id' => $covenant_id));
            $sector_id = array_map(function($i){ return $i['sector_id']; },$sectors);
            $new = array_values(array_diff($data['sector'],$sector_id));
            $add = array();
            for($s=0;$s<count($new);$s++){
                $add[] = array(
                    'covenant_id' => $covenant_id,
                    'sector_id' => $new[$s]
                );
            }
            if(!empty($add))
                $this->Covenant_model->addCovenantSector($add);
            $delete = array_diff($sector_id,$data['sector']);
            if(!empty($delete))
                $this->Covenant_model->deleteCovenantSector(array('covenant_id' => $covenant_id,'sector_id' => $delete));

            //update category
            $category = $this->Covenant_model->getCovenantCategory(array('covenant_id' => $covenant_id));
            $category_id = array_map(function($i){ return $i['category_id']; },$category);
            $new = array_values(array_diff($data['category'],$category_id))        ;
            $add = array();
            for($s=0;$s<count($new);$s++){
                $add[] = array(
                    'covenant_id' => $covenant_id,
                    'category_id' => $new[$s]
                );
            }
            if(!empty($add))
                $this->Covenant_model->addCovenantCategory($add);
            $delete = array_diff($category_id,$data['category']);
            if(!empty($delete))
                $this->Covenant_model->deleteCovenantCategory(array('covenant_id' => $covenant_id,'category_id' => $delete));

            $msg =  $this->lang->line('covenant_update');
        }
        else
        {
            if(!isset($data['covenant_description'])){ $data['covenant_description'] = ''; }
            $covenant_id = $this->Covenant_model->addCovenant(array(
                'covenant_name' => $data['covenant_name'],
                'covenant_description' => $data['covenant_description'],
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_mandatory' => $data['covenant_mandatory'],
                'frequency_id' => $data['frequency_id'],
                'notice_days' => $data['notice_days'],
                'grace_days' => $data['grace_days'],
                'covenant_tags' => $data['covenant_tags'],
                'company_id' => $data['company_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            ));

            if(isset($data['sector'])){
                $add = array();
                for($s=0;$s<count($data['sector']);$s++){
                    $add[] = array(
                        'covenant_id' => $covenant_id,
                        'sector_id' => $data['sector'][$s]
                    );
                }
                $this->Covenant_model->addCovenantSector($add);
            }
            if(isset($data['category'])){
                $add = array();
                for($s=0;$s<count($data['category']);$s++){
                    $add[] = array(
                        'covenant_id' => $covenant_id,
                        'category_id' => $data['category'][$s]
                    );
                }
                $this->Covenant_model->addCovenantCategory($add);
            }
            $msg =  $this->lang->line('covenant_add');
        }


        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('covenant_id', array('required'=> $this->lang->line('covenant_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*$this->Covenant_model->deleteCovenantSector(array('covenant_id' => $data['covenant_id']));
        $this->Covenant_model->deleteCovenantCategory(array('covenant_id' => $data['covenant_id']));
        $this->Covenant_model->deleteCovenant(array('covenant_id' => $data['covenant_id']));*/
        $this->Covenant_model->updateCovenant(array('id_covenant' => $data['covenant_id'],'covenant_status' => 0));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantType_get()
    {
        $data = $this->input->get();
        $result = $this->Covenant_model->getCovenantType();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function search_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('search_type', array('required'=> $this->lang->line('search_type_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        //$this->form_validator->add_rules('category_name', array('required'=> $this->lang->line('category_name_req')));
        if(isset($data['search_type']) && $data['search_type'])
            $this->form_validator->add_rules('search_key', array('required'=> $this->lang->line('search_key_req')));
        //$this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($data['search_type']) {
            $result = $this->Covenant_model->getOtherModuleCovenants($data);
        }
        else {
            $result = $this->Covenant_model->getSearchCovenants($data);
        }
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function moduleCovenant_get()
    {
        $data = $this->input->get();
        if(!isset($data['id_module_covenant'])) {
            $this->form_validator->add_rules('module_id', array('required' => $this->lang->line('module_id_req')));
            $this->form_validator->add_rules('module_type', array('required' => $this->lang->line('module_type_req')));
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Covenant_model->getModuleCovenants($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['assigned_to'] = explode(',',$result[$s]['assigned_to']);
        }
        if(isset($data['id_module_covenant']) && !empty($result))
            $result = $result[0];
        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function moduleCovenant_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_id', array('required'=> $this->lang->line('covenant_ids_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('reference_id', array('required'=> $this->lang->line('reference_id_req')));
        $this->form_validator->add_rules('reference_type', array('required'=> $this->lang->line('reference_type_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $covenants = $this->Covenant_model->getCovenants(array('covenant_ids' => $data['covenant_id']));
        $covenant_data = array();
        for($s=0;$s<count($covenants);$s++)
        {
            $covenant_data[] = array(
                'covenant_id' => $covenants[$s]['id_covenant'],
                'module_id' => $data['module_id'],
                'module_type' => $data['module_type'],
                'covenant_mandatory' => $covenants[$s]['covenant_mandatory'],
                'covenant_frequency_id' => $covenants[$s]['frequency_id'],
                'notice_days' => $covenants[$s]['notice_days'],
                'grace_days' => $covenants[$s]['grace_days'],
                'created_date_time' => currentDate(),
                'reference_id' => $data['reference_id'],
                'reference_type' => $data['reference_type']
            );
        }

        if(!empty($covenant_data))
            $this->Covenant_model->addBatchModuleCovenant($covenant_data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function update_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_module_covenant', array('required'=> $this->lang->line('module_covenant_id_req')));
        $this->form_validator->add_rules('covenant_id', array('required'=> $this->lang->line('covenant_ids_req')));
        $this->form_validator->add_rules('covenant_mandatory', array('required'=> $this->lang->line('covenant_mandatory_req')));
        $this->form_validator->add_rules('covenant_frequency_id', array('required'=> $this->lang->line('frequency_id_req')));
        $this->form_validator->add_rules('notice_days', array('required'=> $this->lang->line('notice_days_req')));
        $this->form_validator->add_rules('grace_days', array('required'=> $this->lang->line('grace_days_req')));
        $this->form_validator->add_rules('covenant_start_date', array('required'=> $this->lang->line('start_date_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('assigned_to', array('required'=> $this->lang->line('assigned_to_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Covenant_model->updateModuleCovenant(array(
            'id_module_covenant' => $data['id_module_covenant'],
            'covenant_mandatory' => $data['covenant_mandatory'],
            'covenant_frequency_id' => $data['covenant_frequency_id'],
            'notice_days' => $data['notice_days'],
            'grace_days' => $data['grace_days'],
            'covenant_start_date' => $data['covenant_start_date'],
            'created_by' => $data['created_by']
        ));
        $users = $this->Covenant_model->getCovenantUsers(array('module_covenant_id' => $data['id_module_covenant']));
        $user_id = array_map(function($i){ return $i['user_id']; },$users);
        $new = array_values(array_diff($data['assigned_to'],$user_id));

        $add = array();
        for($s=0;$s<count($new);$s++){
            $add[] = array(
                'module_covenant_id' => $data['id_module_covenant'],
                'user_id' => $new[$s]
            );
        }

        if(!empty($add))
            $this->Covenant_model->addCovenantUsers($add);
        $delete = array_diff($user_id,$data['assigned_to']);
        if(!empty($delete))
            $this->Covenant_model->deleteCovenantUsers(array('module_covenant_id' => $data['id_module_covenant'],'user_id' => $delete));

        //updating notification date and due date
        $module_covenant = $this->Covenant_model->getModuleCovenants(array('id_module_covenant' => $data['id_module_covenant']));
        //echo "<pre>"; print_r($module_covenant); exit;
        if(!empty($module_covenant) && $module_covenant[0]['module_covenant_status']=='new' && $module_covenant[0]['covenant_start_date']!=''){
            $due_date = '';
            if(strtolower($module_covenant[0]['frequency_key'])=='yearly'){
                $due_date = date('Y-m-d', strtotime('+1 year', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='half_yearly'){
                $due_date = date('Y-m-d', strtotime('+6 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='quarterly'){
                $due_date = date('Y-m-d', strtotime('+3 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='monthly'){
                $due_date = date('Y-m-d', strtotime('+1 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='weekly'){
                $due_date = date('Y-m-d', strtotime('+7 day', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='daily'){
                $due_date = date('Y-m-d', strtotime('+1 day', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            $notification_date = date('Y-m-d',strtotime('-'.$module_covenant[0]['notice_days'].' days',strtotime($due_date)));
            $this->Covenant_model->updateModuleCovenant(array(
                'id_module_covenant' => $data['id_module_covenant'],
                'next_due_date' => $due_date,
                'notification_date' => $notification_date
            ));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function moduleCovenant_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_module_covenant', array('required'=> $this->lang->line('module_covenant_id_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //$this->Covenant_model->deleteModuleCovenantUsers(array('module_covenant_id' => $data['id_module_covenant']));
        //$this->Covenant_model->deleteModuleCovenant(array('id_module_covenant' => $data['id_module_covenant']));
        $this->Covenant_model->updateModuleCovenant(array('id_module_covenant' => $data['id_module_covenant'],'module_covenant_status' => 'deleted'));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function task_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        /*echo "<pre>"; print_r($data);
        print_r($_FILES);exit;*/
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_req')));
        $this->form_validator->add_rules('covenant_task_id', array('required'=> $this->lang->line('covenant_task_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        if(isset($data['type']) && ($data['type']!='postpone'))
            $this->form_validator->add_rules('comments', array('required'=> $this->lang->line('comments_req')));
        if(isset($data['type']) && ($data['type']=='postpone' || $data['type']=='schedule')){
            $this->form_validator->add_rules('date', array('required'=> $this->lang->line('date_req')));
            if($data['type']=='postpone')
                $this->form_validator->add_rules('action_reason', array('required'=> $this->lang->line('reason_req')));
        }
        else if(isset($data['type']) && ($data['type']=='submitted')){
            $this->form_validator->add_rules('task_condition', array('required'=> $this->lang->line('task_condition_req')));
        }

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['action_reason'])){ $data['action_reason'] = ''; }
        if(!isset($data['date'])){ $data['date'] = currentDate(); }
        $company_id = 0;
        if(isset($data['company_id'])){ $company_id = $data['company_id']; }
        $task_action_id = $this->Covenant_model->addTaskAction(array(
            'covenant_task_id' => $data['covenant_task_id'],
            'action_reason' => $data['action_reason'],
            'action_date' => $data['date'],
            'action_comments' => $data['comments'],
            'action_by' => $data['created_by'],
            'task_action_status' => $data['type'],
            'created_date_time' => currentDate()
        ));

        if($data['type']=='submitted')
        {
            $this->Covenant_model->updateTask(array(
                'id_covenant_task' => $data['covenant_task_id'],
                'task_condition' => $data['task_condition'],
                'task_comments' => $data['comments']
            ));
        }

        $this->Covenant_model->updateTask(array(
            'id_covenant_task' => $data['covenant_task_id'],
            'task_status' => $data['type']
        ));

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => 0, 'uploaded_from_id' => $task_action_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'project','form_key' => 'covenant_task');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => 0, 'uploaded_from_id' => $task_action_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_type' => 'project','form_key' => 'covenant_task');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function taskHistory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('module_covenant_id', array('required'=> $this->lang->line('module_covenant_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Covenant_model->getTaskHistory($data);
        $count = $this->Covenant_model->getTaskHistoryCount($data);
        if(empty($count)){ $count = 0; }
        else{ $count = $count[0]['total']; }

        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => $result, 'total_records' => $count));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function taskActionHistory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('covenant_task_id', array('required'=> $this->lang->line('covenant_task_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Covenant_model->getTaskActionHistory($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['documents'] = array();
            if($result[$s]['task_action_status']=='submitted'){
                $result[$s]['documents'] = $this->Crm_model->getDocument(array('group_id' => $result[$s]['id_covenant_task_action'],'module_type' => 'project','form_key' => 'covenant_task'));
            }

            for($st=0;$st<count($result[$s]['documents']);$st++){
                $result[$s]['documents'][$st]['document_source'] = REST_API_URL.'uploads/'.$result[$s]['documents'][$st]['document_source'];
            }
        }
        $count = $this->Covenant_model->getTaskActionHistoryCount($data);
        if(empty($count)){ $count = 0; }
        else{ $count = $count[0]['total']; }

        $result = array('status' => TRUE, 'message' => $this->lang->line('success'), 'data' => array('data' => $result, 'total_records' => $count));
        $this->response($result, REST_Controller::HTTP_OK);
    }

}
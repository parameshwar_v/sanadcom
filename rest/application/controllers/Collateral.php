<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Collateral extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('Collateral_model');
        $this->load->model('Covenant_model');
    }

    public function type_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralType($data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function typeFields_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required' => $this->lang->line('collateral_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Collateral_model->getCollateralType(array('company_id' => $data['company_id'], 'collateral_type_id' => $data['collateral_type_id']));
        if (!empty($result)) {
            $result = $result[0];
            $result['fields'] = $this->Collateral_model->getCollateralTypeFields($data);
        }
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stages_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required' => $this->lang->line('collateral_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralTypeStages($data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function stages_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required' => $this->lang->line('collateral_type_id_req')));
        $this->form_validator->add_rules('collateral_stage_id', array('required' => $this->lang->line('collateral_stage_id')));
        $this->form_validator->add_rules('status', array('required' => $this->lang->line('status')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $already_exists = $this->Collateral_model->checkCollateralTypeStage($data);
        //echo "<pre>"; print_r($already_exists); exit;
        if (empty($already_exists)) {
            $result = $this->Collateral_model->addCollateralTypeStages(array(
                'company_id' => $data['company_id'],
                'collateral_type_id' => $data['collateral_type_id'],
                'collateral_stage_id' => $data['collateral_stage_id'],
                'status' => $data['status']
            ));
        } else {
            $result = $this->Collateral_model->updateCollateralTypeStages(array(
                'id_collateral_type_stage' => $already_exists[0]['id_collateral_type_stage'],
                'status' => $data['status']
            ));
        }


        $result = array('status' => TRUE, 'message' => $this->lang->line('collateral_stage_update'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function list_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        //$this->form_validator->add_rules('project_facility_id', array('required'=>$this->lang->line('project_facility_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralList($data);
        //echo $this->db->last_query(); exit;
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['project_title'] = '';
            $facility_collateral = $this->Collateral_model->getProjectCollateral(array('collateral_id' => $result[$s]['id_collateral']));
            if(!empty($facility_collateral))
                $result[$s]['project_title'] = $facility_collateral[0]['project_title'];
        }

        unset($data['offset']);
        unset($data['limit']);
        $data['count'] = 1;
        $total_records = $this->Collateral_model->getCollateralCount($data);
        $total_records = $total_records[0]['total'];
        unset($data['search_key']);

        //all records
        if(isset($data['created_by'])){ $created_by = $data['created_by']; unset($data['created_by']); }
        $all_collateral = $this->Collateral_model->getCollateralCount($data);
        $all_collateral = $all_collateral[0]['total'];

        //created by user records
        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $my_collateral = $this->Collateral_model->getCollateralCount($data);
        $my_collateral = $my_collateral[0]['total'];

        if (isset($data['id_collateral']))
            $result = array('status' => TRUE, 'message' => '', 'data' => $result[0]);
        else
            $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records,'all_collateral' => $all_collateral,'my_collateral' => $my_collateral));

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function add_post()
    {
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        //$this->form_validator->add_rules('project_facility_id', array('required'=>$this->lang->line('project_facility_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        //$this->form_validator->add_rules('collateral_number', array('required' => $this->lang->line('collateral_number_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required' => $this->lang->line('collateral_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*if(isset($data['id_collateral']))
            $check = $this->Collateral_model->getCollateralList(array('collateral_number' => $data['collateral_number'],'collateral_id_not_equal' => $data['id_collateral']));
        else
            $check = $this->Collateral_model->getCollateralList(array('collateral_number' => $data['collateral_number']));

        if(!empty($check)){
            $result = array('status' => FALSE, 'error' => array('collateral_number' => $this->lang->line('collateral_number_duplicate')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/

        $collateral_data = array(
            'company_id' => $data['company_id'],
            //'project_facility_id' => $data['project_facility_id'],
            //'collateral_number' => $data['collateral_number'],
            'collateral_type_id' => $data['collateral_type_id'],
            'application_priority_id' => isset($data['application_priority_id']) ? $data['application_priority_id'] : '',
            'collateral_currency_id' => isset($data['collateral_currency_id']) ? $data['collateral_currency_id'] : '',
            'collateral_amount' => isset($data['collateral_amount']) ? $data['collateral_amount'] : '',
            'usd_equal_amount' => isset($data['usd_equal_amount']) ? $data['usd_equal_amount'] : '',
            'collateral_description' => isset($data['collateral_description']) ? $data['collateral_description'] : '',
            'collateral_expiry_date' => isset($data['collateral_expiry_date']) ? $data['collateral_expiry_date'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if(isset($data['id_collateral']))
        {
            $collateral_data['id_collateral'] = $data['id_collateral'];
            if(isset($data['collateral_status']))
                $collateral_data['collateral_status'] = $data['collateral_status'];
            $this->Collateral_model->updateCollateral($collateral_data);
            $msg = $this->lang->line('collateral_update');
        }
        else
        {
            $collateral_data['created_by'] = $data['created_by'];
            $collateral_data['created_date_time'] = currentDate();
            $collateral_id=$this->Collateral_model->addCollateral($collateral_data);
            $collateral_data_up['id_collateral'] = $collateral_id;
            //$collateral_data_up['collateral_number'] = 'SANADCOM'.mt_rand(1000, 9999).$collateral_id;
            $collateral_data_up['collateral_number'] = 'SANADCOM'.str_pad($collateral_id, 6, '0', STR_PAD_LEFT);
            $this->Collateral_model->updateCollateral($collateral_data_up);
            $msg = $this->lang->line('collateral_add');
        }

        $result = array('status' => TRUE, 'message' => $msg, 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function contact_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralContact($data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('crm_contact_id', array('required' => $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('relation_type_id', array('required' => $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $contact_data = array(
            'collateral_id' => $data['collateral_id'],
            'crm_contact_id' => $data['crm_contact_id'],
            'relation_type_id' => $data['relation_type_id'],
            'created_by' => $data['created_by'],
            'updated_by' => $data['created_by'],
            'created_date_time' => currentDate(),
            'updated_date_time' => currentDate()
        );
        $result = $this->Collateral_model->addCollateralContact($contact_data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_collateral_contact', array('required' => $this->lang->line('collateral_contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Collateral_model->deleteCollateralContact($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestions_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('assessment_question_type_key', array('required'=> $this->lang->line('type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['collateral_type_id'])){ $data['collateral_type_id'] = 0; }
        if(!isset($data['collateral_stage_id'])){ $data['collateral_stage_id'] = 0; }
        if(!isset($data['crm_reference_id'])){ $data['crm_reference_id'] = 0; }

        $resultArray = array(); $order = 'DESC';
        $question_type_key = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['assessment_question_type_key']));
        if(!empty($question_type_key)) {
            $resultArray = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $question_type_key[0]['id_assessment_question_type'],'collateral_type_id' => $data['collateral_type_id'], 'collateral_stage_id' => $data['collateral_stage_id'], 'order' => $order));

            for ($r = 0; $r < count($resultArray); $r++) {
                $resultArray[$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $resultArray[$r]['id_assessment_question_category'], 'question_status' => 1));
                for ($sr = 0; $sr < count($resultArray[$r]['question']); $sr++) {
                    if (isset($resultArray[$r]['question'][$sr]['question_option']) && $resultArray[$r]['question'][$sr]['question_option'] != '') {
                        $resultArray[$r]['question'][$sr]['question_option'] = json_decode($resultArray[$r]['question'][$sr]['question_option']);
                    }
                    $resultArray[$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $resultArray[$r]['question'][$sr]['id_assessment_question'],'crm_reference_id' => $data['crm_reference_id']));

                    if (isset($resultArray[$r]['question'][$sr]['answer']->assessment_answer) && $resultArray[$r]['question'][$sr]['answer']->assessment_answer != '') {
                        if ($resultArray[$r]['question'][$sr]['question_type'] == 'checkbox')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = json_decode($resultArray[$r]['question'][$sr]['answer']->assessment_answer);
                        else if ($resultArray[$r]['question'][$sr]['question_type'] == 'file')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($resultArray[$r]['question'][$sr]['answer']->assessment_answer);
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $resultArray);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function valuationDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getValuationDetails($data);

        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_valuation'],'module_type' => 'collateral','form_key' => 'valuation_details'));
        }

        $total_records = $this->Collateral_model->getValuationDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function valuationDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => $data['collateral_id'],
            'contact_name' => isset($data['contact_name']) ? $data['contact_name'] : '',
            'agency_name' => isset($data['agency_name']) ? $data['agency_name'] : '',
            'valuation_amount_in_usd' => isset($data['valuation_amount_in_usd']) ? $data['valuation_amount_in_usd'] : '',
            'valuation_date' => isset($data['valuation_date']) ? $data['valuation_date'] : '',
            'valuation_remarks' => isset($data['valuation_remarks']) ? $data['valuation_remarks'] : '',
            'credit_remarks' => isset($data['credit_remarks']) ? $data['credit_remarks'] : '',
            'valuation_basis' => isset($data['valuation_basis']) ? $data['valuation_basis'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_valuation'])) {
            $update_data['id_valuation'] = $data['id_valuation'];
            $this->Collateral_model->updateValuationDetails($update_data);
            $valuation_id = $data['id_valuation'];
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $valuation_id = $this->Collateral_model->addValuationDetails($update_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $data['document_type'] = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $valuation_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'collateral', 'form_key' => 'valuation_details');
                    $document_id = $this->Crm_model->addDocument($document);

                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $valuation_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'collateral', 'form_key' => 'valuation_details');
                $document_id = $this->Crm_model->addDocument($document);

            }
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function valuationDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_valuation', array('required' => $this->lang->line('valuation_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Collateral_model->deleteValuationDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function insuranceDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getInsuranceDetails($data);
        $total_records = $this->Collateral_model->getInsuranceDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function insuranceDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'insurance_number' => isset($data['insurance_number']) ? $data['insurance_number'] : '',
            'insurance_company_name' => isset($data['insurance_company_name']) ? $data['insurance_company_name'] : '',
            'insurance_owner' => isset($data['insurance_owner']) ? $data['insurance_owner'] : '',
            'insurance_company_location' => isset($data['insurance_company_location']) ? $data['insurance_company_location'] : '',
            'coverage_amount_in_usd' => isset($data['coverage_amount_in_usd']) ? $data['coverage_amount_in_usd'] : '',
            'insurance_type_id' => isset($data['insurance_type_id']) ? $data['insurance_type_id'] : '',
            'beneficiary_of_insurance_id' => isset($data['beneficiary_of_insurance_id']) ? $data['beneficiary_of_insurance_id'] : '',
            'start_date' => isset($data['start_date']) ? $data['start_date'] : '',
            'end_date' => isset($data['end_date']) ? $data['end_date'] : '',
            'revision_date' => isset($data['revision_date']) ? $data['revision_date'] : '',
            'periodicity' => isset($data['periodicity']) ? $data['periodicity'] : '',
            'insurance_currency_id' => isset($data['insurance_currency_id']) ? $data['insurance_currency_id'] : '',
            'insurance_amount' => isset($data['insurance_amount']) ? $data['insurance_amount'] : '',
            'insurance_usd_equal_to' => isset($data['insurance_usd_equal_to']) ? $data['insurance_usd_equal_to'] : '',
            'premium_currency_id' => isset($data['premium_currency_id']) ? $data['premium_currency_id'] : '',
            'premium_amount' => isset($data['premium_amount']) ? $data['premium_amount'] : '',
            'premium_usd_equal_to' => isset($data['premium_usd_equal_to']) ? $data['premium_usd_equal_to'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_insurance'])) {
            $update_data['id_insurance'] = $data['id_insurance'];
            $this->Collateral_model->updateInsuranceDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addInsuranceDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function insuranceDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_insurance', array('required' => $this->lang->line('insurance_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteInsuranceDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function fieldInvestigationDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getFieldInvestigationDetails($data);

        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_field_investigation'],'module_type' => 'collateral','form_key' => 'field_investigation'));
        }

        $total_records = $this->Collateral_model->getFieldInvestigationDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function fieldInvestigationDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'contact_name' => isset($data['contact_name']) ? $data['contact_name'] : '',
            'agency_name' => isset($data['agency_name']) ? $data['agency_name'] : '',
            'result' => isset($data['result']) ? $data['result'] : '',
            'date' => isset($data['date']) ? $data['date'] : '',
            'visit_remarks' => isset($data['visit_remarks']) ? $data['visit_remarks'] : '',
            'credit_remarks' => isset($data['credit_remarks']) ? $data['credit_remarks'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_field_investigation'])) {
            $update_data['id_field_investigation'] = $data['id_field_investigation'];
            $this->Collateral_model->updateFieldInvestigationDetails($update_data);
            $field_investigation_id =$data['id_field_investigation'];
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $field_investigation_id = $this->Collateral_model->addFieldInvestigationDetails($update_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $data['document_type'] = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $field_investigation_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'collateral', 'form_key' => 'field_investigation');
                    $document_id = $this->Crm_model->addDocument($document);

                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $field_investigation_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'collateral', 'form_key' => 'field_investigation');
                $document_id = $this->Crm_model->addDocument($document);

            }
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function fieldInvestigationDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_field_investigation', array('required' => $this->lang->line('field_investigation_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteFieldInvestigationDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function externalCheckDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getExternalCheckDetails($data);
        $total_records = $this->Collateral_model->getExternalCheckDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function externalCheckDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'registration_authority' => isset($data['registration_authority']) ? $data['registration_authority'] : '',
            'registration_authority_contact_details' => isset($data['registration_authority_contact_details']) ? $data['registration_authority_contact_details'] : '',
            'result' => isset($data['result']) ? $data['result'] : '',
            'date_of_check' => isset($data['date_of_check']) ? $data['date_of_check'] : '',
            'credit_remarks' => isset($data['credit_remarks']) ? $data['credit_remarks'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_external_check'])) {
            $update_data['id_external_check'] = $data['id_external_check'];
            $this->Collateral_model->updateExternalCheckDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addExternalCheckDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function externalCheckDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_external_check', array('required' => $this->lang->line('external_check_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteExternalCheckDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskEvaluationDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getRiskEvaluationDetails($data);

        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['risk'] = json_decode($result[$s]['risk']);
            $result[$s]['risk']->id_risk_evaluation = $result[$s]['id_risk_evaluation'];
        }
        $total_records = $this->Collateral_model->getRiskEvaluationDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskEvaluationDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'risk' => isset($data['risk']) ? json_encode($data['risk']) : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_risk_evaluation'])) {
            $update_data['id_risk_evaluation'] = $data['id_risk_evaluation'];
            $this->Collateral_model->updateRiskEvaluationDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addRiskEvaluationDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskEvaluationDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_risk_evaluation', array('required' => $this->lang->line('risk_evaluation_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteRiskEvaluationDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function legalOpinionDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getLegalOpinionDetails($data);
        $total_records = $this->Collateral_model->getLegalOpinionDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function legalOpinionDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'legal_opinion' => isset($data['legal_opinion']) ? $data['legal_opinion'] : '',
            'legal_adviser_details' => isset($data['legal_adviser_details']) ? $data['legal_adviser_details'] : '',
            'date_of_advice' => isset($data['date_of_advice']) ? $data['date_of_advice'] : '',
            'external_opinion' => isset($data['external_opinion']) ? $data['external_opinion'] : '',
            'external_adviser_details' => isset($data['external_adviser_details']) ? $data['external_adviser_details'] : '',
            'credit_remarks' => isset($data['credit_remarks']) ? $data['credit_remarks'] : '',
            'final_recommendation' => isset($data['final_recommendation']) ? $data['final_recommendation'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_legal_opinion'])) {
            $update_data['id_legal_opinion'] = $data['id_legal_opinion'];
            $this->Collateral_model->updateLegalOpinionDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addLegalOpinionDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function legalOpinionDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_legal_opinion', array('required' => $this->lang->line('legal_opinion_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteLegalOpinionDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralContactDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralContactDetails($data);
        $total_records = $this->Collateral_model->getCollateralContactDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralContactDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => isset($data['collateral_id']) ? $data['collateral_id'] : '',
            'crm_contact_id' => isset($data['crm_contact_id']) ? $data['crm_contact_id'] : '',
            'relation_type_id' => isset($data['relation_type_id']) ? $data['relation_type_id'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_collateral_contact'])) {
            $update_data['id_collateral_contact'] = $data['id_collateral_contact'];
            $this->Collateral_model->updateCollateralContactDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addCollateralContactDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralContactDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_collateral_contact', array('required' => $this->lang->line('collateral_contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->deleteCollateralContactDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$this->form_validator->add_rules('project_id', array('required' => $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['status'] = 1;
        $result = $this->Collateral_model->getProjectFacilityCollateral($data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('project_facility_id', array('required' => $this->lang->line('project_facility_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getProjectFacilityCollateral($data);
        if(empty($result)){
            $this->Collateral_model->addProjectFacilityCollateral(array(
                'project_facility_id' => $data['project_facility_id'],
                'collateral_id' => $data['collateral_id'],
                'created_by' => $data['created_by'],
                'updated_by' => $data['created_by'],
                'status' => 1,
                'created_date_time' => currentDate(),
                'updated_date_time' => currentDate()
            ));
        }
        else{
            $this->Collateral_model->updateProjectFacilityCollateral(array(
                'id_project_facility_collateral' => $result[0]['id_project_facility_collateral'],
                'status' => 1,
                'updated_by' => $data['created_by'],
                'updated_date_time' => currentDate()
            ));
        }
        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_project_facility_collateral', array('required' => $this->lang->line('collateral_id_req')));
        //$this->form_validator->add_rules('project_facility_id', array('required' => $this->lang->line('project_facility_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->updateProjectFacilityCollateral(array(
            'id_project_facility_collateral' => $data['id_project_facility_collateral'],
            'status' => 0,
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        ));
        $result = array('status' => TRUE, 'message' => $this->lang->line('succ_delete'), 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facilitySearch_get()
    {
        $data = $this->input->get();
        $result = $this->Collateral_model->getProjectFacilityForCollateral($data);
        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function otherFacilityCollateral_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->otherFacilityCollateral($data);

        $result = array('status' => TRUE, 'message' => '', 'data' => $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function typeFormData_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Collateral_model->getCollateralType(array('company_id' => $data['company_id'], 'collateral_type_id' => $data['collateral_type_id']));
        $collateral_type_form_data = $this->Collateral_model->getCollateralTypeFieldData($data);
        for($s=0;$s<count($collateral_type_form_data);$s++){
            $collateral_type_form_data[$s]['form_field_value'] = $collateral_type_form_data[$s]['collateral_type_field_value'];
        }
        $result[0]['fields'] = $collateral_type_form_data;
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=> $result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function typeFormData_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Collateral_model->getCollateralTypeFields($data);
        $collateral_type_form_data = $this->Collateral_model->getCollateralTypeFormData($data);
        if(empty($collateral_type_form_data)) $type = 'insert'; else $type = 'update';
        $collateralId = $data['collateral_id'];
        unset($data['collateral_id']);
        $collateralTypeId = $data['collateral_type_id'];
        $created_by = $data['created_by'];
        unset($data['collateral_type_id']);
        unset($data['created_by']);


        for($s=0;$s<count($form_fields);$s++){
            $check_field = 0;
            $form_data[$st]['collateral_id'] = $collateralId;
            $form_data[$st]['collateral_type_id'] = $collateralTypeId;
            $form_data[$st]['collateral_type_field_id'] = $form_fields[$s]['id_collateral_type_field'];
            $form_data[$st]['created_by'] = $created_by;
            $form_data[$st]['created_date_time'] = currentDate();
            $form_data[$st]['updated_by'] = $created_by;
            $form_data[$st]['updated_date_time'] = currentDate();
            for($r=0;$r<count($data['data']);$r++){
                if($form_fields[$s]['id_collateral_type_field']==$data['data'][$r]['id_collateral_type_field']){
                    if(isset($data['data'][$r]['form_field_value']))
                        $form_data[$st]['collateral_type_field_value'] = $data['data'][$r]['form_field_value'];
                    else
                        $form_data[$st]['collateral_type_field_value'] = '';
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['collateral_type_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => $this->lang->line('invalid_fields'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($type=='insert')
        {//echo "<pre>"; print_r($form_data); exit;
            $this->Collateral_model->addCollateralTypeFormData($form_data);
            $this->Activity_model->addActivity(array('activity_name' => 'Collateral Details','activity_template' => 'Collateral Details Created','module_type' => 'collateral','module_id' =>  $collateralId, 'activity_type' => 'form','created_by' => $created_by,'created_date_time' => currentDate()));

        }
        else
        {
            if(!empty($collateral_type_form_data)){
                for($s=0;$s<count($collateral_type_form_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($collateral_type_form_data[$s]['collateral_type_field_id']==$form_data[$r]['collateral_type_field_id']){
                            $form_data[$r]['id_collateral_type_field_data'] = $collateral_type_form_data[$s]['id_collateral_type_field_data'];
                            unset($form_data[$r]['created_by']);
                            unset($form_data[$r]['created_date_time']);
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_collateral_type_field_data'])){
                    $form_data[$sr]['created_date_time'] = currentDate();
                    $form_data[$sr]['created_by'] = $created_by;
                    $form_data[$sr]['created_date_time'] = currentDate();
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            if(!empty($new_form_fields))
                $this->Collateral_model->addCollateralTypeFormData($new_form_fields);
            $this->Collateral_model->updateCollateralTypeFormData($form_data);
            $this->Activity_model->addActivity(array('activity_name' => 'Collateral Details','activity_template' => 'Collateral Details has been Updated','module_type' => 'collateral','module_id' =>  $collateralId, 'activity_type' => 'form','created_by' => $created_by,'created_date_time' => currentDate()));
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('info_update'), 'data'=> []);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCollateral_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getFacilityCollateral($data);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getCollateralDocument($data);

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('success'), 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_post()
    {
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> $this->lang->line('collateral_type_id_req')));
        $this->form_validator->add_rules('document_name', array('required'=> $this->lang->line('document_name_req')));
        $this->form_validator->add_rules('is_mandatory', array('required'=> $this->lang->line('is_mandatory_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $document_data = array(
            'company_id' => $data['company_id'],
            'collateral_type_id' => $data['collateral_type_id'],
            'document_name' => $data['document_name'],
            'is_mandatory' => $data['is_mandatory']
        );

        if(isset($data['id_collateral_document']))
        {
            $document_data['updated_by'] = $data['created_by'];
            $document_data['updated_date_time'] = currentDate();
            $document_data['id_collateral_document'] = $data['id_collateral_document'];

            $this->Collateral_model->updateCollateralDocument($document_data);
            $msg = $this->lang->line('collateral_document_update');
        }
        else
        {
            $document_data['created_by'] = $data['created_by'];
            $document_data['created_date_time'] = currentDate();

            $this->Collateral_model->addCollateralDocument($document_data);
            $msg = $this->lang->line('collateral_document_add');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_collateral_document', array('required'=> $this->lang->line('collateral_document_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Collateral_model->updateCollateralDocument(array('id_collateral_document' => $data['id_collateral_document'],'status' => 0));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('collateral_document_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_id', array('required'=> $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $facility_collateral = $this->Collateral_model->getProjectFacilityCollateral(array('collateral_id' => $data['collateral_id']));

        if(!empty($facility_collateral)){
            $result = array('status'=>FALSE, 'error' =>array('error' => $this->lang->line('collateral_facility_delete_war')), 'data'=> '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Collateral_model->updateCollateral(array('id_collateral' => $data['collateral_id'],'collateral_status' => 0));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('collateral_document_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function moduleCovenant_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_id', array('required'=> $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('module_type', array('required'=> $this->lang->line('module_type_req')));
        $this->form_validator->add_rules('reference_id', array('required'=> $this->lang->line('reference_id_req')));
        $this->form_validator->add_rules('reference_type', array('required'=> $this->lang->line('reference_type_req')));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_module_covenant']))
        {
            $covenant_data[] = array(
                'covenant_id' => isset($data['id_covenant'])?$data['id_covenant']:0,
                'module_id' => $data['module_id'],
                'module_type' => $data['module_type'],
                'reference_id' => $data['reference_id'],
                'reference_type' => $data['reference_type'],
                'covenant_mandatory' => isset($data['covenant_mandatory'])?$data['covenant_mandatory']:'',
                'covenant_frequency_id' => isset($data['covenant_frequency_id'])?$data['covenant_frequency_id']:'',
                'notice_days' => isset($data['notice_days'])?$data['notice_days']:'',
                'grace_days' => isset($data['grace_days'])?$data['grace_days']:'',
                'covenant_start_date' => isset($data['covenant_start_date'])?$data['covenant_start_date']:'',
                'created_date_time' => currentDate()
            );

            $data['id_module_covenant'] = $this->Covenant_model->addBatchModuleCovenant($covenant_data);
            $msg = $this->lang->line('covenant_add');
            $covenant_data = $covenant_data[0];
        }
        else
        {
            $covenant_data = array(
                'id_module_covenant' => $data['id_module_covenant'],
                'covenant_mandatory' => $data['covenant_mandatory'],
                'covenant_frequency_id' => $data['covenant_frequency_id'],
                'notice_days' => $data['notice_days'],
                'grace_days' => $data['grace_days'],
                'covenant_start_date' => $data['covenant_start_date']
            );

            $this->Covenant_model->updateModuleCovenant($covenant_data);
            $msg = $this->lang->line('covenant_update');
        }

        $users = $this->Covenant_model->getCovenantUsers(array('module_covenant_id' => $data['id_module_covenant']));
        $user_id = array_map(function($i){ return $i['user_id']; },$users);
        $new = array_values(array_diff($data['assigned_to'],$user_id));

        $add = array();
        for($s=0;$s<count($new);$s++){
            $add[] = array(
                'module_covenant_id' => $data['id_module_covenant'],
                'user_id' => $new[$s]
            );
        }

        if(!empty($add))
            $this->Covenant_model->addCovenantUsers($add);
        $delete = array_diff($user_id,$data['assigned_to']);
        if(!empty($delete))
            $this->Covenant_model->deleteCovenantUsers(array('module_covenant_id' => $data['id_module_covenant'],'user_id' => $delete));

        //updating notification date and due date
        $module_covenant = $this->Covenant_model->getModuleCovenants(array('id_module_covenant' => $data['id_module_covenant']));
        //echo "<pre>"; print_r($module_covenant); exit;
        if(!empty($module_covenant) && $module_covenant[0]['module_covenant_status']=='new' && $module_covenant[0]['covenant_start_date']!=''){
            $due_date = '';
            if(strtolower($module_covenant[0]['frequency_key'])=='yearly'){
                $due_date = date('Y-m-d', strtotime('+1 year', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='half_yearly'){
                $due_date = date('Y-m-d', strtotime('+6 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='quarterly'){
                $due_date = date('Y-m-d', strtotime('+3 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='monthly'){
                $due_date = date('Y-m-d', strtotime('+1 month', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='weekly'){
                $due_date = date('Y-m-d', strtotime('+7 day', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            if(strtolower($module_covenant[0]['frequency_key'])=='daily'){
                $due_date = date('Y-m-d', strtotime('+1 day', strtotime($module_covenant[0]['covenant_start_date'])));
            }
            $notification_date = date('Y-m-d',strtotime('-'.$module_covenant[0]['notice_days'].' days',strtotime($due_date)));
            $this->Covenant_model->updateModuleCovenant(array(
                'id_module_covenant' => $data['id_module_covenant'],
                'next_due_date' => $due_date,
                'notification_date' => $notification_date
            ));
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$covenant_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function securityValuationDetails_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Collateral_model->getSecurityValuationDetails($data);

        $total_records = $this->Collateral_model->getSecurityValuationDetails(array('collateral_id' => $data['collateral_id'], 'count' => 1));
        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function securityValuationDetails_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'collateral_id' => $data['collateral_id'],
            'date' => isset($data['date']) ? $data['date'] : '',
            'unit_value' => isset($data['unit_value']) ? $data['unit_value'] : '',
            'comment' => isset($data['comment']) ? $data['comment'] : '',
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );

        if (isset($data['id_security_valuation_details'])) {
            $update_data['id_security_valuation_details'] = $data['id_security_valuation_details'];
            $this->Collateral_model->updateSecurityValuationDetails($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $this->Collateral_model->addSecurityValuationDetails($update_data);
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function securityValuationDetails_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required' => $this->lang->line('collateral_id_req')));
        $this->form_validator->add_rules('id_security_valuation_details', array('required' => $this->lang->line('security_valuation_details_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Collateral_model->deleteSecurityValuationDetails($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactCollateralSearch_get(){
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id',array('required'=> $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('search_key',array('required'=> $this->lang->line('search_key')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*$crm_contact_id = $this->Crm_model->getExistingContactRelation($data);
        $crm_contact_id = array_map(function($i){ return $i['contact_id']; },$crm_contact_id);
        array_push($crm_contact_id,$data['crm_contact_id']);
        $result = $this->Crm_model->getContactRelationSearch(array('crm_contact_id' => array_values($crm_contact_id),'search_key' => $data['search_key']));*/
        $crm_contact_id = $data['crm_contact_id'];
        $result = $this->Collateral_model->getCollateralSearch(array('crm_contact_id' => $crm_contact_id,'search_key' => $data['search_key']));
        //echo '<pre>';print_r($result);exit;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
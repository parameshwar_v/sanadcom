<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Dashboard extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Company_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Crm_model');
        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];
        }

        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if(!empty($user_info) && $user_info[0]['user_role_id']==3 && $user_info[0]['all_projects']==0)
        {
            $this->current_user = $user_id;
        }
        else{
            $this->current_user = 0;
        }
    }

    public function customersByCountry_get()
    {
        $data = $this->Dashboard_model->getCustomersByCountry();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function customersByCategory_get()
    {
        $data = $this->Dashboard_model->getCustomersByCategory();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function childUsers_get()
    {
        $data = $this->input->get();
        $userId_list = $this->Company_model->getChildUsers($data['user_id']);
        $userIds = array();
        foreach($userId_list as $user)
            array_push($userIds,$user['user_id']);
        $user_list = '';
        if($userIds)
            $user_list = $this->Dashboard_model->getChildUserListById($userIds);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$user_list);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function loanOfficerDashboardDetails_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $userDetails = $this->Dashboard_model->getDashboardLoanOfficerDetails($data['user_id']);
        $outstandingDate = $this->Dashboard_model->getLatestOutstandingDate();
        $userId_list = $this->Company_model->getChildUsers($data['user_id']);
        $userIds = array();
        array_push($userIds,$data['user_id']);
        foreach($userId_list as $user)
            array_push($userIds,$user['user_id']);
        //$customers = $this->Dashboard_model->getDashboardTotalCustomers($userIds,$outstandingDate);

        $month = date("m", strtotime(currentDate()));
        $year = date("Y", strtotime(currentDate()));
        if($month%3 != 0)
        {
            $month = $month - ($month%3);
            $latest_qtr_month= date("Y-m-d",strtotime($year."-".$month));
        }
        else
        {
            $latest_qtr_month= date("Y-m-d", strtotime(currentDate()));
        }

        $latestData = $this->Dashboard_model->getDashboardTotalCustomersAndExposure($data['user_id'],$latest_qtr_month);
        $customers = $latestData['customers'];

        $previous_date = date('Y-m-d', strtotime($latest_qtr_month." -3 month"));
        $previousData = $this->Dashboard_model->getDashboardTotalCustomersAndExposure($data['user_id'],$previous_date);


        $current_pipeline = $this->Dashboard_model->getDashboardPipelineData($data['user_id'],currentDate());
        $previous_pipeline_date = date('Y-m-d', strtotime(currentDate()." -1 month"));
        $previous_pipeline = $this->Dashboard_model->getDashboardPipelineData($data['user_id'],$previous_pipeline_date);

        $userDetails['branch_logo'] =  getImageUrl($userDetails['branch_logo'],'company');
        $userDetails['profile_image'] =  getImageUrl($userDetails['profile_image'],'profile');

        $dashboardResponse['userDetails'] = $userDetails;
        $dashboardResponse['customers'] = $latestData['customers'];
        $dashboardResponse['customers'] = $latestData['customers'];
        $dashboardResponse['latestExposure'] = $latestData['exposure'];
        $dashboardResponse['previousExposure'] = $previousData['exposure'];
        $dashboardResponse['latestPipeline'] = $current_pipeline['pipeline'];
        $dashboardResponse['previousPipeline'] = $previous_pipeline['pipeline'];
        //print_r($dashboardResponse);exit;
        /*$current = $this->Dashboard_model->getLoanOfficerDashboardDetails($data['user_id'],currentDate());
        for($s=0;$s<count($current);$s++)
        {
            $current[$s]['branch_logo'] =  getImageUrl($current[$s]['branch_logo'],'company');
            $current[$s]['profile_image'] =  getImageUrl($current[$s]['profile_image'],'profile');
        }
        $previous_date = date('Y-m-d', strtotime(currentDate()." -1 month"));
        $previous = $this->Dashboard_model->getLoanOfficerDashboardDetails($data['user_id'],$previous_date);
        for($s=0;$s<count($previous);$s++)
        {
            $previous[$s]['branch_logo'] =  getImageUrl($previous[$s]['branch_logo'],'company');
            $previous[$s]['profile_image'] =  getImageUrl($previous[$s]['profile_image'],'profile');
        }*/
        //$result['current']=$current;
        //$result['previous']=$previous;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$dashboardResponse);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function loanOfficerDashboardStageAmountDetails_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        //$this->form_validator->add_rules('startDate', array('required'=> $this->lang->line('startDate_req')));
        //$this->form_validator->add_rules('endDate', array('required'=> $this->lang->line('endDate_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $response = [];
        $StageAmountresult = [];

        $month = date("m", strtotime(currentDate()));
        $year = date("Y", strtotime(currentDate()));
        if($month%3 != 0)
        {
            $month = $month - ($month%3);
            $latest_qtr_month= date("Y-m-d",strtotime($year."-".$month));
        }
        else
        {
            $latest_qtr_month= date("Y-m-d", strtotime(currentDate()));
        }
        for($i=12;$i>=0;$i-=3)
        {
            $data['stagingDate']=date("Y-m-d", strtotime($latest_qtr_month.'-'.$i.' month'));
            $stagingMonth=date("M,Y", strtotime($latest_qtr_month.'-'.$i.' month'));
            $StageAmountresult[$stagingMonth] = $this->Dashboard_model->DashboardStageAmountGraphData($data);
        }
        /*for($i=12;$i>=0;$i--)
        {
            $data['stagingDate']=date("Y-m-d", strtotime(currentDate().'-'.$i.' month'));
            $stagingMonth=date("M,Y", strtotime(currentDate().'-'.$i.' month'));
            $StageAmountresult[$stagingMonth] = $this->Dashboard_model->DashboardStageAmountGraphData($data);
        }*/
        $response['Graph'] = $StageAmountresult;
        $data['currentDate']=date("Y-m-d", strtotime(currentDate()));
        //$response['Details'] = $this->Dashboard_model->getLoanOfficerDashboardStageAmountData($data);

        $data['outstandingDate'] = $this->Dashboard_model->getLatestOutstandingDate();
        $response['Details'] = $this->Dashboard_model->getLoanOfficerDashboardStageAmountData($data);
        if($response['Details'][0]['preapprovals'] == null || $response['Details'][0]['preapprovals'] == '')
            $response['Details'][0]['preapprovals']=0;
        if($response['Details'][0]['approvals'] == null || $response['Details'][0]['approvals'] == '')
            $response['Details'][0]['approvals']=0;
        if($response['Details'][0]['disbursements'] == null || $response['Details'][0]['disbursements'] == '')
            $response['Details'][0]['disbursements']=0;
        if($response['Details'][0]['collections'] == null || $response['Details'][0]['collections'] == '')
            $response['Details'][0]['collections']=0;

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$response);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function loanOfficerDashboardSectorWiseLoans_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $response = $this->Dashboard_model->getLoanOfficerDashboardSectorWiseLoans($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$response);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function loanOfficerDashboardCollections_get()
    {
        //getUserCollections
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $response = $this->Dashboard_model->getUserCollections($data,currentDate());
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$response);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function loanOfficerDashboardMapData_get()
    {
        //getProjectCountryList
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['outstandingDate'] = $this->Dashboard_model->getLatestOutstandingDate();
        $response = $this->Dashboard_model->getProjectCountryList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$response);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function dashboardMapCountryWiseProjectList_get()
    {
        $data = $this->input->get();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('country_id', array('required'=> $this->lang->line('country_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['outstandingDate'] = $this->Dashboard_model->getLatestOutstandingDate();
        $data['agingDate'] = $this->Dashboard_model->getLatestAgingDate();
        $response = $this->Dashboard_model->getCountryWiseProjectList($data);
        $result_data = [];
        $aging_status = ['NORM','WATC','SUBS','DOUB','LOSS'];
        for($r=0;$r<count($response);$r++)
        {
            if(!isset($result_data[$response[$r]['id_crm_company']]))
                $result_data[$response[$r]['id_crm_company']] = $response[$r];
            if($response[$r]['field_name'] == 'company_latitude')
            {
                $result_data[$response[$r]['id_crm_company']]['latitude'] = $response[$r]['form_field_value'];
            }
            if($response[$r]['field_name'] == 'company_langitude')
            {
                $result_data[$response[$r]['id_crm_company']]['longitude'] = $response[$r]['form_field_value'];
            }
            if($response[$r]['cp_status'])
            {
                $status_pos = -1;
                $ag_status = '';
                $loan_status = explode(',',$response[$r]['cp_status']);
                for($s=0;$s<count($loan_status);$s++)
                {
                    $key = array_search($loan_status[$s], $aging_status);
                    if($key > $status_pos)
                    {
                        $status_pos = $key;
                        $ag_status = $loan_status[$s];
                    }
                }
                $result_data[$response[$r]['id_crm_company']]['cp_status'] = $ag_status;
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function dashboardNotifications_get()
    {
        $data = $this->input->get();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $filters = array('alert','meeting','task','notification');
        $filterCount = [];
        for($fl=0;$fl<count($filters);$fl++)
        {
            $filterCount[$filters[$fl]] = $this->Dashboard_model->getNotificationsCountByFilter(array(
                'user_id' => $data['user_id'],
                'filter' => $filters[$fl]
            ));
            $filterCount[$filters[$fl]]  = $filterCount[$filters[$fl]][0]['total'];
        }

        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $my_companies = $this->Crm_model->getCompanyListCount($data);
        $filterCount['companies'] = $my_companies[0]['total'];

        if(isset($created_by)){ $data['created_by'] = $created_by; }
        else if(isset($_SERVER['HTTP_USER'])){  $data['created_by'] = $_SERVER['HTTP_USER']; }
        $my_contacts = $this->Crm_model->getContactListCount($data);
        $filterCount['contacts'] = $my_contacts[0]['total'];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$filterCount);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function dashboardWidgetsData_get()
    {

    }
}
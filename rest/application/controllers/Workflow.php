<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Workflow extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('Project_model');
        $this->load->model('Workflow_model');
        $this->load->model('Notification_model');
        $this->load->model('User_model');
        $this->load->model('Activity_model');
    }

    public function workflowPhase_get()
    {
        $data = $this->input->get();
        $result = $this->Workflow_model->getWorkflowPhase($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function actions_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $guestApprovalUser = false;
        $guestApprovalRoleDetails = $this->Company_model->getApplicationUserRoleApprovalRole(['user_id' => $data['user_id'], 'application_role_id' => 2]);
        if(!empty($guestApprovalRoleDetails)) $guestApprovalUser = true;

        $request_data = ['project_stage_info_id' => $data['project_stage_info_id'],'status' => 'pending'];
        if(!$guestApprovalUser) $request_data['assigned_to'] = $data['user_id'];
        $result_array = array();
        $project_stage_phases = $this->Workflow_model->getProjectStageWorkflowPhaseDetails($request_data);
        $project_stage_current_phases = [];
        for($p =0; $p <count($project_stage_phases); $p++)
        {
            $project_stage_current_phases[$p]['id_project_stage_workflow_phase'] = $project_stage_phases[$p]['id_project_stage_workflow_phase'];
            $project_stage_current_phases[$p]['workflow_phase_id'] = $project_stage_phases[$p]['workflow_phase_id'];
            $project_stage_current_phases[$p]['workflow_phase_name'] = $project_stage_phases[$p]['workflow_phase_name'];
            $phase_actions = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id' =>$project_stage_current_phases[$p]['workflow_phase_id']));
            $result_array = [];
            for($s=0;$s<count($phase_actions);$s++)
            {
                $result_array[] = array(
                    'id_workflow_phase_action' => $phase_actions[$s]['id_workflow_phase_action'],
                    'id_project_stage_workflow_phase' => $project_stage_phases[$p]['id_project_stage_workflow_phase'],
                    'workflow_phase_id' => $phase_actions[$s]['workflow_phase_id'],
                    'workflow_phase_activity_id' => $phase_actions[$s]['workflow_phase_activity_id'],
                    'id_workflow_action' => $phase_actions[$s]['id_workflow_action'],
                    'workflow_action_name' => $phase_actions[$s]['workflow_action_name'],
                    'workflow_action_key' => $phase_actions[$s]['workflow_action_key'],
                    'project_stage_info_id' => $data['project_stage_info_id'],
                    'parent_workflow_phase_id' => $phase_actions[$s]['parent_workflow_phase_id'],
                    'is_approve' => $phase_actions[$s]['is_approve'],
                );
            }
            $project_stage_current_phases[$p]['actions'] = $result_array;

        }
       /* echo "<pre>"; print_r($project_stage_current_phases); exit;
        if(!empty($project_stage_details))
        {
            $phase_actions = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id' => $project_stage_phases[0]['workflow_phase_id']));
//echo "<pre>"; print_r($phase_actions); exit;
            for($s=0;$s<count($phase_actions);$s++)
            {
                $result_array[] = array(
                    'id_workflow_phase_action' => $phase_actions[$s]['id_workflow_phase_action'],
                    'workflow_phase_id' => $phase_actions[$s]['workflow_phase_id'],
                    'workflow_phase_activity_id' => $phase_actions[$s]['workflow_phase_activity_id'],
                    'id_workflow_action' => $phase_actions[$s]['id_workflow_action'],
                    'workflow_action_name' => $phase_actions[$s]['workflow_action_name'],
                    'workflow_action_key' => $phase_actions[$s]['workflow_action_key'],
                    'project_stage_info_id' => $data['project_stage_info_id'],
                    'parent_workflow_phase_id' => $phase_actions[$s]['parent_workflow_phase_id'],
                );
            }
        }
echo "<pre>"; print_r($result_array); exit;*/

       /* echo '<pre>';
        print_r($project_stage_current_phases);
        exit;*/

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$project_stage_current_phases);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function actionDetails_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('workflow_phase_id', array('required'=> $this->lang->line('workflow_phase_id_req')));
        $this->form_validator->add_rules('id_workflow_phase_action', array('required'=> $this->lang->line('id_workflow_phase_action_req')));
        $this->form_validator->add_rules('workflow_phase_activity_id', array('required'=> $this->lang->line('workflow_phase_activity_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $workflow_phase_details = $this->Workflow_model->getWorkflowPhase(array('workflow_phase_id' => $data['workflow_phase_activity_id']));
        $result_users = array();
        if(!empty($workflow_phase_details) && $workflow_phase_details[0]['is_sub_phase']==1){
            $workflow_phase = $this->Workflow_model->getWorkflowPhase(array('parent_workflow_phase_id' => $data['workflow_phase_activity_id']));
        }
        else{
            $workflow_phase = $this->Workflow_model->getWorkflowPhase(array('workflow_phase_id' => $data['workflow_phase_activity_id']));
        }
        $getWorkflowPhaseAction=$this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id'=>$data['workflow_phase_id'],'workflow_action_id'=>$data['id_workflow_phase_action']));
        $next_stage_assigning=array();
        $counter=0;
        foreach($getWorkflowPhaseAction as $kg=>$vg){
            if($vg['next_project_stage_id']!=NULL && trim($vg['next_project_stage_id'])!='' && $vg['next_project_stage_id']>0) {
                if ($vg['next_project_stage_assign_type'] == 'Role') {
                    $assigned_to = array();
                    $assigned_to[] = $vg['next_project_stage_assign_to'];
                    $users = $this->Workflow_model->getUsersByApprovalRole(array('approval_role_array' => $assigned_to));
                } else if ($vg['next_project_stage_assign_type'] == 'User') {
                    $assigned_to = array();
                    $assigned_to[] = $vg['next_project_stage_assign_to'];
                    $users = $this->Workflow_model->getUsersByApprovalUser(array('approval_role_array' => $assigned_to));
                } else {
                    $assigned_to = array();
                    $project_details = $this->Project_model->getProject(array('crm_project_id' => $vg['project_id']));
                    $assigned_to[] = $project_details[0]['created_by'];
                    $users = $this->Workflow_model->getUsersByApprovalUser(array('approval_role_array' => $assigned_to));
                }

                $next_stage_assigning['users'] = $users;
                $next_stage_assigning['next_project_stage_assign_type'] = $vg['next_project_stage_assign_type'];
                $next_stage_assigning['stage_details'] = $this->Crm_model->getProjectStages(array('id_project_stage' => $vg['next_project_stage_id']));
                $counter = $counter + 1;
            }
        }
        if($data['workflow_phase_activity_id']!=0)
        {
            for($s=0;$s<count($workflow_phase);$s++)
            {
                $roles = $this->Workflow_model->getWorkflowPhaseRoels(array('workflow_phase_id' => $workflow_phase[$s]['id_workflow_phase']));
                $role_ids = array_map(function ($i) {
                    return $i['approval_role_id'];
                }, $roles);

                if($workflow_phase[$s]['workflow_approval_type']=='User' || $workflow_phase[$s]['workflow_approval_type']=='Committee'){
                    $users = $this->Workflow_model->getUsersByApprovalUser(array('approval_role_array' => $role_ids));
                }
                else {
                    $users = $this->Workflow_model->getUsersByApprovalRole(array('approval_role_array' => $role_ids));
                }

                $result_users[] = array(
                    'phase_name' => $workflow_phase[$s]['workflow_phase_name'],
                    'workflow_phase_id' => $workflow_phase[$s]['id_workflow_phase'],
                    'users' => $users,
                    'workflow_approval_type' => $workflow_phase[$s]['workflow_approval_type']
                );
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result_users,'next_stage_assigning'=>$next_stage_assigning);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function actions_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('project_stage_id', array('required'=> $this->lang->line('project_stage_id_req')));
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('workflow_phase_action_id', array('required'=> $this->lang->line('id_workflow_phase_action_req')));
        $this->form_validator->add_rules('id_project_stage_workflow_phase', array('required'=> $this->lang->line('project_stage_workflow_phase_id_req')));
        $this->form_validator->add_rules('forward_by', array('required'=> $this->lang->line('forward_by_req')));
        $this->form_validator->add_rules('workflow_comments', array('required'=> $this->lang->line('workflow_comments_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_stage_info_details = $this->Project_model->getProjectStageInfo(array('id_project_stage_info' => $data['project_stage_info_id']));
        if(!empty($project_stage_info_details) && $project_stage_info_details[0]['project_stage_status'] == 'approved')
        {
            $result = array('status'=>TRUE, 'message' => $this->lang->line('action_applied_suc'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_project_stage_workflow_phase'])) $this->Workflow_model->updateProjectStageWorkflowPhase(array('id_project_stage_workflow_phase' => $data['id_project_stage_workflow_phase'],'status' => 'completed','updated_date_time' => currentDate()));
        $phase_action_details = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_action_id' => $data['workflow_phase_action_id']));

        /*if(!empty($phase_action_details) && isset($data['flag']) && $data['flag']==1 && ( $phase_action_details[0]['is_approve']==1 || $phase_action_details[0]['is_approve']==2))
        {
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $sub_sector_limit_exceeds_check = $facility_amounts = $sector_limit_exceeds = $sector_limits = $sub_sector_limits = $limit_value = $total_exposure = $sub_sector_exposure = $sector_exposure = 0;
            //sum of facilities amount
            if(isset($data['facility_id']) && !empty($data['facility_id']))
                $facility_amounts = array_sum(array_map(function($i){ return $i['facility_usd_amount']; },$data['facility_id']));
            //sub sector limit
            if($project_details[0]['project_sub_sector_id']) {
                $sub_sector_limits = $this->Company_model->getSectorLimit(array('company_id' => $project_details[0]['company_id'], 'sector_id' => $project_details[0]['project_sub_sector_id']));
                $limit_value = array_sum(array_map(function($i){ return $i['limit_value']; },$sub_sector_limits));
                $sub_sector_limits = ($limit_value/count($sub_sector_limits));
            }
            //sector limit
            if($project_details[0]['project_main_sector_id']) {
                $sector_limits = $this->Company_model->getSectorLimit(array('company_id' => $project_details[0]['company_id'], 'sector_id' => $project_details[0]['project_main_sector_id']));
                $limit_value = array_sum(array_map(function($i){ return $i['limit_value']; },$sector_limits));
                $sector_limits = ($limit_value/count($sector_limits));
            }
            // getting total_exposure
            $date = date('m-Y');
            $total_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date));
            $total_exposure = $total_exposure[0]['amount'];

            //getting sub sector outstanding and comparing with sector limit
            if($project_details[0]['project_sub_sector_id']) {
                $sub_sector_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date, 'sub_sector_id' => $project_details[0]['project_sub_sector_id']));
                $sub_sector_exposure = $sub_sector_exposure[0]['amount'];

                $outstanding_percentage = ($total_exposure/($sub_sector_exposure+$facility_amounts))*100;
                if($sub_sector_limits>$outstanding_percentage){
                    $sub_sector_limit_exceeds_check = 1;
                }
            }
            //getting sector outstanding and comparing with sector limit
            if($sub_sector_limit_exceeds_check){
                $sector_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date,'sector_id' => $project_details[0]['project_main_sector_id']));
                $sector_exposure = $sector_exposure[0]['amount'];
                $outstanding_percentage = ($total_exposure/($sector_exposure+$facility_amounts))*100;
                if($sector_limits>$outstanding_percentage){
                    $sector_limit_exceeds = 1;
                }
            }
            //sending message if limit is exceeds
            if(!$sector_limit_exceeds){
                $result = array('status'=>TRUE, 'error' => $this->lang->line('sector_limit_exceeds'), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }*/
        if(isset($data['facility_id']) && !empty($data['facility_id']) && !empty($phase_action_details) && isset($data['flag']) && $data['flag']==1 && ( $phase_action_details[0]['is_approve']==1 || $phase_action_details[0]['is_approve']==2))
        {
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $sub_sector_limit_exceeds_check = $facility_amounts = $sector_limit_exceeds = $sector_limits = $sub_sector_limits = $limit_value = $total_exposure = $sub_sector_exposure = $sector_exposure = 0;
            //sum of facilities amount
            if(isset($data['facility_id']) && !empty($data['facility_id']))
                $facility_amounts = array_sum(array_map(function($i){ return isset($i['facility_usd_amount']) ? $i['facility_usd_amount'] : $i['facility_amount']; },$data['facility_id']));
            //sub sector limit
            if($project_details[0]['project_sub_sector_id']) {
                $sub_sector_limits = $this->Company_model->getSectorLimit(array('company_id' => $project_details[0]['company_id'], 'sector_id' => $project_details[0]['project_sub_sector_id']));
                $limit_value = array_sum(array_map(function($i){ return $i['limit_value']; },$sub_sector_limits));
                if(count($sub_sector_limits) > 0)
                    $sub_sector_limits = ($limit_value/count($sub_sector_limits));
            }
            //sector limit
            if($project_details[0]['project_main_sector_id']) {
                $sector_limits = $this->Company_model->getSectorLimit(array('company_id' => $project_details[0]['company_id'], 'sector_id' => $project_details[0]['project_main_sector_id']));
                $limit_value = array_sum(array_map(function($i){ return $i['limit_value']; },$sector_limits));
                if(count($sector_limits) > 0 )
                    $sector_limits = ($limit_value/count($sector_limits));
            }
            // getting total_exposure
            $date = date('m-Y');
            $total_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date));
            $total_exposure = $total_exposure[0]['amount'];

            //getting sub sector outstanding and comparing with sector limit
            if($project_details[0]['project_sub_sector_id']) {
                $sub_sector_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date, 'sub_sector_id' => $project_details[0]['project_sub_sector_id']));
                $sub_sector_exposure = $sub_sector_exposure[0]['amount'];

                $outstanding_percentage = ($total_exposure/($sub_sector_exposure+$facility_amounts))*100;
                if($sub_sector_limits>$outstanding_percentage){
                    $sub_sector_limit_exceeds_check = 1;
                }
            }
            //getting sector outstanding and comparing with sector limit
            if($sub_sector_limit_exceeds_check){
                $sector_exposure = $this->Workflow_model->getOutstandingExposure(array('company_id' => $project_details[0]['company_id'],'date' => $date,'sector_id' => $project_details[0]['project_main_sector_id']));
                $sector_exposure = $sector_exposure[0]['amount'];
                $outstanding_percentage = ($total_exposure/($sector_exposure+$facility_amounts))*100;
                if($sector_limits>$outstanding_percentage){
                    $sector_limit_exceeds = 1;
                }
            }
            //sending message if limit is exceeds
            if(!$sector_limit_exceeds){
                $result = array('status'=>TRUE, 'error' => $this->lang->line('sector_limit_exceeds'), 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $parent_workflow_phase_id = 0;
        $change_stage=true;
        $current_phase_details=$this->Workflow_model->getWorkflowPhaseDetails(['workflow_phase_id' => $phase_action_details[0]['workflow_phase_id']]);
        $response_param['current_phase_details']=$current_phase_details;
        $response_param['phase_action_details_main']=$phase_action_details;
        if($current_phase_details[0]['workflow_approval_type']=='Committee'){

            if($current_phase_details[0]['workflow_approval_process']=='Parallel') {
                $pending_commitee_users1 = $this->Workflow_model->getWorkFlowCommiteeUsers(array('workflow_phase_id' => $current_phase_details[0]['id_workflow_phase'], 'status' => 'pending'));
                foreach($pending_commitee_users1 as $k1=>$v1){
                    $this->Workflow_model->updateProjectStageWorkflowPhase(array('id_project_stage_workflow_phase' => $v1['id_project_stage_workflow_phase'],'status' => 'completed','updated_date_time' => currentDate()));
                }
            }
            if($current_phase_details[0]['workflow_approval_process']=='Sequential') {
                $current_commitee_users = $this->Workflow_model->getWorkFlowCommiteeUsers(array('workflow_phase_id' => $current_phase_details[0]['id_workflow_phase']));
                if (count($current_commitee_users) > 0) {
                    $phase_action_details[0]['workflow_phase_activity_id']=$current_phase_details[0]['id_workflow_phase'];
                }
            }
        }
        if($phase_action_details[0]['workflow_phase_activity_id'] > 0)
        {
            $next_phase_details = $this->Workflow_model->getWorkflowPhaseDetails(['workflow_phase_id' => $phase_action_details[0]['workflow_phase_activity_id']]);
            $response_param['next_phase_details']=$next_phase_details;
            if(!empty($next_phase_details) && isset($next_phase_details[0]['workflow_approval_type']))
            {
                if($next_phase_details[0]['workflow_approval_type']=='Committee'){
                        $commitee_users = $this->Workflow_model->getWorkFlowCommiteeUsers(array('workflow_phase_id' => $next_phase_details[0]['id_workflow_phase']));
                    $response_param['commitee_users']=$commitee_users;
                    if (count($commitee_users) > 0) {
                            $inc = 0;
                            $data['forward_to'] = array();
                            if($next_phase_details[0]['workflow_approval_process']=='Parallel') {
                                foreach ($commitee_users as $kc => $vc) {
                                    $data['forward_to'][$inc] = ['workflow_phase_id' => $next_phase_details[0]['id_workflow_phase'], 'user_id' => $vc['approval_role_id']];
                                    $inc=$inc+1;
                                    $response_param['forward_to_loop']=$data['forward_to'];
                                }
                            }
                            if($next_phase_details[0]['workflow_approval_process']=='Sequential') {
                                $data['forward_to'][$inc] = ['workflow_phase_id' => $next_phase_details[0]['id_workflow_phase'], 'user_id' => $commitee_users[0]['approval_role_id']];
                            }
                        }

                }
            }
            if(!empty($next_phase_details) && isset($next_phase_details[0]['is_sub_phase']) && $next_phase_details[0]['is_sub_phase']==1)
                $parent_workflow_phase_id = $phase_action_details[0]['workflow_phase_activity_id'];
        }

        if(isset($data['forward_to']) && !empty($data['forward_to']))
        {
            $response_param['forward_to']=$data['forward_to'];
            for($s=0;$s<count($data['forward_to']);$s++)
            {
                $project_stage_workflow_phase_id = $this->Workflow_model->addProjectStageWorkflowPhase(array(
                    'workflow_phase_id' => $data['forward_to'][$s]['workflow_phase_id'],
                    'assigned_to' => $data['forward_to'][$s]['user_id'],
                    'project_stage_info_id' => $data['project_stage_info_id'],
                    'status' => 'pending',
                    'created_date_time' => currentDate(),
                    'parent_workflow_phase_id' => $parent_workflow_phase_id
                ));

                $stage_workflow_data = array(
                    'project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],
                    'workflow_phase_action_id' => $data['workflow_phase_action_id'],
                    'forward_by' => $data['forward_by'],
                    'forward_to' => $data['forward_to'][$s]['user_id'],
                    'workflow_comments' => $data['workflow_comments'],
                    'forward_to_workflow_phase_id' => $data['forward_to'][$s]['workflow_phase_id'],
                    'created_date_time' => currentDate()
                );
                $project_stage_workflow_id = $this->Workflow_model->addProjectStageWorkflow($stage_workflow_data);

                //notifications and mails
                $project_stage_workflow_content = $this->Workflow_model->getProjectStageWorkflowDetails(array('id_project_stage_workflow' => $project_stage_workflow_id));
                //echo "<pre>"; print_r($project_stage_workflow_content); exit;
                if(isset($project_stage_workflow_content[0]['action']) && $project_stage_workflow_content[0]['action']!='approved') {
                    $project_details = $this->Crm_model->getProject($data['project_id']);
                    $project_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
                    $company_user = $this->Company_model->getCompanyUser($data['forward_to'][$s]['user_id']);
                    $company_user1 = $this->Company_model->getCompanyUser($data['forward_by']);
                    //$company = $this->Company_model->getCompany(array('id_company' => $project_details[0]['company_id']));
                    if ($project_stage_workflow_content[0]['to_user_name'] != '') {
                        $link = '<a style="color: #22bcf2" class="sky-blue" href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">here</a>';
                        $notification_message = str_replace(array('{project_title}', '{stage}', '{action}', '{assigned_user}', '{assigned_user_department}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], $project_stage_workflow_content[0]['from_user_name'], $company_user1->approval_name, date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_message'));
                        $mail_message = str_replace(array('{project_title}', '{stage}', '{action}', '{assigned_user}', '{assigned_user_department}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], $project_stage_workflow_content[0]['from_user_name'], $company_user1->approval_name, date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_mail'));
                        //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded to you by ' . $project_stage_workflow_content[0]['to_user_name'] . ' on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                    } else {
                        $link = '<a style="color: #22bcf2" class="sky-blue" href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">here</a>';
                        $notification_message = str_replace(array('{project_title}', '{stage}', '{action}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_message1'));
                        $mail_message = str_replace(array('{project_title}', '{stage}', '{action}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_mail1'));
                        //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded.  on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                    }

                    $link = '<a style="color: #22bcf2" class="sky-blue" href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">here</a>';
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'to_username' => $project_stage_workflow_content[0]['to_user_name'],
                        'message' => $mail_message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $subject = str_replace(array('{project_title}', '{assigned_user}'), array($project_details[0]['project_title'], $project_stage_workflow_content[0]['to_user_name']), $this->lang->line('workflow_mail_subject'));
                    $template_data = $this->parser->parse('templates/notification.html', $template_data);
                    //$project_stage_workflow_content[0]['to_email']
                    saveSendMail($project_stage_workflow_content[0]['to_email'], $subject, $template_data);

                    $this->Notification_model->addNotification(array(
                        'assigned_by' => $data['forward_by'],
                        'assigned_to' => $project_stage_workflow_content[0]['forward_to'],
                        'module_reference_id' => $project_stage_workflow_content[0]['id_project_stage_workflow'],
                        'module_reference_type' => 'workflow',
                        'notification_subject' => $project_details[0]['project_title'],
                        'notification_template' => $notification_message,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'workflow',
                        'created_date_time' => currentDate(),
                        'module_id' => $project_details[0]['id_crm_project'],
                        'module_type' => 'project'
                    ));
                }
            }
        }
        else
        {
            $forward_user = 0;
            $forward_phase_id = 0;
            if($phase_action_details[0]['parent_workflow_phase_id'] > 0)
            {
                $phase_stage_workflow_details = $this->Workflow_model->getPhaseStageWorkflowDetails(['workflow_phase_id' => $phase_action_details[0]['workflow_phase_id'],'project_stage_info_id' => $data['project_stage_info_id'],'status' => 'completed']);
                if(!empty($phase_stage_workflow_details))
                {
                    $forward_user = $phase_stage_workflow_details[0]['forward_by'];
                    $forward_phase_id = $phase_stage_workflow_details[0]['workflow_phase_id'];
                }
                $phase_workflow_status = $this->Workflow_model->getPhaseStageWorkflowStatus(['workflow_phase_id' => $phase_action_details[0]['parent_workflow_phase_id'],'project_stage_info_id' => $data['project_stage_info_id'],'status' => 'pending']);
                if(empty($phase_workflow_status))
                {
                    $phase_action_details = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id' => $phase_action_details[0]['parent_workflow_phase_id']));
                    $project_stage_workflow_phase_id = $this->Workflow_model->addProjectStageWorkflowPhase(array(
                        'workflow_phase_id' => $phase_action_details[0]['workflow_phase_activity_id'],
                        'assigned_to' => $forward_user,
                        'project_stage_info_id' => $data['project_stage_info_id'],
                        'status' => 'pending',
                        'created_date_time' => currentDate(),
                        'parent_workflow_phase_id' => $parent_workflow_phase_id
                    ));
                }

            }
            $stage_workflow_data = array(
                'project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],
                'workflow_phase_action_id' => $data['workflow_phase_action_id'],
                'forward_by' => $data['forward_by'],
                'forward_to' => $forward_user,
                'workflow_comments' => $data['workflow_comments'],
                'forward_to_workflow_phase_id' => $forward_phase_id,
                'created_date_time' => currentDate()
            );
            $project_stage_workflow_id = $this->Workflow_model->addProjectStageWorkflow($stage_workflow_data);

            //notifications and mails
            $project_stage_workflow_content = $this->Workflow_model->getProjectStageWorkflowDetails(array('id_project_stage_workflow' => $project_stage_workflow_id));
            //echo "<pre>"; print_r($project_stage_workflow_content); exit;
            if(isset($project_stage_workflow_content[0]['action']) && $project_stage_workflow_content[0]['action']!='approved') {

                $project_details = $this->Crm_model->getProject($data['project_id']);
                $project_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
                $company_user = $this->Company_model->getCompanyUser($project_stage_workflow_content[0]['forward_to']);

                if ($project_stage_workflow_content[0]['to_user_name'] != '') {
                    $link = '<a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">here</a>';
                    $notification_message = str_replace(array('{project_title}', '{stage}', '{action}', '{assigned_user}', '{assigned_user_department}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], $project_stage_workflow_content[0]['to_user_name'], $company_user->approval_name, date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_message'));
                    $mail_message = str_replace(array('{project_title}', '{stage}', '{action}', '{assigned_user}', '{assigned_user_department}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], $project_stage_workflow_content[0]['to_user_name'], $company_user->approval_name, date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_mail'));
                } else {
                    $link = '<a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">here</a>';
                    $notification_message = str_replace(array('{project_title}', '{stage}', '{action}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_message1'));
                    $mail_message = str_replace(array('{project_title}', '{stage}', '{action}', '{date_time}', '{here}'), array($project_details[0]['project_title'], $project_stage[0]['stage_name'], $project_stage_workflow_content[0]['workflow_action_key'], date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])), $link), $this->lang->line('workflow_notification_mail1'));
                }

                $link = WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']);
                $template_data = array(
                    'web_base_url' => WEB_BASE_URL,
                    'to_username' => $project_stage_workflow_content[0]['to_user_name'],
                    'message' => $mail_message,
                    'mail_footer' => $this->lang->line('mail_footer')
                );
                $subject = str_replace(array('{project_title}', '{assigned_user}'), array($project_details[0]['project_title'], $project_stage_workflow_content[0]['to_user_name']), $this->lang->line('workflow_mail_subject'));
                $template_data = $this->parser->parse('templates/notification.html', $template_data);
                saveSendMail($project_stage_workflow_content[0]['to_email'], $subject, $template_data);

                $this->Notification_model->addNotification(array(
                    'assigned_by' => $data['forward_by'],
                    'assigned_to' => $project_stage_workflow_content[0]['forward_to'],
                    'module_reference_id' => $project_stage_workflow_content[0]['id_project_stage_workflow'],
                    'module_reference_type' => 'workflow',
                    'notification_subject' => $project_details[0]['project_title'],
                    'notification_template' => $notification_message,
                    'notification_link' => $link,
                    'notification_comments' => '',
                    'notification_type' => 'workflow',
                    'created_date_time' => currentDate(),
                    'module_id' => $project_details[0]['id_crm_project'],
                    'module_type' => 'project'
                ));
            }
        }

        $pending_commitee_users = $this->Workflow_model->getWorkFlowCommiteeUsers(array('workflow_phase_id' => $current_phase_details[0]['id_workflow_phase'], 'status' => 'pending'));
        $response_param['pending_commitee_users']=$pending_commitee_users;
        $response_param['current_phase_details']=$current_phase_details;
        $response_param['pending_commitee_users_count']=count($pending_commitee_users);
        $response_param['phase_action_details']=$phase_action_details;
        if (count($pending_commitee_users) > 0) {
            $change_stage = false;
        }
        if(!empty($phase_action_details) && ( $phase_action_details[0]['is_approve']==1 || $phase_action_details[0]['is_approve']==2) && $change_stage===true && ($phase_action_details[0]['workflow_phase_activity_id']==NULL || $phase_action_details[0]['workflow_phase_activity_id']==0 || $phase_action_details[0]['workflow_phase_activity_id']==''))
        {
            $this->Workflow_model->updateProjectStageInfo(array(
                'id_project_stage_info' => $data['project_stage_info_id'],
                'project_stage_status' => 'approved',
                'end_date_time' => currentDate()
            ));
            $project_stage_info_details = $this->Project_model->getProjectStageInfo(array('project_id' => $data['project_id'],'stage_id' => $data['project_stage_id']));
            if(!empty($project_stage_info_details) && isset($project_stage_info_details[0]['next_project_stage_id']) && $project_stage_info_details[0]['next_project_stage_id'] > 0 ){
                $next_stage_user_id=$project_stage_info_details[0]['created_by'];
                if(isset($data['next_stage_assigning_to_user_id']) && $data['next_stage_assigning_to_user_id']!=NULL && trim($data['next_stage_assigning_to_user_id'])!='' && $data['next_stage_assigning_to_user_id']>0){
                    $next_stage_user_id=$data['next_stage_assigning_to_user_id'];
                }
                $this->initializeNextStage(array('project_id' => $data['project_id'],'project_stage_id' => $data['project_stage_id'],'project_stage_info_id' => $project_stage_info_details[0]['id_project_stage_info'],'user_id' => $project_stage_info_details[0]['created_by'],'next_stage_user_id'=>$next_stage_user_id));
            }
            else
                $this->Crm_model->updateProject(['project_stage_info_id' => $data['project_stage_info_id'],'project_status' => $project_stage_info_details[0]['project_stage_complete_status']],$data['project_id']);

            //facilities update for next stage
        }

        //uploading attachments
        $company_id = $data['company_id'];
        $path='uploads/'; $data['document_type'] = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['id_project_stage_workflow_phase'], 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['forward_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'stage_comments');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['id_project_stage_workflow_phase'], 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['forward_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'stage_comments');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }


        if(isset($data['facility_id']) && !empty($phase_action_details) && ($phase_action_details[0]['is_approve']==1 || $phase_action_details[0]['is_approve']==2))
        {
            for($f=0;$f<count($data['facility_id']);$f++)
            {
                if($data['facility_id'][$f]['project_facility_status']=='rejected')
                    $this->Crm_model->updateFacilities(array('project_facility_status' => $data['facility_id'][$f]['project_facility_status'],'id_project_facility' => $data['facility_id'][$f]['id_project_facility']));
                else
                    $this->Crm_model->updateFacilities(array('project_facility_status' => $data['facility_id'][$f]['project_facility_status'],'facility_amount' => $data['facility_id'][$f]['facility_amount'],'facility_usd_amount' => isset($data['facility_id'][$f]['facility_usd_amount'])? $data['facility_id'][$f]['facility_usd_amount']: $data['facility_id'][$f]['facility_amount'],'id_project_facility' => $data['facility_id'][$f]['id_project_facility']));

                $this->Workflow_model->addProjectStageWorkflowFacility(array(
                    'project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],
                    'project_facility_id' => $data['facility_id'][$f]['id_project_facility'],
                    'amount' => $data['facility_id'][$f]['facility_amount'],
                    'usd_equal_amount' => isset($data['facility_id'][$f]['facility_usd_amount']) ? $data['facility_id'][$f]['facility_usd_amount'] : $data['facility_id'][$f]['facility_amount'],
                    'facility_status' => $data['facility_id'][$f]['project_facility_status'],
                    'created_date_time' => currentDate()
                ));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('action_applied_suc'), 'data'=>$response_param);
        $this->response($result, REST_Controller::HTTP_OK);




       /* $project_stage_workflow = $this->Workflow_model->getProjectStageWorkflow(array('project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],'order_by' => 'DESC'));
        if(!empty($project_stage_workflow) && isset($project_stage_workflow[count($project_stage_workflow)-1]['id_project_stage_workflow'])){
            $this->Workflow_model->updateProjectStageWorkflow(array('id_project_stage_workflow' => $project_stage_workflow[count($project_stage_workflow)-1]['id_project_stage_workflow']));
        }*/

        $next_phase_id = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_action_id' => $data['workflow_phase_action_id']));
        echo '<pre>'; print_r($next_phase_id); exit;
        $data['forward_to_workflow_phase_id'] = $next_phase_id[0]['workflow_phase_activity_id'];
        $parent_project_stage_workflow_details = $this->Workflow_model->getParentWorkflow(array('project_stage_info_id' => $data['project_stage_info_id'],'forward_to_workflow_phase_id' => $data['workflow_phase_action_id'],'order_by' => 'DESC'));
        //echo "<pre>"; print_r($parent_project_stage_workflow_details); exit;
        $parent_workflow_phase_action = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id' => $next_phase_id[0]['parent_workflow_phase_id']));

        if(empty($next_phase_id)){
            $parent_workflow_phase_id = 0;
        }
        else{
            $parent_workflow_phase_id = $next_phase_id[0]['workflow_phase_activity_id'];
        }

        if($next_phase_id[0]['parent_workflow_phase_id']){
            $all_sub_phases = $this->Workflow_model->getWorkflowPhase(array('parent_workflow_phase_id' => $next_phase_id[0]['parent_workflow_phase_id']));
            $sub_phase_ids = array_map(function($i){ return $i['id_workflow_phase']; },$all_sub_phases);
            $sub_phase_actions = $this->Workflow_model->getProjectStageWorkflowPhaseDetails(array('project_stage_info_id' => $data['project_stage_info_id'],'phase_array' => $sub_phase_ids,'status' => 'completed'));
            $child_parent_forwarded_by_workflow = $this->Workflow_model->getProjectStageWorkflow(array('project_stage_workflow_phase_id' => $sub_phase_actions[0]['id_project_stage_workflow_phase']));
            if(count($all_sub_phases)==count($sub_phase_actions))
            {
                $project_stage_workflow_phase_id = $this->Workflow_model->addProjectStageWorkflowPhase(array(
                    'workflow_phase_id' => $parent_workflow_phase_action[0]['workflow_phase_activity_id'],
                    'assigned_to' => $child_parent_forwarded_by_workflow[0]['forward_by'],
                    'project_stage_info_id' => $data['project_stage_info_id'],
                    'status' => 'pending',
                    'created_date_time' => currentDate(),
                    'parent_workflow_phase_id' => $parent_workflow_phase_id
                ));
            }
        }
        if(isset($data['forward_to']) && !empty($data['forward_to']))
        {
            for($s=0;$s<count($data['forward_to']);$s++)
            {
                if(!$next_phase_id[0]['parent_workflow_phase_id']){}
                $stage_workflow_data = array(
                    'project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],
                    'workflow_phase_action_id' => $data['workflow_phase_action_id'],
                    'forward_by' => $data['forward_by'],
                    'forward_to' => $data['forward_to'][$s]['user_id'],
                    'workflow_comments' => $data['workflow_comments'],
                    'forward_to_workflow_phase_id' => $data['forward_to'][$s]['workflow_phase_id'],
                    'created_date_time' => currentDate()
                );
                $project_stage_workflow_phase_id = $this->Workflow_model->addProjectStageWorkflowPhase(array(
                    'workflow_phase_id' => $data['forward_to'][$s]['workflow_phase_id'],
                    'assigned_to' => $data['forward_to'][$s]['user_id'],
                    'project_stage_info_id' => $data['project_stage_info_id'],
                    'status' => 'pending',
                    'created_date_time' => currentDate(),
                    'parent_workflow_phase_id' => $parent_workflow_phase_id
                ));

                $project_stage_workflow_id = $this->Workflow_model->addProjectStageWorkflow($stage_workflow_data);

                if($project_stage_workflow_id){
                    $project_stage_workflow_content = $this->Workflow_model->getProjectStageWorkflowDetails(array('id_project_stage_workflow' => $project_stage_workflow_id));
                    if(isset($project_stage_workflow_content[0]['action']) && $project_stage_workflow_content[0]['action']=='forwarded')
                    {
                        $project_details = $this->Crm_model->getProject($data['project_id']);
                        $project_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
                        $company_user = $this->Company_model->getCompanyUser($project_stage_workflow_content[0]['forward_to']);
                        $company_user1 = $this->Company_model->getCompanyUser($project_stage_workflow_content[0]['forward_by']);
                        //$company = $this->Company_model->getCompany(array('id_company' => $project_details[0]['company_id']));
                        if($project_stage_workflow_content[0]['to_user_name']!='') {
                            $link ='<a style="color: #22bcf2" class="sky-blue" href="'.WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']).'">here</a>';
                            $notification_message = str_replace(array('{project_title}','{stage}','{action}','{assigned_user}','{assigned_user_department}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],$project_stage_workflow_content[0]['from_user_name'],$company_user1->approval_name,date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_message'));
                            $mail_message = str_replace(array('{project_title}','{stage}','{action}','{assigned_user}','{assigned_user_department}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],$project_stage_workflow_content[0]['from_user_name'],$company_user1->approval_name,date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_mail'));
                            //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded to you by ' . $project_stage_workflow_content[0]['to_user_name'] . ' on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                        }
                        else {
                            $link ='<a style="color: #22bcf2" class="sky-blue" href="'.WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']).'">here</a>';
                            $notification_message = str_replace(array('{project_title}','{stage}','{action}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_message1'));
                            $mail_message = str_replace(array('{project_title}','{stage}','{action}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_mail1'));
                            //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded.  on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                        }

                        $link ='<a style="color: #22bcf2" class="sky-blue" href="'.WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']).'">here</a>';
                        $template_data = array(
                            'web_base_url' => WEB_BASE_URL,
                            'to_username' => $project_stage_workflow_content[0]['to_user_name'],
                            'message' => $mail_message,
                            'mail_footer' => $this->lang->line('mail_footer')
                        );
                        $subject = str_replace(array('{project_title}','{assigned_user}'),array($project_details[0]['project_title'],$project_stage_workflow_content[0]['to_user_name']),$this->lang->line('workflow_mail_subject'));
                        $template_data = $this->parser->parse('templates/notification.html',$template_data);
                        //$project_stage_workflow_content[0]['to_email']
                        saveSendMail($project_stage_workflow_content[0]['to_email'],$subject,$template_data);

                        $this->Notification_model->addNotification(array(
                            'assigned_by' => $data['forward_by'],
                            'assigned_to' => $project_stage_workflow_content[0]['forward_to'],
                            'module_reference_id' => $project_stage_workflow_content[0]['id_project_stage_workflow'],
                            'module_reference_type' => 'workflow',
                            'notification_subject' => $project_details[0]['project_title'],
                            'notification_template' => $notification_message,
                            'notification_link' => $link,
                            'notification_comments' => '',
                            'notification_type' => 'notification',
                            'created_date_time' => currentDate(),
                            'module_id' => $project_details[0]['id_crm_project'],
                            'module_type' => 'project'
                        ));

                        /*$created_user = $this->User_model->getUserInfo(array('id' => $data['forward_by']));
                          $template = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has created new revision for '.$stage_forms[0]['stage_name'].' stage </p>';
                          $activity = array(
                            'activity_name' => 'Workflow',
                            'activity_type' => 'workflow',
                            'activity_template' =>$template,
                            'activity_reference_id' => $project_stage_workflow_content[0]['id_project_stage_workflow'],
                            'module_type' => 'project',
                            'module_id' => $data['project_id'],
                            'created_by' => $data['forward_by'],
                            'created_date_time' => currentDate()
                          );
                          $this->Activity_model->addActivity($activity);*/
                    }
                }
            }
        }
        else{

            if(!empty($next_phase_id) && $next_phase_id[0]['is_approve']==1){
                $forward_user = 0;
                $forward_phase_id = 0;
            }
            else{
                $current_project_stage_workflow_phase = $this->Workflow_model->getProjectStageWorkflowPhaseDetails(array('id_project_stage_workflow_phase' => $data['id_project_stage_workflow_phase']));
                $current_child_parent_workflow_phase_action = $this->Workflow_model->getWorkflowPhaseAction(array('workflow_phase_id' => $current_project_stage_workflow_phase[0]['parent_workflow_phase_id']));
                if(isset($child_parent_forwarded_by_workflow[0]['forward_by']))
                    $forward_user = $child_parent_forwarded_by_workflow[0]['forward_by'];
                else
                    $forward_user = $parent_project_stage_workflow_details[0]['forward_by'];

                $forward_phase_id = $current_child_parent_workflow_phase_action[0]['workflow_phase_activity_id'];
            }
            $stage_workflow_data = array(
                'project_stage_workflow_phase_id' => $data['id_project_stage_workflow_phase'],
                'workflow_phase_action_id' => $data['workflow_phase_action_id'],
                'forward_by' => $data['forward_by'],
                'forward_to' => $forward_user,
                'workflow_comments' => $data['workflow_comments'],
                'forward_to_workflow_phase_id' => $forward_phase_id,
                'created_date_time' => currentDate()
            );
            $project_stage_workflow_id = $this->Workflow_model->addProjectStageWorkflow($stage_workflow_data);

            if($project_stage_workflow_id){
                $project_stage_workflow_content = $this->Workflow_model->getProjectStageWorkflowDetails(array('id_project_stage_workflow' => $project_stage_workflow_id));
                if(isset($project_stage_workflow_content[0]['action']) && $project_stage_workflow_content[0]['action']=='forwarded')
                {
                    $project_details = $this->Crm_model->getProject($data['project_id']);
                    $project_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
                    $company_user = $this->Company_model->getCompanyUser($project_stage_workflow_content[0]['forward_to']);
                    //$company = $this->Company_model->getCompany(array('id_company' => $project_details[0]['company_id']));
                    /*if($project_stage_workflow_content[0]['to_user_name']!='')
                        $msg = '<b><a href="'.WEB_BASE_URL.'html/#/project/project-overview/'.base64_encode($data['project_id']).'/'.base64_encode($data['project_stage_info_id']).'/'.base64_encode($data['project_stage_id']).'">'.$project_details[0]['project_title'].'</a></b> is forwarded to you by '.$project_stage_workflow_content[0]['to_user_name'].' on '.date("M d, Y",strtotime($project_stage_workflow_content[0]['created_date_time']));
                    else
                        $msg = '<b><a href="'.WEB_BASE_URL.'html/#/project/project-overview/'.base64_encode($data['project_id']).'/'.base64_encode($data['project_stage_info_id']).'/'.base64_encode($data['project_stage_id']).'">'.$project_details[0]['project_title'].'</a></b> is forwarded.  on '.date("M d, Y",strtotime($project_stage_workflow_content[0]['created_date_time']));

                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'to_username' => $project_stage_workflow_content[0]['to_user_name'],
                        'message' => $msg,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $subject = $project_details[0]['project_title'].' | New project request';
                    $template_data = $this->parser->parse('templates/notification.html',$template_data);
                    //$project_stage_workflow_content[0]['to_email']
                    //mailCheck('app.mazic@gmail.com',$subject,$template_data);
                    mailCheck('app.mazic@gmail.com',$subject,$template_data);*/

                    if($project_stage_workflow_content[0]['to_user_name']!='') {
                        $link ='<a href="'.WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']).'">here</a>';
                        $notification_message = str_replace(array('{project_title}','{stage}','{action}','{assigned_user}','{assigned_user_department}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],$project_stage_workflow_content[0]['to_user_name'],$company_user->approval_name,date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_message'));
                        $mail_message = str_replace(array('{project_title}','{stage}','{action}','{assigned_user}','{assigned_user_department}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],$project_stage_workflow_content[0]['to_user_name'],$company_user->approval_name,date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_mail'));
                        //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded to you by ' . $project_stage_workflow_content[0]['to_user_name'] . ' on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                    }
                    else {
                        $link ='<a href="'.WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']).'">here</a>';
                        $notification_message = str_replace(array('{project_title}','{stage}','{action}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_message1'));
                        $mail_message = str_replace(array('{project_title}','{stage}','{action}','{date_time}','{here}'),array($project_details[0]['project_title'],$project_stage[0]['stage_name'],$next_phase_id[0]['workflow_action_key'],date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time'])),$link),$this->lang->line('workflow_notification_mail1'));
                        //$msg = '<b><a href="' . WEB_BASE_URL . '#/project/project-overview/' . base64_encode($data['project_id']) . '/' . base64_encode($data['project_stage_info_id']) . '/' . base64_encode($data['project_stage_id']) . '">' . $project_details[0]['project_title'] . '</a></b> is forwarded.  on ' . date("M d, Y", strtotime($project_stage_workflow_content[0]['created_date_time']));
                    }

                    $link = WEB_BASE_URL.'html/#/project/project-overview/'.base64_encode($data['project_id']).'/'.base64_encode($data['project_stage_info_id']).'/'.base64_encode($data['project_stage_id']);
                    $template_data = array(
                        'web_base_url' => WEB_BASE_URL,
                        'to_username' => $project_stage_workflow_content[0]['to_user_name'],
                        'message' => $mail_message,
                        'mail_footer' => $this->lang->line('mail_footer')
                    );
                    $subject = str_replace(array('{project_title}','{assigned_user}'),array($project_details[0]['project_title'],$project_stage_workflow_content[0]['to_user_name']),$this->lang->line('workflow_mail_subject'));
                    $template_data = $this->parser->parse('templates/notification.html',$template_data);
                    //$project_stage_workflow_content[0]['to_email']
                    sendmail($project_stage_workflow_content[0]['to_email'],$subject,$template_data);

                    $this->Notification_model->addNotification(array(
                        'assigned_by' => $data['forward_by'],
                        'assigned_to' => $project_stage_workflow_content[0]['forward_to'],
                        'module_reference_id' => $project_stage_workflow_content[0]['id_project_stage_workflow'],
                        'module_reference_type' => 'workflow',
                        'notification_subject' => $project_details[0]['project_title'],
                        'notification_template' => $notification_message,
                        'notification_link' => $link,
                        'notification_comments' => '',
                        'notification_type' => 'notification',
                        'created_date_time' => currentDate(),
                        'module_id' => $project_details[0]['id_crm_project'],
                        'module_type' => 'project'
                    ));
                }
            }
        }

        //if approved initializing next stage
        if(!empty($next_phase_id) && $next_phase_id[0]['is_approve']==1)
        {
            $this->Workflow_model->updateProjectStageInfo(array(
                'id_project_stage_info' => $data['project_stage_info_id'],
                'project_stage_status' => 'approved',
                'end_date_time' => currentDate()
            ));
            $project_stage_info_details = $this->Project_model->getProjectStageInfo(array('project_id' => $data['project_id'],'stage_id' => $data['project_stage_id']));
            $this->initializeNextStage(array('project_id' => $data['project_id'],'project_stage_id' => $data['project_stage_id'],'project_stage_info_id' => $project_stage_info_details[0]['id_project_stage_info'],'user_id' => $project_stage_info_details[0]['created_by']));

            //facilities update for next stage
        }


        //uploading attachments
        $company_id = $data['company_id'];
        $path='uploads/'; $data['document_type'] = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['id_project_stage_workflow_phase'], 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['forward_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'stage_comments');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');


                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['id_project_stage_workflow_phase'], 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['forward_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'stage_comments');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }



        $result = array('status'=>TRUE, 'message' => $this->lang->line('action_applied_suc'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function dumpProjectFacilities_get()  // implemented to insert previous stages facilities in mongo db
    {
        $projects = $this->Project_model->getProjects();
        for($p=0; $p<count($projects); $p++)
        {
            $project_id = $projects[$p]['project_id'];
            $project_stages = $this->Project_model->getProjectStages(['project_id' => $project_id]);
            if(!empty($project_stages))
            {
                for($ps=0; $ps<count($project_stages); $ps++)
                {
                    $project_stage_id = $project_stages[$ps]['project_stage_id'];
                    $result = $this->Crm_model->getFacilities(array('crm_project_id' => $project_id));
                    if(!empty($result))
                    {
                        for($s=0;$s<count($result);$s++){
                            $result[$s]['project_stage_info_id'] = $project_stage_id;
                            $result[$s]['pricing'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'pricing'));
                            for($t=0;$t<count($result[$s]['pricing']);$t++){
                                $result[$s]['pricing'][$t]['facility_field_description'] = addslashes($result[$s]['pricing'][$t]['facility_field_description']);
                            }
                            $result[$s]['terms_conditions'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
                            for($t=0;$t<count($result[$s]['terms_conditions']);$t++){
                                $result[$s]['terms_conditions'][$t]['facility_field_description'] = addslashes($result[$s]['terms_conditions'][$t]['facility_field_description']);
                            }
                        }

                        $mongo_data = array(
                            'log_auth_key' => LOG_AUTH_KEY,
                            'project_id' => $project_id,
                            'project_stage_info_id' => $project_stage_id,
                            'facilities' => $result
                        );

                        $mongo_data = json_encode($mongo_data);

                        $ch = curl_init();
                        //curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_URL.'addFacilityDetails');
                        curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_PHP_URL.'addFacilityDetails.php');
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $mongo_data);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($mongo_data))
                        );
                        $server_output = curl_exec ($ch);
                        curl_close ($ch);

                    }
                }

            }


        }
    }

    public function initializeNextStage($data)
    {
        /*$next_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
        //echo "<pre>"; print_r($next_stage); exit;
        if(!empty($next_stage))
        {
            $project_stage_info_id = $this->Project_model->addProjectStageInfo(array(
                'project_id' => $data['project_id'],
                'stage_id' => $next_stage[0]['next_project_stage_id'],
                'version_number' => 1,
                'project_stage_status' => 'in progress',
                'created_by' => $data['user_id'],
                'start_date_time' => currentDate()
            ));
            $this->Project_model->addProjectStageVersion(['project_stage_info_id' => $project_stage_info_id,'version_number' => 1, 'created_by' => $data['user_id'],'created_date_time' => currentDate()]);

            $this->Crm_model->updateProject(array('project_stage_info_id' => $project_stage_info_id),$data['project_id']);
            //echo $next_stage[0]['next_workflow_id'].'---'; exit;
            if($next_stage[0]['next_workflow_id']!=0 && $next_stage[0]['next_workflow_id']!=''){
                $workflow_phase = $this->Workflow_model->getWorkflowPhase(array('workflow_id' => $next_stage[0]['next_workflow_id']));
                $workflow_phase_id = 0;
                if(!empty($workflow_phase)){
                    $workflow_phase_id = $workflow_phase[0]['id_workflow_phase'];
                }
                $this->Workflow_model->addProjectStageWorkflowPhase(array(
                    'project_stage_info_id' => $project_stage_info_id,
                    'workflow_phase_id' => $workflow_phase_id,
                    'assigned_to' => $data['user_id'],
                    'parent_workflow_phase_id' => 0,
                    'created_date_time' => currentDate(),
                    'status' => 'pending'
                ));
            }
            else{
                //if($data['project_stage_id']!=0 && $data['project_stage_id']!='')
                    //$this->initializeNextStage(array('project_id' => $data['project_id'],'project_stage_id' => $next_stage[0]['next_project_stage_id'],'user_id' => $data['user_id']));
            }
        }*/

        $next_stage = $this->Workflow_model->getNextStageDetails(array('id_project_stage' => $data['project_stage_id']));
        if(!empty($next_stage) && isset($next_stage[0]['next_project_stage_id']) && $next_stage[0]['next_project_stage_id'] && $next_stage[0]['next_project_stage_id']!=0 && trim($next_stage[0]['next_project_stage_id'])!='')
        {
            $project_stage_info_id = $this->Project_model->addProjectStageInfo(array(
                'project_id' => $data['project_id'],
                'stage_id' => $next_stage[0]['next_project_stage_id'],
                'version_number' => 1,
                'project_stage_status' => 'in progress',
                'created_by' => $data['user_id'],
                'start_date_time' => currentDate()
            ));
            $this->Project_model->addProjectStageVersion(['project_stage_info_id' => $project_stage_info_id,'version_number' => 1, 'created_by' => $data['user_id'],'created_date_time' => currentDate()]);
            $this->Crm_model->updateProject(['project_stage_info_id' => $project_stage_info_id,'project_status' => $next_stage[0]['project_stage_complete_status']],$data['project_id']);
            //updating facilities
            $result = $this->Crm_model->getFacilities(array('crm_project_id' => $data['project_id'],'project_stage_info_id' => $data['project_stage_info_id']));
            for($s=0;$s<count($result);$s++){
                $result[$s]['pricing'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'pricing'));
                for($t=0;$t<count($result[$s]['pricing']);$t++){
                    $result[$s]['pricing'][$t]['facility_field_description'] = addslashes($result[$s]['pricing'][$t]['facility_field_description']);
                }
                $result[$s]['terms_conditions'] = $this->Crm_model->getProjectFacilityFieldData(array('project_facility_id' => $result[$s]['id_project_facility'],'field_section' => 'terms_conditions'));
                for($t=0;$t<count($result[$s]['terms_conditions']);$t++){
                    $result[$s]['terms_conditions'][$t]['facility_field_description'] = addslashes($result[$s]['terms_conditions'][$t]['facility_field_description']);
                }
                $this->Crm_model->updateFacilities(array('id_project_facility' => $result[$s]['id_project_facility'],'project_stage_info_id' => $project_stage_info_id));
            }
            //echo "<pre>"; print_r($result); exit;
            $mongo_data = array(
                'log_auth_key' => LOG_AUTH_KEY,
                'project_id' => $data['project_id'],
                'project_stage_info_id' => $data['project_stage_info_id'],
                'facilities' => $result
            );
            $mongo_data = json_encode($mongo_data);

            $ch = curl_init();
            //curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_URL.'addFacilityDetails');
            curl_setopt($ch, CURLOPT_URL, MONGO_SERVICE_PHP_URL.'addFacilityDetails.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mongo_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($mongo_data))
            );
            $server_output = curl_exec ($ch);
            curl_close ($ch);


            if($next_stage[0]['next_workflow_id'] && $next_stage[0]['next_workflow_id']!=0 && trim($next_stage[0]['next_workflow_id'])!=''){
                $workflow_phase = $this->Workflow_model->getWorkflowPhase(array('workflow_id' => $next_stage[0]['next_workflow_id'],'project_id'=>$data['project_id'],'project_stage_id'=>$next_stage[0]['next_project_stage_id'],'is_start'=>1));
                $workflow_phase_id = 0;
                if(!empty($workflow_phase)){
                    $workflow_phase_id = $workflow_phase[0]['id_workflow_phase'];
                }
                $this->Workflow_model->addProjectStageWorkflowPhase(array(
                    'project_stage_info_id' => $project_stage_info_id,
                    'workflow_phase_id' => $workflow_phase_id,
                    'assigned_to' => isset($data['next_stage_user_id'])?$data['next_stage_user_id']:$data['user_id'],
                    'parent_workflow_phase_id' => 0,
                    'created_date_time' => currentDate(),
                    'status' => 'pending'
                ));
            }
           /* else{
                $this->initializeNextStage(array('project_id' => $data['project_id'],'project_stage_id' => $next_stage[0]['next_project_stage_id'],'user_id' => $data['user_id'],'project_stage_info_id' => $project_stage_info_id));
            }*/
        }
        else
            return 1;

    }

    public function projectStageWorkflow_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Workflow_model->getProjectStageWorkflowDetails($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['documents'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_project_stage_workflow_phase'],'module_type' => 'workflow','form_key' => 'stage_comments'));
            for($r=0;$r<count($result[$s]['documents']);$r++){
                $result[$s]['documents'][$r]['document_source'] = getImageUrl($result[$s]['documents'][$r]['document_source'],'file');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectStageWorkflowDocument_get()
    {

    }

    public function comments_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Workflow_model->getProjectStageWorkflowComments($data);

        for($s=0;$s<count($result);$s++){ //echo "<pre>"; print_r($result[$s]);

            if($result[$s]['status']!='need info')
                $result[$s]['documents'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_project_stage_workflow_phase'],'module_type' => 'workflow','form_key' => 'stage_comments'));
            else
                $result[$s]['documents'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_project_stage_workflow_phase'],'module_type' => 'workflow','form_key' => 'form_comments'));
            //echo "<pre>"; print_r($result[$s]);
            for($t=0;$t<count($result[$s]['documents']);$t++)
            {
                $result[$s]['documents'][$t]['document_source'] = getImageUrl($result[$s]['documents'][$t]['document_source'],'file');
                $result[$s]['documents'][$t]['size']=get_file_info($result[$s]['documents'][$t]['document_source'], 'size');
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function comments_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('stage_key', array('required'=> $this->lang->line('stage_key_req')));
        $this->form_validator->add_rules('comment_to', array('required'=> $this->lang->line('comment_to_req')));
        $this->form_validator->add_rules('comment_by', array('required'=> $this->lang->line('comment_by_req')));
        $this->form_validator->add_rules('comment', array('required'=> $this->lang->line('comment_req')));
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('workflow_phase_id', array('required'=> $this->lang->line('workflow_phase_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['document_type'])){ $data['document_type'] =0; }

        $project_stage_workflow_phase_id = $this->Workflow_model->addProjectStageWorkflowPhase(array(
            'workflow_phase_id' => $data['workflow_phase_id'],
            'assigned_to' => $data['comment_to'],
            'project_stage_info_id' => $data['project_stage_info_id'],
            'status' => 'need info',
            'created_date_time' => currentDate(),
            'parent_workflow_phase_id' => 0
        ));

        $stage_workflow_data = array(
            'project_stage_workflow_phase_id' => $project_stage_workflow_phase_id,
            'workflow_phase_action_id' => 0,
            'forward_by' => $data['comment_by'],
            'forward_to' => $data['comment_to'],
            'workflow_comments' => $data['comment'],
            'forward_to_workflow_phase_id' => $data['workflow_phase_id'],
            'created_date_time' => currentDate()
        );
        $project_stage_workflow_id = $this->Workflow_model->addProjectStageWorkflow($stage_workflow_data);

        $user_info = $this->User_model->getUserInfo(array('id' => $data['comment_to']));
        $from_user_info = $this->User_model->getUserInfo(array('id' => $data['comment_by']));
        $project = $this->Crm_model->getProject($data['project_id']);
        $notification = str_replace(array('{project_title}','{comments}','{comments_by}','{date}'),array($project[0]['project_title'],$data['comment'],$from_user_info->first_name.' '.$from_user_info->last_name,date('M d, Y',strtotime(currentDate()))),$this->lang->line('workflow_form_comments_notification'));
        $this->Notification_model->addNotification(array(
            'assigned_by' => $data['comment_by'],
            'assigned_to' => $data['comment_to'],
            'module_reference_id' => $project_stage_workflow_phase_id,
            'module_reference_type' => 'form_comments',
            'notification_subject' => $project[0]['project_title'],
            'notification_template' => $notification,
            'notification_link' => '',
            'notification_comments' => '',
            'notification_type' => 'notification',
            'created_date_time' => currentDate(),
            'module_id' => $project[0]['id_crm_project'],
            'module_type' => 'project'
        ));

        $message = str_replace(array('{project_title}','{comments}','{comments_by}','{date}'),array($project[0]['project_title'],$data['comment'],$from_user_info->first_name.' '.$from_user_info->last_name,date('M d, Y',strtotime(currentDate()))),$this->lang->line('workflow_form_comments_mail'));
        $template_data = array(
            'web_base_url' => WEB_BASE_URL,
            'base_url' => REST_API_URL,
            'to_username' => $user_info->first_name.' '.$user_info->last_name,
            'message' => $message,
            'mail_footer' => $this->lang->line('mail_footer')
        );
        $template_data = $this->parser->parse('templates/notification.html', $template_data);
        //sendmail($user_info->email_id,str_replace(array('{project_title}'),array($project[0]['project_title']),$this->lang->line('workflow_form_comments_subject')),$template_data);
        sendmail($user_info->email_id,str_replace(array('{project_title}'),array($project[0]['project_title']),$this->lang->line('workflow_form_comments_subject')),$template_data);

        $company_id = $data['company_id'];
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $project_stage_workflow_phase_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['comment_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'form_comments');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $project_stage_workflow_phase_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['comment_by'],'created_date_time' => currentDate(),'module_type' => 'workflow','form_key' => 'form_comments');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function projectStageStatus_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('project_stage_id', array('required'=> 'Project stage id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_stage_id = $data['project_stage_id'];
        $result = $this->Workflow_model->getProjectStageStatus($project_stage_id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectStageStatusLog_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Workflow_model->getProjectStageStatusLog($data);
        for($s=0;$s<count($result);$s++){ //echo "<pre>"; print_r($result[$s]);

           $result[$s]['documents'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_project_stage_status_log'],'module_type' => 'project','form_key' => 'project_stage_status_log','field_key' => 'stage_status_comments'));
            //echo $this->db->last_query(); exit;
            //echo "<pre>"; print_r($result[$s]);
            for($t=0;$t<count($result[$s]['documents']);$t++)
            {
                $result[$s]['documents'][$t]['document_source'] = getImageUrl($result[$s]['documents'][$t]['document_source'],'file');
                $result[$s]['documents'][$t]['size']=get_file_info($result[$s]['documents'][$t]['document_source'], 'size');
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function projectStageStatusLog_post()
    {
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('project_stage_status', array('required'=> 'Status required'));
        $this->form_validator->add_rules('comments', array('required'=> $this->lang->line('comment_req')));
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> 'Created by required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['document_type'])){ $data['document_type'] =0; }

        $project_stage_status_log_id = $this->Workflow_model->addProjectStageStatusLog(array(
            'project_stage_info_id' => $data['project_stage_info_id'],
            'status' => $data['project_stage_status'],
            'comments' => $data['comments'],
            'created_by' => $data['created_by'],
            'created_date_time' => currentDate()
        ));

        $this->Crm_model->updateProject(['project_status' => $data['project_stage_status']],$data['project_id']);

        $company_id = $data['company_id'];
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $project_stage_status_log_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_id'=>$data['project_id'],'module_type' => 'project','form_key' => 'project_stage_status_log','field_key' => 'stage_status_comments');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $project_stage_status_log_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'],'created_date_time' => currentDate(),'module_id'=>$data['project_id'],'module_type' => 'project','form_key' => 'project_stage_status_log','field_key' => 'stage_status_comments');
                $document_id = $this->Crm_model->addDocument($document);
            }
        }

        $msg = $this->lang->line('info_update');
        $this->Activity_model->addActivity(array('activity_name' => 'Project Status', 'activity_template' => 'Project status updated to '.$data['project_stage_status'], 'module_type' => 'project', 'module_id' => $data['project_id'], 'activity_type' => 'project_status', 'activity_reference_id' => '', 'created_by' => $data['created_by'], 'created_date_time' => currentDate()));


        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);


    }

    public function userComments_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_stage_info_id', array('required'=> $this->lang->line('project_stage_info_id_req')));
        $this->form_validator->add_rules('stage_key', array('required'=> $this->lang->line('stage_key_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_id_req')));
        $this->form_validator->add_rules('project_id', array('required'=> $this->lang->line('project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getFormComments(array(
            'form_key' => $data['stage_key'],
            'project_stage_section_form_id' => $data['project_stage_info_id'],
            'reference_type' => 'project',
            'reference_id' => $data['project_id'],
            'comment_to' => $data['user_id']
        ));

        for($s=0;$s<count($result);$s++){
            $result[$s]['documents'] = $this->Crm_model->getDocument(array(
                'field_key' => $result[$s]['id_form_comments'],
                'form_key' => $data['stage_key'].'_comments',
                'module_type' => 'project',
                'uploaded_form_id' => $data['project_stage_info_id']
            ));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function workFlowActions_get(){
        $data = $this->input->get();
        $result = $this->Workflow_model->getWorkflowAction();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }


}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Reports extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->model('Master_model');
        $this->load->model('Crm_model');
    }

    public function availableAnalysis_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis_months);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByProductClients_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $products = $this->Reports_model->getPortfolioByProductClients($data);

        $products_clients_total = 0;
        $products_array = [];
        for($c=0; $c<count($products); $c++)
        {
            $items = [];
            $item_index = 0;
            $id = $products[$c]['id'];
            for($sb=0; $sb<count($products); $sb++)
            {
                if($products[$sb]['parent_id'] == $id)
                {
                    $items[$item_index] = $products[$sb];
                    $item_index ++;
                }
            }
            $products[$c]['items'] = $items;
            if($products[$c]['parent_id'] == 0)
            {
                $products_array[] = $products[$c];
                $products_clients_total = $products_clients_total + $products[$c]['clients'];
            }
        }
        $products_array = array_values($products_array);
        $analysis = ['total_products_clients' => $products_clients_total, 'products' => $products_array];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByProduct_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $products = $this->Reports_model->getPortfolioByProduct($data);
        //$sub_products = $this->Reports_model->getPortfolioByProductItem($data);
        $portfolio_total_value = 0;
        $products_array = [];
        for($c=0; $c<count($products); $c++)
        {
            $items = [];
            $item_index = 0;
            $id = $products[$c]['id'];
            for($sb=0; $sb<count($products); $sb++)
            {
                if($products[$sb]['parent_id'] == $id)
                {
                    $items[$item_index] = $products[$sb];
                    $item_index ++;
                }
            }
            $products[$c]['items'] = $items;
            if($products[$c]['parent_id'] == 0)
            {
                $products_array[] = $products[$c];
                $portfolio_total_value = $portfolio_total_value + $products[$c]['amount'];
            }
        }
        $products_array = array_values($products_array);
        $analysis = ['total_outstanding_portfolio' => $portfolio_total_value, 'products' => $products_array];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByProductAnalysis_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $analysis = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        if(!empty($analysis_months))
        {
            $data['start_month'] = date('m-Y',strtotime($analysis_months[count($analysis_months)-1]['analysis_date']));
            $data['end_month'] = date('m-Y',strtotime($analysis_months[0]['analysis_date']));
            $portfolio = $this->Reports_model->getPortfolioByProductAnalysis($data);
            $products = [];
            $c_index = 0;
            for($p = 0; $p<count($portfolio); $p++)
            {
                if(count($products) > 0)
                {
                    $is_exists = 0;
                    for($c=0; $c <count($products); $c++)
                    {
                        if($products[$c]['product_id'] == $portfolio[$p]['product_id'])
                        {
                            $is_exists = 1;
                            break;
                        }
                    }
                    if($is_exists == 0)
                    {
                        $products[$c_index]['product_id'] = $portfolio[$p]['product_id'];
                        $products[$c_index]['product_name'] = $portfolio[$p]['product_name'];
                        $products[$c_index]['amount'] = 0;
                        $c_index ++;
                    }
                }
                else
                {
                    $products[$c_index]['product_id'] = $portfolio[$p]['product_id'];
                    $products[$c_index]['product_name'] = $portfolio[$p]['product_name'];
                    $products[$c_index]['amount'] = 0;
                    $c_index ++;
                }

            }

            for($am = count($analysis_months)-1; $am >= 0;$am--)
            {
                $analysis_date = date('M Y',strtotime($analysis_months[$am]['analysis_date']));
                $analysis_month = date('m-Y',strtotime($analysis_months[$am]['analysis_date']));
                for($c = 0; $c<count($products); $c++)
                {
                    $amount = 0;
                    $product_id = $products[$c]['product_id'];
                    for($p = 0; $p<count($portfolio); $p++)
                    {
                        $portfolio_month = $portfolio[$p]['analysis_date'];
                        if($portfolio[$p]['product_id'] == $product_id && $portfolio_month == $analysis_month)
                        {
                            $amount = $portfolio[$p]['amount'];
                            break;
                        }
                    }
                    $products[$c]['amount'] = $amount;
                }
                $analysis[$analysis_date]= $products;
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getProductClients($products_clients,$productId)
    {
        $analysis_value = 0;
        for($p=0; $p<count($products_clients); $p++)
        {
            if($products_clients[$p]['product_id'] == $productId && !empty($products_clients[$p]['clients']))
            {
                $analysis_value = $products_clients[$p]['clients'];
                break;
            }
        }
        return $analysis_value;
    }

    public function getProductPortfolioValue($products_portfolio,$productId)
    {
        $products_portfolio_value = 0;
        for($p=0; $p<count($products_portfolio); $p++)
        {
            if($products_portfolio[$p]['product_id'] == $productId && !empty($products_portfolio[$p]['amount']))
            {
                $products_portfolio_value = $products_portfolio[$p]['amount'];
                break;
            }
        }
        return $products_portfolio_value;
    }

    public function portfolioByCountry_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $country_portfolio = $this->Reports_model->getPortfolioByCountry($data);
        $country_portfolio_total_value = 0;
        for($c=0; $c<count($country_portfolio); $c++)
        {
            $country_portfolio_total_value = $country_portfolio_total_value + $country_portfolio[$c]['amount'];
        }
        $analysis = ['total_outstanding_portfolio' => $country_portfolio_total_value, 'countries' => $country_portfolio];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByCountryAnalysis_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $analysis = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        if(!empty($analysis_months))
        {
            $data['start_month'] = date('m-Y',strtotime($analysis_months[count($analysis_months)-1]['analysis_date']));
            $data['end_month'] = date('m-Y',strtotime($analysis_months[0]['analysis_date']));
            $portfolio = $this->Reports_model->getPortfolioByCountryAnalysis($data);
            $countries = [];
            $c_index = 0;
            for($p = 0; $p<count($portfolio); $p++)
            {
                if(count($countries) > 0)
                {
                    $is_exists = 0;
                    for($c=0; $c <count($countries); $c++)
                    {
                        if($countries[$c]['id_country'] == $portfolio[$p]['id_country'])
                        {
                            $is_exists = 1;
                            break;
                        }
                    }
                    if($is_exists == 0)
                    {
                        $countries[$c_index]['id_country'] = $portfolio[$p]['id_country'];
                        $countries[$c_index]['country_name'] = $portfolio[$p]['country_name'];
                        $countries[$c_index]['amount'] = 0;
                        $c_index ++;
                    }
                }
                else
                {
                    $countries[$c_index]['id_country'] = $portfolio[$p]['id_country'];
                    $countries[$c_index]['country_name'] = $portfolio[$p]['country_name'];
                    $countries[$c_index]['amount'] = 0;
                    $c_index ++;
                }

            }

            for($am = count($analysis_months)-1; $am >= 0;$am--)
            {
                $analysis_date = date('M Y',strtotime($analysis_months[$am]['analysis_date']));
                $analysis_month = date('m-Y',strtotime($analysis_months[$am]['analysis_date']));
                for($c = 0; $c<count($countries); $c++)
                {
                    $amount = 0;
                    $country_id = $countries[$c]['id_country'];
                    for($p = 0; $p<count($portfolio); $p++)
                    {
                        $portfolio_month = $portfolio[$p]['analysis_date'];
                        if($portfolio[$p]['id_country'] == $country_id && $portfolio_month == $analysis_month)
                        {
                            $amount = $portfolio[$p]['amount'];
                            break;
                        }
                    }
                    $countries[$c]['amount'] = $amount;
                }
                $analysis[$analysis_date]= $countries;
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByCountryClients_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $country_portfolio = $this->Reports_model->getPortfolioByCountryClients($data);
        $country_portfolio_total_value = 0;
        for($c=0; $c<count($country_portfolio); $c++)
        {
            $country_portfolio_total_value = $country_portfolio_total_value + $country_portfolio[$c]['clients'];
        }
        $analysis = ['total_country_portfolio_clients' => $country_portfolio_total_value, 'countries' => $country_portfolio];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByCurrency_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $portfolio = $this->Reports_model->getPortfolioByCurrency($data);
        $portfolio_total_value = 0;
        for($c=0; $c<count($portfolio); $c++)
        {
            $portfolio_total_value = $portfolio_total_value + $portfolio[$c]['amount'];
        }
        $analysis = ['total_outstanding_portfolio' => $portfolio_total_value, 'currencies' => $portfolio];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByCurrencyAnalysis_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $analysis = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        if(!empty($analysis_months))
        {
            $data['start_month'] = date('m-Y',strtotime($analysis_months[count($analysis_months)-1]['analysis_date']));
            $data['end_month'] = date('m-Y',strtotime($analysis_months[0]['analysis_date']));
            $portfolio = $this->Reports_model->getPortfolioByCurrencyAnalysis($data);
            $currencies = [];
            $c_index = 0;
            for($p = 0; $p<count($portfolio); $p++)
            {
                if(count($currencies) > 0)
                {
                    $is_exists = 0;
                    for($c=0; $c <count($currencies); $c++)
                    {
                        if($currencies[$c]['id_currency'] == $portfolio[$p]['id_currency'])
                        {
                            $is_exists = 1;
                            break;
                        }
                    }
                    if($is_exists == 0)
                    {
                        $currencies[$c_index]['id_currency'] = $portfolio[$p]['id_currency'];
                        $currencies[$c_index]['currency_code'] = $portfolio[$p]['currency_code'];
                        $currencies[$c_index]['amount'] = 0;
                        $c_index ++;
                    }
                }
                else
                {
                    $currencies[$c_index]['id_currency'] = $portfolio[$p]['id_currency'];
                    $currencies[$c_index]['currency_code'] = $portfolio[$p]['currency_code'];
                    $currencies[$c_index]['amount'] = 0;
                    $c_index ++;
                }

            }

            for($am = count($analysis_months)-1; $am >= 0;$am--)
            {
                $c_index = 0;
                $currency_analysis = [];
                $analysis_date = date('M Y',strtotime($analysis_months[$am]['analysis_date']));
                $analysis_month = date('m-Y',strtotime($analysis_months[$am]['analysis_date']));
                for($c = 0; $c<count($currencies); $c++)
                {
                    $amount = 0;
                    $currency_id = $currencies[$c]['id_currency'];
                    for($p = 0; $p<count($portfolio); $p++)
                    {
                        $portfolio_month = $portfolio[$p]['analysis_date'];
                        if($portfolio[$p]['id_currency'] == $currency_id && $portfolio_month == $analysis_month)
                        {
                            $amount = $portfolio[$p]['amount'];
                            break;
                        }
                    }
                    $currencies[$c]['amount'] = $amount;
                }
                $analysis[$analysis_date]= $currencies;
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioBySector_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $portfolio = $this->Reports_model->getPortfolioBySector($data);
        $portfolio_total_value = 0;
        for($c=0; $c<count($portfolio); $c++)
        {
            $portfolio_total_value = $portfolio_total_value + $portfolio[$c]['amount'];
        }
        $analysis = ['total_outstanding_portfolio' => $portfolio_total_value, 'sectors' => $portfolio];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioBySectorAnalysis_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $analysis = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        if(!empty($analysis_months))
        {
            $data['start_month'] = date('m-Y',strtotime($analysis_months[count($analysis_months)-1]['analysis_date']));
            $data['end_month'] = date('m-Y',strtotime($analysis_months[0]['analysis_date']));
            $portfolio = $this->Reports_model->getPortfolioBySectorAnalysis($data);
            $sectors = [];
            $c_index = 0;
            for($p = 0; $p<count($portfolio); $p++)
            {
                if(count($sectors) > 0)
                {
                    $is_exists = 0;
                    for($c=0; $c <count($sectors); $c++)
                    {
                        if($sectors[$c]['id_sector'] == $portfolio[$p]['id_sector'])
                        {
                            $is_exists = 1;
                            break;
                        }
                    }
                    if($is_exists == 0)
                    {
                        $sectors[$c_index]['id_sector'] = $portfolio[$p]['id_sector'];
                        $sectors[$c_index]['sector_name'] = $portfolio[$p]['sector_name'];
                        $sectors[$c_index]['amount'] = 0;
                        $c_index ++;
                    }
                }
                else
                {
                    $sectors[$c_index]['id_sector'] = $portfolio[$p]['id_sector'];
                    $sectors[$c_index]['sector_name'] = $portfolio[$p]['sector_name'];
                    $sectors[$c_index]['amount'] = 0;
                    $c_index ++;
                }

            }
            for($am = count($analysis_months)-1; $am >= 0;$am--)
            {
                $analysis_date = date('M Y',strtotime($analysis_months[$am]['analysis_date']));
                $analysis_month = date('m-Y',strtotime($analysis_months[$am]['analysis_date']));
                for($c = 0; $c<count($sectors); $c++)
                {
                    $amount = 0;
                    $sector_id = $sectors[$c]['id_sector'];
                    for($p = 0; $p<count($portfolio); $p++)
                    {
                        $portfolio_month = $portfolio[$p]['analysis_date'];
                        if($portfolio[$p]['id_sector'] == $sector_id && $portfolio_month == $analysis_month)
                        {
                            $amount = $portfolio[$p]['amount'];
                            break;
                        }
                    }
                    $sectors[$c]['amount'] = $amount;
                }
                $analysis[$analysis_date]= $sectors;
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function portfolioByESGrade_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $portfolio = $this->Reports_model->getPortfolioByESGrade($data);
        $portfolio_total_value = 0;
        $grades = [['grade' => 1, 'amount' => 0, 'description' => 'Projects with sigificant adverses E&S impacts that are diverse or sensitive and may extend beyond immediate project site.'],['grade' => 2, 'amount' => 0, 'description' => 'Projects with potentially limited adverse E&S impacts that are few, site specific, and for which mitigation measures are readily available.'],['grade' => 3, 'amount' => 0,'description' => 'Projects with minimal or no adverse E&S impacts.'],['grade' => 4, 'amount' => 0,'description' => 'Caters for situations where EADB channels funds through other institutions such as financial intermediaries.']];
        for($g=0; $g<count($grades); $g++)
        {
            for($c=0; $c<count($portfolio); $c++)
            {
                if($grades[$g]['grade'] == $portfolio[$c]['grade'])
                {
                    $grades[$g]['amount'] = $portfolio[$c]['amount'];
                    $portfolio_total_value = $portfolio_total_value + $portfolio[$c]['amount'];
                    break;
                }
            }
        }

        $analysis = ['total_outstanding_portfolio' => $portfolio_total_value, 'grades' => $grades];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $analysis);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function outstandingExposureByProduct_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));

        $current_analysis_month = date('m-Y',strtotime($data['analysis_date']));
        $previous_analysis_month = '';
        $current_portfolio = [];
        $previous_portfolio = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        for($am =0; $am < count($analysis_months); $am++)
        {
            if(date('m-Y',strtotime($analysis_months[$am]['analysis_date'])) == $current_analysis_month)
            {
                if(isset($analysis_months[$am+1]['analysis_date']))
                {
                    $previous_analysis_month = date('m-Y',strtotime($analysis_months[$am+1]['analysis_date']));
                    break;
                }
            }
        }
        $data['date'] = $current_analysis_month;
        $current_portfolio = $this->Reports_model->getOutstandingExposureByProduct($data);
        $aging = $this->Reports_model->getOutstandingAgingByProduct($data);
        for($c=0; $c<count($current_portfolio); $c++)
        {
            $current_portfolio[$c] = $this->getOutstandingAging($aging,$current_portfolio[$c],$current_portfolio[$c]['id']);
        }
       if(!empty($previous_analysis_month))
        {
            $data['date'] = $previous_analysis_month;
            $previous_portfolio = $this->Reports_model->getOutstandingExposureByProduct($data);
            $aging = $this->Reports_model->getOutstandingAgingByProduct($data);
            for($c=0; $c<count($previous_portfolio); $c++)
            {
                $previous_portfolio[$c] = $this->getOutstandingAging($aging,$previous_portfolio[$c],$previous_portfolio[$c]['id']);
            }
        }
        $portfolio = [];
        if(!empty($current_portfolio))
            $portfolio = $this->compareOutStandingExposure($current_portfolio,$previous_portfolio,'products');

        $products = $portfolio['products'];
        $final_products = [];
        $fp_index = 0;
        for($c=0; $c<count($products); $c++)
        {
            if($products[$c]['parent_id'] > 0)
            {
                continue;
            }
            else
            {
                $items = [];
                $item_index = 0;
                $id = $products[$c]['id'];
                for($sb=0; $sb<count($products); $sb++)
                {
                    if($products[$sb]['parent_id'] == $id)
                    {
                        $items[$item_index] = $products[$sb];
                        $item_index ++;
                    }
                }
                $products[$c]['items'] = $items;
                $final_products[$fp_index] = $products[$c];
                $fp_index ++;
            }
        }
        $portfolio['products'] = $final_products;
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $portfolio);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function outstandingExposureBySector_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        $current_analysis_month = date('m-Y',strtotime($data['analysis_date']));
        $previous_analysis_month = '';
        $current_portfolio = [];
        $previous_portfolio = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        for($am =0; $am < count($analysis_months); $am++)
        {
            if(date('m-Y',strtotime($analysis_months[$am]['analysis_date'])) == $current_analysis_month)
            {
                if(isset($analysis_months[$am+1]['analysis_date']))
                {
                    $previous_analysis_month = date('m-Y',strtotime($analysis_months[$am+1]['analysis_date']));
                    break;
                }
            }
        }

        $data['date'] = $current_analysis_month;
        $current_portfolio = $this->Reports_model->getOutstandingExposureBySector($data);
        $aging = $this->Reports_model->getOutstandingAgingBySector($data);
        for($c=0; $c<count($current_portfolio); $c++)
        {
            $current_portfolio[$c] = $this->getOutstandingAging($aging,$current_portfolio[$c],$current_portfolio[$c]['id']);
        }


        if(!empty($previous_analysis_month))
        {
            $data['date'] = $previous_analysis_month;
            $previous_portfolio = $this->Reports_model->getOutstandingExposureBySector($data);
            $aging = $this->Reports_model->getOutstandingAgingBySector($data);
            for($c=0; $c<count($previous_portfolio); $c++)
            {
                $previous_portfolio[$c] = $this->getOutstandingAging($aging,$previous_portfolio[$c],$previous_portfolio[$c]['id']);
            }
        }

        $portfolio = [];
        if(!empty($current_portfolio))
            $portfolio = $this->compareOutStandingExposure($current_portfolio,$previous_portfolio,'sectors');
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $portfolio);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function outstandingExposureByCurrency_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        $current_analysis_month = date('m-Y',strtotime($data['analysis_date']));
        $previous_analysis_month = '';
        $current_portfolio = [];
        $previous_portfolio = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        for($am =0; $am < count($analysis_months); $am++)
        {
            if(date('m-Y',strtotime($analysis_months[$am]['analysis_date'])) == $current_analysis_month)
            {
                if(isset($analysis_months[$am+1]['analysis_date']))
                {
                    $previous_analysis_month = date('m-Y',strtotime($analysis_months[$am+1]['analysis_date']));
                    break;
                }
            }
        }

        $data['date'] = $current_analysis_month;
        $current_portfolio = $this->Reports_model->getOutstandingExposureByCurrency($data);
        $aging = $this->Reports_model->getOutstandingAgingByCurrency($data);
        for($c=0; $c<count($current_portfolio); $c++)
        {
            $current_portfolio[$c] = $this->getOutstandingAging($aging,$current_portfolio[$c],$current_portfolio[$c]['id']);
        }

        if(!empty($previous_analysis_month))
        {
            $data['date'] = $previous_analysis_month;
            $previous_portfolio = $this->Reports_model->getOutstandingExposureByCurrency($data);
            $aging = $this->Reports_model->getOutstandingAgingByCurrency($data);
            for($c=0; $c<count($current_portfolio); $c++)
            {
                $previous_portfolio[$c] = $this->getOutstandingAging($aging,$previous_portfolio[$c],$previous_portfolio[$c]['id']);
            }
        }

        $portfolio = [];
        if(!empty($current_portfolio))
            $portfolio = $this->compareOutStandingExposure($current_portfolio,$previous_portfolio,'currencies');
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $portfolio);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getOutstandingAging($aging,$portfolio,$id)
    {
        $arrears = 0;
        $aging_analysis = [['name' => 'norm', 'status' => 'norm', 'value' => 0],['name' => 'watch', 'status' => 'watc', 'value' => 0],['name' => 'sub_standard', 'status' => 'subs', 'value' => 0],['name' => 'doubtful', 'status' => 'doub', 'value' => 0],['name' => 'loss', 'status' => 'loss', 'value' => 0]];
        for($as=0; $as<count($aging_analysis);$as++)
        {
            $aging_status = $aging_analysis[$as]['status'];
            $aging_status_name = $aging_analysis[$as]['name'];
            $aging_status_value = $aging_analysis[$as]['value'];
            for($a=0; $a<count($aging);$a++)
            {
                if($aging[$a]['id'] == $id && strtolower($aging[$a]['proposed']) == $aging_status)
                {
                    $arrears = $arrears + $aging[$a]['arrears'];
                    $aging_status_value = $aging_status_value + $aging[$a]['arrears'];
                }
            }
            $portfolio[$aging_status_name] = $aging_status_value;
        }
        $portfolio['arrears'] = $arrears;
        return $portfolio;
    }

    public function outstandingExposureByCountry_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $current_analysis_month = date('m-Y',strtotime($data['analysis_date']));
        $previous_analysis_month = '';
        $current_portfolio = [];
        $previous_portfolio = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        for($am =0; $am < count($analysis_months); $am++)
        {
            if(date('m-Y',strtotime($analysis_months[$am]['analysis_date'])) == $current_analysis_month)
            {
                if(isset($analysis_months[$am+1]['analysis_date']))
                {
                    $previous_analysis_month = date('m-Y',strtotime($analysis_months[$am+1]['analysis_date']));
                    break;
                }
            }
        }
        $data['date'] = $current_analysis_month;
        $current_portfolio = $this->Reports_model->getOutstandingExposureByCountry($data);
        $aging = $this->Reports_model->getOutstandingAgingByCountry($data);
        for($c=0; $c<count($current_portfolio); $c++)
        {
            $current_portfolio[$c] = $this->getOutstandingAging($aging,$current_portfolio[$c],$current_portfolio[$c]['id']);
        }

        if(!empty($previous_analysis_month))
        {
            $data['date'] = $previous_analysis_month;
            $previous_portfolio = $this->Reports_model->getOutstandingExposureByCountry($data);
            $aging = $this->Reports_model->getOutstandingAgingByCountry($data);
            for($c=0; $c<count($previous_portfolio); $c++)
            {
                $previous_portfolio[$c] = $this->getOutstandingAging($aging,$previous_portfolio[$c],$previous_portfolio[$c]['id']);
            }
        }

        $portfolio = [];
        if(!empty($current_portfolio))
            $portfolio = $this->compareOutStandingExposure($current_portfolio,$previous_portfolio,'countries');
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $portfolio);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function compareOutStandingExposure($current_portfolio,$previous_portfolio, $portfolio_name = 'items')
    {
        $portfolio_keys = array_keys($current_portfolio[0]);
        $current_portfolio_total = [];
        $previous_portfolio_total = [];
        for($cp=0; $cp<count($current_portfolio); $cp++)
        {
            $id = $current_portfolio[$cp]['id'];
            for($pk=2; $pk<count($portfolio_keys); $pk++)
            {
                $portfolio_key = $portfolio_keys[$pk];
                if(isset($current_portfolio[$cp]['parent_id']) && $current_portfolio[$cp]['parent_id'] == 0)
                {
                    if(isset($current_portfolio_total[$portfolio_key.'_total']))
                        $current_portfolio_total[$portfolio_key.'_total'] = $current_portfolio_total[$portfolio_key.'_total'] + $current_portfolio[$cp][$portfolio_key];
                    else
                        $current_portfolio_total[$portfolio_key.'_total'] = $current_portfolio[$cp][$portfolio_key];
                    $current_portfolio_total[$portfolio_key.'_total_compare'] = '';
                }
                else if(!isset($current_portfolio[$cp]['parent_id']))
                {
                    if(isset($current_portfolio_total[$portfolio_key.'_total']))
                        $current_portfolio_total[$portfolio_key.'_total'] = $current_portfolio_total[$portfolio_key.'_total'] + $current_portfolio[$cp][$portfolio_key];
                    else
                        $current_portfolio_total[$portfolio_key.'_total'] = $current_portfolio[$cp][$portfolio_key];
                    $current_portfolio_total[$portfolio_key.'_total_compare'] = '';
                }
                $current_portfolio[$cp][$portfolio_key.'_compare'] = '';
            }
            for($pp=0; $pp<count($previous_portfolio); $pp++)
            {
                if($previous_portfolio[$pp]['id'] == $id)
                {
                    for($pk=2; $pk<count($portfolio_keys); $pk++)
                    {
                        $portfolio_key = $portfolio_keys[$pk];

                        if(isset($previous_portfolio_total[$portfolio_key.'_total']))
                            $previous_portfolio_total[$portfolio_key.'_total'] = $previous_portfolio_total[$portfolio_key.'_total'] + $previous_portfolio[$pp][$portfolio_key];
                        else
                            $previous_portfolio_total[$portfolio_key.'_total'] = $previous_portfolio[$pp][$portfolio_key];

                        $current_portfolio[$cp][$portfolio_key.'_compare'] = ($current_portfolio[$cp][$portfolio_key] > $previous_portfolio[$pp][$portfolio_key]) ? 'high' : ($current_portfolio[$cp][$portfolio_key] == $previous_portfolio[$pp][$portfolio_key]) ? 'equal' : 'low';
                    }
                }
            }
        }

        foreach($current_portfolio_total as $cp_key => $cp_value)
        {
            foreach($previous_portfolio_total as $pp_key => $pp_value)
            {
                if($cp_key == $pp_key)
                {
                    $current_portfolio_total[$cp_key.'_compare'] = ($current_portfolio_total[$cp_key] > $previous_portfolio_total[$cp_key]) ? 'high' : ($current_portfolio_total[$cp_key] == $previous_portfolio_total[$cp_key]) ? 'equal' : 'low';
                }
            }
        }

        $portfolio = $current_portfolio_total;
        $portfolio[$portfolio_name] = $current_portfolio;
        return $portfolio;
    }

    public function analysisCountries_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $countries = $this->Reports_model->getAnalysisCountries(['company_id' => $data['company_id']]);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $countries);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function analysisProducts_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $where = ['master_key' => 'facility_type','order' => 'id_child'];
        $products = $this->Reports_model->getCompanyProducts($where);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $products);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function analysisSectors_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $countries = $this->Reports_model->getAnalysisSectors(['company_id' => $data['company_id']]);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $countries);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function outstandingBorrowers_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $all_outstanding_borrowers = $this->Reports_model->getAllOutstandingBorrowers($data);
        $total_records = 0;
        $total_outstanding_borrowers_amount = 0;
        if(isset($all_outstanding_borrowers[0]))
        {
            $total_records = $all_outstanding_borrowers[0]['total_records'];
            $total_outstanding_borrowers_amount = $all_outstanding_borrowers[0]['amount'];
        }
        $outstanding_borrowers = $this->Reports_model->getOutstandingBorrowers($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> ['total_records' => $total_records, 'total_outstanding_borrowers_amount' => $total_outstanding_borrowers_amount, 'data' => $outstanding_borrowers]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function outStandingStatus_get()
    {
        $outstanding_status = [['name' => 'Normal', 'status' => 'norm'],['name' => 'Watch', 'status' => 'watc'],['name' => 'Sub-standard', 'status' => 'subs'],['name' => 'Loss', 'status' => 'loss']];
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $outstanding_status);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sectorLimits_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $current_analysis_month = date('m-Y',strtotime($data['analysis_date']));
        $previous_analysis_month = '';
        $current_portfolio = [];
        $previous_portfolio = [];
        $analysis_months = $this->Reports_model->getAvailableAnalysis(['company_id' => $data['company_id']]);
        for($am =0; $am < count($analysis_months); $am++)
        {
            if(date('m-Y',strtotime($analysis_months[$am]['analysis_date'])) == $current_analysis_month)
            {
                if(isset($analysis_months[$am+1]['analysis_date']))
                {
                    $previous_analysis_month = date('m-Y',strtotime($analysis_months[$am+1]['analysis_date']));
                    break;
                }
            }
        }

        $sectors = [];
        $sub_sectors = [];
        $data['date'] = $current_analysis_month;
        $current_month_sectors_portfolio = $this->Reports_model->getOutstandingExposureBySectorLimits($data);
        $current_month_sub_sectors_portfolio = $this->Reports_model->getOutstandingExposureBySubSectorLimits($data);
        $previous_month_sectors_portfolio = [];
        $previous_month_sub_sectors_portfolio = [];


        if(!empty($previous_analysis_month))
        {
            $data['date'] = $previous_analysis_month;
            $previous_month_sectors_portfolio = $this->Reports_model->getOutstandingExposureBySectorLimits($data);
            $previous_month_sub_sectors_portfolio = $this->Reports_model->getOutstandingExposureBySubSectorLimits($data);
        }

        $current_month_sectors_total_amount = 0;
        $current_month_sectors_total_transactions = 0;
        $previous_month_sectors_total_amount = 0;
        $previous_month_sectors_total_transactions = 0;

        for($csp = 0; $csp < count($current_month_sectors_portfolio); $csp++)
        {
            $sector_id = $current_month_sectors_portfolio[$csp]['id'];
            $current_month_sectors_total_amount = $current_month_sectors_total_amount + $current_month_sectors_portfolio[$csp]['amount'];
            $current_month_sectors_total_transactions = $current_month_sectors_total_transactions + $current_month_sectors_portfolio[$csp]['transactions'];

            for($psp = 0; $psp < count($previous_month_sectors_portfolio); $psp++)
            {
                if($previous_month_sectors_portfolio[$psp]['id'] == $sector_id)
                {
                    $previous_month_sectors_total_amount = $previous_month_sectors_total_amount + $previous_month_sectors_portfolio[$psp]['amount'];
                    $previous_month_sectors_total_transactions = $previous_month_sectors_total_transactions + $previous_month_sectors_portfolio[$psp]['transactions'];
                    $current_month_sectors_portfolio[$csp]['amount_compare'] = ($current_month_sectors_portfolio[$csp]['amount'] > $previous_month_sectors_portfolio[$psp]['amount']) ? 'high' : ($current_month_sectors_portfolio[$csp]['amount'] < $previous_month_sectors_portfolio[$psp]['amount']) ? 'low' : 'equal';
                    $current_month_sectors_portfolio[$csp]['transactions_compare'] = ($current_month_sectors_portfolio[$csp]['transactions'] > $previous_month_sectors_portfolio[$psp]['transactions']) ? 'high' : ($current_month_sectors_portfolio[$csp]['transactions'] < $previous_month_sectors_portfolio[$psp]['transactions']) ? 'low' : 'equal';

                }
            }
        }


        for($csp = 0; $csp < count($current_month_sub_sectors_portfolio); $csp++)
        {
            $sector_id = $current_month_sub_sectors_portfolio[$csp]['id'];
            for($psp = 0; $psp < count($previous_month_sub_sectors_portfolio); $psp++)
            {
                if($previous_month_sub_sectors_portfolio[$psp]['id'] == $sector_id)
                {
                    $current_month_sub_sectors_portfolio[$csp]['amount_compare'] = ($current_month_sub_sectors_portfolio[$csp]['amount'] > $previous_month_sub_sectors_portfolio[$psp]['amount']) ? 'high' : ($current_month_sub_sectors_portfolio[$csp]['amount'] < $previous_month_sub_sectors_portfolio[$psp]['amount']) ? 'low' : 'equal';
                    $current_month_sub_sectors_portfolio[$csp]['transactions_compare'] = ($current_month_sub_sectors_portfolio[$csp]['transactions'] > $previous_month_sub_sectors_portfolio[$psp]['transactions']) ? 'high' : ($current_month_sub_sectors_portfolio[$csp]['transactions'] < $previous_month_sub_sectors_portfolio[$psp]['transactions']) ? 'low' : 'equal';

                }
            }
        }
        $sectors = $current_month_sectors_portfolio;
        $sub_sectors = $current_month_sub_sectors_portfolio;

        $total_compare['total_amount'] = $current_month_sectors_total_amount;
        $total_compare['total_transactions'] = $current_month_sectors_total_transactions;
        $total_compare['total_amount_compare'] = ($current_month_sectors_total_amount > $previous_month_sectors_total_amount) ? 'high' : ($current_month_sectors_total_amount < $previous_month_sectors_total_amount) ? 'low' : 'equal';
        $total_compare['total_transactions_compare'] = ($current_month_sectors_total_transactions > $previous_month_sectors_total_transactions) ? 'high' : ($current_month_sectors_total_transactions < $previous_month_sectors_total_transactions) ? 'low' : 'equal';

        for($s=0; $s<count($sectors);$s++)
        {
            $items = [];
            $item_index = 0;
            $sector_id = $sectors[$s]['id'];
            for($sb=0; $sb<count($sub_sectors); $sb++)
            {
                if($sub_sectors[$sb]['parent_id'] == $sector_id)
                {
                    $items[$item_index] = $sub_sectors[$sb];
                    $item_index ++;
                }
            }
            $sectors[$s]['sub_sectors'] = $items;
        }

        $sector_limits = ['sector_limits_total' => $total_compare, 'sectors' => $sectors];

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> $sector_limits);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function risk_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$this->form_validator->add_rules('risk_name', array('required'=> $this->lang->line('risk_name_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['sector']) && !is_array($data['sector']) && $data['sector']!=''){ $data['sector'] = array_values(explode(',',$data['sector'])); }
        if(isset($data['country_id']) && !is_array($data['country_id']) && $data['country_id']!=''){ $data['country_id'] = array_values(explode(',',$data['country_id'])); }
        if(isset($data['focus_area']) && !is_array($data['focus_area']) && $data['focus_area']!=''){ $data['focus_area'] = array_values(explode(',',$data['focus_area'])); }

        $result = $this->Crm_model->getRisk($data);
        $total = $this->Crm_model->getTotalRisk($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result[$s]['id_crm_project']);
            if(!empty($result[$s]['loan_amount']))
                $result[$s]['loan_amount'] = $result[$s]['loan_amount'][0]['amount'];
            else
                $result[$s]['loan_amount'] = 0;
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> array('data' => $result,'total_records' => $total[0]['total_records']));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function calculateND($x)
    {
        $b1 =  0.319381530;
        $b2 = -0.356563782;
        $b3 =  1.781477937;
        $b4 = -1.821255978;
        $b5 =  1.330274429;
        $p  =  0.2316419;
        $c  =  0.39894228;

        if($x >= 0.0)
        {
            $t = 1.0 / ( 1.0 + $p * $x );
            return (1.0 - $c * exp( -$x * $x / 2.0 ) * $t *
                ( $t *( $t * ( $t * ( $t * $b5 + $b4 ) + $b3 ) + $b2 ) + $b1 ));
        }
        else
        {
            $t = 1.0 / ( 1.0 - $p * $x );
            return ( $c * exp( -$x * $x / 2.0 ) * $t *
                ( $t *( $t * ( $t * ( $t * $b5 + $b4 ) + $b3 ) + $b2 ) + $b1 ));
        }
    }
    public function NormSInv($probability) {
        $a1 = -39.6968302866538;
        $a2 = 220.946098424521;
        $a3 = -275.928510446969;
        $a4 = 138.357751867269;
        $a5 = -30.6647980661472;
        $a6 = 2.50662827745924;
        $b1 = -54.4760987982241;
        $b2 = 161.585836858041;
        $b3 = -155.698979859887;
        $b4 = 66.8013118877197;
        $b5 = -13.2806815528857;

        $c1 = -7.78489400243029E-03;
        $c2 = -0.322396458041136;
        $c3 = -2.40075827716184;
        $c4 = -2.54973253934373;
        $c5 = 4.37466414146497;
        $c6 = 2.93816398269878;
        $d1 = 7.78469570904146E-03;
        $d2 = 0.32246712907004;
        $d3 = 2.445134137143;
        $d4 =  3.75440866190742;
        $p_low = 0.02425;
        $p_high = 1 - $p_low;
        $q = 0;
        $r = 0;
        $normSInv = 0;
        if ($probability < 0 ||
            $probability > 1)
        {
            throw new \Exception("normSInv: Argument out of range.");
        } else if ($probability < $p_low) {
            $q = sqrt(-2 * log($probability));
            $normSInv = ((((($c1 * $q + $c2) * $q + $c3) * $q + $c4) * $q + $c5) * $q + $c6) / (((($d1 * $q + $d2) * $q + $d3) * $q + $d4) * $q + 1);
        } else if ($probability <= $p_high) {
            $q = $probability - 0.5;
            $r = $q * $q;
            $normSInv = ((((($a1 * $r + $a2) * $r + $a3) * $r + $a4) * $r + $a5) * $r + $a6) * $q / ((((($b1 * $r + $b2) * $r + $b3) * $r + $b4) * $r + $b5) * $r + 1);
        } else {
            $q = sqrt(-2 * log(1 - $probability));
            $normSInv = -((((($c1 * $q + $c2) * $q + $c3) * $q + $c4) * $q + $c5) * $q + $c6) /(((($d1 * $q + $d2) * $q + $d3) * $q + $d4) * $q + 1);

        }
        return $normSInv;
    }
    public function valueAtRisk_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('analysis_date', array('required'=> $this->lang->line('date_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['date'] = date('m-Y',strtotime($data['analysis_date']));
        $all_facilities = $this ->Reports_model->getAllValueAtRisk($data);
        $loan_portfolio = 0;
        $expected_loss_in_percentage = 0;
        $expected_loss_in_usd = 0;
        $unexpected_loss_scaled = 0;
        $capital_requirements_scaled = 0;
        for($af =1; $af < count($all_facilities); $af++)
        {
            $facility = $this->calculateVaR($all_facilities[$af]);
            $loan_portfolio = $loan_portfolio + $facility['loan_portfolio'];
            $expected_loss_in_percentage = $expected_loss_in_percentage + $facility['expected_loss_in_percentage'];
            $expected_loss_in_usd = $expected_loss_in_usd + $facility['expected_loss_in_usd'];
            $unexpected_loss_scaled = $unexpected_loss_scaled + $facility['unexpected_loss_scaled'];
            //echo $unexpected_loss_scaled.':::'.$facility['unexpected_loss_scaled'].'<br/>';
            $capital_requirements_scaled = $capital_requirements_scaled + $facility['total_capital_requirement_scaled'];
        }
        $facilities_total = ['loan_portfolio' => $loan_portfolio, 'expected_loss_in_percentage' => $expected_loss_in_percentage, 'expected_loss_in_usd' => $expected_loss_in_usd, 'unexpected_loss_scaled' => $unexpected_loss_scaled, 'capital_requirements_scaled' => $capital_requirements_scaled ];
        $total_records = count($all_facilities);
        $facilities = $this ->Reports_model->getValueAtRisk($data);
        for($f=0; $f < count($facilities); $f++)
        {
            $facilities[$f] = $this->calculateVaR($facilities[$f]);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=> array('data' => $facilities,'total_records' => $total_records, 'facilities_total' => $facilities_total));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function calculateVaR($facilities)
    {
        $net_loan_amount = $facilities['loan_portfolio'] - $facilities['total_provision'];
        $facilities['net_loan_amount'] = $net_loan_amount;
        $maturity_years = 1;
        $maturity_date = $facilities['facility_maturity_date'];
        if(!empty($maturity_date))
        {
            $current_date = date('Y-m-d');
            $maturity_date = new DateTime($maturity_date);
            $current_date = new DateTime($current_date);
            $diff = $maturity_date->diff($current_date);
            $maturity_years = $diff->y;
            if($maturity_years == 0) $maturity_years = 1;
        }

        $facilities['maturity_years'] = $maturity_years;
        $expected_loss_in_percentage = ($facilities['pd']/100) * ($facilities['lgd']/100);
        $facilities['expected_loss_in_percentage'] = $expected_loss_in_percentage * 100;
        $expected_loss_in_usd = $net_loan_amount * ($facilities['lgd']/100) * ($facilities['pd']/100);
        $facilities['expected_loss_in_usd'] = $expected_loss_in_usd; // =0.12*(1-EXP(-0.5*PD))/(1-EXP(-0.5))
        $asset_correlation_value1 = 0.12*(1-EXP(-0.5*($facilities['pd']/100)))/(1-EXP(-0.5));
        $asset_correlation_value2 = 0.24*(1-(1-EXP(-0.5*($facilities['pd']/100)))/(1-EXP(-0.5)));
        $facilities['asset_correlation_value1'] = $asset_correlation_value1;
        $facilities['asset_correlation_value2'] = $asset_correlation_value2;
        $asset_correlation_percentage = ($asset_correlation_value1+$asset_correlation_value2) * 100;
        $facilities['asset_correlation_percentage'] = number_format($asset_correlation_percentage,2);
        $b = pow((0.11852-0.05478*log(($facilities['pd']/100))),2);
        $facilities['b'] = $b;
        $maturity_adjustment = pow((1-1.5*$b),(-1))*(1+($maturity_years-2.5)*$b);
        $facilities['maturity_adjustment'] = $maturity_adjustment; // ==NORMSDIST(/(1-rho)^0.5)
        $unexpected_loss = $net_loan_amount*$facilities['lgd']/100*($this->calculateND(($this->NormSInv($facilities['pd']/100)-pow($asset_correlation_percentage/100,0.5)*$this->NormSInv(1-$facilities['confidence_level']/100))/pow((1-$asset_correlation_percentage/100),0.5))-$facilities['pd']/100)*$maturity_adjustment;
        $facilities['unexpected_loss'] = $unexpected_loss;
        $total_capital_requirement = $expected_loss_in_usd+$unexpected_loss;
        $facilities['total_capital_requirement'] = $total_capital_requirement; //$facilities[$f]['scaling_factor']
        $unexpected_loss_scaled = $unexpected_loss*$facilities['scaling_factor'];
        $facilities['unexpected_loss_scaled'] = $unexpected_loss_scaled;
        $total_capital_requirement_scaled = $total_capital_requirement*$facilities['scaling_factor'];
        $facilities['total_capital_requirement_scaled'] = $total_capital_requirement_scaled;
        return $facilities;
    }

}
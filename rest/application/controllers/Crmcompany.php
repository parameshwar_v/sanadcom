<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Crmcompany extends REST_Controller
{

    public $allowed_created_by = [];
    public $allowed_company_approval_roles = [];
    public $current_user = 0;
    public $order_data = 0;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Company_model');
        $this->load->model('Activity_model');
        $this->load->model('Project_model');
        $this->load->model('Workflow_model');
        $this->load->model('Crm_company_model');

        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];
        }

        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if(!empty($user_info) && $user_info[0]['user_role_id']==3 && $user_info[0]['all_projects']==0)
        {
            $this->current_user = $user_id;
        }
        else{
            $this->current_user = 0;
        }
    }

    public function ratiosCategory_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_company_model->getRatiosCategory($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['ratios'] = $this->Crm_company_model->getRatios(array('ratio_category_id' => $result[$s]['id_ratio_category']));
            $result[$s]['sector_id'] = explode(',',$result[$s]['sector_id']);
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ratiosCategory_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('category_name', array('required'=> $this->lang->line('category_name_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('sector_id', array('required'=> $this->lang->line('sector_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!is_array($data['sector_id'])){
            $data['sector_id'] = explode(',',$data['sector_id']);
        }

        $ratio_category_id = 0;
        if(isset($data['id_ratio_category'])){ $ratio_category_id = $data['id_ratio_category']; }

        $check_name = $this->Crm_company_model->getRatiosCategory(array('category_name' => trim($data['category_name']),'id_ratio_category_not' => $ratio_category_id));
       if(!empty($check_name)){
            $result = array('status'=>FALSE,'error'=>array('category_name' => $this->lang->line('category_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $insert_data = array(
            'company_id' => $data['company_id'],
            'category_name' => $data['category_name']
        );
        $add = $update = $delete = array();

        if(isset($data['id_ratio_category'])){
            $insert_data['id_ratio_category'] = $data['id_ratio_category'];
            $insert_data['updated_by'] = $data['created_by'];
            $insert_data['updated_date_time'] = currentDate();
            $this->Crm_company_model->updateRatioCategory($insert_data);
            $existing_sector = $this->Crm_company_model->getRatioCategorySector(array('ratio_category_id' => $data['id_ratio_category']));
            $existing_sectors = array_map(function($i){ return $i['sector_id']; },$existing_sector);
            //echo "<pre>"; print_r($data['sector_id']); exit;
            $sector = array_values(array_diff($data['sector_id'],$existing_sectors));
            for($s=0;$s<count($sector);$s++){
                $add[] = array(
                    'ratio_category_id' => $data['id_ratio_category'],
                    'sector_id' => $sector[$s]
                );
            }

            $sector = array_values(array_diff($existing_sectors,$data['sector_id']));
            for($s=0;$s<count($sector);$s++){
                $this->Crm_company_model->deleteRatioCategorySector(array('ratio_category_id' => $data['id_ratio_category'],'sector_id' => $sector[$s]));
            }
            $msg = $this->lang->line('ratio_category_update');
        }
        else{
            $insert_data['created_by'] = $data['created_by'];
            $insert_data['created_date_time'] = currentDate();
            $ratio_category_id = $this->Crm_company_model->addRatioCategory($insert_data);
            for($s=0;$s<count($data['sector_id']);$s++){
                $add[] = array(
                    'ratio_category_id' => $ratio_category_id,
                    'sector_id' => $data['sector_id'][$s]
                );
            }
            $msg = $this->lang->line('ratio_category_add');
        }

        if(!empty($add))
            $this->Crm_company_model->addRatioCategorySector($add);

        $result = $this->Crm_company_model->getRatiosCategory($data);
        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ratios_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('ratio_category_id', array('required'=> $this->lang->line('ratio_category_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_company_model->getRatios($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function ratios_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('ratio_category_id', array('required'=> $this->lang->line('ratio_category_id_req')));
        $this->form_validator->add_rules('ratio_name', array('required'=> $this->lang->line('ratio_name_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $insert_data = array(
            'ratio_category_id' => $data['ratio_category_id'],
            'ratio_name' => $data['ratio_name'],
            'ratio_type' => $data['ratio_type'],
            'upper_ceiling' => $data['upper_ceiling'],
            'lower_celing' => $data['lower_celing']
        );
        if(isset($data['id_ratio'])){
            $insert_data['id_ratio'] = $data['id_ratio'];
            $insert_data['updated_by'] = $data['created_by'];
            $insert_data['updated_date_time'] = currentDate();
            $this->Crm_company_model->updateRatios($insert_data);
            $msg = $this->lang->line('ratios_update');
        }
        else{
            $insert_data['created_by'] = $data['created_by'];
            $insert_data['created_date_time'] = currentDate();
            $this->Crm_company_model->addRatios($insert_data);
            $msg = $this->lang->line('ratios_add');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRatiosList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_company_model->getCompanyRatiosList($data);


        /*for($s=0;$s<count($result);$s++)
        {
            $result[$s]['trend_data'] = $this->Crm_company_model->getCompanyRatioTrend(array('crm_company_ratio_id' => $result[$s]['id_crm_company_ratio']));
        }*/
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRatios_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        if(isset($data['id_crm_company_ratio'])){
            $result = $this->Crm_company_model->getCompanyRatiosList($data);
            //echo "<pre>"; print_r($result); exit;
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['company_ratio'] = $this->Crm_company_model->getCompanyRatiosData(array('id_crm_company_ratio' => $result[$s]['id_crm_company_ratio']));
                for($r=0;$r<count($result[$s]['company_ratio']);$r++) {
                    if ($result[$s]['company_ratio'][$r]['ratio_type'] == 'trend')
                        $result[$s]['company_ratio'][$r]['ratio_value'] = $this->Crm_company_model->getCompanyRatioTrend(array('crm_company_ratio_data_id' => $result[$s]['company_ratio'][$r]['id_crm_company_ratio_data']));
                }
            }
            $result = $result[0];
        }
        else
        {
            $company_details = $this->Crm_model->getCompany($data['crm_company_id']);

            $data['sector_id'] = $company_details[0]['sub_sector_id'];
            $result = $this->Crm_company_model->getCompanyRatiosData($data);

            if(empty($result))
                $data['sector_id'] = $company_details[0]['sector_id'];
            $result = $this->Crm_company_model->getCompanyRatiosData($data);

            if(empty($result))
                $data['sector_id'] = 0;
            $result = $this->Crm_company_model->getCompanyRatiosData($data);

            $result = array(
                'company_ratio' => $result,
                'comments' => ''
            );
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRatios_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //validating data
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('year', array('required'=> $this->lang->line('trend_start_year_req')));
        $this->form_validator->add_rules('comments', array('required'=> $this->lang->line('comments_req')));
        $this->form_validator->add_rules('crm_company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $ratio = $data['company_ratio'];

        $crm_company_ratio_id = $this->Crm_company_model->addCompanyRatios(array(
            'crm_company_id' => $data['crm_company_id'],
            'year' => $data['year'],
            'comments' => $data['comments'],
            'created_by' => $data['created_by'],
            'created_date_time' => currentDate()
        ));
        for($s=0;$s<count($ratio);$s++)
        {
            $value = $ratio[$s]['ratio_value'];
            if($ratio[$s]['ratio_type']=='trend'){
                $value = 0;
            }
            $insert_data = array(
                'crm_company_ratio_id' =>$crm_company_ratio_id,
                'ratio_id' => $ratio[$s]['id_ratio'],
                'ratio_value' => $value,
                'upper_ceiling' => isset($ratio[$s]['upper_ceiling'])?$ratio[$s]['upper_ceiling']:'',
                'lower_ceiling' => isset($ratio[$s]['lower_ceiling'])?$ratio[$s]['lower_ceiling']:''
            );
            $crm_company_ratio_data_id = $this->Crm_company_model->addCompanyRatiosData($insert_data);
            if($ratio[$s]['ratio_type']=='trend') {
                $trend = $ratio[$s]['ratio_value'];
                $trend_data = array();
                for ($r = 0; $r < count($trend); $r++) {
                    $trend_data[] = array(
                        'crm_company_ratio_data_id' => $crm_company_ratio_data_id,
                        'year' => $trend[$r]['year'],
                        'trend_value' => $trend[$r]['value']
                    );
                }
                if (!empty($trend_data)) {
                    $this->Crm_company_model->addCompanyRatioTrend($trend_data);
                }
            }
        }

        //getting last form last update
        if(isset($data['form_id']))
        {
            $last_update = $this->Project_model->getProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id']));
            if (empty($last_update))
                $this->Project_model->addProjectStageSectionFormLastUpdate(array('crm_company_id' => $data['crm_company_id'], 'project_stage_section_form_id' => $data['form_id'], 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));
            else
                $this->Project_model->updateProjectStageSectionFormLastUpdate(array('id_project_stage_section_form_last_update' => $last_update[0]['id_project_stage_section_form_last_update'], 'updated_by' => $data['created_by'], 'updated_date_time' => currentDate()));

            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'],'crm_module_type' => 'company','reference_id' => $data['form_id'], 'reference_type' => 'form'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['created_by'], 'created_date_time' => currentDate(), 'updated_date_time' => currentDate()));
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('ratios_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRatios_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('id_crm_company_ratio', array('required'=> $this->lang->line('id_crm_company_ratio_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_company_model->deleteCompanyRatioDataTrend($data);
        $this->Crm_company_model->deleteCompanyRatios($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('ratios_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function checkList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details=$this->Crm_model->checkRecord('crm_project', array('id_crm_project' => $data['crm_project_id']));
        $product_id=$project_details[0]['product_id'];
        $result = $this->Master_model->getMasterChild(array('child_key' => $data['form_key']));
        $resultArray = array();
        if(!empty($result)) {
            $resultArray = $this->Company_model->getAssessmentQuestionCategory(array('operational_checklist_type' => $result[0]['id_child'],'product_id'=>$product_id));
            for ($r = 0; $r < count($resultArray); $r++) {
                $resultArray[$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $resultArray[$r]['id_assessment_question_category'], 'question_status' => 1,'checklist_project_id' => $data['crm_project_id']));
                for ($sr = 0; $sr < count($resultArray[$r]['question']); $sr++) {
                    if (isset($resultArray[$r]['question'][$sr]['question_option']) && $resultArray[$r]['question'][$sr]['question_option'] != '') {
                        $resultArray[$r]['question'][$sr]['question_option'] = json_decode($resultArray[$r]['question'][$sr]['question_option']);
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$resultArray);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCheckList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('project_form_id', array('required'=> $this->lang->line('project_form_id_req')));
        $this->form_validator->add_rules('form_key', array('required'=> $this->lang->line('form_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details=$this->Crm_model->checkRecord('crm_project', array('id_crm_project' => $data['crm_project_id']));
        $product_id=$project_details[0]['product_id'];
        $result = $this->Master_model->getMasterChild(array('child_key' => $data['form_key']));
        $resultArray = array();
        if(!empty($result)) {
            $resultArray = $this->Company_model->getAssessmentQuestionCategory(array('operational_checklist_type' => $result[0]['id_child'],'product_id'=>$product_id));
            for ($r = 0; $r < count($resultArray); $r++) {
                $resultArray[$r]['question'] = $this->Crm_company_model->getProjectCheckListQuestions(array('crm_project_id' => $data['crm_project_id'], 'project_form_id' => $data['project_form_id'],'assessment_question_category_id' => $resultArray[$r]['id_assessment_question_category']));
                for ($sr = 0; $sr < count($resultArray[$r]['question']); $sr++) {

                    if(isset($resultArray[$r]['question'][$sr]['project_facility_id']) && $resultArray[$r]['question'][$sr]['project_facility_id']!='')
                        $resultArray[$r]['question'][$sr]['project_facility_id'] = explode(',',$resultArray[$r]['question'][$sr]['project_facility_id']);

                    if (isset($resultArray[$r]['question'][$sr]['question_option']) && $resultArray[$r]['question'][$sr]['question_option'] != '') {
                        $resultArray[$r]['question'][$sr]['question_option'] = json_decode($resultArray[$r]['question'][$sr]['question_option']);
                    }
                    $resultArray[$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $resultArray[$r]['question'][$sr]['id_assessment_question']));

                    if (isset($resultArray[$r]['question'][$sr]['answer']->assessment_answer) && $resultArray[$r]['question'][$sr]['answer']->assessment_answer != '') {
                        if ($resultArray[$r]['question'][$sr]['question_type'] == 'checkbox')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = json_decode($resultArray[$r]['question'][$sr]['answer']->assessment_answer);
                        else if ($resultArray[$r]['question'][$sr]['question_type'] == 'file')
                            $resultArray[$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($resultArray[$r]['question'][$sr]['answer']->assessment_answer);

                        $doc = array();
                        for($s=0;$s<count($resultArray[$r]['question'][$sr]['answer']);$s++)
                        {
                            $doc = $this->Crm_model->getDocument(array('uploaded_from_id' => $resultArray[$r]['question'][$sr]['answer']->id_assessment_answer,'module_type' => $data['form_key']));
                            for($doci=0;$doci<count($doc);$doci++) {
                                $doc[$doci]['full_path'] = getImageUrl($doc[$doci]['document_source'], 'profile');
                            }
                            $resultArray[$r]['question'][$sr]['answer']->document = $doc;
                        }
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$resultArray);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCheckList_post()
    {
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['assessment_question_id']) && $data['assessment_question_id']==''){
            unset($data['assessment_question_id']);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('project_form_id', array('required'=> $this->lang->line('project_form_id_req')));
        $this->form_validator->add_rules('assessment_question_id', array('required'=> $this->lang->line('assessment_question_id_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $existing_project_checklist = $this->Crm_company_model->getProjectCheckList($data);
        $existing_questions = array();
        if(!empty($existing_project_checklist))
            $existing_questions = array_map(function($i){ return $i['assessment_question_id']; },$existing_project_checklist);

        $add_questions = array_diff($data['assessment_question_id'],$existing_questions);
        //$delete_questions = array_diff($existing_questions,$data['assessment_question_id']);
        $add = $delete = array();

        foreach($add_questions as $i){
            $add[] = array(
                'crm_project_id' => $data['crm_project_id'],
                'project_form_id' => $data['project_form_id'],
                'assessment_question_id' => $i,
                'created_by' => $data['created_by'],
                'updated_by' => $data['created_by'],
                'created_date_time' => currentDate(),
                'updated_date_time' => currentDate()
            );
        }

        if(!empty($add)){
            $this->Crm_company_model->addProjectCheckList_batch($add);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('check_list_add'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCheckList_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_project_checklist', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_assessment_answer']))
            $this->Company_model->deleteAssessmentAnswer(array('id_assessment_answer' => $data['id_assessment_answer']));
        $this->Crm_company_model->deleteCheckListFacility(array('project_checklist_id' => $data['id_project_checklist']));
        $this->Crm_company_model->deleteCheckList(array('id_project_checklist' => $data['id_project_checklist']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('check_list_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactIdentification_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_contact_id', array('required' => $this->lang->line('contact_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_company_model->getContactIdentification($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['document'] = $this->Crm_model->getDocument(array('uploaded_from_id' => $result[$s]['id_contact_identification'],'module_type' => 'contact','form_key' => 'contact_identification'));
            for($r=0;$r<count($result[$s]['document']);$r++)
            {
                $result[$s]['document'][$r]['full_path'] = getImageUrl($result[$s]['document'][$r]['document_source'],'file');
            }
        }

        $result = array('status' => TRUE, 'message' => '', 'data' => array('data' => $result));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactIdentification_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_contact_id', array('required' => $this->lang->line('contact_id_req')));
        $this->form_validator->add_rules('type_of_id', array('required' => $this->lang->line('type_of_id_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('company_id', array('required' => $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $update_data = array(
            'crm_contact_id' => $data['crm_contact_id'],
            'number' => isset($data['number']) ? $data['number'] : '',
            /*'expiry_date' => isset($data['expiry_date']) ? $data['expiry_date'] : '',*/
            'type_of_id' => $data['type_of_id'],
            'updated_by' => $data['created_by'],
            'updated_date_time' => currentDate()
        );
        if(isset($data['expiry_date'])){
            $update_data['expiry_date'] = $data['expiry_date'];
        }

        if (isset($data['id_contact_identification'])) {
            $contact_identification = $data['id_contact_identification'];
            $update_data['id_contact_identification'] = $data['id_contact_identification'];
            $this->Crm_company_model->updateContactIdentification($update_data);
        } else {
            $update_data['created_by'] = $data['created_by'];
            $update_data['created_date_time'] = currentDate();
            $contact_identification = $this->Crm_company_model->addContactIdentification($update_data);
        }

        $company_id = $data['company_id'];
        $path='uploads/';
        $data['document_type'] = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachments']))
        {
            if(is_array($_FILES['file']['name']['attachments']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachments']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachments'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachments'][$s],$_FILES['file']['name']['attachments'][$s],$path,$company_id,'');

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $contact_identification, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'contact', 'form_key' => 'contact_identification');
                    $document_id = $this->Crm_model->addDocument($document);
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachments'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachments'],$_FILES['file']['name']['attachments'],$path,$company_id,'');

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachments'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $contact_identification, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by'], 'created_date_time' => currentDate(), 'module_type' => 'contact', 'form_key' => 'contact_identification');
                $document_id = $this->Crm_model->addDocument($document);

            }
        }

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactIdentification_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_contact_identification', array('required' => $this->lang->line('id_contact_identification')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_company_model->deleteContactIdentification($data);

        $result = array('status' => TRUE, 'message' => $this->lang->line('info_update'), 'data' => '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRelationSearch_get()
    {
        $data = $this->input->get();
        //echo '<pre>';print_r($data);exit;
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('search_key',array('required'=> $this->lang->line('search_key')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $crm_company_ids = $this->Crm_company_model->getExistingCompanyRelation($data);
        $crm_company_ids = array_map(function($i){ return $i['company_id']; },$crm_company_ids);
        $result = $this->Crm_company_model->getCompanyRelationSearch(array('crm_company_id' => $data['crm_company_id'],'crm_company_ids' => array_values($crm_company_ids), 'search_key'=>$data['search_key']));

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRelation_get()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('crm_company_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*$parent = $this->Crm_company_model->getParentCompany($data);
        $child = $this->Crm_company_model->getChildCompany($data);*/
        $resultArray = array();
        $parent = $this->Crm_company_model->getParentCompany($data);
        foreach($parent as $i){
            $resultArray[] = array(
                'id_crm_company_relation' => $i['id_crm_company_relation'],
                'id_crm_company' => $i['id_crm_company'],
                'company_name' => $i['company_name'],
                'email' => $i['email'],
                'phone_number' => $i['phone_number'],
                'company_description' => $i['company_description'],
                'type' => $i['child_name']
            );
        }
        $child = $this->Crm_company_model->getChildCompany($data);
        foreach($child as $i){
            $resultArray[] = array(
                'id_crm_company_relation' => $i['id_crm_company_relation'],
                'id_crm_company' => $i['id_crm_company'],
                'company_name' => $i['company_name'],
                'email' => $i['email'],
                'phone_number' => $i['phone_number'],
                'company_description' => $i['company_description'],
                'type' => $i['child_name']
            );
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$resultArray);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyRelation_post(){
        $message = 'info_update';
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required' => $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('parent_company_id', array('required' => $this->lang->line('crm_company_id_req')));
        $this->form_validator->add_rules('type', array('required' => $this->lang->line('type_req')));
        $this->form_validator->add_rules('created_by', array('required' => $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*if(isset($data['type_id']) && $data['type_id']==1){*/
        $this->Crm_company_model->addCompanyRelation(array(
            'crm_company_id' => $data['crm_company_id'],
            'parent_company_id' => $data['parent_company_id'],
            'relation_id' => $data['type'],
            'created_by' => $data['created_by'],
            'created_date_time' => currentDate()
        ));
        $message = 'comp_rel_add';
        /*}
        else{
            $this->Crm_company_model->addCompanyRelation(array(
                'crm_company_id' => $data['parent_company_id'],
                'parent_company_id' => $data['crm_company_id'],
                'created_by' => $data['created_by'],
                'created_date_time' => currentDate()
            ));
        }*/
        $result = array('status'=>TRUE, 'message' => $this->lang->line($message), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function companyRelation_delete()
    {
        $data = $this->input->get();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_crm_company_relation', array('required' => $this->lang->line('crm_company_relation_id_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_company_model->deleteCompanyRelation($data);

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /* * * 22-05-2017 * * */

    public function emailVerification_post()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('type', array('required' => $this->lang->line('type_req')));
        $this->form_validator->add_rules('module_id', array('required' => $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('email', array('required' => $this->lang->line('email_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $code = generatePassword(6);
        if(isset($data['type']) && $data['type']=='contact')
        {
            $this->Crm_model->updateContact(array(
                'id_crm_contact' => $data['module_id'],
                'verification_code' => $code,
                'is_verified' => 0,
            ));
        }
        else if(isset($data['type']) && $data['type']=='company')
        {
            $this->Crm_model->updateCompany(array(
                'id_crm_company' => $data['module_id'],
                'verification_code' => $code,
                'is_verified' => 0,
            ));
        }

        sendmail($data['email'],$this->lang->line('email_verification_subject'),str_replace('{code}',$code,$this->lang->line('email_verification_message')));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('email_verification'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function emailVerificationCode_post()
    {
        $data = $this->input->post();
        //echo '<pre>'; print_r($data);exit;
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => array('error' => $this->lang->line('invalid_data')), 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('code', array('required' => $this->lang->line('verification_code_req')));
        $this->form_validator->add_rules('module_id', array('required' => $this->lang->line('module_id_req')));
        $this->form_validator->add_rules('type', array('required' => $this->lang->line('type_req')));
        $validated = $this->form_validator->validate($data);
        if ($validated != 1) {
            $result = array('status' => FALSE, 'error' => $validated, 'data' => '');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['type']) && $data['type']=='contact')
        {
            $result = $this->Crm_model->getContactDetails(array(
                'id_crm_contact' => $data['module_id'],
                'verification_code' => $data['code']
            ));

            if(!empty($result)){
                $this->Crm_model->updateContact(array(
                    'id_crm_contact' => $data['module_id'],
                    'is_verified' => 1,
                    'verification_date' => currentDate()
                ));
            }

        }
        else if(isset($data['type']) && $data['type']=='company')
        {
            $result = $this->Crm_model->getCompanyDetails(array(
                'id_crm_company' => $data['module_id'],
                'verification_code' => $data['code']
            ));

            if(!empty($result)){
                $result = $this->Crm_model->updateCompany(array(
                    'id_crm_company' => $data['module_id'],
                    'is_verified' => 1,
                    'verification_date' => currentDate()
                ));
            }
        }

        if(empty($result))
        {
            $result = array('status'=>FALSE, 'error' => array('code' => $this->lang->line('verification_code_invalid')), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('email_verification_success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

}
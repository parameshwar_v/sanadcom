<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2015-12-01
 * Time: 03:58 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
class Master extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Master_model');
        $this->load->model('Company_model');
    }

    public function sectorList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorSubSectorList($data);
        $total_records=$this->Master_model->getSectorCount($data);
        if(isset($data['company_id']))
        {
            array_unshift($result,array('id_sector' => "0", 'parent_id' => "0", 'sector_name' => 'All Sectors', 'parent' => ''));
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sector_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorsList($data);
        $total_records=$this->Master_model->getSectorCount();
        if(isset($data['company_id']))
        {
            array_unshift($result,array('id_sector' => "0", 'parent_id' => "0", 'sector_name' => 'All Sectors', 'parent' => ''));
        }
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function sectorById_get($id)
    {
        $data['id_sector'] = $id;
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_sector', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getSector($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function subSector_get()
    {
        $data = $this->input->get();
        //validating data

        $idRule = array('required'=> $this->lang->line('sector_req'));
        if(!isset($data['id_sector']) && !isset($data['parent_sector_name']))
            $this->form_validator->add_rules('id_sector', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_sector']) && $data['id_sector']==null){
            $result = array();
        }
        else if(isset($data['parent_sector_name'])){
            $result = $this->Master_model->getSectorByName($data);
        }
        else{
            $result = $this->Master_model->getSubSector($data);
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sector_post()//add
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        //print_r($data);
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('sector_name', array('required'=> $this->lang->line('sector_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSector=$this->Master_model->checkSectorExist($data);
        if($existSector){
            $result = array('status'=>FALSE,'error'=> array('sector_name' => $this->lang->line('sector_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertSector($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('sector_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function sector_put()//edit
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('sector_name', array('required' => $this->lang->line('name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Master_model->updateSector($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('sector_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    // country module
    public function country_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getCountriesList($data);
        $total_records=$this->Master_model->getCountriesCount();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function countryById_get($id)
    {
        $data['id_country'] = $id;
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_country', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getCountry($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('country_name', array('required' => $this->lang->line('country_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existCountry=$this->Master_model->checkCountryExist($data);
        if($existCountry){
            $result = array('status'=>FALSE,'error'=>array('country_name'=> $this->lang->line('country_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertCountry($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('country_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $data = $this->input->post();
        }
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('country_name', array('required' => $this->lang->line('country_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existCountry=$this->Master_model->checkCountryExist($data);
        if($existCountry){
            $result = array('status'=>FALSE,'error'=>array('country_name'=> $this->lang->line('country_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateCountry($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('country_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_country', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteCountry($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('country_delete'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    //country end

    // branch start
    public function branch_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getBranchesCount($data);
        $result = $this->Master_model->getBranchesList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branchTypeById_get($id)
    {
        $data['id_branch_type'] = $id;
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_branch_type', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getBranchType($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branch_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('branch_type_name', array('required'=> $this->lang->line('branch_type_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existBranch=$this->Master_model->checkBranchExist($data);
        if($existBranch){
            $result = array('status'=>FALSE,'error'=>array('branch_type_name'=> $this->lang->line('branch_type_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertBranch($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('branch_type_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branch_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('branch_type_name', array('required' => $this->lang->line('name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$existBranch=$this->Master_model->checkBranchExist($data);
        $existBranch = false;
        if($existBranch){
            $result = array('status'=>FALSE,'error'=>array('branch_type_name'=>$this->lang->line('branch_type_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateBranch($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('branch_type_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function plansList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getPlansCount();
        $result = $this->Master_model->getPlansList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_get($id)
    {
        $result = $this->Master_model->getPlanDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $planNameRules=array('required'=> $this->lang->line('plan_name_req'));
        $loansRules=array('required'=> $this->lang->line('no_of_loans_req'),'numeric'=> $this->lang->line('no_of_loans_num'));
        $usersRules=array('required'=> $this->lang->line('no_of_users_req'),'numeric'=> $this->lang->line('no_of_users_num'));
        $diskSpaceRules=array('required'=> $this->lang->line('disk_space_req'),'numeric'=> $this->lang->line('disk_space_num'));
        $priceLoanRules=array('required'=> $this->lang->line('price_per_loan_req'),'numeric'=> $this->lang->line('price_per_loan_num'));

        $this->form_validator->add_rules('plan_name', $planNameRules);
        $this->form_validator->add_rules('no_of_loans', $loansRules);
        $this->form_validator->add_rules('no_of_users', $usersRules);
        $this->form_validator->add_rules('total_disk_space', $diskSpaceRules);
        $this->form_validator->add_rules('price_per_loan', $priceLoanRules);

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updatePlan($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('plan_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_post()
    {
        ////$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $planNameRules=array('required'=> $this->lang->line('plan_name_req'));
        $loansRules=array('required'=> $this->lang->line('no_of_loans_req'),'numeric'=> $this->lang->line('no_of_loans_num'));
        $usersRules=array('required'=> $this->lang->line('no_of_users_req'),'numeric'=> $this->lang->line('no_of_users_num'));
        $diskSpaceRules=array('required'=> $this->lang->line('disk_space_req'),'numeric'=> $this->lang->line('disk_space_num'));
        $priceLoanRules=array('required'=> $this->lang->line('price_per_loan_req'),'numeric'=> $this->lang->line('price_per_loan_num'));

        $this->form_validator->add_rules('plan_name', $planNameRules);
        $this->form_validator->add_rules('no_of_loans', $loansRules);
        $this->form_validator->add_rules('no_of_users', $usersRules);
        $this->form_validator->add_rules('price_per_loan', $priceLoanRules);
        $this->form_validator->add_rules('total_disk_space', $diskSpaceRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertPlan($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('plan_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $planRules = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_plan', $planRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deletePlan($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //risk
    public function risk_get($id)
    {
        $result = $this->Master_model->getRiskDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function riskList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getRiskCount();
        $result = $this->Master_model->getRiskList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function riskItems_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getRiskList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function risk_post()
    {
        ////$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('risk_name', array('required' => $this->lang->line('risk_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existRisk=$this->Master_model->checkRiskExist($data);
        if($existRisk){
            $result = array('status'=>FALSE,'error'=>array('risk_name'=> $this->lang->line('risk_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_risk_type']) && $data['id_risk_type']>0)
        {
            $result = $this->Master_model->updateRisk($data);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('risk_update'), 'data'=>$result);
        }
        else
        {
            $result = $this->Master_model->insertRisk($data);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('risk_add'), 'data'=>$result);
        }
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function risk_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('risk_name', array('required' => $this->lang->line('risk_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existRisk=$this->Master_model->checkRiskExist($data);
        if($existRisk){
            $result = array('status'=>FALSE,'error'=>array('risk_name'=>$this->lang->line('risk_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateRisk($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('risk_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //social
    public function social_get($id)
    {
        $result = $this->Master_model->getSocialDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function socialList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getSocialCount();
        $result = $this->Master_model->getSocialList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function social_post()
    {
        ////$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_social_network', array('required' => $this->lang->line('business_social_network_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSocial=$this->Master_model->checkSocialExist($data);
        if($existSocial){
            $result = array('status'=>FALSE,'error'=>array('business_social_network'=>$this->lang->line('business_social_network_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertSocial($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('business_social_network_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function social_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_social_network', array('required' => $this->lang->line('business_social_network_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSocial=$this->Master_model->checkSocialExist($data);
        if($existSocial){
            $result = array('status'=>FALSE,'error'=>array('business_social_network'=>$this->lang->line('business_social_network_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateSocial($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('business_social_network_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //contact
    public function contact_get($id)
    {
        $result = $this->Master_model->getContactDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contactList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getContactCount();
        $result = $this->Master_model->getContactList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contact_post()
    {
        ////$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_contact', array('required' => $this->lang->line('business_contact_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existContact=$this->Master_model->checkContactExist($data);
        if($existContact){
            $result = array('status'=>FALSE,'error'=>array('business_contact'=>$this->lang->line('business_contact_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertContact($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('business_contact_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contact_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $data = $this->input->post();
        }
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_contact', array('required' => $this->lang->line('business_contact_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existContact=$this->Master_model->checkContactExist($data);
        if($existContact){
            $result = array('status'=>FALSE,'error'=>array('business_contact'=>$this->lang->line('business_contact_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateContact($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('business_contact_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategoryList_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getBankCategoryList($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategory_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getBankCategoryList($data);
        $total_records=$this->Master_model->getBankCategoriesCount();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategoryById_get($id)
    {
        $result = $this->Master_model->getBankCategoryDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategory_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $data = $this->input->post();
        }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('bank_category_name', array('required' => $this->lang->line('bank_category_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateBankCategory($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('bank_category_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function bankCategory_post()
    {
        ////$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('bank_category_name', array('required' => $this->lang->line('bank_category_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertBankCategory($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('bank_category_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function bankCategory_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $req = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteBankCategory($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_get(){
        $data=$this->input->get();
        $total_records=$this->Master_model->getApprovalRolesCount($data);
        $result = $this->Master_model->getApprovalRoles($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRoleById_get($id)
    {
        $data['id_approval_role'] = $id;
        //validating data
        $idRule = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_approval_role', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('approval_name', array('required' => $this->lang->line('approval_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('approval_role_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('approval_name', array('required' => $this->lang->line('approval_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('approval_role_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalRole_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $req = array('required'=> $this->lang->line('id_req'));
        $this->form_validator->add_rules('id_approval_role', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $currency = array();
        if(!empty($data) && isset($data['company_id'])){
            $company_details = $this->Company_model->getCompany(array('id_company'=>$data['company_id']));

            if(!empty($company_details) && $company_details->currency_id!='' || $company_details->currency_id!=0) {
                //$data['currency_id'] = $company_details->currency_id;
            }
        }

        $result = $this->Master_model->getCurrency($data);
        for($s=0;$s<count($result);$s++)
        {
            if(isset($data['id_currency']) && $data['id_currency']!='')
                $result->country_flag = getImageUrl($result->country_flag,'flag');
            else
                $result[$s]['country_flag'] = getImageUrl($result[$s]['country_flag'],'flag');
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currencyList_get()
    {
        $data = $this->input->get();
        if(!isset($data['company_id'])){ $data['company_id']=null; }
        $result = $this->Master_model->getCurrency($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['country_flag'] = getImageUrl($result[$s]['country_flag'],'flag');
        }
        $total = $this->Master_model->getTotalCurrency($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' =>$result,'total_records' => $total[0]['total_records']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectLoanType_get()
    {
        $result = $this->Master_model->getProjectLoanType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectPaymentType_get()
    {
        $result = $this->Master_model->getProjectPaymentType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(!isset($data['currency'])){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['currency'];
        $this->form_validator->add_rules('country_id', array('required'=> $this->lang->line('country_req')));
        $this->form_validator->add_rules('currency_code', array('required'=> $this->lang->line('currency_code_req')));
        $this->form_validator->add_rules('currency_symbol', array('required'=> $this->lang->line('currency_symbol_req')));
        //$this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $currency_details = $this->Master_model->getCurrency(array('country_id' => $data['country_id'],'company_id' => $data['company_id']));
        //echo "<pre>"; print_r($currency_details); exit;
        if(!empty($currency_details)){
            if(isset($data['id_currency']) && $data['id_currency']!='') {
                if($currency_details[0]['id_currency']!=$data['id_currency']){
                    $result = array('status'=>FALSE,'error'=>array('country_id' => $this->lang->line('country_duplicate')),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else {
                $result = array('status'=>FALSE,'error'=>array('country_id' => $this->lang->line('country_duplicate')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['country_flag']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['country_flag'],$_FILES['file']['name']['country_flag'],$path,'','image');

            $data['country_flag'] = $imageName;
        }
        if(isset($data['company_id'])){ unset($data['company_id']);}
        if(isset($data['id_currency']) && $data['id_currency']!='') {
            $data['updated_date_time'] = currentDate();
            $result = $this->Master_model->updateCurrency($data);
            $msg = $this->lang->line('currency_update');
        }
        else {
            $data['created_date_time'] = currentDate();
            $data['created_by'] = $data['updated_by'];
            unset($data['updated_by']);
            $result = $this->Master_model->addCurrency($data);
            $msg = $this->lang->line('currency_add');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function languageList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getLanguage($data);
        $total = $this->Master_model->totalLanguageList();
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['flag'] = getImageUrl($result[$s]['flag'], 'flag');
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $result,'total_records' => $total[0]['total']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function language_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getLanguage($data);

        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['flag'] = getImageUrl($result[$s]['flag'], 'flag');
        }

        if(isset($data['id_language'])) {
            if(empty($result)){ $result[0] = array(); }
            $result = array('status' => TRUE, 'message' => '', 'data' => $result[0]);
        }
        else
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);

        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function language_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => $this->lang->line('invalid_data')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['language'];

        $this->form_validator->add_rules('code', array('required'=> $this->lang->line('code_req')));
        $this->form_validator->add_rules('name', array('required'=> $this->lang->line('name_req')));
        $this->form_validator->add_rules('name_in_english', array('required'=> $this->lang->line('name_in_english_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['flag']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['flag'],$_FILES['file']['name']['flag'],$path,'','image');

            $data['flag'] = $imageName;
        }
        else{
            unset($data['flag']);
        }

        $id_language = 0;
        if(isset($data['id_language'])){
            $id_language = $data['id_language'];
            $existing_data = $this->Master_model->getLanguage(array('id_language' => $id_language));
        }

        //code uniqueness
        $check_name = $this->Master_model->getLanguage(array('code' => $data['code'],'id_language_not' => $id_language));
        if(!empty($check_name)){
            $result = array('status'=>FALSE,'error'=>array('code' => $this->lang->line('code_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //name in english uniqueness
        $check_name = $this->Master_model->getLanguage(array('name_in_english' => $data['name_in_english'],'id_language_not' => $id_language));
        if(!empty($check_name)){
            $result = array('status'=>FALSE,'error'=>array('name_in_english' => $this->lang->line('name_in_english_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($id_language)
        {
            if(!empty($existing_data)){
                if(file_exists('application/language/'.$existing_data[0]['code']))
                    rename('application/language/'.$existing_data[0]['code'],'application/language/'.$data['code']);
            }
            $data['updated_date_time'] = currentDate();
            $this->Master_model->updateLanguage($data);
            $msg = $this->lang->line('lang_updated');
        }
        else
        {
            $Default = 'application/language/default';
            if(!is_dir('application/language/'.$data['code'])){
                $this->recurse_copy($Default,'application/language/'.$data['code']);
            }
            $data['created_by'] = $data['updated_by'];
            unset($data['updated_by']);
            $this->Master_model->addLanguage($data);
            $msg = $this->lang->line('lang_added');
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function language_delete()
    {
        $data = $this->input->get();

        $this->form_validator->add_rules('id_language', array('required'=> $this->lang->line('language_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Master_model->deleteLanguage($data);
        $result = array('status'=>TRUE, 'message' =>$this->lang->line('lang_delete'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function recurse_copy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    function idProof_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getIdProof($data);
        $total_records=$this->Master_model->getIdProofCount($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function idProof_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('name', array('required'=> $this->lang->line('name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $id_proof_data = array(
            'name' => $data['name'],
            'created_date_time' => currentDate()
        );

        if(isset($data['id_proof']))
        {
            $id_proof_data = array(
                'name' => $data['name'],
                'id_proof' => $data['id_proof']
            );
            $result = $this->Master_model->updateIdProof($id_proof_data);
            $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Master_model->addIdProof($id_proof_data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function idProofList_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getIdProof($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function idProof_delete()
    {
        $data=$this->input->get();
        $result = $this->Master_model->deleteIdProof($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function master_get()
    {
        $data=$this->input->get();
        $this->form_validator->add_rules('master_key', array('required'=> $this->lang->line('master_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getMaster($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
        //  naresh other start
    function otherList_get()
    {
        $result = $this->Master_model->getOtherList();
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function other_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getOther($data);
        $total_records=$this->Master_model->getOtherCount($data['master_id']);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function otherById_get($id)
    {
        $result = $this->Master_model->getOtherDetails($id);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('success'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function other_post()
    {
        //$data = json_decode(file_get_contents("php://input"), true);
        //if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('child_name', array('required' => $this->lang->line('child_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existMaster=$this->Master_model->checkOtherExist($data);
        if($existMaster){
            $result = array('status'=>FALSE,'error'=>array('child_name'=>$this->lang->line('child_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertOther($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('child_name_add'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function other_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>$this->lang->line('invalid_data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('child_name', array('required' => $this->lang->line('child_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existMaster=$this->Master_model->checkOtherExist($data);
        if($existMaster){
            $result = array('status'=>FALSE,'error'=>array('child_name'=>$this->lang->line('child_name_duplicate')),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateOther($data);
        $result = array('status'=>TRUE, 'message' => $this->lang->line('child_name_update'), 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
?>
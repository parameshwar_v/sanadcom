var nav_data = {
    'client_nav': {
        'title': 'Menu - Client',
        'description': 'Clicked Client Navigation',
    },
    'company_nav': {
        'title': 'Menu - Company',
        'description': 'Clicked Company Navigation',
    },
    'project_nav': {
        'title': 'Menu - Project',
        'description': 'Clicked Project Navigation',
    },
    /*'html_nav': {
        'title': 'Menu - Html',
        'description': 'Clicked Html Navigation',
    },*/

    'customer_nav': {
        'title': 'Menu - Customers',
        'description': 'Clicked Customers Navigation',
    },
    'masters_nav': {
        'title': 'Menu - Masters',
        'description': 'Clicked Masters Navigation',
    },
    'setup_nav': {
        'title': 'Menu - Setup',
        'description': 'Clicked Setup Navigation',
    },
    'setup_page_nav': {
        'title': 'Menu - Setup Page',
        'description': 'Clicked Setup Page Navigation',
    },
    'entity_structure_nav': {
        'title': 'Menu - Entity Structure',
        'description': 'Clicked Entity Structure Navigation',
    },
    'users_nav': {
        'title': 'Menu - Users',
        'description': 'Clicked Users Navigation',
    },
    'roles_nav': {
        'title': 'Menu - Roles',
        'description': 'Clicked Roles Navigation',
    },
    'company_config_nav': {
        'title': 'Menu - Company Config',
        'description': 'Clicked Company Config Navigation',
    },
    'assessment_config_nav': {
        'title': 'Menu - Assessment Config',
        'description': 'Clicked Assessment Config Navigation',
    },
    'assessment_questions_nav': {
        'title': 'Menu - Assessment Questions',
        'description': 'Clicked Assessment Questions Navigation',
    },
    'business_plan_nav': {
        'title': 'Menu - Business Plan',
        'description': 'Clicked HBusiness Plantml Navigation',
    },
    'expert_rating_model_nav': {
        'title': 'Menu - Expert Rating Model',
        'description': 'Clicked Expert Rating Model Navigation',
    },
    'products_config_nav': {
        'title': 'Menu - Products Config',
        'description': 'Clicked Products Config Navigation',
    },
    'products_nav': {
        'title': 'Menu - Products',
        'description': 'Clicked Products Navigation',
    },
    'positive_covenant_nav': {
        'title': 'Menu - Positive Covenant',
        'description': 'Clicked Positive Covenant Navigation',
    },
    'negative_covenant_nav': {
        'title': 'Menu - Negative Covenant',
        'description': 'Clicked Negative Covenant Navigation',
    },
    'representations_warranties_nav': {
        'title': 'Menu - Representations Warranties',
        'description': 'Clicked Representations Warranties Navigation',
    },
    'events_of_defaults_nav': {
        'title': 'Menu - Events Of Defaults',
        'description': 'Clicked Events Of Defaults Navigation',
    },
    'knowledge_management_nav': {
        'title': 'Menu - Knowledge Management',
        'description': 'Clicked Knowledge Management Navigation',
    },
    'covenant_nav': {
        'title': 'Menu - Covenant',
        'description': 'Clicked Covenant Navigation',
    },
    'collateral_nav': {
        'title': 'Menu - Collateral',
        'description': 'Clicked Collateral Navigation',
    },
    'operating_currency_nav': {
        'title': 'Menu - Operating Currency',
        'description': 'Clicked Operating Currency Navigation',
    },
    'roles_management_nav': {
        'title': 'Menu - Roles Management',
        'description': 'Clicked Roles Management Navigation',
    },
    'facility_setup_nav': {
        'title': 'Menu - Facility Setup',
        'description': 'Clicked Facility Setup Navigation',
    },
    'currency_nav': {
        'title': 'Menu - Currency',
        'description': 'Clicked Currency Navigation',
    },
    'sector_nav': {
        'title': 'Menu - Sector',
        'description': 'Clicked Sector Navigation',
    },
    'branch_type_nav': {
        'title': 'Menu - Branch Type',
        'description': 'Clicked Branch Type Navigation',
    },
    'country_nav': {
        'title': 'Menu - Country',
        'description': 'Clicked Country Navigation',
    },
    'approval_role_nav': {
        'title': 'Menu - Approval Role',
        'description': 'Clicked Approval Role Navigation',
    },
    'bank_category_nav': {
        'title': 'Menu - Bank Category',
        'description': 'Clicked Bank Category Navigation',
    },
    'risk_nav': {
        'title': 'Menu - Risk',
        'description': 'Clicked Risk Navigation',
    },
    'social_nav': {
        'title': 'Menu - Social',
        'description': 'Clicked Social Navigation',
    },
    'contact_nav': {
        'title': 'Menu - Contact',
        'description': 'Clicked Contact Navigation',
    },
    'language_nav': {
        'title': 'Menu - Language',
        'description': 'Clicked Language Navigation',
    },
    'client_list': {
        'title': 'Client list',
        'description': 'Clicked Client list',
    },
    'client_list': {
        'title': 'Client list Page',
        'description': 'Clicked Client list Page',
    },
    'client_list': {
        'title': 'Client list Page',
        'description': 'Clicked Client list Page',
    },
    'client_list': {
        'title': 'Client list Page',
        'description': 'Clicked Client list Page',
    },

}
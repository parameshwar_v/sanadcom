/**
 * Created by RAKESH on 18-12-2015.
 */
angular.module('app')
    .controller('sectorDashboard', function ($scope) {

        $scope.country_product = {
            "type": "ComboChart",
            "options": {
                seriesType: 'bars',
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top', alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#ff0100'},
                    3: {color: '#01b0f1'},
                    4: {color: '#01b051'}
                }
            },
            "data": [
                ['Country', 'Equity', 'Lease', 'Loan ST', 'Loan MT', 'Loan LT'],
                ['Kenya USD/KES', 15000, 8000, 4000, 3000, 9000],
                ['Rawanda USD/RWF/KES', 0, 8000, 2000, 3000, 15000],
                ['Tanzania USD/TZS', 0, 8000, 2000, 0, 0],
                ['Uganda USD/UGX', 0, 0, 0, 0, 9000]
            ]
        };

        $scope.out_standing_portfolio = {
            "type": "BarChart",
            "options": {
                chartArea: {width: '70%'},
                isStacked: true,
                orientation: 'horizontal',

                hAxis: {
                    minValue: 0,
                },
                vAxis: {},
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top', alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#fe0000'},
                    3: {color: '#01b0f1'},
                    4: {color: '#00af50'}
                }
            },
            "data": [
                ['days', 'USD', 'KES', 'UGX', 'EUR', 'RWF'],
                ['Jan', 1000, 1000, 1000, 2000, 5000],
                ['Feb', 2000, 1000, 1000, 2000, 5000],
                ['Mar', 3000, 15000, 1000, 4000, 5000],
                ['Apr', 4000, 15000, 2000, 5000, 5000],
                ['May', 5000, 1000, 1000, 6000, 5000],
                ['Jun', 6000, 12000, 1000, 7000, 5000],
                ['Jul', 7000, 1000, 1000, 1500, 5000],
                ['Aug', 8000, 15000, 1000, 2000, 5000],
                ['Sep', 9000, 5000, 1000, 4000, 5000],
                ['Oct', 10000, 8000, 1000, 5000, 5000],
                ['Nov', 11000, 45200, 1000, 6000, 5000],
                ['Dec', 12000, 15000, 1000, 7000, 5000]
            ]
        };

        $scope.sector_limit = {
            "type": "PieChart",
            "options": {
                backgroundColor: 'transparent',
                legend: {position: "none"},
                tooltip: {trigger: 'none'},
                series: {
                    0: {color: '#f47677'},
                    1: {color: '#3ab54a'},
                    2: {color: '#2e3192'}
                }
            },
            "data": [
                ['Task', 'Hours per Day'],
                ['Work', 11],
                ['Eat', 2],
                ['Commute', 2]
            ]
        }
    })
    .controller('loanOfficerDashboard', function ($scope) {
        $scope.sectir_vise_loans = {
            "type": "PieChart",
            "options": {
                chartArea: {width: '90%'},
                legend: {position: 'bottom', alignment: 'start'},
                pieHole: 0.4,
                slices: {0: {color: '#85c976'}, 1: {color: '#0bc3df'}, 2: {color: '1e74eb'}, 3: {color: 'f1a80b'}}
            },
            "data": [
                ['Task', 'Hours per Day'],
                ['Agriculture and Fisheries', 62],
                ['Forestry & Agroforestry', 17],
                ['Mining and Quarrying', 8],
                ['Fisheries', 13]
            ]
        };
    })
    .controller('multipleSectorDashboard', function ($scope) {
        $scope.sector_limit = {
            "type": "PieChart",
            "options": {
                backgroundColor: 'transparent',
                legend: {position: "none"},
                tooltip: {trigger: 'none'},
                series: {
                    0: {color: '#f47677'},
                    1: {color: '#3ab54a'},
                    2: {color: '#2e3192'}
                }
            },
            "data": [
                ['Task', 'Hours per Day'],
                ['Work', 11],
                ['Eat', 2],
                ['Commute', 2]
            ]
        }
        $scope.out_standing_portfolio = {
            "type": "BarChart",
            "options": {
                chartArea: {width: '70%'},
                isStacked: true,
                orientation: 'horizontal',

                hAxis: {
                    minValue: 0,
                },
                vAxis: {},
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top', alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#fe0000'},
                    3: {color: '#01b0f1'},
                    4: {color: '#00af50'}
                }
            },
            "data": [
                ['days', 'USD', 'KES', 'UGX', 'EUR', 'RWF'],
                ['Jan', 1000, 1000, 1000, 2000, 5000],
                ['Feb', 2000, 1000, 1000, 2000, 5000],
                ['Mar', 3000, 15000, 1000, 4000, 5000],
                ['Apr', 4000, 15000, 2000, 5000, 5000],
                ['May', 5000, 1000, 1000, 6000, 5000],
                ['Jun', 6000, 12000, 1000, 7000, 5000],
                ['Jul', 7000, 1000, 1000, 1500, 5000],
                ['Aug', 8000, 15000, 1000, 2000, 5000],
                ['Sep', 9000, 5000, 1000, 4000, 5000],
                ['Oct', 10000, 8000, 1000, 5000, 5000],
                ['Nov', 11000, 45200, 1000, 6000, 5000],
                ['Dec', 12000, 15000, 1000, 7000, 5000]
            ]
        };

        $scope.exposure_product_types = {
            "type": "LineChart",
            "options": {
                chartArea: {width: '70%'},
                hAxis: {
                    logScale: true
                },
                vAxis: {
                    logScale: false
                },
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top', alignment: 'end'},
                series: {
                    0: {color: '#f3a036'},
                    1: {color: '#ee1d23'},
                    2: {color: '#b5c421'},
                    3: {color: '#9f397b'},
                    4: {color: '#36a8a0'}

                },
                lineWidth: 4
            },
            "data": [
                ['days', 'Normal', 'Watch', 'Substandard', 'Doubtful', 'Less'],
                ['Jan', 10000, 8000, 5000, 2000, 5000],
                ['Feb', 2000, 30000, 20000, 2000, 5000],
                ['Mar', 3000, 15000, 20000, 4000, 5000],
                ['Apr', 4000, 15000, 2000, 5000, 5000],
                ['May', 5000, 1000, 1000, 6000, 5000],
                ['Jun', 6000, 12000, 20000, 7000, 5000],
                ['Jul', 7000, 1000, 20000, 1500, 5000],
                ['Aug', 8000, 15000, 20000, 2000, 5000],
                ['Sep', 9000, 5000, 20000, 4000, 5000],
                ['Oct', 10000, 8000, 20000, 5000, 5000],
                ['Nov', 11000, 45200, 20000, 6000, 5000],
                ['Dec', 12000, 15000, 20000, 7000, 5000]
            ]
        };
    })

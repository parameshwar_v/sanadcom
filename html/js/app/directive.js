angular.module('app')
    .directive("multiSelect",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery.each(jQuery("select[multiple='multiple']"),function(i,o){
                    jQuery(o).multipleSelect({
                        position: jQuery(o).attr('position'),
                        placeholder: jQuery(o).attr('text'),
                        single: jQuery(o).attr('single')==='true'?true:false,
                        filter: jQuery(o).attr('filter')==='true'?true:false,
                        selectAll: jQuery(o).attr('selectAll')==='true'?true:false
                    });
                });
            }
        }
    }])
    .directive("datePicker",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('#appointment').datetimepicker({
                    format: 'DD/MM/YYYY',
                    //pickTime: false,
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
            }
        }
    }])
    .directive("donutGraph",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('#loansPerformance').highcharts({
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 0
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: "Total 5",
                        verticalAlign: 'middle',
                        y: 2,
                        x: -110
                    },
                    subtitle: {
                        text: null
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 110,
                            depth: 0
                        },
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            formatter: function(){
                                return this.percentage.toFixed(2)+'%';
                            }
                        }
                    },
                    legend: {
                        enabled: true,
                        layout: 'vertical',
                        align: 'right',
                        width: 200,
                        verticalAlign: 'middle',
                        useHTML: true,
                        labelFormatter: function(){
                            return '<div style="line-height:15px;margin-bottom:5px;"><b>'+this.y+'</b>&nbsp;'+this.name+'</div>';
                        }
                    },
                    series: [{
                        name: 'Delivered amount',
                        data: [
                            ['Loans Payment',3],
                            ['New Loans',1],
                            ['Pending Loans',1],
                            ['Cancelled Loans',1]
                        ],
                        showInLegend: true,
                        dataLabels: {
                            enabled: false
                        }
                    }]
                });
            }
        }
    }])
    .directive("tabContent",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                element.find(".sub-list li:first-child").trigger('click');
                $('div.handsontable').handsontable('render');
                jQuery(".tabs li").on('click',function(e){
                    e.preventDefault();
                    if(!jQuery(this).hasClass('show')){
                        e.stopImmediatePropagation();
                    }
                    else{
                        /*jQuery(".tabs li").removeClass("current-tab");
                         jQuery(this).addClass("current-tab");*/
                        jQuery(".tab-data, .sub-list").hide();
                        var x=jQuery(this).find("a").attr("data-tabs"),
                            y=jQuery(this).find(".sub-list"); //Find the rel attribute value to identify the active tab + content
                        //$('data-tabs').animate({scrollTop:$(x).position().top}, 'slow');
                        jQuery(x).fadeIn();
                        jQuery(y).show();
                        jQuery(y).find('li:first-child').trigger('click');
                        return false;
                    }
                });
            }
        }
    }])
    .directive("tabNext",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('.next-btn').on('click',function(e){
                    e.preventDefault();
                    //toastr();
                    jQuery(".status-list li.status-active").next().addClass("status-active")
                    jQuery('.current-tab').next().addClass('show');
                    jQuery(".tabs .show.current-tab").next().trigger("click");
                    jQuery(".tabs .show.current-tab").prev().addClass("active-tab");
                });
            }
        }
    }])
    .directive("starRating",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                //Star Rating
                jQuery.each(jQuery('.rate-star > span'),function(ind,elem){
                    jQuery(elem).on('click',function(){
                        jQuery(this).closest('.rate-star').find('span > i.fa').css('color','#ccc');
                        jQuery(this).find('i.fa').css('color','#FBCD45');
                        jQuery.each(jQuery(this).prevAll('span'),function(i,o){
                            jQuery(o).find('i.fa').css('color','#FBCD45');
                        });
                    });
                });
                jQuery.each(jQuery('.rate-star > span'),function(i,o){
                    if(jQuery(o).hasClass('active')){
                        jQuery(o).find('i.fa').css('color','#FBCD45');
                        jQuery(o).prevAll('span').find('i.fa').css('color','#FBCD45');
                    }
                });
            }
        }
    }])
    .directive("selectedAlpha",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('.alpha-list > ul > li:eq(0)').addClass('selected-alpha');
                jQuery('.alpha-list > ul > li').on('click',function(){
                    jQuery('.alpha-list > ul').find('.selected-alpha').removeClass('selected-alpha');
                    jQuery(this).addClass('selected-alpha');
                });
            }
        }
    }])
    .directive("selectedResult",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('.selected-result-list li:eq(0)').addClass('selected-result');
                jQuery('.selected-result-list li').on('click',function(){
                    jQuery('.selected-result-list li').removeClass('selected-result');
                    jQuery(this).addClass('selected-result');
                });
            }
        }
    }])
    .directive("toggleCheckbox",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery.each(jQuery('body').find('.toggle'),function(i,o){
                    jQuery(o).toggles({on: true});
                });
            }
        }
    }])
    .directive("searchBox",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                $('.search-box span').on('click',function(){
                    $('.search-box').toggleClass('expand');
                    $(this).closest('.search-box').find('input').focus();
                })
            }
        }
    }])
    .directive("tabData",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                $.each($('body').find('.custom-tabs'),function(i,o){
                    $(o).find('li').removeClass('active');
                    $(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
                    $(o).next('[data-area="tab"]').find('[data-content="tab"]:eq(0)').fadeIn('fast');
                    $(o).find('a[data-tag="tab"]:eq(0)').parent('li').addClass('active');
                    $(o).find('a[data-tag="tab"]').on('click',function(){
                        $(o).find('li').removeClass('active');
                        $(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
                        $(this).parent('li').addClass('active');
                        $(o).next('[data-area="tab"]').find('#'+$(this).attr('data-select')).fadeIn('fast');
                    });
                });
            }
        }
    }])
    .directive('activeLink',['$location',function(location){
        return {
            restrict: 'A',
            link: function(scope,element,attrs,controller){
                var clazz=attrs.activeLink;
                var path='';
                if(attrs.ngHref)
                    path=attrs.ngHref;
                if(attrs.href)
                    path=attrs.href;

                if(path!=undefined){
                    path=decodeURIComponent(path.substring(1));
                }else{
                    path='#';
                }//hack because path does not return including hashbang
                scope.location=location;
                scope.$watch('location.path()',function(newPath){
                    var url=attrs.aliasUrl;
                    if(path===newPath){
                        element.parent().addClass(clazz);
                    }else{
                        element.parent().removeClass(clazz);
                    }
                });
            }
        };
    }])
    .directive('httpResponse',['$http','httpLoader',function($http,httpLoader){
        return {
            restrict: 'A',
            link: function(scope,elm,attrs){
                scope.isLoading=function(){
                    //console.log('httpLoader.getPendingReqs',httpLoader.getPendingReqs());
                    return httpLoader.getPendingReqs()>0;
                };
                scope.$watch(scope.isLoading,function(v){
                    if(v){
                        $(elm).fadeIn();
                    }else{
                        $(elm).fadeOut();
                    }
                });
            }
        };
    }])
    //Sub Menu
    .directive("openSubmenu",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                element.addClass('active in');
                jQuery('ul#menu > li').on('click',function(){
                    jQuery('ul#menu > li').removeClass('active');
                    jQuery(this).addClass('active');
                    jQuery(this).addClass('in');
                });
            }
        }
    }])
    .directive("removeSubmenu",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('ul#menu').on('mouseleave',function(){
                    //jQuery('ul#menu li').removeClass('active');
                });
            }
        }
    }])
    .directive("listActive",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                jQuery('ul.groups-list li').on('click',function(){
                    jQuery(this).closest('ul.groups-list').find('li').removeClass('list-active');
                    jQuery(this).addClass('list-active');
                });
            }
        }
    }])
    .directive('dropDown',function($compile){
        return {
            restrict: 'E',
            scope: {
                user: '=user',
                branchs: '=branchs'
            },
            controller: function($scope){
                $scope.branchsList=$scope.branchs;
                $scope.brachType={};
                $scope.children={};
                $scope.addChild=function(child){
                    var index=$scope.user.children.length;
                    $scope.children.push({
                        "parent": $scope.user,
                        "children": [],
                        "index": index
                    });
                }
                $scope.remove=function(){
                    if($scope.user.parent){
                        var parent=$scope.user.parent;
                        var index=parent.children.indexOf($scope.user);
                        parent.children.splice(index,1);
                    }
                }
            },
            templateUrl: 'partials/admin/dropdown.html',
            link: function($scope,$element,$attrs){
            },
            compile: function(tElement,tAttr){
                var contents=tElement.contents().remove();
                var compiledContents;
                return function(scope,iElement,iAttr){
                    if(!compiledContents){
                        compiledContents=$compile(contents);
                    }
                    compiledContents(scope,function(clone,scope){
                        iElement.append(clone);
                    });
                };
            }
        };
    })
    .directive("bulidSelect",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                element.on('change',function(){
                    element.closest('.level').find('.ok-btn').attr('data-level',$(element).find(':selected').text());
                });
            }
        }
    }])
    .directive("buildLevel",[function($compile){
        return {
            //restrict: "A",
            template: '<span>TEST'// + attr + ''
            +'<div class="others-menu-wrap small-menu right-aligned" data-actions="menu">'
            +'<a href="javascript:;" class="others-menu mt5" onclick="showActionsMenu(this)">'
            +'<img src="images/others.png" alt="others-image" class="h13" />'
            +'</a>'
            +'<div class="others-menu-list">'
            +'<div class="clearfix ptrl5">'
            +'<h2 class="pull-left f13 gray mt5">'
            +'<a class="others-menu" href="javascript:;"  onclick="showActionsMenu(this)">'
            +'<img alt="others-image" src="images/others.png" class="h13">'
            +'</a>'
            +'Actions</h2>'
            +'</div>'
            +'<ul>'
            +'<li><a onclick="editLevel(this);">Edit</a></li>'
            +'<li><a onclick="deleteLevel(this);">Delete</a></li>'
            +'</ul>'
            +'</div>'
            +'</div>'
            +'</span>'
            +'<div class="level">'
            +'<select build-select>'
            +'<option ng-repeat="branch in branchList">{{branch.branch_type_name}}</option>'
            +'</select>'
            +'<a class="ok-btn" build-level><i class="fa fa-check"></i></a>'
            +'</div>',
            link: function(scope,element,attr){
                scope.active=false;
                element.on('click',function(){
                    scope.$apply(function(){
                        scope.active= !scope.active;
                    });
                });
            }
        }
    }])
    .directive("accordianContent",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                $(".accordion_head").click(function(){
                    if($('.accordion_body').is(':visible')){
                        $(this).parent('div').find($(".accordion_body")).slideUp(300);
                    }
                    if($(this).next(".accordion_body").is(':visible')){
                        $(this).next(".accordion_body").slideUp(300);
                    }else{
                        $(this).next(".accordion_body").slideDown(300);
                    }
                });
            }
        }
    }])
    .directive("accordianData",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                $(".accordion_head").click(function(){
                    $(".accordion_head").removeClass("acc-expand");
                    $(this).addClass("acc-expand");
                    if($('.accordion_body').is(':visible')){
                        $(this).parent('div').find($(".accordion_body")).slideUp(300);
                    }
                    if($(this).next(".accordion_body").is(':visible')){
                        $(this).next(".accordion_body").slideUp(300);
                        $(".accordion_head").removeClass("acc-expand");
                    }else{
                        $(this).next(".accordion_body").slideDown(300);
                    }
                });
            }
        }
    }])
    .directive('pdfDownload',function(){
        return {
            restrict: 'E',
            templateUrl: 'http://desk8-tss/q-lana/rest/Classes/company_branches.xls',
            scope: true,
            link: function(scope,element,attr){
                var anchor=element.children()[0];
                // When the download starts, disable the link
                scope.$on('download-start',function(){
                    $(anchor).attr('disabled','disabled');
                });
                // When the download finishes, attach the data to the link. Enable the link and change its appearance.
                scope.$on('downloaded',function(event,data){
                    $(anchor).attr({
                            href: 'data:application/pdf;base64,'+data,
                            download: attr.filename
                        })
                        .removeAttr('disabled')
                        .text('Save')
                        .removeClass('btn-primary')
                        .addClass('btn-success');
                    // Also overwrite the download pdf function to do nothing.
                    scope.downloadPdf=function(){
                    };
                });
            },
            controller: ['$scope','$attrs','$http',function($scope,$attrs,$http){
                $scope.downloadPdf=function(){
                    $scope.$emit('download-start');
                    $http.get($attrs.url).then(function(response){
                        $scope.$emit('downloaded',response.data);
                    });
                };
            }]
        }
    })
    .directive("dropdownMenu",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
            }
        }
    }])
    .directive('resizeWindow',function($window){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    $('.col-fixed-width').height($(window).height()-195);
                    $('.col-fixed-width ul.liPadLeft0').height($(window).height()-235);
                }
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    })
    .directive('colsHorizontal',['$window',function($window){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    $('.horizontal-div-for-scroll').width($(window).width()-80);
                    $('.custom-width').width($(window).width()-100);
                    $('.custom-width-new').width($(window).width()-$('.left-menu').width()-30);
                    $('.h-width').width($(window).width()-230);
                    $('.table-fullwidth-scroll').width($(window).width()-212);
                    $('.horizontal-div-for-scroll,.table-fullwidth-scroll').mCustomScrollbar({
                        theme: "minimal-dark",
                        horizontalScroll: true,
                        scrollButtons: {enable: true},
                        updateOnContentResize: true,
                        axis: "x",
                        scrollbarPosition: "outside",
                        autoHideScrollbar: true,
                        autoExpandScrollbar: true,
                        advanced: {
                            autoExpandHorizontalScroll: true
                        }
                    });
                }
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })

                scope.$watch('vm.child.width()',function(){

                });
            }
        }
    }])
    .directive('minScroll',['$document','$window','$timeout','$interval',function($document,$window,$timeout,$interval){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    $timeout(function(){
                        var top_pos=elem.offset().top;
                        var scroll_height=$window.innerHeight-top_pos;
                        elem.css('height',scroll_height);
                        $('.scroll-div-wrap').mCustomScrollbar({
                            theme: "minimal-dark",
                            scrollButtons: {enable: true},
                            updateOnContentResize: true,
                            axis: "y",
                            scrollbarPosition: "outside",
                            autoHideScrollbar: false,
                            autoExpandScrollbar: false
                        });
                    },500)
                }
                $interval(function(){
                    scope.onResize();
                },100);
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    }])
    //parent-scroll
    .directive('parentScroll',['$document','$window','$timeout','$interval','$rootScope',function($document,$window,$timeout,$interval,$rootScope){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    $timeout(function(){

                        var top_pos=elem.offset().top;
                        var scroll_height=elem.closest('.parent').height()-200;
                        elem.css('min-height',$window.innerHeight-top_pos);
                        elem.css('height',$window.innerHeight-top_pos);
                        // console.log('scroll_height',$window.innerHeight - top_pos);
                        elem.mCustomScrollbar({
                            theme: "minimal-dark",
                            scrollButtons: {enable: true},
                            updateOnContentResize: true,
                            axis: "y",
                            scrollbarPosition: "outside",
                            autoHideScrollbar: false,
                            autoExpandScrollbar: false,
                            advanced: {
                                autoScrollOnFocus: true
                            }
                        });
                    },500)
                }
                scope.onResize();

                angular.element($window).bind('resize',function(){
                    scope.onResize();
                    $rootScope.$apply();
                })
            }
        }
    }])
    .directive('winHeight',['$document','$window','$timeout',function($document,$window,$timeout){
        return {
            link: function(scope,elem,attrs){

                var w=angular.element($window);
                scope.getWindowDimensions=function(){
                    return {'h': w.height(),'w': w.width(),'offset': elem.offset().top};
                };
                scope.$watch(scope.getWindowDimensions,function(newValue,oldValue){
                    scope.windowHeight=newValue.h;
                    scope.windowWidth=newValue.w;
                    if(attrs.whOffset){
                        var top_pos=attrs.whOffset;
                    }else{
                        var top_pos=elem.offset().top;
                    }

                    var scroll_height=scope.windowHeight-top_pos;
                    elem.css('height',scroll_height);

                },true);

                w.bind('resize',function(){
                    scope.$apply();
                });
            }
        }
    }])
    /*
     .directive('winHeight', ['$document', '$window', '$timeout', function ($document, $window, $timeout) {
     return {
     link: function (scope, elem, attrs) {
     scope.onResize = function () {
     //$timeout(function () {
     var top_pos = 0;
     top_pos = elem.offset().top;
     var scroll_height = $window.innerHeight - top_pos;
     console.log('innerHeight',$window.innerHeight);
     console.log('top_pos',top_pos);
     elem.css('height', scroll_height);
     // });
     }
     scope.onResize();
     angular.element($window).bind('resize', function () {
     scope.onResize();
     })
     }
     }
     }])
     */
    .directive('mcScroll',['$document','$window','$timeout',function($document,$window,$timeout){
        return {
            link: function(scope,elem,attrs){
                elem.mCustomScrollbar({
                    theme: "minimal-dark",
                    scrollButtons: {enable: true},
                    updateOnContentResize: true,
                    axis: "y",
                    scrollbarPosition: "outside",
                    autoHideScrollbar: false,
                    autoExpandScrollbar: false
                });
            }
        }
    }])
    .directive('docHeight',['$document','$window','$timeout','$interval',function($document,$window,$timeout,$interval){
        return {
            link: function(scope,elem){
                scope.onResize=function(){
                    var top_pos=elem.offset().top;
                    var scroll_height=$document.height()-top_pos;
                    elem.css('height',scroll_height);
                    elem.mCustomScrollbar({
                        theme: "minimal-dark",
                        scrollButtons: {enable: true},
                        updateOnContentResize: true,
                        axis: "y",
                        scrollbarPosition: "outside",
                        autoHideScrollbar: false,
                        autoExpandScrollbar: false
                    });
                }
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                });
            }
        }
    }])
    .directive('tableHorizontal',['$window','$interval',function($window,$interval){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    $(".table-scroll").css('max-width',$(window).width()-280);
                    $(".table-scroll-new").css('max-width',$(window).width()-180);
                    $(".table-scroll-div").css('max-width',$(window).width()-400);
                    $(".c-hscroll").css('max-width',$('.page-left-wrap').width()-$('.t-str-view .table-fixed td').width()-30);
                    $('.table-scroll,.table-scroll-new,.table-scroll-div,.c-hscroll').mCustomScrollbar({
                        theme: "minimal-dark",
                        horizontalScroll: true,
                        scrollButtons: {enable: true},
                        updateOnContentResize: true,
                        axis: "x",
                        scrollbarPosition: "outside",
                        autoHideScrollbar: true,
                        autoExpandScrollbar: true,
                        advanced: {
                            autoExpandHorizontalScroll: true
                        }
                    });
                }
                $interval(function(){
                    scope.onResize();
                },500);
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    }])
    .directive('onlyDigits',function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope,element,attr,ctrl){
                function inputValue(val){
                    if(val||val!=' '){
                        var digits=val.replace(/[^0-9]/g,'');
                        if(digits!==val){
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseInt(digits,10);
                    }
                    return undefined;
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive('onlyDigitsWithLength',function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope,element,attr,ctrl){
                var maxlength = Number(attr.onlyDigitsWithLength);
                function inputValue(val){
                    if(val||val!=' '){
                        var digits=val.replace(/[^0-9]/g,'');
                        if(digits!==val){
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        } else if (val.length > maxlength) {
                            var transformedInput = val.substring(0, maxlength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                            return transformedInput;
                        }
                        return parseInt(digits,10);
                    }
                    return undefined;
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive('resultHeight',function($window){
        return {
            link: function(scope,elem,attrs){
                scope.onResize=function(){
                    elem.height($window.innerHeight-150);
                    elem.mCustomScrollbar({
                        autoHideScrollbar: true
                    });
                }
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    })
    .directive('addfieldsRow',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.on('click',function(){
                    elem.closest('tbody').find('.added-row').slideDown();
                });
            }
        }
    })
    .directive('addRow',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                var _rowAdd='';
                elem.on('click',function(){
                    var _type=elem.closest('tr').find('input[name="type"]').val(),
                        _amount=elem.closest('tr').find('input[name="amount"]').val();
                    var _rowAdd='<tr>'
                        +'<td name="type">'+_type+'</td>'
                        +'<td name="amount">'+_amount+'</td>'
                        +'<td>'
                        +'<a class="custom-color-btn" edit-row><i class="fa fa-pencil"></i></a>'
                        +'</td></tr>'
                    elem.closest('tbody').append(_rowAdd);
                    elem.closest('tr').find('input').val('');
                    elem.closest('tbody').find('.added-row').hide();
                });
            }
        }
    })
    .directive('cancelRow',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.on('click',function(){
                    elem.closest('tr').find('input').val('');
                    elem.closest('tbody').find('.added-row').slideUp();
                });
            }
        }
    })
    .directive("accordianTabbed",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                element.click(function(){
                    $('.tab-data').find('div.acc_wrap').removeClass('open');
                    $('.tab-data').find('div.acc_wrap').find('.accordion_body').slideUp();
                    $(this).closest('div.acc_wrap').find('.accordion_body').slideDown();
                })
            }
        }
    }])
    .directive('triggerAccord',function($window){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.closest('ul').find('li:first-child').addClass('active');
                elem.on('click',function($event){
                    $event.stopPropagation();
                    elem.closest('ul').find('li').removeClass('active');
                    elem.addClass('active');
                    var _acc=elem.attr('data-link');
                    $('.content-container').find('.accordion_head[data-head="'+_acc+'"]').trigger('click');
                });
            }
        }
    })
    .directive('rightModal',function($window){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.click(function(){
                    var _modal=attrs.modal,
                        _rightModalOverlay='<div id="right-overlay"></div>';
                    $(window).find('#right-overlay').remove();
                    $('[data-modal="'+_modal+'"]').addClass('shown');
                    $('body').addClass('overflow-hidden');
                    $('[data-modal="'+_modal+'"] .right-modal-body').height($(window).height()-41)
                    $(_rightModalOverlay).insertAfter($('[data-modal="'+_modal+'"]'));
                });
            }
        }
    })
    .directive('closeRight',function($window){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.click(function(){
                    elem.closest('.right-modal').removeClass('shown');
                    $('#right-overlay').remove();
                    $('body').removeClass('overflow-hidden');
                });
            }
        }
    })

    .directive("fixedTop",function($window){
        return function(scope,element,attrs){
            angular.element($window).bind("scroll",function(){
                if(this.pageYOffset>=70){
                    element.addClass(attrs.fixedTop);
                    scope.boolChangeClass=true;
                }else{
                    element.removeClass(attrs.fixedTop);
                    scope.boolChangeClass=false;
                }
                scope.$apply();
            });
        };
    })
    .directive("firstAlpha",function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                attrs.$observe('firstAlpha',function(val){
                    elem.text($.trim(attrs.firstAlpha.charAt(0).toUpperCase()));
                    var colorArray=['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                        '#989898','#75579C','#A02783','#3a27ab','#856B07',
                        '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                        '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c',
                        '#136287','#305203','#961900','#efb400','#acad00','#22baf1'];
                    if(attrs.backroundColor==undefined){
                        elem.css({
                            backgroundColor: colorArray[Math.floor((Math.random()*colorArray.length))]
                        });
                    }
                })
            }
        }
    })
    .directive("bkColor",function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                var str=attrs.bkColor;
                if(scope.setColor==undefined){
                    scope.setColor=[];
                }
                var randomColor=Math.floor(Math.random()*16777215).toString(16);
                var colors=['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                    '#989898','#75579C','#A02783','#3a27ab','#856B07',
                    '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                    '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c','#136287','#305203','#961900'];
                if(colors.length>scope.setColor.length){
                    var colorCode=colors[scope.setColor.length];
                }else{
                    var colorCode='#'+randomColor;
                }
                var a=_.findWhere(scope.setColor,{'id': str});
                if(a){
                    var _color=a.code;
                }else{
                    var _color=colorCode;
                    scope.setColor.push({'code': colorCode,'id': str});
                }
                elem.css({
                    backgroundColor: _color
                });
            }
        }
    })
    .directive("textColor",function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                var str=attrs.textColor;
                if(scope.setColor==undefined){
                    scope.setColor=[];
                }
                var randomColor=Math.floor(Math.random()*16777215).toString(16);
                var colors=['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                    '#989898','#75579C','#A02783','#3a27ab','#856B07',
                    '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                    '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c','#136287','#305203','#961900'];
                if(colors.length>scope.setColor.length){
                    var colorCode=colors[scope.setColor.length];
                }else{
                    var colorCode='#'+randomColor;
                }
                var a=_.findWhere(scope.setColor,{'id': str});
                if(a){
                    var _color=a.code;
                }else{
                    var _color=colorCode;
                    scope.setColor.push({'code': colorCode,'id': str});
                }
                elem.css({
                    color: _color
                });
            }
        }
    })
    .directive('uploadFile',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                switch(attrs.filetype){
                    case 'image/jpeg':
                        elem.find('i').addClass('jpg-icon');
                        break;
                    case 'image/jpg':
                        elem.find('i').addClass('jpg-icon');
                        break;
                    case 'image/png':
                        elem.find('i').addClass('png-icon');
                        break;
                    case 'application/pdf':
                        elem.find('i').addClass('pdf-icon');
                        break;
                    case 'application/octet-stream':
                        elem.find('i').addClass('zip-icon');
                        break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        elem.find('i').addClass('word-icon');
                        break;
                    default:
                        elem.find('i').addClass('default-file-icon');
                        break;
                }
            }
        }
    })
    .filter('getFileName',function(){
        return function(value,fileName){
            return fileName.replace(/\.[^/.]+$/,"");
        };
    })
    .filter('getFileName',function(){
        return function(value,fileName){
            return fileName.replace(/\.[^/.]+$/,"");
        };
    })
    .filter('capitalize',function(){
        return function(input){
            return (!!input)?input.charAt(0).toUpperCase()+input.substr(1).toLowerCase():'';
        }
    })
    .directive('touchPoints',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                if(attrs.isreminder==1){
                    //elem.parent().parent().addClass('light-orange-bg light-o-bg');
                }
                switch(attrs.tauchpoint){
                    case 'Mail':
                        elem.find('i').addClass('fa fa-envelope');
                        break;
                    case 'Call':
                        elem.find('i').addClass('fa fa-phone');
                        break;
                    case 'Chat':
                        elem.find('i').addClass('fa fa-comments');
                        break;
                    case 'Message':
                        elem.find('i').addClass('fa fa-mobile f18');
                        break;
                    case 'Note':
                        elem.find('i').addClass('fa fa-file-text-o');
                        break;
                }
            }
        }
    })
    .directive('knowledgeIcons',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                switch(attrs.image){
                    case 'document':
                        elem.html('<span class="icon-file"></span>');
                        break;
                    case 'blog':
                        elem.html('<span class="icon-clipped-file"></span>');
                        break;
                    case 'news':
                        elem.html('<span class="icon-workload"></span>');
                        break;
                    case 'video':
                        elem.html('<span class="icon-play"></span>');
                        break;
                    case 'Total':
                        elem.html('<span class=" icon-harddrive1"></span>');
                        break;
                }
            }
        }
    })
    .directive('qtypeIcons',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                switch(attrs.qtype){
                    case 'text':
                        elem.html('<i class="icon-input-text mt4 w25"></i>');
                        break;
                    case 'textarea':
                        elem.html('<i class="icon-input-text mt4 w25"></i>');
                        break;
                    case 'radio':
                        elem.html('<i class="icon-yes-no mt4 w25"></i>');
                        break;
                    case 'date':
                        elem.html('<i class="icon-calendar1 mt4 w25"></i>');
                        break;
                    case 'dropdown':
                        elem.html('<i class="icon-list mt4 w25"></i>');
                        break;
                    case 'file':
                        elem.html('<i class="icon-cloud-export mt4 w25"></i>');
                        break;
                    case 'checkbox':
                        elem.html('<i class="icon-right-circle mt4 w25"></i>');
                        break;
                }
            }
        }
    })
    .directive('taskStatusBg',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                switch(attrs.bgcolor){
                    case 'completed':
                        elem.addClass('btn-success');
                        break;
                    case 'pending':
                        elem.addClass('btn-warning');
                        break;
                    case 'open':
                        elem.addClass('btn-primary');
                        break;
                    case 'cancel':
                        elem.addClass('btn-danger');
                        break;
                    case 'cancelled':
                        elem.addClass('btn-danger');
                        break;
                }
            }
        }
    })
    .directive('labelStatus',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                attrs.$observe('labelStatus',function(val){
                    var str=attrs.labelStatus.toLowerCase();
                    switch(str){

                        case 'completed' :
                        case 'approved':
                        case 'accepted':
                        case 'submitted':
                        case 'available':
                        case 'supervision':
                            elem.addClass('label f13 bold label-success');
                            break;

                        case 'inactive':
                        case 'rejected':
                        case 'cancel':
                        case 'cancelled':
                            elem.addClass('label f13 bold label-danger');
                            break;
                        case 'pending' :
                        case 'in progress' :
                        case 'forwarded':
                        case 'register':
                        case 'postpone':
                            elem.addClass('label f13 bold label-warning');
                            break;
                        case 'pre-approved':
                        case 'pipeline' :
                        case 'open':
                        case 'new':
                        case 'its accepted':
                            elem.addClass('label f13 bold label-primary');
                            break;
                        case 'monitoring':
                        case 'need info':
                        case 'create':
                            elem.addClass('label f13 bold btn-dark');
                            break;
                        case 'disbursement' :
                        case 'in-user':
                        case 'schedule':
                            elem.addClass('label f13 bold btn-info');
                            break;

                    }
                });
            }
        }
    })
    .directive('workStatus',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                attrs.$observe('workStatus',function(val){
                    var str=attrs.workStatus.toLowerCase();
                    switch(str){
                        case 'completed' :
                            elem.addClass('fa fa-check');
                            break;
                        case 'approved':
                        case 'accepted':
                            elem.addClass('icon-right-circle');
                            break;
                        case 'rejected':
                            elem.addClass('fa fa-close');
                            break;
                        case 'in progress' :
                        case 'forwarded':
                            elem.addClass('icon-info');
                            break;

                    }
                });
            }
        }
    })
    .directive('approvals',function(){
        return {
            restrict: "A",
            scope: true,
            link: function(scope,elem,attrs){
                switch(attrs.approvaltype){
                    case 'meeting':
                        var id=attrs.approvalid;
                        elem.find('.icon-btn').addClass('btn-primary');
                        elem.find('.icon-btn i').addClass('fa fa-users');
                        elem.find('.btn-box').addClass('blue-status');
                        elem.find('.template').bind('click',function(){
                            scope.meetingModal(id)
                        });
                        break;
                    case 'task':
                        var id=attrs.approvalid;
                        elem.find('.template').bind('click',function(){
                            scope.addTasksModal(id)
                        });
                        elem.find('.icon-btn').addClass('btn-info');
                        elem.find('.icon-btn i').addClass('fa fa-tasks');
                        elem.find('.btn-box').addClass('light-blue-status');
                        break;
                    case 'approval':
                        elem.find('.icon-btn').addClass('btn-success');
                        elem.find('.icon-btn i').addClass('fa fa-check');
                        elem.find('.btn-box').addClass('green-status');
                        break;
                    case 'file':
                        elem.bind('click',function(){
                            scope.quickAttachmentModal()
                        });
                        elem.find('.icon-btn').addClass('lt-bg');
                        elem.find('.icon-btn i').addClass('fa fa-file');
                        elem.find('.btn-box').addClass('light-grey-status');
                        break;
                }
            }
        }
    })
    .directive('ngEnter',function(){
        return function(scope,element,attrs){
            element.bind("keydown",function(e){
                if(e.which===13){
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter,{'e': e});
                    });
                    e.preventDefault();
                }
            });
        };
    })

    // hierarchical show
    .directive('hierarchicalShow',[
        '$timeout',
        '$rootScope',
        function($timeout,$rootScope){
            return {
                restrict: 'A',
                scope: true,
                link: function(scope,elem,attrs){
                    var parent_el=$(elem),
                        baseDelay=60;
                    var add_animation=function(children,length){
                        children
                            .each(function(index){
                                $(this).css({
                                    '-webkit-animation-delay': (index*baseDelay)+"ms",
                                    '-moz-animation-delay': (index*baseDelay)+"ms",
                                    '-o-animation-delay': (index*baseDelay)+"ms",
                                    '-ms-animation-delay': (index*baseDelay)+"ms",
                                    'animation-delay': (index*baseDelay)+"ms"
                                })
                            })
                            .end()
                            .waypoint({
                                element: elem[0],
                                handler: function(){
                                    parent_el.addClass('hierarchical_show_inView');
                                    setTimeout(function(){
                                        parent_el
                                            .removeClass('hierarchical_show hierarchical_show_inView fast_animation')
                                            .children()
                                            .css({
                                                '-webkit-animation-delay': '',
                                                '-moz-animation-delay': '',
                                                '-o-animation-delay': '',
                                                '-ms-animation-delay': '',
                                                'animation-delay': ''
                                            });
                                    },(length*baseDelay)+1200);
                                    this.destroy();
                                },
                                context: window,
                                offset: '90%'
                            });
                    };
                    $rootScope.$watch('pageLoaded',function(){
                        if($rootScope.pageLoaded){
                            var children=parent_el.children(),
                                children_length=children.length;
                            $timeout(function(){
                                add_animation(children,children_length)
                            },560)
                        }
                    });
                }
            }
        }
    ])
    .directive('hierarchicalSlide',[
        '$timeout',
        '$rootScope',
        function($timeout,$rootScope){
            return {
                restrict: 'A',
                scope: true,
                link: function(scope,elem,attrs){
                    var $this=$(elem),
                        baseDelay=100;
                    var add_animation=function(children,context,childrenLength){
                        children.each(function(index){
                            $(this).css({
                                '-webkit-animation-delay': (index*baseDelay)+"ms",
                                'animation-delay': (index*baseDelay)+"ms"
                            })
                        });
                        $this.waypoint({
                            handler: function(){
                                $this.addClass('hierarchical_slide_inView');
                                $timeout(function(){
                                    $this.removeClass('hierarchical_slide hierarchical_slide_inView');
                                    children.css({
                                        '-webkit-animation-delay': '',
                                        'animation-delay': ''
                                    });
                                },(childrenLength*baseDelay)+1200);
                                this.destroy();
                            },
                            context: context[0],
                            offset: '90%'
                        });
                    };
                    $rootScope.$watch('pageLoaded',function(){
                        if($rootScope.pageLoaded){
                            var thisChildren=attrs['slideChildren']?$this.children(attrs['slideChildren']):$this.children(),
                                thisContext=attrs['slideContext']?$this.closest(attrs['slideContext']):'window',
                                thisChildrenLength=thisChildren.length;
                            if(thisChildrenLength>=0){
                                $timeout(function(){
                                    add_animation(thisChildren,thisContext,thisChildrenLength)
                                },560)
                            }
                        }
                    });
                }
            }
        }
    ])
    .directive("childCount",function(){
        return {
            restrict: "EA",
            template: '{{count}}',
            link: function(scope,elem,attrs){
                var tree=scope.data;
                scope.count=0;
                treeStructure(tree);
                function treeStructure(tree){
                    angular.forEach(tree.nodes,function(value,key){
                        scope.count++;
                        treeStructure(value);
                    });
                }
            }
        }
    })
    .directive("basicAccordian",[function(){
        return {
            restrict: "A",
            link: function(scope,element,attr){
                element.click(function(){
                    element.parent('.acc-head').toggleClass('acc-open');
                    element.parent().parent().toggleClass('racc-open');
                    element.parents('.acc-head').toggleClass('acc-open');
                    element.parentsUntil('.accordian-wrapper').next('.acc-body').slideToggle();
                    element.parent().find('.add-but').slideToggle();
                })
            }
        }
    }])

    // coffeescript's for in loop
    .directive('fieldDirective',function($http,$compile){
        var __indexOf=[].indexOf||function(item){
                for(var i=0,l=this.length; i<l; i++){
                    if(iinthis&&this[i]===item) return i;
                }
                return -1;
            };
        var getTemplateUrl=function(field){
            var type=field.field_type;
            var templateUrl='./partials/crm/project/field/';
            var supported_fields=[
                'text',
                'email',
                'textarea',
                'checkbox',
                'date',
                'dropdown',
                'hidden',
                'password',
                'radio'
            ]
            if(__indexOf.call(supported_fields,type)>=0){
                return templateUrl+=type+'.html';
            }
        }
        var linker=function(scope,element){
            // GET template content from path
            var templateUrl=getTemplateUrl(scope.field);
            $http.get(templateUrl).success(function(data){
                element.html(data);
                $compile(element.contents())(scope);
            });
        }
        return {
            template: '<div ng-bind="field"></div>',
            restrict: 'E',
            scope: {
                field: '='
            },
            link: linker
        };
    })
    .directive("dynamicName",function($compile){
        return {
            restrict: "A",
            terminal: true,
            priority: 1000,
            link: function(scope,element,attrs){
                element.attr('name',scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    })
    .directive('cdTrueValue',[function(){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope,element,attrs,ngModel){
                ngModel.$parsers.push(function(v){
                    return v?scope.$eval(attrs.cdTrueValue):scope.$eval(attrs.cdFalseValue);
                });
            }
        };
    }])
    .filter('decode',function(){
        return function(text){
            if(text!=undefined){
                return window.atob(text);
            }
        }
    })
    .filter('encode',function(){
        return function(text){
            return window.btoa(text);
        }
    })
    .filter('split',function(){
        return function(input,splitChar,splitIndex){
            // do some bounds checking here to ensure it has that index
            if(input!=undefined){
                return input.split(splitChar)[splitIndex];
            }
        }
    })
    .filter('underscoreless',function(uppercaseFilter){
        return function(input){
            return uppercaseFilter(input.replace(/_/g,' '));
        };
    })
    .filter('underscorelesslowercase',function(uppercaseFilter){
        return function(input){
            return input.replace(/_/g,' ');
        };
    })
    .filter('underscoreadd',function(lowercaseFilter){
        return function(input){
            return lowercaseFilter(input.replace(/ /g,'_'));
        };
    })
    .filter('capitalize',function(){
        return function(input){
            return (!!input)?input.charAt(0).toUpperCase()+input.substr(1).toLowerCase():'';
        }
    })
    .filter('showmodule',function(){
        return function(input){
            input=''+input;
            var temp=input.split('#')[1];
            temp=temp.split('/');
            if(temp.length>2){
                return temp[1]+'/'+temp[2];
            }else if(temp.length<=2){
                return temp[1];
            }else{
                return input;
            }
        }
    })
    .filter('checkEmpty',function(){
        return function(input,string){
            if(input==''||input==null){
                if(string)
                    return string;
                return '-----';
            }else{
                return input;
            }
        };
    })
    .directive('convertJson',function($filter){
        return {
            restrict: "E",
            link: function(scope,elem,attrs){
                var json = $filter('json');
                var string=attrs.objectData;
                if(string){
                    try {
                        string = JSON.parse(string);
                        elem.html('<pre>'+json(string)+'</pre>');
                    } catch (e) {
                        elem.html(string);
                    }
                }else
                    elem.html('-----');
            }
        }
    })
    .directive('datetimepickerNeutralTimezone',function(){
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function(scope,element,attrs,ctrl){
                ctrl.$formatters.push(function(value){
                    var date=new Date(Date.parse(value));
                    date=new Date(date.getTime()+(60000*date.getTimezoneOffset()));
                    return date;
                });
                ctrl.$parsers.push(function(value){
                    var date=new Date(value.getTime()-(60000*value.getTimezoneOffset()));
                    return date;
                });
            }
        };
    })
    .directive('addIframe',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                elem.html('<iframe width="100%" height="415" src="'+attrs.iframesrc+'" frameborder="0" allowfullscreen></iframe>');
            }
        }
    })
    .directive('newtouchPoints',function(){
        return {
            restrict: "A",
            link: function(scope,elem,attrs){
                switch(attrs.tauchpoint){
                    case 'Mail':
                        elem.parent('.icons-div').addClass('sky-blue-bg');
                        break;
                    case 'Call':
                        elem.parent('.icons-div').addClass('btn-primary');
                        break;
                    case 'Chat':
                        elem.parent('.icons-div').addClass('btn-success');
                        break;
                    case 'Message':
                        elem.parent('.icons-div').addClass('btn-orange');
                        break;
                    case 'Note':
                        elem.parent('.icons-div').addClass('btn-dark');
                        break;
                }
            }
        }
    })
    .directive('appFilereader',function($q){
        /*
         made by elmerbulthuis@gmail.com WTFPL licensed
         */
        var slice=Array.prototype.slice;
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope,element,attrs,ngModel){
                if(!ngModel) return;
                ngModel.$render=function(){
                }
                element.bind('change',function(e){
                    var element=e.target;
                    if(!element.value) return;
                    element.disabled=true;
                    $q.all(slice.call(element.files,0).map(readFile))
                        .then(function(values){
                            if(element.multiple) ngModel.$setViewValue(values);
                            else ngModel.$setViewValue(values.length?values[0]:null);
                            element.value=null;
                            element.disabled=false;
                        });
                    function readFile(file){
                        var deferred=$q.defer();
                        var reader=new FileReader()
                        reader.onload=function(e){
                            deferred.resolve(e.target.result);
                        }
                        reader.onerror=function(e){
                            deferred.reject(e);
                        }
                        reader.readAsDataURL(file);
                        return deferred.promise;
                    }
                }); //change
            } //link
        }; //return
    }) //appFilereader
    .directive('ngConfirmClick',[
        function(){
            return {
                link: function(scope,element,attr){
                    var msg=attr.ngConfirmClick||"Are you sure?";
                    var clickAction=attr.confirmedClick;
                    element.bind('click',function(event){
                        if(window.confirm(msg)){
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        }])
    .directive('knowledgeManagementGraph',function(){
        return {
            restrict: "A",
            scope: {graph: '='},
            link: function(scope,elem,attrs){
                scope.$watch('graph',function(){
                    if(scope.graph==undefined){
                        return false;
                    }
                    jQuery('#knowledgeManagementGraph').highcharts({
                        "chart": {
                            "type": "column",
                            "style": {
                                "fontFamily": "Arial"
                            },
                            "height": 250
                        },
                        "yAxis": {
                            "labels": {
                                "format": "{value:,.of}"
                            },
                            "title": {
                                "text": null
                            }
                        },
                        "exporting": {
                            "enabled": false
                        },
                        "colors": [
                            "#39B468",
                            "#bf0000"
                        ],
                        "tooltip": {
                            "shared": true
                        },
                        "title": {
                            "text": " ",
                            "align": "left"
                        },
                        "xAxis": {
                            "type": "datetime"
                        },
                        "series": [
                            {
                                "index": 0,
                                "name": "Doc.'s Viewed",
                                "data": scope.graph.views
                            },
                            {
                                "index": 1,
                                "name": "Doc.'s Uploaded",
                                "data": scope.graph.uploads
                            }
                        ]
                    });
                })
            }
        }
    })
    //Loand Stages
    .directive('loanStagesGraph',function(){
        return {
            restrict: "EA",
            link: function(scope,elem,attrs){
                elem.highcharts({
                    chart: {
                        type: 'funnel'
                    },
                    title: {
                        text: false,
                        x: -50
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b> ({point.y:,.0f})',
                                color: (Highcharts.theme&&Highcharts.theme.contrastTextColor)||'black',
                                softConnector: true
                            },
                            neckWidth: '15%',
                            neckHeight: '35%',
                            showInLegend: true

                            //-- Other available options
                            // height: pixels or percent
                            // width: pixels or percent
                        }
                    },
                    legend: {
                        enabled: true
                    },
                    series: [{
                        name: 'Unique users',
                        colorByPoint: true,
                        data: [{
                            name: 'Website visits',
                            color: 'rgba(255,99,86,1)',
                            y: 13654
                        },{
                            name: 'Downloads',
                            color: 'rgba(255,167,75,1)',
                            y: 8403
                        },{
                            name: 'Requested price list',
                            color: 'rgba(255,87,120,1)',
                            y: 1938
                        },{
                            name: 'Approved',
                            color: 'rgba(132,199,119,1)',
                            y: 877
                        }]
                    }]

                });

            }
        }
    })
    //calendar areachart Stages
    .directive('calendarAreaGraph',function(){
        return {
            restrict: "EA",
            link: function(scope,elem,attrs){
                elem.highcharts(JSON.parse(attrs.calendarAreaGraph));
            }
        }
    })
    //calendar areachart Stages
    .directive('countryVsLoanStatus',function(){
        return {
            restrict: "EA",
            link: function(scope,elem,attrs){
                var colors=Highcharts.getOptions().colors,
                    categories=['Kenya','Rwanda','Tanzania','Uganda'],
                    data=[{
                        y: 25,
                        color: '#ed008c',
                        drilldown: {
                            name: 'Kenya',
                            categories: ['Normal','Watch','Substandard','Loss','Doubtful'],
                            data: [5,5,5,5,5],
                            color: colors[0]
                        }
                    },{
                        y: 25,
                        color: '#f37e3b',
                        drilldown: {
                            name: 'Rwanda',
                            categories: ['Normal','Substandard','Loss'],
                            data: [10,10,5],
                            color: colors[1]
                        }
                    },{
                        y: 25,
                        color: '#008a3f',
                        drilldown: {
                            name: 'Tanzania',
                            categories: ['Normal','Watch'],
                            data: [12.5,12.5],
                            color: colors[1]
                        }
                    },{
                        y: 25,
                        color: '#01b0f1',
                        drilldown: {
                            name: 'Uganda',
                            categories: ['Normal'],
                            data: [25],
                            color: colors[1]
                        }
                    }],
                    browserData=[],
                    versionsData=[],
                    i,
                    j,
                    dataLen=data.length,
                    drillDataLen,
                    brightness;


                // Build the data arrays
                for(i=0; i<dataLen; i+=1){

                    // add browser data
                    browserData.push({
                        name: categories[i],
                        y: data[i].y,
                        color: data[i].color
                    });

                    // add version data
                    drillDataLen=data[i].drilldown.data.length;
                    for(j=0; j<drillDataLen; j+=1){
                        brightness=0.2-(j/drillDataLen)/5;
                        versionsData.push({
                            name: data[i].drilldown.categories[j],
                            y: data[i].drilldown.data[j],
                            color: Highcharts.Color(data[i].color).brighten(brightness).get()
                        });
                    }
                }

                elem.highcharts({

                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: false
                    },
                    yAxis: {
                        title: {
                            text: 'Total percent market share'
                        }
                    },
                    plotOptions: {
                        pie: {
                            shadow: false,
                            center: ['50%','50%']
                        }
                    },
                    tooltip: {
                        valueSuffix: '%'
                    },
                    series: [{
                        name: 'Browsers',
                        data: browserData,
                        size: '60%',
                        innerSize: '40%',
                        dataLabels: {
                            formatter: function(){
                                return this.y>5?this.point.name:null;
                            },
                            color: '#ffffff',
                            distance: -30
                        },
                        showInLegend: true
                    },{
                        name: 'Versions',
                        data: versionsData,
                        size: '80%',
                        innerSize: '60%',
                        dataLabels: {
                            formatter: function(){
                                // display only if larger than 1
                                return this.y>1?'<b>'+this.point.name+':</b> '+this.y+'%':null;
                            }
                        }
                    }]
                });

            }
        }
    })
    //Outstanding Exposure by sector
    .directive('exposureBySectorGraph',function(){
        return {
            restrict: "EA",
            link: function(scope,elem,attrs){
                elem.highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: false
                    },
                    legend: false,
                    xAxis: {
                        categories: [
                            'Sectors'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: false,
                        gridLineWidth: false,
                        lineColor: '#d1d1d1',
                        lineWidth: 1
                    },
                    tooltip: {
                        shared: true,
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    tooltip: {
                        shared: false
                    },
                    series: [{
                        name: 'Agriculture and Fisheries',
                        data: [7000],
                        color: '#6296d1'

                    },{
                        name: 'Forestry & Agroforestry',
                        data: [2000],
                        color: "#eb7f38"

                    },{
                        name: 'Mining and Quarrying',
                        data: [8000],
                        color: "#ea2a2e"

                    },{
                        name: 'General Industrial Manufacturing',
                        data: [3000],
                        color: "#fcc321"

                    },{
                        name: 'Consumer Industrial Manufacturing',
                        data: [4000],
                        color: "#4f6cb6"

                    },{
                        name: 'Agro, Marine and Food Processing',
                        data: [4000],
                        color: "#3b592d"

                    },{
                        name: 'Construction Companies,Building Materials',
                        data: [8000],
                        color: "#305c91"

                    },{
                        name: 'Hotels, Tourism, Leisure and Entertainment',
                        data: [1000],
                        color: "#9b4e2a"

                    },{
                        name: 'Transport',
                        data: [500],
                        color: "#636364"

                    },{
                        name: 'Commercial Banks',
                        data: [800],
                        color: "#957631"

                    },{
                        name: 'Investment Companies',
                        data: [14000],
                        color: "#2e4378"

                    },{
                        name: 'Micro Finance Institutions',
                        data: [16000],
                        color: "#456a34"

                    },{
                        name: 'Development Financial Institution',
                        data: [7000],
                        color: "#7faada"

                    },{
                        name: 'Electricity',
                        data: [8000],
                        color: "#b5b7b6"

                    },{
                        name: 'Water and Sewers',
                        data: [6000],
                        color: "#3e78be"

                    },{
                        name: 'Oil and Gas',
                        data: [2000],
                        color: "#fdd13b"

                    },{
                        name: 'Alternative energies',
                        data: [1000],
                        color: "#d06530"

                    },{
                        name: 'Education Health and other Cummunity services',
                        data: [2000],
                        color: "#6e89c7"

                    },{
                        name: 'General Trading(holesale and retail,and repairs)',
                        data: [4000],
                        color: "#ef995d"

                    },{
                        name: 'Commercial Real Estate Companies',
                        data: [3000],
                        color: "#8ac468"

                    },{
                        name: 'Residential Real Estate Companies',
                        data: [4000],
                        color: "#848485"

                    },{
                        name: 'Telecommunication Services and Information Technology',
                        data: [7000],
                        color: "#c89d33"

                    }]
                });

            }
        }
    })
    //reports visualization chart
    .directive('annualReportVisualization',function(){
        return {
            restrict: "EA",
            link: function(scope,elem,attrs){
                var graphPattern=
                    elem.highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: false
                        },
                        xAxis: {
                            type: 'category'
                        },
                        colors: ['#6AD0E9','#6d9bc0','#eff0a8','#e3d18c','#90cebc','#c5928f'],
                        tooltip: {
                            backgroundColor: {
                                linearGradient: [0,0,0,60],
                                stops: [
                                    [0,'#FFFFFF'],
                                    [1,'#FFFFFF']
                                ]
                            },
                            borderWidth: 0,
                            borderColor: '#AAA'
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                text: 'Sort by Sector',
                                style: {
                                    fontWeight: 'normal',
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: false
                                },
                                stacking: 'normal'
                            }
                        },

                        series: [{
                            name: 'Agriculture and Fisheries',
                            data: [{
                                name: '2011-12',
                                y: 5,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 2,
                                drilldown: '2012-2013'
                            },{
                                name: '2013-14',
                                y: 4,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 8,
                                drilldown: '2014-15'
                            }]
                        },{
                            name: 'Forestry & Agroforestry',
                            data: [{
                                name: '2011-12',
                                y: 1,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 5,
                                drilldown: '2012-13'
                            },{
                                name: '2013-14',
                                y: 2,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 6,
                                drilldown: '2014-15'
                            }]
                        },{
                            name: 'Mining and Quarrying',
                            data: [{
                                name: '2011-12',
                                y: 1,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 5,
                                drilldown: '2012-13'
                            },{
                                name: '2013-14',
                                y: 2,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 2,
                                drilldown: '2014-15'
                            }]
                        },{
                            name: 'General Industrial Manufacturing',
                            data: [{
                                name: '2011-12',
                                y: 1,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 5,
                                drilldown: '2012-13'
                            },{
                                name: '2013-14',
                                y: 2,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 4,
                                drilldown: '2014-15'
                            }]
                        },{
                            name: 'Consumer Industrial Manufacturing',
                            data: [{
                                name: '2011-12',
                                y: 1,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 5,
                                drilldown: '2012-13'
                            },{
                                name: '2013-14',
                                y: 2,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 3,
                                drilldown: '2014-15'
                            }]
                        },{
                            name: 'Agro, Marine and Food Processing',
                            data: [{
                                name: '2011-12',
                                y: 1,
                                drilldown: '2011-12'
                            },{
                                name: '2012-13',
                                y: 5,
                                drilldown: '2012-13'
                            },{
                                name: '2013-14',
                                y: 2,
                                drilldown: '2013-14'
                            },{
                                name: '2014-15',
                                y: 5,
                                drilldown: '2014-15'
                            }]
                        }]
                    });

            }
        }
    })
    //reports Outstanding Exposure By Currency html
    .directive('reportsOutstandingExposureByCurrency',function(){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var graphPattern=
                    elem.highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: false,
                        xAxis: {
                            categories: ['31-03-2013','30-06-2013','30-09-2013','31-12-2013','31-03-2014','30-06-2014','30-09-2014','31-12-2014','31-03-2015','30-06-2015','30-09-2015','31-12-2015']
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        legend: {
                            align: 'right',
                            x: -30,
                            verticalAlign: 'top',
                            y: 25,
                            floating: true,
                            backgroundColor: (Highcharts.theme&&Highcharts.theme.background2)||'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false
                        },
                        colors: ['#262626','#808080','#002060','#00b0f0','#ffc000','#92d050'],
                        tooltip: {
                            shared: true
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontWeight: 'normal',
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: [
                            {name: 'RWF',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'EUR',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'TGS',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'UGX',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'KES',data: [4000,6000,5000,5000,7000,6000,5000,6000,4000,4000,5000,5000]},
                            {
                                name: 'USD',
                                data: [60000,62000,55000,50000,70000,66000,75000,60000,64000,64000,50000,50000]
                            }]
                    });
            }
        }
    })
    //reports Pertfolio Per Watchlist Category
    .directive('reportsPertfolioPerWatchlistCategory',function(){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var graphPattern=
                    elem.highcharts({
                        chart: {
                            type: 'line'
                        },
                        title: false,
                        xAxis: {
                            categories: ['31-03-2013','30-06-2013','30-09-2013','31-12-2013','31-03-2014','30-06-2014','30-09-2014','31-12-2014','31-03-2015','30-06-2015','30-09-2015','31-12-2015']
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            reversed: true,
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontWeight: 'normal',
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        colors: ['#262626','#808080','#002060','#00b0f0','#ffc000','#92d050'],
                        tooltip: {
                            shared: true
                        },
                        plotOptions: {
                            series: {
                                marker: {
                                    enabled: false
                                }
                            },
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: [{name: 'RWF',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'EUR',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'TGS',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'UGX',data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]},
                            {name: 'KES',data: [4000,6000,5000,5000,7000,6000,5000,6000,4000,4000,5000,5000]},
                            {
                                name: 'USD',
                                data: [60000,62000,55000,50000,70000,66000,75000,60000,64000,64000,50000,50000]
                            }]
                    });
            }
        }
    })
    //reports Outstanding Exposure By Sector
    .directive('reportsOutstandingExposureBySector',function(){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var graphPattern=
                    elem.highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: false,
                        xAxis: {
                            categories: ['31-03-2013','30-06-2013','30-09-2013','31-12-2013','31-03-2014','30-06-2014','30-09-2014','31-12-2014','31-03-2015','30-06-2015','30-09-2015','31-12-2015']
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        tooltip: {
                            shared: false
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'left',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 20,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontWeight: 'normal',
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        series: [{
                            name: 'Telecommunication Servicesa and Information Technology',
                            data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                        },
                            {
                                name: 'Residential Realestate Companies',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'General Trading,Wholesale and Retail,Repairs',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Alternative Energies',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Water and Sewers',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Commercial Realestate Companies',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Education,Health and Other Community Services',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Oil and Gas',
                                data: [0,0,4000,1200,3000,0,1600,0,3000,0,2000,0]
                            },
                            {
                                name: 'Electricity',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Development Financial Institutions',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Micro Finance Institutions',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Investment Companies',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Commercial Banks',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Transport',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Hotels,Tourism,Leisure and Entertainment',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Construction Companies,Building Materials',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Agro,Marine and Food Processing',
                                data: [5000,8000,3000,12000,8000,11000,10600,10000,5000,4000,8000,2000]
                            },
                            {
                                name: 'Consumer Industrial Manufacturing',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'General Industrial Manufacturing',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },
                            {
                                name: 'Mining & Quarrying',
                                data: [2000,3000,4000,1200,3000,1000,1600,1000,3000,2000,2000,2000]
                            },{
                                name: 'Forestry & Agroforestry',
                                data: [4000,6000,5000,5000,7000,6000,5000,6000,4000,4000,5000,5000]
                            },{
                                name: 'Agriculture & Fisheries',
                                data: [1000,1500,2000,1800,4000,1000,1200,1600,3000,4000,6000,3000]
                            }]
                    });
            }
        }
    })




    // dinamic graph start
    //all pie charts
    .directive('reportsPortfolioByCountry',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                attrs.$observe('reportsPortfolioByCountry',function(val){
                    var data=angular.fromJson(attrs.reportsPortfolioByCountry);

                    var graphPattern=elem.highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: false,
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        legend: {
                            useHTML: true,
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            labelFormatter: function(){
                                //this.extra = (this.extra == 0 || this.y == null)?'0':((parseInt( this.y)/parseInt(this.extra))*100).toFixed(2);
                                this.extra=(this.extra==0||this.y==null)?'0':((parseFloat(this.y)/parseFloat(this.extra))*100).toFixed(2);
                                return '<a title="'+this.name+'" >'+this.name+':</a> <span class="font-normal">'+currencyFilter(toBillionFilter(this.y),'')+'</span> <span class="font-normal">('+this.extra+'%)</span> </n>';
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true,
                                point: {
                                    events: {
                                        legendItemClick: function() {
                                            return false;
                                        }
                                    }
                                }
                            }
                        },
                        tooltip: {
                            formatter: function(){
                                if(this.point.options.extraValue) return 'name: '+this.key+' <br/>y:'+this.y+'<br/>Outstanding '+this.point.options.extraValue;
                                else return this.key+' <br/>USD '+currencyFilter(toBillionFilter(this.y),'')+' million';
                            }
                        },
                        series: [{
                            name: 'Brands',
                            colorByPoint: true,
                            data: data
                        }]
                    });


                })

            }
        }
    })
    //currency
    .directive('outstandingExposureByCurrency',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var data=angular.fromJson(attrs.outstandingExposureByCurrency);
                var xAxis=[];
                var yAxis=[];
                var index=0;
                angular.forEach(data,function(item,key){
                    xAxis.push(key);
                    angular.forEach(item,function(val,k){
                        if(index==0){
                            yAxis.push({name: val.currency_code,data: [toBillionFilter(parseInt(val.amount))]});
                        }else{
                            yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                        }
                    });
                    index++;
                });
                $timeout(function(){
                    var graphPattern=elem.highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: false,
                        xAxis: {
                            categories: xAxis
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        tooltip: {
                            shared: true,
                            valueDecimals: 2
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: yAxis
                    });
                });
            }
        }
    })
    .directive('outstandingPerWatchlistByCurrency',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                attrs.$observe('outstandingPerWatchlistByCurrency',function(val){
                    var data=angular.fromJson(attrs.outstandingPerWatchlistByCurrency);
                    var xAxis=[];
                    var yAxis=[];
                    var index=0;
                    angular.forEach(data,function(item,key){
                        xAxis.push(key);
                        angular.forEach(item,function(val,k){
                            if(index==0){
                                yAxis.push({name: val.currency_code,data: [toBillionFilter(parseInt(val.amount))]});
                            }else{
                                yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                            }
                        });
                        index++;
                    });
                    $timeout(function(){
                        var graphPattern=elem.highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: false,
                            xAxis: {
                                categories: xAxis
                            },
                            yAxis: {
                                min: 0,
                                title: false,
                                stackLabels: {
                                    enabled: true,
                                    style: {
                                        fontWeight: 'bold',
                                        color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                    }
                                }
                            },
                            legend: {
                                itemHoverStyle: {
                                    color: '#01aef0'
                                },
                                align: 'right',
                                verticalAlign: 'top',
                                layout: 'vertical',
                                reversed: true,
                                x: 0,
                                y: 0,
                                symbolHeight: 12,
                                symbolWidth: 12,
                                symbolRadius: 6,
                                title: {
                                    style: {
                                        fontSize: '14px',
                                        fontWeight: 'bold',
                                        color: '#33316c',
                                        fontFamily: '"RobotoRegular"'
                                    }
                                }
                            },
                            colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                            tooltip: {
                                shared: true,
                                valueDecimals: 2
                            },
                            plotOptions: {
                                series: {
                                    marker: {
                                        enabled: false
                                    }
                                },
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false
                                    }
                                }
                            },
                            series: yAxis
                        });
                    })

                })
            }
        }
    })
    //sector
    .directive('outstandingExposureBySector',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var data=angular.fromJson(attrs.outstandingExposureBySector);
                var xAxis=[];
                var yAxis=[];
                var index=0;
                angular.forEach(data,function(item,key){
                    xAxis.push(key);
                    angular.forEach(item,function(val,k){
                        if(index==0){
                            yAxis.push({name: val.sector_name,data: [toBillionFilter(parseInt(val.amount))]});
                        }else{
                            yAxis[k].data.push(parseFloat(toBillionFilter(val.amount)));
                        }
                    });
                    index++;
                });
                $timeout(function(){
                    var graphPattern=elem.highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: false,
                        xAxis: {
                            categories: xAxis
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        tooltip: {
                            shared: false,
                            valueDecimals: 2
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'left',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 20,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        series: yAxis
                    });
                })

            }
        }
    })
    //Product
    .directive('outstandingExposureByProduct',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var data=angular.fromJson(attrs.outstandingExposureByProduct);
                var xAxis=[];
                var yAxis=[];
                var index=0;
                angular.forEach(data,function(item,key){
                    xAxis.push(key);
                    angular.forEach(item,function(val,k){
                        if(index==0){
                            yAxis.push({name: val.product_name,data: [toBillionFilter(parseInt(val.amount))]});
                        }else{
                            yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                        }
                    });
                    index++;
                });
                $timeout(function(){
                    var graphPattern=elem.highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: false,
                        xAxis: {
                            categories: xAxis
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        tooltip: {
                            shared: true,
                            valueDecimals: 2
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            thousandsSep: ',',
                            title: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: yAxis
                    });
                })

            }
        }
    })
    .directive('outstandingPerWatchlistByProduct',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                attrs.$observe('outstandingPerWatchlistByProduct',function(val){
                    var data=angular.fromJson(attrs.outstandingPerWatchlistByProduct);
                    var xAxis=[];
                    var yAxis=[];
                    var index=0;
                    angular.forEach(data,function(item,key){
                        xAxis.push(key);
                        angular.forEach(item,function(val,k){
                            if(index==0){
                                yAxis.push({name: val.product_name,data: [toBillionFilter(parseInt(val.amount))]});
                            }else{
                                yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                            }
                        });
                        index++;
                    });
                    $timeout(function(){
                        var graphPattern=elem.highcharts({
                            chart: {
                                type: 'line'
                            },
                            lang: {
                                thousandsSep: ','
                            },
                            title: 'test',
                            xAxis: {
                                categories: xAxis
                            },
                            yAxis: {
                                min: 0,
                                title: false,
                                stackLabels: {
                                    enabled: true,
                                    style: {
                                        fontWeight: 'bold',
                                        color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                    }
                                }
                            },
                            legend: {
                                itemHoverStyle: {
                                    color: '#01aef0'
                                },
                                align: 'right',
                                verticalAlign: 'top',
                                layout: 'vertical',
                                reversed: true,
                                x: 0,
                                y: 0,
                                symbolHeight: 12,
                                symbolWidth: 12,
                                symbolRadius: 6,
                                title: {
                                    style: {
                                        fontSize: '14px',
                                        fontWeight: 'bold',
                                        color: '#33316c',
                                        fontFamily: '"RobotoRegular"'
                                    }
                                }
                            },
                            colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                            tooltip: {
                                shared: true,
                                valueDecimals: 2
                            },
                            plotOptions: {
                                series: {
                                    marker: {
                                        enabled: false
                                    }
                                },
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false
                                    }
                                }
                            },
                            series: yAxis
                        });
                    })

                })
            }
        }
    })
    //Product
    .directive('outstandingExposureByCountry',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                var data=angular.fromJson(attrs.outstandingExposureByCountry);
                var xAxis=[];
                var yAxis=[];
                var index=0;
                angular.forEach(data,function(item,key){
                    xAxis.push(key);
                    angular.forEach(item,function(val,k){
                        if(index==0){
                            yAxis.push({name: val.country_name,data: [toBillionFilter(parseInt(val.amount))]});
                        }else{
                            yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                        }
                    });
                    index++;
                });
                $timeout(function(){
                    var graphPattern=elem.highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: false,
                        xAxis: {
                            categories: xAxis
                        },
                        yAxis: {
                            min: 0,
                            title: false,
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                }
                            }
                        },
                        colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                        tooltip: {
                            shared: true,
                            valueDecimals: 2
                        },
                        legend: {
                            itemHoverStyle: {
                                color: '#01aef0'
                            },
                            reversed: true,
                            align: 'right',
                            verticalAlign: 'top',
                            layout: 'vertical',
                            x: 0,
                            y: 0,
                            symbolHeight: 12,
                            symbolWidth: 12,
                            symbolRadius: 6,
                            title: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'bold',
                                    color: '#33316c',
                                    fontFamily: '"RobotoRegular"'
                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: yAxis
                    });
                })

            }
        }
    })
    .directive('outstandingPerWatchlistByCountry',function(toBillionFilter,currencyFilter,$timeout){
        return {
            restrict: "E,A",
            link: function(scope,elem,attrs){
                attrs.$observe('outstandingPerWatchlistByCountry',function(val){
                    var data=angular.fromJson(attrs.outstandingPerWatchlistByCountry);
                    var xAxis=[];
                    var yAxis=[];
                    var index=0;
                    angular.forEach(data,function(item,key){
                        xAxis.push(key);
                        angular.forEach(item,function(val,k){
                            if(index==0){
                                yAxis.push({name: val.country_name,data: [toBillionFilter(parseInt(val.amount))]});
                            }else{
                                yAxis[k].data.push(toBillionFilter(parseInt(val.amount)));
                            }
                        });
                        index++;
                    });
                    $timeout(function(){
                        var graphPattern=elem.highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: false,
                            xAxis: {
                                categories: xAxis
                            },
                            yAxis: {
                                min: 0,
                                title: false,
                                stackLabels: {
                                    enabled: true,
                                    style: {
                                        fontWeight: 'bold',
                                        color: (Highcharts.theme&&Highcharts.theme.textColor)||'gray'
                                    }
                                }
                            },
                            legend: {
                                itemHoverStyle: {
                                    color: '#01aef0'
                                },
                                align: 'right',
                                verticalAlign: 'top',
                                layout: 'vertical',
                                reversed: true,
                                x: 0,
                                y: 0,
                                symbolHeight: 12,
                                symbolWidth: 12,
                                symbolRadius: 6,
                                title: {
                                    style: {
                                        fontSize: '14px',
                                        fontWeight: 'bold',
                                        color: '#33316c',
                                        fontFamily: '"RobotoRegular"'
                                    }
                                }
                            },
                            colors: ['#002060','#ffc000','#ff0000','#00b0f0','#92d050','#006666','#570efd','#dd76a1','#694c23','#ff6704'],
                            tooltip: {
                                shared: true,
                                valueDecimals: 2
                            },
                            plotOptions: {
                                series: {
                                    marker: {
                                        enabled: false
                                    }
                                },
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false
                                    }
                                }
                            },
                            series: yAxis
                        });
                    })

                })
            }
        }
    })
    //dropdown scroll
    /*.directive('dropdownScrollOverflow', function($window){
     return {
     restrict: "A",
     link: function(scope,elem,attr) {
     var dropdownMenu;

     // and when you show it, move it to the body
     $(window).on('show.bs.dropdown', function (e) {

     // grab the menu
     dropdownMenu = $(e.target).find('.dropdown-menu');

     // detach it and append it to the body
     $('body').append(dropdownMenu.detach());

     // grab the new offset position
     var eOffset = $(e.target).offset();

     // make sure to place it where it would normally go (this could be improved)
     dropdownMenu.css({
     'display': 'block',
     'top': eOffset.top + $(e.target).outerHeight(),
     'left': eOffset.left - 130
     });
     });

     // and when you hide it, reattach the drop down, and hide it normally
     $(window).on('hide.bs.dropdown', function (e) {
     $(e.target).append(dropdownMenu.detach());
     dropdownMenu.hide();
     });
     /!*$(".some-c").mCustomScrollbar({
     callbacks:{

     onScrollStart:function(){
     alert();
     $(e.target).append(dropdownMenu.detach());
     dropdownMenu.hide();
     }
     }
     });*!/
     }
     }
     })*/

    /**
     * projectRiskAssessmentCtrl
     */
    .filter('attrFactor',function(){
        return function(sub_category){
            var count=0;
            var factorSum=0;
            angular.forEach(sub_category.attributes,function(item){
                if(item.grade!=undefined&&item.grade>0){
                    factorSum=factorSum+parseInt(item.grade);
                    count++;
                }
            })
            var totalSum=factorSum/count;
            if(isNaN(totalSum)){
                totalSum=0;
            }
            sub_category.factor=totalSum;
            return totalSum.toFixed(2);
            // do some bounds checking here to ensure it has that index
        }
    })
    .filter('subCategoryFactor',function(){
        return function(cateogry){
            var count=0;
            var factorSum=0;
            angular.forEach(cateogry.sub_category,function(item){
                if(item.factor!=undefined&&item.factor>0){
                    factorSum=factorSum+(parseInt(item.risk_percentage)*parseFloat(item.factor));
                }
            })
            return (factorSum/100).toFixed(2);
        }
    })
    .filter('ellipsis',function(){
        return function(text,limit){
            var changedString=String(text).replace(/<[^>]+>/gm,'');
            var length=changedString.length;
            return changedString.length>limit?changedString.substr(0,limit-1)+'...':changedString;
        }
    })
    .filter('limitchr',function(){
        return function(text,limit){
            var changedString=String(text).replace(/<[^>]+>/gm,'');
            var length=changedString.length;
            var suffix = '...';
            return changedString.length>limit?(changedString.substr(0,limit-1))+suffix:changedString;
        }
    })
    .directive('hotSize',function(){ //declaration; identifier master
        function link(scope,element,attrs){ //scope we are in, element we are bound to, attrs of that element
            scope.$watch(function(){ //watch any changes to our element
                var hotTableHeight=element.find('.wtHider').height();
                var tableHeight=element.find('.wtHolder').height();
                var setTableHeight=0;
                if(hotTableHeight<tableHeight){
                    setTableHeight=hotTableHeight;
                }else{
                    setTableHeight=tableHeight;
                }
                element.css({ //scope variable style, shared with our controller
                    height: setTableHeight+20+'px', //set the height in style to our elements height
                });
            });
        }

        return {
            restrict: 'AE', //describes how we can assign an element to our directive in this case like <div master></div
            link: link // the function to link to our element
        };
    })
    .directive('parentActive',function(){
        return {
            link: function(scope,element){
                var leng=element.find('ul li.in').length;
                scope.$watch(function(){
                    return element.html();
                },function(){
                    var leng=element.find('ul li.in').length;
                    if(leng>0){
                        element.addClass('active parent-sub');
                        //element.find('.sub-menu').css('display',"block");
                    }else{
                        element.removeClass('active parent-sub');
                        // element.find('.sub-menu').css('display',"none");
                    }
                })
            },
        };
    })
    .directive("scrollPos",function($window){
        return function(scope,element,attrs){
            angular.element($window).bind("scroll",function(){
                element.css({
                    'top': this.pageYOffset
                })
                scope.visible=false;
                scope.$apply();
            });
        };
    })
    .directive('textLine',function(){
        return {
            link: function(scope,element){
                element.addClass('text-line');
                element.bind('click',function(){
                    element.toggleClass('text-line');
                });
            },
        };
    })

    .directive('iconAlltouchpointsTypes',function(){
        return {
            link: function(scope,element,attrs){
                switch(attrs.tuchpointtype){
                    case 'touchpoint':
                        if(attrs.isremainder==1){
                            element.addClass('alert-box');
                        }
                        else{
                            element.addClass('red-box');
                        }
                        element.find('.t-icon i').addClass('icon-touchpoint');
                        break;
                    case 'contact':
                        element.addClass('sky-blue-box');
                        element.find('.t-icon i').addClass('icon-users2');
                        break;
                    case 'attachment':
                        element.addClass('attachment-box');
                        element.find('.t-icon i').addClass('icon-workload');
                        break;
                    case 'company':
                        element.addClass('sky-blue-box');
                        element.find('.t-icon i').addClass('icon-building');
                        break;
                    case 'mail':
                        element.addClass('mail-box');
                        element.find('.t-icon i').addClass('icon-mail');
                        break;
                    case 'meeting':
                        element.addClass('meeting-box');
                        element.find('.t-icon i').addClass('icon-users1');
                        break;
                    case 'knowledge':
                        element.addClass('knowledge-box');
                        element.find('.t-icon i').addClass('icon-clipped-file2');
                        break;
                    case 'facility':
                        element.addClass('facility-box');
                        element.find('.t-icon i').addClass('icon-info');
                        break;
                    case 'approval':
                        element.addClass('approval-box');
                        element.find('.t-icon i').addClass('icon-right-circle');
                        break;
                    default :
                        element.addClass('red-box');
                        element.find('.t-icon i').addClass('icon-clipped-file');
                        break;
                }
            }
        };
    })
    .directive('orgchart',function(){
        return {
            restrict: 'AE',
            scope: {
                source: '=',
                container: '=',
                stack: '=',
                depth: '=',
                interactive: '=',
                showLevels: '=',
                clicked: '=' // We use '=' here rather than '&' for this callback function
            },
            link: function(scope,element,attrs){
                $("#basic-styles-source").orgChart({
                    container: $("#chart"),
                    interactive: true
                });
            }
        }
    })
    .directive('questionOption',function(){
        return {
            restrict: 'E',
            link: function(scope,element,attrs){
                // some ode
            },
            templateUrl: function(elem,attrs){
                return attrs.templateUrl||'partials/directive-templates/question_options/all_inputs.html'
            }
        }
    })
    .directive('numberFloat',function(){
        return {
            require: '?ngModel',
            link: function(scope,element,attrs,ngModelCtrl){
                if(!ngModelCtrl){
                    return;
                }
                ngModelCtrl.$parsers.push(function(val){
                    if(angular.isUndefined(val)){
                        var val='';
                    }
                    var clean=val.replace(/[^-0-9\.]/g,'');
                    var negativeCheck=clean.split('-');
                    var decimalCheck=clean.split('.');
                    if(!angular.isUndefined(negativeCheck[1])){
                        negativeCheck[1]=negativeCheck[1].slice(0,negativeCheck[1].length);
                        clean=negativeCheck[0]+'-'+negativeCheck[1];
                        if(negativeCheck[0].length>0){
                            clean=negativeCheck[0];
                        }
                    }
                    if(!angular.isUndefined(decimalCheck[1])){
                        decimalCheck[1]=decimalCheck[1].slice(0,2);
                        clean=decimalCheck[0]+'.'+decimalCheck[1];
                    }
                    if(val!==clean){
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress',function(event){
                    if(event.keyCode===32){
                        event.preventDefault();
                    }
                });
            }
        };
    })


    /**
     * table wediget
     */
    .directive('tableWidget',function($compile){
        return {
            restrict: 'E',
            scope: {
                tableWidgetVar: '=',
                //bindAttr: '='
            },
            // replace: true,
            //require: 'ngModel',
            controller: function($scope){
                $scope.addTwRow=function(){
                    alert(0);
                };
            },
            link: function($scope,elem,attr,ctrl){
                $scope.row={};
                if($scope.tableWidgetVar==undefined){
                    $scope.tableWidgetVar=[];
                }
                // remove user
                $scope.removeUser=function(index){
                    $scope.tableWidgetVar.splice(index,1);
                };
                // add user
                $scope.addTwRow=function(row){
                    alert(0);
                    $scope.row.id=$scope.tableWidgetVar.length+1;
                    $scope.tableWidgetVar.push(row);
                    row={};
                };
            }
        };
    })
    /**
     * Html Table convert to excel file and download
     *
     *
     */
    .factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){
                return $window.btoa(unescape(encodeURIComponent(s)));
            },
            format=function(s,c){
                return s.replace(/{(\w+)}/g,function(m,p){
                    return c[p];
                })
            };
        return {
            tableToExcel: function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet: worksheetName,table: table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
    .filter('dateToISO',function(){
        return function(input){
            input=new Date(input).toISOString();
            return input;
        };
    })
    .filter('utcToLocal',function($filter,moment){
        return function(dateString,format){

            if(!format||format=='')
                format='medium';

            switch(format){
                case 'date':
                    format='MMM d, y';
                    break;
                case 'time':
                    format='h:mm:ss a';
                    break;
                case 'datetime':
                    format='yyyy-MM-dd HH:mm:ss';
                    break;
                default:
                    break;
            }
            if(!dateString||dateString==''){
                return dateString;
            }else{
                var utcDate=moment.utc(dateString).toDate();
                var date=$filter('date')(utcDate,format);
                return date;
            }
        };
    })
    .filter('trusted',function($sce){
        return function(html){
            return $sce.trustAsHtml(html)
        }
    })
    .directive('novalidate',function(){

        return {
            link: function($scope,elem,attrs,$rootScope){
                /*if (elem[0].nodeName === 'FORM') { //the user wants to check a form
                 window.onbeforeunload = function(){
                 if (angular.element(elem[0]).hasClass('ng-dirty') && !angular.element(elem[0]).hasClass('ng-submitted')) {
                 return "You have unsaved changes - are you sure you want to leave the page?";
                 }
                 }
                 $scope.$on('$locationChangeStart', function(event, next, current) {
                 console.log(angular.element(elem[0]).hasClass('ng-dirty'));
                 if (angular.element(elem[0]).hasClass('ng-dirty') && !angular.element(elem[0]).hasClass('ng-submitted')) {
                 if(!confirm("You have unsaved changes - are you sure you want to leave the page?")) {
                 event.preventDefault();
                 }
                 }
                 });
                 }*/

                // set up event handler on the form element
                elem.on('submit',function(){
                    // find the first invalid element
                    var firstInvalid=elem[0].querySelector('.ng-invalid');
                    if(firstInvalid){
                        var height=angular.element(firstInvalid).offset().top-220;
                        $("body").animate({scrollTop: height},800,function(){
                            firstInvalid.focus();
                        });
                    }
                });
            }
        };
    })
    .filter("s2o",function(){
        return function(cls){
            var j=JSON.parse(cls);
            //console.log(j);
            return j;
        }

    })
    .directive('toggleCheckbox',function(){

        /**
         * Directive
         */
        return {
            restrict: 'A',
            transclude: true,
            replace: false,
            require: 'ngModel',
            link: function($scope,$element,$attr,require){

                var ngModel=require;

                // update model from Element
                var updateModelFromElement=function(){
                    // If modified
                    var checked=$element.prop('checked');
                    if(checked!=ngModel.$viewValue){
                        // Update ngModel
                        ngModel.$setViewValue(checked);
                        $scope.$apply();
                    }
                };

                // Update input from Model
                var updateElementFromModel=function(){
                    // Update button state to match model
                    $element.trigger('change');
                };

                // Observe: Element changes affect Model
                $element.on('change',function(){
                    updateModelFromElement();
                });

                // Observe: ngModel for changes
                $scope.$watch(function(){
                    return ngModel.$viewValue;
                },function(){
                    updateElementFromModel();
                });

                // Initialise BootstrapToggle
                $element.bootstrapToggle();
            }
        };
    })

    /**/
    .directive('nT',['$location','Auth',function($location,Auth){
        return {
            restrict: 'A',

            link: function($scope,elem,attr){

                // set up event handler on the form element
                if(ACCESS_LOG_ENABLE){
                    elem.on('click',function(){
                        var IsCorrect=false;
                        var attrData=angular.fromJson(attr.nT);
                        var data={
                            'event_name': attrData.e_n,
                            'event_description': attrData.e_d,
                        }
                        /*var data = angular.fromJson(attr.nT);*/
                        /*angular.fromJson(attr.eventText)*/
                        data.url=$location.absUrl();
                        var userDetails=angular.fromJson(Auth.getFields());
                        var logObj={};
                        /*var access_time=new Date(new Date().toISOString());*/
                        var access_time=new Date(new Date().toISOString());
                        logObj.keys={
                            'user_id': userDetails.id_user,
                            user_name: userDetails.first_name+' '+userDetails.last_name,
                            'access_time': access_time
                        };
                        logObj.data=data;
                        logObj.log_auth_key=LOG_AUTH_KEY;
                        if(attrData.hasOwnProperty('form')){
                            if(attrData.form=='true'){
                                /*console.log([logObj]);*/
                                IsCorrect=true;
                            }
                        }else{
                            /*console.log([logObj]);*/
                            IsCorrect=true;
                        }
                        if(IsCorrect){
                            console.log('access :'+logObj);
                            Auth.userLogs([logObj]).then(function(result){
                                if(result.status){
                                    //console.log('log success');
                                }
                            });
                        }
                    });
                }

            }
        };
    }])

    .directive("passwordVerify",function(){
        return {
            require: "ngModel",
            scope: {
                passwordVerify: '='
            },
            link: function($scope,element,attrs,ctrl){
                $scope.$watch(function(){
                    var combined;

                    if($scope.passwordVerify||ctrl.$viewValue){
                        combined=$scope.passwordVerify+'_'+ctrl.$viewValue;
                    }
                    return combined;
                },function(value){
                    if(value){
                        ctrl.$parsers.unshift(function(viewValue){
                            var origin=$scope.passwordVerify;
                            if(origin!==viewValue){
                                ctrl.$setValidity("passwordVerify",false);
                                return undefined;
                            }else{
                                ctrl.$setValidity("passwordVerify",true);
                                return viewValue;
                            }
                        });
                    }
                });
            }
        };
    })

    //is filtered
    /*.directive('isFiltered',function(){
     return{
     restrict:'A',
     link:function($scope,element){
     var filteredActive = element.find('li');

     element.click(function () {
     var i = 0;
     angular.forEach(filteredActive, function (value, key) {
     if($(value).hasClass('active')){ i++; }
     });
     if(i > 0){
     $('#sector-filter-panel-total-wrap').css('display','block');
     }else {
     $('#sector-filter-panel-total-wrap').css('display','none');
     }
     })
     console.log('filteredActive',filteredActive);
     }

     }
     })*/
    .directive('alertBlink',function(){
        return {
            restrict: 'A',
            link: function($scope,element){
                //console.log('alertBlink');
                setInterval(function(){
                    element.fadeTo(100,0.1).fadeTo(200,1.0);
                },1000);
            }
        }
    })


    .directive('confirmOnExit',function(){
        return {
            link: function($scope,elem,attrs){
                if(elem[0].nodeName==='FORM'){ //the user wants to check a form
                    window.onbeforeunload=function(){
                        if(angular.element(elem[0]).hasClass('ng-dirty')&& !angular.element(elem[0]).hasClass('ng-submitted')){
                            return "You have unsaved changes - are you sure you want to leave the page?";
                        }
                    }
                    $scope.$on('$locationChangeStart',function(event,next,current){
                        if(angular.element(elem[0]).hasClass('ng-dirty')&& !angular.element(elem[0]).hasClass('ng-submitted')){
                            if(!confirm("You have unsaved changes - are you sure you want to leave the page?")){
                                event.preventDefault();
                            }
                        }
                    });
                }
                else{ //the user wants to check a specific collection
                    if(attrs.confirmOnExit){
                        var loaded=false;
                        var hasUnsavedChanges=false;
                        $scope.$watchCollection(
                            attrs.confirmOnExit,
                            function(newValue,oldValue){
                                if(!loaded&&newValue!==undefined){ // the collection will change once on initialization; allow for this
                                    loaded=true;
                                }
                                else if(newValue!==undefined){ // the collection has already been initialized; mark it as changed
                                    hasUnsavedChanges=true;
                                }
                            }
                        );

                        $scope.$on('$locationChangeStart',function(event,next,current){
                            if(hasUnsavedChanges){
                                if(!confirm("Make sure you save your changes before you go!")){
                                    event.preventDefault();
                                }
                            }
                        });
                    }
                    else{
                        //console.log('Please pass in a collection for confirm-on-exit to work correctly.');
                        //console.log('Example: <div confirm-on-exit="collectionName">...</div>');
                    }
                }
            }
        };
    })
    /*Used in Collater list, activity list, facility list*/
    .directive('accessDenied',function(){
        return {
            restrict: 'A',
            replace: true,
            require: 'ngModel',
            //templateUrl points to an external html template.
            templateUrl: function(elem,attrs){
                return 'partials/error/access-denied.html'
            }
        }
    })
    /*Used in Collater list, activity list, facility list*/
    .directive('compareTo',function(){
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope,element,attributes,ngModel){

                ngModel.$validators.compareTo=function(modelValue){
                    return modelValue==scope.otherModelValue;
                };

                scope.$watch("otherModelValue",function(){
                    ngModel.$validate();
                });
            }
        }
    })


    .directive('graphClass',function(){
        return {
            restrict: 'A',
            link: function($scope,elem,attr){
                attr.$observe('graphClass',function(val){
                    elem.removeClass('icon-up-arrow arrow-up');
                    elem.removeClass('icon-up-arrow arrow-nutral');
                    elem.removeClass('icon-down-arrow arrow-down');
                    switch(attr.graphClass){
                        case 'high':
                            elem.addClass('icon-up-arrow arrow-up');
                            break;
                        case 'equal':
                            elem.addClass('icon-up-arrow arrow-nutral');
                            break;
                        case 'low':
                            elem.addClass('icon-down-arrow arrow-down');
                            break;
                    }
                })

            }
        }
    })

    .directive('myDate',function(dateFilter){
        return {
            restrict: 'EAC',
            require: '?ngModel',
            link: function(scope,element,attrs,ngModel){
                ngModel.$parsers.push(function(viewValue){
                    return dateFilter(viewValue,'MM-dd-yyyy');
                });
            }
        };

    })

    .factory('twelveMonths',function(){
        return {
            getMonths: function(){
                var date=new Date();
                var months=[],
                    monthNames=["Jan","Feb","Mar","Apr","May","Jun",
                        "Jul","Aug","Sep","Oct","Nov","Dec"];
                for(var i=0; i<12; i++){
                    months.push(monthNames[date.getMonth()]+' '+date.getFullYear());

                    // Subtract a month each time
                    date.setMonth(date.getMonth()-1);
                }
                return months;
            }
        };
    })

    .filter('toBillion',function(){
        return function(input){
            return parseFloat((input/1000000).toFixed(2));
            //return ((input/1000000).toFixed(2));
        }
    })
    .filter('toThousand',function(){
        return function(input){
            return parseFloat((input/1000).toFixed(2));
            //return ((input/1000).toFixed(2));
        }
    })
    .directive("detectFormChange", function($parse, $interpolate){
        return {
            require: "form",
            link: function(scope, element, attrs, form){
                var cb = $parse(attrs.detectFormChange);
                element.on("change", function(){
                    cb(scope);
                });
            }
        };
    })



angular.module('app')
    .controller('AppCtrl', function (Idle,$rootScope,$modalStack,$scope, Auth, onlineStatus, $translate, $cookies, $http, $location, notificationService, redirectToUrlAfterLogin, appService,$uibModal) {

        $scope.logout = function () {
            $rootScope.addCustumAccessLogs('Logout', '');   //Added to get Access log while logout
            $http.post(API_URL + 'user/logout').then(function (response) {
            });
            redirectToUrlAfterLogin.direct_entry = true;
            // modalInstance.close();
            $modalStack.dismissAll();
            Auth.logout();
        };
        //idle login
        function closeModals() {
            if ($scope.warning) {
                $scope.warning.close();
                $scope.warning = null;
            }

            if ($scope.timedout) {
                $scope.timedout.close();
                $scope.timedout = null;
            }
        }
        $scope.$on('IdleStart', function() {
            closeModals();

            $scope.warning = $uibModal.open({
                templateUrl: 'partials/error/warning-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $scope.$on('IdleEnd', function() {
            closeModals();

        });
        $scope.$on('IdleTimeout', function() {
            closeModals();
            if(Auth.isLoggedIn()){
                $scope.timedout = $uibModal.open({
                    templateUrl: 'partials/error/timedout-dialog.html',
                    windowClass: 'modal-danger',
                });
                $scope.logout();
            }
        });

        /*$rootScope.addCustumAccessLogs = function (title, description) {
         // set up event handler on the form element
         if (ACCESS_LOG_ENABLE) {
         var IsCorrect = false;
         /!*var attrData = angular.fromJson(attr.nT);*!/
         var data = {
         'event_name': title,
         'event_description': description,
         };
         data.url = $location.absUrl();
         var userDetails = angular.fromJson(Auth.getFields());
         var logObj = {};
         /!*var access_time=new Date(new Date().toISOString());*!/
         var access_time = new Date(new Date().toISOString());
         logObj.keys = {
         'user_id': userDetails.id_user,
         user_name: userDetails.first_name + ' ' + userDetails.last_name,
         'access_time': access_time
         };
         logObj.data = data;
         logObj.log_auth_key = LOG_AUTH_KEY;

         Auth.userLogs([logObj]).then(function (result) {
         if (result.status) {
         //console.log('log success');
         }
         });
         }
         };*/
        $scope.menuOpened = false;
        $scope.toggleMenu = function (element, event) {
            $scope.menuOpened = !($scope.menuOpened);
            event.stopPropagation();
        };
        window.onclick = function (e) {
            if ($scope.menuOpened) {
                $scope.menuOpened = false;
                $scope.$apply();
            }
        };
        $scope.onlineStatus = onlineStatus;
        $scope.$watch('onlineStatus.isOnline()', function (online) {
            $scope.online_status_string = online ? 'online' : 'offline';
            if (!online) {
                $rootScope.toast('ERR_INTERNET_DISCONNECTED', 'Unable to connect to the Internet', 'warning_m');
            }
        });
        if ($cookies.get('lan_code')) {
            $rootScope.lg_code = $cookies.get('lan_code');
            $translate.use($rootScope.lg_code);
        } else {
            $rootScope.lg_code = 'en';
        }
        $scope.changeLanguage = function (key) {
            $cookies.put('lan_code', key);
            $translate.use(key);
        };
        //notification
        $rootScope.notification_count = 0;
        $rootScope.getNotificationCount = function () {
            notificationService.count({'user_id': $rootScope.userId}).then(function (result) {
                $scope.notification_count = result.data;
            })
        }
        $scope.notificationList = [];
        $scope.getNotificationList = function (isOpen) {
            if (isOpen) {
                notificationService.list({
                    'user_id': $rootScope.userId,
                    'limit': 3,
                    'notification_status': 'unread'
                }, 'notification_loader').then(function (result) {
                    $scope.notificationList = result.data.data;
                    console.log('result list', result);
                })
            }

        }
        $scope.menuList = [];
        $scope.getMenuList = function () {
            appService.menu.get({'user_id': $rootScope.userId}).then(function (result) {
                $scope.menuList = result.data;
            })
        }
        /*
         var theInterval = $interval(function(){
         $scope.getNotificationCount()
         }.bind(this), 1000);
         */

        $rootScope.getNotificationCount();

    })
    .controller('notificationCtrl', function ($scope, $rootScope, notificationService) {
        $scope.isLoading = true;
        $scope.notificationList = [];
        $scope.f_type = 'all';
        $scope.getNotificationList = function (tableState) {
            $scope.isLoading = true;
            $scope.notificationList = [];
            tableState.user_id = $rootScope.userId;
            tableState.filter = $scope.f_type;
            notificationService.list(tableState).then(function (result) {
                $scope.notificationList = result.data.data;
                $scope.filter_count = result.data.filter_count;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
                $scope.isLoading = false;
            })
        }
        $scope.nStatusChange = function (n) {
            if (n.notification_status == 'unread') {
                var param = {};
                param.notification_status = 'read';
                param.id_notification = n.id_notification;
                notificationService.changeStatus(param).then(function (result) {
                    if (result.status)
                        n.notification_status = 'read';
                    $rootScope.getNotificationCount();
                })

            }
        }


        //$scope.getNotificationList();
    })
    .controller('LoginCtrl', function ($rootScope, $scope, Auth, UserService, $state, encodeFilter) {
        $scope.showQuestion = false;
        $scope.user = {};
        $scope.user = {"email_id": '', "password": ''};
        $scope.secureLogin = false;
        $scope.userSecurityData = '';
        $scope.submitForm = function (user) {
            // check to make sure the form is completely valid
            if ($scope.loginForm.$valid) {
                var param = {};
                param.password = encodeFilter(user.password);
                param.email_id = user.email_id;
                param.session_exceed = user.session_exceed;
                UserService.login(param).then(function (result) {
                    if (result.status) {
                        //console.log('result.status', result.status);
                        var userObj = result.data;
                        userObj.access_token = result.access_token;
                        Auth.login(userObj);
                        $rootScope.addCustumAccessLogs('Login', '');   //Added to get Access log while login
                        $state.go("app.dashboard");
                    } else {
                        if (result.data == 'session_exceed'){
                            $rootScope.toast('Alert', result.error, 'warning_l');
                            $scope.secureLogin = true;
                        }

                        $rootScope.toast('Error', result.error, 'error', $scope.user);    //old code
                    }
                });
            }
        };
        $scope.data = {"emailId": ''};
        $scope.sendEmail = function (emailId, forgotPasswordForm) {
            if (forgotPasswordForm.$valid) {
                UserService.getUserQuestion({email_id: emailId}).then(function (result) {
                    if (result.status) {
                        $scope.userSecurityData = result.data;
                        $scope.showQuestion = true;
                    } else {
                        $rootScope.toast('Error', result.error, 'error', {'emailId': ''});
                    }
                })
            }
        }
        $scope.send = function (data, forgotPasswordForm) {
            if (forgotPasswordForm.$valid) {
                console.log(data);
                UserService.forgotPassword(data).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        angular.element('#cancelSecurity').trigger('click');
                    } else {
                        $rootScope.toast('Error', result.error, 'error', {'emailId': ''});
                    }
                })
            }
        }
        $scope.forgotPassword = function () {
            $scope.showQuestion = false;
        }
    })
    .controller('ForgotPasswordCtrl', function ($rootScope, $scope, UserService, $state) {
        $scope.send = function (email) {
            UserService.forgotPassword(email).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                    $state.go("app.login");
                } else {
                    $rootScope.toast('Error', result.error, 'error', {'email_id': ''});
                }
            })
        }
    })
    .controller('ChangePasswordCtrl', function ($rootScope, $scope, UserService, $state, Auth, $cookies) {
        $scope.user = {'oldpassword': '', 'password': '', 'cpassword': '', 'user_id': $rootScope.userId};
        $scope.newInputType='password';
        $scope.oldInputType='password';
        $scope.cinputType='password';
        $scope.confirmPasswordCheckbox = false;
        $scope.oldPasswordCheckbox = false;
        $scope.newPasswordCheckbox = false;

        $scope.hideShowPassword = function(pwdType){
            if(pwdType == 'oldpassword'){
                if ($scope.oldInputType == 'password'){
                    $scope.oldPasswordCheckbox = true;
                    $scope.oldInputType = 'text';
                } else{
                    $scope.oldPasswordCheckbox = false;
                    $scope.oldInputType = 'password';
                }
            }
            if(pwdType == 'newpassword'){
                if ($scope.newInputType == 'password'){
                    $scope.newPasswordCheckbox = true;
                    $scope.newInputType = 'text';
                } else{
                    $scope.newPasswordCheckbox = false;
                    $scope.newInputType = 'password';
                }
            }
            if(pwdType == 'cpassword'){
                if ($scope.cinputType == 'password'){
                    $scope.confirmPasswordCheckbox = true;
                    $scope.cinputType = 'text';
                } else{
                    $scope.confirmPasswordCheckbox = false;
                    $scope.cinputType = 'password';
                }
            }
        };
        $scope.save = function (user) {
            UserService.changePassword($scope.user).then(function (result) {
                if (result.status) {
                    var temp = angular.fromJson(Auth.getFields());
                    temp.password_expire_days = 90;
                    $cookies.put('app', angular.toJson(temp));
                    $rootScope.notification = false;
                    $rootScope.toast('Success', result.message);
                    $state.go('app.my-profile');
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.user);
                }
            })
        }
        $scope.cancel = function () {
            $state.go('app.my-profile');
        };
    })
    .controller('MyProfileCtrl', function ($rootScope, $scope, $uibModal, UserService, $state) {
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.open = function () {
            $state.go('app.change-password');
        };
        /*$scope.openSecurity = function () {
         var modalInstance = $uibModal.open({
         animation: $scope.animationsEnabled,
         templateUrl: 'partials/change-security-question.html',
         controller: function ($rootScope, $scope, $uibModalInstance, UserService,masterService,$q) {


         var promise2 = UserService.getUserProfile({'id': $rootScope.userId}).then(function (result) {
         $scope.field = result.data;
         });
         $q.all([promise2]).then(function (data) {
         $rootScope.loaderOverlay = false;
         masterService.masterList({'master_key':'security_questions'}).then(function (result) {
         if (result.status)
         $scope.security_questions = result.data;
         })
         })

         $scope.submitForm = function ($inputData) {
         UserService.updateSecurityQuestion($inputData).then(function (result) {
         if (result.status) {
         $rootScope.toast('Success', result.message);
         } else {
         $rootScope.toast('Error', result.error, 'error', $scope.sector);
         }
         })
         };
         $scope.cancel = function () {
         $uibModalInstance.dismiss('cancel');
         };
         }
         });
         modalInstance.result.then(function (selectedItem) {
         //$scope.selected = selectedItem;
         }, function () {
         // $log.info('Modal dismissed at: ' + new Date());
         });
         };*/
        $scope.getUserProfile = function () {
            UserService.getUserProfile({'id': $rootScope.userId}).then(function (result) {
                if (result.status) {
                    /*$scope.userDetails = result.data;*/
                    $scope.user = result.data;
                } else {
                    $rootScope.toast(result.error);
                }
            })
        }
        $scope.getUserProfile();
        $scope.displayed = [];
        $scope.callServer = function callServer(tableState) {
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.
            tableState.user_id = $rootScope.userId;
            UserService.getLoginHistory(tableState).then(function (result) {
                $scope.displayed = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };
        $scope.callServerNotification = function callServer(tableState) {
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.
            tableState.company_id = $rootScope.id_company;
            UserService.getUserNotifications(tableState).then(function (result) {
                $scope.notification = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };
    })
    .controller('editProfileCtrl', function (decodeFilter, $rootScope, $scope, companyService, Upload, $state, $location, $q, masterService, UserService) {
        $scope.compnayUsers = [];
        $scope.uploadUserImage = function (file) {
            if (file != null && file != '') {
                $scope.userLogoRemove();
                setTimeout(function () {
                    $scope.userImage = file;
                    $scope.trash = true;
                    $scope.$apply();
                }, 100)
            }
        };
        $scope.userLogoRemove = function () {
            $scope.userImage = '';
            $scope.trash = false;
        };
        $scope.title = 'Edit Profile';
        $scope.btn = 'Update';
        $rootScope.company_user_id = $rootScope.userId;
        $scope.user = {
            "email_id": '',
            "phone_number1": '',
            "last_name": '',
            "first_name": '',
            "company_id": '',
            "user_status": ''
        };
        if ($rootScope.company_user_id) {
            var promise2 = masterService.country.get().then(function (result) {
                $scope.countries = result.data;
            });
            $q.all([promise2]).then(function (data) {
                $rootScope.loaderOverlay = false;
                UserService.getUserProfile({'id': $rootScope.userId}).then(function (result) {
                    $scope.user = {};
                    var res = result.data;
                    $scope.user = res;
                    if ($scope.user.phone_number) {
                        var phone = $scope.user.phone_number;
                        phone = phone.split(' ');
                        var patt = new RegExp(" ");
                        if(patt.test($scope.user.phone_number)){
                            setTimeout(function () {
                                $scope.$apply(function() {
                                    if(typeof phone[1] == 'undefined'){
                                        $scope.user.phone_number1 = $scope.user.phone_number;
                                    }else{
                                        $scope.user.phone_number_isd = phone[0];
                                        $scope.user.phone_number1 = phone[1];
                                    }
                                });
                            },100);
                        }
                    }
                });
            })
        }
        $scope.submitForm = function () {
            /*if ($scope.userForm.$valid) {*/
                $scope.user.company_id = $rootScope.id_company;

                var phone = '';
                if($scope.user.phone_number1){
                    /*$scope.user.phone_number = $.trim($scope.user.phone_number_isd)+' '+$.trim($scope.user.phone_number1);*/
                    phone = $.trim($scope.user.phone_number_isd)+' '+$.trim($scope.user.phone_number1);
                }
                $scope.user = {
                    "id_user": $rootScope.userId,
                    "phone_number": phone,
                    "last_name": $scope.user.last_name,
                    "first_name": $scope.user.first_name,
                    "company_id": $scope.user.company_id,
                    "address": $scope.user.address,
                    "city": $scope.user.city,
                    "state": $scope.user.state,
                    "country_id": $scope.user.country_id,
                    "zip_code": $scope.user.zip_code,
                    "user_status": $scope.user.user_status
                };
                Upload.upload({
                    url: API_URL + 'User/userInfo',
                    data: {
                        file: {'profile_image': $scope.userImage},
                        'user': $scope.user
                    }
                }).then(function (resp) {
                    if (resp.data.status) {
                        $rootScope.toast('Success', resp.data.message);
                        $state.go("app.my-profile");
                    } else {
                        $rootScope.toast('Error', resp.data.error, 'error', $scope.user);
                    }
                }, function (resp) {
                    $rootScope.toast('Error', resp.error);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                });
            /*}*/
        }
        $scope.navigatePrevious = function (state) {
            if (state != undefined)
                $state.go(state);
            else
                $state.go('app.my-profile');
        };
    })
    .controller('ChangeSecurityCtrl', function ($rootScope, $scope, $uibModal, $q, $state, UserService, masterService) {
        var promise2 = UserService.getUserProfile({'id': $rootScope.userId}).then(function (result) {
            $scope.field = result.data;
        });
        $q.all([promise2]).then(function (data) {
            $rootScope.loaderOverlay = false;
            masterService.masterList({'master_key': 'security_questions'}).then(function (result) {
                if (result.status)
                    $scope.security_questions = result.data;
            })
        })

        $scope.submitForm = function ($inputData) {
            UserService.updateSecurityQuestion($scope.field).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        };
    })
    .controller('LoginHistoryCtrl', function ($rootScope, $scope, $uibModal, $q, $state, UserService) {
        $scope.displayed = [];
        $scope.callServer = function callServer(tableState) {
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.
            tableState.user_id = $rootScope.userId;
            UserService.getLoginHistory(tableState).then(function (result) {
                $scope.displayed = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };
    })
    .controller('ContactGroupCtrl', function ($rootScope, $scope) {
        $scope.GROUPS_LIST = [
            {"id": 0, "Name": "House Loan", "Members": "20 members", "Status": null},
            {"id": 1, "Name": "USA", "Members": "45 members", "Status": null},
            {"id": 2, "Name": "Defaulters", "Members": "54 members", "Status": null},
            {"id": 3, "Name": "New Request", "Members": "34 members", "Status": null},
            {"id": 4, "Name": "Late Payees", "Members": "10 members", "Status": null},
            {"id": 5, "Name": "United Kingdom", "Members": "39 members", "Status": null},
            {"id": 6, "Name": "Perfect Payees", "Members": "110 members", "Status": null}
        ]
    })
    .controller('CompaniesListCtrl', function ($rootScope, $scope) {
        $scope.COMPANY_RESULTS_DATA = [
            {"id": 0, "Name": "Foxfire Technologies India Ltd", "Contact": "London", "Status": true},
            {"id": 0, "Name": "Inooga Solutions PVT LTD", "Contact": "India", "Status": true},
            {"id": 0, "Name": "Lance Soft India Pvt Ltd", "Contact": "India", "Status": false},
            {"id": 0, "Name": "Azri Solutions Pvt Ltd", "Contact": "UK", "Status": null},
            {"id": 0, "Name": "Infodat Technologies", "Contact": "US", "Status": null},
            {"id": 0, "Name": "Sipera Systems PVT LTD", "Contact": "Australia", "Status": null},
            {"id": 0, "Name": "Deep Channel Solutions", "Contact": "China", "Status": null},
            {"id": 0, "Name": "Larsen & Toubro Ltd", "Contact": "India", "Status": false},
            {"id": 0, "Name": "Kernex Microsystems India Ltd", "Contact": "London", "Status": true},
            {"id": 0, "Name": "Progress Software Pvt Ltd", "Contact": "Australia", "Status": false}
        ];
    })
    .controller('SectorAdminCtrl', function ($rootScope, $scope, $uibModal, $log) {
        $scope.animationsEnabled = true;
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'partials/super-admin/create-sector.html',
                controller: 'CreateSectorCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
    })
    .controller('CreateAdminSetupCtrl', function ($scope, $uibModalInstance, items) {
        $scope.ok = function () {
            $uibModalInstance.close($scope.selected.item);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('AdminSetupCtrl', ['$scope', '$rootScope', '$uibModal', '$log', 'companyService', function ($scope, $rootScope, $uibModal, $log, companyService) {
        $scope.animationsEnabled = true;
        companyService.getById($rootScope.id_company).then(function ($result) {
            if ($result.status) {
                $scope.companyDetail = $result.data;
            }
        });
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'partials/admin/create-admin-setup.html',
                controller: 'CreateAdminSetupCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
    }])
    .controller('UserInformationCtrl', function ($rootScope, $scope, companyService, masterService, Upload, $state, $location, encodeFilter) {
        $scope.compnayUsers = [];
        $rootScope.currentUrl = 'org.user-information';
        companyService.userList($rootScope.id_company).then(function (result) {
            $scope.companyUsers = result.data.data;
        });
        $scope.userView = function (result) {
            $scope.user = result;
        };
        $scope.editCompanyUser = function (id) {
            $state.go('org.edit-user', {'id': encodeFilter(id)});
        };
    })
    .controller('UserCreationCtrl', function ($rootScope, $scope, companyService, masterService, Upload, $state, $location) {
        $scope.title = 'Create New User';
        $scope.btn = 'Create';
        $scope.editPassword = false;
        companyService.companyBranchType($rootScope.id_company).then(function (result) {
            $scope.companyBranchType = result.data;
        });
        masterService.country.get().then(function (result) {
            $scope.countries = result.data;
        });
        $scope.updateBranchType = function (branch_type_id) {
            $scope.user.branch_id = '';
            $scope.user.user_role_id = '';
            companyService.companyApprovalRoles({
                'company_id': $rootScope.id_company,
                'branch_type_id': branch_type_id
            }).then(function (result) {
                $scope.userRole = result.data;
            });
            companyService.companyBranchesList({
                'company_id': $rootScope.id_company,
                'branch_type_id': branch_type_id
            }).then(function (result) {
                $scope.company_branches = result.data.data;
            });
        }
        $scope.updateRole = function (user_role_id, branch_id) {
            if (user_role_id  && branch_id ) {
                var params = {
                    company_id: $rootScope.id_company,
                    id_company_approval_role: user_role_id,
                    branch_id: branch_id
                };
                companyService.branchReportingToUsers(params).then(function (result) {
                    $scope.companyUsers = result.data.data;
                });
            }
        }
        $scope.updateReportingTo = function (user_id) {
            companyService.user(user_id).then(function (result) {
                $scope.reportingUserDetails = result.data;
            });
        }
        $scope.uploadUserImage = function (file) {
            if (file != null && file != '') {
                $scope.userLogoRemove();
                setTimeout(function () {
                    $scope.userImage = file;
                    $scope.trash = true;
                    $scope.$apply();
                }, 100)
            }
        };
        $scope.userLogoRemove = function () {
            $scope.userImage = '';
            $scope.trash = false;
        };
        /*$scope.user = {
            'first_name': '',
            'last_name': '',
            'phone_number': '',
            'email_id': '',
            'address': '',
            'user_role_id': '',
            'branch_id': '',
            'reporting_user_id': ''
        };*/
        $scope.selectedRole = {};
        if ($rootScope.roleId != undefined) {
            $scope.selectedRole.id_approval_role = $rootScope.roleId;
        }
        $scope.submitForm = function () {
            /*if ($scope.userForm.$valid) {*/
                $scope.user.company_id = $rootScope.id_company;
                delete $scope.user['user_status'];
                if($scope.user.phone_number1){
                    $scope.user.phone_number = $.trim($scope.user.phone_number_isd)+' '+$.trim($scope.user.phone_number1);
                }
                Upload.upload({
                    url: API_URL + 'Company/user',
                    data: {
                        file: {'profile_image': $scope.userImage},
                        'user': $scope.user
                    }
                }).then(function (resp) {
                    if (resp.data.status) {
                        $rootScope.toast('Success', resp.data.message);
                        $state.go("org.org-structure-view");
                    } else {
                        $rootScope.toast('Error', resp.data.error, 'error', $scope.user);
                    }
                }, function (resp) {
                    $rootScope.toast('Error', '', 'error', resp.error);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                });
            /*}*/
        }
        $scope.navigatePrevious = function (state) {
            if (state != undefined)
                $state.go(state);
            else
                $state.go('org.user-information');
        };
    })
    .controller('EditCompanyUserCtrl', function (decodeFilter, $rootScope, $scope, companyService, Upload, $state, $location, $q, masterService, $stateParams, UserService) {
        $scope.compnayUsers = [];
        $scope.uploadUserImage = function (file) {
            if (file != null && file != '') {
                $scope.userLogoRemove();
                setTimeout(function () {
                    $scope.userImage = file;
                    $scope.trash = true;
                    $scope.$apply();
                }, 100)
            }
        };
        $scope.userLogoRemove = function () {
            $scope.userImage = '';
            $scope.trash = false;
        };
        $scope.title = 'Edit User';
        $scope.btn = 'Update';
        $rootScope.company_user_id = decodeFilter($stateParams.id);
        if ($rootScope.company_user_id) {
            var promise1 = companyService.companyBranchType($rootScope.id_company).then(function (result) {
                $scope.companyBranchType = result.data;
            });
            /*  var promise2 = companyService.userList($rootScope.id_company,{'current_user_id':$rootScope.company_user_id}).then(function(result){
             $scope.companyUsers = result.data.data;
             });*/
            var promise2 = masterService.country.get().then(function (result) {
                $scope.countries = result.data;
            });
            $q.all([promise1, promise2]).then(function (data) {
                $rootScope.loaderOverlay = false;
                companyService.user($rootScope.company_user_id).then(function (result) {
                    var res = result.data;
                    $scope.workingBranch = res.branch_id;
                    $scope.userRoletype = res.user_role_id;
                    $scope.reportingUserId = res.reporting_user_id;
                    delete res.branch_id;
                    delete res.user_role_id;
                    delete res.reporting_user_id;
                    $scope.user = res;
                    if ($scope.user.phone_number) {
                        var phone = $scope.user.phone_number;
                        phone = phone.split(' ');
                        setTimeout(function () {
                            $scope.$apply(function() {
                                if(typeof phone[1] == 'undefined'){
                                    $scope.user.phone_number1 = $scope.user.phone_number;
                                }else{
                                    $scope.user.phone_number_isd = phone[0];
                                    $scope.user.phone_number1 = phone[1];
                                }
                            });
                        },1000)
                    }
                });
            })
            $scope.updateBranchType = function (branch_type_id, flag) {
                if (!flag) {
                    $scope.user.user_role_id = '';
                    $scope.user.branch_id = '';
                    $scope.user.reporting_user_id = '';
                }
                $rootScope.loaderOverlay = false;
                companyService.companyApprovalRoles({
                    'company_id': $rootScope.id_company,
                    'branch_type_id': branch_type_id
                }).then(function (result) {
                    $scope.userRole = result.data;
                    if (flag) {
                        setTimeout(function () {
                            $scope.user.user_role_id = $scope.userRoletype;
                            $scope.$apply();
                        }, 100);
                    }
                });
                $rootScope.loaderOverlay = false;
                companyService.companyBranchesList({
                    'company_id': $rootScope.id_company,
                    'branch_type_id': branch_type_id
                }).then(function (result) {
                    $scope.company_branches = result.data.data;
                    if (flag) {
                        setTimeout(function () {
                            $scope.user.branch_id = $scope.workingBranch;
                            $scope.$apply();
                        }, 100);
                    }
                });
            }
            $scope.updateRole = function (user_role_id, branch_id, flag) {
                $rootScope.loaderOverlay = false;
                if (user_role_id != undefined && branch_id != undefined) {
                    var params = {
                        company_id: $rootScope.id_company,
                        id_company_approval_role: user_role_id,
                        branch_id: branch_id,
                        current_user_id: $rootScope.company_user_id
                    };
                    companyService.branchReportingToUsers(params).then(function (result) {
                        $scope.companyUsers = result.data.data;
                        if (flag) {
                            setTimeout(function () {
                                $scope.user.reporting_user_id = $scope.reportingUserId;
                                $scope.$apply();
                            }, 100);
                        }
                    });
                }
            }
            $scope.updateReportingTo = function (user_id) {
                $rootScope.loaderOverlay = false;
                if (user_id > 0) {
                    companyService.user(user_id).then(function (result) {
                        $scope.reportingUserDetails = result.data;
                    });
                }
            }
        }
        else {
            $location.path('/user-information');
        }
        $scope.getCheckedValue = function (val) {
            if (val == "1") {
                return true;
            } else {
                return false;
            }
        }
        /*$scope.user = {
            "email_id": '',
            "phone_number1": '',
            "last_name": '',
            "first_name": '',
            "company_id": '',
            "user_status": ''
        };*/
        $scope.submitForm = function () {
            if ($scope.userForm.$valid) {
                $scope.user.company_id = $rootScope.id_company;
                if($scope.user.phone_number1){
                    $scope.user.phone_number = $.trim($scope.user.phone_number_isd)+' '+$.trim($scope.user.phone_number1);
                }
                /*console.log($scope.password);*/
                /*if($scope.editPassword){
                 $scope.user.password = $scope.password;
                 }else{
                 delete $scope.user.password;
                 }*/
                Upload.upload({
                    url: API_URL + 'Company/user',
                    data: {
                        file: {'profile_image': $scope.userImage},
                        'user': $scope.user
                    }
                }).then(function (resp) {
                    if (resp.data.status) {
                        if ($scope.editPassword) {
                            $scope.user.password = $scope.password = '';
                        }
                        $rootScope.toast('Success', resp.data.message);
                        $state.go("org.org-structure-view");
                    } else {
                        $rootScope.toast('Error', resp.data.error, 'error', $scope.user);
                    }
                }, function (resp) {
                    $rootScope.toast('Error', resp.error);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                });
            }
        }
        $scope.submitPasswordForm = function () {
            $scope.passwordData = {};
            $scope.passwordData.company_id = $rootScope.id_company;
            $scope.passwordData.id_user = $rootScope.company_user_id;
            $scope.passwordData.password = $scope.password;
            UserService.resetPassword($scope.passwordData).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                    $scope.password = '';
                    $scope.repassword = '';
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            });
        };
        $scope.navigatePrevious = function (state) {
            if (state != undefined)
                $state.go(state);
            else
                $state.go('org.user-information');
        };
    })
    .controller('BranchStructureViewCtrl', function ($rootScope, $scope, companyService, $state, $uibModal, encodeFilter, $timeout) {
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.brachTreeShow = true;
        $scope.open = function ($mode, $editId) {
            var $item = {};
            if ($mode == 'edit') {
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'partials/admin/upload-branch.html',
                controller: 'UploadBranchCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    items: function () {
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.branchStructure();
            }, function () {
            });
        };
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        $scope.branchTypes = [];
        $scope.branchTree = [];
        $scope.branchTreeStructure = [];
        $scope.treeDepth = [];
        $scope.setColor = [];
        $scope.branchTreeStructureShow = true;
        $scope.branchStructure = function () {
            companyService.branchStructure($rootScope.id_company).then(function (result) {
                $scope.branchTypes = result.data;
                angular.forEach($scope.branchTypes, function (value, key) {
                    angular.forEach(value.branches, function (branch) {
                        branch.branch_type_name = value.branch_type_name;
                        branch.level = key;
                        $scope.branchTree.push(branch);
                    });
                });
                if ($scope.branchTree.length == 0) {
                    $scope.createNewBtn = true;
                } else {
                    $scope.createNewBtn = false;
                }
                $scope.branchTreeStructure = $scope.getNestedChildren($scope.branchTree, 0);
                if (result.data.length == 0) {
                    $state.go("entity.branch-structure");
                }
            });
        };
        $scope.branchChange = function (data) {
            $scope.brachTreeShow = false;
            $scope.branchTreeStructure = [];
            if (data == null || data == undefined) {
                data = 0;
            }
            if (data != 0) {
                var branches = $scope.branchTree;
                var currentBranch = [];
                for (var b in branches) {
                    if (branches[b].id_branch == data) {
                        currentBranch.push(branches[b]);
                        $scope.branchTreeStructure = currentBranch;
                        break;
                    }
                }
                $timeout(function () {
                    $scope.brachTreeShow = true;
                }, 1000)
            }
            else {
                $timeout(function () {
                    $scope.brachTreeShow = true;
                }, 1000)
                $scope.branchTreeStructure = $scope.getNestedChildren($scope.branchTree, data);
            }
        }
        $scope.treeStructureCount = function (treeB, count) {
            return Bcount;
        }
        $scope.getNestedChildren = function (arr, parent) {
            var out = []
            for (var i in arr) {
                if (arr[i].reporting_branch_id == parent) {
                    var nodes = $scope.getNestedChildren(arr, arr[i].id_branch);
                    if (nodes.length) {
                        arr[i].nodes = nodes;
                    }
                    out.push(arr[i])
                }
            }
            return out
        }
        $scope.branchStructure();
        $scope.createBranch = function (branchId, currentBranchId) {
            $rootScope.branchId = branchId;
            $rootScope.currentBranchId = currentBranchId;
            $state.go("entity.create-branch");
        };
        $scope.editBranch = function (branchId) {
            $rootScope.branchId = branchId;
            $state.go("entity.edit-branch", {id: encodeFilter($rootScope.branchId)});
        };
        $scope.branchViewModal = function (branch_id) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'partials/admin/entity-structure/branch-view-modal.html',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    item: function () {
                        return companyService.branchView({
                            'company_id': $rootScope.id_company,
                            'branch_id': branch_id
                        }).then(function (result) {
                            return result.data[0];
                        })
                    }
                },
                controller: function (item, $scope, $uibModalInstance) {
                    $scope.branch = item;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
    })
    .controller('UploadBranchCtrl', function ($rootScope, $scope, $uibModalInstance, companyService, Upload) {
        companyService.download().then(function (result) {
            if (result.status) {
                $scope.download_url = result.data;
                /*$rootScope.toast('Success', result.error);
                 $uibModalInstance.close();*/
            } else {
                /*$rootScope.toast('Error', result.error, 'error');*/
            }
        });
        $scope.uploadBranch = function (file) {
            if (file != null && file != '') {
                $scope.branchExcel = file;
            } else {
                $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
            }
        };
        $scope.submitForm = function () {
            Upload.upload({
                url: API_URL + 'Company/uploadBranch',
                data: {
                    excel: $scope.branchExcel,
                    company_id: $rootScope.id_company
                }
            }).then(function (result) {
                if (result.data.status) {
                    $rootScope.toast('Success', result.data.error);
                    $uibModalInstance.close();
                } else {
                    $rootScope.toast('Error', result.data.error, 'l-error', $scope.branchExcel);
                }
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('orgStructureViewCtrl', function ($rootScope, $scope, companyService, $state, $uibModal, encodeFilter, $timeout) {
        //add new row in modal
        $scope.activeParentIndex;
        $scope.animationsEnabled = true;
        $scope.roleTypes = [];
        $scope.orgRoleTree = [];
        $scope.setColor = [];
        $scope.orgTreeShow = true;
        $scope.userList = [];
        $rootScope.currentUrl = 'org.org-structure-view';
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        $scope.rolesStructure = function () {
            companyService.roleStructure($rootScope.id_company).then(function (result) {
                if (result.data.length == 0) {
                    $state.go("org.org-structure");
                }
                $scope.roleTypes = result.data;
                angular.forEach($scope.roleTypes, function (value, key) {
                    angular.forEach(value.users, function (user) {
                        user.approval_name = value.approval_name;
                        user.id_approval_role = value.id_approval_role;
                        //branch.level = key;
                        $scope.orgRoleTree.push(user);
                    });
                });
                if ($scope.orgRoleTree.length == 0) {
                    $scope.createNewBtn = true;
                }
                $scope.orgRoleTreeStructure = $scope.getNestedChildren($scope.orgRoleTree, 0);
            });
        };
        //Old service
        /*$scope.getUserList = function () {
         companyService.userList({'company_id': $rootScope.id_company, 'type': 'admin'}).then(function (result) {
         if (result.status) {
         $scope.userList = result.data.data;
         angular.forEach($scope.userList, function (user) {
         user.user_id = encodeFilter(user.id_user);
         });
         }
         });
         };*/
        $scope.getUserList = function () {
            companyService.adminUserList({
                'company_id': $rootScope.id_company,
                'type': 'admin'
            }).then(function (result) {
                if (result.status) {
                    $scope.userList = result.data.data;
                    angular.forEach($scope.userList, function (user) {
                        user.user_id = encodeFilter(user.id_user);
                    });
                }
            });
        };
        $scope.getNestedChildren = function (arr, parent) {
            var out = []
            for (var i in arr) {
                if (arr[i].reporting_user_id == parent) {
                    var nodes = $scope.getNestedChildren(arr, arr[i].user_id);
                    if (nodes.length) {
                        arr[i].nodes = nodes;
                    }
                    out.push(arr[i])
                }
            }
            return out
        }
        $scope.roleChange = function (data) {
            $scope.orgTreeShow = false;
            $scope.orgRoleTreeStructure = [];
            if (data == null || data == undefined) {
                data = 0;
            }
            if (data != 0) {
                var roles = $scope.orgRoleTree;
                var currentRole = [];
                for (var b in roles) {
                    if (roles[b].user_id == data) {
                        currentRole.push(roles[b]);
                        $scope.orgRoleTreeStructure = currentRole;
                        break;
                    }
                }
                $timeout(function () {
                    $scope.orgTreeShow = true;
                }, 1000)
            }
            else {
                $timeout(function () {
                    $scope.orgTreeShow = true;
                }, 1000)
                $scope.orgRoleTreeStructure = $scope.getNestedChildren($scope.orgRoleTree, data);
            }
        }
        $scope.createUser = function (roleId) {
            $rootScope.roleId = roleId;
            $state.go("org.create-user");
        };
        $scope.editCompanyUser = function (id) {
            $rootScope.company_user_id = id;
            $state.go('org.edit-user', {id: encodeFilter($rootScope.company_user_id)});
        };
        $scope.userViewModal = function (user_id) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'partials/admin/org-user/user-information-modal.html',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    item: function () {
                        return companyService.userView({
                            'company_id': $rootScope.id_company,
                            'user_id': user_id,
                            'user_type': 'view'
                        }).then(function (result) {
                            return result.data.data[0];
                        })
                    }
                },
                controller: function (item, $scope, $uibModalInstance) {
                    $scope.user = item;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
        //initial call
        $scope.rolesStructure();
        $scope.getUserList();
        $scope.searchUserResult = function (form, s) {
            if (typeof s.logdate)
                var last_login = s.logdate.toString();
            companyService.userList({
                'log_date': last_login,
                'company_id': $rootScope.id_company
            }).then(function (result) {
                if (result.status) {
                    $scope.userList = result.data.data;
                    form.$setPristine();
                    form.$setUntouched();
                }
            });
        }
    })
    .controller('orgStructureHistoryCtrl', function ($rootScope, $scope, $stateParams, UserService, $state, $uibModal, encodeFilter, $timeout, decodeFilter) {
        $scope.searchUserHistory = function (searchText) {
            $scope.isLoading = true;
            $scope.userList = [];
            if (searchText == 'login') {
                UserService.getLoginHistory({'user_id': decodeFilter($stateParams.id)}).then(function (result) {
                    if (result.status) {
                        $scope.userList = result.data.data;
                        /*$scope.userDetail = result.data.user_details;*/
                        $scope.isLoading = false;
                    }
                });
            } else {
                UserService.getUserLogs({
                    'type': searchText,
                    'user_id': decodeFilter($stateParams.id)
                }).then(function (result) {
                    if (result.status) {
                        $scope.userList = result.data.data;
                        $scope.userDetail = result.data.user_details;
                        $scope.userDetail.id_user = encodeFilter($scope.userDetail.id_user);
                        console.log($scope.userDetail);
                        $scope.isLoading = false;
                    }
                });
            }
        };
        $scope.searchText = 'password_change';
        $scope.searchUserHistory($scope.searchText);
    })
    .controller('UploadApprovalCtrl', function ($rootScope, $scope, $uibModalInstance, Upload, companyService) {
        $scope.uploadUsers = function (file) {
            if (file != null && file != '') {
                $scope.userExcel = file;
            } else {
                $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
            }
        };
        companyService.downloadUsers().then(function (result) {
            if (result.status) {
                $scope.download_users = result.data;
            } else {
            }
        });
        $scope.submitUsersForm = function () {
            Upload.upload({
                url: API_URL + 'Company/uploadUser',
                data: {
                    excel: $scope.userExcel,
                    id_company: $rootScope.id_company
                }
            }).then(function (result) {
                if (result.data.status) {
                    $rootScope.toast('Success', result.data.error);
                    $uibModalInstance.close();
                } else {
                    $rootScope.toast('Error', result.data.error, 'l-error', $scope.userExcel);
                }
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('createBranchCtrl', function ($rootScope, $scope, $state, Upload, masterService, companyService) {
        if ($rootScope.branchId == undefined) {
            $state.go("entity.branch-structure-view");
        }
        $scope.Countries = [];
        //$scope.title = 'Create New Branch';
        $scope.title = 'Create New ';
        $scope.btn = 'Create';
        masterService.country.get().then(function ($result) {
            $scope.Countries = $result.data;
        });
        $scope.getReportingBranch = function (branchTypeId) {
            companyService.companyBranches({
                'company_id': $rootScope.id_company,
                'branch_type_id': branchTypeId
            }).then(function (result) {
                $scope.company_branches = result.data.data;
                $scope.branchTypeName = result.data.branch_type.branch_type_name;
                $scope.branch.reporting_branch_id = $rootScope.currentBranchId;
            });
        };
        $scope.branch = {};
        companyService.companyBranchType($rootScope.id_company, $rootScope.branchId).then(function (result) {
            $scope.branchTypes = result.data;
            $scope.branch.branch_type_id = $rootScope.branchId;
        });
        $scope.uploadBranchImage = function (file) {
            if (file != null && file != '') {
                $scope.branch.branch_logo = file;
                $scope.trash = true;
            }
        };
        $scope.brachLogoRemove = function () {
            $scope.branch.branch_logo = '';
            $scope.trash = false;
        };
        $scope.branch = {
            'legal_name': '',
            'country_id': '',
            'branch_state': '',
            'branch_city': '',
            'branch_address': '',
            'reporting_branch_id': '',
            'branch_code': '',
            'branch_zip_code': '',
            'branch_email': '',
            'branch_phone_number': ''
        };
        $scope.branch.branch_type_id = $rootScope.branchId;
        $scope.branch.company_id = $rootScope.id_company;
        $scope.submitForm = function () {
            delete $scope.branch.id_branch_type;
            delete $scope.branch.branch_type_name;
            delete $scope.branch.branch_type_code;
            delete $scope.branch.description;
            delete $scope.branch.created_date_time;
            Upload.upload({
                url: API_URL + 'Company/companyBranch',
                data: {
                    file: {'branch_logo': $scope.branch.branch_logo},
                    'branch': $scope.branch
                }
            }).then(function (result) {
                if (result.data.status) {
                    $rootScope.toast('Success', result.data.message);
                    $state.go("entity.branch-structure-view");
                } else {
                    $rootScope.toast('Error', result.data.error, 'error', $scope.branch);
                }
            });
        }
    })
    .controller('editBranchCtrl', function ($rootScope, $scope, $state, Upload, masterService, companyService, $q, $stateParams, decodeFilter) {
        $rootScope.branchId = decodeFilter($stateParams.id);
        if ($rootScope.branchId == undefined) {
            $state.go("entity.branch-structure-view");
        }
        $scope.Countries = [];
        //$scope.title = 'Edit Branch';
        $scope.title = 'Edit ';
        $scope.btn = 'Update';
        $scope.branch = {};
        var getReportingBranch = $scope.getReportingBranch = function (branchTypeId, flag) {
            if (flag) {
                $scope.branch.reporting_branch_id = '';
            }
            companyService.companyBranches({
                'company_id': $rootScope.id_company,
                'branch_type_id': branchTypeId,
                'branch_id': $rootScope.branchId
            }).then(function (result) {
                $scope.company_branches = result.data.data;
                //$scope.branch = result.data.branch_type;
                $scope.branchTypeName = result.data.branch_type.branch_type_name;
            });
        };
        var countries = masterService.country.get().then(function ($result) {
            $scope.Countries = $result.data;
        });
        var branch = $q.all([getReportingBranch, countries]).then(function () {
            companyService.branch($rootScope.branchId).then(function (result) {
                $scope.branch = result.data;
                $scope.branch.branch_type_id = result.data.branch_type_id;
                $rootScope.branch_type_id = result.data.branch_type_id;
            });
        })
        $q.all([branch]).then(function () {
            companyService.companyBranchType($rootScope.id_company, $rootScope.branch_type_id).then(function (result) {
                $scope.branchTypes = result.data;
            });
        })
        $scope.uploadBranchImage = function (file) {
            if (file != null && file != '') {
                $scope.branch.branch_logo = file;
                $scope.trash = true;
            }
        };
        $scope.brachLogoRemove = function () {
            $scope.branch.branch_logo = '';
            $scope.trash = false;
        };
        $scope.branch = {
            'legal_name': '',
            'country_id': '',
            'branch_state': '',
            'branch_city': '',
            'branch_address': '',
            'reporting_branch_id': '',
            'branch_code': '',
            'branch_zip_code': '',
            'branch_email': '',
            'branch_phone_number': ''
        };
        $scope.submitForm = function () {
            Upload.upload({
                url: API_URL + 'Company/companyBranch',
                data: {
                    file: {'branch_logo': $scope.branch.branch_logo},
                    'branch': $scope.branch
                }
            }).then(function (result) {
                if (result.data.status) {
                    $rootScope.toast('Success', result.data.message);
                    $state.go("entity.branch-structure-view");
                } else {
                    $rootScope.toast('Error', result.data.error, 'error', $scope.branch);
                }
            });
        }
    })
    .controller('CreateProjectCtrl', function ($rootScope, $scope) {
        $scope.demo4 = {
            rangeMin: 10,
            rangeMax: 1500,
            min: 80,
            max: 1000,
            disabled: false
        };
    })
    .controller('BranchStructureCtrl', function ($rootScope, $scope, masterService, companyService, $state, $uibModal) {
        $scope.branchTree = [];
        $rootScope.branchList = [];
        $rootScope.branchListAll = [];
        $scope.addNewBtn = false;
        $scope.setColor = [];
        $scope.createBtnShow = false;
        $rootScope.getBranchTypeStructure = function () {
            $scope.setColor = [];
            companyService.getBranchTypeStructure($rootScope.id_company).then(function ($result) {
                if ($result.status) {
                    $rootScope.branchData = $result.data;
                    if ($rootScope.branchData.length > 0) {
                        $scope.createBtnShow = false;
                        $scope.branchTree = $scope.getNestedChildren($result.data, 0);
                    } else {
                        $scope.createBtnShow = true;
                    }
                }
            });
        }
        $scope.editBranchType = function (data) {
            $scope.branchTypeModal(data);
        }
        $scope.createBranchType = function () {
            $scope.branchTypeModal();
        }
        $rootScope.branchListUpdate = function () {
            angular.forEach($rootScope.branchData, function (value, key) {
                var a = _.findWhere($rootScope.branchList, {'id_branch_type': value.id_branch_type});//searching Element in Array
                var b = _.indexOf($rootScope.branchList, a);// getting index.
                if (b != -1) {
                    $rootScope.branchList.splice(b, 1);
                }// removing.
            })
        }
        $scope.getNestedChildren = function (arr, parent) {
            var out = []
            for (var i in arr) {
                if (arr[i].reporting_branch_type_id == parent) {
                    var nodes = $scope.getNestedChildren(arr, arr[i].id_branch_type)
                    if (nodes.length) {
                        arr[i].nodes = nodes
                    }
                    out.push(arr[i])
                }
            }
            return out
        }
        $rootScope.getBranchTypeStructure();
        //branchTypeModal
        $scope.branchTypeModal = function (item) {
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'branch_structure_modal.html',
                resolve: {
                    item: item
                },
                controller: function ($scope, $uibModalInstance, masterService, item, companyService) {
                    $scope.branch = {};
                    $scope.submitBtn = false;
                    $scope.branch.is_primary = false;
                    companyService.branchType.get({'company_id': $rootScope.id_company}).then(function ($result) {
                        $rootScope.branchList = $result.data;
                        angular.copy($rootScope.branchList, $rootScope.branchListAll);
                        $scope.editMode = false;
                        if (item != undefined) {
                            // $scope.branch = item;
                            $scope.branch.id_branch_type = item.branch_type_id;
                            $scope.branch.reporting_branch_type_id = item.reporting_branch_type_id;
                            $scope.branch.branch_type_name = item.branch_type_name;
                            $scope.branch.branch_type_code = item.branch_type_code;
                            $scope.branch.description = item.description;
                            $scope.branch.is_edit = item.is_edit;
                            if (item.is_edit == 1) {
                                $scope.editMode = true;
                            }
                            if (item.reporting_branch_type_id == 0) {
                                $scope.branch.is_primary = true;
                            }
                        } else {
                            $rootScope.branchListUpdate();
                        }
                    })
                    $scope.submitForm = function (branch) {
                        $scope.submitBtn = true;
                        branch.company_id = $rootScope.id_company;
                        companyService.branchType.post(branch).then(function (result) {
                            $scope.submitBtn = false;
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $rootScope.getBranchTypeStructure();
                                $scope.cancel();
                            } else {
                                $rootScope.toast('Error', result.error, 'error');
                            }
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
            }, function () {
            });
        };
    })
    .controller('orgStructureCtrl', function ($rootScope, $scope, masterService, companyService, $state, $uibModal) {
        $rootScope.roleList = [];
        $rootScope.roleListAll = [];
        $scope.tableList = [];
        $scope.approvalTree = [];
        $scope.addNewBtn = false;
        $scope.setColor = [];
        $rootScope.getApprovalStructure = function () {
            $scope.setColor = [];
            companyService.getApprovalTypeStructure($rootScope.id_company).then(function ($result) {
                if ($result.status) {
                    angular.copy($result.data, $scope.tableList);
                    $rootScope.approvalList = $result.data;
                    if ($rootScope.approvalList.length > 0) {
                        $scope.approvalTree = $scope.getNestedChildren($rootScope.approvalList, null);
                    } else {
                        $scope.addNewBtn = true;
                    }
                }
            })
        }
        $scope.getNestedChildren = function (arr, parent) {
            var out = []
            for (var i in arr) {
                if (arr[i].reporting_id == parent) {
                    var nodes = $scope.getNestedChildren(arr, arr[i].id_approval_role)
                    if (nodes.length) {
                        arr[i].nodes = nodes
                    }
                    out.push(arr[i])
                }
            }
            return out
        }
        $rootScope.getApprovalStructure();
        $rootScope.roleListUpdate = function () {
            angular.forEach($rootScope.approvalList, function (data) {
                var a = _.findWhere($rootScope.roleList, {'id_approval_role': data.id_approval_role});//searching Element in Array
                var b = _.indexOf($rootScope.roleList, a);// getting index.
                if (b != -1) {
                    $rootScope.roleList.splice(b, 1);// removing.
                }
            })
        }
        $scope.modalOpen = function (sData) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                //windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'approvalStructureForm.html',
                resolve: {
                    item: function () {
                        return sData;
                    }
                },
                controller: function ($rootScope, $scope, $uibModalInstance, companyService, item, $q, masterService) {
                    $scope.sData = {};
                    $scope.sData.is_primary = false;
                    $scope.editMode = false;
                    $scope.approvalRole = masterService.approvalRole.getPage().then(function ($result) {
                        $rootScope.roleList = $result.data.data;
                        angular.copy($rootScope.roleList, $rootScope.roleListAll);
                    });
                    $scope.branchType = companyService.getBranchTypeStructure($rootScope.id_company).then(function ($result) {
                        $rootScope.branchList = $result.data;
                    });
                    $q.all([$scope.approvalRole, $scope.branchType]).then(function () {
                        if (item != undefined) {
                            $scope.sData.id_approval_role = item.id_approval_role;
                            $scope.sData.id_branch_type = item.id_branch_type;
                            $scope.sData.id_company_approval_role = item.id_company_approval_role;
                            $scope.sData.reporting_approval_name = item.reporting_approval_name;
                            $scope.sData.approval_name = item.approval_name;
                            $scope.sData.description = item.description;
                            $scope.sData.reporting_id = item.reporting_id;
                            $scope.sData.all_projects = (item.all_projects == 1) ? 1 : 0;
                            $scope.sData.is_edit = item.is_edit;
                            if (item.is_edit == 1) {
                                $scope.editMode = true;
                            }
                            if (item.reporting_id == 0 || item.reporting_id == null) {
                                $scope.sData.is_primary = true;
                            }
                        } else {
                            $rootScope.roleListUpdate();
                            if ($scope.approvalList.length > 0 && $scope.approvalList[$scope.approvalList.length - 1].id_approval_role != undefined) {
                                $scope.sData.reporting_approval_name = $scope.approvalList[$scope.approvalList.length - 1].approval_name;
                            }
                        }
                    });
                    $scope.addApproval = function ($data) {
                        var postData = {};
                        postData.id_approval_role = $data.id_approval_role;
                        postData.branch_type_id = $data.id_branch_type;
                        // postData.is_primary=$data.is_primary;
                        postData.all_projects = $data.all_projects;
                        postData.company_id = $rootScope.id_company;
                        postData.approval_name = $data.approval_name;
                        postData.description = $data.description;
                        postData.reporting_role_id = $data.reporting_id;
                        companyService.approvalTypeStructure(postData).then(function ($result) {
                            $rootScope.getApprovalStructure();
                            if ($result.status) {
                                $rootScope.toast('Success', $result.message);
                                $uibModalInstance.dismiss('cancel');
                            } else {
                                $rootScope.toast('Error', $result.error, 'error');
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (selectedItem) {
                //$scope.selected = selectedItem;
            }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.companyId = $rootScope.id_company;
    })
    .controller('AdminManageDashboardCtrl', function ($rootScope, $scope) {
    })
    .controller('productCtrl',function($stateParams,$rootScope,$scope,masterService,companyService, decodeFilter,$uibModal, productService){
        /*$scope.productList=[];
         companyService.approvalStructure.get({
         'company_id': $rootScope.id_company,
         'company_approval_category_id': 1
         }).then(function(response){
         $scope.approvalList=response.data.data;
         });
         companyService.approvalStructure.get({
         'company_id': $rootScope.id_company,
         'company_approval_category_id': 2
         }).then(function(response){
         $scope.preApprovalList=response.data.data;
         });
         $scope.displayed=false;
         $scope.callServer=function(tableState){
         tableState.company_id=$rootScope.id_company;
         masterService.getProductType(tableState).then(function(result){
         $scope.displayed=true;
         $scope.tableStateRef=tableState;
         $scope.productList=result.data.data;
         $scope.totalCount=result.data.total_records;
         tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/tableState.pagination.number);
         });
         };
         $scope.submitQuickProduct=function($inputData,form){
         if(form.$valid){
         $inputData.company_id=$rootScope.id_company;
         $inputData.created_by=$rootScope.userId;
         masterService.addProductType($inputData).then(function(result){
         if(result.status){
         $rootScope.toast('Success',result.message);
         $scope.product={};
         $scope.submitted=false;
         $scope.callServer($scope.tableStateRef);
         }else{
         $rootScope.toast('Error',result.error,'error',result.error);
         }
         })
         }
         };*/
        $scope.id_product = decodeFilter($stateParams.productId);
        $scope.getProductsList = function(){
            var params = {};
            params.company_id = $rootScope.id_company;
            productService.getProductsList(params).then(function(result){
                $scope.productsList = result.data.data;
            });
        }
        $scope.getProductsList();
        $scope.addProduct = function(row){
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl:'add-product.html',
                controller : function($scope,$uibModalInstance,crmCompanyService,productService){
                    $scope.product = {};
                    $scope.title = 'admin.product.add_product';
                    if(row){
                        $scope.title = 'admin.product.update_product';
                        $scope.product = row;
                        $scope.product.product_name = row.product_name;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.close();
                    };
                    $scope.saveProduct = function(data){
                        var params ={};
                        params.company_id = $rootScope.id_company;
                        params.product_name = data.product_name;
                        if(data.id_product)params.id_product = data.id_product;
                        params.created_by = $rootScope.userId;
                        productService.postProduct(params).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $scope.getProductsList();
                                $scope.cancel();
                            }else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        });
                    }
                },
                resolve:{}
            });
            modalInstance.result.then(function(){
            },function(){
            });
        }
    })
    .controller('productConditionsCtrl', function (productService, $scope) {
        $scope.getProductStateData = function () {
            productService.getProductData('terms_and_conditions').then(function (result) {
                $scope.productdata = result.data;
            });
        };
        $scope.getProductStateData();
    })
    .controller('productSummaryRecommendationsCtrl', function (productService, $scope) {
        $scope.getProductStateData = function () {
            productService.getProductData('summary_and_recommendations').then(function (result) {
                $scope.productdata = result.data;
            });
        };
        $scope.getProductStateData();
    })
    .controller('productLimitsApprovalsCtrl', function ($rootScope, $scope, masterService, companyService, $q) {
        $scope.productLimitsApprovals = [];
        var service1 = companyService.approvalStructure.get({
            'company_id': $rootScope.id_company,
            'company_approval_category_id': 1
        }).then(function (response) {
            $scope.approvalList = response.data.data;
        });
        var allSectors = [];
        var service2 = masterService.sector.getLimitSectorsAll().then(function (result) {
            $scope.sectorsList = result.data.data;
        });
        $scope.getApprovalLimitType = function () {
            masterService.getApprovalLimitType($rootScope.id_company, $rootScope.product_id).then(function ($result) {
                $scope.productLimitsApprovals = $result.data;
                $scope.product = $result.data.approval_structure;
                $scope.existingSectors = $result.data.sectors;
                masterService.sector.getLimitSectorsAll().then(function (result) {
                    $scope.sectorsList = result.data.data;
                    $scope.removeExistSector($scope.existingSectors, $scope.sectorsList);
                });
            })
        };
        $scope.removeExistSector = function (existingSectors, sectorsList) {
            angular.forEach(existingSectors, function (object, index1) {
                angular.forEach(sectorsList, function (value, index) {
                    if (value.id_sector == object.id_sector) {
                        $scope.sectorsList.splice(index, 1);
                    }
                })
            })
        }
        $q.all([service1, service2]).then(function () {
            $scope.getApprovalLimitType();
        });
        $scope.addSector = function (sector) {
            if (sector == undefined) {
                return false;
            }
            var details = {
                company_id: $rootScope.id_company,
                product_id: $rootScope.product_id,
                created_by: $rootScope.userId,
                'id_company_approval_structure': $scope.product.id_company_approval_structure,
                'sector_id': sector.id_sector
            }
            masterService.addSectorToLimits(details).then(function ($result) {
                $scope.getApprovalLimitType();
            })
            setTimeout(function () {
                var index = $scope.sectorsList.indexOf(sector);
                $scope.sectorsList.splice(index, 1);
                $scope.sectorValue = '';
                $scope.addMore = !$scope.addMore;
                $scope.$apply();
                $scope.updateWidth();
            }, 1000)
        }
        $scope.updateSector = function (allSectorDetails, sectorId) {
            var validate = $scope.validateData(sectorId);
            if (validate) {
                var details = {
                    data: allSectorDetails,
                    details: {
                        id_company: $rootScope.id_company,
                        product_id: $rootScope.product_id,
                        created_by: $rootScope.userId,
                        'updatedSectorId': sectorId
                    }
                }
                masterService.postApprovalLimits(details).then(function ($result) {
                    $rootScope.toast('Success', $result.message);
                    $scope.getApprovalLimitType();
                })
            } else {
                alert('Lower committee approval limit should not greater than higher committee');
            }
        }
        $scope.validateData = function (sectorId) {
            var valid = true;
            var previousValue = '';
            var setFirstVal = 0;
            angular.forEach($scope.productLimitsApprovals.data, function (value, peKey) {
                angular.forEach($scope.productLimitsApprovals.data[peKey].committee, function (value, chKey) {
                    angular.forEach($scope.productLimitsApprovals.data[peKey].committee[chKey].limit, function (value, key) {
                        if ($scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].id_sector == sectorId) {
                            if (setFirstVal == 0) {
                                setFirstVal++;
                                previousValue = $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount;
                            }
                            if (parseInt($scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount) > parseInt(previousValue)) {
                                $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].invalid = true;
                                valid = false;
                            } else {
                                $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].invalid = false;
                            }
                            previousValue = $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount;
                        }
                    });
                });
            });
            return valid;
        }
        $scope.deleteSector = function (sectorId) {
            var r = confirm("Do you want to remove this sector?");
            if (r == true) {
                var details = {
                    company_id: $rootScope.id_company,
                    product_id: $rootScope.product_id,
                    'sector_id': sectorId
                }
                masterService.deleteApprovalLimitSector(details).then(function ($result) {
                    $rootScope.toast('Success', $result.message);
                    $scope.getApprovalLimitType();
                })
            }
        }
        $scope.updateWidth = function () {
            var th_length = $('.table-scroll').find('th').length;
            if (th_length == 1) {
                //$(".table-scroll").css('width',$('.table-scroll').find('th').width()*2);
            }
            else {
                // $(".table-scroll").css('width',th_length*$('.table-scroll').find('th').width());
            }
            $(".table-scroll").css('max-width', $(window).width() - 280);
            $(".table-scroll").mCustomScrollbar("update");
            $(".table-scroll").mCustomScrollbar("scrollTo", "last", {
                scrollInertia: 200,
                scrollEasing: "easeInOutQuad"
            });
        }
        $scope.updateApprovalStructure = function (ApprovalStructureId) {
            masterService.updateApprovalStructure({
                company_id: $rootScope.id_company,
                id_product: $rootScope.product_id,
                'company_approval_structure_id': ApprovalStructureId
            }).then(function ($result) {
                $rootScope.toast('Success', $result.message);
                $scope.getApprovalLimitType();
            })
        }
    })
    .controller('productPreApprovalsCtrl', function ($rootScope, $scope, masterService, companyService, $q) {
        $scope.productLimitsApprovals = [];
        $scope.product = {};
        var service1 = companyService.approvalStructure.get({
            'company_id': $rootScope.id_company,
            'company_approval_category_id': 2,
            'product_id': $rootScope.product_id
        }).then(function (response) {
            $scope.approvalList = response.data.data;
            $scope.product.id_company_approval_structure = response.data.product_approval_structure.id_company_approval_structure;
            //$scope.$apply();
        });
        $scope.updateApprovalStructure = function (ApprovalStructureId) {
            masterService.updateApprovalStructure({
                company_id: $rootScope.id_company,
                id_product: $rootScope.product_id,
                'company_pre_approval_structure_id': ApprovalStructureId
            }).then(function ($result) {
                if ($result.status) {
                    $rootScope.toast('Success', $result.message);
                } else {
                    $rootScope.toast('Error', response.error, 'error');
                }
            })
        }
    })
    .controller('approvalStructureCtrl', function ($rootScope, $scope, $stateParams, $state, $uibModal, companyService) {
        $scope.approvalList = {};
        $scope.approvalTypeId = $state.current.data.approval_type;
        $scope.title = $state.current.data.title;
        $rootScope.getApprovalList = function () {
            companyService.approvalStructure.get({
                'company_id': $rootScope.id_company,
                'company_approval_category_id': $scope.approvalTypeId
            }).then(function (response) {
                $scope.approvalList = response.data.data;
            })
        };
        $scope.modalOpen = function (selectedItem) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                //windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'addApproval.html',
                scope: $scope,
                resolve: {
                    item: selectedItem
                },
                controller: function ($rootScope, $scope, $uibModalInstance, companyService, item) {
                    $scope.approvalData = {};
                    $scope.btnDisabled = false;
                    if (item != undefined) {
                        $scope.approvalData = item;
                    }
                    $scope.saveApproval = function (approvalData) {
                        $scope.btnDisabled = true;
                        var postParams = {};
                        postParams.company_id = $rootScope.id_company;
                        postParams.approval_structure_name = approvalData.approval_structure_name;
                        postParams.company_approval_category_id = $scope.approvalTypeId;
                        if (item != undefined) {
                            postParams.id_company_approval_structure = item.id_company_approval_structure;
                        }
                        companyService.approvalStructure.post(postParams).then(function (response) {
                            if (response.status) {
                                $scope.approvalData = {};
                                $rootScope.toast('Success', response.message, 'Success');
                                $rootScope.getApprovalList();
                                $scope.cancel();
                            } else {
                                $scope.btnDisabled = false;
                                $rootScope.toast('Error', response.error, 'error');
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (selectedItem) {
            }, function () {
            });
        };
    })
    .controller('approvalStructureViewCtrl', function ($rootScope, $scope, $stateParams, $state, $uibModal, companyService, decodeFilter) {
        $scope.setColor = [];
        $rootScope.current_structure_id = decodeFilter($stateParams.structure_id);
        $rootScope.getApprovalCommitees = function () {
            companyService.approvalCommittees.get({
                company_id: $rootScope.id_company,
                id_company_approval_structure: $rootScope.current_structure_id
            }).then(function (res) {
                $scope.approvalCommittees = res.data.data;
                $rootScope.approval_structure_name = res.data.approval_structure_name;
            });
        }
        $scope.getApprovalCommitees();
        //Add Edit modal
        $scope.approvalStructureModal = function (branch_type_id, id_company_approval_credit_committee) {
            var item = {};
            item.branch_type_id = branch_type_id;
            item.id_company_approval_credit_committee = id_company_approval_credit_committee;
            var modalInstance2 = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/approval/approval-structure-modal.html',
                controller: 'approvalStructureViewModalCtrl',
                resolve: {
                    item: item
                }
            });
            modalInstance2.result.then(function () {
                $scope.getApprovalCommitees();
            }, function () {
            });
        };
    })
    .controller('approvalStructureViewModalCtrl', function ($rootScope, $scope, $uibModalInstance, companyService, item, $q) {
        //scope
        $scope.committeeObj = {};
        $scope.committeeObj.committee = {};
        $scope.committeeObj.committee.company_id = $rootScope.id_company;
        $scope.committeeObj.committee.company_approval_structure_id = $rootScope.current_structure_id;//params
        $scope.committeeObj.committee.branch_type_id = item.branch_type_id;//params
        $scope.committeeObj.committee.is_primary_committee = false;
        $scope.committeeObj.committee_members = [];
        $scope.committeeObj.delete_members = [];
        $scope.member = {};
        $scope.submitBtn = false;
        $scope.braTypeRoleListFun = function () {
            companyService.branchTypeRole({
                'company_id': $rootScope.id_company,
                'id_branch_type_role': 1
            }).then(function (response) {
                $scope.branchTypeRoleList = response.data;
            })
        }
        $scope.companyApprovalRoleFun = function () {
            companyService.companyApprovalRole({
                'company_id': $rootScope.id_company,
                'branch_type_id': item.branch_type_id
            }).then(function (response) {
                $scope.companyApprovalRoleList = response.data;
            })
        }
        $scope.approvalForwardFun = function () {
            var obj = {};
            obj.company_id = $rootScope.id_company;
            obj.branch_type_id = item.branch_type_id;
            obj.id_company_approval_structure = $rootScope.current_structure_id;
            if (item.id_company_approval_credit_committee) {
                obj.id_company_approval_credit_committee = item.id_company_approval_credit_committee;
            }
            companyService.approvalCommittees.approvalCreditCommitteeList(obj).then(function (response) {
                $scope.approvalForwardList = response.data;
                if ($scope.approvalForwardList == 0) {
                    $scope.committeeObj.committee.is_primary_committee = true;
                }
            })
        }
        $q.all([$scope.companyApprovalRoleFun, $scope.braTypeRoleListFun, $scope.approvalForwardFun]).then(function () {
            if (item.id_company_approval_credit_committee != undefined) {
                companyService.approvalCommittees.approvalCreditCommitteeById({
                    'company_id': $rootScope.id_company,
                    'id_company_approval_credit_committee': item.id_company_approval_credit_committee
                }).then(function (response) {
                    $scope.committeeObj.committee = response.data.committee;
                    if (response.data.committee_members) {
                        $scope.committeeObj.committee_members = response.data.committee_members;
                    } else {
                        $scope.committeeObj.committee_members = [];
                    }
                })
            }
        })
        $scope.saveCommittee = function (committee) {
            $scope.submitBtn = true;
            companyService.approvalCommittees.forwardCommittee(committee).then(function (response) {
                $scope.submitBtn = false;
                if (response.status) {
                    r = true;
                } else {
                    var r = confirm(response.error);
                }
                if (r == true) {
                    companyService.approvalCommittees.post(committee).then(function (response) {
                        if (response.status) {
                            $rootScope.getApprovalCommitees();
                            $rootScope.toast('Success', response.message);
                            $scope.cancel();
                        } else {
                            $rootScope.toast('Error', response.error, 'error');
                        }
                    })
                }
            });
        }
        //committee members
        $scope.addCommitteeMember = function (item) {
            var existItem = false;
            $.each($scope.committeeObj.committee_members, function (key, data) {
                if (data.branch_type_role_id == item.branch_type_role.id_branch_type_role && data.company_approval_role_id == item.company_approval_role.id_company_approval_role) {
                    existItem = true;
                }
            })
            if (!existItem) {
                var obj = {};
                obj.branch_type_role_name = item.branch_type_role.branch_type_role_name;
                obj.branch_type_role_id = item.branch_type_role.id_branch_type_role;
                obj.company_approval_role_id = item.company_approval_role.id_company_approval_role;
                obj.approval_name = item.company_approval_role.approval_name;
                $scope.committeeObj.committee_members.push(obj);
            }
            $scope.member = {};
        }
        $scope.removeCommitteeMembers = function (item) {
            if (item.id_company_approval_credit_committee_structure) {
                $scope.committeeObj.delete_members.push(item.id_company_approval_credit_committee_structure);
            }
            var index = $scope.committeeObj.committee_members.indexOf(item);
            $scope.committeeObj.committee_members.splice(index, 1);
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        //init
        $scope.braTypeRoleListFun();
        $scope.companyApprovalRoleFun();
        $scope.approvalForwardFun();
    })
    .controller('assessmentCtrl', function ($scope, $rootScope, $uibModal, companyService, $state, $q) {
        $scope.categoryQuestionList = [];
        $scope.show_options = true;
        $scope.id_assessment_question_type = $state.current.data.id_assessment_question_type;
        $scope.title = $state.current.data.title;
        $scope.sortableOptions = {
            update: function (e, ui) {
            },
            stop: function (e, ui) {
            }
        };
        $scope.beforeDrop = function (evt, ui, category) {
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function (e) {
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function (e) {
            angular.element(e.target).removeClass("drop-hover");
        };
        $scope.getAssessmentQuestion = function () {
            companyService.assessmentQuestion.question.get({
                'company_id': $rootScope.id_company,
                'id_assessment_question_type': $scope.id_assessment_question_type
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                }
            })
        }
        $scope.getAssessmentQuestion();
        $scope.statusChange = function (category, question) {
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function (result) {
                if (result.status) {
                    //$scope.getAssessmentQuestion();
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.categoryStatusChange = function (category) {
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.product_id = undefined;
        $scope.assessmentModal = function (option_type, question) {
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/assessment/assessment-questions-view.html',
                scope: $scope,
                controller: 'assessmentModalCtrl',
                resolve: {
                    category: option,
                    item: question
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        }
        $scope.addCategoryModal = function (selectedItem) {
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/assessment/add-category-modal.html',
                scope: $scope,
                resolve: {
                    item: selectedItem
                },
                controller: function ($uibModalInstance, $scope, $rootScope, item, companyService, masterService) {
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    $scope.submitted = false;
                    masterService.sector.getAssessmentSectors().then(function (result) {
                        $scope.sectorsList = result.data.data;
                    });
                    if (item != undefined) {
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        if (item.sector_id != null && item.sector_id != undefined) {
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }
                    }
                    $scope.save = function (category, form) {
                        if (form.$valid) {
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = $scope.id_assessment_question_type;
                            category.sector_id = category.sector_id.toString();
                            companyService.assessmentQuestion.category.post(category).then(function (result) {
                                if (result.status) {
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success', result.message);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            })
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        }
    })
    .controller('disbursementChecklistCtrl', function (decodeFilter, $scope, $rootScope, $uibModal, companyService, $state, $q, $stateParams, masterService) {
        $scope.categoryQuestionList = [];
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.show_options = true;
        $scope.id_assessment_question_type = $state.current.data.id_assessment_question_type;
        $scope.title = $state.current.data.title;
        $scope.product_id = decodeFilter($stateParams.productId);
        $scope.sortableOptions = {
            update: function (e, ui) {
            },
            stop: function (e, ui) {
            }
        };
        $scope.beforeDrop = function (evt, ui, category) {
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function (e) {
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function (e) {
            angular.element(e.target).removeClass("drop-hover");
        };
        $scope.getAssessmentQuestion = function () {
            companyService.assessmentQuestion.question.get({
                'company_id': $rootScope.id_company,
                'id_assessment_question_type': $scope.id_assessment_question_type,
                'product_id': $scope.product_id
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                }
            })
        }
        $scope.getAssessmentQuestion();
        $scope.statusChange = function (category, question) {
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.categoryStatusChange = function (category) {
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.assessmentModal = function (option_type, question) {
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'modal-full my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/assessment/assessment-questions-view.html',
                scope: $scope,
                controller: 'assessmentModalCtrl',
                resolve: {
                    category: option,
                    item: question
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        }
        $scope.addCategoryModal = function (selectedItem) {
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-category-modal.html',
                scope: $scope,
                resolve: {
                    item: selectedItem
                },
                controller: function ($uibModalInstance, $scope, $rootScope, item, companyService, masterService) {
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    masterService.sector.getAll().then(function (result) {
                        $scope.sectorsList = result.data.data;
                    });
                    if (item != undefined) {
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        $scope.category.operational_checklist_type = item.operational_checklist_type;
                        if (item.sector_id != null && item.sector_id != undefined) {
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }
                    }
                    $scope.save = function (category) {
                        if ($scope.form.$valid) {
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = $scope.id_assessment_question_type;
                            category.sector_id = category.sector_id.toString();
                            category.product_id = $scope.product_id;
                            category.legal_operational_checklist = category.legal_operational_checklist;
                            companyService.assessmentQuestion.category.post(category).then(function (result) {
                                if (result.status) {
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success', result.message);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            })
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        }
    })
    .controller('assessmentModalCtrl', function ($uibModalInstance, $scope, $rootScope, item, category, companyService) {
        $scope.question = {};
        $scope.question.required = 1;
        $scope.question.created_by = $rootScope.userId;
        $scope.question.question_option = [];
        $scope.question.question_type = category.value;
        $rootScope.loaderOverlay = false;
        $scope.getCategory = function () {
            companyService.assessmentQuestion.category.get({
                'company_id': $rootScope.id_company,
                'assessment_question_type_id': $scope.id_assessment_question_type,
                'product_id': $scope.product_id
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryList = result.data;
                    if ($scope.id_assessment_question_category != undefined) {
                        $scope.question.assessment_question_category_id = $scope.id_assessment_question_category;
                    }
                    if (item != undefined) {
                        angular.copy(item, $scope.question);
                        $scope.question.required = parseInt(item.required);
                        $scope.option_type = item.question_type;
                    }
                }
            })
        }
        $scope.getCategory();
        $scope.save = function (question) {
            if ($scope.form.$valid) {
                companyService.assessmentQuestion.question.post(question).then(function (result) {
                    if (result.status) {
                        $scope.getAssessmentQuestion();
                        $rootScope.toast('Success', result.message);
                        $scope.cancel();
                    } else {
                        $rootScope.toast('Error', result.error, 'error');
                    }
                })
            }
        }
        // add new option to the field
        $scope.addOption = function (question, title) {
            if (!question.question_option)
                question.question_option = new Array();
            var lastOptionID = 0;
            if (question.question_option[question.question_option.length - 1])
                lastOptionID = question.question_option[question.question_option.length - 1].option_id;
            // new option's id
            var option_id = lastOptionID + 1;
            var newOption = {
                "option_id": option_id,
                "option_title": title,
                "option_value": option_id,
                "option_name": 'option_name_' + option_id,
                "option_check": true
            };
            // put new option into field_options array
            question.question_option.push(newOption);
        }
        // delete particular option
        $scope.deleteOption = function (question, option) {
            for (var i = 0; i < question.question_option.length; i++) {
                if (question.question_option[i].option_id == option.option_id) {
                    question.question_option.splice(i, 1);
                    break;
                }
            }
        }
        // decides whether field options block will be shown (true for dropdown and radio fields)
        $scope.showAddOptions = function (question) {
            if (question.question_type == "radio" || question.question_type == "dropdown" || question.question_type == "checkbox") {
                if (question.question_option.length == 0) {
                    if (question.question_type == "radio") {
                        $scope.addOption(question, 'Yes');
                        $scope.addOption(question, 'No');
                    } else {
                        $scope.addOption(question);
                    }
                }
                return true;
            }
            else
                return false;
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('assessmentModalCtrl2', function ($uibModalInstance, $scope, $rootScope, item, category, companyService) {
        $scope.categoryTitle = category.assessment_question_category_name;
        $scope.question = {};
        $scope.question.required = true;
        $scope.question.created_by = $rootScope.userId;
        $scope.question.assessment_question_category_id = category.id_assessment_question_category;
        $scope.question.question_option = [];
        if (item != undefined) {
            angular.copy(item, $scope.question);
        }
        $scope.save = function (question) {
            if ($scope.form.$valid) {
                companyService.assessmentQuestion.question.post(question).then(function (result) {
                    if (result.status) {
                        $scope.getAssessmentQuestion();
                        $rootScope.toast('Success', result.message);
                        $scope.cancel();
                    } else {
                        $rootScope.toast('Error', result.error, 'error');
                    }
                })
            }
        }
        // add new option to the field
        $scope.addOption = function (question) {
            if (!question.question_option)
                question.question_option = new Array();
            var lastOptionID = 0;
            if (question.question_option[question.question_option.length - 1])
                lastOptionID = question.question_option[question.question_option.length - 1].option_id;
            // new option's id
            var option_id = lastOptionID + 1;
            var newOption = {
                "option_id": option_id,
                "option_title": "",
                "option_value": option_id,
                "option_name": 'option_name_' + option_id,
                "option_check": true
            };
            // put new option into field_options array
            question.question_option.push(newOption);
        }
        // delete particular option
        $scope.deleteOption = function (question, option) {
            for (var i = 0; i < question.question_option.length; i++) {
                if (question.question_option[i].option_id == option.option_id) {
                    question.question_option.splice(i, 1);
                    break;
                }
            }
        }
        // decides whether field options block will be shown (true for dropdown and radio fields)
        $scope.showAddOptions = function (question) {
            if (question.question_type == "radio" || question.question_type == "dropdown" || question.question_type == "checkbox") {
                if (question.question_option.length == 0) {
                    $scope.addOption(question);
                }
                return true;
            }
            else
                return false;
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('assessmentQuestionView', function ($scope, $rootScope, companyService) {
        $scope.categoryQuestionList = [];
        $scope.getAssessmentQuestion = function () {
            companyService.assessmentQuestion.question.get({
                'company_id': $rootScope.id_company,
                'id_assessment_question_type': 3
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                }
            })
        };
        $scope.getAssessmentQuestion();
    })
    .controller('productWorkFlowCtrl', function($scope, $rootScope, $stateParams, $state, decodeFilter, $uibModal, productService, companyService){
        $scope.productsList = {};
        $scope.productDetails = {};
        /*$rootScope.id_product = decodeFilter($stateParams.productId);*/
        $rootScope.id_product = $rootScope.product_id;
        console.log('$rootScope.id_product',$rootScope.id_product);
        $rootScope.isActive = 'info';
        $scope.getProductsList = function(){
            var params = {};
            params.company_id = $rootScope.id_company;
            params.id_product = $rootScope.id_product;
            productService.getProductsList(params).then(function(result){
                $scope.productsList = result.data.data;
                if($scope.productsList.length>0){
                    $scope.productDetails = $scope.productsList[0];
                } else {
                    //redirect
                }
            });
        }
        $scope.getProductsList();
        $scope.selectRow = {};
        productService.getProductStages().then(function(result){
            if(result.status){
                $scope.productStages = result.data;
                $scope.selectRow = $scope.productStages[0];
                $scope.getList();
            }
        });
        $scope.getList=function(){
            var data = {};
            data.company_id=$rootScope.id_company;
            data.product_id=$rootScope.id_product;
            data.project_stage_id=$scope.selectRow.id_project_stage;
            companyService.productStageWorkflow.get(data).then(function(result){
                if(result.status){
                    $scope.tableData = result.data.data;
                    console.log('$scope.tableData',$scope.tableData);
                }
            })
        };
        $scope.getStageData = function(row) {
            $scope.selectRow = row;
            $scope.getList();
        }
        $scope.deleteWorkFlow=function(index){
            var r=confirm("Do you want to continue?");
            if(r==true){
                $scope.tableData[index].workflow_phase_status = '2';
                var data = $scope.tableData[index];
                companyService.productStageWorkflow.post(data).then(function(result){
                    if(result.status){
                        $scope.getList();
                        $rootScope.toast('Success',result.message);
                    }else{
                        $rootScope.toast('Error',result.error,'error');
                    }
                });
            }
        };
        companyService.userList({'company_id':$rootScope.id_company,'type':'admin'}).then(function(result){
            $scope.userList = result.data.data;
        });
        companyService.companyApprovalRole({'company_id':$rootScope.id_company}).then(function(result){
            $scope.userRoles = result.data;
        });
        $scope.addStageStep = function(row){
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl:'partials/admin/product/product-add-step.html',
                controller : function($scope,$uibModalInstance,crmCompanyService,projectService,item){
                    $scope.setup ={};
                    $scope.setup.approver_info =[];
                    $scope.approver_info =[];
                    $scope.isEdit = false;
                    $scope.cancel = function(){
                        $uibModalInstance.close();
                        $scope.approver_info = [];
                    };
                    if(item){
                        /*$scope.editData=item;*/
                        $scope.setup=item;
                        if(item.product_workflow_approval_type=='Committee'){
                            if(item.approver_info.length>0){
                                for(var a in item.approver_info){
                                    $scope.approver_info[a] = item.approver_info[a];
                                    if(!$scope.approver_info[a]['name']){
                                        console.log($scope.approver_info[a]['name']);
                                        $scope.approver_info[a]['name'] = item.approver_info[a].approver;
                                    }
                                }
                            }
                        } else if(item.product_workflow_approval_type=='User'){
                            $scope.setup.user = item.approver_info[0].approver_id;
                        } else if(item.product_workflow_approval_type=='Role'){
                            $scope.setup.role = item.approver_info[0].approver_id;
                        }
                        $scope.isEdit = true;
                    }
                    $scope.addUser=function(user){
                        if(user){
                            for(var a in $scope.approver_info){
                                if($scope.approver_info[a].approver_id == user){
                                    $rootScope.toast('Error','User already added');
                                    return;
                                }
                            }
                            var found = false;
                            var userData = {};
                            for(var a in $scope.userList){
                                if($scope.userList[a].id_user == user){
                                    found = true;
                                    userData = $scope.userList[a];
                                }
                            }
                            $scope.approver_info.push({"approver_id":user,"is_approval_mandatory":"1","status":"1","name":userData.first_name+' '+userData.last_name});
                            $scope.setup.user = '';
                            userData = {};
                        } else {
                            $rootScope.toast('Error','Select any User');
                        }
                    }
                    $scope.addRole=function(user){
                        if(user){
                            for(var a in $scope.approver_info){
                                if($scope.approver_info[a].approver_id == user.id_approval_role){
                                    $rootScope.toast('Error','User already added');
                                    return;
                                }
                            }
                            $scope.approver_info.push({"approver_id":user.id_approval_role,"is_approval_mandatory":"0","status":"1","name":user.approval_name});
                            $scope.setup.role = '';
                        } else {
                            $rootScope.toast('Error','Select any User');
                        }
                    }
                    $scope.saveRole = function(data){
                        console.log(data);
                        if(data.product_workflow_approval_type=='Committee'){
                            if($scope.approver_info) {
                                data.approver_info = $scope.approver_info;
                                data.product_workflow_approval_process = 'Sequential';
                            } else {
                                $rootScope.toast('Error','Select any User');
                                return;
                            }
                        } else if(data.product_workflow_approval_type=='User'){
                            data.approver_id = data.user;
                            data.product_workflow_approval_process = 'Parallel';
                        } else if(data.product_workflow_approval_type=='Role'){
                            data.approver_id = data.role;
                            data.product_workflow_approval_process = 'Parallel';
                        }
                        data.company_id=$rootScope.id_company;
                        data.product_id=$rootScope.id_product;
                        data.project_stage_id=$scope.selectRow.id_project_stage;
                        data.created_by=$rootScope.userId;
                        console.log(data);
                        companyService.productStageWorkflow.post(data).then(function(result){
                            if(result.status){
                                $scope.getList();
                                $rootScope.toast('Success',result.message);
                                $scope.cancel();
                            }else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        })
                    };
                    $scope.deleteProductStep=function(index){
                        var r=confirm("Do you want to remove this sector?");
                        if(r==true){
                            console.log(index);
                            $scope.approver_info[index].status = '2';
                        }
                    };
                },
                resolve:{
                    item: row
                }
            });
            modalInstance.result.then(function(){

            },function(){

            });
        }
        $scope.manageActionsWorkFlow = function(row,stage){
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl:'manage-actions.html',
                controller : function($scope,$uibModalInstance,productService,decodeFilter,item){
                    $scope.action = {};
                    $scope.managedActions = [];
                    $scope.deletedList= [];
                    $scope.getManagedActionsList = function () {
                        var params = {};
                        params.company_id = $rootScope.id_company;
                        params.product_id = $scope.id_product;
                        params.project_stage_id = stage.id_project_stage;
                        params.id_product_workflow_phase = row.id_product_workflow_phase;
                        productService.getManageActionsList(params).then(function(result){
                            if(result.status){
                                $scope.workFlowActions = result.data.workflow_actions;
                                $scope.workFlowPhases = result.data.workflow_phases;
                                $scope.managedActions = result.data.workflow_actions_data;
                                $scope.workflow_next_stage_details = result.data.workflow_next_stage_details;
                                angular.forEach($scope.managedActions, function(i,o){
                                    angular.forEach($scope.workFlowActions, function(item,key){
                                        if(item.id_workflow_action == i.id_workflow_action){
                                            $scope.workFlowActions.splice(key,1);
                                        }
                                    });
                                });
                            }
                        });
                    };

                    $scope.getManagedActionsList();
                    $scope.getNewActions = function(data, form){
                        form.$setPristine();
                        form.$setDirty();
                        if(!angular.equals({}, data)){
                            var obj={};
                            obj.workflow_action_name = data.workflow_action.workflow_action_name;
                            obj.id_workflow_action = data.workflow_action.id_workflow_action;
                            obj.is_approve = data.workflow_action.is_approve;
                            if(data.workflow_phase){
                                obj.id_product_workflow_phase = data.workflow_phase.id_product_workflow_phase;
                                obj.workflow_phase_name = data.workflow_phase.workflow_phase_name;
                            }
                            obj.next_project_stage_id = null;
                            if(data.workflow_action.is_approve=='1'){
                                obj.id_product_workflow_phase = null;
                                obj.workflow_phase_name = null;
                                obj.next_project_stage_assign_type = data.next_project_stage_assign_type;
                                obj.next_project_stage_id = $scope.workflow_next_stage_details.id_project_stage;
                                obj.workflow_phase_name=$scope.workflow_next_stage_details.stage_name;
                                if(data.next_project_stage_assign_type=='Owner'){
                                    obj.next_project_stage_assign_to = null;
                                }else{
                                    obj.next_project_stage_assign_to = data.next_project_stage_assign_to;
                                    if(data.next_project_stage_assign_type=='User') {
                                        for (var a in $scope.userList) {
                                            if ($scope.userList[a].id_user == data.next_project_stage_assign_to) {
                                                obj.next_project_stage_assign_to_name = $scope.userList[a].first_name + ' ' + $scope.userList[a].last_name;
                                                break;
                                            }
                                        }
                                    } else if(data.next_project_stage_assign_type=='Role'){
                                        for (var a in $scope.userRoles) {
                                            if ($scope.userRoles[a].id_approval_role == data.next_project_stage_assign_to) {
                                                obj.next_project_stage_assign_to_name = $scope.userRoles[a].approval_name;
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else {
                                obj.next_project_stage_assign_to = null;
                                obj.next_project_stage_assign_type = null;
                            }
                            $scope.managedActions.push(obj);
                            $scope.action = {};
                            angular.forEach($scope.workFlowActions, function(i,o){
                                if(i.id_workflow_action == data.workflow_action.id_workflow_action){
                                    $scope.workFlowActions.splice(o,1);
                                }
                            });
                        }
                    }
                    $scope.removeActionPhase = function(row,index){
                        var bool = true;
                        angular.forEach($scope.workFlowActions, function(i,o){
                            if(bool){
                                if (i.id_workflow_action == row.id_workflow_action) {
                                    $scope.workFlowActions.splice(o, 1);
                                }else {
                                    $scope.workFlowActions.push(row);
                                    bool = false;
                                }
                            }
                        });
                        console.log(index);
                        $scope.deletedList.push($scope.managedActions.splice(index,1));
                    }
                    $scope.saveProductWorkflowActions = function(data){
                        console.log('data',data);
                        $scope.workflow_actions_data = [];
                        var param = {};
                        param.created_by = $rootScope.userId;
                        param.id_product_workflow_phase = row.id_product_workflow_phase;
                        angular.forEach(data, function(item,o){
                            var obj = item;
                            obj.id_workflow_action = item.id_workflow_action;
                            obj.next_project_stage_id = item.next_project_stage_id;
                            obj.status = 1;
                            if(item.forward_product_workflow_phase_id){
                                obj.forward_product_workflow_phase_id = item.forward_product_workflow_phase_id;
                            }
                            if(item.id_product_workflow_phase)
                                obj.forward_product_workflow_phase_id = item.id_product_workflow_phase;
                            if(item.id_product_workflow_phase_action)
                                obj.id_product_workflow_phase_action = item.id_product_workflow_phase_action;
                            $scope.workflow_actions_data.push(obj);
                        });
                        if($scope.deletedList){
                            angular.forEach($scope.deletedList, function(it,ok){
                                if(it[0].id_product_workflow_phase_action){
                                    var obj = {};
                                    obj.forward_product_workflow_phase_id = it[0].forward_product_workflow_phase_id;
                                    obj.id_workflow_action = it[0].id_workflow_action;
                                    obj.id_product_workflow_phase_action = it[0].id_product_workflow_phase_action;
                                    obj.status = 2;
                                    $scope.workflow_actions_data.push(obj);
                                }
                            });
                        }
                        param.workflow_actions_data = $scope.workflow_actions_data;
                        console.log('param',param);
                        productService.postWorkflowActions(param).then(function(result){
                            if(result.status){
                                $scope.getList();
                                $rootScope.toast('Success',result.message);
                                $scope.cancel();
                            }else $rootScope.toast('Error',result.error,'error');
                        });
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.close();
                    };
                },
                resolve:{
                    item: row
                }
            });
            modalInstance.result.then(function(){
            },function(){
            });
        }
    })
    .controller('knowledgeManagementCtrl', function ($scope, $rootScope, $state, $uibModal, companyService) {
        $scope.addKnowledgeManagementModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-full my-class modal-bodyPaddingnone',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/knowledge-management/add-knowledge-management.html',
                controller: 'addKnowledgeManagementCtrl'
            });
            modalInstance.result.then(function () {
            }, function () {
                $scope.knowledgeManagement();
                $rootScope.knowledgeManagementObj = '';
            });
        }
        $scope.knowledgeManagement = function () {
            var params = {};
            params.company_id = $rootScope.id_company;
            if ($rootScope.user_role_id != 2) {
                params.created_by = $rootScope.userId;
            }
            companyService.knowledgeManagement.get(params).then(function (result) {
                $scope.knowledgeData = result.data.data;
                $scope.countData = result.data.document_type_data;
                $scope.usersData = result.data.users;
            })
        };
        $scope.knowledgeManagementGraph = function () {
            companyService.knowledgeManagement.knowledgeManagementGraph({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.knowledgeGraphData = result.data;
                $scope.graph = {views: '', uploads: ''};
                var views = [];
                var uploads = [];
                angular.forEach($scope.knowledgeGraphData.views, function (value, key) {
                    views.push([new Date(value.date_time).getTime(), parseInt(value.views)])
                });
                angular.forEach($scope.knowledgeGraphData.uploads, function (value, key) {
                    uploads.push([new Date(value.date_time).getTime(), parseInt(value.uploads)])
                });
                $scope.graph = {views: views, uploads: uploads};
            })
        };
    })
    .controller('searchKnowledgeManagementCtrl', function ($scope, $rootScope, $state, $uibModal, companyService, crmContactService) {
        companyService.knowledgeManagement.getTags().then(function (result) {
            $scope.tagsData = result.data;
        });
        $scope.knowledgeManagement = function () {
            companyService.knowledgeManagement.get({
                'company_id': $rootScope.id_company,
                'created_by': $rootScope.userId
            }).then(function (result) {
                $scope.countData = result.data.document_type_data;
            })
        };
        $scope.knowledgeManagement();
        $scope.changedValue = function (val) {
            $scope.tableStateRef.search_tags = JSON.stringify(val);
            $scope.callServer($scope.tableStateRef);
        }
        $scope.displayed = [];
        $scope.callServer = function (tableState) {
            tableState.company_id = $rootScope.id_company;
            if ($rootScope.user_role_id != 2) {
                tableState.created_by = $rootScope.userId
            }
            companyService.knowledgeManagement.getSearchResult(tableState).then(function (result) {
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.displayed = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
            });
        };
        $scope.addKnowledgeManagementModal = function (knowledgeObj) {
            if (knowledgeObj) {
                $rootScope.knowledgeManagementObj = knowledgeObj;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-full my-class modal-bodyPaddingnone',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/knowledge-management/add-knowledge-management.html',
                controller: 'addKnowledgeManagementCtrl'
            });
            modalInstance.result.then(function () {
            }, function () {
                $rootScope.knowledgeManagementObj = '';
                $scope.callServer($scope.tableStateRef);
            });
        }
    })
    .controller('addKnowledgeManagementCtrl', function ($scope, $rootScope, $uibModalInstance, companyService, Upload) {
        $scope.knowledge = {};
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        companyService.knowledgeManagement.getTags().then(function (result) {
            $scope.tagsData = result.data;
            if ($rootScope.knowledgeManagementObj && $rootScope.knowledgeManagementObj != '') {
                $scope.knowledge = $rootScope.knowledgeManagementObj;
            }
        });
        $scope.uploadDocument = function (file) {
            if (file != null && file != '') {
                $scope.file = file;
            } else {
                $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
            }
        };
        $scope.save = function (knowledge, knowledgeForm) {
            $scope.submitStatus = true;
            if (!knowledgeForm.$valid) {
                return false;
            }
            if (knowledge.document_type == 'document' && $rootScope.knowledgeManagementObj == undefined && knowledge.file.document.length == 0) {
                $scope.fileError = true;
                return false;
            }
            var postObj = knowledge;
            postObj.uploaded_by = $rootScope.userId;
            postObj.company_id = $rootScope.id_company;
            Upload.upload({
                url: API_URL + 'Company/knowledge',
                data: postObj
            }).then(function (result) {
                if ($rootScope.toastMsg(result.data)) {
                    //$rootScope.toast('Success', resp.data.message);
                    $uibModalInstance.dismiss('cancel');
                }
            });
        }
    })
    .controller('knowledgeManagementViewCtrl', function ($scope, $rootScope, companyService, $stateParams, crmCompanyService) {
        $scope.knowledgeId = $stateParams.id;
        $scope.knowledgeDocumentDetails = function () {
            companyService.knowledgeManagement.getDetails({'knowledge_document_id': $scope.knowledgeId}).then(function (result) {
                $scope.knowledgeDocumentDetails = result.data;
            })
        };
        crmCompanyService.updateViewCount({'id_knowledge_document': $scope.knowledgeId});
        $scope.knowledgeDocumentDetails();
        $scope.getKnowledgeDocumentComment = function () {
            companyService.knowledgeDocumentComment.get({'knowledge_document_id': $scope.knowledgeId}).then(function (result) {
                $scope.knowledgeDocumentComment = result.data;
            })
        };
        $scope.getKnowledgeDocumentComment();
        $scope.changeKnowledgeDocumentStatus = function () {
            companyService.knowledgeManagement.knowledgePublish({
                'knowledge_document_id': $scope.knowledgeId,
                'knowledge_document_status': 1
            }).then(function (result) {
                $scope.knowledgeDocumentDetails.knowledge_document_status = 1
                $rootScope.toast('Success', result.message);
                ;
            })
        };
        $scope.addKnowledgeManagementComment = function (comment, form) {
            $scope.submitStatus = true;
            if (form.$valid) {
                companyService.knowledgeDocumentComment.add(
                    {
                        'knowledge_document_id': $stateParams.id,
                        'commented_by': $rootScope.userId,
                        'comment': comment
                    }
                ).then(function (result) {
                    $scope.comment = '';
                    $scope.submitStatus = false;
                    $scope.getKnowledgeDocumentComment();
                })
            }
        };
    })
    .controller('admRiskAssessmentCtrl', function ($scope, $rootScope, masterService, companyService) {
        //riskCategory
        $scope.riskCategory = [];
        $scope.subCategoryForm = {};
        isContact=false;
        $scope.clone = function ($org_obj) {
            return angular.copy($org_obj);
        }
        masterService.sector.getAll().then(function (result) {
            $scope.sectorsList = result.data.data;
            $scope.sectorsList.unshift({'id_sector': "0", 'sector_name': 'All Sectors'});
        });
        $scope.getContactOrCompany = function(type){
            console.log(type);
            companyService.riskAssessment.riskCategory.get({'company_id': $rootScope.id_company, 'type': type}).then(function (result) {
                if (result.status) {
                    $scope.riskCategory = result.data;
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        };
        $scope.itemDelete = function (params, item, index) {
            if ($rootScope.confirmMsg()) {
                companyService.riskAssessment.riskCategory.delete(params).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        item.splice(index, 1);
                    }
                })
            }
        }

        $scope.addRiskCategory = function (category, rcForm, type) {
            console.log('category',category);
            console.log('rcForm',rcForm);
            console.log('type',type);
            $scope.isContact = false;

            //category.sub_category = [];
            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = 0;
            param.risk_percentage = category.risk_percentage;
            param.risk_category_name = category.risk_category_name;
            param.sector_id = category.sector_id?category.sector_id:0;
            console.log('category.sector_id[0]',param);
            param.type = type;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function (result) {
                if (result.status) {
                    rcForm.$setPristine();
                    $scope.showCategoryForm = false;
                    result.data.sub_category = [];
                    $scope.riskCategory.push(result.data);
                    $scope.category = {};
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.addSubRiskCategory = function (categoryIndex, category, form, type) {
            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = category.id_risk_category;
            param.risk_percentage = category.subCategoryForm.risk_percentage;
            param.risk_category_name = category.subCategoryForm.risk_category_name;
            param.type = type;
            //item.attributes = [];
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function (result) {
                if (result.status) {
                    form.$setPristine();
                    form.$setUntouched();
                    result.data.attributes = [];
                    $scope.riskCategory[categoryIndex].sub_category.push(result.data);
                    category.subCategoryForm = {};
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.addSubcategoryAttr = function (category, form, type) {
            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = category.subSelected.id_risk_category;
            param.risk_percentage = 0;
            param.risk_category_name = category.subSelected.selectedForm.risk_category_name;
            param.type = type;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function (result) {
                if (result.status) {
                    form.$setPristine();
                    form.$setUntouched();
                    result.data.sector_list = [];
                    result.data.items = [];
                    category.subSelected.attributes.push(result.data);
                    category.subSelected.selectedForm = {};
                    category.subSelected.attrForm = false;
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.addSectorItems = function (attribute, form) {
            console.log('attribute',attribute);
            var param = {};
            param.risk_category_id = attribute.id_risk_category;
            param.risk_category_item_name = attribute.form.risk_category_item_name;
            param.risk_category_item_grade = attribute.form.risk_category_item_grade;
            //param.sector_id = attribute.id_sector;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskItem.post(param).then(function (result) {
                if (result.status) {
                    form.$setPristine();
                    form.$setUntouched();
                    $rootScope.toast('Success', result.message);
                    attribute.items.push(result.data);
                    attribute.form = {};
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.subCategoryClick = function (subCategory, category) {
            category.subSelected = subCategory;
            category.attrDivShow = true;
        }
        $scope.sectorClick = function (attribute, sector) {
            attribute.sectorSelected = sector;
            attribute.itemDivShow = true;
        }
        $scope.sectorDropdownChange = function (attribute, item) {
            if (item != undefined) {
                var obj = [];
                obj.id_sector = item.id_sector
                obj.parent_id = item.parent_id
                obj.sector_name = item.sector_name
                obj.parent = item.parent
                obj.items = [];
                attribute.sector_list.push(obj);
            }
        }
        $scope.checkSectorDropdown = function (sectorList, option) {
            return _.findWhere(sectorList, {'id_sector': option.id_sector})
        }
        $scope.isActive = function (item, category) {
            return category.subSelected === item;
        };
        $scope.isSectorActive = function (attribute, item) {
            return attribute.sectorSelected.id_sector === item.id_sector;
        };
        $scope.editRiskCategory = function (subCategory) {
            console.log('subCategory',subCategory);
            var param = {};
            param.company_id = $rootScope.id_company;
            param.id_risk_category = subCategory.id_risk_category;
            param.risk_percentage = subCategory.risk_percentage;
            param.risk_category_name = subCategory.risk_category_name;
            param.parent_risk_category_id = subCategory.parent_risk_category_id;
            param.sector_id = subCategory.sector_id;
            param.type = subCategory.type;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function (result) {
                if (result.status) {
                    subCategory.sector_name = result.data.sector_name;
                    subCategory.popover = false;
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.editAttributeItem = function (item) {
            var param = {};
            param.id_risk_category_item = item.id_risk_category_item;
            param.risk_category_id = item.risk_category_id;
            param.risk_category_item_name = item.risk_category_item_name;
            param.risk_category_item_grade = item.risk_category_item_grade;
            // param.sector_id = item.sector_id;
            item.popover = false;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskItem.post(param).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
    })
    .controller('businessAssessmentCtrl', function ($scope, $rootScope, crmContactService) {
        $scope.allAssessmentList = [];
        $scope.getBusinessAssessment = function () {
            crmContactService.getAllAssessment({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.allAssessmentList = result.data;
                angular.forEach($scope.allAssessmentList, function (pvalue, pkey) {
                    if (pvalue.checked == 0) {
                        $scope.allAssessmentList[pkey].status = false;
                        $scope.allAssessmentList[pkey].checked = false;
                    } else {
                        $scope.allAssessmentList[pkey].status = true;
                        $scope.allAssessmentList[pkey].checked = true;
                    }
                });
            });
        }
        $scope.getBusinessAssessment();
        $scope.updateAssessment = function () {
            var assessment = [];
            angular.forEach($scope.allAssessmentList, function (pvalue, pkey) {
                if (pvalue.status) {
                    assessment.push($scope.allAssessmentList[pkey].id_assessment);
                }
            });
            var $postData = {
                'company_id': $rootScope.id_company,
                'created_by': $rootScope.userId,
                'assessments': assessment
            }
            crmContactService.updateAssessment($postData).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            });
        }
    })
    .controller('exchangeRateCtrl', function ($scope, $rootScope, companyService, $uibModal, $q, Excel, $timeout) {
        $scope.currencyList = [];
        $scope.currencySelectedList = [];
        $scope.companyInfo = {};
        $scope.init = function () {
            $scope.currencyTable({'company_id': $rootScope.id_company});
            $scope.processArray = function (json) {
                var chartData = {};
                var rows = [];
                var cols = [];
                angular.forEach(json[0], function (value, key) {
                    if (key == 'Date') {
                        cols.push({
                            "id": key,
                            "label": key,
                            "type": 'string',
                            "p": {}
                        });
                    } else {
                        cols.push({
                            "id": key,
                            "label": key,
                            "type": 'number',
                            "p": {}
                        });
                    }
                })
                angular.forEach(json, function (value, key) {
                    var data = [];
                    angular.forEach(value, function (value) {
                        data.push({"v": value});
                    })
                    rows.push({c: data});
                })
                chartData.cols = cols;
                chartData.rows = rows;
                return chartData;
            }
            companyService.getById($rootScope.id_company).then(function (result) {
                $scope.companyInfo = result.data;
            })
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                $scope.currencySelectedList = $result.data;
            });
        }
        $scope.exportToExcel = function (tableId, sheetName) { // ex: '#my-table'
            $scope.exportHref = Excel.tableToExcel(tableId, sheetName);
            $timeout(function () {
                location.href = $scope.exportHref;
            }, 100); // trigger download
        }
        $scope.filterSearch = function (search) {
            var params = {};
            params.company_id = $rootScope.id_company;
            params.from_date = search.from_date;
            params.to_date = search.to_date;
            $scope.currencyTable(params);
        }
        $scope.currencyTable = function (params) {
            companyService.currency.currencyTable(params).then(function (result) {
                $scope.currencyList = result.data;
                $scope.chartCurrencyList = $scope.processArray($scope.currencyList);
                $scope.chartObject = {
                    "type": "Line",
                    "data": $scope.chartCurrencyList
                };
            })
        }
        $scope.params = [];
        $scope.addCurrecyValue = function (currencyItems) {
            $scope.form.$submitted = false;
            angular.forEach(currencyItems, function (item) {
                var $obj = {};
                $obj.company_id = $rootScope.id_company;
                $obj.company_currency_id = item.id_company_currency;
                $obj.company_currency_value = item.currency_value_new;
                $scope.params.push($obj);
            })
            $q.all($scope.params).then(function (params) {
                companyService.currency.currencyValue(params).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.init();
                    } else {
                        $rootScope.toast('Error', result.error);
                    }
                })
            });
        }
        $scope.addCurrencyModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                windowClass: 'my-class',
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/operating-currency/exchange-rate-modal.html',
                resolve: {
                    currency: function () {
                        return companyService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                            return $result.data;
                        });
                    }
                },
                controller: function ($rootScope, $scope, $uibModalInstance, currency, $q, companyService) {
                    $scope.currencyList = currency;
                    $scope.params = [];
                    $scope.save = function (currencyItems) {
                        angular.forEach(currencyItems, function (item) {
                            var $obj = {};
                            $obj.company_id = $rootScope.id_company;
                            $obj.company_currency_id = item.id_company_currency;
                            $obj.company_currency_value = item.currency_value;
                            $scope.params.push($obj);
                        })
                        $q.all($scope.params).then(function (params) {
                            companyService.currency.currencyValue(params).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $uibModalInstance.close();
                                } else {
                                    $rootScope.toast('Error', result.error);
                                }
                            })
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.init();
            }, function () {
            });
        };
    })
    .controller('primaryCurrencyCtrl', function ($scope, $rootScope, masterService, companyService) {
        $scope.currencyList = [];
        $scope.companyInfo = {};
        $scope.init = function () {
            masterService.currency.getAll().then(function (result) {
                $scope.currencyList = result.data;
                companyService.getById($rootScope.id_company).then(function (result) {
                    $scope.companyInfo = result.data;
                })
            })
        }
        $scope.updateCurrency = function ($data) {
            var param = {};
            param.company_id = $rootScope.id_company;
            param.currency_id = $data;
            if (confirm("If you change currency, it will effect future projects")) {
                companyService.primaryCurrency(param).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error');
                    }
                });
            }
        }
        $scope.init();
    })
    .controller('operatingCurrencyCtrl', function ($scope, $rootScope, masterService, companyService) {
        $scope.currencyList = [];
        $scope.companyInfo = {};
        $scope.init = function () {
            $scope.currencyList = [];
            companyService.currency.CurrencySelected({'company_id': $rootScope.id_company}).then(function (result) {
                angular.forEach(result.data, function (item) {
                    var obj = item;
                    obj.checkStatus = (item.is_selected == 0) ? false : true;
                    $scope.currencyList.push(obj);
                })
                companyService.getById($rootScope.id_company).then(function (result) {
                    $scope.companyInfo = result.data;
                })
            })
        }
        $scope.updateSelected = function () {
            $scope.selectedIds = [];
            angular.forEach($scope.currencyList, function (item) {
                if (item.checkStatus) {
                    $scope.selectedIds.push(item.id_currency);
                }
            })
            var param = {};
            param.company_id = $rootScope.id_company;
            param.currency_id = $scope.selectedIds;
            companyService.currency.post(param).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                    $scope.init();
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.init();
    })
    .controller('accessCtrl', function ($scope, $rootScope, $stateParams, $http, $q, $filter, companyService, Auth, decodeFilter) {
        $rootScope.maxDate = new Date();
        $scope.userList = [];
        $scope.s = {};
        $scope.s.fromdate = $filter('date')($rootScope.maxDate, "dd-MM-yyyy");
        $scope.s.todate = $filter('date')($rootScope.maxDate, "dd-MM-yyyy");
        var promise1 = companyService.userList({'company_id': $rootScope.id_company}).then(function (result) {
            $scope.userList = result.data.data;
        });
        $q.all([promise1]).then(function (data) {
            var id = $stateParams.id;
            if (typeof id != 'undefined') {
                id = decodeFilter(id);
                $scope.s.ids = id;
                $scope.displayResult = true;
            }
        });
        //findUserAccessLog?id=254&fromdate=07-09-2016&todate=11-09-2016
        $scope.accessList = [];
        $scope.displayResult = false;
        $scope.searchAccessResult = function (s) {
            $scope.displayResult = true;
            if ($scope.s.ids) {
                $scope.refTableState.ids = $scope.s.ids.toString();
            } else {
                delete $scope.refTableState.ids;
            }
            $scope.refTableState.fromdate = $scope.s.fromdate;
            $scope.refTableState.todate = $scope.s.todate;
            $scope.getAccessList($scope.refTableState);
        }
        $scope.getAccessList = function (tableState) {
            companyService.userList({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.userList = result.data.data;
            });
            var id = $stateParams.id;
            var browser = '';
            var os = '';

            $scope.refTableState = tableState;
            $scope.accessList = [];
            if (typeof id != 'undefined') {
                id = decodeFilter(id);
                tableState.ids = id;
                tableState.fromdate = $scope.s.fromdate;
                tableState.todate = $scope.s.todate;
                //tableState.offset = 0;
            }
            if (tableState.ids) {
                Auth.userAccessLogsHistory(tableState).then(function (result) {
                    $scope.accessList = result.rows;
                    angular.forEach($scope.accessList, function(value, key){
                        var browser_name = $scope.accessList[key].data.user_agent.browser_name;
                        //var os_name = $scope.accessList[key].data.user_agent.os;
                        $scope.accessList[key].data.browser = browser_name.match(/[a-z ]+/ig)[0];
                        //$scope.accessList[key].data.os = os_name.match(/[a-z ]+/ig)[0];
                        $scope.accessList[key].data.os = $scope.accessList[key].data.user_agent.os;
                    });
                    tableState.pagination.numberOfPages = Math.ceil(result.total_records / tableState.pagination.number);
                })
            }
        }
    })
    .controller('auditLogCtrl', ['$scope', '$rootScope', '$uibModal', 'crmCompanyService', function ($scope, $rootScope, $uibModal, crmCompanyService) {
        $scope.pageTitle = 'Audit Logs';

        var userid = $rootScope.userId;
        var companyId = $rootScope.id_company;
        $scope.isLoading = false;

        $scope.getModuleList = function (moduleName) {
            $scope.auditLogsForm.$submitted = false;
            $scope.data.selected_list = '';
            var data = {
                'module_name': moduleName,
                'company_id': companyId
            };
            crmCompanyService.getModuleList(data).then(function (result) {
                if (result.status) {
                    $scope.moduleList = result.data;
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        };

        $scope.searchAuditLogsForm = function (data) {
            var regex_exp = /(_id|id_|-id|id-|created_by|updated_by)/i;
            $scope.isLoading = true;
            $scope.auditList = [];
            var selected = '';
            var data = {
                'module_name': data.module,
                'module_id': data.selected_list,
                'search_date': data.selected_date == undefined ? '' : data.selected_date
            };
            crmCompanyService.getAuditLogs(data).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                    $scope.isLoading = false;
                    $scope.auditList = result.data;
                    var browser_name = '';
                    var os_name = '';
                    angular.forEach(result.data, function (value, key) {
                        if (value.data) {
                            var formData = {};
                            angular.forEach(value.data, function (val1, key1) {
                                if (!(regex_exp.test(key1.trim()))) {
                                    formData[key1] = val1;
                                }
                            });
                            $scope.auditList[key].data = formData;
                        }
                        browser_name = $scope.auditList[key].user_agent.browser_name;
                        //os_name = $scope.auditList[key].user_agent.os;
                        $scope.auditList[key].browser = browser_name.match(/[a-z ]+/ig)[0];
                        //$scope.auditList[key].os = os_name.match(/[a-z ]+/ig)[0];
                        $scope.auditList[key].os = $scope.auditList[key].user_agent.os;
                    });
                }
            });
        };

        $scope.opendataModal = function (data) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                size:'lg',
                templateUrl: 'audit-data-modal.html',
                resolve: {
                    data: data
                },
                controller: function ($scope, $rootScope, $uibModalInstance, data) {
                    $scope.auditData = data;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.init();
            }, function () {
            });
        }
    }])
    .controller('SecurityCtrl', function ($scope, $rootScope, masterService, UserService, Auth, $state, $cookies, redirectToUrlAfterLogin) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.security_questions = result.data;
            })
        }
        $scope.submitForm = function ($inputData, form) {
            $scope.submitStatus = true;
            if (form.$valid) {
                $inputData.user_id = $rootScope.userId;
                UserService.saveSecurityQuestion($inputData).then(function (result) {
                    if (result.status) {
                        var temp = angular.fromJson(Auth.getFields());
                        temp.security_question_id = $inputData.questions;
                        temp.security_question_ans = $inputData.ans;
                        $rootScope.notification=false;
                        $rootScope.notificationMsg = '';
                        $cookies.put('app', angular.toJson(temp));
                        $rootScope.toast('Success', result.message);
                        if ($rootScope.user_role_id == '2') {
                            $rootScope.site_url = '#/admin-setup';
                            $state.go('adm.admin-setup');
                        } else if ($rootScope.user_role_id == '3') {

                            $rootScope.site_url = '#/user-dashboard';
                            $state.go('contact.contact-dashboard');
                        }
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })
    .controller('ExposureCtrl', function ($scope, $rootScope, crmFacilityService) {
        $scope.dropDownItems = [];
        $scope.getContactExposure = function (params) {
            crmFacilityService.crm.contactExposure(params).then(function (result) {
                if (result.status)
                    $scope.contactExposure = result.data;
            })
        }
        $scope.getCompanyExposure = function (params) {
            crmFacilityService.crm.companyExposure(params).then(function (result) {
                if (result.status)
                    $scope.companyExposure = result.data;
            })
        }
    })
    .controller('sectorLimitCtrl', function ($scope, $rootScope, $uibModal, companyService) {
        $scope.sectorLimitList = [];
        $scope.sectorCount = 0;
        $scope.subSectorCount = 0;
        $scope.disabledBtn = false;

        $scope.totalPercentage = 0;
        $scope.getPercentage = function () {
            var sum = 0;
            var dataValid = true;
            angular.forEach($scope.parentSector, function (obj) {
                sum = sum + eval(obj.limit_value);
                var subSum = 0;
                angular.forEach(obj.subSector, function (subObj) {
                    subSum = subSum + eval(subObj.limit_value);
                    if (subSum > 100 || isNaN(subSum)) {
                        dataValid = false;
                        $rootScope.toast('Error', obj.sector_name + ' Sub-Sectors percentage has been exceeded', 'warning')
                    }
                })
            })
            if (sum > 100 || isNaN(sum)) {

                dataValid = false;
                $rootScope.toast('Error', 'Sector percentage has been exceeded', 'warning')
            }
            $scope.disabledBtn = (dataValid) ? false : true;

            $scope.totalPercentage = sum;
        }
        /* $scope.$watch(function (newValue, oldValue) {
         $scope.getPercentage();
         }, true)*/

        $scope.checkSectorDropdown = function (item) {
            return _.findWhere($scope.sectorLimitList, {'id_sector': item.id_sector})
        }
        $scope.getSectorLimit = function () {
            $scope.sectorCount = 0;
            $scope.subSectorCount = 0;
            companyService.sectorLimit.get({'company_id': $rootScope.id_company}).then(function (result) {
                console.log('result.data', result.data);
                $scope.sectorLimitList = result.data;
                // console.log('$scope.sectorLimitList',$scope.sectorLimitList);
                $scope.parentSector = [];
                $scope.parentSector = _.where($scope.sectorLimitList, function (obj) {
                    if (obj.parent_sector_id == 0) {
                        $scope.sectorCount++;
                        obj.subSector = _.where($scope.sectorLimitList, function (obj2) {
                            if (obj2.parent_sector_id == obj.id_sector)
                                $scope.subSectorCount++;
                            return obj2.parent_sector_id == obj.id_sector;
                        })
                        return obj;
                    }
                });
                console.log('$scope.parentSector', $scope.parentSector);
            })
        }
        $scope.deleteSelectorLimit = function (id_sector_limit) {
            if ($rootScope.confirmMsg()) {
                companyService.sectorLimit.delete({'id_sector_limit': id_sector_limit}).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.getSectorLimit();
                    }
                })
            }
        }
        $scope.saveLimit = function () {
            var params = {};
            params.sector_data = $scope.sectorLimitList;
            params.created_by = $rootScope.userId;
            params.company_id = $rootScope.id_company;
            companyService.sectorLimit.post(params).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                    $scope.getSectorLimit();
                }
            })
        }
        $scope.getSectorLimit();
        $scope.openSectorLimitModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'sector-limit-modal.html',
                controller: function (masterService, $scope, $rootScope, $uibModalInstance) {
                    $scope.sectorsList = [];
                    $scope.getMainSector = function () {
                        masterService.sector.getAll({'parent_sector_id': 0}).then(function (result) {
                            $scope.sectorsList = result.data.data;
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.saveSector = function (sector_ids) {
                        var params = {};
                        params.sector_id = sector_ids;
                        params.company_id = $rootScope.id_company;
                        params.created_by = $rootScope.userId;
                        companyService.sectorLimit.addSectors(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.getSectorLimit();
                                $scope.cancel();
                            }
                        })
                    }


                }
            });
        }

    })



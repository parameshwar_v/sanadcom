angular.module('app')
    .config(['$ocLazyLoadProvider',function($ocLazyLoadProvider){
        $ocLazyLoadProvider.config({
            debug: false
        });
        $ocLazyLoadProvider.config({
            modules: [
                {
                    name: 'googlechart',
                    files: ['js/plugins/google-charts/ng-google-chart.js']
                },
                {
                    insertBefore: '#loadBefore',
                    name: 'chosen',
                    files: ['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                },
                {
                    serie: true,
                    name: 'multiselect',
                    files: ['css/multiple-select.css','js/plugins/multi-select/jquery.multiple.select.js','js/plugins/multi-select/angularjs-dropdown-multiselect.js']
                },
                {
                    serie: true,
                    name: 'ui.sortable',
                    files: ['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-sortable/sortable.js']
                },
                {
                    serie: true,
                    name: 'ngDragDrop',
                    files: ['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-dragdrop/angular-dragdrop.js']
                },
                {
                    name: 'xeditable',
                    files: ['js/plugins/xeditable/xeditable.js']
                },
                //js!https://maps.googleapis.com/maps/api/js?key=AIzaSyAhZhmZVyjcFarz57DBFmlC5WTHdXCkcA0&libraries=places&sensor=false
                {
                    serie: true,
                    name: 'ngMap',
                    files: ['js/plugins/ng-map/ng-map.js']
                },
                {
                    serie: true,
                    name: 'ngHandsontable',
                    files: [
                        'css/ng-handsontables/handsontable.full.css',
                        'js/plugins/ng-handsontable/handsontable.full.js',
                        'js/plugins/ng-handsontable/rulejs/numeric.js',
                        'js/plugins/ng-handsontable/formula.js',
                        'js/plugins/ng-handsontable/rulejs/parser.js',
                        'js/plugins/ng-handsontable/rulejs/ruleJS.js',
                        'js/plugins/ng-handsontable/handsontable.formula.js',
                        'js/plugins/ng-handsontable/ngHandsontable.min.js'
                    ]
                },
                {
                    serie: true,
                    name: 'ngTagsInput',
                    files: ['css/plugins/ng-tag-input/ng-tags-input.bootstrap.css','css/plugins/ng-tag-input/ng-tags-input.css','js/plugins/ng-tag-input/ng-tags-input.js']
                },
                {
                    name: 'ckeditor',
                    files: ['js/plugins/ckeditor/ckeditor.js','js/plugins/ckeditor/angular-ckeditor.js']
                },
                {
                    name: 'touch.points',
                    files: ['partials/component/touch_points/touch-points-directive.js']
                },
                {
                    name: 'attachment',
                    files: ['partials/component/attachment/attachment-directive.js']
                },
                {
                    name: 'meeting',
                    files: ['partials/component/meeting/meeting-directives.js']
                },
                {
                    name: 'ui-rangeSlider',
                    files: ['css/plugins/range-slider/rangeSlider.css','js/plugins/range-slider/rangeSlider.js']
                },
                {
                    name: 'task',
                    files: ['partials/component/task/task-directive.js']
                },
                {
                    name: 'notifications',
                    files: ['partials/component/notifications/notifications-directive.js']
                },
                {
                    name: 'crm.covenant',
                    files: ['partials/component/covenant/crm-covenant.js']
                },
                {
                    name: 'utility',
                    files: ['partials/component/utility/utility.js']
                }
            ]
        });
    }])
'use strict';
angular.module('app',[
        'ngAnimate',
        'ngCookies',
        'ngIdle',
        'ui.router',
        'oc.lazyLoad',
        'ui.bootstrap',
        'smart-table',
        'ngFileUpload',
        'easypiechart',
        'ui.utils.masks',
        "ngSanitize",
        "pascalprecht.translate",
        "angularMoment",
        "ngScrollbars"
    ])
    .value('redirectToUrlAfterLogin',{url: '/',first_time: true,direct_entry: true})
    .run(['$location','$rootScope','Auth','$state','$timeout','$http','$cookies','$window','$q','redirectToUrlAfterLogin',function($location,$rootScope,Auth,$state,$timeout,$http,$cookies,$window,$q,redirectToUrlAfterLogin){
        $rootScope.$state=$state;
        $rootScope.kendoOptions=["bold","italic","underline","justifyLeft","justifyCenter","insertUnorderedList","insertOrderedList"];
        $rootScope.ckOptions=[];
        $rootScope.resetForm=function(form){
            form.$setPristine();
            form.$setUntouched();
        }
        //hot table settings
        $rootScope.HTsettings={
            colHeaders: true,
            autoWrapCol: true,
            minSpareRows: 1,
            rowHeaders: true,
            contextMenu: ['row_above','row_below','remove_row'],
            afterInit: function(){
                this.updateSettings({formulas: true});
            }
        };
        $rootScope.removeErrorMessages=function(){
            $('.has-error').removeClass('has-error');
            $('.server-message').remove();
        }
        $rootScope.confirmMsg=function(custom_message){
            var message='Do you want to continue?';
            if(custom_message)
                message=custom_message;

            return $window.confirm(message);
        }
        $rootScope.scrollConfig={
            autoHideScrollbar: false,
            theme: 'light',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 200,
            scrollInertia: 0
        };
        $rootScope.toastMsg=function(data,msgDisplay){
            console.log('data',data);
            if(data.status){
                $rootScope.removeErrorMessages();
                $rootScope.toast('Success',data.message);
                return true;
            }else{
                $rootScope.toast('Error',data.error,'error');
                return false;
            }
        }
        $rootScope.toast=function(title,message,type,allFields){
            toastr.clear();
            $timeout(function(){
                toastr.options={
                    showMethod: 'fadeIn',
                    preventDuplicates: true,
                    timeOut: 4000
                };
                switch(type){
                    case 'error':
                        var errorCount=0;
                        $('.has-error').removeClass('has-error');
                        $('.error-message').remove();
                        jQuery.each(message,function(index,value){
                            if(message.hasOwnProperty(index)){
                                errorCount++;
                                jQuery('[name="'+index+'"]').addClass('has-error');
                                jQuery('[name="'+index+'"]').parent().append('<div class="error-message server-message">'+message[index]+'</div>');
                            }
                        })
                        if(errorCount<2){
                            jQuery.each(message,function(index,value){
                                toastr.error(value,title);
                            });
                        }else{
                            toastr.error('Invalid Form Fields',title);
                        }
                        break;
                    case 'image-error':
                        toastr.error(message,title);
                        break;
                    case 'l-error':
                        toastr.error(message,title);
                        break;
                    case 'Success':
                        $rootScope.removeErrorMessages();
                        toastr.success(message,title);
                        break;
                    case 'warning':
                        toastr.warning(message,title);
                        break;
                    case 'warning_m':
                        toastr.options.positionClass='toast-top-center';
                        toastr.warning(message,title);
                        break;
                    case 'warning_l':
                        toastr.warning(message.message,title);
                        break;
                    default:
                        if(title=='Success'){
                            $rootScope.removeErrorMessages();
                            toastr.success(message,title);
                        }else if(title=='Error'){
                            toastr.error(message,title);
                        }else{
                            toastr.info(message,title);
                        }
                        break;
                }
            },200);
        }
        $rootScope.goToState=function(data,params){
            $state.go(data,params);
        };
        $rootScope.addCustumAccessLogs = function (title, description) {
            // set up event handler on the form element
            if (ACCESS_LOG_ENABLE) {
                var IsCorrect = false;
                /*var attrData = angular.fromJson(attr.nT);*/
                var data = {
                    'event_name': title,
                    'event_description': description,
                };
                /*if($rootScope.redirectURL){
                    data.url = $rootScope.redirectURL;
                    delete $rootScope.redirectURL;
                }else {
                    data.url = $location.absUrl();
                }*/
                data.url = $location.absUrl();
                var userDetails = angular.fromJson(Auth.getFields());
                var logObj = {};
                /*var access_time=new Date(new Date().toISOString());*/
                var access_time = new Date(new Date().toISOString());
                logObj.keys = {
                    'user_id': userDetails.id_user,
                    'user_name': userDetails.first_name + ' ' + userDetails.last_name,
                    'access_time': access_time
                };
                logObj.data = data;
                logObj.log_auth_key = LOG_AUTH_KEY;

                Auth.userLogs([logObj]).then(function (result) {
                    if (result.status) {
                        //console.log('log success');
                    }
                });
            }
        };
        $rootScope.errorMessage='';
        $rootScope.loader=false;
        $rootScope.loaderOverlay=true;
        //Auth.checkLogin();
        $rootScope.$on('loggedIn',function(){
            $rootScope.site_url='#/login';
            /*if (Auth.isLoggedIn()) {*/
            var temp=angular.fromJson(Auth.getFields());
            $rootScope.userId=temp.id_user;
            $rootScope.access_token=temp.access_token;
            $rootScope.user_role_id=temp.user_role_id;
            $rootScope.user_name=temp.first_name+' '+temp.last_name;
            $rootScope.first_name=temp.first_name;
            $rootScope.email_id=temp.email_id;
            $rootScope.id_company=temp.id_company;
            $rootScope.company_logo=temp.company_logo;
            $rootScope.profile_image=temp.profile_image;
            $rootScope.company_name=temp.company_name;
            $rootScope.branch_name=temp.legal_name;
            $rootScope.role_name=temp.role_name;
            $rootScope.security_question_id=temp.security_question_id;
            $rootScope.security_question_ans=temp.security_question_ans;

            //notification
            $rootScope.password_expire_days=(temp.password_expire_days>0)?temp.password_expire_days:0;
            $rootScope.notification_password_show=(temp.notification_days>=temp.password_expire_days)?true:false;


            if($rootScope.access_token!=undefined){
                $http.defaults.headers.common['Authorization']=$rootScope.access_token;
                $http.defaults.headers.common['User']=$rootScope.userId;
            }
            if($rootScope.user_role_id=='1'){
                $rootScope.site_url='#/sa/index';
            }else if($rootScope.user_role_id=='2'){
                $rootScope.site_url='#/admin-setup';
            }else if($rootScope.user_role_id=='3'){
                $rootScope.site_url='#/user-dashboard';
            }
            $rootScope.currentDateTime=new Date();

            if($rootScope.security_question_id==null||$rootScope.password_expire_days<=0){
                $state.go("login");
            }

            /*}*/
        });
        $rootScope.datePickerOptions={
            formatYear: 'yy',
            showWeeks: false,
            shortcutPropagation: true,
            showButtonbar: false
        };
        $rootScope.dataFormate="yyyy-MM-dd"
        $rootScope.$on('loggedOut',function(){
            $cookies.remove('app');
            $state.go("login");
        });
        $rootScope.$on('$stateChangeStart',function(event,toState,toParams,fromState,fromParams){
            /*$rootScope.pageLoaded=false;*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.pageLoaded=false;
                    $rootScope.isFormChanged = false;
                } else {
                    event.preventDefault();
                }
            } else
                $rootScope.pageLoaded=false;
        });
        $rootScope.back=function(){
            $window.history.back();
            /*if ($rootScope.previousState_name) {
             $state.go($rootScope.previousState_name, $rootScope.previousState_params);
             }*/
        };
        $rootScope.$on('$stateChangeSuccess',function(ev,to,toParams,fromState,fromParams){
            // scroll view to top
            $rootScope.previousState_name=fromState.name;
            $rootScope.previousState_params=fromParams;

            $("html, body").animate({
                scrollTop: 0
            },200);
            $timeout(function(){
                $rootScope.pageLoaded=true;
            },600);
        });
        $rootScope.closeNotification=function(){
            $rootScope.notification=false;
        }
    }])
    .config(function(IdleProvider, KeepaliveProvider) {
        IdleProvider.idle(30*60); // 10 minutes idle
        IdleProvider.timeout(60); // after 30 seconds idle, time the user out
        KeepaliveProvider.interval(15*60); // 5 minute keep-alive ping

    })
    //language config
    .config(['$translateProvider',function($translateProvider){
        $translateProvider.useStaticFilesLoader({
            prefix: 'language/lag_',
            suffix: '.json'
        });
        $translateProvider.useSanitizeValueStrategy(null);
        $translateProvider.preferredLanguage('en'); //default language
    }])

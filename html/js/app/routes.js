angular.module('app')
    .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','$rootScopeProvider',function myAppConfig($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,$rootScopeProvider){
        $rootScopeProvider.digestTtl(500);
        $urlRouterProvider.otherwise('login');
        $stateProvider
        //error pages
            .state('error',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('error.404',{
                url: '/404',
                templateUrl: 'partials/error/404.html'
            })
            //common for all user
            .state('app',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['chosen']);
                    }
                }
            })
            .state('app.security',{
                url: '/security',
                controller: 'SecurityCtrl',
                templateUrl: 'partials/security.html'
            })
            .state('app.dashboard',{
                url: '/dashboard',
                resolve: {
                    authenticate: LoginAuthenticate
                }
            })
            .state('login',{
                url: '/login',
                controller: 'LoginCtrl',
                templateUrl: 'partials/login.html',
                resolve: {
                    authenticate: LoginAuthenticate
                }
            })
            .state('forgot-password',{
                url: '/forgot-password',
                controller: 'ForgotPasswordCtrl',
                templateUrl: 'partials/forgot-password.html'
            })
            .state('app.my-profile',{
                url: '/my-profile',
                controller: 'MyProfileCtrl',
                templateUrl: 'partials/my-profile.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('app.change-password',{
                url: '/change-password',
                controller: 'ChangePasswordCtrl',
                templateUrl: 'partials/change-password.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('app.change-security',{
                url: '/change-security-question',
                controller: 'ChangeSecurityCtrl',
                templateUrl: 'partials/change-security-question.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('app.login-history',{
                url: '/login-history',
                controller: 'LoginHistoryCtrl',
                templateUrl: 'partials/login-history.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('app.notification',{
                url: '/notification',
                controller: 'notificationCtrl',
                templateUrl: 'partials/notifications.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('app.edit-profile',{
                url: '/edit-profile',
                controller: 'editProfileCtrl',
                templateUrl: 'partials/edit-profile.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            //super admin - index
            .state('sa',{
                controller: '',
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['js/app/sa_controller.js']
                            }
                        ]);
                    }
                }
            })
            .state('sa.index',{
                url: '/sa/index',
                controller: 'superAdminDashboardCtrl',
                templateUrl: 'partials/super-admin/dashboard.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['googlechart']);
                    }
                }
            })

            //super admin -  Master data
            .state('sa.master',{
                template: ' <div ui-view>  </div>'
            })
            .state('sa.master.sector',{
                url: '/sa/master/sector',
                controller: 'sectorCtrl',
                templateUrl: 'partials/super-admin/master-data/sector.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.branch-type',{
                url: '/sa/master/branch-type',
                controller: 'branchTypeCtrl',
                templateUrl: 'partials/super-admin/master-data/branch-type.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.approval-role',{
                url: '/sa/master/approval-role',
                controller: 'ApprovalRoleCtrl',
                templateUrl: 'partials/super-admin/master-data/approval-role.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.plans',{
                url: '/sa/master/plans',
                controller: 'plansCtrl',
                templateUrl: 'partials/super-admin/master-data/plans.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.bank-category',{
                url: '/sa/master/bank-category',
                controller: 'bankCategoryCtrl',
                templateUrl: 'partials/super-admin/master-data/bank-category.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.country',{
                url: '/sa/master/country',
                controller: 'countryCtrl',
                templateUrl: 'partials/super-admin/master-data/country.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.currency',{
                url: '/sa/master/currency',
                controller: 'currencyCtrl',
                templateUrl: 'partials/super-admin/master-data/currency.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.risk',{
                url: '/sa/master/risk',
                controller: 'riskCtrl',
                templateUrl: 'partials/super-admin/master-data/risk.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.social',{
                url: '/sa/master/social',
                controller: 'socialCtrl',
                templateUrl: 'partials/super-admin/master-data/social.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.contact',{
                url: '/sa/master/contact',
                controller: 'contactCtrl',
                templateUrl: 'partials/super-admin/master-data/contact.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })
            .state('sa.master.language',{
                url: '/sa/master/language',
                controller: 'languageCtrl',
                templateUrl: 'partials/super-admin/master-data/language.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('masters');
                    }
                }
            })


            //super admin - create customer
            .state('sa.create-customer',{
                url: '/sa/create-customer/:customerId',
                defaultParams: {customerId: 0},
                controller: 'CreateCustomerCtrl',
                templateUrl: 'partials/super-admin/create-customer.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('sa.customer-overview',{
                url: '/sa/customer-overview/:id',
                controller: 'CustomerOverviewCtrl',
                templateUrl: 'partials/super-admin/customer-overview.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('sa.customer-overview.customer-overview-tab',{
                url: '/sa/customer-overview-tab',
                templateUrl: 'partials/super-admin/customer-tabs/over-view.html'
            })
            .state('sa.customer-overview.customer-loans',{
                url: '/customer-loans',
                templateUrl: 'partials/super-admin/customer-tabs/loans.html'
            })
            .state('sa.customer-overview.customer-branches',{
                url: '/customer-branches',
                controller: 'customerBranchesCtrl',
                templateUrl: 'partials/super-admin/customer-tabs/branches.html'
            })
            .state('sa.customer-overview.customer-login-history',{
                url: '/customer-login-history',
                templateUrl: 'partials/super-admin/customer-tabs/login-history.html'
            })
            .state('sa.customer-overview.customer-users',{
                url: '/customer-users',
                controller: 'customerUserCtrl',
                templateUrl: 'partials/super-admin/customer-tabs/users.html'
            })

            //Admin
            .state('adm',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['chosen']);
                    }
                }
            })
            .state('adm.admin-setup',{
                url: '/admin-setup',
                controller: 'AdminSetupCtrl',
                templateUrl: 'partials/admin/admin-setup.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('setup_page');
                    }
                }
            })
            .state('adm.access',{
                url: '/access?id',
                controller: 'accessCtrl',
                templateUrl: 'partials/admin/access-list.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('acces');
                    }
                }
            })
            .state('adm.audit-log',{
                url: '/audit-log',
                controller: 'auditLogCtrl',
                templateUrl: 'partials/admin/auditLog.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('audit_logs');
                    }
                }
            })
            //admin -  Roles Mnangement
            .state('roles',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'roles.module',
                                files: ['partials/admin/roles-management/roles-module.js']
                            },'chosen'
                        ]);
                    }
                }
            })
            .state('roles.roles-management',{
                url: '/roles/roles-management',
                controller: 'rolesList',
                templateUrl: 'partials/admin/roles-management/roles-management.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('roles_management');
                    }
                }
            })
            .state('roles.role-create',{
                url: '/roles/role-create',
                templateUrl: 'partials/admin/roles-management/roles-management-create.html'
            })
            .state('roles.role-edit',{
                url: '/roles/role-edit',
                templateUrl: 'partials/admin/roles-management/roles-management-edit.html'
            })
            .state('roles.role-module-create',{
                url: '/roles/module/:roleId',
                controller: 'moduleCreation',
                templateUrl: 'partials/admin/roles-management/roles-module-permission.html'
            })
            .state('org',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['chosen']);
                    }
                }
            })
            .state('org.org-structure',{
                url: '/org/organisational-structure',
                controller: 'orgStructureCtrl',
                templateUrl: 'partials/admin/org/org-structure.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('roles');
                    }
                }
            })
            .state('org.org-structure-view',{
                url: '/org/user-list',
                controller: 'orgStructureViewCtrl',
                templateUrl: 'partials/admin/org/org-structure-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('users');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore: '#loadBefore',
                                files: ['css/plugins/org-chart/jquery.orgchart.css','js/plugins/org-chart/jquery.orgchart.js']
                            }
                        ]);
                    }
                }
            })
            .state('org.user-history',{
                url: '/org/user-history/:id',
                controller: 'orgStructureHistoryCtrl',
                templateUrl: 'partials/admin/org/org-structure-history.html',
                resolve: {
                    authenticate: authenticate,

                    permission: function(crmService){
                        return crmService.setPermission('users');
                    },
                }
            })
            .state('org.user-information',{
                url: '/user-information',
                controller: 'UserInformationCtrl',
                templateUrl: 'partials/admin/user-information.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('org.create-user',{
                url: '/create-user',
                controller: 'UserCreationCtrl',
                templateUrl: 'partials/admin/org-user/create-user.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('org.edit-user',{
                url: '/edit-user/:id',
                controller: 'EditCompanyUserCtrl',
                templateUrl: 'partials/admin/org-user/create-user.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('entity',{
                controller: '',
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['chosen']);
                    }
                }
            })
            .state('entity.branch-structure',{
                url: '/entity/branch-structure',
                controller: 'BranchStructureCtrl',
                templateUrl: 'partials/admin/entity-structure/branch-structure.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('entity_structure');
                    }
                }
            })
            .state('entity.branch-structure-view',{
                url: '/entity/branch-structure-view',
                controller: 'BranchStructureViewCtrl',
                templateUrl: 'partials/admin/entity-structure/branch-structure-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('entity_structure');
                    }
                }
            })
            .state('entity.create-branch',{
                url: '/entity/create-branch',
                controller: 'createBranchCtrl',
                templateUrl: 'partials/admin/entity-structure/create-branch.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('entity.edit-branch',{
                url: '/entity/edit-branch/:id',
                controller: 'editBranchCtrl',
                templateUrl: 'partials/admin/entity-structure/create-branch.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('approval',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('approval.approval-structure',{
                url: '/approval/approval-structure',
                controller: 'approvalStructureCtrl',
                templateUrl: 'partials/admin/approval/approval-structure.html',
                data: {
                    approval_type: 1,
                    title: "Approval Structure"
                },
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('approval.approval-structure-view',{
                url: '/approval/approval-structure-view/:structure_id',
                controller: 'approvalStructureViewCtrl',
                templateUrl: 'partials/admin/approval/approval-structure-view.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('preapproval',{
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    checkUrl: function(crmService){
                        return crmService.checkUrlPermission(2);
                    }
                }
            })
            .state('preapproval.pre-approval-structure',{
                url: '/pre-approval/pre-approval-structure',
                controller: 'approvalStructureCtrl',
                templateUrl: 'partials/admin/pre-approval/approval-structure.html',
                data: {
                    approval_type: 2,
                    title: "Pre Approval Structure"
                },
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('preapproval.pre-approval-structure-view',{
                url: '/pre-approval/pre-approval-structure-view/:structure_id',
                controller: 'approvalStructureViewCtrl',
                templateUrl: 'partials/admin/pre-approval/approval-structure-view.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('product',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['multiselect']);
                    }
                }
            })
            .state('product.list',{
                url: '/product/list',
                controller: 'productCtrl',
                templateUrl: 'partials/admin/product/product.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('products_config');
                    }
                }
            })
            .state('product.tabs',{
                url: '/product/tabs/:productId',
                templateUrl: 'partials/admin/product/product-tabs.html',
                controller: function($rootScope,$scope,$state,$stateParams,decodeFilter,masterService){
                    $rootScope.product_id=decodeFilter($stateParams.productId);
                    //$state.go('product.tabs.product-data');
                    masterService.getProductById({
                        'id_product': $rootScope.product_id,
                        'company_id': $rootScope.id_company
                    }).then(function(result){
                        $scope.productName=result.data.data[0].product_name;
                    });
                },
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['ui.sortable','chosen','ngDragDrop']);
                    }
                }
            })
            .state('product.tabs.product-data',{
                url: '/product-data',
                templateUrl: 'partials/admin/product/product-data.html'
            })
            .state('product.tabs.product-conditions',{
                url: '/product-conditions',
                controller: 'productConditionsCtrl',
                templateUrl: 'partials/admin/product/product-conditions.html'
            })
            .state('product.tabs.risk-assessment',{
                url: '/risk-assessment',
                templateUrl: 'partials/admin/product/product-risk-assessment.html'
            })
            .state('product.tabs.summary-recommendation',{
                url: '/summary-recommendation',
                controller: 'productSummaryRecommendationsCtrl',
                templateUrl: 'partials/admin/product/product-summary-recommendation.html'
            })
            .state('product.tabs.disbursement-checklist',{
                url: '/disbursement-checklist',
                controller: 'disbursementChecklistCtrl',
                templateUrl: 'partials/admin/product/product-disbursement-checklist.html',
                data: {
                    id_assessment_question_type: 6,
                    title: "Pre Disbursement Checklist"
                }
            })
            .state('product.tabs.restrictions',{
                url: '/restrictions',
                controller: 'disbursementChecklistCtrl',
                templateUrl: 'partials/admin/product/product-restrictions.html',
                data: {
                    id_assessment_question_type: 4,
                    title: "Pre Disbursement Checklist"
                }
            })
            .state('product.tabs.product-limits-approvals',{
                url: '/product/tabs/product-limits-approvals',
                controller: 'productLimitsApprovalsCtrl',
                templateUrl: 'partials/admin/product/product-limits-approvals.html'
            })
            .state('product.tabs.pre-approval',{
                url: '/product/tabs/pre-approval',
                controller: 'productPreApprovalsCtrl',
                templateUrl: 'partials/admin/product/pre-approval.html'
            })
            .state('product.tabs.workflow-info',{
                url : '/workflow',
                controller : 'productWorkFlowCtrl',
                templateUrl : 'partials/admin/product/product-info.html',
                resolve : {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'chosen'
                        ]);
                    }
                }
            })
            .state('assessment',{
                controller: '',
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['ui.sortable','ngDragDrop']);
                    }
                }
            })
            .state('assessment.assessment-questions',{
                url: '/assessment-questions',
                controller: 'assessmentCtrl',
                templateUrl: 'partials/admin/assessment/assessment-questions.html',
                data: {
                    id_assessment_question_type: 1,
                    title: "Assessment Questions"
                },
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('assessment_questions');
                    }
                }
            })
            .state('assessment.business-plan',{
                url: '/business-plan',
                controller: 'assessmentCtrl',
                templateUrl: 'partials/admin/assessment/assessment-questions.html',
                data: {
                    id_assessment_question_type: 2,
                    title: "Business Plan"
                },
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('company_business_plan');
                    }
                }
            })
            .state('assessment.assessment-questions-view',{
                url: '/assessment-questions-view',
                //controller: 'assessmentCtrl',
                templateUrl: 'partials/admin/assessment/assessment-questions-view.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('adm.risk-assessment',{
                url: '/risk-assessment',
                controller: 'admRiskAssessmentCtrl',
                templateUrl: 'partials/admin/risk-assessment/risk-assessment.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('expert_rating_model');
                    }
                }
            })
            .state('adm.sector-limit',{
                url: '/sector-limit',
                controller: 'sectorLimitCtrl',
                templateUrl: 'partials/admin/sector-limit.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('sector_limit');
                    }
                }
            })
            .state('operating-currency',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('operating-currency.tab',{
                url: '/operating-currency/tab',
                templateUrl: 'partials/admin/operating-currency/tab.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('operating_currency');
                    }
                }
            })
            .state('operating-currency.tab.primary-currency',{
                url: '/primary-currency',
                controller: 'primaryCurrencyCtrl',
                templateUrl: 'partials/admin/operating-currency/primary-currency.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('operating_currency');
                    }
                }
            })
            .state('operating-currency.tab.operating-currency',{
                url: '/operating-currency',
                controller: 'operatingCurrencyCtrl',
                templateUrl: 'partials/admin/operating-currency/operating-currency.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('operating_currency');
                    }
                }
            })
            .state('operating-currency.tab.exchange-rate',{
                url: '/exchange-rate',
                controller: 'exchangeRateCtrl',
                templateUrl: 'partials/admin/operating-currency/exchange-rate.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('operating_currency');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['googlechart']);
                    }
                }
            })
            .state('operating-currency.tab.currency-rates',{
                url: '/currency-rates',
                controller: 'exchangeRateCtrl',
                templateUrl: 'partials/admin/operating-currency/currency-rates.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('knowledge-management',{
                controller: '',
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                /*  name: 'ui.knob',*/
                                files: ['js/plugins/highcharts/highcharts.js']
                            },'chosen'
                        ])
                    }
                }
            })
            .state('knowledge-management.knowledge-dashboard',{
                url: '/knowledge-management/dashboard',
                controller: 'knowledgeManagementCtrl',
                templateUrl: 'partials/admin/knowledge-management/knowledge-management-list.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('Knowledge_management');
                    }
                }
            })
            .state('knowledge-management.view',{
                url: '/knowledge-management/view',
                controller: 'searchKnowledgeManagementCtrl',
                templateUrl: 'partials/admin/knowledge-management/knowledge-management-all.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('adm.business-assessment',{
                url: '/business-assessment',
                controller: 'businessAssessmentCtrl',
                templateUrl: 'partials/admin/business-assessment/business-assessment.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('assessment_config');
                    }
                }
            })
            .state('knowledge-management.item-view',{
                url: '/knowledge-management/item-view/:id',
                controller: 'knowledgeManagementViewCtrl',
                templateUrl: 'partials/admin/knowledge-management/knowledge-management-view.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('ratios-management',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'rationmanagement',
                            files: ['partials/admin/ratios-management/ratiosManagementCtrl.js']
                        },'chosen']);
                    }
                }
            })
            .state('ratios-management.list',{
                url: '/ratios-management',
                templateUrl: 'partials/admin/ratios-management/ratio-list.html',
                controller: 'ratiosManagementListCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('ratios_management');
                    }
                }
            })

            .state('crm-facility',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'crm.facility',
                            files: ['partials/crm/facility/crmFacilityCtrl.js']
                        },'chosen']);
                    }
                }
            })
            .state('crm-facility.list',{
                url: '/crm/facility/list',
                templateUrl: 'partials/crm/facility/facility-list.html',
                controller: 'crmFacilityListCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('facility_list');
                    }
                }
            })


            .state('crm-activity',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'crm.activity',
                            files: ['partials/crm/activity/crmActivityCtrl.js']
                        },'chosen','task']);
                    }
                }
            })
            .state('crm-activity.list',{
                url: '/task/list?taskId',
                reloadOnSearch: false,
                templateUrl: 'partials/crm/activity/activity-task.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('task_list');
                    }
                }
            })

            .state('crm-meeting',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'crm.meeting',
                            files: ['partials/crm/meeting/crmMeetingCtrl.js']
                        },'chosen','meeting']);
                    }
                }
            })
            .state('crm-meeting.list',{
                url: '/meeting/list?meetingId',
                reloadOnSearch: false,
                templateUrl: 'partials/crm/activity/activity-meeting.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('meeting_list');
                    }
                }
            })

            .state('crm-covenant',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            files: ['partials/crm/covenant/crmCovenantCtrl.js']
                        },'chosen']);
                    }
                }
            })
            .state('crm-covenant.list',{
                url: '/covenant/list?covenantId',
                reloadOnSearch: false,
                templateUrl: 'partials/crm/covenant/covenant-list.html',
                controller: 'crmCovenantListCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('covenant_list');
                    }
                }
            })
            .state('collateral',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'collateralCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'collateral',
                            files: ['partials/admin/collateral/collateralCtrl.js']
                        },'chosen']);
                    }
                }
            })
            .state('collateral.collateral-dashboard',{
                url: '/collateral-dashboard',
                templateUrl: 'partials/admin/collateral/collateral-dashboard.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('collateral.collateral-list',{
                url: '/collateral/list',
                templateUrl: 'partials/admin/collateral/collateral-list.html',
                controller: 'collateralListCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('collateral_management');
                    }
                }
            })
            .state('collateral.edit',{
                url: '/collateral/edit/:collateralId',
                templateUrl: 'partials/admin/collateral/collateral-edit.html',
                controller: 'collateralEditCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('collateral.add-collateral',{
                url: '/collateral/config',
                controller: 'collateralConfigCtrl',
                templateUrl: 'partials/admin/collateral/collateral-config.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['npMap']);
                    }
                }
            })
            .state('covenant',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'covenantCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            'chosen','ngTagsInput',
                            {
                                name: 'app',
                                files: ['partials/admin/covenant/covenantCtrl.js']
                            }
                        ]);
                    }
                }
            })
            .state('covenant.list',{
                url: '/covenant-management/list',
                controller: 'covenantListCtrl',
                templateUrl: 'partials/admin/covenant/covenant-list.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('covenant_management');
                    }
                }

            })
            .state('covenant.positive',{
                url: '/covenant/positive',
                template: '<h3 class="fixed content-heading">Positive Covenant</h3><covenant key="positive_covenant"></covenant>',
            })
            .state('covenant.negative',{
                url: '/covenant/negative',
                template: '<h3 class="fixed content-heading">Negative Covenant</h3><covenant key="negative_covenant"></covenant>',
            })
            .state('covenant.representations',{
                url: '/covenant/representations',
                template: '<h3 class="fixed content-heading">Representations Warranties</h3><covenant key="representations_warranties"></covenant>',
            })
            .state('covenant.events',{
                url: '/covenant/events-of-defaults',
                template: '<h3 class="fixed content-heading">Events Of Defaults</h3><covenant key="events_of_defaults"></covenant>',
            })
            .state('facility',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'facilityCtrl',
                resolve: {
                    authenticate: authenticate,
                    checkUrl: function(crmService){
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/admin/facility/facilityCtrl.js']
                            },'chosen','ckeditor'
                        ]);
                    }
                }
            })
            .state('facility.type',{
                url: '/facility/type',
                controller: 'facilityTypeCtrl',
                templateUrl: 'partials/admin/facility/facility-type.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('facility_setup');
                    }
                }
            })
            .state('facility.view',{
                url: '/facility/view/:facilityId',
                controller: 'facilityViewCtrl',
                templateUrl: 'partials/admin/facility/facility-view.html'
            })
            .state('facility.view.general',{
                url: '/general',
                controller: 'facilityGeneralTabCtrl',
                templateUrl: 'partials/admin/facility/tabs/general.html'
            })
            .state('facility.view.price',{
                url: '/price',
                controller: 'facilityPriceTabCtrl',
                templateUrl: 'partials/admin/facility/tabs/price.html'
            })
            .state('facility.view.termconditions',{
                url: '/term-conditions',
                controller: 'facilityTermConditionsCtrl',
                templateUrl: 'partials/admin/facility/tabs/terms-conditions.html'
            })
            //master Module
            .state('master-data',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/admin/master-data/masterCtrl.js']
                            }
                        ]);
                    }
                }
            })
            .state('master-data.other',{
                url: '/master-data/other',
                controller: 'otherCtrl',
                templateUrl: 'partials/admin/master-data/other.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('other');
                    }
                }
            })
            .state('master-data.currency',{
                url: '/master-data/currency',
                controller: 'currencyCtrl',
                templateUrl: 'partials/admin/master-data/currency.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('currency');
                    }
                }
            })
            .state('master-data.sector',{
                url: '/master-data/sector',
                controller: 'sectorCtrl',
                templateUrl: 'partials/admin/master-data/sector.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('sector');
                    }
                }
            })
            .state('master-data.branch-type',{
                url: '/master-data/branch-type',
                controller: 'branchTypeCtrl',
                templateUrl: 'partials/admin/master-data/branch-type.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('branch_type');
                    }
                }
            })
            .state('master-data.approval-role',{
                url: '/master-data/approval-role',
                controller: 'ApprovalRoleCtrl',
                templateUrl: 'partials/admin/master-data/approval-role.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('approval_role');
                    }
                }
            })
            .state('master-data.bank-category',{
                url: '/master-data/bank-category',
                controller: 'bankCategoryCtrl',
                templateUrl: 'partials/admin/master-data/bank-category.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('bank_category');
                    }
                }
            })
            .state('master-data.country',{
                url: '/master-data/country',
                controller: 'countryCtrl',
                templateUrl: 'partials/admin/master-data/country.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('country');
                    }
                }
            })
            .state('master-data.risk',{
                url: '/master-data/risk',
                controller: 'riskCtrl',
                templateUrl: 'partials/admin/master-data/risk.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('risk');
                    }
                }
            })
            .state('master-data.social',{
                url: '/master-data/social',
                controller: 'socialCtrl',
                templateUrl: 'partials/admin/master-data/social.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('social');
                    }
                }
            })
            .state('master-data.contact',{
                url: '/master-data/contact',
                controller: 'contactCtrl',
                templateUrl: 'partials/admin/master-data/contact.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('master_contact');
                    }
                }
            })
            .state('master-data.language',{
                url: '/master-data/language',
                controller: 'languageCtrl',
                templateUrl: 'partials/admin/master-data/language.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('language');
                    }
                }
            })



            //User Role
            //User - CRM states
            //contact Module
            .state('contact',{
                templateUrl: 'partials/layout.html',
                controller: 'contactCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'app',
                            files: ['partials/crm/contact/contactCtrl.js','partials/component/exposure/exposure-directive.js']
                        },'attachment','ngHandsontable','ckeditor','ngMap','utility']);
                    }
                }
            })
            .state('contact.contact-dashboard',{
                url: '/contact/list',
                controller: 'contactListCtrl',
                templateUrl: 'partials/crm/contact/contact-list.html',
                params: {module_key: 'contact_list'},
                resolve: {
                    permission: function(crmService){
                        return crmService.setPermission('contact_list');
                    }
                }
            })
            .state('contact.contact-form-list',{
                url: '/contact/view/:id',
                controller: 'contactViewCtrl',
                templateUrl: 'partials/crm/contact/contact-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('contact_view');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                /*  name: 'ui.knob',*/
                                files: ['js/plugins/highcharts/highcharts.js']
                            },'touch.points','attachment','task','notifications','meeting','chosen'
                        ])
                    },
                }
            })
            .state('contact.contact-form-edit',{
                url: '/contact/form-view/:id',
                controller: 'contactFormViewCtrl',
                templateUrl: 'partials/crm/contact/contact-form-view.html',
                resolve: {
                    authenticate: authenticate,
                    checkCookies: function(crmService,$state,$rootScope,$timeout,$q,$stateParams){
                        var deferred=$q.defer();
                        $timeout(function(){
                            if($rootScope.contactParames==undefined){
                                $state.go("contact.contact-form-list",{'id': $stateParams.id});
                                deferred.reject();
                            }else{
                                deferred.resolve();
                            }
                        })
                        return deferred.promise;
                    }
                }
            })
            .state('contact.contact-complete-view',{
                url: '/contact/contact-complete-view/:id',
                controller: 'ContactCompleteViewCtrl',
                templateUrl: 'partials/crm/contact/contact-complete-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('contact_complete_view');
                    }
                }
            })
            //company Module
            .state('company',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'companyCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'app',
                            files: ['partials/crm/company/companyCtrl.js','partials/component/exposure/exposure-directive.js']
                        },'attachment','ckeditor','utility'])
                    }
                }
            })
            .state('company.company-dashboard',{
                url: '/company/list',
                controller: 'companyListCtrl',
                templateUrl: 'partials/crm/company/company-list.html',
                resolve: {
                    permission: function(crmService){
                        return crmService.setPermission('company_list');
                    }
                }
            })
            .state('company.company-creation',{
                url: '/company/view/:id',
                controller: 'CompanyCreationCtrl',
                templateUrl: 'partials/crm/company/company-view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            'touch.points','task','notifications','meeting','chosen','ngMap'
                        ])
                    },
                    permission: function(crmService){
                        return crmService.setPermission('company_view');
                    }
                }
            })
            .state('company.business-assessment',{
                url: '/company/complete-view/:id',
                controller: 'CompanyPriviewCtrl',
                templateUrl: 'partials/crm/company/company-complete-view.html',
                resolve: {
                    permission: function(crmService){
                        return crmService.setPermission('company_complete_view');
                    }
                }
            })
            .state('company.business-keydata',{
                url: '/company/form-view/:id',
                controller: 'companyFormViewCtrl',
                templateUrl: 'partials/crm/company/company-form-view.html',
                resolve: {
                    checkCookies: function(crmService,$state,$rootScope,$timeout,$q,$stateParams){
                        var deferred=$q.defer();
                        $timeout(function(){
                            if($rootScope.companyParames==undefined){
                                $state.go("company.company-creation",{'id': $stateParams.id});
                                deferred.reject();
                            }else{
                                deferred.resolve();
                            }
                        })
                        return deferred.promise;
                    }
                }
            })

            //Project Module
            .state('project',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'projectCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/crm/project/projectCtrl.js']
                            },'attachment','meeting','chosen']);
                    }
                }
            })
            .state('project.project-dashboard',{
                url: '/project/list',
                controller: 'ProjectListCtrl',
                templateUrl: 'partials/crm/project/project-list.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('project_list');
                    }
                }
            })
            .state('project.project-view',{
                url: '/project/view/:id',
                controller: 'ProjectViewCtrl',
                templateUrl: 'partials/crm/project/project-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('project_view');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            'touch.points','attachment','task','notifications','crm.covenant'
                        ])
                    }
                }
            })
            .state('project.project-form-view',{
                url: '/project/form-view/:projectId/:stageId/:formId?selectedVersion&currentVersion',
                controller: 'ProjectFormViewCtrl',
                templateUrl: 'partials/crm/project/project-from-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('stages');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['ckeditor','googlechart'])
                    }
                }
            })
            .state('project.project-overview',{
                url: '/project/project-overview/:projectId/:stageInfoId/:stageId',
                controller: 'projectPreviewCtrl',
                templateUrl: 'partials/crm/project/project-stage-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('stages');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            'ngTagsInput'
                        ])
                    }
                }
            })

            .state('project.project-facility-view',{
                url: '/project/project-facility-view/:projectId/:facilityId/:stageId',
                controller: 'projectFacilityView',
                templateUrl: 'partials/crm/project/forms/facilities-view.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('project_list');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['ckeditor']);
                    }
                }
            })
            .state('project.covenant',{
                url: '/project/project-covenant/:projectId',
                controller: function($rootScope,decodeFilter,$stateParams){
                    $rootScope.currentProjectViewId=decodeFilter($stateParams.projectId);
                },
                templateUrl: 'partials/crm/project/project-covenant.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('facility_list');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['ckeditor','crm.covenant']);
                    }
                }
            })
            .state('project.terms-conditions',{
                url: '/project/terms-conditions/:id',
                controller: 'ProjectListCtrl2',
                templateUrl: 'partials/crm/project/pre-disbursement-checklist.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('project.project-approval-level.status-flow',{
                url: '/status-flow',
                templateUrl: 'partials/crm/project/project-approval-level/status-flow.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            .state('project.project-approval-level.facility-table',{
                url: '/facility-table',
                templateUrl: 'partials/crm/project/facility_com/facilities-approval-tableview.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            //collateral Module
            .state('crm-collateral',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'crmCollateralCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([{
                            name: 'crm.collateral',
                            files: ['partials/crm/collateral/crmCollateralCtrl.js']
                        },'chosen','attachment']);
                    }
                }
            })
            .state('crm-collateral.list',{
                url: '/collateral/collateral-list',
                templateUrl: 'partials/crm/collateral/collateral-list.html',
                controller: 'crmCollateralListCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('collateral_list');
                    }
                }
            })
            .state('crm-collateral.collateral-view',{
                url: '/collateral/collateral-view/:collateralId/:collateralTypeId',
                templateUrl: 'partials/crm/collateral/collateral-view.html',
                controller: 'collateralViewCtrl',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('collateral_list');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['crm.covenant','ckeditor']);
                    }
                }
            })

            //deligence Module
            .state('deligence',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('deligence.pre-due-deligence',{
                url: '/deligence/pre-due-deligence',
                templateUrl: 'partials/crm/deligence/pre-due-deligence.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('deligence.deligence',{
                url: '/deligence/deligence',
                templateUrl: 'partials/crm/deligence/deligence.html',
                resolve: {
                    authenticate: authenticate
                }
            })

            //loan Module
            .state('loan',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('loan.loan-monitoring',{
                url: '/loan/loan-monitoring',
                templateUrl: 'partials/crm/loan/loan-monitoring.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('loan.payment-history',{
                url: '/loan/payment-history',
                templateUrl: 'partials/crm/loan/payment-history.html',
                resolve: {
                    authenticate: authenticate
                }
            })
            //Dashboard
            //https://code.highcharts.com/modules/funnel.js
            .state('dashboard',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/crm/dashboard/CrmDashboard.js']
                            },{
                                serie: true,
                                files: ['js/plugins/highcharts/highcharts.js']
                            }
                            ,'googlechart','ngMap']);
                    }
                }
            })
            /*.state('dashboard.sector', {
             url: '/dashboard/sector-dashboard',
             templateUrl: 'partials/crm/dashboard/sector-dashboard.html',
             controller: 'sectorDashboard'
             })*/
            .state('dashboard.loan-officer',{
                url: '/user-dashboard',
                templateUrl: 'partials/crm/dashboard/loan-officer-dashboard.html',
                controller: 'loanOfficerDashboard',
                resolve: {
                    permission: function(crmService){
                        return crmService.setPermission('dashboard');
                    }
                }
            })
            /*.state('dashboard.multiple-sector', {
             url: '/dashboard/multiple-sector-dashboard',
             templateUrl: 'partials/crm/dashboard/multiple-sector-dashboard.html',
             controller: 'multipleSectorDashboard'
             })*/
            //Mis uploads
            .state('mis-upload',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/admin/mis/misUploadCtrl.js']
                            }
                        ]);
                    }
                }
            })
            .state('mis-upload.disbursement',{
                url: '/upload/disbursement',
                controller: 'misUploadCtrl',
                templateUrl: 'partials/admin/mis/uploadExcel.html',
                params: {type: 'disbursement'},
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('disbursement_plan');
                    }
                }
            })
            .state('mis-upload.repayment',{
                url: '/upload/repayment',
                controller: 'misUploadCtrl',
                templateUrl: 'partials/admin/mis/uploadExcel.html',
                params: {type: 'repayment'},
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('repayment_plan');
                    }
                }
            })
            .state('mis-upload.collection',{
                url: '/upload/collection',
                controller: 'misUploadCtrl',
                templateUrl: 'partials/admin/mis/uploadExcel.html',
                params: {type: 'collection'},
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('collection_plan');
                    }
                }
            })
            .state('mis-upload.aging',{
                url: '/upload/aging',
                controller: 'misUploadCtrl',
                templateUrl: 'partials/admin/mis/uploadAging.html',
                params: {type: 'aging'},
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('aging');
                    }
                }
            })
            .state('mis-upload.outstanding',{
                url: '/upload/outstanding',
                controller: 'misUploadCtrl',
                templateUrl: 'partials/admin/mis/outstanding.html',
                params: {type: 'outstanding'},
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('outstanding');
                    }
                }
            })
            //HTML
            .state('html',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name: 'app',
                                files: ['partials/html/html_controller.js']
                            }]);
                    }
                }
            })
            .state('html.facilitate',{
                url: '/html/facilitate',
                templateUrl: 'partials/html/facilitate/facilitate.html'
            })
            .state('html.facilitate-setup',{
                url: '/html/facilitate-setup',
                templateUrl: 'partials/html/facilitate/facilitate-setup.html'
            })
            .state('html.facilitate-info',{
                url: '/html/facilitate-info',
                templateUrl: 'partials/html/facilitate/facilitate-info2.html'
            })
            .state('html.facilitate-total-view',{
                url: '/html/facilitate-total-view',
                templateUrl: 'partials/html/facilitate/facilitate-total-view.html'
            })
            .state('html.client-list',{
                url: '/html/client-list',
                templateUrl: 'partials/html/facilitate/client-list.html'
            })
            .state('html.sector-dashboard',{
                url: '/html/sector-dashboard',
                templateUrl: 'partials/html/facilitate/sector-dashboard.html'
            })
            .state('html.loan-officer-dashboard',{
                url: '/html/loan-officer-dashboard',
                templateUrl: 'partials/html/facilitate/loan-officer-dashboard.html'
            })
            .state('html.concept-view',{
                url: '/html/concept-view',
                templateUrl: 'partials/html/facilitate/concept-view.html'
            })
            .state('html.concept-note',{
                url: '/html/concept-note',
                templateUrl: 'partials/html/facilitate/concept-note.html'
            })
            .state('html.pre-appraisal',{
                url: '/html/pre-appraisal',
                templateUrl: 'partials/html/facilitate/pre-appraisal.html'
            })
            .state('html.indicative-term',{
                url: '/html/indicative-term',
                templateUrl: 'partials/html/facilitate/indicative-term.html'
            })
            .state('html.datailed-appraisal-report',{
                url: '/html/datailed-appraisal-report',
                templateUrl: 'partials/html/facilitate/datailed-appraisal-report.html'
            })
            .state('html.facility-term-sheet',{
                url: '/html/facility-term-sheet',
                templateUrl: 'partials/html/facilitate/facility-term-sheet.html'
            })
            .state('html.concept-loan-appraisals',{
                url: '/html/concept-loan-appraisals',
                templateUrl: 'partials/html/facilitate/concept-loan-appraisals.html'
            })
            .state('html.covenant',{
                url: '/html/covenant',
                templateUrl: 'partials/html/covenant/covenant.html',
                controller: 'crmCovenantCtrl'
            })
            .state('html.notifications',{
                url: '/html/notifications',
                templateUrl: 'partials/html/covenant/notifications.html',
                controller: 'crmCovenantCtrl'
            })
            .state('html.collateral-types',{
                url: '/html/collateral-types',
                templateUrl: 'partials/html/collateral-types/collateral-types.html',
                controller: 'crmCovenantCtrl'
            })
            .state('html.collateral-type-view',{
                url: '/html/collateral-type-view',
                templateUrl: 'partials/html/collateral-types/collateral-type-view.html'
            })
            .state('html.Collaterals-user',{
                url: '/html/Collaterals-user',
                templateUrl: 'partials/html/collaterals-user/collaterals-user.html'
            })
            .state('html.Collaterals-info-users',{
                url: '/html/Collaterals-info-users',
                templateUrl: 'partials/html/collaterals-user/collaterals-info-users.html'
            })
            .state('html.concept',{
                url: '/html/project-progress-levels',
                templateUrl: 'partials/html/concept/project-progress-levels.html'
            })
            .state('html.reports',{
                url: '/html/reports',
                templateUrl: 'partials/html/reports/report.html',
                controller: 'reportsCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                                {
                                    serie: true,
                                    files: ['js/plugins/highcharts/highcharts.js','js/plugins/highcharts/highcharts_drilldown.js']
                                },'attachment','ui-rangeSlider','googlechart'
                            ]
                        )
                    }
                }
            })
            .state('html.new-report',{
                url: '/html/reports/new-report',
                templateUrl: 'partials/html/reports/new-report.html',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                                {
                                    serie: true,
                                    files: ['js/plugins/highcharts/highcharts.js','js/plugins/highcharts/highcharts_drilldown.js']
                                },'attachment','googlechart'
                            ]
                        )
                    }
                }
            })
            .state('html.new-report-angular',{
                url: '/html/reports/new-report-angular',
                templateUrl: 'partials/html/reports/new-report-angular.html',
                controller: 'newReportsCtrl',
            })
            .state('html.sector-limits',{
                url: '/html/reports/sector-limits',
                templateUrl: 'partials/html/reports/sector-limits.html',
            })
            .state('risk',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'riskCtrl',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(
                            {
                                serie: true,
                                name: 'app',
                                files: ['partials/crm/risk/riskCtrl.js']
                            });
                    }
                }
            })
            .state('risk.risk-list',{
                url: '/risk-report',
                controller: 'riskListCtrl',
                templateUrl: 'partials/crm/risk/risk-list.html',
                resolve: {
                    authenticate: authenticate,
                    permission: function(crmService){
                        return crmService.setPermission('risk_list');
                    },
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load(['chosen']);
                    }
                }
            })
            .state('reports',{
                abstract: true,
                templateUrl: 'partials/layout.html',
                controller: 'homeReport',
                resolve: {
                    authenticate: authenticate,
                    loadPlugin: function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                serie: true,
                                name: 'app',
                                files: ['partials/crm/reports/reportsCtrl.js','js/plugins/highcharts/highcharts.js','js/plugins/highcharts/highcharts_drilldown.js']
                            },'attachment','googlechart']);
                    }
                }
            })
            .state('reports.summary',{
                url: '/reports/summary',
                templateUrl: 'partials/crm/reports/summary.html',
                controller: 'summaryCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('reports.assessment-country',{
                url: '/reports/assessment-country',
                templateUrl: 'partials/crm/reports/assessment-country.html',
                controller: 'assessmentCountryCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('reports.assessment-product',{
                url: '/reports/assessment-product',
                templateUrl: 'partials/crm/reports/assessment-product.html',
                controller: 'assessmentProductCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('reports.assessment-sector',{
                url: '/reports/assessment-sector',
                templateUrl: 'partials/crm/reports/assessment-sector.html',
                controller: 'assessmentSectorCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('reports.borrower-list',{
                url: '/reports/value-at-risk',
                templateUrl: 'partials/crm/reports/value-at-risk.html',
                controller: 'valueAtRiskCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })
            .state('reports.sector-limits',{
                url: '/reports/sector-limits',
                templateUrl: 'partials/crm/reports/sector-limits.html',
                controller: 'sectorLimitsCtrl',
                resolve: {
                    authenticate: authenticate
                }
            })


        function authenticate($q,Auth,$state,$timeout){
            if(Auth.checkLogin()){
                return $q.when()
            }else{
                $timeout(function(){
                    $state.go('login')
                })
                return $q.reject()
            }
        }

        function LoginAuthenticate($q,Auth,$state,$timeout,$rootScope,redirectToUrlAfterLogin,$location){
            $rootScope.site_url='#/';
            var defer=$q.defer();
            if(Auth.isLoggedIn()){
                var temp=angular.fromJson(Auth.getFields());
                $rootScope.user_role_id=temp.user_role_id;
                $rootScope.security_question_id=temp.security_question_id;
                $rootScope.security_question_ans=temp.security_question_ans;

                if($rootScope.notification_password_show){
                    $rootScope.notification=true;
                    if($rootScope.password_expire_days==0){
                        $rootScope.notificationMsg='Your password has been expired. <a href="#/change-password" class="notification-link"><b>Click here</b></a> to update';
                    }else{
                        //$rootScope.notificationMsg = 'password will expire in - '+ $rootScope.password_expire_days  +' days update &nbsp; <a href="#/change-password"> here </a>';
                        $rootScope.notificationMsg='Your password expires in <b>'+$rootScope.password_expire_days+'</b> days. <a href="#/change-password" class="notification-link"><b>Click here</b></a> to update';
                    }
                }

                $timeout(function(){
                    $rootScope.closeNotification();
                    if($rootScope.security_question_id==null||$rootScope.password_expire_days<=0){
                        if($rootScope.security_question_id==null){
                            $rootScope.notification=true;
                            $rootScope.dontShowClose=true;
                            /*$rootScope.notificationMsg='It is manadatory to update your password for the first log in.';*/
                            $rootScope.notificationMsg='Welcome '+$rootScope.user_name+', Update your Password and Security Question for secured access.';
                            $state.go('app.security');
                        }else{
                            $state.go('app.change-password');
                        }
                        defer.reject();
                    }else if($rootScope.user_role_id=='1'){
                        $rootScope.site_url='#/sa/index';
                        $state.go('sa.index');
                        defer.reject();
                    }else if($rootScope.user_role_id=='2'){
                        if(!redirectToUrlAfterLogin.direct_entry&& !redirectToUrlAfterLogin.first_time){
                            Auth.redirectToAttemptedUrl();
                        }else{
                            $rootScope.site_url='#/admin-setup';
                            $state.go('adm.admin-setup');
                            defer.reject();
                        }
                    }else if($rootScope.user_role_id=='3'){
                        if(!redirectToUrlAfterLogin.direct_entry&& !redirectToUrlAfterLogin.first_time){
                            Auth.redirectToAttemptedUrl();
                        }else{
                            $rootScope.site_url='#/user-dashboard';
                            $state.go('dashboard.loan-officer');
                            defer.reject();
                        }

                    }
                })
                return defer.promise;
            }
        }
    }]);


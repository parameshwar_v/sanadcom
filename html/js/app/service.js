/**
 * A service that returns some data.
 */
'use strict';
angular.module('app')
    .factory('onlineStatus', ["$window", "$rootScope", function ($window, $rootScope) {
        var onlineStatus = {};
        onlineStatus.onLine = $window.navigator.onLine;
        onlineStatus.isOnline = function () {
            return onlineStatus.onLine;
        };
        $window.addEventListener("online", function () {
            onlineStatus.onLine = true;
            $rootScope.$digest();
        }, true);
        $window.addEventListener("offline", function () {
            onlineStatus.onLine = false;
            $rootScope.$digest();
        }, true);
        return onlineStatus;
    }])
    .factory('httpLoader', function ($rootScope) {
        var pendingReqs = {};
        return {
            addPendingReq: function (config) {
                if (config.hasOwnProperty('loaderId')) {
                    if (config.loaderId)
                        $('#' + config.loaderId).fadeIn();
                } else {
                    pendingReqs[config.url] = true;
                }
            },
            subtractPendingReq: function (config) {
                if (config && config.hasOwnProperty('loaderId')) {
                    if (config.loaderId)
                        $('#' + config.loaderId).fadeOut();
                } else {
                    if (config) {
                        delete pendingReqs[config.url];
                    } else {
                        pendingReqs = {};
                    }
                }
            },
            getPendingReqs: function () {
                return sizeOf(pendingReqs);
            }
        }
        function sizeOf(obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    size++;
                }
            }
            return size;
        }

    })
    .factory('errorInterceptor', function ($q, $rootScope, httpLoader) {
        return {
            request: function (config) {
                // $('#loader').show();
                httpLoader.addPendingReq(config);

                if (config.method == "POST" && ACCESS_LOG_ENABLE && !config.hasOwnProperty('accessLog')) {
                    //console.log(config.data[0].log_auth_key);
                    var auditLog = {};
                    auditLog.updated_by = config.headers.User;
                    auditLog.data = config.data;
                    auditLog.method = config.method;
                    auditLog.updated_date_time = new Date(new Date().toISOString());
                    auditLog.url = config.url;
                    auditLog.log_auth_key = LOG_AUTH_KEY;
                     jQuery.ajax({
                     type: 'POST',
                     url: API_URL + 'signup/addAuditLog',
                     dataType: 'json',
                     data: angular.toJson(auditLog),
                     contentType: "application/json",
                     })
                     .done(function (msg) {
                     //alert("Data Saved: " + msg);
                     });
                }
                var encrypt = DATA_ENCRYPT;
                if (config.hasOwnProperty('DATA_ENCRYPT'))
                    encrypt = config.DATA_ENCRYPT

                if (config.hasOwnProperty('params') && encrypt) {
                    var actualParams = config.params;
                    config.params = {};
                    config.params.requestData = GibberishAES.enc(JSON.stringify(actualParams), AES_KEY);
                }
                else if (config.hasOwnProperty('data') && encrypt) {
                    if (config.data) {
                        var actualParams = config.data;
                        config.data = {};
                        //console.log(actualParams);
                        if (actualParams.hasOwnProperty('file')) {
                            config.data.file = actualParams.file;
                            delete actualParams.file;
                        }
                        if (actualParams.hasOwnProperty('$$hashKey')) {
                            delete actualParams['$$hashKey'];
                        }
                        config.data.requestData = GibberishAES.enc(JSON.stringify(actualParams), AES_KEY);
                    }
                }
                return config || $q.when(config);
            },
            response: function (response) {
                // $('#loader').hide();
                httpLoader.subtractPendingReq(response.config);

                var encrypt = DATA_ENCRYPT;
                if (response.hasOwnProperty('DATA_ENCRYPT'))
                    encrypt = config.DATA_ENCRYPT

                if (response.data.hasOwnProperty('responseData') && encrypt) {
                    response.data = JSON.parse(GibberishAES.dec(response.data.responseData, AES_KEY));
                }

                return response || $q.when(response);
            },
            responseError: function (response) {
                httpLoader.subtractPendingReq(response.config);
                if (response && response.status === 404) {
                    $rootScope.toast('404', 'Not Found', 'warning');
                }
                if (response && response.status === -1) {
                    $rootScope.toast('Service Connection Error', 'Error while fetching URL', 'warning');
                    // $rootScope.$broadcast('loggedOut');
                }
                if (response && response.status === 401) {
                    $rootScope.toast('401', 'session expired', 'warning');
                    $rootScope.$broadcast('loggedOut');
                }
                if (response && response.status >= 500) {
                    $rootScope.toast('500', 'INTERNAL SERVER ERROR', 'warning');
                }
                return $q.reject(response);
            }
        };
    })

    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $httpProvider.interceptors.push('errorInterceptor');
    }])
    .factory("transformRequestAsFormPost", function () {
        // I prepare the request data for the form post.
        function transformRequest(data, getHeaders) {
            var headers = getHeaders();
            headers["Content-type"] = "application/x-www-form-urlencoded; charset=utf-8";
            return ( serializeData(data) );
        }

        // Return the factory value.
        return ( transformRequest );
        // ---
        // PRVIATE METHODS.
        // ---
        // I serialize the given Object into a key-value pair string. This
        // method expects an object and will default to the toString() method.
        // --
        // NOTE: This is an atered version of the jQuery.param() method which
        // will serialize a data collection for Form posting.
        // --
        // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
        function serializeData(data) {
            // If this is not an object, defer to native stringification.
            if (!angular.isObject(data)) {
                return ( ( data == null ) ? "" : data.toString() );
            }
            var buffer = [];
            // Serialize each key in the object.
            for (var name in data) {
                if (!data.hasOwnProperty(name)) {
                    continue;
                }
                var value = data[name];
                buffer.push(
                    encodeURIComponent(name) +
                    "=" +
                    encodeURIComponent(( value == null ) ? "" : value)
                );
            }
            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                .join("&")
                .replace(/%20/g, "+")
                ;
            return ( source );
        }
    })
    //Auth
    .factory('Auth', function (Idle,$rootScope, $http, $cookies, $location, redirectToUrlAfterLogin) {
        return {
            checkLogin: function () {
                // Check if logged in and fire events
                if (this.isLoggedIn()) {
                    $rootScope.$broadcast('loggedIn');
                    return true;
                } else {
                    $rootScope.$broadcast('loggedOut');
                    return false;
                }
            },
            isLoggedIn: function () {
                this.saveAttemptUrl();
                if ($cookies.get('app'))
                    return true;
                return false;
                // Check auth token here from localStorage
            },
            getFields: function () {
                //if(window.localStorage['app']!=undefined)
                //    return window.localStorage['app'];
                if ($cookies.get('app'))
                    return $cookies.get('app');
            },
            login: function (data) {
                Idle.watch();
                $cookies.put('app', angular.toJson(data));
                $http.defaults.headers.common['Authorization'] = data.access_token;
                $http.defaults.headers.common['User'] = data.id_user;
                $rootScope.$broadcast('loggedIn');
            },
            logout: function (user, pass) {
                $rootScope.$broadcast('loggedOut');
            },
            userLogs: function ($postData) {
                return $http({
                    //url: LOG_URL + 'addUserAccessLog',
                    url: API_URL + 'signup/addUserAccessLog',
                    method: "POST",
                    data: $postData,
                    DATA_ENCRYPT: false,
                    loaderId: false,
                    accessLog : true,
                    headers: {'Content-Type': 'application/json'}
                }).error(function () {
                }).then(function (response) {
                    return response.data;
                });
            },
            userAccessLogsHistory: function (params) {
                return $http({
                    //url: LOG_URL + 'findUserAccessLog',
                    url: API_URL + 'signup/findUserAccessLog',
                    method: "GET",
                    params: params,
                    DATA_ENCRYPT: false,
                    headers: {'Content-Type': 'application/json'}
                }).error(function () {
                }).then(function (response) {
                    return response.data;
                });
            },
            saveAttemptUrl: function () {
                if ($location.path().toLowerCase() != '/login' && $location.path().toLowerCase() != '' && redirectToUrlAfterLogin.first_time && redirectToUrlAfterLogin.direct_entry) {
                    redirectToUrlAfterLogin.url = $location.path();
                    $rootScope.redirectURL = $location.absUrl();    //Added to get redirectURL
                    redirectToUrlAfterLogin.first_time = false;
                    redirectToUrlAfterLogin.direct_entry = false;
                }
            },
            redirectToAttemptedUrl: function () {
                $location.path(redirectToUrlAfterLogin.url);
            }
        }
    })
    .factory('appService', function ($rootScope, $http, $cookies, $location, redirectToUrlAfterLogin) {
        return {
            menu: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/menu', {params: params,cache:true})
                        .then(function (response) {
                            return response.data;
                        });
                }
            }
        }
    })
    //User
    .factory('UserService', function ($http, $cacheFactory, $rootScope, transformRequestAsFormPost) {
        // Might use a resource here that returns a JSON array
        return {
            login: function ($postData) {
                return $http.post(API_URL + 'Signup/login', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            forgotPassword: function ($postData) {
                return $http.post(API_URL + 'Signup/forgetPassword', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUserQuestion: function ($postData) {
                return $http.post(API_URL + 'Signup/getUserQuestion', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            changePassword: function ($postData) {
                return $http.post(API_URL + 'User/changePassword', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUserProfile: function ($params) {
                return $http.get(API_URL + 'User/userInfo', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            postUserProfile: function (params) {
                return $http.post(API_URL + 'User/userInfo', params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getLoginHistory: function (params) {
                return $http.get(API_URL + 'User/loginHistory', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUserNotifications: function (params) {
                return $http.get(API_URL + 'Activity/notification', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveSecurityQuestion: function (params) {
                return $http.post(API_URL + 'User/saveSecurityQuestion', params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            updateSecurityQuestion: function (params) {
                return $http.post(API_URL + 'User/updateSecurityQuestion', params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUserLogs: function (params) {
                console.log(params);
                /*var paramss = '?offset=' + start + '&limit=' + number;*/
                return $http.get(API_URL + 'User/userLogByType', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            resetPassword: function (params) {
                return $http.post(API_URL + 'Company/changePassword', params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            }
        }
    })
    //masterService
    .factory('masterService', function ($http, $cacheFactory, $rootScope, Upload, $timeout) {
        // Might use a resource here that returns a JSON array
        return {
            country: {
                getPage: function (start, number) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/country' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }, //
                get: function (params) {
                    return $http.get(API_URL + 'Master/country',{params:params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                createCountry: function ($postData) {
                    return $http.post(API_URL + 'Master/country', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/countryById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                edit: function ($postData) {
                    return $http.put(API_URL + 'Master/country', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            other: {
                otherList: function () {
                    //  var paramss = '?offset='+start+'&limit='+number;
                    return $http.get(API_URL + 'Master/otherList')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getPage: function (start, number, masterOtherSelected) {
                    var paramss = '?offset=' + start + '&limit=' + number + '&master_id=' + masterOtherSelected;
                    return $http.get(API_URL + 'Master/other' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/otherById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                create: function ($postData) {
                    return $http.post(API_URL + 'Master/other', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                update: function ($postData) {
                    return $http.put(API_URL + 'Master/other', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            branchType: {
                getPage: function (params) {
                    //  var paramss = '?offset='+start+'&limit='+number;
                    return $http.get(API_URL + 'Master/branch', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getAll: function (start, number) {
                    return $http.get(API_URL + 'Master/branch')
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                createBranchType: function ($postData) {
                    return $http.post(API_URL + 'Master/branch', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/branchTypeById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                edit: function ($postData) {
                    return $http.put(API_URL + 'Master/branch', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            approvalRole: {
                getPage: function (params) {
                    return $http.get(API_URL + 'Master/approvalRole', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function () {
                    return $http.get(API_URL + 'Master/approvalRole')
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                createApprovalRole: function ($postData) {
                    return $http.post(API_URL + 'Master/approvalRole', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/approvalRoleById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                edit: function ($postData) {
                    return $http.put(API_URL + 'Master/approvalRole', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            plans: {
                get: function (start, number, param) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/plansList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                createPlan: function ($postData) {
                    return $http.post(API_URL + 'Master/plans', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getPage: function (start, number, params) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/plansList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                getPlan: function ($id) {
                    return $http.get(API_URL + 'Master/plans/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                updatePlan: function ($postData) {
                    return $http.put(API_URL + 'Master/plans', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            risk: {
                get: function (start, number, param) {
                    var paramss = '?offset=' + start + '&limit=' + number + '&' + param;
                    return $http.get(API_URL + 'Master/riskList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getAll: function () {
                    return $http.get(API_URL + 'Master/riskItems')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getRiskStage: function (param) {
                    param = $.param(param);
                    return $http.get(API_URL + 'Master/riskItems?'+param)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/risk/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                create: function ($postData) {
                    return $http.post(API_URL + 'Master/risk', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                update: function ($postData) {
                    return $http.post(API_URL + 'Master/risk', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getAllProject: function () {
                    return $http.get(API_URL + 'Master/riskItems')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
            },
            social: {
                get: function (start, number, param) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/socialList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/social/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                create: function ($postData) {
                    return $http.post(API_URL + 'Master/social', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                update: function ($postData) {
                    return $http.put(API_URL + 'Master/social', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            contact: {
                get: function (start, number, param) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/contactList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/contact/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                create: function ($postData) {
                    return $http.post(API_URL + 'Master/contact', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                update: function ($postData) {
                    return $http.put(API_URL + 'Master/contact', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            bankCategory: {
                getAll: function (start, number, param) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/bankCategoryList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function () {
                    return $http.get(API_URL + 'Master/bankCategory')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getPage: function (start, number, params) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/bankCategory' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                getBankCategory: function ($id) {
                    return $http.get(API_URL + 'Master/bankCategoryById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                updateBankCategory: function ($postData) {
                    return $http.put(API_URL + 'Master/bankCategory', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                createBankCategory: function ($postData) {
                    return $http.post(API_URL + 'Master/bankCategory', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            sector: {
                getPage: function (start, number, params) {
                    var paramss = '?offset=' + start + '&limit=' + number;
                    return $http.get(API_URL + 'Master/sectorList' + paramss)
                        .error(function () {
                        }).then(function (response) {
                            return response.data.data;
                        });
                },
                getAll: function (params) {
                    return $http.get(API_URL + 'Master/sector', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                getAssessmentSectors: function (params) {
                    return $http.get(API_URL + 'Master/sector', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getAllSectors: function () {
                    return $http.get(API_URL + 'Master/sector/?parent_sector_id=0')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getLimitSectorsAll: function () {
                    return $http.get(API_URL + 'Company/approvalLimitSector')
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param id_sector,parent_sector_name
                 * @returns {*}
                 */
                getSubSector: function (params) {
                    return $http.get(API_URL + 'Master/subSector', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                getById: function ($id) {
                    return $http.get(API_URL + 'Master/sectorById/' + $id)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                add: function (data) {
                    return $http.post(API_URL + 'Master/sector', data)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                edit: function ($postData) {
                    console.log('Master/sector');
                    return $http.put(API_URL + 'Master/sector', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            currency: {
                getAll: function (params) {
                    return $http.get(API_URL + 'Master/currency', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getTable: function (params) {
                    return $http.get(API_URL + 'Master/currencyList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (file, params) {
                    return Upload.upload({
                        url: API_URL + 'Master/currency',
                        data: {
                            file: {'country_flag': file},
                            'currency': params
                        }
                    }).then(function (response) {
                        return response.data;
                    }, function (resp) {
                        $rootScope.toast('Error', resp.data.message);
                    }, function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });
                }
            },
            language: {
                delete: function (params) {
                    return $http.delete(API_URL + 'Master/language', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getAll: function (params) {
                    return $http.get(API_URL + 'Master/language', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getTable: function (params) {
                    return $http.get(API_URL + 'Master/languageList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (file, params) {
                    return Upload.upload({
                        url: API_URL + 'Master/language',
                        data: {
                            file: {'flag': file},
                            'language': params
                        }
                    }).then(function (response) {
                        return response.data;
                    }, function (resp) {
                        $rootScope.toast('Error', resp.data.message);
                    }, function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });

                }
            },
            idProofList: {
                /**
                 *
                 * @param params
                 * @returns {*}
                 */
                getAll: function (params) {
                    return $http.get(API_URL + 'Master/idProofList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            projectLoanType: function (params) {
                return $http.get(API_URL + 'Master/projectLoanType', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            projectPaymentType: function (params) {
                return $http.get(API_URL + 'Master/projectPaymentType', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            getProductType: function ($params) {
                return $http.get(API_URL + 'Crm/product', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProductById: function ($params) {
                return $http.get(API_URL + 'Crm/product', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addProductType: function (data) {
                return $http.post(API_URL + 'Crm/product', data)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getApprovalLimitType: function (company_id, product_id) {
                return $http.get(API_URL + 'Company/approvalLimit?company_id=' + company_id + '&product_id=' + product_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addSectorToLimits: function (params) {
                return $http.get(API_URL + 'Company/checkApprovalLimitSector', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            postApprovalLimits: function ($postData) {
                return $http.post(API_URL + 'Company/approvalLimit', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            updateApprovalStructure: function ($postData) {
                return $http.post(API_URL + 'Company/product', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteApprovalLimitSector: function ($postData) {
                return $http.delete(API_URL + 'Company/approvalLimit', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             *
             * @param master_key = ['facility_term_type']
             * @returns {*}
             */
            masterList: function (params) {
                return $http.get(API_URL + 'Master/master', {params: params,'loaderId':false})
                    .then(function (response) {
                        return response.data;
                    });
            }
        }
    })
    //company
    .factory('companyService', function ($http, $cacheFactory, $rootScope) {
        // Might use a resource here that returns a JSON array
        return {
            get: function () {
                return $http.get(API_URL + 'Company/companyList')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getById: function ($id) {
                return $http.get(API_URL + 'Company/company/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyInformation: function (params) {
                return $http.get(API_URL + 'Company/companyInformation', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            post: function ($postData) {
                return $http.post(API_URL + 'Company/company', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            userList: function (params) {
                return $http.get(API_URL + 'Company/userList', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            adminUserList: function (params) {
                return $http.get(API_URL + 'Company/userList', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             *company_id
             * currency_id
             * @returns {*}
             */
            primaryCurrency: function (params) {
                return $http.post(API_URL + 'Company/primaryCurrency', params).then(function (response) {
                    return response.data;
                });
            },
            user: function ($userId) {
                return $http.get(API_URL + 'Company/user/' + $userId)
                    .then(function (response) {
                        return response.data;
                    });
            },
            users: function (params) {
                return $http.get(API_URL + 'Company/users', {'params': params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            userView: function (params) {
                /**
                 * company_id , user_id
                 */
                return $http.get(API_URL + 'Company/userList', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            branchView: function (params) {
                return $http.get(API_URL + 'Company/branchDetails', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            branch: function ($Id) {
                return $http.get(API_URL + 'Company/branch/' + $Id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchList: function ($companyId, $branchId, start, number) {
                var params = '';
                if ($branchId == undefined) $branchId = 0;
                if (start != undefined && number != undefined) {
                    params = '&offset=' + start + '&limit=' + number;
                }
                return $http.get(API_URL + 'Company/companyBranch/' + $companyId + '/' + $branchId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             *
             * @param company_id branch_type_id(optional)
             * @returns {*|Promise.<T>}
             */
            companyApprovalRoles: function (params) {
                return $http.get(API_URL + 'Company/companyApprovalRole', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyBranchesList: function ($params) {
                return $http.get(API_URL + 'Company/companyBranchList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyBranches: function ($params) {
                return $http.get(API_URL + 'Company/companyBranches', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchReportingToUsers: function ($postData) {
                return $http.get(API_URL + 'Company/userList', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchTable: function ($companyId, start, number) {
                var params = '';
                if (start != undefined && number != undefined) {
                    params = '&offset=' + start + '&limit=' + number;
                }
                return $http.get(API_URL + 'Company/companyBranchList?company_id=' + $companyId + params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchStructure: function ($companyId) {
                return $http.get(API_URL + 'Company/branches/' + $companyId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            roleStructure: function ($companyId) {
                return $http.get(API_URL + 'Company/approvals/' + $companyId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            download: function () {
                return $http.get(API_URL + 'Company/downloadBranchExcel')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            downloadUsers: function () {
                return $http.get(API_URL + 'Company/downloadUserExcel')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchTypeStructure: function ($postData) {
                return $http.post(API_URL + 'Company/companyBranchType', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getBranchTypeStructure: function ($company_id) {
                return $http.get(API_URL + 'Company/branchTypeStructure/' + $company_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            approvalTypeStructure: function ($postData) {
                return $http.post(API_URL + 'Company/approvalRole', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getApprovalTypeStructure: function ($company_id) {
                return $http.get(API_URL + 'Company/approvalTypeStructure/' + $company_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            chartCustomersByCountry: function () {
                return $http.get(API_URL + 'dashboard/customersByCountry')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            chartCustomersByCategory: function () {
                return $http.get(API_URL + 'dashboard/customersByCategory')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyBranchType: function ($companyId, $branchTypeId) {
                if ($branchTypeId == undefined) {
                    $branchTypeId = 0;
                }
                return $http.get(API_URL + 'Company/companyBranchType?company_id=' + $companyId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            branchTypeRole: function (params) {
                return $http.get(API_URL + 'Company/branchTypeRole', {params: params}).then(function (response) {
                    return response.data;
                });
            },
            branchType: {
                post: function (params) {
                    return $http.post(API_URL + 'Company/branchType', params).then(function (response) {
                        return response.data;
                    });
                },
                get: function (params) {
                    return $http.get(API_URL + 'Company/branchType', {params: params}).then(function (response) {
                        return response.data;
                    });
                }
            },
            companyApprovalRole: function (params) {
                return $http.get(API_URL + 'Company/companyApprovalRole', {params: params}).then(function (response) {
                    return response.data;
                });
            },
            productStageWorkflow: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/productStageWorkflow', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                getById: function (params) {
                    return $http.get(API_URL + 'Crm/productStageWorkflow', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crm/productStageWorkflow', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            knowledgeManagement: {
                getTags: function () {
                    return $http.get(API_URL + 'Company/knowledgeTags').then(function (response) {
                        return response.data;
                    });
                },
                get: function (params) {
                    return $http.get(API_URL + 'Company/knowledge', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                getSearchResult: function (params) {
                    return $http.get(API_URL + 'Company/knowledgeList', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                knowledgeManagementGraph: function (params) {
                    return $http.get(API_URL + 'Company/knowledgeGraph', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                getDetails: function (params) {
                    return $http.get(API_URL + 'Crm/knowledgeDocumentDetails', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                knowledgePublish: function ($postData) {
                    return $http.post(API_URL + 'Crm/knowledgePublish', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            knowledgeDocumentComment: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/knowledgeDocumentComment', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                add: function ($postData) {
                    return $http.post(API_URL + 'Crm/knowledgeDocumentComment', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            approvalStructure: {
                /**
                 * type Obj
                 * @params comapnay Id
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Company/approvalStructure', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                /**
                 * type object
                 * @param params company_id,approval_structure_name
                 * if update  approval_id
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Company/approvalStructure', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            approvalCommittees: {
                /**
                 * type Obj
                 * @params comapnay Id
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Company/approvalStructure', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                /**
                 *
                 * @param params
                 * @returns {}
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Company/approvalCreditCommittee', params).then(function (response) {
                        return response.data;
                    });
                },
                /**
                 *
                 */

                forwardCommittee: function (params) {
                    return $http.post(API_URL + 'Company/forwardCommittee', params).then(function (response) {
                        return response.data;
                    });
                },
                approvalCreditCommitteeList: function (params) {
                    return $http.get(API_URL + 'Company/approvalCreditCommitteeList', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                /**
                 *
                 * @param params id_company_approval_credit_committee
                 * @returns {*}
                 */
                approvalCreditCommitteeById: function (params) {
                    return $http.get(API_URL + 'Company/approvalCreditCommittee', {params: params}).then(function (response) {
                        return response.data;
                    });
                }
            },
            assessmentQuestion: {
                category: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/assessmentQuestionCategory', {params: params}).then(function (response) {
                            return response.data;
                        });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Company/assessmentQuestionCategory', params).then(function (response) {
                            return response.data;
                        });
                    }
                },
                question: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/assessmentQuestion', {params: params}).then(function (response) {
                            return response.data;
                        });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Company/assessmentQuestion', params).then(function (response) {
                            return response.data;
                        });
                    }
                }
            },
            riskAssessment: {
                riskCategory: {
                    /*
                     *@params get company_id
                     */
                    get: function ($params) {
                        return $http.get(API_URL + 'Company/riskCategory', {params: $params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    /**
                     * @params
                     * risk_category_name
                     * parent_risk_category_id
                     * company_id
                     * risk_percentage
                     *
                     */
                    post: function ($params) {
                        return $http.post(API_URL + 'Company/riskCategory', $params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function ($params) {
                        return $http.delete(API_URL + 'Company/riskCategory', {params: $params})
                            .then(function (response) {
                                return response.data;
                            });
                    }

                },
                riskItem: {
                    /*
                     *risk_category_id
                     *  risk_category_item_name
                     *  risk_category_item_grade
                     *  sector_id
                     *
                     */
                    post: function ($params) {
                        return $http.post(API_URL + 'Company/riskCategoryItem', $params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            },
            currency: {
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Company/companyCurrency', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                currencyTable: function (params) {
                    return $http.get(API_URL + 'Company/companyCurrencyList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Company/companyCurrency', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 * @constructor
                 */
                CurrencySelected: function (params) {
                    return $http.get(API_URL + 'Company/companyCurrencySelected', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 */
                currencyValue: function (params) {
                    return $http.post(API_URL + 'Company/companyCurrencyValue', params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            covenant: {
                get: function (params) {
                    return $http.get(API_URL + 'Company/covenant', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Company/covenant', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crm/covenant', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                type: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/covenantType', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                category: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/covenantCategory', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    /**
                     * company_id=1&covenant_type_key=positive_covenant
                     * @param params
                     * @returns {*|Promise.<T>}
                     */
                    list: function (params) {
                        return $http.get(API_URL + 'Company/covenantCategoryList', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    /**
                     *
                     * @param covenant_type_id.
                     sector(1,2,3)
                     covenant_category
                     company_id
                     * @returns {*|Promise.<T>}
                     */
                    post: function (params) {
                        return $http.post(API_URL + 'Company/covenantCategory', params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            },
            role: {
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Company/applicationRole', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Company/applicationRole', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                mapping: {
                    /**
                     *
                     * @param params
                     * application_role_id company_approval_role_id or user_id
                     * type: approval_role or user
                     * @returns {*|Promise.<T>}
                     */
                    get: function (params) {
                        return $http.get(API_URL + 'Company/applicationUserRole', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Company/applicationUserRole', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Company/applicationUserRole', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                /**
                 *
                 * @param
                 * @returns {*|Promise.<T>}
                 */
                module: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/module', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Company/module', params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            },
            facilityType: {
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Company/companyFacility', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Company/companyFacility', params)
                        .then(function (response) {

                            return response.data;
                        });
                },
                timePeriod: function (params) {
                    return $http.get(API_URL + 'Company/timePeriod', {cache: true, params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                facilityAmountType: function (params) {
                    return $http.get(API_URL + 'Company/facilityAmountType', {cache: true, params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                companyFacilityFields: function (params) {
                    return $http.post(API_URL + 'Company/companyFacilityFields', params)
                        .then(function (response) {

                            return response.data;
                        });
                },
                getCompanyFacilityFields: function (params) {
                    return $http.get(API_URL + 'Company/companyFacilityFields', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                postFacilityGeneral: function (params) {
                    return $http.post(API_URL + 'Company/facilityGeneral', params)
                        .then(function (response) {

                            return response.data;
                        });
                },
                deleteCompanyFacilityFields: function (params) {
                    return $http.delete(API_URL + 'Company/companyFacilityFields', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getFacilityFields: function (params) {
                    return $http.get(API_URL + 'Company/facilityFields', {cache: true, params: params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            sectorLimit:{
                get: function (params) {
                    return $http.get(API_URL + 'Company/sectorLimit', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Company/sectorLimit', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete:function(params){
                    return $http.delete(API_URL + 'Company/sectorLimit', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                addSectors: function (params) {
                    return $http.post(API_URL + 'Company/addSectors', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
            }


        }
    })
    //crm
    .factory('crmService', function ($http, $rootScope, $state, $stateParams, $timeout, $q) {
        return {
            touchPoint: {
                get: function (params, loaderId) {
                    return $http.get(API_URL + 'Crm/touchPoint', {params: params, 'loaderId': loaderId})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                post: function ($postData) {
                    return $http.post(API_URL + 'Crm/touchPoints', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                delete:function(params){
                    return $http.delete(API_URL + 'Crm/touchPoints', {params: params})
                       .then(function (response) {
                            return response.data;
                        });
                },
                touchPointType: function () {
                    return $http.get(API_URL + 'Crm/touchPointType',{cache:true})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            modulePermission: {
                /**
                 *
                 * @param company_id &module_key&user_id
                 * @returns {*|Promise.<T>}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Crm/moduleAccess', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
            },
            document: {
                /**
                 *
                 * @param type(version or document),document_id
                 * @returns {*}
                 */
                delete: function (params) {
                    return $http.delete(API_URL + 'Crm/document', {params: params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            checkUrlPermission: function (user_type) {
                return true;
                /*if ($rootScope.user_role_id == user_type) {
                    return true;
                } else {
                    // $rootScope.toast('Error',result.error,'l-error');
                    $timeout(function () {
                        $state.go('error.404');
                    });
                    return false;
                }*/
            },
            setPermission: function (module_key) {
                /*if($rootScope.user_role_id != 3){
                    return true;
                }*/
                var defer = $q.defer();
                var params = {};
                params.module_key = module_key; //$state.current.data.module_key;
                params.user_id = $rootScope.userId;
                params.company_id = $rootScope.id_company;
                params.module_access = 1;
                $http.get(API_URL + 'Crm/moduleAccess', {params: params}).then(function (response) {
                    var result = response.data;
                    $rootScope.permission = {};

                    if (result.status) {
                        var trueCount = 0;
                        angular.forEach(result.data, function (value, key) {
                            angular.forEach(value, function (value, key) {
                                $rootScope.permission[key] = value;
                                if(value){
                                    trueCount++;
                                }
                            })
                        });
                        if(trueCount>0){
                            defer.resolve(result.data);
                        }else{
                            $timeout(function () {
                                $state.go('error.404',null,{
                                    location: false
                                });
                            });
                            defer.reject();
                        }
                    } else {
                        $rootScope.toast('Error', result.error, 'l-error');
                        $timeout(function () {
                            $state.go('error.404',null,{
                                location: false
                            });
                        });
                        defer.reject();
                    }
                });
                return defer.promise;
            }
        }
    })
    .factory('meetingService', function ($http, Upload) {
        return {
            post: function ($params) {
                return Upload.upload({
                    async: true,
                    url: API_URL + 'Activity/meeting',
                    data: $params
                }).then(function (response) {
                    return response.data;
                });
            },
            get: function ($params,loaderId) {
                //if get by id @ id_meeting
                return $http.get(API_URL + 'Activity/meeting', {params: $params,'loaderId':loaderId})
                    .then(function (response) {
                        return response.data;
                    });
            },
            delete: function ($params) {
                return $http.delete(API_URL + 'Activity/meeting', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            }
        }
    })

    //contact
    .factory('crmContactService', function ($http, $cacheFactory, $rootScope, transformRequestAsFormPost,Upload ) {
        return {
            contactInformation: function (params) {
                return $http.get(API_URL + 'Crm/contactInformation', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            contactForms: function (params) {
                return $http.get(API_URL + 'Crm/contactForms', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            getModules: function ($module_id) {
                return $http.get(API_URL + 'Crm/module/' + $module_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveApplicant: function ($postData) {
                return $http.post(API_URL + 'Crm/formData', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getIdentityDocumentList: function (params) {
                return $http.get(API_URL + 'Crmcompany/contactIdentification',{params:params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveIdentityDocument: function ($postData) {
                return Upload.upload({url: API_URL + 'Crmcompany/contactIdentification', data: $postData})
                    .then(function (response) {
                        return response.data;
                    });
            },
            deleteIdentityDocument: function (id) {
                return $http.delete(API_URL + 'Crmcompany/contactIdentification/?id_contact_identification=' + id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            contactList: function ($params) {
                return $http.get(API_URL + 'Crm/contactList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            crmContactType: function () {
                return $http.get(API_URL + 'Crm/crmContactType')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            changeUserContactType: function ($postData) {
                return $http.post(API_URL + 'Crm/contact', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            contactDetails: function ($companyId, start) {
                return $http.get(API_URL + 'Crm/contact/' + $companyId + '/' + start)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteProjectContact: function (contactId) {
                return $http.delete(API_URL + 'Crm/projectContact/' + contactId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            contactFormDetails: function (id_crm_contact, formId, type) {
                return $http.get(API_URL + 'Crm/formData/' + id_crm_contact + '/' + formId + '/' + type)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            contactType: function () {
                return $http.get(API_URL + 'Crm/contactType')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getAttachmentsTypes: function (params,loaderId) {
                return $http.get(API_URL + 'Crm/documentType', {params: params,loaderId:loaderId})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUploadedAttachments: function (params,loaderId) {
                return $http.get(API_URL + 'Crm/document', {params: params,loaderId:loaderId})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getTouchPointsList: function (params) {
                return $http.get(API_URL + 'Crm/touchPoints/?' + params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            touchPointType: function () {
                return $http.get(API_URL + 'Crm/touchPointType')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            contactTouchPoints: function (params) {
                params.crm_module_type = 'contact';
                return $http.get(API_URL + 'Crm/touchPoint', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getCompanyTouchPoints: function (params) {
                params.crm_module_type = 'company';
                return $http.get(API_URL + 'Crm/touchPoint', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProjectTouchPoints: function (params) {
                params.crm_module_type = 'project';
                return $http.get(API_URL + 'Crm/touchPoint', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addTouchPointComment: function ($postData) {
                return $http.post(API_URL + 'Crm/touchPoints', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getCompanyAssessment: function ($params) {
                return $http.get(API_URL + 'Company/companyAssessment', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getInternalRating: function ($params) {
                return $http.get(API_URL + 'Company/internalRating', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getUrl: function (params) {
                return $http.get(API_URL + 'Crm/getDownloadedFile', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             * company_id
             * @returns {*}
             */
            getAllAssessment: function ($params) {
                return $http.get(API_URL + 'Company/assessment', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            updateAssessment: function ($postData) {
                return $http.post(API_URL + 'Company/companyAssessment', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addCompanyAssessmentItemStep: function ($postData) {
                return $http.post(API_URL + 'Company/companyAssessmentItemStep', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            updateAssessmentStep: function ($postData) {
                return $http.post(API_URL + 'Company/assessmentStep', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteCompanyAssessmentItemStep: function ($postData) {
                return $http.delete(API_URL + 'Company/companyAssessmentItemStep', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            emailVerify: function ($postData) {
                return $http.post(API_URL + 'Crmcompany/emailVerification', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            verifyEmailCode: function ($postData) {
                return $http.post(API_URL + 'Crmcompany/emailVerificationCode', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getPersonalInformation: function ($params) {
                return $http.get(API_URL + 'Crm/contactPersonalInformation', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            addPersonalInformation: function ($postData) {
                return $http.post(API_URL + 'Crm/contactPersonalInformation', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deletePersonalInformation: function ($id) {
                return $http.delete(API_URL + 'Crm/contactRelation/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteRefrenceGuarantor: function ($id) {
                return $http.delete(API_URL + 'Crm/contactRelation/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /*getRefrenceGuarantor: function ($params) {
                return $http.get(API_URL + 'Crm/contactReferenceGuarantor', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },*/
            getRefrenceGuarantor: function ($params) {
                return $http.get(API_URL + 'Crm/contactRelationInformation', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            addRefrenceGuarantor: function ($postData) {
                return $http.post(API_URL + 'Crm/contactReferenceGuarantor', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getTangibleLiabilities: function ($params) {
                return $http.get(API_URL + 'Crm/contactAssetsAndLiabilities', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            getTangibleCollateral: function ($params) {
                return $http.get(API_URL + 'Collateral/contactCollateralSearch', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            addTangible: function ($postData) {
                return $http.post(API_URL + 'Crm/contactAssets', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteTangible: function ($id) {
                return $http.delete(API_URL + 'Crm/contactAssets/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveLiabilities: function ($postData) {
                return $http.post(API_URL + 'Crm/contactLiabilities', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteLiabilities: function ($id) {
                return $http.delete(API_URL + 'Crm/contactLiabilities/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getRelationsList : function ($params) {
                return $http.get(API_URL + 'Crm/contactRelationSearch', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            saveRelation : function ($params) {
                return $http.post(API_URL + 'Crm/contactRelationalInformation', $params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getRelations : function ($params) {
                return $http.get(API_URL + 'Crm/contactRelationInformation',  {params: $params})
                   .then(function (response) {
                        return response.data;
                    });
            },
            getCompanyListForContact: function ($params) {
                return $http.get(API_URL + 'Crm/mapModule', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getIncomeAndExpense: function ($params) {
                return $http.get(API_URL + 'Crm/contactIncomeAndExpenses', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            saveIncomeOrExpense: function ($postData) {
                return $http.post(API_URL + 'Crm/contactIncomeAndExpenses', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteIncomeOrExpense: function ($id) {
                return $http.delete(API_URL + 'Crm/contactIncomeAndExpenses/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getContactInternalRatingDetails: function ($params) {
                return $http.get(API_URL + 'Crm/contactInternalRating', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            riskAssessment: {
                /*
                 *@params get company_id
                 * crm_project_id
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/riskCategory', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectRiskFactorsGet: function ($params) {
                    return $http.get(API_URL + 'Crm/projectRiskFactors', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectRiskFactorsPost: function ($postData) {
                    return $http.post(API_URL + 'Crm/projectRiskFactors', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                /**
                 * @params
                 *crm_project_id
                 *user_id
                 * attribute data array
                 */
                post: function ($params) {
                    return $http.post(API_URL + 'Crm/contactRiskCategory', $params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            }
        }
    })
    //company
    .factory('crmCompanyService', function ($http,Upload, $cacheFactory, $rootScope, transformRequestAsFormPost) {
        return {
            getModules: function ($module_id, $crm_id) {
                return $http.get(API_URL + 'Crm/module/' + $module_id + '/' + $crm_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyInformation:function(params){
                return $http.get(API_URL + 'Crm/companyInformation',{params:params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            getFinancialInfo: function (params) {
                return $http.get(API_URL + 'Company/financialStatements', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveCompany: function ($postData) {
                return $http.post(API_URL + 'Crm/company', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyList: function ($params) {
                return $http.get(API_URL + 'Crm/companyList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getModuleList: function ($params) {
                return $http.get(API_URL + 'Mismaster/getModuleItems', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getAuditLogs: function ($params) {
                return $http.get(API_URL + 'Mismaster/getAuditLogItems', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             * params company Id
             * @returns {*}
             */
            companyTabInfo: function ($params) {
                return $http.get(API_URL + 'Crm/companyTabInfo', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            crmCompanyList: function ($params) {
                return $http.get(API_URL + 'Crm/crmCompanyList', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            getAllCompanyList: function ($params) {
                return $http.get(API_URL + 'Crm/getAllCompaniesList', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            /*companyDesignation: function ($companyId, $params) {
                return $http.get(API_URL + 'Crm/companyDesignation/')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },*/
            companyDesignation: function ($params) {
                return $http.get(API_URL + 'Crm/mappingRelationType',{'params' : $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyDetails: function ($companyId, formId) {
                return $http.get(API_URL + 'Crm/company/' + $companyId + '/' + formId)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyFormDetails: function ($companyId, formId, type) {
                return $http.get(API_URL + 'Crm/companyForm/' + $companyId + '/' + formId + '/' + type)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveFinancialInfo: function ($infoKey) {
                return $http.post(API_URL + 'Company/financialExcel', $infoKey)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getIntelligenceData: function (params) {
                return $http.get(API_URL + 'Crm/intelligence/', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getKnowledgeData: function (params) {
                return $http.get(API_URL + 'Crm/knowledge', {params: params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            updateViewCount: function (param) {
                return $http.post(API_URL + 'Crm/knowledgeCount', param)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getFinancialData: function ($params) {
                return $http.get(API_URL + 'Company/financialInformation', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addCompanyToContact: function ($fields) {
                return $http.post(API_URL + 'Crm/mapModule', $fields)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addSubCompany: function ($fields) {
                return $http.post(API_URL + 'Crmcompany/companyRelation', $fields)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteSubModule: function ($params) {
                return $http.delete(API_URL + 'Crmcompany/companyRelation?id_crm_company_relation='+ $params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteContactCompany: function ($id) {
                return $http.delete(API_URL + 'Crm/companyContact/' + $id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            crmListForMapping: function ($params) {
                return $http.get(API_URL + 'Crmcompany/companyRelationSearch', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyListForMapping: function ($params) {
                return $http.get(API_URL + 'Crm/crmListForMapping', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getCompanyListForCompany: function ($params) {
                return $http.get(API_URL + 'Crmcompany/companyRelation', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getContactForCompany : function ($params) {
                return $http.get(API_URL + 'Crm/mapModule', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getCompanyList: function ($params) {
                return $http.get(API_URL + 'Crm/companyList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyType: function () {
                return $http.get(API_URL + 'Crm/companyType')
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectCompany: function ($fields) {
                return $http.post(API_URL + 'Crm/projectCompany', $fields)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectCompanyListFun: function ($postData) {
                return $http.get(API_URL + 'Crm/projectCompany', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteProjectCompany: function ($postData) {
                return $http.delete(API_URL + 'Crm/projectCompany/' + $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getcompanyPriviewDetails: function ($params) {
                return $http.get(API_URL + 'Company/allCompanyAssessments', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getCompanyInternalRatingDetails: function ($params) {
                return $http.get(API_URL + 'Crm/companyInternalRisk', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            companyFinancialInformation: function ($params) {
                return $http.get(API_URL + 'Company/companyFinancialInformation', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            assessmentQuestion: {
                question: {
                    get: function (params) {
                        return $http.get(API_URL + 'Company/assessmentQuestion', {params: params}).then(function (response) {
                            return response.data;
                        });
                    }
                },
                answer: {

                    post: function (params) {
                        return Upload.upload({url: API_URL + 'Company/assessmentAnswer', data: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                }
            },
            misUpload: {
                post: function (params) {
                    return $http.post(API_URL + 'Mismaster/saveFacilityPlans', params).then(function (response) {
                        return response.data;
                    });
                },
                updateFacilityPlans: function (params) {
                    return $http.post(API_URL + 'Mismaster/updateFacilityPlans', params).then(function (response) {
                        return response.data;
                    });
                },
                aging: function (params) {
                    return $http.post(API_URL + 'Mismaster/saveAgingAnalysis', params).then(function (response) {
                        return response.data;
                    });
                },
                outstanding: function (params) {
                    return $http.post(API_URL + 'Mismaster/saveOutstandingAmount', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            ratios: {
                get: function (params) {
                    return $http.get(API_URL + 'Crmcompany/ratios', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crmcompany/ratios', params).then(function (response) {
                        return response.data;
                    });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crmcompany/ratios', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            },
            overheadcost: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/getCompanyOverheadCostEstimate', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    console.log(params);
                    return $http.post(API_URL + 'Crm/saveCompanyOverheadCostEstimate', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            companyRatios: {
                list: function (params) {
                    return $http.get(API_URL + 'Crmcompany/companyRatiosList', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                get: function (params) {
                    return $http.get(API_URL + 'Crmcompany/companyRatios', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crmcompany/companyRatios', params).then(function (response) {
                        return response.data;
                    });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crmcompany/companyRatios', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                financialInformation: function (params) {
                    return $http.get(API_URL + 'Crm/financialInformation', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
            },
            ratiosCategory: {
                get: function (params) {
                    return $http.get(API_URL + 'Crmcompany/ratiosCategory', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crmcompany/ratiosCategory', params).then(function (response) {
                        return response.data;
                    });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crmcompany/ratiosCategory', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            },
            customers: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/getcompanycustomer', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crm/saveCompanyCustomer', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            suppliers: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/getCompanySupplier', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crm/savecompanysupplier', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            companyShareholderDetails:{
                post: function (params) {
                    return $http.post(API_URL + 'Crm/crmCompanyOwnership',params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                get: function (params) {
                    return $http.get(API_URL + 'Crm/crmCompanyOwnershipList', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crm/crmCompanyOwnership', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            competitors: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/getCompanyCompetitor', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crm/saveCompanyCompetitor', params).then(function (response) {
                        return response.data;
                    });
                }
            },
            SalesDirectcostEstimate: {
                get: function (params) {
                    return $http.get(API_URL + 'Crm/getCompanySalesDirectCostEstimate', {params: params}).then(function (response) {
                        return response.data;
                    });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Crm/saveCompanySalesDirectCostEstimate', params).then(function (response) {
                        return response.data;
                    });
                }
            },
        }
    })
    //Project
    .factory('crmProjectService', function ($http, $cacheFactory, $rootScope, Upload) {
        return {
            currency: {
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Crm/currency', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            },
            getModules: function ($module_id) {
                return $http.get(API_URL + 'Crm/module/' + $module_id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectTabInfo: function ($params) {
                return $http.get(API_URL + 'Crm/projectTabInfo', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            saveProject: function ($postData) {
                return $http.post(API_URL + 'Crm/project', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectList: function ($params) {
                var str = '';
                return $http.get(API_URL + 'Crm/projectList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            crmProjectList: function ($params) {
                var str = '';
                return $http.get(API_URL + 'Crm/crmProjectList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            /**
             *
             * @param $projectId
             * @param formId
             * @param userId
             * @returns {*}
             */
            projectDetails: function ($params) {
                return $http.get(API_URL + 'Project/info', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectProcessDetails: function ($params) {
                return $http.get(API_URL + 'Project/projectProcess', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            form: function ($params) {
                return $http.get(API_URL + 'Project/forms', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            guarentor: {
                get: function ($params) {
                    return $http.get(API_URL + 'Project/guarantee', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            formFields: {
                get: function ($params) {
                    return $http.get(API_URL + 'Project/formFields', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                post: function ($params) {
                    return $http.post(API_URL + 'Project/formFields', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                stageView: function ($params) {
                    return $http.get(API_URL + 'Project/stageView', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                stageViewDoc: function ($params) {
                    return $http.get(API_URL + 'Documentdownload/stageViewDoc', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                termSheetDoc: function ($params) {
                    return $http.get(API_URL + 'Documentdownload/termSheet', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                facilityTermSheetDoc: function ($params) {
                    return $http.get(API_URL + 'Documentdownload/facilitytermSheet', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                offerLetterDoc: function ($params) {
                    return $http.get(API_URL + 'Documentdownload/offerLetter', {'params': $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                stageViewDocDelete: function ($params) {
                    return $http.post(API_URL + 'Documentdownload/unlinkDocx', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            projectProductTerm: function ($params) {
                return $http.get(API_URL + 'Company/productTerm', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            approvalComments: function ($params) {
                return $http.get(API_URL + 'Crm/approvalComments', {'params': $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            formComments: {
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/formComments', {'params': $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function ($params) {
                    return $http.post(API_URL + 'Crm/formComments', $params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            projectFormDetails: function ($companyId, formId, type) {
                return $http.get(API_URL + 'Crm/projectForm/' + $companyId + '/' + formId + '/' + type)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProjectContactList: function ($companyId, userId) {
                var $params = {};
                $params.company_id = $companyId;
                $params.current_user_id = userId;
                return $http.get(API_URL + 'Company/userList', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectReportingUser: function ($postData) {
                return $http.post(API_URL + 'Activity/approval', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            committeeSecretary: function ($params) {
                return $http.get(API_URL + 'Company/committeeSecretary', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            preApprovalCommittee: {
                get: function ($params) {
                    return $http.get(API_URL + 'Company/preApprovalCommitties', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            preApprovalCommitteeSecretary: {
                get: function ($params) {
                    return $http.get(API_URL + 'Company/preApprovalCommitteeSecretary', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function ($postData) {
                    return $http.post(API_URL + 'Company/preApprovalComment', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            projectFinancals: {
                get: function ($params) {
                    return $http.get(API_URL + 'Project/getProjectFinancials', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function ($postData) {
                    return $http.post(API_URL + 'Project/saveProjectFinancials', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            projectTerm: {
                projectTremesAndConditions: function ($params) {
                    return $http.get(API_URL + 'Crm/terms', {params: $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                projectTremesAndConditionUpdate: function ($postData) {
                    return $http.post(API_URL + 'Crm/productTermItemData', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                projectTerms: function ($params) {
                    return $http.get(API_URL + 'Crm/projectTerms', {params: $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                }
            },
            riskAssessment: {
                /*
                 *@params get company_id
                 * crm_project_id
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/riskCategory', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectRiskFactorsGet: function ($params) {
                    return $http.get(API_URL + 'Crm/projectRiskFactors', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectRiskFactorsPost: function ($postData) {
                    return $http.post(API_URL + 'Crm/projectRiskFactors', $postData)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                /**
                 * @params
                 *crm_project_id
                 *user_id
                 * attribute data array
                 */
                post: function ($params) {
                    return $http.post(API_URL + 'Crm/companyRiskCategory', $params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            statusFlow: {
                /*
                 *@params get company_id
                 * crm_project_id company_id
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/statusFlow', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            facility: {
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/facility', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },

                stageFacilities: function ($params) {
                    return $http.get(API_URL + 'Project/stageFacilities', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                facilityList: function ($params) {
                    return $http.get(API_URL + 'Crm/facilityList', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crm/facility', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                companyFacility: function ($params) {
                    return $http.get(API_URL + 'Crm/companyFacility', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getPreviosFacilitees: function ($params) {
                    return $http.get(API_URL + 'Crm/companyProjectFacilities', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                existingFacility: function ($params) {
                    return $http.post(API_URL + 'Crm/existingFacility', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                submitExposureComments: function ($params) {
                    return $http.post(API_URL + 'Crm/crmProject', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param
                 * id_project_facility
                 crm_project_id
                 currency_id
                 loan_amount
                 expected_start_date
                 expected_maturity
                 expected_maturity_type
                 expected_interest_rate
                 project_loan_type_id
                 project_payment_type_id
                 project_facility_status
                 created_by
                 created_date_time

                 * @returns {*}
                 */
                post: function ($params) {
                    return $http.post(API_URL + 'Crm/facility', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                quickFacility: function ($params) {
                    return $http.post(API_URL + 'Crm/quickFacility', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                facilityDisbursement: function ($params) {
                    return $http.get(API_URL + 'Project/facilityDisbursement', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                facilityLoanReceipts: function ($params) {
                    return $http.get(API_URL + 'Project/facilityLoanReceipts', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                facilityLoanRepayment: function ($params) {
                    return $http.get(API_URL + 'Project/facilityLoanRepayment', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            collateral: {
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                collateralProjectTabInfo: function ($params) {
                    return $http.get(API_URL + 'Crm/collateralProjectTabInfo', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                collateralProjectList: function ($params) {
                    return $http.get(API_URL + 'Crm/collateralProjectList', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                getList: function ($params) {
                    return $http.get(API_URL + 'Crm/collateralList', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/collateral', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Crm/collateral', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param
                 * id_project_facility
                 crm_project_id
                 currency_id
                 loan_amount
                 expected_start_date
                 expected_maturity
                 expected_maturity_type
                 expected_interest_rate
                 project_loan_type_id
                 project_payment_type_id
                 project_facility_status
                 created_by
                 created_date_time

                 * @returns {*}
                 */
                post: function ($params) {
                    $params.company_id = $rootScope.id_company;
                    return $http.post(API_URL + 'Crm/collateral', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param params company_id
                 * @returns {*|Promise.<T>}
                 */
                collateralType: function (params) {
                    return $http.get(API_URL + 'Company/collateralType', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                collateralTypeFormData: {
                    /**
                     *
                     * @param collateral_id,collateral_type_id
                     * @returns {*|Promise.<T>}
                     */
                    get: function (params) {
                        return $http.get(API_URL + 'Crm/collateralTypeFormData', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    /**
                     * collateral_id,collateral_type_id form data key values
                     * @param params
                     */
                    post: function (params) {
                        return $http.post(API_URL + 'Crm/collateralTypeFormData', params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                collateralStageFormData: {
                    get: function (params) {
                        return $http.get(API_URL + 'Crm/collateralStageFormData', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Crm/collateralStageFormData', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function ($postData) {
                        return $http.delete(API_URL + 'Crm/collateralStageFormData', {params: $postData})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                collateralQuestion: {
                    /**
                     *
                     * @param collateral_id,collateral_type_id
                     * @returns {*|Promise.<T>}
                     */
                    get: function (params) {
                        return $http.get(API_URL + 'Crm/collateralTypeDocuments', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                documents: {
                    /**
                     *
                     * @param collateral_id
                     * @returns {*}
                     */
                    get: function (params) {
                        return $http.get(API_URL + 'Crm/collateralDocument', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    /**
                     *
                     * @param post_type = 'file_upload'
                     * company_id
                     collateral_id
                     date
                     due_date
                     type
                     * @returns {*}
                     */
                    post: function (params) {
                        return $http.post(API_URL + 'Crm/collateralDocument', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                }
            },
            covenant: {
                /**
                 *
                 * @param crm_project_id , company_id
                 * @returns {*|Promise.<T>}
                 */
                get: function (params) {
                    return $http.get(API_URL + 'Crm/covenant', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param crm_project_id , company_id
                 * @returns {*|Promise.<T>}
                 */
                projectCovenant: function (params) {
                    return $http.get(API_URL + 'Crm/projectCovenant', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getCovenantsCategoryItems: function (params) {
                    return $http.get(API_URL + 'Crm/covenant', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 *
                 * @param crm_project_id , company_id , covenant_ids
                 * @returns {*|Promise.<T>}
                 */
                post: function (params) {
                    return $http.post(API_URL + 'Crm/covenant', params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            workflow: {
                /**
                 *
                 * @param project_stage_info_id=40
                 * @returns {*|Promise.<T>}
                 */
                getAction: function (params) {
                    return $http.get(API_URL + 'Workflow/actions', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                postAction: function (params) {
                    return Upload.upload({url: API_URL + 'Workflow/actions', data: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                actionDetails: function (params) {
                    return $http.get(API_URL + 'Workflow/actionDetails', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectStageWorkflow: function (params) {
                    return $http.get(API_URL + 'Workflow/projectStageWorkflow', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectStageInfoDetail: function (params) {
                    return $http.get(API_URL + 'Project/projectStageInfoDetail', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectStageComments: {
                    get: function (params) {
                        return $http.get(API_URL + 'Workflow/comments', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return Upload.upload({url: API_URL + 'Workflow/comments', data: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }

            },
            projectLegalCheckList: {
                getCheckList: function (params) {
                    return $http.get(API_URL + 'Project/projectLegalCheckList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getProjectCheckList: function (params) {
                    return $http.get(API_URL + 'Crmcompany/projectCheckList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getUnselectedCheckList: function (params) {
                    return $http.get(API_URL + 'Crmcompany/checkList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                postSelectedCheckList: function (params) {
                    return $http.post(API_URL + 'Crmcompany/projectCheckList',  params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                deleteCheckList: function (params) {
                    return $http.delete(API_URL + 'Crmcompany/projectCheckList',  {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })
    .factory('taskService', function ($http,Upload) {
        return {
            saveTask: function ($postData) {
                return Upload.upload({
                    url: API_URL + 'Activity/task',
                    data: $postData
                }).then(function (response) {
                    return response.data;
                });
            },
            getTasks: function ($params,loaderId) {
                return $http.get(API_URL + 'Activity/task', {params: $params,loaderId:loaderId})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteTask: function ($params) {
                return $http.delete(API_URL + 'Activity/task', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            updateTaskStatus: function ($params) {
                return $http.post(API_URL + 'Activity/updateTask', $params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            }
        }
    })
    .factory('notificationService', function ($http, $cacheFactory, $rootScope) {
        return {
            /**
             *
             * @param userId
             * @returns {*}
             */
            count: function ($params) {
                return $http.get(API_URL + 'Notifications/notificationCount', {params: $params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            list: function ($params, loaderId) {

                return $http.get(API_URL + 'Notifications/getList', {params: $params, 'loaderId': loaderId})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            moduleList: function ($params, loaderId) {

                return $http.get(API_URL + 'Notifications/module', {params: $params, 'loaderId': loaderId})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            changeStatus: function ($postData) {
                return $http.post(API_URL + 'Notifications/notification', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
        }
    })
    //quickService
    .factory('quickService', function ($http, $cacheFactory, $rootScope, transformRequestAsFormPost) {
        return {
            saveQuickContact: function ($postData) {
                return $http.post(API_URL + 'Crm/quickContact', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveQuickCompany: function ($postData) {
                return $http.post(API_URL + 'Crm/quickCompany', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            saveQuickProject: function ($postData) {
                return $http.post(API_URL + 'Crm/quickProject', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteContact: function ($postData) {
                return $http.delete(API_URL + 'Cleanup/contact', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteCompany: function ($postData) {
                return $http.delete(API_URL + 'Cleanup/company', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            deleteProject: function ($postData) {
                return $http.delete(API_URL + 'Cleanup/project', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            }
        }
    })
    .factory('Resource', ['$q', '$filter', '$timeout', function ($q, $filter, $timeout) {

        //this would be the service to call your server, a standard bridge between your model an $http
        // the database (normally on your server)
        var randomsItems = [];

        function createRandomItem(id) {
            var heroes = ['Batman', 'Superman', 'Robin', 'Thor', 'Hulk', 'Niki Larson', 'Stark', 'Bob Leponge'];
            return {
                id: id,
                name: heroes[Math.floor(Math.random() * 7)],
                age: Math.floor(Math.random() * 1000),
                saved: Math.floor(Math.random() * 10000)
            };
        }

        for (var i = 0; i < 1000; i++) {
            randomsItems.push(createRandomItem(i));
        }
        //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
        //in our case, it actually performs the logic which would happened in the server
        function getPage(start, number, params) {
            console.log(params);
            var deferred = $q.defer();
            var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;
            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }
            var result = filtered.slice(start, start + number);
            $timeout(function () {
                //note, the server passes the information about the data set size
                deferred.resolve({
                    data: result,
                    numberOfPages: Math.ceil(filtered.length / number)
                });
            }, 1500);
            return deferred.promise;
        }

        return {
            getPage: getPage
        };
    }])
    .factory('projectService', function ($http, $cacheFactory, $rootScope, transformRequestAsFormPost, Upload) {
        return {
            projectContact: function ($postData) {
                return $http.post(API_URL + 'Crm/projectContact', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProjectContact: function ($postData) {
                return $http.get(API_URL + 'Crm/projectContact', {params: $postData})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            addProjectTeam: function ($postData) {
                return $http.post(API_URL + 'Crm/projectTeam', $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProjectTeamContact: function (id) {
                return $http.get(API_URL + 'Company/branchUsers/?user_id=' + id)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            projectApprovals: function (params) {
                return $http.get(API_URL + 'Activity/activity/?' + params)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            facility: {
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Project/facilities', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            monitoring: {
                post: function ($params) {
                    return $http.post(API_URL + 'Crm/monitoring', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    return $http.get(API_URL + 'Crm/monitoring', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            },
            disbursementRequest: {
                post: function ($params) {
                    return $http.post(API_URL + 'Project/disbursementRequest', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/disbursementRequest', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/disbursementRequest', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                download: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Documentdownload/downloadDisbursementRequest', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }

            },
            riskAssessment: {
                post: function (params) {
                    return Upload.upload({
                        async: true,
                        url: API_URL + 'Project/riskAssessment',
                        data: params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/riskAssessment', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/riskAssessment', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            supervisionAssessment: {
                post: function ($params) {
                    return Upload.upload({
                        url: API_URL + 'Project/supervisionAssessment',
                        data: $params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/supervisionAssessment', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/supervisionAssessment', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                download: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Documentdownload/downloadSupervisionReport', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }

            },
            supervisionProjectCommittee: {
                post: function ($params) {
                    return $http.post(API_URL + 'Project/supervisionProjectCommittee', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/supervisionProjectCommittee', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/supervisionProjectCommittee', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            initialSupervisionReport: {
                post: function (params) {
                    return Upload.upload({
                        async: true,
                        url: API_URL + 'Project/initialSupervisionReport',
                        data: params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/initialSupervisionReport', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/initialSupervisionReport', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            annualReview: {
                post: function (params) {
                    return Upload.upload({
                        async: true,
                        url: API_URL + 'Project/annualReview',
                        data: params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/annualReview', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/annualReview', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            ongoingMonitoring: {
                post: function (params) {
                    return Upload.upload({
                        async: true,
                        url: API_URL + 'Project/ongoingMonitoring',
                        data: params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/ongoingMonitoring', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/ongoingMonitoring', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            otherMonitoring: {
                post: function (params) {
                    return Upload.upload({
                        async: true,
                        url: API_URL + 'Project/otherMonitoring',
                        data: params
                    }).then(function (response) {
                        return response.data;
                    });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/otherMonitoring', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/otherMonitoring', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            grade: {
                post: function ($params) {
                    return $http.post(API_URL + 'Project/esGrade', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/esGrade', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    //if get by id @ id_meeting
                    return $http.delete(API_URL + 'Project/esGrade', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            ProjectMeeting: {
                /**
                 *
                 * @param $params
                 * company_id   user_id
                 * @returns {*}
                 */
                getMeetingGuest: function ($params) {
                    return $http.get(API_URL + 'Activity/meetingGuest', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function ($params) {
                    return $http.post(API_URL + 'Activity/meeting', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Activity/meeting', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Activity/meeting', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            version: {
                post: function ($params) {
                    return $http.post(API_URL + 'Project/projectStageVersion', $params)
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    //if get by id @ id_meeting
                    return $http.get(API_URL + 'Project/stageVersion', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }
        }
    })
    .factory('productService', function ($http, $cacheFactory, $rootScope, transformRequestAsFormPost) {
        return {
            getProductData: function ($postData) {
                return $http.get(API_URL + 'Company/productTermItem/?product_term_key=' + $postData)
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getProductsList : function(params){
                return $http.get(API_URL + 'Crm/product', {'params' : params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            postProduct : function(params){
                return $http.post(API_URL + 'Crm/product', params).then(function (response) {
                    return response.data;
                });
            },
            getProductStages : function(params) {
                return $http.get(API_URL + 'Crm/projectStage',{'params' : params})
                    .error(function () {
                    }).then(function (response) {
                        return response.data;
                    });
            },
            getManageActionsList : function (params) {
                return $http.get(API_URL + 'Crm/productStageWorkflowActionsSettings', {'params' : params})
                    .error(function () {
                    }).then(function(response){
                        return response.data;
                    });
            },
            postWorkflowActions : function(params) {
                return $http.post(API_URL + 'Crm/productStageWorkflowActionsSettings', params).then(function(response){
                    return response.data;
                });
            }
        }
    })
    .factory('activityService', function ($http, $cacheFactory, $rootScope) {
        return {
            discussion: {
                /*
                 *@params get discussion_type, disussion_reference_id
                 */
                get: function ($params) {
                    return $http.get(API_URL + 'Activity/discussion', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /**
                 * @params discussion_type, discussion_reference_id,created_by,discussion_description
                 */
                post: function ($params) {
                    return $http.post(API_URL + 'Activity/discussion', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            }
        }
    })
    .factory('covenantService', function ($http, $cacheFactory, $rootScope,Upload) {
        return {
            get: function ($params) {
                return $http.get(API_URL + 'Covenants/list', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            post: function ($params) {
                return $http.post(API_URL + 'Covenants/addCovenant', $params)
                    .then(function (response) {
                        return response.data;
                    });
            },
            delete: function ($params) {
                return $http.delete(API_URL + 'Covenants/covenant', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            covenantType: function ($params) {
                return $http.get(API_URL + 'Covenants/covenantType', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            search: function ($params) {
                return $http.get(API_URL + 'Covenants/search', {params: $params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            crmCovenant: {
                post: function ($params) {
                    return $http.post(API_URL + 'Covenants/moduleCovenant', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                get: function ($params) {
                    return $http.get(API_URL + 'Covenants/moduleCovenant', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                update: function ($params) {
                    return $http.post(API_URL + 'Covenants/update', $params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function ($params) {
                    return $http.delete(API_URL + 'Covenants/moduleCovenant', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                taskHistory: function ($params) {
                    return $http.get(API_URL + 'Covenants/taskHistory', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                taskActionHistory: function ($params) {
                    return $http.get(API_URL + 'Covenants/taskActionHistory', {params: $params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                updateTaskStatus: function ($params) {
                   return Upload.upload({
                        async: true,
                        url: API_URL + 'Covenants/task',
                        data: $params
                    }).then(function (response) {
                        return response.data;
                    })
                }

            }

        }
    })
    .factory('collateralService', function ($http, $cacheFactory, $rootScope,Upload) {
        return {
            get: function (params) {
                return $http.get(API_URL + 'Collateral/type', {params: params})
                    .then(function (response) {
                        return response.data;
                    });
            },
            typeFields: {
                get: function (params) {
                    return $http.get(API_URL + 'Collateral/typeFields', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Collateral/typeFields', params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            facility: {
                get: function (params) {
                    return $http.get(API_URL + 'Collateral/facility', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Collateral/facility', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function (params) {
                    return $http.delete(API_URL + 'Collateral/facility', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            stage: {
                get: function (params) {
                    return $http.get(API_URL + 'Collateral/stages', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Collateral/stages', params)
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            documents:{
                get: function (params) {
                    return $http.get(API_URL + 'Collateral/document', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post: function (params) {
                    return $http.post(API_URL + 'Collateral/document', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function (params) {
                    return $http.delete(API_URL + 'Collateral/document', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            },
            crm: {
                get: function (params) {
                    return $http.get(API_URL + 'Collateral/list', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                delete: function (params) {
                    return $http.delete(API_URL + 'Collateral/collateral', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                //add new collateral
                post: function (params) {
                    return $http.post(API_URL + 'Collateral/add', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                otherCollateral: function (params) {
                    return $http.get(API_URL + 'Collateral/otherFacilityCollateral', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                typeFormData: {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/typeFormData', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/typeFormData', params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                assessmentQuestions: {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/assessmentQuestions', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                contact: {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/contact', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/contact', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    //add new collateral
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/contact', params)
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                valuationDetails: {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/valuationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return Upload.upload({
                            async: true,
                            url: API_URL + 'Collateral/valuationDetails',
                            data: params
                        }).then(function (response) {
                            return response.data;
                        });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/valuationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                fieldInvestigation : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/fieldInvestigationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return Upload.upload({
                            async: true,
                            url: API_URL + 'Collateral/fieldInvestigationDetails',
                            data: params
                        }).then(function (response) {
                            return response.data;
                        });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/fieldInvestigationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                externalCheck : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/externalCheckDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/externalCheckDetails', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/externalCheckDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                riskEvaluation : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/riskEvaluationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/riskEvaluationDetails', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/riskEvaluationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                legalOpinion : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/legalOpinionDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/legalOpinionDetails', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/legalOpinionDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                },
                securityValuationDetails : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/securityValuationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/securityValuationDetails', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/securityValuationDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                insuranceDetails : {
                    get: function (params) {
                        return $http.get(API_URL + 'Collateral/insuranceDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    post: function (params) {
                        return $http.post(API_URL + 'Collateral/insuranceDetails', params)
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    delete: function (params) {
                        return $http.delete(API_URL + 'Collateral/insuranceDetails', {params: params})
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                facilitySearch:function(params,loaderId){
                    return $http.get(API_URL + 'Collateral/facilitySearch', {params: params,'loaderId':loaderId})
                        .then(function (response) {
                            return response.data;
                        });
                },
                moduleCovenant: function (params) {
                    return $http.post(API_URL + 'Collateral/moduleCovenant', params)
                        .then(function (response) {
                            return response.data;
                        });
                }


            }

        }
    })

    .factory('crmDashboardService', function ($http, $cacheFactory, $rootScope){
        return {
            loanOfficer: {
                getDetails: function (params) {
                    return $http.get(API_URL + 'Dashboard/loanOfficerDashboardDetails', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                loanOfficerStageAmountDetails: function (params) {
                    return $http.get(API_URL + 'Dashboard/loanOfficerDashboardStageAmountDetails', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                loanOfficerDashboardSectorWiseLoans: function (params) {
                    return $http.get(API_URL + 'Dashboard/loanOfficerDashboardSectorWiseLoans', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                childUsers:function(params) {
                    return $http.get(API_URL + 'Dashboard/childUsers', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                projectList:function(params) {
                    return $http.get(API_URL + 'Crm/projectList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                FacilityCollections:function(params) {
                    return $http.get(API_URL + 'Dashboard/loanOfficerDashboardCollections', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                userMapCountryList:function(params) {
                    return $http.get(API_URL + 'Dashboard/loanOfficerDashboardMapData', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                MapCountryWiseProjects:function(params) {
                    return $http.get(API_URL + 'Dashboard/dashboardMapCountryWiseProjectList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                DashboardNotifications:function(params) {
                    return $http.get(API_URL + 'Dashboard/dashboardNotifications', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                DashboardCovenantsList:function(params) {
                    return $http.get(API_URL + 'Covenants/covenantList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                DashboardTasksList:function(params) {
                    return $http.get(API_URL + 'Activity/task', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                DashboardKnowledgeList:function(params) {
                    return $http.get(API_URL + 'Company/knowledgeList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                }
            }
        }
    })

    .factory('crmFacilityService', function ($http, $cacheFactory, $rootScope) {
        return {
            crm: {
                get: function (params) {
                    return $http.get(API_URL + 'Facility/facilityList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                post:function(params){
                    return $http.post(API_URL + 'Facility/facility', params)
                        .then(function (response) {
                            return response.data;
                        });
                },
                contactExposure: function (params) {
                    return $http.get(API_URL + 'Facility/clientFacilities', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                companyExposure: function (params) {
                    return $http.get(API_URL + 'Facility/companyFacilities', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                clientGuaranteeFacilities: function (params) {
                    return $http.get(API_URL + 'Facility/clientGuaranteeFacilities', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getById: function (params) {
                    return $http.get(API_URL + 'Facility/facilityDetails', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })
    .factory('crmActivityService', function ($http, $cacheFactory, $rootScope) {
        return {
            crm: {
                get: function (params) {
                    return $http.get(API_URL + 'Activity/taskList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })
    .factory('crmMeetingService', function ($http, $cacheFactory, $rootScope) {
        return {
            crm: {
                get: function (params) {
                    return $http.get(API_URL + 'Activity/meetingList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })
    .factory('crmCovenantService', function ($http, $cacheFactory, $rootScope) {
        return {
            crm: {
                get: function (params) {
                    return $http.get(API_URL + 'Covenants/covenantList', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })


    .service('anchorSmoothScroll', function () {
        this.scrollTo = function (eID) {

            // This scrolling function
            // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY);
                return;
            }
            var speed = Math.round(distance / 100);
            if (speed >= 20) speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (var i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step;
                    if (leapY > stopY) leapY = stopY;
                    timer++;
                }
                return;
            }
            for (var i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step;
                if (leapY < stopY) leapY = stopY;
                timer++;
            }
            function currentYPosition() {
                // Firefox, Chrome, Opera, Safari
                if (self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID) {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop - 165;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) {
                    node = node.offsetParent;
                    y += node.offsetTop;
                }
                return y;
            }
        };
    })

    .factory('reportService', function ($http, $rootScope) {
        return {
            reports: {
                availableAnalysisYears: function (params) {
                    return $http.get(API_URL + 'reports/availableAnalysis', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                availableAnalysisCountries: function (params) {
                    return $http.get(API_URL + 'reports/analysisCountries', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                availableAnalysisProducts: function (params) {
                    return $http.get(API_URL + 'reports/analysisProducts', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                availableAnalysisSectors: function (params) {
                    return $http.get(API_URL + 'reports/analysisSectors', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },

                getProductOutstandingPortfolio: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByProduct', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getProductClients: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByProductClients', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getCountryClients: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByCountryClients', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getPortpolioByCountry: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByCountry', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getPortfolioByCurrency: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByCurrency', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getPortfolioBySector: function (params) {
                    return $http.get(API_URL + 'reports/portfolioBySector', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getPortfolioByEsGrade: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByESGrade', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                getProjectRiskList: function ($params) {
                    var str = '';
                    return $http.get(API_URL + 'reports/risk', {params: $params})
                        .error(function () {
                        }).then(function (response) {
                            return response.data;
                        });
                },
                outstandingExposureByCurrency: function (params) {
                    return $http.get(API_URL + 'reports/outstandingExposureByCurrency', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                outstandingExposureByCountry: function (params) {
                    return $http.get(API_URL + 'reports/outstandingExposureByCountry', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                outstandingExposureBySector: function (params) {
                    return $http.get(API_URL + 'reports/outstandingExposureBySector', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                outstandingExposureByProduct: function (params) {
                    return $http.get(API_URL + 'reports/outstandingExposureByProduct', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                portfolioByCurrencyAnalysis: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByCurrencyAnalysis', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                portfolioBySectorAnalysis: function (params) {
                    return $http.get(API_URL + 'reports/portfolioBySectorAnalysis', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                portfolioByCountryAnalysis: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByCountryAnalysis', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                portfolioByProductAnalysis: function (params) {
                    return $http.get(API_URL + 'reports/portfolioByProductAnalysis', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                outStandingStatus: function (params) {
                    return $http.get(API_URL + 'reports/outStandingStatus', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                /*Not using*/
                outstandingBorrowers: function (params) {
                    return $http.get(API_URL + 'reports/outstandingBorrowers', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                valueAtRiskReports: function (params) {
                    return $http.get(API_URL + 'reports/valueAtRisk', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
                sectorLimits: function (params) {
                    return $http.get(API_URL + 'reports/sectorLimits', {params: params})
                        .then(function (response) {
                            return response.data;
                        });
                },
            }

        }
    })


.factory("confirm", function ($window, $q, $timeout) {

    // Define promise-based confirm() method.
    function confirm(custom_message) {
        var message = 'Do you want to continue?';
        if(custom_message)
            message = custom_message;
        var defer = $q.defer();

        $timeout(function () {
            if ($window.confirm(message)) {
                defer.resolve(true);
            }
            else {
                defer.reject(false);
            }
        }, 0, false);

        return defer.promise;
    }

    return confirm;
})




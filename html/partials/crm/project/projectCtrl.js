/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('app')
    .controller('projectCtrl', function ($scope, $rootScope, $cookies, crmProjectService, masterService, crmCompanyService, projectService) {
        console.log('projectCtrl');
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }

        $scope.projectDetails = {};
        $scope.getProjectDetails = function (project_id) {
            var params = {};
            params.user_id = $rootScope.userId;
            params.crm_project_id = $rootScope.currentProjectViewId;
            params.module_id = 3;
            crmProjectService.projectDetails(params).then(function (result) {
                if (result.status) {
                    $scope.projectDetails = result.data;
                    $rootScope.projectTitle = result.data.project_title;
                }
            })
        }
        $scope.projectProcessDetails = [];
        $scope.getProjectProcessDetails = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            crmProjectService.projectProcessDetails(params).then(function (result) {
                if (result.status) {
                    $scope.projectProcessDetails = result.data;
                }
            })
        }
        $rootScope.selectedCompanyList = [];
        $rootScope.selectedCompanyListFun = function () {
            crmCompanyService.projectCompanyListFun({'crm_project_id': $rootScope.currentProjectViewId}).then(function (result) {
                $rootScope.selectedCompanyList = result.data;
            });
        };
        $rootScope.projectContactList = [];
        $rootScope.projectContactListFun = function () {
            projectService.getProjectContact({'crm_project_id': $rootScope.currentProjectViewId}).then(function (result) {
                $rootScope.projectContactList = result.data;
            });
        };

        $scope.parentSectorsList = [];
        $scope.getSector = function () {
            masterService.sector.getAll({'parent_sector_id': 0}).then(function (result) {
                $scope.parentSectorsList = result.data.data;
            });
        }
        $scope.getSubSector = function (parentId) {
            masterService.sector.getSubSector({'id_sector': parentId}).then(function (result) {
                $scope.subSectorList = result.data;
            })
        }
        $scope.countries = [];
        $scope.getCountries = function () {
            masterService.country.get().then(function (result) {
                $scope.countries = result.data;
            });
        }
        $scope.jsonConvert = function (data) {
            if (data) {
                return JSON.parse(data);
            }

        }
        $scope.multipleCheckbox = function (data) {

            if (data) {
                var _array = [];
                // var obj =  JSON.parse(data);
                angular.forEach(data, function (value, key) {
                    if (value) {
                        if (key == 'company_sme_business') {
                            _array.push('SME Business');
                        }
                        if (key == 'company_incorporate_business') {
                            _array.push('Corporate Business');
                        }
                        if (key == 'company_construction') {
                            _array.push('Construction');
                        }
                        if (key == 'company_acquisition') {
                            _array.push('Acquisition');
                        }
                    }
                })
                return _array.toString();
            }
        }

        $scope.changeVersion = function (stage, selectedVersion) {
            stage.selectedVersion = selectedVersion;
        }
        $scope.createNewVersion = function (stage) {
            console.log('stage', stage);
            var conf = confirm('Do you want to create new version for this stage?');
            var param = {};
            param.project_stage_info_id = stage.id_project_stage_info;
            param.created_by = $rootScope.userId;
            if (conf) {
                projectService.version.post(param).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        stage.project_stage_versions = result.data.project_stage_versions;
                    }
                })
            }
        }
        $rootScope.versionDetails = {};
        $scope.getStageVersion = function () {
            var param = {};
            param.project_id = $rootScope.currentProjectViewId;
            //param.created_by = $rootScope.userId;
            param.version_number = $rootScope.selectedVersion;
            param.stage_id = $rootScope.stageId;

            projectService.version.get(param).then(function (result) {
                $rootScope.versionDetails = result.data[0];
            });
        }
        $scope.formChangeEvent = function(){
            $rootScope.isFormChanged=true;
        };
    })
    .controller('ProjectListCtrl', function ($scope, $filter, $rootScope, crmCompanyService, crmProjectService, $state, quickService, masterService, encodeFilter) {
        $scope.projectList = [];
        $scope.totalProjectCount = 0;
        $scope.isSingleProductType = false;
        $scope.gotoState = function (project) {
            var id = encodeFilter(project.id_crm_project);
            $rootScope.currentProjectViewId = project.id_crm_project;
            if (project.user_access == 'edit') {
                $state.go('project.project-view', {'id': id});
            } else if (project.user_access == 'status_flow') {
                $state.go('project.project-approval-level.status-flow', {'id': id});
            } else {
                $state.go('project.project-overview', {'id': id});
            }
        }
        $scope.getCrmCompanyList = function ($search_key) {
            console.log('$search_key', $search_key);
            return crmCompanyService.getAllCompanyList({
                'company_id': $rootScope.id_company,
                'search_key': $search_key
            }).then(function (result) {
                $scope.companyList = result.data;
                return result.data;
            });
        }
        $scope.getProjectTabInfo = function () {
            crmProjectService.projectTabInfo({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.projectTabInfo = result.data;
            });
        }

        $scope.getProductList = function () {
            masterService.getProductType({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.productTypeList = result.data.data;
                if ($scope.productTypeList.length == 1) {
                    $scope.isSingleProductType = true;
                    $scope.project.product_id = $scope.productTypeList[0].id_product;
                }
            });
        }
        //data tables
        $scope.displayed = false;
        $scope.category = '';
        $scope.callServer = function (tableState) {
            tableState.company_id = $rootScope.id_company;
            crmProjectService.crmProjectList(tableState).then(function (result) {
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.projectList = result.data;
                $scope.pagination = false;
                $scope.pagination = true;
                /*var count = result.data[tableState.search.category].count;
                tableState.pagination.numberOfPages = Math.ceil(count / tableState.pagination.number);*/
                $scope.isEmptyRow = (result.data.length == 0) ? true : false;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records / tableState.pagination.number);
                console.log('number',tableState.pagination.number);
                console.log('totlal',result.total_records);
            });
        };
        /*$scope.callServer=function(tableState){
            tableState.company_id=$rootScope.id_company;
            $scope.tableStateRef=tableState;
            crmProjectService.crmProjectList(tableState).then(function(result){
                console.log('result : 123',result);
                $scope.displayed=true;
                $scope.projectList=result.data;
                $scope.isEmptyRow = (result.data.length == 0) ? true : false;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records / tableState.pagination.number);
            });
        };*/
        //table filter  change status
        $scope.changeStatus = function (categoryType) {
            $scope.category = categoryType;
            $scope.tableStateRef.search.category = categoryType;
            $scope.callServer($scope.tableStateRef);
        }
        $scope.project = {};
        $scope.submitQuickProject = function ($inputData, form, moreFields) {
            $inputData.company_id = $rootScope.id_company;
            $inputData.created_by = $rootScope.userId;
            $inputData.project_description = $inputData.project_description;
            quickService.saveQuickProject($inputData).then(function (result) {
                if (result.status) {
                    form.$setPristine();
                    $rootScope.toast('Success', result.message);
                    $scope.project = {};
                    $scope.callServer($scope.tableStateRef);
                    if (moreFields) {
                        $rootScope.currentProjectViewId = result.data.crm_project_id;
                        $state.go('project.project-view', {'id': encodeFilter($rootScope.currentProjectViewId)});
                    }
                } else {
                    $rootScope.toast('Error', result.error, 'error', result.error);
                }
            })
        };

        $scope.deleteProject = function(row) {
            var r=confirm('Are you sure you want to delete? Once deleted cannot be reverted');
            if(r) {
                quickService.deleteProject({'project_id': row.id_crm_project}).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.callServer($scope.tableStateRef);
                    }
                })
            }
        };

        //initial call
        $scope.getProjectTabInfo();
    })
    .controller('ProjectViewCtrl', function ($cookieStore, $rootScope, $scope, $sce, $q, crmProjectService, $state, $uibModal, crmContactService, crmCompanyService, projectService, taskService, $stateParams, decodeFilter, encodeFilter) {
        $scope.options = {
            minDate: new Date(),
        };
        $rootScope.currentProjectViewId = decodeFilter($stateParams.id);
        $rootScope.contactId = $rootScope.currentProjectViewId;
        $scope.showProjectList = true;
        if ($rootScope.currentProjectViewId == undefined) {
            $state.go('project.project-dashboard');
            return false;
        }
        $scope.stageList = {};
        $scope.formList = [];
        $scope.getProjectForms = function () {
            crmProjectService.form({'crm_project_id': $rootScope.currentProjectViewId}).then(function (result) {
                if (result.data) {
                    $scope.stageList = result.data;
                    angular.forEach($scope.stageList, function (stages) {
                        angular.forEach(stages.section, function (section) {
                            angular.forEach(section.form, function (form) {
                                var obj = {};
                                obj.stageId = stages.id_project_stage;
                                obj.formId = form.id_project_stage_section_form;
                                obj.projectId = $rootScope.currentProjectViewId;
                                $scope.formList.push(obj);
                            })
                        })
                    })
                    $cookieStore.put('cFormList', $scope.formList)
                }
            })
        }
        $scope.goToEditCtrl = function (stage, form) {
            var param = {};
            param.formId = encodeFilter(form.id_project_stage_section_form);
            param.stageId = encodeFilter(stage.id_project_stage);
            param.projectId = $stateParams.id;
            console.log('stage.selectedVersion', stage);
            param.selectedVersion = (stage.hasOwnProperty('selectedVersion')) ? stage.selectedVersion.version_number : 1;
            param.currentVersion = stage.project_stage_versions.length;
            $state.go("project.project-form-view", param);
        };
        $scope.goToState = function (data) {
            $state.go(data);
        }
        $scope.getStatus = function (form) {
            $scope.status = false;
            angular.forEach($rootScope.formStatus, function (formStatus, index) {
                if (formStatus.id == form.id_form && formStatus.status == 1) {
                    $scope.status = true;
                }
            });
            if ($scope.status) {
                return "box-active";
            }
        }

        $rootScope.projecTeamtContactList = [];
        $rootScope.projectTeamContactListFun = function () {
            projectService.getProjectTeamContact($rootScope.userId).then(function (result) {
                $rootScope.projectTeamContactList = result.data;
            });
        };

        $rootScope.projectApprovalsList = [];
        $rootScope.projectApprovalsFun = function () {
            var getParams = 'project_id=' + $rootScope.currentProjectViewId;
            if ($scope.activity != '') {
                getParams = getParams + '&activity_type=' + $scope.activity;
            }
            projectService.projectApprovals(getParams).then(function (result) {
                $rootScope.projectApprovalsList = result.data;
            });
        };

        $scope.quickAttachmentModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                template: '<div class="right-modal modal-custom" data-modal="attachment">' +
                ' <h3 class="right-modal-head">' +
                '<i class="fa fa-paperclip mr8"></i> {{"Attachments" | translate}}' +
                ' <a class="close-right pull-right" ng-click="cancel()" n-t=\'{"e_n":"Close modal","e_d":""}\'><img src="images/close.png"></a>' +
                '</h3>' +
                '<attachment-view module-id="3" ref-id="{{currentProjectViewId}}"></attachment-view>' +
                '</div>',
                controller: function ($scope, $uibModalInstance) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                // $rootScope.$emit("reloadTouchPoint");
                // $scope.init();
                // $state.reload();
            });
        }
        /*Added Contacts Modal*/
        $rootScope.AddedContactsModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/project/added-contacts.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService, projectService) {
                    $rootScope.projectContactListFun();
                    $scope.getContactList = function (search_key) {
                        $scope.visibleAddButton = false;
                        return crmContactService.contactList({
                            'company_id': $rootScope.id_company,
                            'crm_project_id': $rootScope.currentProjectViewId,
                            'search_key': search_key
                        }).then(function (result) {
                            return result.data.data;
                        });
                    };
                    $scope.deleteProjectContact = function (contactId) {
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            return crmContactService.deleteProjectContact(contactId).then(function (result) {
                                $rootScope.toast('Success', result.data.message);
                                $rootScope.projectContactListFun();
                            });
                        }
                    };
                    $scope.onSelect = function ($item, $model, $label) {
                        $scope.visibleAddButton = true;
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                        crmContactService.contactType().then(function (response) {
                            $scope.contactType = response.data;
                        });
                    };
                    $scope.save = function (designation, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var fields = {
                                'crm_project_id': $rootScope.currentProjectViewId,
                                'crm_contact_id': $scope.selectedItem.id_crm_contact,
                                'created_by': $rootScope.userId,
                                contact_type_id: designation.contact_type_id
                            }
                            projectService.projectContact(fields).then(function (result) {
                                if (result.status) {
                                    $scope.visibleDesignationDropDown = false;
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.projectContactListFun();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
                $state.reload();
            }, function () {
            });
        };
        /*Added Companies Modal*/
        $rootScope.AddedCompaniesModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/project/added-companies.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService) {
                    //initial calls
                    $scope.selectedCompanyListFun();
                    $scope.getCompanyList = function (search_key) {
                        $scope.visibleAddButton = false;
                        return crmCompanyService.getCompanyList({
                            'crm_project_id': $rootScope.currentProjectViewId,
                            'search_key': search_key
                        }).then(function (result) {
                            return result.data.data;
                        });
                    };
                    $scope.onSelect = function ($item, $model, $label) {
                        $scope.visibleAddButton = true;
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                        crmCompanyService.companyType().then(function (response) {
                            $scope.companyType = response.data;
                        });
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function (designation, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var fields = {
                                'crm_project_id': $rootScope.currentProjectViewId,
                                'crm_company_id': $scope.selectedItem.id_crm_company,
                                'company_type_id': designation.company_type_id,
                                'created_by': $rootScope.userId
                            };
                            crmCompanyService.projectCompany(fields).then(function (result) {
                                if (result.status) {
                                    $scope.visibleDesignationDropDown = false;
                                    $rootScope.toast('Success', result.message);
                                    $scope.selectedCompanyListFun();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.deleteProjectCompany = function ($id) {
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            crmCompanyService.deleteProjectCompany($id).then(function (result) {
                                $rootScope.toast('Success', result.data.message);
                                $rootScope.selectedCompanyListFun();
                            });
                        }
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
                $state.reload();
            }, function () {
            });
        };

        $scope.getProjectStageWorkflow = function (stageInfoId) {
            crmProjectService.workflow.projectStageWorkflow({project_stage_info_id: stageInfoId}).then(function (result) {
                $scope.stageWorkFlow = result.data;
            })
        }
        //initial call
        $scope.getProjectForms();
        $scope.postField = {'reminder_date': new Date()};

    })
    .controller('ProjectFormViewCtrl', function ($cookieStore, $rootScope, $scope, $sce, crmProjectService, $state, $uibModal, crmContactService, crmCompanyService, masterService, projectService, taskService, $stateParams, decodeFilter, encodeFilter) {
        $rootScope.currentProjectViewId = decodeFilter($stateParams.projectId);
        $rootScope.formId = decodeFilter($stateParams.formId);
        $rootScope.stageId = decodeFilter($stateParams.stageId);
        $rootScope.selectedVersion = $stateParams.selectedVersion;
        $rootScope.currentVersion = $stateParams.currentVersion;
        if ($rootScope.currentProjectViewId == undefined) {
            $state.go("project.project-view", {'id': $stateParams.projectId});
            return false;
        }
        $scope.actualObj = {};
        $rootScope.isFormChanged=false;

        $scope.saveBtnShow = function () {
            return (($stateParams.selectedVersion == $stateParams.currentVersion) && ($scope.projectDetails.stage_id == $rootScope.stageId)) ? true : false;
        }

        //get and save form Details and fields
        $scope.form_fields = [];
        $scope.getFormDetails = function () {
            var params = {};
            params.project_stage_section_form_id = $rootScope.formId;
            params.project_stage_id = $rootScope.stageId;
            params.crm_project_id = $rootScope.currentProjectViewId;
            params.version_number = $stateParams.selectedVersion;
            crmProjectService.formFields.get(params).then(function (result) {
                if (result.status) {
                    console.log('result.data', result.data);
                    $scope.sectionName = result.data.section_name;
                    $scope.stage_key = result.data.stage_key;
                    $scope.stageName = result.data.stage_name;
                    $scope.form_key = result.data.form_key;
                    $scope.project_form_id = result.data.project_form_id;
                    $scope.template = 'dynamic_form.html';
                    if (result.data.fields_exists == 0)
                        $scope.template = result.data.form_template;

                    $scope.formName = result.data.form_name;
                    $scope.form_fields = result.data.fields;
                    $scope.actualObj = angular.copy($scope.form_fields);
                    $scope.getFormComments();
                    $scope.getKnowledgeDetails();
                }

            })
        }
        $scope.submitDynamicForm = function (formFields, nextFrom) {
            var params = {};
            params.data = formFields;
            params.id_project_stage_section_form = $rootScope.formId;
            params.stage_id = $rootScope.stageId;
            params.project_id = $rootScope.currentProjectViewId;
            params.created_by = $rootScope.userId;
            crmProjectService.formFields.post(params).then(function (result) {
                $scope.actualObj = angular.copy($scope.form_fields);
                if ($rootScope.toastMsg(result)) {
                    if(result.status)
                        $rootScope.isFormChanged = false;
                    if (nextFrom) {
                        $scope.nextForm();
                    }
                }
            })
        }

        //each form comments
        $scope.formComments = [];
        $scope.getFormComments = function () {
            crmProjectService.formComments.get({
                reference_id: $rootScope.currentProjectViewId,
                reference_type: 'project',
                form_key: $scope.form_key,
                project_stage_section_form_id: $rootScope.formId
            }).then(function (result) {
                $scope.formComments = result.data;
            });
        }
        $scope.addFormComment = function (commentForm, comment) {
            $scope.submitStatus = true;
            if (commentForm.$valid) {
                $commentObj = {
                    reference_id: $rootScope.currentProjectViewId,
                    reference_type: 'project',
                    form_key: $scope.form_key,
                    project_stage_section_form_id: $rootScope.formId,
                    comment_by: $rootScope.userId,
                    comment: comment
                };
                crmProjectService.formComments.post($commentObj).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.submitStatus = false;
                        $scope.getFormComments();
                        $scope.comment = '';
                        $scope.add_comment = false;
                    }
                    else {
                        $rootScope.toast('Error', result.error);
                    }
                });
            }
        }

        //page nav
        $scope.allFormList = $cookieStore.get('cFormList');
        $scope.formList = [];
        angular.forEach($scope.allFormList, function (obj) {
            if (obj.stageId == $rootScope.stageId) {
                $scope.formList.push(obj);
            }
        })
        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;
        //find current form index
        if ($rootScope.formId) {
            var a = _.findWhere($scope.formList, {'formId': $rootScope.formId});//searching Element in Array
            $scope.currentFormIndex = _.indexOf($scope.formList, a);// getting index.
            if ($scope.currentFormIndex != -1) {
                $scope.prevBtnShow = ($scope.currentFormIndex > 0) ? true : false;
                $scope.nextBtnShow = ($scope.currentFormIndex < ($scope.formList.length - 1)) ? true : false;
            }
        }
        //goTo next form
        $scope.nextForm = function () {
            /*if (!angular.equals($scope.actualObj, $scope.form_fields)) {
             var r = confirm("Continue without saving data?");
             if (r) {
             var formIndex = $scope.currentFormIndex + 1;
             $scope.goToForm(formIndex);
             }
             } else {
             var formIndex = $scope.currentFormIndex + 1;
             $scope.goToForm(formIndex);
             }*/
            /*var formIndex = $scope.currentFormIndex + 1;
            $scope.goToForm(formIndex);*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var formIndex = $scope.currentFormIndex + 1;
                    $scope.goToForm(formIndex);
                } else
                    return false;
            } else {
                var formIndex = $scope.currentFormIndex + 1;
                $scope.goToForm(formIndex);
            }
        }
        //goTo prev form
        $scope.prevForm = function () {
            /*if (!angular.equals($scope.actualObj, $scope.form_fields)) {
             var r = confirm("Continue without saving data?");
             if (r) {
             var formIndex = $scope.currentFormIndex - 1;
             $scope.goToForm(formIndex);
             }
             } else {
             var formIndex = $scope.currentFormIndex - 1;
             $scope.goToForm(formIndex);
             }*/
            /*var formIndex = $scope.currentFormIndex - 1;
            $scope.goToForm(formIndex);*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var formIndex = $scope.currentFormIndex - 1;
                    $scope.goToForm(formIndex);
                } else
                    return false;
            } else {
                var formIndex = $scope.currentFormIndex - 1;
                $scope.goToForm(formIndex);
            }
        }


        $scope.goToForm = function (formIndex) {
            var form = $scope.formList[formIndex];
            var param = {};
            param.formId = encodeFilter(form.formId);
            param.stageId = encodeFilter(form.stageId);
            param.projectId = $stateParams.projectId;
            $state.go("project.project-form-view", param);
        };

        $scope.knowledgeData = [];
        $scope.getKnowledgeDetails = function () {
            crmCompanyService.getKnowledgeData({
                'company_id': $rootScope.id_company,
                'type': 'crm_project',
                'tags': $scope.formName,
                'crm_project_id': $rootScope.currentProjectViewId
            }).then(function (result) {
                $scope.knowledgeData = result.data;
            });
        }


        $scope.getIntelligenceDetails = function () {
            crmCompanyService.getIntelligenceData({
                'company_id': $rootScope.id_company,
                'sector_id': $scope.projectDetails.project_main_sector_id,
                'sub_sector_id': $scope.projectDetails.project_sub_sector_id,
                type: 'crm_project',
                'crm_company_id': $rootScope.id_crm_company
            }).then(function (result) {
                $scope.knowledgeDetails = result.data.list;
                $scope.peopleDetails = result.data.user_list;
            });
        };

        //initial call
        $scope.getFormDetails();


    })
    .controller('mapCollateralCtrl', function ($scope, $rootScope, $uibModal, collateralService, crmProjectService) {
        $scope.projectCollateralList = [];
        $scope.getProjectCollateralList = function () {
            collateralService.facility.get({project_id: $rootScope.currentProjectViewId}).then(function (result) {
                $scope.projectCollateralList = result.data;
                //stTable.pagination.numberOfPages = Math.ceil(result.data.total_records / stTable.pagination.number);
            })
        };
        $rootScope.collateralList = [];
        $scope.getCollateralList = function () {
            collateralService.crm.get({company_id: $rootScope.id_company}).then(function (result) {
                $rootScope.collateralList = result.data;
            })
        }
        $scope.getCollateralListWithKey = function (search_key) {
            $scope.visibleAddButton = false;
            return collateralService.crm.get({
                company_id: $rootScope.id_company,
                'search_key': search_key
            }).then(function (result) {
                return result.data.data;
            });
        };
        $scope.facilityList = [];
        $scope.getProjectFacilityList = function () {
            crmProjectService.facility.facilityList({
                'crm_project_id': $rootScope.currentProjectViewId,
                /*'stage_id': $rootScope.stageId,*/
                'type': 'workflow'
            }).then(function (result) {
                $scope.facilityList = result.data;
            })
        };

        $scope.facilityCollateralSave = function (project_facility_id, form) {
            console.log('selectedItem', $scope.selectedItem);
            $scope.submitStatus = true;
            if (form.$valid) {
                var param = {};
                param.collateral_id = $scope.selectedItem.id_collateral;
                param.project_facility_id = project_facility_id;
                param.created_by = $rootScope.userId;
                collateralService.facility.post(param).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.visibleFacilityDropDown = false;
                        $scope.getProjectCollateralList();
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                });
            }
        };
        $scope.removeFacilityCollateral = function ($id) {
            console.log($id);
            if ($rootScope.confirmMsg()) {
                collateralService.facility.delete({
                    'id_project_facility_collateral': $id,
                    'created_by': $rootScope.userId
                }).then(function (result) {
                    $scope.getProjectCollateralList();
                })
            }
        }
        $scope.cancelAssign = function () {
            $scope.visibleFacilityDropDown = false;
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.visibleFacilityDropDown = false;
        $scope.onSelect = function ($item, $model, $label) {
            $scope.visibleAddButton = true;
            $scope.selectedItem = $item;
            $scope.visibleAddButton = false;
            $scope.visibleFacilityDropDown = true;
            $scope.asyncSelected = '';
        };
        $scope.getProjectCollateralList();
        $scope.mapCollateral = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'modal-full my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'map_collateral.html',
                scope: $scope,
                controller: function ($uibModalInstance, $scope, $rootScope, collateralService, item) {
                    $scope.title = 'Add';
                    if (item) {
                        $scope.title = 'Edit';
                        $scope.field = item;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                $scope.getCollateralDetails();
            }, function () {
            });
        }
    })
    .controller('collateralAddedContactsCtrl', function ($rootScope, $scope, crmCompanyService, collateralService, crmContactService) {
        $rootScope.collateralContactList = [];
        $scope.getCollateralContactList = function () {
            collateralService.crm.contact.get({'collateral_id': $rootScope.currentCollateralId}).then(function (result) {
                $rootScope.collateralContactList = result.data;
            })
        }
        $scope.getCollateralContactList();
        $scope.getContactList = function (search_key) {
            $scope.visibleAddButton = false;
            return crmContactService.contactList({
                'collateral_id': $rootScope.currentCollateralId,
                'search_key': search_key,
                'company_id': $rootScope.id_company
            }).then(function (result) {
                return result.data.data;
            });
        };
        $scope.onSelect = function ($item, $model, $label) {
            $scope.visibleAddButton = true;
            $scope.selectedItem = $item;
            $scope.visibleAddButton = false;
            $scope.visibleDesignationDropDown = true;
            $scope.asyncSelected = '';
        };
        $scope.visibleDesignationDropDown = false;
        $scope.collateralContactSave = function (fd, form) {
            console.log('selectedItem', $scope.selectedItem);
            $scope.submitStatus = true;
            if (form.$valid) {
                var param = {};
                param.collateral_id = $rootScope.currentCollateralId;
                param.crm_contact_id = $scope.selectedItem.id_crm_contact;
                param.relation_type_id = fd.relation_type_id;
                param.created_by = $rootScope.userId;
                param.created_by = $rootScope.userId;
                collateralService.crm.contact.post(param).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.visibleDesignationDropDown = false;
                        $scope.getCollateralContactList();
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                });
            }
        };
        $scope.cancelAssign = function () {
            $scope.visibleDesignationDropDown = false;
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.removeContactCompany = function ($id) {
            if ($rootScope.confirmMsg()) {
                collateralService.crm.contact.delete({'id_collateral_contact': $id}).then(function (result) {
                    $scope.getCollateralContactList();
                })
            }
        }
    })
    .controller('projectPreviewCtrl', function (companyService, $uibModal, $rootScope, $scope, $state, crmProjectService, $stateParams, decodeFilter, anchorSmoothScroll, $location, $window, underscoreaddFilter) {

        $rootScope.currentProjectViewId = decodeFilter($stateParams.projectId);
        $rootScope.stageInfoId = decodeFilter($stateParams.stageInfoId);
        $rootScope.stageId = decodeFilter($stateParams.stageId);
        $scope.gotoElement = function (eId) {
            eId = underscoreaddFilter(eId);
            if (eId == '') return false;
            var elm = $('body').find('#' + eId);
            if (elm.length) {
                $location.hash();
                anchorSmoothScroll.scrollTo(eId);
            }
        };
        $scope.downloadDocument = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.id_project_stage_info = $rootScope.stageInfoId;
            crmProjectService.formFields.stageViewDoc(params).then(function (result) {
                $scope.fileName = result.data;
                window.open(
                    $scope.fileName,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        }
        $scope.downloadTermSheetDoc = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.id_project_stage_info = $rootScope.stageInfoId;
            crmProjectService.formFields.termSheetDoc(params).then(function (result) {
                $scope.fileName = result.data;
                window.open(
                    $scope.fileName,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        }
        $scope.downloadFacilityTermSheetDoc = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.id_project_stage_info = $rootScope.stageInfoId;
            crmProjectService.formFields.facilityTermSheetDoc(params).then(function (result) {
                $scope.fileUrl = result.data.fileUrl;
                $scope.fileName = result.data.fileName;
                var a         = document.createElement('a');
                a.href        = $scope.fileUrl;
                a.target      = '_blank';
                a.download    = $scope.fileName;
                document.body.appendChild(a);
                a.click();
            });
        }
        $scope.downloadOfferLetter = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.id_project_stage_info = $rootScope.stageInfoId;
            crmProjectService.formFields.offerLetterDoc(params).then(function (result) {
                $scope.fileName = result.data;
                window.open(
                    $scope.fileName,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        }


        $scope.getFields = function () {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.id_project_stage_info = $rootScope.stageInfoId;
            crmProjectService.formFields.stageView(params).then(function (result) {
                $scope.stageInfo = result.data;
                $scope.field = result.data.fields;
            });
        }
        $scope.workFlowActionBtn = {};
        $scope.getWorkflowActions = function () {
            crmProjectService.workflow.getAction({
                project_stage_info_id: $rootScope.stageInfoId,
                'user_id': $rootScope.userId
            }).then(function (result) {
                $scope.workFlowActionBtn = result.data[0];
            })
        }
        $scope.getProjectStageWorkflow = function () {
            crmProjectService.workflow.projectStageWorkflow({project_stage_info_id: $rootScope.stageInfoId}).then(function (result) {
                $scope.stageWorkFlow = result.data;
            })
        }
        $scope.getProjectStageWorkflowInfo = function () {
            crmProjectService.workflow.projectStageInfoDetail({
                stage_id: $rootScope.stageId,
                'project_id': $rootScope.currentProjectViewId
            }).then(function (result) {
                $scope.stageWorkFlowInfo = result.data;

            })
        }


        $scope.actionModal=function(actionInfo){
            $scope.actionInfo=actionInfo;
            //console.log('$scope.actionInfo ',$scope.actionInfo);
            var modalInstance=$uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/project/stage-action-modal.html',
                resolve: {
                    actionDetails: function(){
                        var param={};
                        param.workflow_phase_id=$scope.actionInfo.workflow_phase_id;
                        param.id_workflow_phase_action=$scope.actionInfo.id_workflow_action;
                        param.workflow_phase_activity_id=$scope.actionInfo.workflow_phase_activity_id;
                        return crmProjectService.workflow.actionDetails(param).then(function(result){
                            return result;
                        })
                    }
                },
                controller: function($uibModalInstance,$scope,actionDetails,crmProjectService,projectService) {
                    $scope.approvedShow = false;
                    if ($scope.actionInfo.is_approve == 1) {
                        $scope.approvedShow = true;
                    }
                    $scope.facilityList = [];
                    $scope.getProjectFacilityList = function () {
                        crmProjectService.facility.facilityList({
                            'crm_project_id': $rootScope.currentProjectViewId,
                            'stage_id': $rootScope.stageId,
                            'type': 'workflow'
                        }).then(function (result) {
                            $scope.projectFacilityList = result.data;
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.action = {};
                    $scope.action.forward_to = [];
                    console.log(actionDetails);
                    $scope.actionDetails = actionDetails.data;
                    $scope.actionDetailsCompleteData = actionDetails;
                    if ($scope.actionDetailsCompleteData.next_stage_assigning &&
                        $scope.actionDetailsCompleteData.next_stage_assigning.next_project_stage_assign_type
                        && $scope.actionDetailsCompleteData.next_stage_assigning.next_project_stage_assign_type != ''
                        && $scope.actionDetailsCompleteData.next_stage_assigning.next_project_stage_assign_type != 'Role') {
                        for (var a in $scope.actionDetailsCompleteData.next_stage_assigning.users) {
                            console.log($scope.actionDetailsCompleteData.next_stage_assigning.users[a]);
                            $scope.action.next_stage_assigning_to_user_id = $scope.actionDetailsCompleteData.next_stage_assigning.users[a].user_id;
                            break;
                        }
                    }

                    $scope.disabledBtn=false;
                    $scope.saveAction=function(action){
                        /*console.log(action);*/
                        $scope.disabledBtn=true;
                        var param=action;
                        param.facility_id=[];
                        angular.forEach($scope.projectFacilityList,function(item){
                            var obj={};
                            obj.project_facility_status=item.action_facility_status;
                            obj.facility_usd_amount=item.action_facility_usd_amount;
                            obj.facility_amount=item.action_facility_amount;
                            obj.id_project_facility=item.id_project_facility;
                            param.facility_id.push(obj);
                        });

                        param.id_project_stage_workflow_phase=$scope.actionInfo.id_project_stage_workflow_phase;
                        param.workflow_phase_action_id=$scope.actionInfo.id_workflow_phase_action;
                        param.project_stage_info_id=$rootScope.stageInfoId;
                        param.project_stage_id=$rootScope.stageId;
                        param.project_id=$rootScope.currentProjectViewId;
                        param.forward_by=$rootScope.userId;
                        param.company_id=$rootScope.id_company;
                        if($scope.actionInfo.is_approve==1){
                            param.flag=1;
                        }
                        /*console.log('post',param);*/
                        //delete param.forward_to;
                        crmProjectService.workflow.postAction(param).then(function(result){
                            $scope.disabledBtn=false;
                            if($scope.actionInfo.is_approve==1&&result.hasOwnProperty('error')){
                                var r=confirm(result.error);
                                if(r){
                                    delete param.flag;
                                    crmProjectService.workflow.postAction(param).then(function(result){
                                        if($rootScope.toastMsg(result)){
                                            // $scope.cancel();
                                            $state.reload();
                                        }
                                    })
                                }
                            }else{
                                if($rootScope.toastMsg(result)){
                                    $state.reload();
                                }
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function(){
            },function(){
            });
        }
        $scope.userList = [];
        $scope.getUsers = function () {
            companyService.users({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.userList = result.data;
            });
        }
        //Stage comments
        $scope.stageCommentsList = [];
        $scope.getProjectStageComments = function () {
            crmProjectService.workflow.projectStageComments.get({
                'project_stage_info_id': $rootScope.stageInfoId
            }).then(function (result) {
                $scope.stageCommentsList = result.data;

            })
        }


        //need info
        $scope.needInfoModal = function (workflow_phase_id) {
            $scope.workflow_phase_id = workflow_phase_id;
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'need-info.html',
                resolve: {},
                controller: function ($uibModalInstance, $scope, crmProjectService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.comment = {};
                    $scope.addFormComment = function (commentForm, comment, stage_key) {
                        var param = comment;
                        param.project_stage_info_id = $rootScope.stageInfoId;
                        param.stage_key = stage_key;
                        param.comment_by = $rootScope.userId;
                        param.project_id = $rootScope.currentProjectViewId;
                        param.company_id = $rootScope.id_company;
                        param.workflow_phase_id = $scope.workflow_phase_id;
                        crmProjectService.workflow.projectStageComments.post(param).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.comment = {};
                                commentForm.$setPristine();
                                $scope.getProjectStageComments();
                                $scope.cancel();
                            }
                        })
                    }
                }
            })
        }


        //initial call
        $scope.getFields();


        //facility
        $scope.concept_hide_btn = false;
        $scope.currencyList = [];
        $scope.getCurrencyList = function () {
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.currencyList = result.data;
                //$scope.showUsdAmount($scope.facilityInfo.currency_id);
            })
        }
        $scope.timePeriod = [];
        $scope.getTimePeriod = function () {
            companyService.facilityType.timePeriod({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.timePeriod = result.data;
            })
        }
        $scope.amountType = [];
        $scope.getAmountType = function () {
            companyService.facilityType.facilityAmountType({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.amountType = result.data;
            })
        }
        $scope.facilitiesInfo = {};
        $scope.getFacilities = function (id_project_stage_info) {
            crmProjectService.facility.stageFacilities({'project_stage_info_id': id_project_stage_info})
                .then(function (result) {
                    var data = result.data;
                    $scope.facilitiesInfo = data;
                })
        }
        $scope.facility_details_show = false;
        $scope.getFacility = function (data) {
            if (data) {
                $scope.facilityInfo = data;
                $scope.pricingList = data.pricing;
                $scope.termsConditionsList = data.terms_conditions;
                $scope.id_company_facility = data.company_facility_id;
                $rootScope.id_project_facility = data.id_project_facility;
                $scope.facility_details_show = true;
            }
        }

        $scope.cancel = function () {
            $scope.facility_details_show = false;
        }


    })
    .controller('crmFacilityCtrl', function (confirm, decodeFilter, $stateParams, $location, anchorSmoothScroll, $scope, $rootScope, crmProjectService, companyService, masterService, $uibModal, encodeFilter, crmFacilityService) {
        $rootScope.stageId = decodeFilter($stateParams.stageId);
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.facilityTypeList = [];
        $scope.getFacilityTypeList = function () {
            companyService.facilityType.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.facilityTypeList = result.data;
            })
        }
        $scope.getProjectFacilityList = function () {
            crmProjectService.facility.facilityList({
                'crm_project_id': $rootScope.currentProjectViewId,
                'stage_id': $rootScope.stageId
            }).then(function (result) {
                $scope.projectFacilityList = result.data;
                angular.forEach($scope.projectFacilityList, function (value, pkey) {


                    var x = {
                        "type": "ColumnChart",
                        "options": {
                            title: 'Term Frequency',
                            chartArea: {width: '80%', left: 80},
                            isStacked: true,
                            orientation: 'horizontal',
                            focusTarget: 'category',
                            opacity: 0.2,
                            bar: {groupWidth: '90%'},
                            hAxis: {
                                minValue: 0
                            },
                            vAxis: {},
                            animation: {
                                duration: 1200,
                                easing: 'inAndOut',
                                startup: true
                            },
                            annotations: {
                                style: 'line'
                            },
                            legend: {position: 'top', alignment: 'end'},
                            series: {
                                0: {color: '#6180d4'},
                                1: {color: '#ffd73e'},
                                2: {color: '#ff0000'},
                                3: {color: '#00a651'}
                            }
                        },
                        "data": [
                            ['days',
                                'Prinicple', {role: 'annotation', role: 'style'},
                                'Interest', {role: 'annotation', role: 'style'},
                                'Skipped', {role: 'annotation', role: 'style'},
                                'Paid', {role: 'annotation', role: 'style'}
                            ]
                        ]
                    }
                    var index = 0;
                    angular.forEach(value.facility_transactions, function (value, key) {

                        index++;
                        if (index) {
                            x.data.push([key + 1, parseInt(value.principal), 'opacity: 0.6', parseInt(value.interest_due), 'opacity: 0.6', parseInt(0), 'opacity: 0.6', parseInt(0), 'opacity: 0.6']);
                        } else {
                            x.data.push([key + 1, parseInt(value.principal), 'opacity: 0.6', parseInt(value.interest_due), 'opacity: 0.6', parseInt(0), 'opacity: 0.6', parseInt(0), 'opacity: 0.6']);
                        }
                        ;
                    });
                    $scope.projectFacilityList[pkey].facility_transactions = x;
                });
                console.log($scope.projectFacilityList);
            })
        }
        $scope.facilityTermTypeList = [];
        $scope.getFacilityTermTypeList = function () {
            masterService.masterList({
                'company_id': $rootScope.id_company,
                'master_key': 'facility_term_type'
            }).then(function (result) {
                $scope.facilityTermTypeList = result.data;
            })
        }
        $scope.noOfTermsList = [];
        $scope.getNoOfTermsList = function () {
            masterService.masterList({
                'company_id': $rootScope.id_company,
                'master_key': 'payment_modalities'
            }).then(function (result) {
                $scope.noOfTermsList = result.data;
            })
        }
        $scope.getProjectFacilityList();
        $scope.new_facility = false;
        $scope.closeFacility = function () {
            $scope.new_facility = false;
            $rootScope.div_size = undefined;
        }

        $scope.newFacility = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-new-faciliey-modal.html',
                controller: function ($uibModalInstance, $scope) {
                    $scope.quickAddFacility = function (facility, submitBtn) {
                        var params = {};
                        params.id_company_facility = facility.facilityType.id_company_facility;
                        params.project_facility_name = facility.facilityType.facility_name;
                        params.crm_project_id = $rootScope.currentProjectViewId;
                        params.created_by = $rootScope.userId;
                        params.id_project_stage_section_form = $rootScope.formId;
                        params.stage_id = $rootScope.stageId;
                        crmProjectService.facility.quickFacility(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.getProjectFacilityList();
                                $scope.cancel();
                                if (submitBtn == 'save_continue') {
                                    $scope.editFacility(result.data);
                                }
                            }
                        })


                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
                $scope.getProjectFacilityList();
            }, function () {
            });
            //  $scope.new_facility = true;
            // $rootScope.div_size = 'full';
            //  $scope.facilityInfo = {};
            //  $scope.pricingList = [];
            //   $scope.termsConditionsList =[];
            //  $scope.id_company_facility = undefined;
            //   $rootScope.id_project_facility = undefined;

        }
        $scope.gotoElement = function (eID) {
            $location.hash();
            anchorSmoothScroll.scrollTo(eID);
        };
        $scope.editFacility = function (id_project_facility, elementId) {
            $scope.facilityId = eval(id_project_facility);
            var params = {};
            params.facilityId = encodeFilter($scope.facilityId);
            params.projectId = encodeFilter($rootScope.currentProjectViewId);
            params.stageId = encodeFilter($rootScope.stageId);
            $rootScope.goToState('project.project-facility-view', params);
        }
        $scope.deleteFacility = function (facility) {
            var params = {}
            params.project_facility_id = facility.id_project_facility;
            params.crm_project_id = $rootScope.currentProjectViewId;
            var r = confirm("Do you want to continue?");
            if (r == true) {
                crmProjectService.facility.delete(params).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.getProjectFacilityList();
                    }
                })
            }
        }

        $scope.facilityStatusChange = function (facility, status) {
            var param = {};
            param.id_project_facility = facility.id_project_facility;
            param.project_facility_status = status;
            confirm().then(function () {
                crmFacilityService.crm.post(param).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.getProjectFacilityList();
                    }
                })
            })
        }
        $scope.openFacilityRetireModal = function (facility) {

            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'retired-modal.html',
                controller: function ($uibModalInstance, $scope) {
                    $scope.facilityRetireStatusChange = function (date) {
                        var param = {};
                        param.id_project_facility = facility.id_project_facility;
                        param.project_facility_status = 'retired';
                        param.facility_closed_date = date;
                        crmFacilityService.crm.post(param).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.getProjectFacilityList();
                                $scope.cancel();
                            }
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }

        /*$scope.facilityGraphObject = {
         "type": "ColumnChart",
         "options": {
         title: 'Term Frequency',
         chartArea: {width: '80%'},
         isStacked: true,
         orientation: 'horizontal',
         focusTarget: 'category',
         opacity: 0.2,
         bar: {groupWidth: '90%'},
         hAxis: {
         minValue: 0,
         },
         vAxis: {},
         animation: {
         duration: 1200,
         easing: 'inAndOut',
         startup: true
         },
         legend: {position: 'top', alignment: 'end'},
         series: {
         0: {color: '#6180d4'},
         1: {color: '#ffd73e'},
         2: {color: '#ff0000'},
         3: {color: '#00a651'}
         }
         },
         "data": [
         ['days',
         'Prinicple', {role: 'annotation', role: 'style'},
         'Interest', {role: 'annotation', role: 'style'},
         'Skipped', {role: 'annotation', role: 'style'},
         'Paid', {role: 'annotation', role: 'style'}
         ],

         ['2', 1000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['3', 2000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 2000, 'opacity: 0.6'],
         ['4', 3000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 4000, 'opacity: 0.6'],
         ['5', 4000, 'opacity: 0.6', 15000, 'opacity: 0.6', 2000, 'opacity: 0.6', 5000, 'opacity: 0.6'],
         ['6', 5000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 6000, 'opacity: 0.6'],
         ['7', 6000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 7000, 'opacity: 0.6'],
         ['8', 7000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['9', 8000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['10', 9000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 4000, 'opacity: 0.6'],
         ['11', 10000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['12', 11000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['13', 13000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['14', 14000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['15', 15000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['16', 16000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['17', 17000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['18', 18000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['19', 19000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ['20', 20000, 'opacity: 0.6', 15000, 'opacity: 0.6', 1000, 'opacity: 0.6', 0, 'opacity: 0.6'],
         ]
         };*/


        $rootScope.facilityFormUpdate = function (id_company_facility) {
            var params = {};
            params.company_facility_id = $scope.id_company_facility = id_company_facility;
            params.company_id = $rootScope.id_company;
            crmProjectService.facility.companyFacility(params).then(function (result) {
                $scope.facilityInfo = result.data.general;
                $scope.pricingList = result.data.pricing;
                $scope.termsConditionsList = result.data.terms_conditions;

            })
        }
    })
    .controller('projectFacilityView', function ($location, anchorSmoothScroll, companyService, $stateParams, crmProjectService, $scope, decodeFilter, $rootScope, $q) {
        $rootScope.currentProjectViewId = decodeFilter($stateParams.projectId)
        $rootScope.stageId = decodeFilter($stateParams.stageId);
        $scope.gotoElement = function (eID) {
            $location.hash();
            anchorSmoothScroll.scrollTo(eID);
            if (!angular.element('#' + eID).hasClass('racc-open')) {
                angular.element('#' + eID + ' .acc-head h6').triggerHandler('click');
            }
        };
        $scope.currencyList = [];
        $scope.getCurrencyList = function () {
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.currencyList = result.data;
                $scope.showUsdAmount($scope.facilityInfo.currency_id);
            })
        }
        $scope.timePeriod = [];
        $scope.getTimePeriod = function () {
            companyService.facilityType.timePeriod({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.timePeriod = result.data;
            })
        }
        $scope.amountType = [];
        $scope.getAmountType = function () {
            companyService.facilityType.facilityAmountType({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.amountType = result.data;
            })
        }
        $scope.facilityId = decodeFilter($stateParams.facilityId);
        $scope.facilityInfo = {};
        $scope.getFacility = function () {
            crmProjectService.facility.get({
                'id_project_facility': $scope.facilityId,
                'crm_project_id': $rootScope.currentProjectViewId,
                'stage_id': $rootScope.stageId
            }).then(function (result) {
                var data = result.data;
                $scope.facilityInfo = data;
                $scope.pricingList = data.pricing;
                $scope.termsConditionsList = data.terms_conditions;
                $scope.id_company_facility = data.company_facility_id;
                $rootScope.id_project_facility = data.id_project_facility;
                $scope.showUsdAmount($scope.facilityInfo.currency_id);
            })
        }
        $scope.loanRepayment = function (init) {
            if (init) {
                crmProjectService.facility.facilityLoanRepayment({'contract_id': $scope.facilityInfo.contract_id}).then(function (result) {
                    $scope.facilityLoanRepayment = result.data;
                });
            }
        }
        $scope.loanDisbursement = function (init) {
            if (init) {
                crmProjectService.facility.facilityDisbursement({'contract_id': $scope.facilityInfo.contract_id}).then(function (result) {
                    $scope.facilityDisbursement = result.data;
                });
            }
        }
        $scope.loanReceipts = function (init) {
            if (init) {
                crmProjectService.facility.facilityLoanReceipts({'contract_id': $scope.facilityInfo.contract_id}).then(function (result) {
                    $scope.facilityLoanReceipts = result.data;
                });
            }
        }
        $scope.submitFacility = function (formData, field_section) {
            var $params = {};
            if (field_section == 'general') {
                if ($scope.currentCurrencyCode == 'USD')
                    formData.facility_usd_amount = formData.facility_amount;

                //console.log('formData',formData);return false;
                $params = formData;
            } else {
                $params.data = formData;
            }

            $params.crm_project_id = $rootScope.currentProjectViewId;
            $params.field_section = field_section;
            $params.id_project_facility = $rootScope.id_project_facility;
            $params.id_company_facility = $scope.id_company_facility;
            $params.created_by = $rootScope.userId;
            $params.id_project_stage_section_form = $rootScope.formId;
            delete formData.company_id;
            crmProjectService.facility.post($params).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                }
            })
        }

        $scope.showUsdAmount = function (currency_id) {
            $scope.showUsdAmountDiv = false;
            if (currency_id) {
                var a = _.findWhere($scope.currencyList, {'currency_id': currency_id});//searching Element in Array
                if (a) {
                    $scope.currentCurrencyCode = a.currency_code;
                    if (a.currency_code == 'USD') {
                        //$scope.facilityInfo.facility_usd_amount = $scope.facilityInfo.facility_amount;
                        $scope.showUsdAmountDiv = false;
                    } else {
                        $scope.showUsdAmountDiv = true;
                    }
                }
            }
            //$scope.currentFormIndex = _.indexOf($scope.companyFormDetails, a);
        }

    })
    .controller('riskAssessmentCtrl', function ($scope, $rootScope, crmProjectService, projectService, $uibModal, masterService) {
        $scope.getData = function () {
            projectService.riskAssessment.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.riskAssessment.delete({'id_risk_assessment': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.getParent = function (id, field) {
            if (id == 0 || id) {
                masterService.risk.getRiskStage({'parent_id': id}).then(function (result) {
                    $scope[field] = result.data;
                });
            }

        };
        $scope.openModel = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'risk-assessment-modal-form.html',
                resolve: {
                    item: row
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fdata = {};
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fdata);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (fData) {
                        var params = fData;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.riskAssessment.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('operationalChecklistTypeCtrl', function ($scope, $rootScope, crmProjectService, crmCompanyService, Upload, $q, $timeout, $uibModal) {
        $scope.categoryQuestionList = [];
        $scope.getProjectFacilityList = function () {
            crmProjectService.facility.facilityList({
                'crm_project_id': $rootScope.currentProjectViewId,
                'stage_id': $rootScope.stageId
            }).then(function (result) {
                $scope.projectFacilityList = result.data;
            })
        }
        $scope.getProjectFacilityList();
        $scope.getAssessmentQuestion = function (form_key,project_form_id) {
            $scope.form_key = form_key;
            $scope.project_form_id = project_form_id;
            crmProjectService.projectLegalCheckList.getProjectCheckList({
                'form_key': $scope.form_key,
                'crm_project_id':$rootScope.currentProjectViewId,
                'project_form_id': $scope.project_form_id
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data;
                }
            });
        }
        $scope.saveAnswerRestrictions = function (category) {
            bulkAnswers = [];
            angular.forEach(category, function (category) {
                angular.forEach(category.question, function (question, $index) {
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentProjectViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                    $obj.crm_project_id = $rootScope.currentProjectViewId;
                    if (question.answer.id_assessment_answer != undefined) {
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }
                    $rootScope.loaderOverlay = false;
                    if (question.question_type == 'file') {
                        if (question.answer.file.name != undefined) {
                            Upload.upload({
                                url: API_URL + 'Company/assessmentAnswer',
                                data: {
                                    file: question.answer.file,
                                    'answer': $obj
                                }
                            }).then(function (result) {
                                if (result.status) {
                                    if ($index == category.question.length - 1) {
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            });
                        }
                    } else {
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                if ($index == category.question.length - 1) {
                                    $scope.getAssessmentQuestion();
                                }
                            } else {
                                $rootScope.toast('Error', result.error, 'error');
                            }
                        })
                    }
                })
            })
        }
        $scope.saveAnswer = function (questions) {
            var bulkAnswers = [];
            angular.forEach(questions, function (question, $index) {

                var $obj = {};
                $obj.assessment_answer = question.answer.assessment_answer;
                $obj.assessment_question_id = question.id_assessment_question;
                $obj.crm_reference_id = $rootScope.currentProjectViewId;
                $obj.created_by = $rootScope.userId;
                $obj.company_id = $rootScope.id_company;
                $obj.comment = question.answer.comment;
                $obj.project_facility_id = question.project_facility_id;
                $obj.project_checklist_id = question.id_project_checklist;
                $obj.project_stage_section_form_id =$rootScope.formId;
                $obj.type = 'media';
                $obj.form_key = $scope.form_key;
                $obj.company_id = $rootScope.id_company;
                //$obj.file = question.file;
                   $obj.crm_project_id = $rootScope.currentProjectViewId;
                if (question.answer.id_assessment_answer != undefined) {
                    $obj.id_assessment_answer = question.answer.id_assessment_answer;
                }

                $rootScope.loaderOverlay = false;
                crmCompanyService.assessmentQuestion.answer.post({'answer':$obj,'file': question.file}).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        if ($index == questions.length - 1) {
                            $scope.getAssessmentQuestion($scope.form_key,$scope.project_form_id);
                        }
                    }
                })
            })
        }
        $scope.openChecklistModalOpen = function (form_key, project_form_id) {
            $scope.form_key = form_key;
            $scope.project_form_id = project_form_id;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'modal_checklist.html',
                resolve: {
                    unselectQuestionsList: function () {
                        return crmProjectService.projectLegalCheckList.getUnselectedCheckList({
                            'form_key': $scope.form_key,
                            'crm_project_id': $rootScope.currentProjectViewId
                        }).then(function (result) {
                            if (result.status) {
                                return result.data;

                            }
                        })
                    }
                },
                controller: function ($uibModalInstance, $scope, unselectQuestionsList, projectService) {
                    console.log('unselectQuestionsList', unselectQuestionsList);
                    $scope.unselectQuestionsList = unselectQuestionsList;
                    $scope.saveBtnShow = false;
                    angular.forEach(unselectQuestionsList,function(category,key){
                        console.log('key',key);
                        if(category.question.length>0){
                            $scope.saveBtnShow = true;
                        }
                    })
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.saveSelectQuestion = function (category) {
                        $scope.checkedIds = [];
                        angular.forEach(category, function (category) {
                            angular.forEach(category.question, function (question, $index) {
                                if (question.check)
                                    $scope.checkedIds.push(question.id_assessment_question);
                            })
                        })
                        crmProjectService.projectLegalCheckList.postSelectedCheckList({
                            'form_key': $scope.form_key,
                            'crm_project_id': $rootScope.currentProjectViewId,
                            'project_form_id': $scope.project_form_id,
                            'assessment_question_id': $scope.checkedIds,
                            'created_by': $rootScope.userId
                        }).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.getAssessmentQuestion($scope.form_key,$scope.project_form_id);
                                $scope.cancel();

                            }
                        })
                    }
                }
            })


        }
        $scope.deleteQuestion = function(question){
            if($rootScope.confirmMsg()){
                crmProjectService.projectLegalCheckList.deleteCheckList({
                    'id_project_checklist':question.id_project_checklist,
                    'id_assessment_answer':question.answer.id_assessment_answer
                }).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.getAssessmentQuestion($scope.form_key,$scope.project_form_id);
                    }
                });
            }

        }
    })
    .controller('disbursementRequestCtrl', function ($scope, $rootScope, projectService, crmProjectService, $uibModal) {


        $scope.getData = function () {
            projectService.disbursementRequest.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.getData();
        $scope.delete = function (row) {
            if ($rootScope.confirmMsg()) {
                projectService.disbursementRequest.delete({'id_disbursement_request': row.id_disbursement_request}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.download = function (row) {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.project_stage_info_id = $rootScope.stageId;
            params.project_stage_section_form_id = $rootScope.formId;
            params.disbursement_request_id = row.id_disbursement_request;
            projectService.disbursementRequest.download(params).then(function (result) {
                $scope.fileName = result.data;
                window.open(
                    $scope.fileName,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        }

        $scope.openModel = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'disbursement_request.html',
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    facilityList: function () {
                        return projectService.facility.get({
                            'crm_project_id': $rootScope.currentProjectViewId,
                            'stage_id': $rootScope.stageId
                        }).then(function (result) {
                            return result.data;
                        })
                    }
                },
                controller: function ($uibModalInstance, $scope, item, facilityList, projectService) {
                    $scope.fData = {};
                    $scope.projectFacilityList = facilityList;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fData);
                        $scope.facility = _.findWhere($scope.projectFacilityList, {'id_project_facility': item.project_facility_id});
                        ;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.formSubmit = function (form) {
                        var params = form;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.project_facility_id = $scope.facility.id_project_facility;
                        projectService.disbursementRequest.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('supervisionAssessmentCtrl', function ($scope, $rootScope, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.supervisionAssessment.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.supervisionAssessment.delete({'id_supervision_assessment': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.download = function (id) {
            var params = {};
            params.project_id = $rootScope.currentProjectViewId;
            params.project_stage_info_id = $rootScope.stageId;
            params.project_stage_section_form_id = $rootScope.formId;
            params.supervision_assessment_id = id;
            projectService.supervisionAssessment.download(params).then(function (result) {
                $scope.fileName = result.data;
                window.open(
                    $scope.fileName,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
        }
        $scope.getData();
        $scope.openModel = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'supervision_assessment.html',
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fData = {};
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fData);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (form) {
                        var params = form;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.supervisionAssessment.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('supervisionProjectCommitteeCtrl', function ($scope, $rootScope, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.supervisionProjectCommittee.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.supervisionProjectCommittee.delete({'id_supervision_project_committee': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.openModel = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'supervision_project_committee_model.html',
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fData = {};
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fData);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (form) {
                        var params = form;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.supervisionProjectCommittee.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('EsGradeCtrl', function ($scope, $rootScope, crmCompanyService, $uibModal, projectService) {
        var data = {};
        data.company_id = $rootScope.id_company;
        /*data.form_id=$rootScope.companyParames.formId;*/
        data.user_id = $rootScope.userId;

        $rootScope.gradesValue = [
            {
                'grade': '1',
                'description': 'Projects with sigificant adverse E&S impacts that are diverse or sensitive and may extend beyond immediate project site.'
            },
            {
                'grade': '2',
                'description': 'Projects with potentially limited adverse E&S impacts that are few, site specific, and for which mitigation measures are readily available'
            },
            {'grade': '3', 'description': 'Projects with minimal or no adverse E&S impacts'},
            {
                'grade': '4',
                'description': 'Caters for situations where Access Bank channels funds through other institutions such as financial intermediaries'
            },
        ];

        $scope.getCompanyGrade = function () {
            projectService.grade.get({'project_id': $rootScope.currentProjectViewId}).then(function (result) {
                $scope.grades = result.data;
                console.log($scope.grades);
            });
        }
        $scope.getCompanyGrade();

        $scope.removeGrade = function (row) {
            var r = confirm("Do you want to continue?");
            if (r == true) {
                projectService.grade.delete({'id_project_es_grade': row.id_project_es_grade}).then(function (result) {
                    $scope.getCompanyGrade();
                });
            }
        }

        $scope.addCompanyGrade = function (row, isEditable) {
            $scope.ratioData = {'row': row, 'isEditable': isEditable};
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'gread-add.html',
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.isEditable = item.isEditable;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item.row) {
                        $scope.grade = item.row;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.saveGrade = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $Data.project_id = $rootScope.currentProjectViewId;
                            $Data.form_id = $rootScope.formId;
                            $Data.created_by = $rootScope.userId;
                            projectService.grade.post($Data).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $uibModalInstance.dismiss('cancel');
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.ratioData) {
                            return $scope.ratioData;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
                $scope.getCompanyGrade();
            });
        }
    })
    .controller('initalSupervisionCtrl', function ($scope, $rootScope, crmProjectService, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.initialSupervisionReport.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.initialSupervisionReport.delete({'id_supervision_report': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.openModel = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'inital-supervision-modal-form.html',
                resolve: {
                    item: row
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fdata = {};
                    $scope.fdata.status = 'no';
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fdata);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (fData) {
                        var params = fData;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.initialSupervisionReport.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('annualReviewCtrl', function ($scope, $rootScope, crmProjectService, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.annualReview.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.annualReview.delete({'id_annual_review': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.openModel = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'annual-review-modal-form.html',
                resolve: {
                    item: row
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fdata = {};
                    $scope.fdata.status = 'no';
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fdata);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (fData) {
                        var params = fData;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.annualReview.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('ongoingMonitoringCtrl', function ($scope, $rootScope, crmProjectService, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.ongoingMonitoring.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.ongoingMonitoring.delete({'id_ongoing_monitoring': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.openModel = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'ongoing-monitoring-modal-form.html',
                resolve: {
                    item: row
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fdata = {};
                    $scope.fdata.status = 'no';
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fdata);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (fData) {
                        var params = fData;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.ongoingMonitoring.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('otherMonitoringCtrl', function ($scope, $rootScope, crmProjectService, projectService, $uibModal) {
        $scope.getData = function () {
            projectService.otherMonitoring.get({
                project_id: $rootScope.currentProjectViewId,
                project_stage_info_id: $rootScope.stageId,
                project_stage_section_form_id: $rootScope.formId,
            }).then(function (result) {
                $scope.rows = result.data;
            });
        }
        $scope.delete = function (id) {
            if ($rootScope.confirmMsg()) {
                projectService.otherMonitoring.delete({'id_other_monitoring': id}).then(function (result) {
                    if ($rootScope.toastMsg(result))
                        $scope.getData();
                });
            }
        }
        $scope.getData();
        $scope.openModel = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'other-monitoring-modal-form.html',
                resolve: {
                    item: row
                },
                controller: function ($uibModalInstance, $scope, item, projectService) {
                    $scope.fdata = {};
                    $scope.fdata.status = 'no';
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.update = true;
                        angular.copy(item, $scope.fdata);
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.formSubmit = function (fData) {
                        var params = fData;
                        params.project_id = $rootScope.currentProjectViewId;
                        params.project_stage_info_id = $rootScope.stageId;
                        params.project_stage_section_form_id = $rootScope.formId;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        projectService.otherMonitoring.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $uibModalInstance.close();
                            }
                        })
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.getData();
            }, function () {
            });
        }
    })
    .controller('projectFinancials', function ($scope, $uibModal, $timeout, $rootScope, crmCompanyService, crmProjectService, masterService,$q ) {
        $scope.source = {};
        $scope.use_funds = {};
        $scope.project ={};
        $scope.child_data ={};
        $scope.getCurrency = function(){
            return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                var data = {};
                for(var a in $result.data){
                    if($result.data[a]['id_company_currency']){
                        data[a] = $result.data[a];
                    }
                }
                console.log('currency',data);
                $scope.currecnyList = data;
            });
        };
        $scope.getCurrency();
        $scope.getprojectFinancials = function(){
            $q.all([

                $scope.getCurrency()
            ]).then(function(r){

                crmProjectService.projectFinancals.get({'crm_project_id':$rootScope.currentProjectViewId}).then(function (result) {
                    if(result.data.master.length>0){
                        $scope.project = result.data.master[0];
                        if($scope.project.is_product_quotes_received == null){
                            $scope.project.is_product_quotes_received = 1;
                        }
                        if($scope.project.can_loan_disbursed_to_supplier == null){
                            $scope.project.can_loan_disbursed_to_supplier = 1;
                        }
                        if($scope.project.can_loan_disbursed_to_supplier == 1){
                            $scope.project.can_loan_disbursed_to_supplier_comments = '';
                        }

                        if($scope.project.currency_id){
                            /*Select the value which is already selected*/
                            angular.forEach($scope.currecnyList, function (value, index) {
                                if(value.currency_id==$scope.project.currency_id){
                                    $scope.project.currency=value;

                                }
                            });
                        } else {
                            /*default selection*/
                            $scope.project.currency=$scope.currecnyList[0];
                            $scope.project.currency_id=$scope.currecnyList[0].currency_id;
                        }
                        //$scope.source = result.data.master[0].child_data.source_of_funds;
                        //$scope.use_funds = result.data.master[0].child_data.use_of_funds;
                        //$scope.child_data = result.data.master[0];

                        //console.log('$scope.use_funds',$scope.use_funds);
                    }else{
                        //$scope.source = {};
                        //$scope.use_funds = {};
                    }
                    $scope.project.child_data.source_of_funds.sum_source_of_amount_req_ext=0;
                    $scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_ext=0;
                    $scope.project.child_data.source_of_funds.sum_source_of_amount_req_int=0;
                    $scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_int=0;
                    $scope.project.child_data.use_of_funds.sum_use_of_amount_req_wrkng=0;
                    $scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_wrkng=0;
                    $scope.project.child_data.use_of_funds.sum_use_of_amount_req_fixed=0;
                    $scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_fixed=0;

                    $scope.project.child_data.source_of_funds.total_req=0;
                    $scope.project.child_data.source_of_funds.total_prop=0;
                    $scope.project.child_data.use_of_funds.total_req=0;
                    $scope.project.child_data.use_of_funds.total_prop=0;

                    ///sums of blocks
                    for(a in $scope.project.child_data.source_of_funds.external_sources){
                        $scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_ext = parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_ext) + parseFloat($scope.project.child_data.source_of_funds.external_sources[a].proposed_amount);

                        $scope.project.child_data.source_of_funds.sum_source_of_amount_req_ext = parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_req_ext)+parseFloat($scope.project.child_data.source_of_funds.external_sources[a].requested_amount);
                    }
                    for(a in $scope.project.child_data.source_of_funds.internal_sources){
                        $scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_int = parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_int)+parseFloat($scope.project.child_data.source_of_funds.internal_sources[a].proposed_amount);

                        $scope.project.child_data.source_of_funds.sum_source_of_amount_req_int = parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_req_int)+parseFloat($scope.project.child_data.source_of_funds.internal_sources[a].requested_amount);
                    }
                    for(a in $scope.project.child_data.use_of_funds.fixed_assets){
                        $scope.project.child_data.use_of_funds.sum_use_of_amount_req_fixed = parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_req_fixed)+parseFloat($scope.project.child_data.use_of_funds.fixed_assets[a].requested_amount);
                        $scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_fixed = parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_fixed)+parseFloat($scope.project.child_data.use_of_funds.fixed_assets[a].proposed_amount);
                    }
                    for(a in $scope.project.child_data.use_of_funds.working_capital){
                        $scope.project.child_data.use_of_funds.sum_use_of_amount_req_wrkng = parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_req_wrkng)+parseFloat($scope.project.child_data.use_of_funds.working_capital[a].requested_amount);
                        $scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_wrkng = parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_wrkng)+parseFloat($scope.project.child_data.use_of_funds.working_capital[a].proposed_amount);
                    }

                    //percenages + parseFloat()/parseFloat();
                    for(a in $scope.project.child_data.source_of_funds.external_sources){
                        $scope.project.child_data.source_of_funds.external_sources[a].percentage_of_total = (parseFloat($scope.project.child_data.source_of_funds.external_sources[a].proposed_amount)/parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_ext))*100;
                    }
                    for(a in $scope.project.child_data.source_of_funds.internal_sources){
                        $scope.project.child_data.source_of_funds.internal_sources[a].percentage_of_total = (parseFloat($scope.project.child_data.source_of_funds.internal_sources[a].proposed_amount)/parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_int))*100;
                    }
                    for(a in $scope.project.child_data.use_of_funds.fixed_assets){
                        $scope.project.child_data.use_of_funds.fixed_assets[a].percentage_of_total = (parseFloat($scope.project.child_data.use_of_funds.fixed_assets[a].proposed_amount)/parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_fixed))*100;
                    }
                    for(a in $scope.project.child_data.use_of_funds.working_capital){
                        $scope.project.child_data.use_of_funds.working_capital[a].percentage_of_total = (parseFloat($scope.project.child_data.use_of_funds.working_capital[a].proposed_amount)/parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_wrkng))*100;
                    }

                    //Totals
                    $scope.project.child_data.source_of_funds.total_req=  parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_req_ext)+parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_req_int);
                    $scope.project.child_data.source_of_funds.total_prop= parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_ext)+parseFloat($scope.project.child_data.source_of_funds.sum_source_of_amount_proposed_int);
                    $scope.project.child_data.use_of_funds.total_req=    parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_req_wrkng)+parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_req_fixed);
                    $scope.project.child_data.use_of_funds.total_prop=    parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_wrkng)+parseFloat($scope.project.child_data.use_of_funds.sum_use_of_amount_proposed_fixed);



                })
            });

        };
        $scope.getprojectFinancials();

        $scope.addSourceofFunds = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'source-of-funds.html',
                controller: function ($uibModalInstance, $scope, item,  $q, fund_type_id) {
                    var orginalData = {};
                    $scope.popStatus = false;
                    $scope.submitStatus = false;
                    $scope.fund_type_id = fund_type_id;

                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.popStatus=true;
                        $scope.source_of_fundsForm = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        angular.forEach($scope.fund_type_id, function (value, $index) {
                            if(value.id==$scope.source_of_fundsForm.fund_type_id){
                                $scope.source_of_fundsForm.fund_type_idObj=value;
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        $scope.popStatus=true;
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        if(form.$valid){
                            $Data.status = '1';
                            //var lable=$Data.item.name.split(' ').join('_').toLowerCase();
                            //delete $Data['item'];
                            //$Data[lable].push($Data);

                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    fund_type_id: masterService.masterList({'master_key':'project_financials_source_of_funds'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    })


                }
            });
            modalInstance.result.then(function ($data) {
                console.log($data);
                if($data){
                    console.log($scope.project.child_data);
                    var lable_master=$data.fund_type_idObj.master_name.split(' ').join('_').toLowerCase();
                    var lable=$data.fund_type_idObj.name.split(' ').join('_').toLowerCase();
                    $data.fund_type_id=$data.fund_type_idObj.id;
                    delete $data.fund_type_idObj;
                    console.log("after delete",$data.fund_type_idObj);
                    $scope.project.child_data[lable_master][lable].push($data);


                }

            }, function () {
            });
        };

        $scope.addUseofFunds = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'use-of-funds.html',
                controller: function ($uibModalInstance, $scope, item,  $q, fund_type_id) {
                    var orginalData = {};
                    $scope.submitStatus = false;
                    $scope.popStatus = false;
                    $scope.fund_type_id = fund_type_id;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.popStatus = true;
                        $scope.use_of_fundsForm = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        angular.forEach($scope.fund_type_id, function (value, $index) {
                            if(value.id==$scope.use_of_fundsForm.fund_type_id){
                                $scope.use_of_fundsForm.fund_type_idObj=value;
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        $scope.popStatus = true;
                        if(form.$valid){
                            $Data.status = '1';
                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    fund_type_id: masterService.masterList({'master_key':'project_financials_use_of_funds'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    })

                }
            });
            modalInstance.result.then(function ($data) {
                if($data){
                    console.log($scope.project.child_data);
                    var lable_master=$data.fund_type_idObj.master_name.split(' ').join('_').toLowerCase();
                    var lable=$data.fund_type_idObj.name.split(' ').join('_').toLowerCase();
                    $data.fund_type_id=$data.fund_type_idObj.id;
                    delete $data.fund_type_idObj;
                    console.log("after delete",$data.fund_type_idObj);
                    $scope.project.child_data[lable_master][lable].push($data);

                }
            }, function () {
            });
        };
        $scope.removeSourceOfFund = function(row){
            row.status = '0';
        };
        $scope.removeUseOfFund = function(row){
            row.status = '0';
        };
        $scope.submit = function ($inputData , form) {
            $scope.submitStatus = true;
            $inputData.company_id = $rootScope.id_company;
            $inputData.crm_project_id = $rootScope.currentProjectViewId;
            $inputData.id_project_stage_section_form = $rootScope.formId;
            //$inputData.form_key = $rootScope.companyParames.form_key;
            $inputData.user_id = $rootScope.userId;
            if (form.$valid) {
                /*console.log('$inputData',$inputData);*/
                crmProjectService.projectFinancals.post($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.getprojectFinancials();
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })
    .controller('projectGuarentee', function ($scope, $uibModal, $timeout, $rootScope, crmCompanyService, crmProjectService, masterService) {
        $scope.source = {};
        $scope.use_funds = {};
        $scope.project ={};
        $scope.child_data ={};
        /*$scope.getCurrency = function(){
            return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                var data = {};
                for(var a in $result.data){
                    if($result.data[a]['id_company_currency']){
                        data[a] = $result.data[a];
                    }
                }
                console.log('currency',data);
                $scope.currecnyList = data;
            });
        };*/
        //$scope.getCurrency();
        $scope.getprojectFinancials = function(){
            crmProjectService.guarentor.get({'crm_project_id':$rootScope.currentProjectViewId}).then(function (result) {

                console.log('result.data12345',result.data);
                $scope.projectFacilityList=result.data.facilities;
                $scope.contactList=result.data.contacts;
                $scope.loan_amount=result.data.loan_amount;
                $scope.data=result.data;

            })
        };
        $scope.getprojectFinancials();
        /////////////
        $scope.addSourceofFunds = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'source-of-funds.html',
                controller: function ($uibModalInstance, $scope, item,  $q, fund_type_id) {
                    $scope.popStatus = false;
                    $scope.submitStatus = false;
                    $scope.fund_type_id = fund_type_id;

                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.popStatus=true;
                        $scope.source_of_fundsForm = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        angular.forEach($scope.fund_type_id, function (value, $index) {
                            if(value.id==$scope.source_of_fundsForm.fund_type_id){
                                $scope.source_of_fundsForm.fund_type_idObj=value;
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        $scope.popStatus=true;

                        if(form.$valid){
                            $Data.status = '1';
                            //var lable=$Data.item.name.split(' ').join('_').toLowerCase();
                            //delete $Data['item'];
                            //$Data[lable].push($Data);

                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    fund_type_id: masterService.masterList({'master_key':'project_financials_source_of_funds'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    })


                }
            });
            modalInstance.result.then(function ($data) {
                console.log($data);
                if($data){
                    console.log($scope.project.child_data);
                    var lable_master=$data.fund_type_idObj.master_name.split(' ').join('_').toLowerCase();
                    var lable=$data.fund_type_idObj.name.split(' ').join('_').toLowerCase();
                    $data.fund_type_id=$data.fund_type_idObj.id;
                    delete $data.fund_type_idObj;
                    console.log("after delete",$data.fund_type_idObj);
                    $scope.project.child_data[lable_master][lable].push($data);


                }

            }, function () {
            });
        };
        ///////////////
        $scope.addUseofFunds = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'use-of-funds.html',
                controller: function ($uibModalInstance, $scope, item,  $q, fund_type_id) {

                    $scope.submitStatus = false;
                    $scope.popStatus = false;
                    $scope.fund_type_id = fund_type_id;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.popStatus = true;
                        $scope.use_of_fundsForm = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        angular.forEach($scope.fund_type_id, function (value, $index) {
                            if(value.id==$scope.use_of_fundsForm.fund_type_id){
                                $scope.use_of_fundsForm.fund_type_idObj=value;
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        $scope.popStatus = true;
                        if(form.$valid){
                            $Data.status = '1';
                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    fund_type_id: masterService.masterList({'master_key':'project_financials_use_of_funds'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    })

                }
            });
            modalInstance.result.then(function ($data) {
                if($data){
                    console.log($scope.project.child_data);
                    var lable_master=$data.fund_type_idObj.master_name.split(' ').join('_').toLowerCase();
                    var lable=$data.fund_type_idObj.name.split(' ').join('_').toLowerCase();
                    $data.fund_type_id=$data.fund_type_idObj.id;
                    delete $data.fund_type_idObj;
                    console.log("after delete",$data.fund_type_idObj);
                    $scope.project.child_data[lable_master][lable].push($data);

                }
            }, function () {
            });
        };
        $scope.removeSourceOfFund = function(row){
            row.status = '0';
        };
        $scope.removeUseOfFund = function(row){
            row.status = '0';
        };
        $scope.submit = function ($inputData , form) {
            $scope.submitStatus = true;
            $inputData.company_id = $rootScope.id_company;
            $inputData.crm_project_id = $rootScope.currentProjectViewId;
            $inputData.id_project_stage_section_form = $rootScope.formId;
            //$inputData.form_key = $rootScope.companyParames.form_key;
            $inputData.user_id = $rootScope.userId;


            if (form.$valid) {

                console.log('$inputData',$inputData);
                crmProjectService.projectFinancals.post($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.getprojectFinancials();
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })






/**
 * Created by rameshpaul on 1/10/16.
 */

angular.module('app')
    .controller('sectorDashboard',function($scope){
        $scope.country_product={
            "type": "ComboChart",
            "options": {
                seriesType: 'bars',
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top',alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#ff0100'},
                    3: {color: '#01b0f1'},
                    4: {color: '#01b051'}
                }
            },
            "data": [
                ['Country','Equity','Lease','Loan ST','Loan MT','Loan LT'],
                ['Kenya USD/KES',15000,8000,4000,3000,9000],
                ['Rawanda USD/RWF/KES',0,8000,2000,3000,15000],
                ['Tanzania USD/TZS',0,8000,2000,0,0],
                ['Uganda USD/UGX',0,0,0,0,9000]
            ]
        };

        $scope.out_standing_portfolio={
            "type": "BarChart",
            "options": {
                chartArea: {width: '70%'},
                isStacked: true,
                orientation: 'horizontal',

                hAxis: {
                    minValue: 0,
                },
                vAxis: {},
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top',alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#fe0000'},
                    3: {color: '#01b0f1'},
                    4: {color: '#00af50'}
                }
            },
            "data": [
                ['days','USD','KES','UGX','EUR','RWF'],
                ['Jan',1000,1000,1000,2000,5000],
                ['Feb',2000,1000,1000,2000,5000],
                ['Mar',3000,15000,1000,4000,5000],
                ['Apr',4000,15000,2000,5000,5000],
                ['May',5000,1000,1000,6000,5000],
                ['Jun',6000,12000,1000,7000,5000],
                ['Jul',7000,1000,1000,1500,5000],
                ['Aug',8000,15000,1000,2000,5000],
                ['Sep',9000,5000,1000,4000,5000],
                ['Oct',10000,8000,1000,5000,5000],
                ['Nov',11000,45200,1000,6000,5000],
                ['Dec',12000,15000,1000,7000,5000]
            ]
        };

        $scope.sector_limit={
            "type": "PieChart",
            "options": {
                backgroundColor: 'transparent',
                legend: {position: "none"},
                tooltip: {trigger: 'none'},
                series: {
                    0: {color: '#f47677'},
                    1: {color: '#3ab54a'},
                    2: {color: '#2e3192'}
                }
            },
            "data": [
                ['Task','Hours per Day'],
                ['Work',11],
                ['Eat',2],
                ['Commute',2]
            ]
        }
    })

    .controller('loanOfficerDashboard',function($scope,$rootScope,crmDashboardService,NgMap,$uibModal,covenantService ){
        $scope.sectorWiseLoans=[['Task','Hours per Day']];
        $scope.sectir_vise_loans={
            "type": "PieChart",
            "options": {
                chartArea: {width: '90%'},
                legend: {position: 'bottom',alignment: 'start'},
                pieHole: 0.4,
                slices: {0: {color: '#85c976'},1: {color: '#0bc3df'},2: {color: '1e74eb'},3: {color: 'f1a80b'}}
            },
            "data": $scope.sectorWiseLoans
        };

        $scope.loanOfficerDashboardSectorWiseLoans=function(){
            crmDashboardService.loanOfficer.loanOfficerDashboardSectorWiseLoans({'user_id': $rootScope.userId}).then(function(result){
                $scope.loanOfficerDashboardSectorWiseLoans=result.data;
                //$scope.sectorWiseLoans.push($scope.loanOfficerDashboardSectorWiseLoans);
                angular.forEach($scope.loanOfficerDashboardSectorWiseLoans,function(pValue,pKey){
                    $scope.sectorWiseLoans.push([pValue.sector_name,parseInt(pValue.loans)]);
                });
            })
        }

        $scope.loanOfficerDashboardDetails=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.getDetails({'user_id': user_id}).then(function(result){
                $scope.loanOfficerDetails = result.data
                /*$scope.loanOfficerDetails=result.data['current'][0];
                $scope.loanOfficerPrevDetails=result.data['previous'][0];*/
            })
        }
        $scope.childUsersList=[];
        $scope.childUsers=function(){
            crmDashboardService.loanOfficer.childUsers({'user_id': $rootScope.userId}).then(function(result){
                $scope.childUsersList=result.data;
            })
        }
        $scope.childUserData=function(user_id){
            if(user_id=='null'&&user_id==''){
                return false;
            }
            $scope.loanOfficerDashboardDetails(user_id);
        }

        $scope.userProjectList=[];
        $scope.projectList=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.projectList({
                'user_id': user_id,
                'company_id': $rootScope.id_company,
                'offset': 0,
                'limit': 5
            }).then(function(result){
                $scope.userProjectList=result.data.data;
            });
        }
        $scope.projectList();

        $scope.userFacilityCollection=[];
        $scope.FacilityCollections=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.FacilityCollections({
                'user_id': user_id,
                'offset': 0,
                'limit': 5
            }).then(function(result){
                $scope.userFacilityCollection=result.data;
            });
        }
        $scope.FacilityCollections();

        $scope.userMapCountries = [];
        $scope.selectedMapCountry = "";
        $scope.userMapCountryList = function (user_id)
        {
            if(!user_id)
            {
                user_id = $rootScope.userId;
            }
            crmDashboardService.loanOfficer.userMapCountryList({'user_id': user_id}).then(function (result)
            {
                $scope.userMapCountries = result.data;
                if(result.data.length > 0){
                    $scope.selectedMapCountry =  result.data[0].country_id;
                    $scope.MapCountryWiseProjects($scope.selectedMapCountry)
                }
            });
        }
        /*Removed*/
        //$scope.userMapCountryList();


        $scope.vm = {};

        $scope.vm.image = {
            url: 'images/marker/doubtful.png',
            size: [32, 32],
            origin: [0,0],
            anchor: [0, 32]
        };
        $scope.getMarkerImage= function(status){
            var obj = {
                size: [32, 32],
                origin: [0,0],
                anchor: [0, 32]
            };
            switch(status){
                case 'NORM': obj.url ='images/marker/normal.png';break;
                case 'LOSS': obj.url ='images/marker/loss.png';break;
                case 'SUBS': obj.url ='images/marker/substandard.png';break;
                case 'WATC': obj.url ='images/marker/watch.png';break;
                case 'DOUB': obj.url ='images/marker/doubtful.png';break;
                default:obj.url ='images/marker/default.png';

            }
            return obj;


        }
        $scope.vm.shape = {
            coords: [1, 1, 1, 20, 18, 20, 18 , 1],
            type: 'poly'
        };
       /* $scope.mapProjectList = [
            ['Bondi Beach', -33.890542, 151.274856, 4],
            ['Coogee Beach', -33.923036, 151.259052, 5],
            ['Cronulla Beach', -34.028249, 151.157507, 3],
            ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
            ['Maroubra Beach', -33.950198, 151.259302, 1]
        ];*/


        NgMap.getMap({id:"map"}).then(function(map) {
            $scope.map = map;
        });
        $scope.markerClick = function(event,markerData){
            $scope.selectedMarker = markerData;
            $scope.map.showInfoWindow('myInfoWindow', this);
        }
        $scope.mapProjectList = [];

        $scope.MapCountryWiseProjects=function(country_id){
            $scope.mapShow = false;
            if(!country_id){
                country_id=$rootScope.country_id;
            }
            crmDashboardService.loanOfficer.MapCountryWiseProjects({
                'user_id': $rootScope.userId,
                'country_id': country_id
            }).then(function(result){
                $scope.mapProjectList = [];
                angular.forEach(result.data,function(data){
                    var obj = {};
                    obj.company_name = data.company_name;
                    obj.amount = data.amount;
                    obj.facility_count = data.facility_count;
                    obj.index = $scope.mapProjectList.length+1;
                    obj.cp_status = data.cp_status;
                    obj.icon = $scope.getMarkerImage(data.cp_status);
                    if(data.latitude && data.longitude){
                        obj.location = data.latitude+','+data.longitude;
                    }else{
                        obj.location = data.country_name;
                    }

                    $scope.mapProjectList.push(obj);
                    $scope.mapShow = true;
                })
                //$scope.mapProjectList = $scope.mapData;
                //console.log('$scope.mapProjectList',$scope.mapProjectList);
                // $scope.mapProjectList=result.data;

            });
        }

        $scope.notificationData=[];
        $scope.DashboardNotifications=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.DashboardNotifications({
                'user_id': user_id,
                'company_id': $rootScope.id_company
            }).then(function(result){
                $scope.notificationData=result.data;
            });
        }
        $scope.DashboardNotifications();

        $scope.DashboardCovenants=[];
        $scope.DashboardCovenantsList=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.DashboardCovenantsList({
                'assigned_to': user_id,
                'created_by': user_id,
                'company_id': $rootScope.id_company,
                'limit': 2,
                'offset': 0
            }).then(function(result){
                $scope.DashboardCovenants=result.data.data;
            });
        }
        $scope.DashboardCovenantsList();

        /*$scope.DashboardTasks=[];
        $scope.DashboardTasksList=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.DashboardTasksList({
                'assigned_to': user_id,
                'created_by': user_id,
                'user_id': user_id,
                'module_type': 'general',
                'limit': 2,
                'offset': 0
            }).then(function(result){
                $scope.DashboardTasks=result.data.data;
            });
        }
        $scope.DashboardTasksList();*/

        $scope.DashboardKnowledge=[];
        $scope.DashboardKnowledgeList=function(user_id){
            if(!user_id){
                user_id=$rootScope.userId;
            }
            crmDashboardService.loanOfficer.DashboardKnowledgeList({
                'created_by': user_id,
                'company_id': $rootScope.id_company,
                'limit': 2,
                'offset': 0
            }).then(function(result){
                $scope.DashboardKnowledge=result.data.data;
            });
        }
        $scope.DashboardKnowledgeList();

        crmDashboardService.loanOfficer.loanOfficerStageAmountDetails({
            'user_id': $rootScope.userId,
            'startDate': '2016-01-01',
            'endDate': '2016-11-01'
        }).then(function(result){
            $scope.loanOfficerGraphDetails=result.data.Details[0];
            $scope.loanOfficerGraph=result.data.Graph;
            $scope.durations=[];
            $scope.approvals=[];
            $scope.collections=[];
            $scope.disbursements=[];
            $scope.preapprovals=[];
            angular.forEach($scope.loanOfficerGraph,function(pValue,pKey){
                $scope.durations.push(pKey);
                $scope.approvals.push(parseInt(pValue.approved));
                $scope.collections.push(parseInt(pValue.collections));
                $scope.disbursements.push(parseInt(pValue.disbursements));
                $scope.preapprovals.push(parseInt(pValue.pipeline));
            });
            $scope.initGraph();
        })

        $scope.initGraph=function(){
            $scope.chartData={
                chart: {
                    type: 'areaspline'
                },
                title: {
                    text: false
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: -300,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: (Highcharts.theme&&Highcharts.theme.legendBackgroundColor)||'#FFFFFF'
                },
                xAxis: {
                    categories: $scope.durations,
                    gridLineWidth: 1,
                    gridLineDashStyle: 'longdash'
                },
                yAxis: {
                    title: {
                        text: false
                    },
                    gridLineWidth: 1,
                    gridLineDashStyle: 'longdash'
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' ',
                    crosshairs: true
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    areaspline: {
                        fillOpacity: 0.6
                    }
                },
                colors: ['#99cc65','#f1a909','#00cccd','#3366ff'],
                series: [{
                    name: 'ITS Accepted',
                    data: $scope.preapprovals
                },{
                    name: 'Approved',
                    data: $scope.approvals
                },{
                    name: 'Disbursment',
                    data: $scope.disbursements
                },{
                    name: 'Monitoring',
                    data: $scope.collections
                }]
            }
        }
        $scope.covanentPreview = function (params) {

            $scope.params = {};
            angular.copy(params,$scope.params);
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                resolve: {
                    item: function () {
                        if ($scope.params.module_type) {
                            return covenantService.crmCovenant.get($scope.params).then(function ($result) {
                                return $result.data;
                            });
                        }
                    }
                },
                templateUrl: 'partials/crm/covenant/covenent-view.html',
                controller: function ($scope, $rootScope, $uibModalInstance, item) {
                    $scope.cTaskHistroy = {}
                    $scope.getCovenantHistory = function (tableState) {
                        covenantService.crmCovenant.taskHistory(tableState).then(function (result) {
                            $scope.refTableState = tableState;
                            $scope.cTaskHistroy[tableState.module_covenant_id] = result.data.data;
                            // tableState.pagination.
                            tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
                        })
                    }
                    if (item) {
                        $scope.covenantCrmList = item;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

    })

    .controller('multipleSectorDashboard',function($scope){
        $scope.sector_limit={
            "type": "PieChart",
            "options": {
                backgroundColor: 'transparent',
                legend: {position: "none"},
                tooltip: {trigger: 'none'},
                series: {
                    0: {color: '#f47677'},
                    1: {color: '#3ab54a'},
                    2: {color: '#2e3192'}
                }
            },
            "data": [
                ['Task','Hours per Day'],
                ['Work',11],
                ['Eat',2],
                ['Commute',2]
            ]
        }
        $scope.out_standing_portfolio={
            "type": "BarChart",
            "options": {
                chartArea: {width: '70%'},
                isStacked: true,
                orientation: 'horizontal',

                hAxis: {
                    minValue: 0,
                },
                vAxis: {},
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top',alignment: 'end'},
                series: {
                    0: {color: '#2e3192'},
                    1: {color: '#ffc000'},
                    2: {color: '#fe0000'},
                    3: {color: '#01b0f1'},
                    4: {color: '#00af50'}
                }
            },
            "data": [
                ['days','USD','KES','UGX','EUR','RWF'],
                ['Jan',1000,1000,1000,2000,5000],
                ['Feb',2000,1000,1000,2000,5000],
                ['Mar',3000,15000,1000,4000,5000],
                ['Apr',4000,15000,2000,5000,5000],
                ['May',5000,1000,1000,6000,5000],
                ['Jun',6000,12000,1000,7000,5000],
                ['Jul',7000,1000,1000,1500,5000],
                ['Aug',8000,15000,1000,2000,5000],
                ['Sep',9000,5000,1000,4000,5000],
                ['Oct',10000,8000,1000,5000,5000],
                ['Nov',11000,45200,1000,6000,5000],
                ['Dec',12000,15000,1000,7000,5000]
            ]
        };

        $scope.exposure_product_types={
            "type": "LineChart",
            "options": {
                chartArea: {width: '70%'},
                hAxis: {
                    logScale: true
                },
                vAxis: {
                    logScale: false
                },
                animation: {
                    duration: 1200,
                    easing: 'inAndOut',
                    startup: true
                },
                legend: {position: 'top',alignment: 'end'},
                series: {
                    0: {color: '#f3a036'},
                    1: {color: '#ee1d23'},
                    2: {color: '#b5c421'},
                    3: {color: '#9f397b'},
                    4: {color: '#36a8a0'}

                },
                lineWidth: 4
            },
            "data": [
                ['days','Normal','Watch','Substandard','Doubtful','Less'],
                ['Jan',10000,8000,5000,2000,5000],
                ['Feb',2000,30000,20000,2000,5000],
                ['Mar',3000,15000,20000,4000,5000],
                ['Apr',4000,15000,2000,5000,5000],
                ['May',5000,1000,1000,6000,5000],
                ['Jun',6000,12000,20000,7000,5000],
                ['Jul',7000,1000,20000,1500,5000],
                ['Aug',8000,15000,20000,2000,5000],
                ['Sep',9000,5000,20000,4000,5000],
                ['Oct',10000,8000,20000,5000,5000],
                ['Nov',11000,45200,20000,6000,5000],
                ['Dec',12000,15000,20000,7000,5000]
            ]
        };

    })
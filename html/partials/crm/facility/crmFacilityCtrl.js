/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('crm.facility',[])
    .controller('crmFacilityListCtrl',function($scope,$rootScope,crmFacilityService,$uibModal){
        $scope.collateralList=[];
        $scope.filter_type='all_collateral';
        $scope.getFacilityList=function(stTable){
            stTable.company_id=$rootScope.id_company;
            if(!stTable)
                stTable=$scope.refTable;
            $scope.refTable=stTable;
            stTable.company_id=$rootScope.id_company;
            stTable.assigned_to=$rootScope.userId,stTable.created_by=$rootScope.userId;
            crmFacilityService.crm.get(stTable).then(function(result){
                $scope.facilityList=result.data.data;
                $scope.all_facilites_count=result.data.total_records;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }

        $scope.preview = function (params) {
            $scope.params = {};
            angular.copy(params,$scope.params);
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                resolve: {
                    item: function () {
                        if ($scope.params.project_facility_id) {
                            return crmFacilityService.crm.getById($scope.params).then(function ($result) {
                                if($result){
                                    $result = $result.data[0];
                                    $scope.facilityInfo = $result;
                                    $scope.pricingList = $result.pricing;
                                    $scope.termsConditionsList = $result.terms_conditions;
                                }
                                return $scope;
                            });
                        }
                    }
                },
                templateUrl: 'partials/crm/facility/facility-view.html',
                controller: function ($scope, $rootScope, $uibModalInstance, item, companyService,meetingService) {
                    $scope.company_id = $rootScope.id_company;
                    console.log('item',item);
                    if (item) {
                        $scope = item;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
    })



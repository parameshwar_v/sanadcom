/**
 * Created by THRESHOLD on 2016-10-18.
 */
angular.module('app')

.controller('homeReport', function ($scope,$rootScope,reportService,toBillionFilter) {
    reportService.reports.availableAnalysisYears({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.allMonths = result.data;
        console.log('$scope.allMonths',$scope.allMonths);
    });
    //calculateGraph table
    $rootScope.calculateGraph = function(currnt, compair){
        var obj = currnt;
        angular.forEach(obj.head,function(val,key){
            angular.forEach(compair.head,function(v,k){
                if(k == key){
                    var graph = '';
                    if(parseFloat(val.amount[0].amount) == parseFloat(v.amount.amount)){
                        graph = 'icon-up-arrow arrow-nutral';
                    }else if(parseFloat(val.amount[0].amount) < parseFloat(v.amount.amount)){
                        graph = 'icon-down-arrow arrow-down';
                    }else if(parseFloat(val.amount[0].amount) > parseFloat(v.amount.amount)){
                        graph = 'icon-up-arrow arrow-up';
                    }

                    val.amount[0].graph = graph;
                    //val.amount[0].graph = (parseInt(val.amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt(val.amount.amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';

                }

            });
        });
        return obj;
    }

    //build product object
    $rootScope.buildObjProduct = function(data,type){
        $scope.firstData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.firstData.totalSum = $scope.firstData.total_products_clients: $scope.firstData.totalSum = $scope.firstData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.firstData.totalSum});
        angular.forEach($scope.firstData.products,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.product_name,'amount':[{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,product.amount)}]});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.product_name,'is_sub':true,'amount':[{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,item.amount)}]}));
            })
        });
        return myObj;
    }
    $rootScope.secondBuildObjProduct = function(data,type){
        $scope.secoundData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.secoundData.totalSum = $scope.secoundData.total_products_clients: $scope.secoundData.totalSum = $scope.secoundData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.secoundData.totalSum});
        angular.forEach($scope.secoundData.products,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.product_name,'amount':{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.secoundData.totalSum,product.amount)}});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.product_name,'is_sub':true,'amount':{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.secoundData.totalSum,item.amount)}}));
            })
        });
        return myObj;
    }

    //build client
    $rootScope.buildObjClient = function(data,type){
        $scope.firstData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.firstData.totalSum = $scope.firstData.total_products_clients: $scope.firstData.totalSum = $scope.firstData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.firstData.totalSum});
        angular.forEach($scope.firstData.products,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.product_name,'amount':[{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,product.amount)}]});
            angular.forEach(product.items,function(item){
                (type == 'client')?item.amount = item.clients : item.amount = item.amount ;
                myObj.head.push(({'product_name':item.product_name,'is_sub':true,'amount':[{'item.amount':item.amount,'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,item.amount)}]}));
            })
        });
        return myObj;
    }
    $rootScope.secondBuildObjClient = function(data,type){
        $scope.secoundData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.secoundData.totalSum = $scope.secoundData.total_products_clients: $scope.secoundData.totalSum = $scope.secoundData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.secoundData.totalSum});
        angular.forEach($scope.secoundData.products,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.product_name,'amount':{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.secoundData.totalSum,product.amount)}});
            angular.forEach(product.items,function(item){
                (type == 'client')?item.amount = item.clients : item.amount = item.amount ;
                myObj.head.push(({'product_name':item.product_name,'is_sub':true,'amount':{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.secoundData.totalSum,item.amount)}}));
            })
        });
        return myObj;
    }

    //build firstBuildObjEs of  Es
    $rootScope.firstBuildObjEs = function(data,type){
        $scope.firstDataEs = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.firstDataEs.totalSum = $scope.firstDataEs.total_products_clients: $scope.firstDataEs.totalSum = $scope.firstDataEs.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.firstDataEs.totalSum});
        angular.forEach($scope.firstDataEs.grades,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.grade,'description':product.description,'amount':[{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.firstDataEs.totalSum,product.amount)}]});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.grade,'is_sub':true,'amount':[{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.firstDataEs.totalSum,item.amount)}]}));
            })
        });
        return myObj;
    }
    //build Outstanding Portfolio & Number of  Es
    $rootScope.secondBuildObjEs = function(data,type){
        $scope.secoundDataEs = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.secoundDataEs.totalSum = $scope.secoundDataEs.total_products_clients: $scope.secoundDataEs.totalSum = $scope.secoundDataEs.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.secoundDataEs.totalSum});
        angular.forEach($scope.secoundDataEs.grades,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.grade,'amount':{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.secoundDataEs.totalSum,product.amount)}});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.grade,'is_sub':true,'amount':{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.secoundDataEs.totalSum,item.amount)}}));
            })
        });
        return myObj;
    }

    //calculatePercentage
    $rootScope.calculatePercentage = function(total, val){
        return (total == 0 || val == null)?'0':((parseInt(val)/parseInt(total))*100).toFixed(2);
    }

    $rootScope.buildPortpolioByProduct = function(data){
        $rootScope.portfolio_by_procuct = [];
        angular.forEach(data.data.products , function(v,k){
            $rootScope.portfolio_by_procuct.push({name: v.product_name, y: parseInt(v.amount),extra:data.data.total_outstanding_portfolio});
        })
    };
    $rootScope.buildPortpolioByCountry = function(data){
        $rootScope.portfolio_by_country = [];
        angular.forEach(data.data.countries, function(v,k){
            $rootScope.portfolio_by_country.push({name: v.country_name, y: parseInt(v.amount),extra:data.data.total_outstanding_portfolio});
        })
    };
    $rootScope.buildPortpolioByCurrency = function(data){
        $rootScope.portfolio_by_currency = [];
        angular.forEach(data.data.currencies , function(v,k){
            $rootScope.portfolio_by_currency.push({name: v.currency_code, y: parseInt(v.amount),extra:data.data.total_outstanding_portfolio});
        })
    };
    $rootScope.buildPortpolioBySector = function(data){
        $rootScope.portfolio_by_sector = [];
        angular.forEach(data.data.sectors , function(v,k){
            $rootScope.portfolio_by_sector.push({name: v.sector_name, y: parseInt(v.amount),extra:data.data.total_outstanding_portfolio});
        })
    };
    $rootScope.buildPortpolioByEsGrade = function(data){
        $rootScope.portfolio_by_es_grade = [];
        angular.forEach(data.data.grades , function(v,k){
            $rootScope.portfolio_by_es_grade.push({name: v.grade, y: parseInt(v.amount),extra:data.data.total_outstanding_portfolio});
        })
    };
    // graph status
    reportService.reports.outStandingStatus({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.outStandingStatus = result.data;
    });
})

.controller('summaryCtrl', function ($scope,$rootScope, reportService,$q) {
    $scope.compairMonthData = '';
    $scope.analysisDate = function(analysis_date){
        if(analysis_date == 'null' && analysis_date == ''){ return false; }
        $scope.compleateLoad = false;
        $scope.analysis_date = analysis_date;
        $scope.analysis_head = [];
        var params = {'company_id': $rootScope.id_company, 'analysis_date':analysis_date};
        var que1 = reportService.reports.getPortpolioByCountry(params).then(function (result) {
            $rootScope.buildPortpolioByCountry(result);
        });
        var que2 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $rootScope.buildPortpolioByProduct(result);
        });
        var que3 = reportService.reports.getPortfolioByCurrency(params).then(function (result) {
            $rootScope.buildPortpolioByCurrency(result);
        });
        var que4 = reportService.reports.getPortfolioBySector(params).then(function (result) {
            $rootScope.buildPortpolioBySector(result);
        });
        var que5 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $rootScope.buildPortpolioByEsGrade(result);
        });

        //Outstanding Portfolio
        var que6 = reportService.reports.getProductClients(params).then(function (result) {
            var resultClientsObj = $rootScope.buildObjClient(result.data,'client');
            $scope.secondCompairMonthData = angular.copy(resultClientsObj);
            $scope.clientCompairMonthData = resultClientsObj;

        });

        //Number of Clients
        var que7 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            var resultOutstandingObj = $rootScope.buildObjProduct(result.data,'outstanding');
            $scope.firstCompairMonthData = angular.copy(resultOutstandingObj);
            $scope.compairMonthData = resultOutstandingObj;
        });

        $q.all([que1,que2,que3,que4,que5,que6,que7]).then(function(result) {
            var previousMonth = '';
            angular.forEach($scope.allMonths,function(val,key){
                if(val.analysis_date == analysis_date){
                    if($scope.allMonths.length > key+1){  previousMonth = $scope.allMonths[key+1].analysis_date; }
                }
            });
            if(previousMonth != ''){
                reportService.reports.getProductOutstandingPortfolio({'company_id': $rootScope.id_company,'analysis_date':previousMonth}).then(function (result) {
                    var result = $rootScope.secondBuildObjProduct(result.data,'outstanding');
                    $scope.compairMonthData = $rootScope.calculateGraph($scope.compairMonthData,result);
                    console.log('$scope.compairMonthData',$scope.compairMonthData);
                });
                reportService.reports.getProductClients({'company_id': $rootScope.id_company, 'analysis_date':previousMonth}).then(function (result) {
                 var result = $scope.secondBuildObjClient(result.data,'client');
                  $scope.clientCompairMonthData = $rootScope.calculateGraph($scope.clientCompairMonthData,result);
                });
            }

            $scope.analysis_head.push({'year':analysis_date});
            $scope.compleateLoad = true;
        });
    }

    //build Outstanding Portfolio & Number of Clients compair previous year
    $scope.newRecordInsert = function(analysis_date){
        if(analysis_date == 'null' && analysis_date == ''){ return false; }
        var params = {'company_id': $rootScope.id_company, 'analysis_date':analysis_date};
        reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $scope.childCompairMonthData = $rootScope.secondBuildObjProduct(result.data,'outstanding');
            $scope.compairMonthData.total.push($scope.childCompairMonthData.total[0]);
            angular.forEach($scope.compairMonthData.head,function(val,key){
                angular.forEach($scope.childCompairMonthData.head,function(v,k){
                    if(k == key){
                        //v.amount.graph = (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getProductClients(params).then(function (result) {
            $scope.childClientCompairMonthData = $rootScope.secondBuildObjClient(result.data,'client');
            $scope.clientCompairMonthData.total.push($scope.childClientCompairMonthData.total[0]);
            angular.forEach($scope.clientCompairMonthData.head,function(val,key){
                angular.forEach($scope.childClientCompairMonthData.head,function(v,k){
                    if(k == key){
                        //v.amount.graph = (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });
        $scope.analysis_head.push({'year':analysis_date});
    }
})

.controller('assessmentCountryCtrl', function ($scope,$rootScope, reportService,$q) {
    reportService.reports.availableAnalysisCountries({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.allCountries = result.data;
    });

     $scope.currencyStatus = function(params){ console.log('params',params); }
    //Analysis graphs
    $scope.currencyAnalysis = function(params){
        reportService.reports.portfolioByCurrencyAnalysis(params).then(function (result) {
            $scope.portfolioByCurrencyAnalysis = result.data;
        });
    };


    $scope.sectorAnalysis = function(params){
        reportService.reports.portfolioBySectorAnalysis(params).then(function (result) {
            $scope.portfolioBySectorAnalysis = result.data;
        });
    };


    $scope.productAnalysis = function(params){
        reportService.reports.portfolioByProductAnalysis(params).then(function (result) {
            $scope.portfolioByProductAnalysis = result.data;
        });
    };


     $scope.reloadGraphs = function(params){
         $scope.productAnalysis(params);
         $scope.sectorAnalysis(params);
         $scope.currencyAnalysis(params);
     }
    //Analysis graphs end

    $scope.submitForm = function(data){
        $scope.analysis_head = [];
        $scope.compleateLoad = false;
        var params = {'company_id': $rootScope.id_company, 'analysis_date':data.analysis_date, 'country_id':data.analysis_country};
        $scope.reloadGraphs(params);
        var que1 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $rootScope.buildPortpolioByProduct(result);
        });

        var que2 = reportService.reports.getPortfolioByCurrency(params).then(function (result) {
            $rootScope.buildPortpolioByCurrency(result);
        });

        var que3 = reportService.reports.getPortfolioBySector(params).then(function (result) {
            $rootScope.buildPortpolioBySector(result);
        });

        var que4 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $rootScope.buildPortpolioByEsGrade(result);
        });

        //Outstanding Portfolio
        var que5 = reportService.reports.getProductClients(params).then(function (result) {
            //console.log('result.data',result.data);
            var resultClientsObj = $rootScope.buildObjClient(result.data,'client');
            $scope.secondCompairMonthData = angular.copy(resultClientsObj);
            $scope.clientCompairMonthData = resultClientsObj;
        });

        //Number of Clients
        var que6 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            var resultOutstandingObj = $rootScope.buildObjProduct(result.data,'outstanding');
            $scope.firstCompairMonthData = angular.copy(resultOutstandingObj);
            $scope.compairMonthData = resultOutstandingObj;
        });

        //Number of EsGrade
        var que7 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            var resultEsGradeObj = $rootScope.firstBuildObjEs(result.data,'outstanding');
            $scope.threadCompairMonthData = angular.copy(resultEsGradeObj);
            $scope.EsGradecompairMonthData = resultEsGradeObj;
        });

        //Number of Currency
        var que8 = reportService.reports.outstandingExposureByCurrency(params).then(function (result) {
            $scope.outstandingExposureByCurrency = result.data;
        });

        //Number of Sectors
        var que9 = reportService.reports.outstandingExposureBySector(params).then(function (result) {
            $scope.outstandingExposureBySector = result.data;
        });
        //Number of Product
        var que10 = reportService.reports.outstandingExposureByProduct(params).then(function (result) {
            $scope.outstandingExposureByProduct = result.data;
            var myObj =[];
            angular.forEach(result.data.products,function(product){
                myObj.push(product);
                angular.forEach(product.items,function(item){
                    item.is_sub=true;
                    myObj.push(item);
                })
            });
            $scope.outstandingExposureByProduct.products = myObj;
        });

        $q.all([que1,que2,que3,que4,que5,que6,que7,que8,que9,que10]).then(function(result) {

            var previousMonth = '';
            angular.forEach($scope.allMonths,function(val,key){
                if(val.analysis_date == data.analysis_date){
                    if($scope.allMonths.length > key+1){  previousMonth = $scope.allMonths[key+1].analysis_date; }
                }
            });
            if(previousMonth != ''){
                reportService.reports.getProductOutstandingPortfolio({'company_id': $rootScope.id_company,'country_id':data.analysis_country,'analysis_date':previousMonth}).then(function (result) {
                    var result = $rootScope.secondBuildObjProduct(result.data,'outstanding');
                    $scope.compairMonthData = $rootScope.calculateGraph($scope.compairMonthData,result);
                });
                reportService.reports.getProductClients({'company_id': $rootScope.id_company,'country_id':data.analysis_country,'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObjClient(result.data,'client');
                    $scope.clientCompairMonthData = $rootScope.calculateGraph($scope.clientCompairMonthData,result);
                });
                reportService.reports.getPortfolioByEsGrade({'company_id': $rootScope.id_company,'country_id':data.analysis_country,'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObjEs(result.data,'outstanding');
                    $scope.EsGradecompairMonthData = $rootScope.calculateGraph($scope.EsGradecompairMonthData,result);
                });
            }

            $scope.analysis_head.push({'year':data.analysis_date});
            $scope.compleateLoad = true
        });
    }


    //build Outstanding Portfolio & Number of Clients compair previous year
    $scope.newRecordInsert = function(analysis_date,analysis_country){
        if(analysis_date == 'null' && analysis_date == ''){ return false; }
        var params = {'company_id': $rootScope.id_company, 'analysis_date':analysis_date, 'country_id':analysis_country};

        reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $scope.childCompairMonthData = $rootScope.secondBuildObjProduct(result.data,'outstanding');
            $scope.compairMonthData.total.push($scope.childCompairMonthData.total[0]);
            angular.forEach($scope.compairMonthData.head,function(val,key){
                angular.forEach($scope.childCompairMonthData.head,function(v,k){
                    if(k == key){
                        //v.amount.graph = (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getProductClients(params).then(function (result) {
            $scope.childClientCompairMonthData = $rootScope.secondBuildObjClient(result.data,'client');
            $scope.clientCompairMonthData.total.push($scope.childClientCompairMonthData.total[0]);
            angular.forEach($scope.clientCompairMonthData.head,function(val,key){
                angular.forEach($scope.childClientCompairMonthData.head,function(v,k){
                    if(k == key){
                        //v.amount.graph = (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $scope.childEsGradeCompairMonthData = $rootScope.secondBuildObjEs(result.data,'outstanding');
            $scope.EsGradecompairMonthData.total.push($scope.childEsGradeCompairMonthData.total[0]);
            angular.forEach($scope.EsGradecompairMonthData.head,function(val,key){
                angular.forEach($scope.childEsGradeCompairMonthData.head,function(v,k){
                    if(k == key){
                        v.amount.graph = (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });
        $scope.analysis_head.push({'year':analysis_date});
    }
})

.controller('assessmentProductCtrl', function ($scope,$rootScope, reportService,$q) {
    reportService.reports.availableAnalysisProducts({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.allProducts = result.data;
    });

    //Analysis graphs

    $scope.currencyAnalysis = function(params){
        reportService.reports.portfolioByCurrencyAnalysis(params).then(function (result) {
            $scope.portfolioByCurrencyAnalysis = result.data;
        });
    };

    $scope.sectorAnalysis = function(params){
        reportService.reports.portfolioBySectorAnalysis(params).then(function (result) {
            $scope.portfolioBySectorAnalysis = result.data;
        });
    };

    $scope.countryAnalysis = function(params){
        reportService.reports.portfolioByCountryAnalysis(params).then(function (result) {
            $scope.portfolioByCountryAnalysis = result.data;
        });
    };

        $scope.reloadGraphs = function(params){
            $scope.currencyAnalysis(params);
            $scope.sectorAnalysis(params);
            $scope.countryAnalysis(params);
        }
    //Analysis graphs end

    $scope.submitForm = function(data){
        $scope.analysis_head = [];
        $scope.compleateLoad = false;
        var params = {'company_id': $rootScope.id_company, 'analysis_date':data.analysis_date, 'product_id':data.analysis_product};
        $scope.reloadGraphs(params);
        var que1 = reportService.reports.getPortpolioByCountry(params).then(function (result) {
            $rootScope.buildPortpolioByCountry(result);
        });

        var que2 = reportService.reports.getPortfolioByCurrency(params).then(function (result) {
            $rootScope.buildPortpolioByCurrency(result);
        });

        var que3 = reportService.reports.getPortfolioBySector(params).then(function (result) {
            $rootScope.buildPortpolioBySector(result);
        });

        var que4 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $rootScope.buildPortpolioByEsGrade(result);
        });

        //Outstanding Portfolio
        var que5 = reportService.reports.getCountryClients(params).then(function (result) {
            var resultClientsObj = $scope.buildObj(result.data,'client');
            $scope.secondCompairMonthData = angular.copy(resultClientsObj);
            $scope.clientCompairMonthData = resultClientsObj;
        });

        //Number of Clients
        var que6 = reportService.reports.getPortpolioByCountry(params).then(function (result) {
            var resultOutstandingObj = $scope.buildObj(result.data,'outstanding');
            console.log('resultOutstandingObj',resultOutstandingObj);
            $scope.firstCompairMonthData = angular.copy(resultOutstandingObj);
            $scope.compairMonthData = resultOutstandingObj;
        });

        //Number of EsGrade
        var que7 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            var resultEsGradeObj = $rootScope.firstBuildObjEs(result.data,'outstanding');
            $scope.threadCompairMonthData = angular.copy(resultEsGradeObj);
            $scope.EsGradecompairMonthData = resultEsGradeObj;
        });

        //Number of Currency
        var que8 = reportService.reports.outstandingExposureByCurrency(params).then(function (result) {
            $scope.outstandingExposureByCurrency = result.data;
        });

        //Number of Sectors
        var que9 = reportService.reports.outstandingExposureBySector(params).then(function (result) {
            $scope.outstandingExposureBySector = result.data;
        });
        //Number of Product
        var que10 = reportService.reports.outstandingExposureByCountry(params).then(function (result) {
            $scope.outstandingExposureByCountry = result.data;
        });

        $q.all([que1,que2,que3,que4,que5,que6,que7,que8,que9,que10]).then(function(result) {

            var previousMonth = '';
            angular.forEach($scope.allMonths,function(val,key){
                if(val.analysis_date == data.analysis_date){
                    if($scope.allMonths.length > key+1){  previousMonth = $scope.allMonths[key+1].analysis_date; }
                }
            });
            if(previousMonth != ''){

                reportService.reports.getPortpolioByCountry({'company_id': $rootScope.id_company,'product_id':data.analysis_product, 'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObj(result.data,'outstanding');
                    $scope.compairMonthData = $rootScope.calculateGraph($scope.compairMonthData,result);
                });

                reportService.reports.getCountryClients({'company_id': $rootScope.id_company,'product_id':data.analysis_product, 'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObj(result.data,'client');
                    $scope.clientCompairMonthData = $rootScope.calculateGraph($scope.clientCompairMonthData,result);
                });
                reportService.reports.getPortfolioByEsGrade({'company_id': $rootScope.id_company,'product_id':data.analysis_product, 'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObjEs(result.data,'outstanding');
                    $scope.EsGradecompairMonthData = $rootScope.calculateGraph($scope.EsGradecompairMonthData,result);
                });
            }

            $scope.analysis_head.push({'year':data.analysis_date});
            $scope.compleateLoad = true
        });
    }

    //build Country
    $scope.buildObj = function(data,type){
        $scope.firstData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.firstData.totalSum = $scope.firstData.total_country_portfolio_clients: $scope.firstData.totalSum = $scope.firstData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.firstData.totalSum});
        angular.forEach($scope.firstData.countries,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.country_name,'amount':[{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,product.amount)}]});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.country_name,'is_sub':true,'amount':[{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.firstData.totalSum,item.amount)}]}));
            })
        });
        return myObj;
    }
    $scope.secondBuildObj = function(data,type){
        $scope.secondData = data;
        var total = '';var myObj = {};
        myObj.head = [];myObj.total = [];
        (type == 'client')? $scope.secondData.totalSum = $scope.secondData.total_outstanding_portfolio: $scope.secondData.totalSum = $scope.secondData.total_outstanding_portfolio;
        myObj.total.push({'total':$scope.secondData.totalSum});
        angular.forEach($scope.secondData.countries,function(product){
            (type == 'client')?product.amount = product.clients : product.amount = product.amount ;
            myObj.head.push({'product_name':product.country_name,'amount':{'amount':product.amount,'percentage':$rootScope.calculatePercentage($scope.secondData.totalSum,product.amount)}});
            angular.forEach(product.items,function(item){
                myObj.head.push(({'product_name':item.country_name,'is_sub':true,'amount':{'amount':item.amount,'percentage':$rootScope.calculatePercentage($scope.data.totalSum,item.amount)}}));
            })
        });
        return myObj;
    }
    //build Outstanding Portfolio & Number of Clients compair previous year
    $scope.newRecordInsert = function(analysis_date,analysis_product){
        if(analysis_date == 'null' && analysis_date == ''){ return false; }
        var params = {'company_id': $rootScope.id_company, 'analysis_date':analysis_date, 'product_id':analysis_product};

        reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $scope.childCompairMonthData = $scope.secondBuildObj(result.data,'outstanding');
            $scope.compairMonthData.total.push($scope.childCompairMonthData.total[0]);
            angular.forEach($scope.compairMonthData.head,function(val,key){
                angular.forEach($scope.childCompairMonthData.head,function(v,k){
                    if(k == key){
                        v.amount.graph = (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getProductClients(params).then(function (result) {
            $scope.childClientCompairMonthData = $scope.secondBuildObj(result.data,'client');
            $scope.clientCompairMonthData.total.push($scope.childClientCompairMonthData.total[0]);
            angular.forEach($scope.clientCompairMonthData.head,function(val,key){
                angular.forEach($scope.childClientCompairMonthData.head,function(v,k){
                    if(k == key){
                        v.amount.graph = (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $scope.childEsGradeCompairMonthData = $rootScope.secondBuildObjEs(result.data,'outstanding');
            $scope.EsGradecompairMonthData.total.push($scope.childEsGradeCompairMonthData.total[0]);
            angular.forEach($scope.EsGradecompairMonthData.head,function(val,key){
                angular.forEach($scope.childEsGradeCompairMonthData.head,function(v,k){
                    if(k == key){
                        v.amount.graph = (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });
        $scope.analysis_head.push({'year':analysis_date});
    }
})

.controller('assessmentSectorCtrl', function ($scope,$rootScope, reportService,$q) {
    reportService.reports.availableAnalysisSectors({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.allSectors= result.data;
    });

    //Analysis graphs
    $scope.currencyAnalysis = function(params){
        reportService.reports.portfolioByCurrencyAnalysis(params).then(function (result) {
            $scope.portfolioByCurrencyAnalysis = result.data;
        });
    };

    $scope.productAnalysis = function(params){
        reportService.reports.portfolioByProductAnalysis(params).then(function (result) {
            $scope.portfolioByProductAnalysis = result.data;
        });
    };

    $scope.countryAnalysis = function(params){
        reportService.reports.portfolioByCountryAnalysis(params).then(function (result) {
            $scope.portfolioByCountryAnalysis = result.data;
        });
    };
    //Analysis graphs end

        $scope.reloadGraphs = function(params){
            $scope.currencyAnalysis(params);
            $scope.productAnalysis(params);
            $scope.countryAnalysis(params);
        }
    $scope.submitForm = function(data){
        $scope.analysis_head = [];
        $scope.compleateLoad = false;
        var params = {'company_id': $rootScope.id_company, 'analysis_date':data.analysis_date, 'sector_id':data.analysis_sector};
        $scope.reloadGraphs(params);
        var que1 = reportService.reports.getPortpolioByCountry(params).then(function (result) {
            $rootScope.buildPortpolioByCountry(result);
        });

        var que2 = reportService.reports.getPortfolioByCurrency(params).then(function (result) {
            $rootScope.buildPortpolioByCurrency(result);
        });

        var que3 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $rootScope.buildPortpolioByProduct(result);
        });

        var que4 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $rootScope.buildPortpolioByEsGrade(result);
        });

        //Outstanding Portfolio
        var que5 = reportService.reports.getProductClients(params).then(function (result) {
            var resultClientsObj = $scope.buildObjClient(result.data,'client');
            $scope.secondCompairMonthData = angular.copy(resultClientsObj);
            $scope.clientCompairMonthData = resultClientsObj;
        });

        //Number of Clients
        var que6 = reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            var resultOutstandingObj = $scope.buildObjProduct(result.data,'outstanding');
            $scope.firstCompairMonthData = angular.copy(resultOutstandingObj);
            $scope.compairMonthData = resultOutstandingObj;
        });
        //Number of EsGrade
        var que7 = reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            var resultEsGradeObj = $rootScope.firstBuildObjEs(result.data,'outstanding');
            $scope.threadCompairMonthData = angular.copy(resultEsGradeObj);
            $scope.EsGradecompairMonthData = resultEsGradeObj;
        });

        //Number of Currency
        var que8 = reportService.reports.outstandingExposureByCurrency(params).then(function (result) {
            $scope.outstandingExposureByCurrency = result.data;
        });

        //Number of Sectors
        var que9 = reportService.reports.outstandingExposureByProduct(params).then(function (result) {
            $scope.outstandingExposureByProduct = result.data;
            var myObj =[];
            angular.forEach(result.data.products,function(product){
                myObj.push(product);
                angular.forEach(product.items,function(item){
                    item.is_sub=true;
                    myObj.push(item);
                })
            });
            $scope.outstandingExposureByProduct.products = myObj;
        });
        //Number of Product
        var que10 = reportService.reports.outstandingExposureByCountry(params).then(function (result) {
            $scope.outstandingExposureByCountry = result.data;
        });

        $q.all([que1,que2,que3,que4,que5,que6,que7,que8,que9,que10]).then(function(result) {

            var previousMonth = '';
            angular.forEach($scope.allMonths,function(val,key){
                if(val.analysis_date == data.analysis_date){
                    if($scope.allMonths.length > key+1){  previousMonth = $scope.allMonths[key+1].analysis_date; }
                }
            });
            if(previousMonth != ''){
                reportService.reports.getProductOutstandingPortfolio({'company_id': $rootScope.id_company,'sector_id':data.analysis_sector, 'analysis_date':previousMonth}).then(function (result) {
                    var result = $rootScope.secondBuildObjProduct(result.data,'outstanding');
                    $scope.compairMonthData = $rootScope.calculateGraph($scope.compairMonthData,result);
                });
                reportService.reports.getProductClients({'company_id': $rootScope.id_company,'sector_id':data.analysis_sector,  'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObjClient(result.data,'client');
                    $scope.clientCompairMonthData = $rootScope.calculateGraph($scope.clientCompairMonthData,result);
                });
                reportService.reports.getPortfolioByEsGrade({'company_id': $rootScope.id_company,'sector_id':data.analysis_sector,  'analysis_date':previousMonth}).then(function (result) {
                    var result = $scope.secondBuildObjEs(result.data,'outstanding');
                    $scope.EsGradecompairMonthData = $rootScope.calculateGraph($scope.EsGradecompairMonthData,result);
                });
            }

            $scope.analysis_head.push({'year':data.analysis_date});
            $scope.compleateLoad = true
        });
    }
    //build Outstanding Portfolio & Number of Clients compair previous year
    $scope.newRecordInsert = function(analysis_date,analysis_sector) {
        if (analysis_date == 'null' && analysis_date == '') {
            return false;
        }
        var params = {
            'company_id': $rootScope.id_company,
            'analysis_date': analysis_date,
            'sector_id': analysis_sector
        };
        reportService.reports.getProductOutstandingPortfolio(params).then(function (result) {
            $scope.childCompairMonthData = $rootScope.secondBuildObjProduct(result.data, 'outstanding');
            $scope.compairMonthData.total.push($scope.childCompairMonthData.total[0]);
            angular.forEach($scope.compairMonthData.head, function (val, key) {
                angular.forEach($scope.childCompairMonthData.head, function (v, k) {
                    if (k == key) {
                        //v.amount.graph = (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.firstCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getProductClients(params).then(function (result) {
            $scope.childClientCompairMonthData = $rootScope.secondBuildObjClient(result.data, 'client');
            $scope.clientCompairMonthData.total.push($scope.childClientCompairMonthData.total[0]);
            angular.forEach($scope.clientCompairMonthData.head, function (val, key) {
                angular.forEach($scope.childClientCompairMonthData.head, function (v, k) {
                    if (k == key) {
                        //v.amount.graph = (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.secondCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });

        reportService.reports.getPortfolioByEsGrade(params).then(function (result) {
            $scope.childEsGradeCompairMonthData = $rootScope.secondBuildObjEs(result.data,'outstanding');
            $scope.EsGradecompairMonthData.total.push($scope.childEsGradeCompairMonthData.total[0]);
            angular.forEach($scope.EsGradecompairMonthData.head,function(val,key){
                angular.forEach($scope.childEsGradeCompairMonthData.head,function(v,k){
                    if(k == key){
                        //v.amount.graph = (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) == parseInt(v.amount.amount)) ? 'icon-up-arrow arrow-nutral' : (parseInt($scope.threadCompairMonthData.head[key].amount[0].amount) > parseInt(v.amount.amount)) ? 'icon-down-arrow arrow-down' : 'icon-up-arrow arrow-up';
                        val.amount.push(v.amount);
                    }
                });
            });
        });
        $scope.analysis_head.push({'year': analysis_date});
    }
})

.controller('valueAtRiskCtrl', function ($scope,$rootScope, reportService,$q) {
    reportService.reports.availableAnalysisCountries({'company_id': $rootScope.id_company}).then(function (result) {
        $scope.allCountries = result.data;
    });

    $scope.outstandingBorrowersList = [];
    $scope.getOutstandingBorrowersList=function(stTable){
        if(!stTable)
            stTable=$scope.refTable;
        $scope.refTable=stTable;
        reportService.reports.valueAtRiskReports(stTable).then(function(result){
            $scope.outstandingBorrowersList=result.data;
            $scope.totalOutstandingBorrowersAmount=result.data.total_outstanding_borrowers_amount;
            stTable.pagination.numberOfPages=Math.ceil(parseInt(result.data.total_records)/stTable.pagination.number);
            $scope.compleateLoad = true;
        })
    }

    $scope.submitForm = function(data){
        $scope.compleateLoad = true;
        var stTable = $scope.refTable;
        stTable.pagination= {"start":0,"totalItemCount":0,"number":10,"numberOfPages":null};
        stTable.offset = 0;
        stTable.company_id = $rootScope.id_company;
        /*stTable.outstanding_status = data.outstanding_status;*/
        stTable.country_id = data.country_id==undefined?'0':data.country_id;
        stTable.analysis_date=data.analysis_date;
        $scope.getOutstandingBorrowersList(stTable);
    }

})

.controller('sectorLimitsCtrl', function ($scope,$rootScope, reportService,$q) {
    $scope.sectorLimitsList = [];
    $scope.getSectorLimitsListList=function(params){
        reportService.reports.sectorLimits(params).then(function(result){
            $scope.sectorLimitsList=result.data;
        })
    }

    $scope.submitForm = function(data){
        $scope.compleateLoad = true;
        var obj = {};
        obj.company_id = $rootScope.id_company;
        obj.analysis_date = data.analysis_date;
        $scope.getSectorLimitsListList(obj);
    }

})
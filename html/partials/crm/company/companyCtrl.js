/**
 * Created by RAKESH on 18-12-2015.
 */
angular.module('app')
    .controller('companyCtrl', function ($scope, $rootScope, crmCompanyService, masterService) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.companyInfo = {};
        $scope.getCompanyInfo = function () {
            crmCompanyService.companyInformation({'crm_company_id': $rootScope.currentCompanyViewId}).then(function (result) {
                $scope.companyInfo = result.data;
            });
        }
        $scope.formChangeEvent = function(){
            $rootScope.isFormChanged=true;
        };
    })
    .controller('companyListCtrl', function ($scope, $rootScope, masterService, crmCompanyService, $state, quickService, encodeFilter) {
        $scope.companyList = [];
        $scope.company = {};
        $scope.all_companies = 0;
        $scope.my_companies = 0;
        $scope.filter_type_temp = 'all_companies';
        $scope.isEmptyRow = false;
        $scope.getCompanyList = function (tableState) {
            tableState.company_id = $rootScope.id_company;
            tableState.created_by = undefined;
            tableState.company_id = $rootScope.id_company;
            if ($scope.filter_type_temp == 'my_companies') {
                tableState.created_by = $rootScope.userId;
            }

            crmCompanyService.companyList(tableState).then(function (result) {
                $scope.tableStateRef = tableState;
                $scope.companyList = result.data.data;
                $scope.all_companies = result.data.all_companies;
                $scope.my_companies = result.data.my_companies;
                $scope.isEmptyRow = (result.data.data.length == 0) ? true : false;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
            });

        }
        $scope.getCountryList = function(data){
            masterService.country.get(data).then(function ($result) {
                $scope.ISDCodes = $result.data;
            });
        }

        $scope.gotoState = function (row) {
            var Id = encodeFilter(row.id_crm_company);
            $state.go('company.company-creation', {'id': Id});
        }

        $scope.submitBtnDisabled = false;
        $scope.submitted = false;

        $scope.submitQuickCompany = function ($inputData, form, moreFields) {
            $inputData.company_id = $rootScope.id_company;
            $inputData.created_by = $rootScope.userId;
            if($inputData.company_phone_number1){
                $inputData.company_phone_number = $inputData.company_phone_number_isd+' '+$inputData.company_phone_number1;
            }
            quickService.saveQuickCompany($inputData).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                    $scope.getCompanyList($scope.tableStateRef);
                    if (moreFields) {
                        $rootScope.currentCompanyViewId = result.data.crm_company_id;
                        $state.go('company.company-creation', {id: encodeFilter($rootScope.currentCompanyViewId)});
                    }
                    form.$setPristine();
                    $scope.company = {};
                }
            })
        };
        $scope.deleteCompany = function(row) {
            var r=confirm('Are you sure you want to delete? Once deleted cannot be reverted');
            if(r) {
                quickService.deleteCompany({'crm_company_id': row.id_crm_company}).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.getCompanyList($scope.tableStateRef);
                    }
                })
            }
        };
    })
    .controller('CompanyCreationCtrl', function ($rootScope, $scope, $state, crmCompanyService, crmContactService, masterService, $stateParams, decodeFilter, $uibModal, encodeFilter, $q) {
        $scope.isEditMode = function (flag) {
            $scope.isEdit = flag;
        }
        $scope.submitBudgetForm = function (budgetForm) {
        }
        $scope.uploadBudget = function (file) {
            if (file != null && file != '') {
                $scope.budgetExcel = file;
            } else {
                $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
            }
        }
        $rootScope.currentCompanyViewId = decodeFilter($stateParams.id);
        $rootScope.module_id = 2;
        $rootScope.contactId = $rootScope.currentCompanyViewId;
        $rootScope.currentModuleId = 2;
        $scope.companyObj = {};
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;
        $scope.checkForm = [];
        $scope.touchPointsList = [];
        $scope.init = function () {
            $rootScope.companyParames = {};
            $q.all({
                'companyDetails': $scope.getCompanyModule(),
                //  'financialInformaion':$scope.getFinancialInformation(),
                // 'companyAssessment':$scope.getCompanyAssessment(),
                // 'internalRating':$scope.getInternalRating()
            }).then(function (result) {
                $rootScope.companyFormDetails = [];
                angular.forEach($scope.companyForms, function (section) {
                    angular.forEach(section.form, function ($item) {
                        var $obj = {};
                        $obj.sectionName = section.section_name;
                        $obj.sectionKey = section.section_key;
                        $obj.tag = $item.form_name;
                        $obj.formId = $item.id_form;
                        $obj.template = $item.form_template;
                        $obj.form_key = $item.form_key;
                        $rootScope.companyFormDetails.push($obj);
                    })
                })
            })
            $scope.selectedCompanyListFun();
        }
        $scope.percentage = 0;
        $scope.parentSectorsList = [];
        $scope.subSectorList = [];
        $scope.anotherOptions = {
            barColor: '#02B000',
            trackColor: '#f9f9f9',
            scaleColor: '#dfe0e0',
            scaleLength: 5,
            lineCap: 'round',
            lineWidth: 3,
            size: 50,
            rotate: 0,
            animate: {
                duration: 1000,
                enabled: true
            }
        }
        $scope.getCompanyModule = function () {
            return crmCompanyService.companyDetails($rootScope.currentCompanyViewId, $rootScope.module_id).then(function (result) {
                $scope.companyDetails = result.data.details[0];
                $rootScope.business_name = result.data.details[0].company_name;
                $rootScope.grade = result.data.details[0].grade;
                $rootScope.rating = result.data.details[0].rating;
                $rootScope.company_created_date = result.data.details[0].created_date_time;
                $rootScope.sector = result.data.details[0].sector;
                $rootScope.subsector = result.data.details[0].sub_sector;
                $scope.companyForms = result.data.forms;
                return result;
            });
        }
        //financial information
        $scope.getFinancialInformation = function () {
            return crmCompanyService.getFinancialInfo({'crm_company_id': $rootScope.currentCompanyViewId}).then(function ($result) {
                return $scope.financialInfo = $result.data;
            });
        };
        $scope.getCompanyAssessment = function () {
            return crmContactService.getCompanyAssessment({
                company_id: $rootScope.id_company,
                'crm_company_id': $rootScope.currentCompanyViewId
            }).then(function (result) {
                return $scope.companyAssessmentList = result.data;
                //$rootScope.existingAssessmentList = result.data;
            });
        }
        $scope.getInternalRating = function () {
            return crmContactService.getInternalRating({
                company_id: $rootScope.id_company,
                'crm_company_id': $rootScope.currentCompanyViewId
            }).then(function (result) {
                return $scope.internalRatingList = result.data;
            });
        }
        $scope.formView = function (id_form) {
            crmCompanyService.companyFormDetails($rootScope.id_crm_company, id_form, 'view').then(function (result) {
                if (result.status) {
                    $scope.viewContactForm = result.data.data.form_data;
                    if (result.data.data.form_data == undefined) {
                        $scope.viewContactForm = [];
                    }
                    $scope.formName = result.data.data.form_name;
                }
            });
        }
        $rootScope.selectedCompanyList = [];
        $rootScope.selectedCompanyListFun = function () {
            crmCompanyService.getCompanyListForCompany({'crm_company_id': $rootScope.currentCompanyViewId}).then(function (result) {
                $rootScope.selectedCompanyList = result.data;
            });
        };
        /*add TouchPoint*/
        $scope.showReminders = false;
        $scope.showAllPoints = false;
        $scope.touchPointDescription = '';
        $scope.serchType = '';
        $scope.postField = {'reminder_date': new Date()};
        $scope.submit = function (postField, form) {
            $scope.submitStatus = true;
            if (form.$valid) {
                postField.crm_module_id = $rootScope.currentModuleId;
                postField.touch_point_from_id = $rootScope.contactId;
                postField.created_by = $rootScope.userId;
                crmContactService.addTouchPointComment(postField).then(function (result) {
                    $scope.postField = {'reminder_date': new Date()};
                    $scope.submitStatus = false;
                    $scope.togglePoint = false;
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.togglePoint = false;
                        $state.reload();
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                });
            }
        }
        /*Attachments*/
        $scope.quickAttachmentModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                template: '<div class="right-modal modal-custom" data-modal="attachment">' +
                ' <h3 class="right-modal-head">' +
                '<i class="fa fa-paperclip mr8"></i> {{"Attachments" | translate}}' +
                ' <a class="close-right pull-right" ng-click="cancel()" n-t=\'{"e_n":"Close modal","e_d":""}\'><img src="images/close.png"></a>' +
                '</h3>' +
                '<attachment-view module-id="2" ref-id="{{currentCompanyViewId}}"></attachment-view>' +
                '</div>',
                controller: function ($scope, $uibModalInstance) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                // $rootScope.$emit("reloadTouchPoint");
                // $scope.init();
                // $state.reload();
            });
        }
        $scope.AddedCompaniesModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/add-sub-companies.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService) {
                    $rootScope.selectedCompanyListFun();
                    $scope.getCompanyListSearch = function (search_key) {
                        $scope.visibleAddButton = false;
                        if(search_key.length > 3){
                            return crmCompanyService.crmListForMapping({
                                'search_key': search_key,
                                'crm_company_id': $rootScope.currentCompanyViewId
                            }).then(function (result) {
                                return result.data;
                            });
                        }
                    };
                    $scope.getCompanyDesignations = function(key){
                        crmCompanyService.companyDesignation({'map_type':key}).then(function (result) {
                            $scope.companyDesignations = result.data;
                        });
                    }
                    $scope.onSelect = function ($item, $model, $label) {
                        $scope.visibleAddButton = true;
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function (designation, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var fields = {
                                'crm_company_id': $scope.selectedItem.id_crm_company,
                                'parent_company_id': decodeFilter($stateParams.id),
                                'type': designation.id_child,
                                'created_by': $rootScope.userId
                            };
                            crmCompanyService.addSubCompany(fields).then(function (result) {
                                $scope.visibleDesignationDropDown = false;
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedCompanyListFun();
                                    $scope.init();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        };
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.removeContactCompany = function (row) {
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            crmCompanyService.deleteSubModule(row.id_crm_company_relation).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedCompanyListFun();
                                    $scope.init();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            });
                        }
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                //$state.reload();
            });
        };
        /*contacts*/
        $rootScope.selectedContactsList = [];
        $rootScope.getAddedContactsForCompany = function() {
            crmCompanyService.getContactForCompany({'crm_company_id': decodeFilter($stateParams.id)}).then(function(result){
                $rootScope.selectedContactsList = result.data;
            });
        };
        $scope.AddedContactsModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/added-contacts.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService) {
                    $scope.getContactList = function (search_key) {
                        $scope.visibleAddButton = false;
                        if(search_key.length > 3){
                            return crmCompanyService.companyListForMapping({
                                'crm_company_id': decodeFilter($stateParams.id),
                                'search_key': search_key,
                                'company_id': $rootScope.id_company
                            }).then(function (result) {
                                return result.data;
                            });
                        }
                    };
                    $scope.onSelect = function ($item, $model, $label) {
                        $scope.visibleAddButton = true;
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                        $scope.getCompanyDesignations = function(key){
                            crmCompanyService.companyDesignation({'map_type':key}).then(function (result) {
                                $scope.companyDesignations = result.data;
                            });
                        }
                        /* crmContactService.contactType().then(function (response) {
                         $scope.contactType = response.data;
                         });*/
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function (designation, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var fields = {
                                'crm_contact_id': $scope.selectedItem.id_crm_contact,
                                'crm_company_id': $rootScope.currentCompanyViewId,
                                'crm_company_designation_id': designation.id_crm_company_designation,
                                'is_primary_company': designation.is_primary_company,
                                'created_by': $rootScope.userId,
                                'type': 'company'
                            };
                            crmCompanyService.addCompanyToContact(fields).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.visibleDesignationDropDown = false;
                                    $rootScope.getAddedContactsForCompany();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.removeCompanyContact = function ($id) {
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            crmCompanyService.deleteContactCompany($id).then(function (result) {
                                $rootScope.getAddedContactsForCompany();
                            });
                        }
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
            });
        }
        /*Touch points*/
        $scope.touchpointsModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/contact/touchpoints.html',
                controller: 'touchPointsCtrl',
                resolve: {
                    crmParams: {
                        'crm_module_type': 'company',
                        'crm_module_id': $rootScope.currentCompanyViewId
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                $state.reload();
            });
        }
        $scope.goToEditCtrl = function (data) {
            $rootScope.companyParames = data;
            $state.go("company.business-keydata", {'id': encodeFilter($rootScope.currentCompanyViewId)});
        };
    })
    .controller('AddedContactsModalCtrl', function ($rootScope, $scope, $uibModalInstance, crmCompanyService, crmContactService,decodeFilter, $stateParams) {

    })
    .controller('companyFormViewCtrl', function ($rootScope, $scope, $state, crmCompanyService, crmContactService, masterService, $stateParams, decodeFilter, $uibModal, anchorSmoothScroll, $location, underscoreaddFilter, crmProjectService) {
        $rootScope.isFormChanged=false;
        $scope.minDate = $scope.minDate ? null : new Date();
        if ($rootScope.companyParames == undefined) {
            $state.go("company.company-creation", {'id': $stateParams.id});
            return false;
        }
        $scope.company = {};
        $rootScope.id_crm_company = decodeFilter($stateParams.id);
        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;
        $scope.actualObj = {};
        $scope.diffObject = function (a, b) {
            return Object.keys(a).reduce(function (map, k) {
                if (a[k] !== b[k] && !angular.isObject(b[k]) && !angular.isArray(b[k]) && a[k] !== '') map[k] = b[k];
                return map;
            }, {});
        }
        //goto next form
        $scope.nextForm = function () {
            /*var diff = $scope.diffObject($scope.company, $scope.actualObj);
            var keys = Object.keys(diff);
            if (keys.length) {
                var r = confirm("Continue without saving data?");
                if (r) {
                    var nextIndex = $scope.currentFormIndex + 1;
                    $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                    $scope.init();
                }
            } else {
                var nextIndex = $scope.currentFormIndex + 1;
                $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                $scope.init();
            }*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var nextIndex = $scope.currentFormIndex + 1;
                    $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                    $scope.init();
                } else
                    return false;
            } else {
                var nextIndex = $scope.currentFormIndex + 1;
                $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                $scope.init();
            }
        }
        //goto prev form
        $scope.prevForm = function () {
            /*var diff = $scope.diffObject($scope.company, $scope.actualObj);
            var keys = Object.keys(diff);
            if (keys.length) {
                var r = confirm("Continue without saving data?");
                if (r) {
                    var nextIndex = $scope.currentFormIndex - 1;
                    $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                    $scope.init();
                }
            } else {
                var nextIndex = $scope.currentFormIndex - 1;
                $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                $scope.init();
            }*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var nextIndex = $scope.currentFormIndex - 1;
                    $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                    $scope.init();
                } else
                    return false;
            } else {
                var nextIndex = $scope.currentFormIndex - 1;
                $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
                $scope.init();
            }
        }
        $scope.init = function () {
            //find current index
            var a = _.findWhere($rootScope.companyFormDetails, {'tag': $rootScope.companyParames.tag});//searching Element in Array
            $scope.currentFormIndex = _.indexOf($rootScope.companyFormDetails, a);// getting index.
            if ($scope.currentFormIndex != -1) {
                $scope.prevBtnShow = ($scope.currentFormIndex > 0) ? true : false;
                $scope.nextBtnShow = ($scope.currentFormIndex < ($rootScope.companyFormDetails.length - 1)) ? true : false;
            }
            $scope.form_key = $rootScope.companyParames.form_key;
            if ($rootScope.companyParames.formId) {
            }
            $scope.sectionName = $rootScope.companyParames.sectionName;
            $scope.form_key = $rootScope.companyParames.form_key;
            $scope.template = $rootScope.companyParames.template;
            $scope.formName = $rootScope.companyParames.tag;

            $scope.formShow = false;
            switch ($rootScope.companyParames.sectionKey) {

                case 'business_key_data':

                    $scope.business_types = [];
                    $scope.getProofList = function (params) {
                        masterService.masterList(params).then(function (result) {
                            if (result.status)
                                $scope.business_types = result.data;
                        })
                    }
                    crmCompanyService.companyFormDetails(decodeFilter($stateParams.id), $rootScope.companyParames.formId, 'edit').then(function (result) {
                        if (result.data.data.hasOwnProperty('company_nature_of_business')) {
                            if (result.data.data.company_nature_of_business) {
                                result.data.data.company_nature_of_business = JSON.parse(result.data.data.company_nature_of_business);
                            }
                        }
                        $scope.company = result.data.data;
                        if($scope.company.company_phone_number){
                            var phone = $scope.company.company_phone_number.split(' ');
                            if(typeof phone[1] == 'undefined'){
                                $scope.company.company_phone_number1 = $scope.company.company_phone_number;
                            }else{
                                $scope.company.company_phone_number_isd = phone[0];
                                $scope.company.company_phone_number1 = phone[1];
                            }
                        }
                        if($scope.company.company_alternative_number){
                            var phone = $scope.company.company_alternative_number.split(' ');
                            if(typeof phone[1] == 'undefined'){
                                $scope.company.company_alternative_number1 = $scope.company.company_alternative_number;
                            }else{
                                $scope.company.company_alternative_phone_number_isd = phone[0];
                                $scope.company.company_alternative_number1 = phone[1];
                            }
                        }
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.company_created_date = result.data.company_details.created_date_time;
                        $scope.business_name = result.data.company_details.company_name;
                        $scope.formName = result.data.data.form_name;
                        $scope.template = $rootScope.companyParames.template;
                        $scope.formShow = true;

                        $scope.actualObj = angular.copy($scope.company);
                    });
                    $scope.getOwnershipDetails = function (){
                        crmCompanyService.companyShareholderDetails.get({crm_company_id: $rootScope.id_crm_company,
                        }).then(function(result){
                            $scope.ownership_details = result.data;
                        });
                    };
                    $scope.removeItem = function removeItem(row) {
                        if($rootScope.confirmMsg()){
                            crmCompanyService.companyShareholderDetails.delete({
                                crm_company_id: $rootScope.id_crm_company,
                                id_crm_company_shareholder_details: row.id_crm_company_shareholder_details,
                            }).then(function(result){
                                if($rootScope.toastMsg(result))
                                    $scope.getOwnershipDetails();
                            });
                        }
                    };
                    $scope.addOwnerShip = function (row) {
                        $scope.selectedRow = row;
                        var modalInstance = $uibModal.open({
                            animation: true,
                            windowClass:'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            scope: $scope,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'partials/crm/company/forms/ownership-form-fields.html',
                            controller: function ($uibModalInstance, $scope, item, currency, $q) {
                                $scope.ownership = {
                                    shareholder_name: '',
                                    no_of_shares: '',
                                    percentage_share: '',
                                    value_of_shares: '',
                                    currency: ''
                                };
                                $scope.currecnyList = currency;
                                $scope.update = false;
                                $scope.title = 'Add';
                                if (item) {
                                    //$scope.ownership = item;
                                    $scope.submitStatus = true;
                                    $scope.ownership = angular.copy(item);
                                    $scope.update = true;
                                    $scope.title = 'Edit';
                                }
                                $scope.validOwnerShip = function ($Data , val) {
                                    var ownershipSum = 0;
                                    angular.forEach($scope.ownership_details, function (ownership, $index) {
                                        if($Data.id_crm_company_shareholder_details!=ownership.id_crm_company_shareholder_details){
                                            ownershipSum = ownershipSum + parseFloat(ownership.percentage_share);
                                        }
                                    });
                                    if (parseFloat(ownershipSum) + parseFloat(val) > 100) {
                                        $rootScope.toast('Error', ['All Ownership % less than or equal to 100%'], 'error');
                                        return false;
                                    } else {
                                        return true;
                                    }
                                };
                                $scope.cancel = function () {
                                    $uibModalInstance.close();
                                };
                                $scope.save=function($Data, form){
                                    if(form.$valid){
                                        var status = $scope.validOwnerShip($Data , $scope.ownership.percentage_share);
                                        if (status) {
                                            $Data.crm_company_id = $rootScope.id_crm_company;
                                            $Data.created_by = $rootScope.userId;
                                            $Data.company_id = $rootScope.id_company;
                                            crmCompanyService.companyShareholderDetails.post($Data).then(function (result) {
                                                if ($rootScope.toastMsg(result)) {
                                                    $rootScope.toast('Success', result.message);
                                                    $uibModalInstance.close();
                                                    $scope.getOwnershipDetails();
                                                    $scope.getCompanyInfo();
                                                } else {
                                                    $rootScope.toast('Error', result.error);
                                                }
                                            });
                                        }

                                    }
                                }
                            },
                            resolve: {
                                item: function () {
                                    if ($scope.selectedRow) {
                                        return $scope.selectedRow;
                                    }
                                },
                                currency: function () {
                                    return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                                        return $result.data;
                                    });
                                }
                            }
                        });
                        modalInstance.result.then(function ($data) {
                        }, function () {
                        });
                    };
                    $scope.viewOwnership = function (row) {
                        $scope.selectedRow = row;
                        var modalInstance = $uibModal.open({
                            animation: true,
                            //windowClass:'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'partials/crm/company/forms/view-ownership-form.html',
                            controller: function ($uibModalInstance, $scope, item) {
                                $scope.ownership = {};
                                $scope.update = false;
                                if (item) {
                                    $scope.ownership = item;
                                    $scope.update = true;
                                }
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function () {
                                    if ($scope.selectedRow) {
                                        return $scope.selectedRow;
                                    }
                                }
                            }
                        });
                    };
                    $scope.verifyEmail = function(objData){
                        var data = {};
                        data.type = 'company';
                        data.module_id = objData.id_crm_company;
                        data.email = objData.email;
                        crmContactService.emailVerify(data).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.emailVerifyModal();
                            } else {
                                $rootScope.toast('Error',result.message,'error');
                            }
                        });
                    };
                    $scope.emailVerifyModal = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            scope: $scope,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'partials/email-verify.html',
                            controller: function ($scope, $uibModalInstance, crmContactService) {
                                $scope.verify = function (data) {
                                    var fields = {
                                        'module_id': $scope.companyInfo.id_crm_company,
                                        'email': $scope.companyInfo.email,
                                        'code': data.code,
                                        'type': 'company'
                                    };
                                    crmContactService.verifyEmailCode(fields).then(function (result) {
                                        if ($rootScope.toastMsg(result)) {
                                            $scope.getCompanyInfo();
                                            $scope.cancel();
                                        }
                                    });
                                };
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                            }
                        });
                        modalInstance.result.then(function () {
                        }, function () {
                            //$state.reload();
                        });
                    };

                    break;


                case 'financial_information':
                    //console.log('$rootScope.companyParames.form_key', $rootScope.companyParames.form_key);
                    if ($rootScope.companyParames.form_key == 'balance_sheet' || $rootScope.companyParames.form_key == 'account_liabilietes' || $rootScope.companyParames.form_key == 'credit_rating') {
                        crmCompanyService.companyFormDetails(decodeFilter($stateParams.id), $rootScope.companyParames.formId, 'edit').then(function (result) {
                            $scope.company = result.data.data;
                            $scope.percentage = Math.ceil(result.data.percentage);
                            $scope.company_created_date = result.data.company_details.created_date_time;
                            $scope.business_name = result.data.company_details.company_name;
                            $scope.formName = result.data.data.form_name;
                            $scope.template = $rootScope.companyParames.template;
                            $scope.formShow = true;

                            $scope.actualObj = angular.copy($scope.company);
                        });
                    }
                    if ($rootScope.companyParames.form_key == 'balance_sheet') {

                        /* $rootScope.statementId=$rootScope.companyParames.formId;
                         crmCompanyService.getFinancialData({
                         form_key: $rootScope.companyParames.form_key,
                         crm_company_id: $rootScope.id_crm_company
                         }).then(function(result){
                         $scope.statementData=result.data;
                         });*/
                    }
                    $scope.uploadSheet = function (row) {
                        $scope.selectedRow = row;
                        var modalInstance = $uibModal.open({
                            animation: true,
                            //windowClass:'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'upload-balance-sheet.html',
                            controller: function ($uibModalInstance, $scope, Upload) {
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                                $scope.uploadFinancialExcel = function (file) {
                                    if (file != null && file != '') {
                                        $scope.uploadFile = file;
                                        $scope.isExcel = false;
                                    }
                                };
                                $scope.submitUpload = function (statement_id, financial_statement_comments) {
                                    if ($scope.uploadFile == null || $scope.uploadFile == '') {
                                        $scope.isExcel = true;
                                    } else {
                                        $scope.isExcel = false;
                                    }
                                    if ($scope.uploadFile && $scope.uploadFile != null) {
                                        $scope.uploadData = {
                                            'user_id': $rootScope.userId,
                                            'crm_company_id': $rootScope.id_crm_company,
                                            'form_key': $rootScope.companyParames.form_key,
                                            'financial_statement_comments': financial_statement_comments
                                        };
                                        Upload.upload({
                                            url: API_URL + 'Company/financialExcel',
                                            data: {
                                                file: {'excel': $scope.uploadFile},
                                                'financeData': $scope.uploadData
                                            }
                                        }).success(function (result) {
                                            if (result.status) {
                                                $rootScope.toast('Success', result.message);
                                                $scope.statementData = result.data;
                                                $scope.uploadFile = '';
                                                angular.element("input[type='file']").val(null);
                                                $uibModalInstance.close();
                                                $state.reload()
                                            } else {
                                                $rootScope.toast('Error', [result.error], 'error', []);
                                            }
                                        });
                                    }
                                };
                            }
                        });
                        modalInstance.result.then(function ($data) {
                        }, function () {
                        });
                    }

                    break;
                case 'business_assessment':
                    /*Add AddAssessment*/
                    $scope.itemType = 'item';
                    switch ($rootScope.companyParames.form_key) {
                        case 'assessment_questions':
                            $scope.itemType = 'question';
                            break;
                        case 'swot':
                            $scope.itemType = 'item';
                            break;
                        case 'forces':
                            $scope.itemType = 'item';
                            break;
                        case 'factors':
                            $scope.itemType = 'item';
                            break;
                        case 'business_plan':
                            $scope.itemType = 'question';
                            break;
                        case 'business_canvas':
                            $scope.itemType = 'item-canvas';
                            break;
                    }
                    $scope.getCompanyAssessment = function (id) {
                        if ($scope.itemType != 'question') {
                            crmContactService.getCompanyAssessment({
                                assessment_id: $rootScope.companyParames.assessmentId,
                                form_key: $rootScope.companyParames.form_key,
                                type: $scope.itemType,
                                crm_company_id: $rootScope.id_crm_company,
                                company_id: $rootScope.id_company,
                            }).then(function (result) {
                                if (result.status) {
                                    $scope.companyAssessmentsItems = result.data[0].item;
                                    $scope.tag = result.data[0].assessment_name;
                                    $scope.companyAssessmentsId = result.data[0].id_company_assessment;
                                    if (id) {
                                        setTimeout(function () {
                                            var elm = $('#' + id).find('h1');
                                            elm.trigger("click");
                                        }, 500)
                                    }
                                }
                                $scope.getKnowledgeDetails();
                            });
                        }
                    };
                    $scope.getCompanyAssessment();
                    $scope.addComment = function (comment, id_assessment_item) {
                        var $params = {
                            //company_assessment_id:$scope.companyAssessmentsId,
                            crm_company_id: $rootScope.id_crm_company,
                            assessment_item_id: id_assessment_item,
                            form_key: $rootScope.companyParames.form_key,
                            'assessment_id': $rootScope.companyParames.assessmentId,
                            step_title: comment,
                            created_by: $rootScope.userId
                        };
                        crmContactService.addCompanyAssessmentItemStep($params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.getCompanyAssessment(id_assessment_item);
                            } else {
                                $rootScope.toast('Error', result.error, 'error', $scope.sector);
                            }
                        });
                    };
                    $scope.updateCommentFun = function (comment, id, showInputShow) {
                        var $params = {
                            id_company_assessment_item_step: id,
                            step_title: comment
                        };
                        crmContactService.updateAssessmentStep($params).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.showInputShow = false;
                            } else {
                                $rootScope.toast('Error', result.error, 'error', $scope.sector);
                            }
                        });
                    }
                    $scope.deleteComment = function (comment_id, id_assessment_item) {
                        var r = confirm("Do you want to continue?");
                        if (r == false) {
                            return false;
                        }
                        crmContactService.deleteCompanyAssessmentItemStep({id_company_assessment_item_step: comment_id}).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.getCompanyAssessment(id_assessment_item);
                            } else {
                                $rootScope.toast('Error', result.error, 'error', $scope.sector);
                            }
                        });
                    }
                    break;
                default:
            }

        }
        $scope.getCountryList = function(data){
            masterService.country.get(data).then(function ($result) {
                $scope.ISDCodes = $result.data;
            });
        }
        $scope.submitForm = function ($inputData, $form_data, form) {

            $scope.submitStatus = true;
            if (form.$valid) {
                $inputData.company_id = $rootScope.id_company;
                $inputData.form_id = $rootScope.companyParames.formId;
                $inputData.user_id = $rootScope.userId;
                if ($rootScope.id_crm_company != undefined) {
                    $inputData.crm_company_id = $rootScope.id_crm_company;
                }
                if($inputData.company_phone_number1){
                    $inputData.company_phone_number = $.trim($inputData.company_phone_number_isd)+' '+ $.trim($inputData.company_phone_number1);
                }
                if($inputData.company_alternative_number1){
                    $inputData.company_alternative_number = $.trim($inputData.company_alternative_phone_number_isd)+' '+$.trim($inputData.company_alternative_number1);
                }
                if($rootScope.companyParames.form_key=='company_information'){
                    if($inputData.company_license_is_bear=='yes'){
                        delete $inputData.company_lincense_not_bear_comments;
                    }
                    if($inputData.company_valid_license=='yes'){
                        delete $inputData.company_invalid_license_comments;
                    }
                }
                crmCompanyService.saveCompany($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.actualObj = angular.copy($scope.company);
                        $scope.getCompanyInfo();
                        $rootScope.toast('Success', result.message);
                        if ($rootScope.crm_company_id == undefined) {
                            $rootScope.crm_company_id = result.data.crm_company_id;
                        }
                        $scope.percentage = Math.ceil(result.data.percentage);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
        $scope.init();
        $scope.parentSectorsList = [];
        $scope.sectorList = function (selectedId) {
            masterService.sector.getAllSectors().then(function (result) {
                $scope.parentSectorsList = result.data.data;
                //$scope.company.selectedId = selectedId;
            });
        }
        $scope.getSubSector = function (parentName) {
            masterService.sector.getSubSector({'parent_sector_name': parentName}).then(function (result) {
                $scope.subSectorList = result.data;
            });
        };

        $scope.Countries = [];
        masterService.country.get().then(function ($result) {
            $scope.Countries = $result.data;
        });

        $scope.getIntelligenceDetails = function () {
            crmCompanyService.getIntelligenceData({
                'company_id': $rootScope.id_company,
                'sector_id': $scope.companyInfo.sector_id,
                'sub_sector_id': $scope.companyInfo.sub_sector_id,
                type: 'crm_company',
                'crm_company_id': $rootScope.id_crm_company
            }).then(function (result) {
                $scope.knowledgeDetails = result.data.list;
                $scope.peopleDetails = result.data.user_list;
            });
        };
        $scope.tag = '';
        $scope.getKnowledgeDetails = function () {
            crmCompanyService.getKnowledgeData({
                'company_id': $rootScope.id_company,
                'type': 'crm_company',
                'tags': $rootScope.companyParames.tag,
                'crm_company_id': $rootScope.id_crm_company
            }).then(function (result) {
                $scope.knowledgeData = result.data;
            });
        };
        $scope.getKnowledgeDetails();
        $scope.modalOpen = function (selectedItem) {
            $scope.btnDisabled = false;
            if (selectedItem != undefined) {
                crmCompanyService.updateViewCount({'id_knowledge_document': selectedItem.id_knowledge_document}).then(function (result) {
                    if (result.status) {
                    } else {
                    }
                })
            }
        };
        $scope.gotoElement = function (eId) {
            eId = underscoreaddFilter(eId);
            if (eId == '') return false;
            var elm = $('body').find('#' + eId);
            if (elm.length) {
                $location.hash();
                anchorSmoothScroll.scrollTo(eId);
                /*if(elm.find().hasClass('add-but')){*/
                elm.find('.add-but').slideToggle();
                /*}*/

            }
        };
        $scope.saveBtnShow=function(){
            if(($scope.companyInfo.created_by == $rootScope.userId) || ($rootScope.loggedUserId && $rootScope.loggedUserId == $scope.companyInfo.created_by)){
                return true
            }else{
                return false;
            }
        };
    })

    .controller('RatiosCtrl', function ($scope, $rootScope, crmCompanyService, $uibModal, $q) {
        console.log('RatiosCtrl');

        var data = {};
        data.company_id = $rootScope.id_company;
        data.form_id = $rootScope.companyParames.formId;
        data.user_id = $rootScope.userId;
        $scope.lastTenYear = [];

        $scope.lastTenYearFun = function () {
            var d = new Date();
            var data = [];
            for (var i = 0; i <= 9; i += 1) {
                $scope.lastTenYear.push({'label': d.getFullYear() - i, 'value': d.getFullYear() - i});
            }
        };
        $scope.financialInformation = [];
        $scope.ratiosList = [];
        $scope.getCompanyFinancialInformation = function () {
            crmCompanyService.companyRatios.financialInformation({'crm_company_id': $rootScope.currentCompanyViewId,'form_key':$rootScope.companyParames.form_key}).then(function (result) {
                /*$scope.financialInformation = result.data;*/
                $scope.ratiosList = result.data;
                console.log('',$scope.ratiosList);
                //$scope.getFirstRatioDetails();
            });
        }
        $scope.getCompanyFinancialInformation();
console.log('ratiosList : ',$scope.ratiosList);
        $scope.getCompanyRatios = function () {
            crmCompanyService.companyRatios.list({'crm_company_id': $rootScope.currentCompanyViewId}).then(function (result) {
                $scope.ratiosList = result.data;
                $scope.getFirstRatioDetails();
               /* angular.forEach($scope.ratios, function (value, key) {
                    if (value.hasOwnProperty('net_profit_margin_trend')) {
                        value.net_profit_margin_trend = JSON.parse(value.net_profit_margin_trend);
                    }
                    if (value.hasOwnProperty('net_profit_trend')) {
                        value.net_profit_trend = JSON.parse(value.net_profit_trend);
                    }
                    if (value.hasOwnProperty('dscr_trend')) {
                        value.dscr_trend = JSON.parse(value.dscr_trend);
                    }
                    if (value.hasOwnProperty('equity_ratio_trend')) {
                        value.equity_ratio_trend = JSON.parse(value.equity_ratio_trend);
                    }
                });*/
            });
        }
        //$scope.getCompanyRatios();


        $scope.getRatioDetails = function(id){
           return crmCompanyService.companyRatios.get({
                'crm_company_id': $rootScope.currentCompanyViewId,
                'id_crm_company_ratio': id
            }).then(function (result) {
                return result.data;
            });

        }
        $scope.firstRatio = {};
        $scope.getFirstRatioDetails =function(){
            if($scope.ratiosList.length>0){
                $q.when($scope.getRatioDetails($scope.ratiosList[0].id_crm_company_ratio)).then(function(result){
                    $scope.firstRatio = result;
                })
            }
        };

        $scope.removeCompanyRatio = function (row) {
            var r = confirm("Do you want to continue?");
            if (r == true) {
                crmCompanyService.companyRatios.delete({'id_crm_company_ratio': row.id_crm_company_ratio}).then(function (result) {
                    $scope.getCompanyRatios();
                });
            }
        }


        $scope.addCompanyRatio = function (selectYear, row, isEditable) {
            $scope.ratioData = {'selectYear': selectYear, 'row': row, 'isEditable': isEditable};
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'ratios-add.html',
                resolve: {
                    item: function () {
                        if ($scope.ratioData) {
                            return $scope.ratioData;
                        }
                    },
                    ratio:function(){
                        return $scope.getRatioDetails(row.id_crm_company_ratio);
                    }
                },
                controller: function ($uibModalInstance, $scope, item, $state,ratio) {
                    $scope.isEditable = item.isEditable;
                    $scope.update = false;
                    $scope.title = 'Add';
                    $scope.ratio = ratio;
                    $scope.ratio.crm_company_id = $rootScope.currentCompanyViewId;
                    $scope.ratio.created_by = $rootScope.userId;
                    if(!$scope.ratio.id_crm_company_ratio)
                        $scope.ratio.year = item.selectYear;
                    $scope.ratio.form_id = $rootScope.companyParames.formId,

                    $scope.formData = {
                        'crm_company_id': $rootScope.currentCompanyViewId,
                        'created_by': $rootScope.userId,
                        'trend_start_year': item.selectYear,
                        'equity_ratio': '',
                        'eadb_exposure': '',
                        'net_profit_margin_trend': [],
                        'net_profit_trend': [],
                        'dscr_trend': [],
                        'equity_ratio_trend': [],
                        'irr_ration': '',
                        'err_ration': '',
                        'security_cover': '',
                        'ratio_comments': ''
                    };
                    $scope.trendYears = [];
                    for (var i = 0; i < ((parseInt(item.selectYear) + 10) - parseInt(item.selectYear)); i++) {

                        $scope.trendYears.push({
                            'year': parseInt(item.selectYear) + i,
                            'value': '',
                            'name': (item.selectYear + i) + '_name'
                        });
                        $scope.formData.net_profit_margin_trend.push({
                            'year': parseInt(item.selectYear) + i,
                            'value': '',
                            'name': (item.selectYear + i) + '_name'
                        });
                        $scope.formData.net_profit_trend.push({
                            'year': parseInt(item.selectYear) + i,
                            'value': '',
                            'name': (item.selectYear + i) + '_name'
                        });
                        $scope.formData.dscr_trend.push({
                            'year': parseInt(item.selectYear) + i,
                            'value': '',
                            'name': (item.selectYear + i) + '_name'
                        });
                        $scope.formData.equity_ratio_trend.push({
                            'year': parseInt(item.selectYear) + i,
                            'value': '',
                            'name': (item.selectYear + i) + '_name'
                        });
                    }
                    $scope.setTrendValue = function(trendvalue){
                        if(trendvalue){
                            return trendvalue;
                        }else{
                            return angular.copy($scope.trendYears);
                        }
                    }
                    if (item.row) {
                        $scope.formData = item.row;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.saveRatio = function (form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {

                                crmCompanyService.companyRatios.post($scope.ratio).then(function (result) {
                                    if (result.status) {
                                        $rootScope.toast('Success', result.message);
                                        $uibModalInstance.dismiss('cancel');
                                    } else {
                                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                    }
                                });
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },

            });
            modalInstance.result.then(function ($data) {
            }, function () {
                $scope.getCompanyRatios();
            });
        }
    })

    .controller('CompanyPriviewCtrl', function ($rootScope, $scope, $state, crmCompanyService, $stateParams, decodeFilter, anchorSmoothScroll, $location, underscoreaddFilter) {
        $scope.gotoElement = function (eId) {
            eId = underscoreaddFilter(eId);
            if (eId == '') return false;
            var elm = $('body').find('#' + eId);
            if (elm.length) {
                $location.hash();
                anchorSmoothScroll.scrollTo(eId);
            }
        };
        $rootScope.currentCompanyViewId = decodeFilter($stateParams.id);
        $scope.finacialInfo = {};
        $scope.getFinancialData = function (form_key) {
            crmCompanyService.getFinancialData({
                form_key: form_key,
                crm_company_id: $rootScope.currentCompanyViewId
            }).then(function (result) {
                $scope.finacialInfo[form_key] = result.data.data;
            });
        };
        $scope.customer = {};
        $scope.competitor = {};
        $scope.supplier = {};
        $scope.overheadcost = {};
        $scope.salesdata={};
        $scope.getCustomers = function(){
            crmCompanyService.customers.get({'crm_company_id':$rootScope.currentCompanyViewId}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.customer = result.data.master[0];
                }else
                    $scope.customer = {};
            })
        };
        $scope.getCompetitors = function(){
            crmCompanyService.competitors.get({'crm_company_id':$rootScope.currentCompanyViewId}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.competitor = result.data.master[0];
                }else
                    $scope.competitor = {};
            })
        };
        $scope.getSuppliers = function(){
            crmCompanyService.suppliers.get({'crm_company_id':$rootScope.currentCompanyViewId}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.supplier = result.data.master[0];
                }else
                    $scope.supplier = {};
            })
        };
        $scope.getOverHead = function(){
            return crmCompanyService.overheadcost.get({'crm_company_id': $rootScope.currentCompanyViewId}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.overheadcost = result.data.master[0];
                }
            });
        };
        $scope.getSalesData = function(){
            return crmCompanyService.SalesDirectcostEstimate.get({'crm_company_id': $rootScope.currentCompanyViewId}).then(function ($result) {
                $scope.salesdata = {};
                if($result.data.master.length>0)
                    $scope.salesdata = $result.data.master[0];
            });
        };
        crmCompanyService.getcompanyPriviewDetails({
            'company_id': $rootScope.id_company,
            'crm_company_id': decodeFilter($stateParams.id)
        }).then(function (result) {
            $scope.companyBusinessAssessment = result.data;
        });
        crmCompanyService.getCompanyInternalRatingDetails({
            'company_id': $rootScope.id_company,
            'crm_company_id': decodeFilter($stateParams.id)
        }).then(function (result) {
            $scope.internalRatingData = result.data;
        });
        crmCompanyService.companyDetails(decodeFilter($stateParams.id), 2).then(function (result) {
            $scope.businessKeyData = result.data.forms;
            $rootScope.business_name = result.data.details[0].company_name;
            $rootScope.sector = result.data.details[0].sector;
            $rootScope.subsector = result.data.details[0].sub_sector;
            $rootScope.companyDescription = result.data.details[0].company_description;
            $rootScope.grade = result.data.details[0].grade;
            $rootScope.rating = result.data.details[0].rating;
        });
        /*  crmCompanyService.companyFinancialInformation({'crm_company_id': decodeFilter($stateParams.id)}).then(function (result) {
         $scope.companyFinancialInformation = result.data;
         });
         */

        $scope.jsonConvert = function (data) {
            if (data) {
                return JSON.parse(data);
            }
        }
        $scope.multipleCheckbox = function (data) {

            if (data) {
                var _array = [];
                // var obj =  JSON.parse(data);
                angular.forEach(data, function (value, key) {
                    if (value) {
                        if (key == 'company_sme_business') {
                            _array.push('SME Business');
                        }
                        if (key == 'company_incorporate_business') {
                            _array.push('Corporate Business');
                        }
                        if (key == 'company_construction') {
                            _array.push('Construction');
                        }
                        if (key == 'company_acquisition') {
                            _array.push('Acquisition');
                        }

                    }
                })
                return _array.toString();
            }
        }
    })
    .controller('riskAssessmentComponentsCtrl', function ($scope, $rootScope, crmProjectService) {
        $scope.init = function () {
            $scope.risk_items = {};
            crmProjectService.riskAssessment.projectRiskFactorsGet({
                crm_project_id: $rootScope.currentProjectViewId
            }).then(function (result) {
                $scope.risk_items = result.data.risk_items;
            });
        }
    })
    .controller('approvalCommentsComponentCtrl', function ($scope, $rootScope, crmProjectService) {
        $scope.init = function () {
            $scope.approvalComments = {};
            crmProjectService.approvalComments({
                crm_project_id: $rootScope.currentProjectViewId
            }).then(function (result) {
                $scope.approvalComments = result.data;
            });
        }
    })
    .controller('previousExperienceCtrl', function ($scope, $rootScope, $uibModal) {
        $scope.removeItem = function removeItem(row) {
            var index = $scope.contact.previous_experience_institution.indexOf(row);
            if (index !== -1) {
                $scope.contact.previous_experience_institution.splice(index, 1);
            }
        }
        $scope.addPreviousExperience = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/contact/forms/previous-experience-fields.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.previousExperience = {
                        "current_loan_amount_distribution": "",
                        "client_since": "",
                        "current_outstanding_loan_amount": "",
                        "current_loan_payments_on_time": "",
                        "total_previous_loans": "",
                        "previous_loan_payments_on_time": ""
                    };
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.previousExperience = item;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $uibModalInstance.close($scope.previousExperience);
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                if (!$scope.contact.previous_experience_institution) {
                    $scope.contact.previous_experience_institution = [];
                }
                $scope.contact.previous_experience_institution.push($data);
            }, function () {
            });
        }
        $scope.viewPreviousExperience = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/contact/forms/view-previous-experience-form.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.previousExperience = {};
                    $scope.update = false;
                    if (item) {
                        $scope.previousExperience = item;
                        $scope.update = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
        };
    })
    .controller('companyOverheadCtrl', function ($scope, $rootScope, $uibModal, crmProjectService, crmCompanyService, $q) {
        $scope.company.company_overhead_fields = {};
        $scope.currecnyList = {};
        $scope.submitStatus = true;
        $scope.company.company_overhead_fields.child_data = [];
        $scope.getOverHead = function(){
            return crmCompanyService.overheadcost.get({'crm_company_id': $rootScope.currentCompanyViewId}).then(function (result) {
                var data = {};
                if(result.data.master.length>0){
                    data = result.data.master[0];
                }
                return data;
            });
        };
        $scope.getCurrency = function(){
            return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                var data = {};
                for(var a in $result.data){
                    if($result.data[a]['id_company_currency']){
                        data[a] = $result.data[a];
                    }
                }
                return data;
            });
        };
        /*Async call*/
        $scope.getData = function(){
            $q.all([
                $scope.getOverHead(),
                $scope.getCurrency()
            ]).then(function(result){
                $scope.company.company_overhead_fields = result[0];
                $scope.currecnyList = result[1];

                if($scope.company.company_overhead_fields.currency_id){
                    /*Select the value which is already selected*/
                    angular.forEach($scope.currecnyList, function (value, index) {
                        if(value.currency_id==$scope.company.company_overhead_fields.currency_id){
                            $scope.company.company_overhead_fields.currency=value;
                        }
                    });
                } else {
                    /*default selection*/
                    $scope.company.company_overhead_fields.currency=$scope.currecnyList[0];
                }

            });
        }
        $scope.getData();

        $scope.removeItem = function removeItem(row) {
            row.status=0;
        };
        $scope.getOverHead();
        $scope.addCustomerCategory = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-customer-form.html',
                controller: function ($uibModalInstance, $scope, item, masterService, $q) {
                    var orginalData = {};
                    $scope.dropDownItems = [];
                    $scope.masterDropDownList = function (params) {
                        masterService.masterList(params).then(function (result) {
                            if (result.status)
                                $scope.dropDownItems[params.master_key] = result.data;
                        })
                    }
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.customerfield = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        masterService.masterList({'master_key':'company_overhead_cost_estimate_category'}).then(function (result) {
                            if (result.status){
                                $scope.categoryList =  result.data;
                                angular.forEach($scope.categoryList, function (value, $index) {
                                    if(value.id==$scope.customerfield.category_id){
                                        $scope.customerfield.category=value;
                                    }
                                });
                            }
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function ($Data, form) {
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        $scope.submitStatus = false;
                        if(form.$valid){
                            $Data.status='1';
                            if (!$scope.update) {
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                if($data)
                    $scope.company.company_overhead_fields['child_data'].push($data);
            }, function () {
            });

        };
        $scope.submit = function ($inputData) {
            var params = $scope.company.company_overhead_fields;
            $scope.submitStatus = true;
            params.company_id = $rootScope.id_company;
            params.crm_company_id = $rootScope.currentCompanyViewId;
            params.form_id = $rootScope.companyParames.formId;
            params.form_key = $rootScope.companyParames.form_key;
            params.user_id = $rootScope.userId;
            /*console.log(params);*/
            crmCompanyService.overheadcost.post(params).then(function (result) {
                if (result.status) {
                    $rootScope.isFormChanged=false;
                    $scope.getData();
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        };
    })
    .controller('companyOwnershipCtrl', function ($scope, $rootScope, $uibModal, crmProjectService) {
        $scope.removeItem = function removeItem(row) {
            var index = $scope.company.company_ownership_details.indexOf(row);
            if (index !== -1) {
                $scope.company.company_ownership_details.splice(index, 1);
            }
        }
        $scope.addOwnerShip = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/ownership-form-fields.html',
                controller: function ($uibModalInstance, $scope, item, currency, $q) {
                    $scope.ownership = {
                        shareholder_name: '',
                        no_of_shares: '',
                        ownership_percentage: '',
                        value_of_shares: '',
                        currency: ''
                    };
                    $scope.currecnyList = currency;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.ownership = item;
                        // angular.copy(item,$scope.ownership);
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.validOwnerShip = function (val) {
                        var ownershipSum = 0;
                        angular.forEach($scope.company.company_ownership_details, function (ownership, $index) {
                            ownershipSum = ownershipSum + parseFloat(ownership.ownership_percentage);
                        });
                        if (parseFloat(ownershipSum) + parseFloat(val) > 100) {
                            $rootScope.toast('Error', ['All Ownership % less than or equal to 100%'], 'error');
                            return false;
                        } else {
                            return true;
                        }
                    }
                    $scope.cancel = function () {
                        var status = $scope.validOwnerShip(0);
                        if (status) {
                            $uibModalInstance.dismiss('cancel');
                        }
                    };
                    $scope.save = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var status = $scope.validOwnerShip($scope.ownership.ownership_percentage);
                            if (status) {
                                $uibModalInstance.close($scope.ownership);
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    currency: function () {
                        return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                            return $result.data;
                        });
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                $scope.company.company_ownership_details.push($data);
            }, function () {
            });
        }
        $scope.viewOwnership = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/view-ownership-form.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.ownership = {};
                    $scope.update = false;
                    if (item) {
                        $scope.ownership = item;
                        $scope.update = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
        };
    })
    .controller('companyFamilyCtrl', function ($scope, $rootScope, $uibModal) {
        $scope.removeItem = function removeItem(row) {
            var index = $scope.company.family_projected_development.indexOf(row);
            if (index !== -1) {
                $scope.company.family_projected_development.splice(index, 1);
            }
        }
        $scope.addFamilyBussinessPoint = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/family-form-points.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.familyDescription = {description: ''};
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        $scope.familyDescription = item;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        $scope.submitStatus = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $uibModalInstance.close($scope.familyDescription);
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                $scope.company.family_projected_development.push($data);
            }, function () {
            });
        }
    })
    .controller('socialNetworkCtrl', function ($scope, $rootScope, $uibModal) {
        $scope.removeItem = function removeItem(row) {
            var index = $scope.company.company_social_networks.indexOf(row);
            if (index !== -1) {
                $scope.company.company_social_networks.splice(index, 1);
            }
        }
        $scope.addNetwork = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/network-form-fields.html',
                controller: function ($uibModalInstance, $scope, item, masterService) {
                    $scope.networkDetails = {'name': '', 'details': ''};
                    $scope.update = false;
                    $scope.title = 'Add';
                    masterService.social.get().then(function (result) {
                        $scope.socialList = result.data.data;
                    });
                    if (item) {
                        $scope.networkDetails = item;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        $scope.submitStatus = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $uibModalInstance.close($scope.networkDetails);
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                if (!$scope.company.company_social_networks)
                    $scope.company.company_social_networks = [];

                $scope.company.company_social_networks.push($data);
            }, function () {
            });
        }
        $scope.viewNetwork = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/view-network-form.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.networkDetails = {};
                    $scope.update = false;
                    if (item) {
                        $scope.networkDetails = item;
                        $scope.update = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
        };
    })
    .controller('contactTypeCtrl', function ($scope, $rootScope, $uibModal) {
        $scope.removeItem = function removeItem(row) {
            var index = $scope.company.business_contact_type.indexOf(row);
            if (index !== -1) {
                $scope.company.business_contact_type.splice(index, 1);
            }
        }
        $scope.addNetwork = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/contact-type-form-fields.html',
                controller: function ($uibModalInstance, $scope, item, masterService) {
                    $scope.business_contact_type = {'name': '', 'details': ''};
                    $scope.update = false;
                    $scope.title = 'Add';
                    masterService.contact.get().then(function (result) {
                        $scope.contactList = result.data.data;
                    });
                    if (item) {
                        $scope.business_contact_type = item;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        $scope.submitStatus = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function ($Data, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $uibModalInstance.close($scope.business_contact_type);
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function ($data) {
                $scope.company.business_contact_type.push($data);
            }, function () {
            });
        }
        $scope.viewNetwork = function (row) {
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/company/forms/view-network-form.html',
                controller: function ($uibModalInstance, $scope, item) {
                    $scope.networkDetails = {};
                    $scope.update = false;
                    if (item) {
                        $scope.networkDetails = item;
                        $scope.update = true;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    }
                }
            });
        };
    })
    .controller('companyAssessmentQuestions', function ($scope, $rootScope, crmCompanyService, crmContactService, Upload, $timeout) {
        $scope.categoryQuestionList = [];
        $scope.QsaveBtnShow = false;
        $scope.getAssessmentQuestion = function () {
            //'assessment_id':$rootScope.companyParames.form_key,
            //'assessment_key':$rootScope.companyParames.assessment_key,
            crmContactService.getCompanyAssessment({
                'form_key': $rootScope.companyParames.form_key,
                'type': 'question',
                'crm_company_id': $rootScope.currentCompanyViewId,
                'company_id': $rootScope.id_company
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                    console.log('$scope.categoryQuestionList',$scope.categoryQuestionList);
                    angular.forEach($scope.categoryQuestionList,function(item,key){
                        if(item.question.length>0){
                            $scope.QsaveBtnShow = true;
                        }
                    })
                }
            });
        }
        $scope.getAssessmentQuestion();
        $scope.saveAnswer = function (category) {
            bulkAnswers = [];
            angular.forEach(category, function (category) {
                angular.forEach(category.question, function (question, $index) {
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentCompanyViewId;
                    $obj.crm_company_id = $rootScope.currentCompanyViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.assessment_id = $rootScope.companyParames.assessmentId;
                    $obj.form_key = $rootScope.companyParames.form_key;
                    if (question.answer.id_assessment_answer != undefined) {
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }
                    $rootScope.loaderOverlay = false;
                    if (question.question_type == 'file') {
                        if (question.answer.file.name != undefined) {
                            Upload.upload({
                                url: API_URL + 'Company/assessmentAnswer',
                                data: {
                                    file: question.answer.file,
                                    'answer': $obj
                                }
                            }).then(function (result) {
                                if (result.status) {
                                    if ($index == category.question.length - 1) {
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            });
                        }
                    } else {
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function (result) {
                            if (result.status) {
                                $rootScope.isFormChanged = false;
                                $rootScope.toast('Success', result.message);
                                if ($index == category.question.length - 1) {
                                    $scope.getAssessmentQuestion();
                                }
                            } else {
                                $rootScope.toast('Error', result.error, 'error');
                            }
                        })
                    }
                })
            })
        }
    })
    .controller('projectAssessmentQuestionsCheckList', function ($scope, $rootScope, crmProjectService, crmCompanyService, Upload, $q, $timeout) {
        $scope.categoryQuestionList = [];
        $scope.getAssessmentQuestion = function () {
            crmProjectService.projectTerm.projectTremesAndConditions({
                product_term_key: $rootScope.projectParames.sectionName,
                product_term_id: $rootScope.projectParames.formId,
                company_id: $rootScope.id_company,
                crm_project_id: $rootScope.currentProjectViewId,
                product_term_type: $rootScope.projectParames.product_term_type
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                }
            });
        }
        $scope.getAssessmentQuestion();
        $scope.saveAnswerRestrictions = function (category) {
            bulkAnswers = [];
            angular.forEach(category, function (category) {
                angular.forEach(category.question, function (question, $index) {
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentProjectViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                    $obj.crm_project_id = $rootScope.currentProjectViewId;
                    if (question.answer.id_assessment_answer != undefined) {
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }
                    $rootScope.loaderOverlay = false;
                    if (question.question_type == 'file') {
                        if (question.answer.file.name != undefined) {
                            Upload.upload({
                                url: API_URL + 'Company/assessmentAnswer',
                                data: {
                                    file: question.answer.file,
                                    'answer': $obj
                                }
                            }).then(function (result) {
                                if (result.status) {
                                    if ($index == category.question.length - 1) {
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            });
                        }
                    } else {
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                if ($index == category.question.length - 1) {
                                    $scope.getAssessmentQuestion();
                                }
                            } else {
                                $rootScope.toast('Error', result.error, 'error');
                            }
                        })
                    }
                })
            })
        }
        $scope.saveAnswer = function (questions) {
            var bulkAnswers = [];
            angular.forEach(questions, function (question, $index) {
                var $obj = {};
                $obj.assessment_answer = question.answer.assessment_answer;
                $obj.assessment_question_id = question.id_assessment_question;
                $obj.crm_reference_id = $rootScope.currentProjectViewId;
                $obj.created_by = $rootScope.userId;
                $obj.company_id = $rootScope.id_company;
                $obj.comment = question.answer.comment;
                $obj.type = 'media';
                $obj.crm_project_id = $rootScope.currentProjectViewId;
                $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                if (question.answer.id_assessment_answer != undefined) {
                    $obj.id_assessment_answer = question.answer.id_assessment_answer;
                }
                $rootScope.loaderOverlay = false;
                if (question.answer.file.name != undefined) {
                    Upload.upload({
                        url: API_URL + 'Company/assessmentAnswer',
                        data: {
                            file: question.answer.file,
                            'answer': $obj
                        }
                    }).then(function (result) {
                        if (result.status) {
                            $rootScope.toast('Success', result.message);
                            if ($index == questions.length - 1) {
                                $scope.getAssessmentQuestion();
                            }
                        } else {
                        }
                    });
                } else {
                    crmCompanyService.assessmentQuestion.answer.post($obj).then(function (result) {
                        if (result.status) {
                            $rootScope.toast('Success', result.message);
                            if ($index == questions.length - 1) {
                                $scope.getAssessmentQuestion();
                            }
                        } else {
                        }
                    })
                }
            })
        }
    })
    .controller('companyInternalRatingCtrl', function ($scope, $rootScope, crmProjectService) {
        console.log('companyInternalRatingCtrl');
        $scope.riskCategory = [];
        crmProjectService.riskAssessment.get({
            'company_id': $rootScope.id_company,
            'crm_company_id': $rootScope.currentCompanyViewId,
            'type': 'company'
        }).then(function (result) {
            if (result.status) {
                $scope.riskCategory = result.data;
            } else {
                $rootScope.toast('Error', result.error, 'error');
            }
        });
        $scope.convertToInt = function (value) {
            return parseInt(value);
        };
        $scope.subAttrFactor = 0;
        $scope.dropdownSelected = function (attribute) {
            angular.forEach(attribute.items, function (item) {
                if (item.selected == 1) {
                    attribute.selectedItem = item;
                }
            })
        }
        $scope.saveAttribute = function (data) {
            var param = {};
            param.company_id = $rootScope.id_company;
            param.crm_company_id = $rootScope.currentCompanyViewId;
            param.user_id = $rootScope.userId;
            param.form_key = 'internal_rating';
            param.data = [];
            angular.forEach(data, function (item) {
                if (item.selectedItem != undefined) {
                    var obj = {};
                    obj.grade = item.grade;
                    obj.id_risk_category = item.id_risk_category;
                    obj.id_risk_category_item = item.selectedItem.id_risk_category_item;
                    obj.risk_category_id = item.id_risk_category;
                    param.data.push(obj);
                }
            })
            crmProjectService.riskAssessment.post(param).then(function (result) {
                if (result.status) {
                    $scope.getCompanyInfo();
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', result.error);
                }
            })
        }
    })
    .controller('mapCompanyCtrl', function (NgMap, $scope, $uibModal, $timeout) {
        $scope.$watch('company', function (newVal, oldVal) {
            if (newVal.company_latitude && newVal.company_langitude) {
                $scope.mapAddress = "" + newVal.company_latitude + "," + newVal.company_langitude;
                $scope.latitude = $scope.company.company_latitude;
                $scope.langitude = $scope.company.company_langitude;
            } else {
                $scope.mapAddress = newVal.company_city + "," + newVal.company_country;
            }
            if (oldVal.company_country && newVal.company_country != oldVal.company_country) {
                $scope.mapAddress = newVal.company_country;
            }
        }, true);
        $scope.openMapModel = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/map-modal.html',
                controller: function ($scope, $uibModalInstance, $timeout) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.showMap = false;
                    $timeout(function () {
                        $scope.showMap = true;
                    })
                    $scope.getMap = function (object) {
                        var marker = this;
                        $scope.company.company_latitude = marker.getPosition().lat();
                        $scope.company.company_langitude = marker.getPosition().lng();
                        $scope.latitude = $scope.company.company_latitude;
                        $scope.langitude = $scope.company.company_langitude;
                        NgMap.getMap().then(function (map) {
                            map.getCenter().lat();
                            map.getCenter().lng();
                        });
                    }
                }
            });
        }


    })
    .controller('customerCtrl', function ($scope, $uibModal, $timeout, $rootScope, crmCompanyService, crmProjectService, masterService) {
        $scope.customer = {};
        $scope.years = [];
        $scope.getCustomers = function(){
            crmCompanyService.customers.get({'crm_company_id':$rootScope.id_crm_company}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.customer = result.data.master[0];
                    var d = new Date();
                    for(var i=0;i<30;i++) {
                        var obj = {};

                        obj.value = (d.getFullYear() - i)+'';
                        obj.id = obj.value;
                        $scope.years.push(obj);
                    }
                }else
                    $scope.customer = {};
            })
        };
        $scope.getCustomers();
        $scope.addCustomers = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-customers.html',
                controller: function ($uibModalInstance, $scope, item, currency, ISDCodes, $q) {
                    $scope.currecnyList = [];
                    var orginalData = {};
                    $scope.submitStatus = false;
                    for(var a in currency){
                        if(currency[a]['id_company_currency']){
                            $scope.currecnyList[a] = currency[a];
                        }
                    }
                    $scope.ISDCodes = ISDCodes;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.customers = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        if(form.$valid){
                            $Data.status = '1';
                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    currency: function () {
                        return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                            return $result.data;
                        });
                    },
                    ISDCodes: masterService.country.get({'company_id': $rootScope.id_company}).then(function (result) {
                        return result.data;
                    })
                }
            });
            modalInstance.result.then(function ($data) {
                //console.log('$data',$data);
                if($data){
                    $scope.customer['child_data'].push($data);
                }
            }, function () {
            });
        };
        $scope.removeCustomers = function(row){
            row.status = '0';
        };
        $scope.submit = function ($inputData , form) {
            $scope.submitStatus = true;
            $inputData.company_id = $rootScope.id_company;
            $inputData.crm_company_id = $rootScope.currentCompanyViewId;
            $inputData.form_id = $rootScope.companyParames.formId;
            $inputData.form_key = $rootScope.companyParames.form_key;
            $inputData.user_id = $rootScope.userId;
            if (form.$valid) {
                crmCompanyService.customers.post($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.getCustomers();
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })
    .controller('competitorCtrl', function ($scope, $uibModal, $timeout, $rootScope, crmCompanyService, crmProjectService, masterService) {
        $scope.competitor = {};
        $scope.submitStatus = false;
        masterService.masterList({'master_key':'company_estimated_no_of_competitors'}).then(function (result) {
            if (result.status)
                $scope.estimated_no_of_competitorsList=result.data;
        });
        $scope.getCompetitors = function(){
            crmCompanyService.competitors.get({'crm_company_id':$rootScope.id_crm_company}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.competitor = result.data.master[0];
                }else
                    $scope.competitor = {};
            })
        };
        $scope.getCompetitors();
        $scope.addCompetitors = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-competitors.html',
                controller: function ($uibModalInstance, $scope, item, currency, ISDCodes, $q,price,brand,size) {
                    var orginalData = {};
                    $scope.currecnyList = [];
                    $scope.submitStatus = false;
                    for(var a in currency){
                        if(currency[a]['id_company_currency']){
                            $scope.currecnyList[a] = currency[a];
                        }
                    }
                    $scope.ISDCodes = ISDCodes;
                    $scope.price = price;
                    $scope.brand = brand;
                    $scope.size = size;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.competitors = item;
                        item.isEdit = true;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        angular.forEach($scope.price, function (value, $index) {
                            if(value.id==$scope.competitors.price){
                                $scope.competitors.priceobj=value;
                            }
                        });
                        angular.forEach($scope.brand, function (value, $index) {
                            if(value.id==$scope.competitors.brand){
                                $scope.competitors.brandobj=value;
                            }
                        });
                        angular.forEach($scope.size, function (value, $index) {
                            if(value.id==$scope.competitors.size){
                                $scope.competitors.sizeobj=value;
                            }
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        /*console.log($Data);*/
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        if(form.$valid){
                            $Data.status = '1';
                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    currency: function () {
                        return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                            return $result.data;
                        });
                    },
                    ISDCodes: masterService.country.get({'company_id': $rootScope.id_company}).then(function (result) {
                        return result.data;
                    }),
                    price: masterService.masterList({'master_key':'company_competitor_price'}).then(function (result) {
                    if (result.status)
                        return result.data;
                    }),
                    brand: masterService.masterList({'master_key':'company_competitor_brand'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    }),
                    size: masterService.masterList({'master_key':'company_competitor_size'}).then(function (result) {
                        if (result.status)
                            return result.data;
                    })
                }
            });
            modalInstance.result.then(function ($data) {
                if($data){
                    $scope.competitor['child_data'].push($data);
                }
            }, function () {
            });
        };
        $scope.removeCompetitors = function(row){
            row.status = '0';
        };
        $scope.submit = function ($inputData , form) {
            $scope.submitStatus = true;
            $inputData.company_id = $rootScope.id_company;
            $inputData.crm_company_id = $rootScope.currentCompanyViewId;
            $inputData.form_id = $rootScope.companyParames.formId;
            $inputData.form_key = $rootScope.companyParames.form_key;
            $inputData.user_id = $rootScope.userId;
            if (form.$valid) {
                $rootScope.isFormChanged = false;
                crmCompanyService.competitors.post($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.getCompetitors();
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })
    .controller('supplierCtrl', function ($scope, $uibModal, $timeout, $rootScope, crmCompanyService, crmProjectService, masterService) {
        $scope.supplier = {};
        $scope.getSuppliers = function(){
            crmCompanyService.suppliers.get({'crm_company_id':$rootScope.id_crm_company}).then(function (result) {
                if(result.data.master.length>0){
                    $scope.supplier = result.data.master[0];
                }else
                    $scope.supplier = {};
            })
        };
        $scope.getSuppliers();
        $scope.addSuppliers = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass:'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add-supplier.html',
                controller: function ($uibModalInstance, $scope, item, currency, ISDCodes, $q) {
                    var orginalData = {};
                    $scope.currecnyList = [];
                    $scope.submitStatus = false;
                    for(var a in currency){
                        if(currency[a]['id_company_currency']){
                            $scope.currecnyList[a] = currency[a];
                        }
                    }
                    $scope.ISDCodes = ISDCodes;
                    $scope.update = false;
                    $scope.title = 'Add';
                    if (item) {
                        angular.copy(item,orginalData);
                        $scope.suppliers = item;
                        $scope.submitStatus = false;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function (form, $Data) {
                        if(!angular.equals($Data, orginalData)){
                            $rootScope.isFormChanged=true;
                        }
                        if(form.$valid){
                            $Data.status = '1';
                            if(!$scope.update){
                                $uibModalInstance.close($Data);
                            } else {
                                $uibModalInstance.close();
                            }
                        }
                    }
                },
                resolve: {
                    item: function () {
                        if ($scope.selectedRow) {
                            return $scope.selectedRow;
                        }
                    },
                    currency: function () {
                        return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                            return $result.data;
                        });
                    },
                    ISDCodes: masterService.country.get({'company_id': $rootScope.id_company}).then(function (result) {
                        return result.data;
                    })
                }
            });
            modalInstance.result.then(function ($data) {
                if($data){
                    $scope.supplier['child_data'].push($data);
                }
            }, function () {
            });
        };
        $scope.removeSuppliers = function(row){
            row.status = '0';
        };
        $scope.submit = function ($inputData , form) {
            $scope.submitStatus = true;
            $inputData.company_id = $rootScope.id_company;
            $inputData.crm_company_id = $rootScope.currentCompanyViewId;
            $inputData.form_id = $rootScope.companyParames.formId;
            $inputData.form_key = $rootScope.companyParames.form_key;
            $inputData.user_id = $rootScope.userId;
            if (form.$valid) {
                crmCompanyService.suppliers.post($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.isFormChanged = false;
                        $scope.getSuppliers();
                        $rootScope.toast('Success', result.message);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })
    .controller('companySalesDirectcostEstimate', function ($scope, $uibModal, $timeout, $rootScope, $q, crmCompanyService, crmProjectService, masterService) {
    $scope.company.salesData = {};
    $scope.submitStatus = true;
    $scope.getSalesData = function(){
        return crmCompanyService.SalesDirectcostEstimate.get({'crm_company_id': $rootScope.id_crm_company}).then(function ($result) {
            var data = {};
            if($result.data.master.length>0)
                data = $result.data.master[0];
            return data;
        });
    };
    $scope.getCurrency = function(){
        return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
            var data = {};
            for(var a in $result.data){
                if($result.data[a]['id_company_currency']){
                    data[a] = $result.data[a];
                }
            }
            return data;
        });
    };
    $scope.getData = function(){
        $q.all([
            $scope.getSalesData(),
            $scope.getCurrency()
        ]).then(function(result){
            $scope.company.salesData = result[0];
            $scope.currecnyList = result[1];
            if($scope.company.salesData.currency_id){
                /*Select the value which is already selected*/
                angular.forEach($scope.currecnyList, function (value, index) {
                    if(value.currency_id==$scope.company.salesData.currency_id){
                        $scope.company.salesData.currency=value;
                    }
                });
            } else {
                /*default selection*/
                $scope.company.salesData.currency=$scope.currecnyList[0];
                $scope.company.salesData.currency_id=$scope.currecnyList[0].currency_id;
            }

            $scope.company.salesData.sales_estimates=$scope.company.salesData.sales_estimates;
        });
    }
    $scope.getData();
    $scope.submit = function ($inputData) {
        var data = $inputData;
        $scope.submitStatus = true;
        data.company_id = $rootScope.id_company;
        data.crm_company_id = $rootScope.currentCompanyViewId;
        data.form_id = $rootScope.companyParames.formId;
        data.form_key = $rootScope.companyParames.form_key;
        data.user_id = $rootScope.userId;
        crmCompanyService.SalesDirectcostEstimate.post(data).then(function (result) {
            if (result.status) {
                $rootScope.isFormChanged = false;
                $scope.getData();
                $rootScope.toast('Success', result.message);
            } else {
                $rootScope.toast('Error', result.error, 'error', $scope.sector);
            }
        })
    };
    $scope.addProduct = function(row){
        $scope.selectedRow = row;
        var modalInstance = $uibModal.open({
            animation: true,
            //windowClass:'my-class',
            backdrop: 'static',
            keyboard: false,
            scope: $scope,
            openedClass: 'right-panel-modal modal-open',
            templateUrl: 'add-product-form.html',
            controller: function ($uibModalInstance, $scope, item,  $q) {
                var orginalData = {};
                $scope.currecnyList = [];
                //$scope.submitStatus = false;

                $scope.update = false;
                $scope.title = 'Add';
                if (item) {
                    angular.copy(item,orginalData);
                    $scope.child_data = item;
                    item.isEdit = true;
                    $scope.submitStatus = true;
                    $scope.update = true;
                    $scope.title = 'Edit';
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.save = function (form, $Data) {
                    if(!angular.equals($Data, orginalData)){
                        $rootScope.isFormChanged=true;
                    }
                    $scope.submitStatus = false;
                    if(form.$valid){
                        $Data['status'] = '1';
                        if(!$scope.update){
                            $uibModalInstance.close($Data);
                        } else {
                            $uibModalInstance.close();
                        }
                    }
                }
            },
            resolve: {
                item: function () {
                    if ($scope.selectedRow) {
                        return $scope.selectedRow;
                    }
                }
            }
        });
        modalInstance.result.then(function ($data) {
            console.log($data);
            if($data){
                $scope.company.salesData['child_data'].push($data);
                console.log($scope.company.salesData);
            }
        }, function () {
        });
    };
    $scope.removeProduct = function(row){
        row.status = '0';
    };

})
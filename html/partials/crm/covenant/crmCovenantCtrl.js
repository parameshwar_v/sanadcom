/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('app')
    .controller('crmCovenantListCtrl',function($scope,$rootScope,decodeFilter,crmCovenantService,$uibModal,covenantService,$stateParams){
        $scope.collateralList=[];
        $scope.filter_type='all_collateral';
        console.log('$stateParams',$stateParams);


        $scope.getCovenanteList=function(stTable){
            stTable.company_id=$rootScope.id_company;
            if(!stTable)
                stTable=$scope.refTable;
            $scope.refTable=stTable;
            stTable.company_id=$rootScope.id_company;
            stTable.assigned_to=$rootScope.userId;
            stTable.created_by=$rootScope.userId;
            crmCovenantService.crm.get(stTable).then(function(result){
                $scope.covenanteList=result.data.data;
                $scope.all_covenant_count=result.data.total_records;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }
        $scope.preview = function (params) {
            $scope.params = {};
            angular.copy(params,$scope.params);
            console.log('params',$scope.params);
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                resolve: {
                    item: function () {
                        if ($scope.params.id_module_covenant) {
                            return covenantService.crmCovenant.get($scope.params).then(function ($result) {
                                if($result.status)
                                    return $result.data;
                            });
                        }
                    }
                },
                templateUrl: 'partials/crm/covenant/covenent-view.html',
                controller: function ($scope, $rootScope, $uibModalInstance, item) {
                    $scope.cTaskHistroy = {}
                    $scope.getCovenantHistory = function (tableState) {
                        covenantService.crmCovenant.taskHistory(tableState).then(function (result) {
                            $scope.refTableState = tableState;
                            $scope.cTaskHistroy[tableState.module_covenant_id] = result.data.data;
                            // tableState.pagination.
                            tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
                        })
                    }
                    if (item) {
                        $scope.covenantCrmList = item;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }

        if($stateParams.covenantId){
            var params ={};
            params.id_module_covenant = decodeFilter($stateParams.covenantId);
            $scope.preview(params);
        }

    })



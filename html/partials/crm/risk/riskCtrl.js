/**
 * Created by Abhilash on 10-08-2016.
 */
angular.module('app')
    .controller('riskCtrl', function ($scope, $rootScope, masterService) {
        $scope.parentSectorsList=[];
        $scope.sector=[];
        $scope.country_id=[];
        $scope.focus_area=[];
        $scope.filter='';
        $scope.sectorList=function(){
            masterService.sector.getAllSectors().then(function(result){
                $scope.parentSectorsList=result.data.data;
                //$scope.company.selectedId = selectedId;
            });
        }
        $scope.getSubSector=function(parentId){
            masterService.sector.getSubSector({'id_sector': parentId}).then(function(result){
                $scope.subSectorList=result.data;
            });
        };
        $scope.countryList=[];
        $scope.getCountries=function(){
            masterService.country.get().then(function(result){
                $scope.countryList=result.data;
            });
        }
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        };
    })
    .controller('riskListCtrl', function ($scope, $rootScope, $state, $uibModal, masterService, reportService, taskService, companyService, encodeFilter) {

        $scope.filterPanel = false;

        masterService.risk.get('','','parent_id=0').then(function (result) {
            $scope.parents = result.data.data;
        });
        $scope.getParent = function(id, field){
            if(id){
                masterService.risk.getRiskStage({'parent_id': id}).then(function (result) {
                    $scope[field] = result.data;
                });
            }
        };
        $scope.openFilterTab = function(current){
            if($scope.filter == ''){
                $scope.filterPanel = true;
            }else if($scope.filter != current){
                $scope.filterPanel = true;
            }
            $scope.filter = current;
            if(current == 'sector')
                $scope.sectorList();
            else if(current == 'country')
                $scope.getCountries();
            else if(current == 'focus_area')
                $scope.getFocusArea();
        };
        $scope.callServer = function (tableState) {
            $scope.loading = true;
            tableState.company_id = $rootScope.id_company;
            delete tableState.sector;
            delete tableState.country_id;
            delete tableState.focus_area;
            delete tableState.sector2;
            delete tableState.country_id2;
            delete tableState.focus_area2;
            if(!tableState.risk_name){
                delete tableState.child_name;
                delete tableState.sub_child_name;
            }
            if(!tableState.child_name){
                delete tableState.sub_child_name;
            }
            if($scope.sector.length>0){
                var sector = [];
                $scope.sector.map(function(item, index){
                    if(item>0){
                        sector.push(item);
                    }
                });
                tableState.sector = sector.toString();
            }
            if($scope.country_id.length>0){
                var country_id = [];
                $scope.country_id.map(function(item, index){
                    if(item>0){
                        country_id.push(item);
                    }
                });
                tableState.country_id = country_id.toString();
            }
            if($scope.focus_area.length>0){
                var focus_area = [];
                $scope.focus_area.map(function(item, index){
                    if(item>0){
                        focus_area.push(item);
                    }
                });
                tableState.focus_area = focus_area.toString();
            }

            reportService.reports.getProjectRiskList(tableState).then(function (result) {
                $scope.projectList = result.data.data;
                $scope.loading = false;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
            });
        };
        $scope.getFocusArea = function(){
            var params = {'master_key': 'focus_area'};
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.focusArea = result.data;
            })
        };

        $scope.getUserList = function(){
            companyService.userList({'company_id': $rootScope.id_company}).then(function(result){
                $scope.companyUsers=result.data.data;
            });
        };
        $scope.addTasksModal=function(data){
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/component/task/add-tasks-modal.html',
                controller: function($scope,$uibModalInstance){
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.task = {};
                    $scope.currentView = 'risk';
                    $scope.task.task_name = 'Risk Alert - '+data.company_name;

                    $scope.submitStatus=false;
                    $scope.saveTask=function(task,form,submitType){
                        $scope.submitStatus=true;
                        if(form.$valid){
                            var params = {};
                            params.module_type='project';
                            params.module_id=data.id_crm_project;
                            params.module_reference_type='project';
                            params.module_reference_id=data.id_crm_project;

                            params.task_name=task.task_name;
                            params.task_description=task.task_description;
                            params.task_due_date=task.task_due_date;

                            params.file = task.file;
                            params.comment = task.comment;

                            params.created_by=$rootScope.userId;
                            params.company_id=$rootScope.id_company;

                            if(!task.member||task.member.length==0){
                                params.member=[];
                                params.member.push($rootScope.userId);
                            }else{
                                params.member = task.member;
                            }

                            if(submitType == 'completed'){
                                params.task_status = 'completed';
                            }
                            taskService.saveTask(params).then(function(result){
                                if($rootScope.toastMsg(result)){
                                    $scope.cancel();
                                    $rootScope.resetForm(form);
                                    $scope.tasksList();
                                }
                            });

                        }
                    };

                    $scope.updateTaskDetails={};
                    $scope.updateStatus=function(taskId,taskStatus){
                        $scope.updateTaskDetails={
                            'id_task': taskId,
                            'task_status': taskStatus
                        };
                        taskService.updateTaskStatus($scope.updateTaskDetails).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.cancel();
                                $scope.tasksList();
                                $scope.task={};
                            }
                        });
                    };

                }
            });
            modalInstance.result.then(function(){
                // $state.reload();
            },function(){
            });
        };
    })
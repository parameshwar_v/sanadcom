/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('crm.activity',[])
    .controller('crmActivityCtrl',function($scope,$rootScope,crmActivityService){})

    .controller('crmActivityListCtrl',function($scope,$rootScope,crmActivityService,taskService,$uibModal){
        $scope.collateralList=[];
        $scope.filter_type='all_collateral';
        $scope.getActivityList=function(stTable){
            stTable.company_id=$rootScope.id_company;
            if(!stTable)
                stTable=$scope.refTable;
            $scope.refTable=stTable;
            stTable.company_id=$rootScope.id_company;
            stTable.assigned_to=$rootScope.userId,stTable.created_by=$rootScope.userId;
            crmActivityService.crm.get(stTable).then(function(result){
                $scope.activityList=result.data.data;
                $scope.all_tasks_count=result.data.total_records;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }

        $scope.preview=function(id){
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope:$scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/activity/add-tasks-modal.html',
                controller: function($scope,$uibModalInstance,companyService,taskService,Upload){
                    $scope.showDetailtaskList=false;
                    $scope.currentView='List';
                    companyService.userList({'company_id': $rootScope.id_company}).then(function(result){
                        $scope.companyUsers=result.data.data;
                    });
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.submitStatus=false;
                    $rootScope.viewtaskDetails=[];
                    $scope.viewTask=function(taskId){
                        taskService.getTasks({'id_task': taskId}).then(function(result){
                            if(result.status){
                                $scope.viewtaskDetails=result.data.data[0];
                                $scope.currentView='View';
                            }
                            else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }
                        });
                    };
                    $scope.closeViewTask=function(){
                        $scope.showDetailtaskList=false;
                    };
                    $scope.showViewTask=function(index){
                        $scope.showIndex=index;
                    };
                    $scope.viewTask(id);
                }
            });
            modalInstance.result.then(function(){
                // $state.reload();
            },function(){
            });
        }

    })



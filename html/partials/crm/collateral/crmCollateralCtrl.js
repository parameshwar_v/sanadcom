/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('crm.collateral',[])
    .controller('crmCollateralCtrl',function($scope,$rootScope,masterService,collateralService,companyService){
        $scope.dropDownItems=[];
        $scope.masterDropDownList=function(params){
            masterService.masterList(params).then(function(result){
                if(result.status)
                    $scope.dropDownItems[params.master_key]=result.data;
            })
        }
        $scope.collateralTypeList=[];
        $scope.getCollateralTypeList=function(){
            collateralService.get({'company_id': $rootScope.id_company}).then(function(result){
                $scope.collateralTypeList=result.data;
            });
        }
        $scope.currencyList=[];
        $scope.getCurrencyList=function(){
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function(result){
                $scope.currencyList=result.data;
                //$scope.showUsdAmount($scope.facilityInfo.currency_id);
            })
        }
        $scope.countries=[];
        $scope.getCountries=function(){
            masterService.country.get().then(function(result){
                $scope.countries=result.data;
            });
        }
        $scope.showUsdAmountDiv=false;
        $scope.showUsdAmount=function(currency_id){
            $scope.showUsdAmountDiv=false;
            if(currency_id){
                var a=_.findWhere($scope.currencyList,{'currency_id': currency_id});//searching Element in Array
                if(a){
                    $scope.currentCurrencyCode=a.currency_code;
                    if(a.currency_code=='USD'){
                        //$scope.facilityInfo.facility_usd_amount = $scope.facilityInfo.facility_amount;
                        $scope.showUsdAmountDiv=false;
                    }else{
                        $scope.showUsdAmountDiv=true;
                    }
                }
            }
            if($scope.showUsdAmountDiv)
                return true;
            return false;
            //$scope.currentFormIndex = _.indexOf($scope.companyFormDetails, a);
        }
        $scope.checkUsd=function(currency_id){
            if(currency_id){
                var a=_.findWhere($scope.currencyList,{'currency_id': currency_id});//searching Element in Array
                if(a){
                    $scope.currentCurrencyCode=a.currency_code;
                    if(a.currency_code=='USD'){
                       return true;
                    }else{
                       return false;
                    }
                }
            }
            return false;
        }
    })
    .controller('crmCollateralListCtrl',function($scope,$rootScope,collateralService,$uibModal){
        $scope.collateralList=[];
        $scope.my_collateral_count=0;
        $scope.all_collateral_count=0;
        $scope.filter_type='all_collateral';
        $scope.getCollateralList=function(stTable){
            if(!stTable)
                stTable=$scope.refTable;
            $scope.refTable=stTable;

            stTable.company_id=$rootScope.id_company;
            delete stTable.created_by;
            if($scope.filter_type=='my_collateral')
                stTable.created_by=$rootScope.userId;

            collateralService.crm.get(stTable).then(function(result){
                $scope.collateralList=result.data.data;
                $scope.my_collateral_count=result.data.my_collateral;
                $scope.all_collateral_count=result.data.all_collateral;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }

        $scope.deleteCollateral =function(id_collateral){
            if($rootScope.confirmMsg()){
                collateralService.crm.delete({'collateral_id':id_collateral}).then(function(result){
                    if($rootScope.toastMsg(result)){
                        $scope.getCollateralList($scope.refTable);
                    }
                })
            }
        }

        $scope.createCovenantModalOpen=function(selectedItem){
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/collateral/collateral-edit.html',
                resolve: {
                    item: selectedItem
                },
                controller: function($uibModalInstance,$rootScope,item){
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    }
                    //form data
                    $scope.fd={};
                    if(item){
                        $scope.fd=item;
                    }
                    $scope.saveCollateral=function(formData){
                        var param=formData;
                        if($scope.checkUsd(formData.collateral_currency_id)){
                            formData.usd_equal_amount = formData.collateral_amount;
                        }
                        param.company_id=$rootScope.id_company;
                        param.created_by=$rootScope.userId;
                        collateralService.crm.post(param).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.getCollateralList();
                                $scope.cancel();
                            }
                        })
                    }
                },
            });
            modalInstance.result.then(function(){
            },function(){
            });
        }
    })
    .controller('collateralViewCtrl',function($scope,$rootScope,$uibModal,covenantService,decodeFilter,encodeFilter,$stateParams,collateralService){
        $scope.collateralId=decodeFilter($stateParams.collateralId);
        $rootScope.collateralTypeId=decodeFilter($stateParams.collateralTypeId);
        $rootScope.currentCollateralId=$scope.collateralId;
        $scope.collateralStageList=[];
        $scope.stageTitle='';
        $scope.isLoading=true;


        collateralService.crm.get({
            'id_collateral': $scope.collateralId,
            'company_id': $rootScope.id_company
        }).then(function(result){
            $scope.collateralDetails=result.data;
            $scope.fd=result.data;
            $rootScope.collateralNumber = $scope.collateralDetails.collateral_number;
        })

        $scope.saveCollateral=function(formData){
            var param=formData;
            if($scope.checkUsd(formData.collateral_currency_id)){
                formData.usd_equal_amount = formData.collateral_amount;
            }
            param.company_id=$rootScope.id_company;
            param.created_by=$rootScope.userId;
            collateralService.crm.post(param).then(function(result){
                if($rootScope.toastMsg(result)){
                    var obj = {};
                    obj.collateralId = encodeFilter($rootScope.currentCollateralId);
                    obj.collateralTypeId = encodeFilter(formData.collateral_type_id);
                    $rootScope.goToState('crm-collateral.collateral-view',obj);
                }
            })
        }

        $scope.getCollateralStageList=function(){
            collateralService.stage.get({
                'company_id': $rootScope.id_company,
                'collateral_type_id': $rootScope.collateralTypeId,
                'status': 1
            }).then(function(result){
                $scope.collateralStageList=result.data;
            })
        }
        //get dynamic form fields
        $scope.form_fields=[];
        $scope.getFields=function(){
            collateralService.crm.typeFormData.get({
                'company_id': $rootScope.id_company,
                'collateral_type_id': $rootScope.collateralTypeId,
                'collateral_id': $rootScope.currentCollateralId
            }).then(function(result){
                $scope.collateralInfo=result.data;
                $scope.form_fields=result.data.fields;
                // $scope.stageTitle=result.data.collateral_type_name;
                // $scope.collateral_stage_icon=result.data.collateral_type_icon;
            })
        }
        //dynamic form filed post

        $scope.saveFormFields=function(form_fields){
            var params={};
            params.data=$scope.form_fields;
            params.collateral_id=$rootScope.currentCollateralId;
            params.collateral_type_id=$rootScope.collateralTypeId;
            params.created_by=$rootScope.userId;
            collateralService.crm.typeFormData.post(params).then(function(result){
                if($rootScope.toastMsg(result)){
                }
            });
        }

        $scope.changeTemplate=function(cs){
            //console.log('cs',cs);
            $rootScope.currentCollateralStageId=cs.id_collateral_stage;
            $scope.stageTitle=cs.collateral_stage_name;
            if($scope.collateralDetails.collateral_type_name=='Personal Guarantee'){
                $scope.stageTitle='Guarantees';
            }
            $scope.template=cs.collateral_stage_template;
            $scope.collateral_stage_icon=cs.collateral_stage_icon;
            $scope.collateral_stage_key=cs.collateral_stage_key;
            $scope.getCollateralDetails();
            $scope.covenantShow=false;
        }
        $scope.getFields();
        $scope.getCollateralStageList();

        $scope.deleteCollateral=function(id){
            if($rootScope.confirmMsg()){
                $scope.isLoading=true;
                switch($scope.collateral_stage_key){
                    case 'valuation_details':
                        collateralService.crm.valuationDetails.delete({
                            'id_valuation': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'documents':
                        break;
                    case 'insurance':
                        collateralService.crm.insuranceDetails.delete({
                            'id_insurance': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break
                    case 'contacts':
                        break;
                    case 'verification_visit':
                        collateralService.crm.fieldInvestigation.delete({
                            'id_field_investigation': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'external_checks':
                        collateralService.crm.externalCheck.delete({
                            'id_external_check': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'risk_evaluation':
                        collateralService.crm.riskEvaluation.delete({
                            'id_risk_evaluation': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'legal_opinion':
                        collateralService.crm.legalOpinion.delete({
                            'id_legal_opinion': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'security_valuation_details':
                        collateralService.crm.securityValuationDetails.delete({
                            'id_security_valuation_details': id,
                            'collateral_id': $scope.collateralId
                        }).then(function(result){
                            if(result.status){
                                if($rootScope.toastMsg(result)){
                                    $scope.getCollateralDetails();
                                    $scope.isLoading=false;
                                }
                            }
                        });
                        break;
                    case 'checklist':
                        break;
                    case 'covenants':
                        break;
                    default:

                }
            }
        }

        $scope.getCollateralDetails=function(){
            $scope.isLoading=true;
            switch($scope.collateral_stage_key){
                case 'valuation_details':
                    $scope.collateralData=[];
                    collateralService.crm.valuationDetails.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'documents':
                    break;
                case 'insurance':
                    $scope.collateralData=[];
                    collateralService.crm.insuranceDetails.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'contacts':
                    break;
                case 'verification_visit':
                    $scope.collateralData=[];
                    collateralService.crm.fieldInvestigation.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'external_checks':
                    $scope.collateralData=[];
                    collateralService.crm.externalCheck.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'risk_evaluation':
                    $scope.collateralData=[];
                    collateralService.crm.riskEvaluation.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'legal_opinion':
                    $scope.collateralData=[];
                    collateralService.crm.legalOpinion.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'security_valuation_details':
                    $scope.collateralData=[];
                    collateralService.crm.securityValuationDetails.get({'collateral_id': $scope.collateralId}).then(function(result){
                        $scope.collateralData=result.data.data;
                        $scope.isLoading=false;
                    });
                    break;
                case 'checklist':
                    break;
                case 'covenants':
                    break;
                default:

            }
        }

        $scope.addCollateral=function(row){
            $scope.selectedRow=row;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'add_'+$scope.template,
                scope: $scope,
                resolve: {
                    item: function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                },
                controller: function($uibModalInstance,$scope,$rootScope,collateralService,item){
                    $scope.title='Add';
                    if(item){
                        $scope.title='Edit';
                        $scope.field=item;
                    }
                    switch($scope.collateral_stage_key){
                        case 'valuation_details':
                            $scope.addFacility=function(field){
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                params.company_id=$rootScope.id_company;
                                collateralService.crm.valuationDetails.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'documents':
                            break;
                        case 'insurance':

                            $scope.addFacility=function(field){
                                if($scope.checkUsd(field.premium_currency_id)){
                                    field.premium_usd_equal_to = field.premium_amount;
                                }
                                if($scope.checkUsd(field.insurance_currency_id)){
                                    field.insurance_usd_equal_to = field.insurance_amount;
                                }
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                collateralService.crm.insuranceDetails.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'contacts':
                            break;
                        case 'verification_visit':
                            $scope.addFacility=function(field){
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                params.company_id=$rootScope.id_company;
                                collateralService.crm.fieldInvestigation.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'external_checks':
                            $scope.addFacility=function(field){
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                collateralService.crm.externalCheck.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'risk_evaluation':
                            $scope.addFacility=function(field){
                                var params={};
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                params.id_risk_evaluation = field.id_risk_evaluation;
                                params.risk = field;
                                collateralService.crm.riskEvaluation.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'legal_opinion':
                            $scope.addFacility=function(field){
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                collateralService.crm.legalOpinion.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'security_valuation_details':
                            $scope.addFacility=function(field){
                                var params=field;
                                params.collateral_id=$scope.collateralId;
                                params.created_by=$rootScope.userId;
                                collateralService.crm.securityValuationDetails.post(params).then(function(result){
                                    if($rootScope.toastMsg(result)){
                                        $uibModalInstance.close();
                                    }
                                })
                            }
                            break;
                        case 'covenants':
                            break;
                        default:

                    }
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });
            modalInstance.result.then(function($data){
                $scope.getCollateralDetails();
            },function(){
            });
        }

        $scope.otherCollateralList=[];
        $scope.getOtherCollateral=function(){
            collateralService.crm.otherCollateral({'collateral_id': $scope.collateralId}).then(function(result){
                $scope.otherCollateralList=result.data;
            })
        }

        //covenant
        $scope.covenantShow=false;
        $scope.gotoTable=function(){
            $scope.covenantShow=false;
        }

        $scope.deleteCovenant=function(id_module_covenant){
            if($rootScope.confirmMsg()){
                covenantService.crmCovenant.delete({'id_module_covenant': id_module_covenant}).then(function(result){
                    if($rootScope.toastMsg(result)){
                        $scope.getCovenantDetails();
                    }
                })
            }
        }
        $scope.createCovenant=function(name,data){
            $scope.c={};
            $scope.covenantShow=true;
            $scope.c.covenant_name=name;
            $scope.getData=data;

            $scope.getCovenantDetails=function(){
                var params={};
                angular.extend(params,$scope.getData);
                params.company_id=$rootScope.id_company;
                covenantService.crmCovenant.get(params).then(function(result){
                    console.log('result',result);
                    $scope.c = {};
                    if(result.status)
                        if(result.data[0])
                            $scope.c=result.data[0];

                    $scope.c.covenant_name=name;
                })
            }
            $scope.getCovenantDetails();

        }
        $scope.configCovenantModalOpen=function(id_module_covenant){
            $scope.id_module_covenant=id_module_covenant;
            var modalInstance=$uibModal.open({
                animation: true,
                openedClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl: 'partials/component/covenant/config-covenant-modal.html',
                controller: function($uibModalInstance,covenantService,companyService,collateralService){
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.covenant={};
                    $scope.updateCovenant=function(params){
                        params.created_by=$rootScope.userId;
                        params.company_id=$rootScope.id_company;
                        angular.extend(params,$scope.getData);
                        collateralService.crm.moduleCovenant(params).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.getCovenantDetails();
                                $scope.cancel();
                            }

                        })

                    }
                    if(id_module_covenant){
                        var params={};
                        params.id_module_covenant=id_module_covenant;
                        params.company_id=$rootScope.id_company;
                        angular.forEach($scope.getData,function(value,key){
                            params[key]=value;
                        })
                        covenantService.crmCovenant.get(params).then(function(result){
                            if(result.status)
                                $scope.covenant=result.data;
                        })
                    }
                    $scope.userList=[];
                    $scope.getUserAllList=function(){
                        companyService.userList({'company_id': $rootScope.id_company}).then(function(result){
                            $scope.userList=result.data.data;
                        });
                    }
                    //$scope.getUserAllList();


                }
            })
        }

    })
    //stage contact stage controller
    .controller('collateralAddedContactsCtrl',function($rootScope,$scope,crmCompanyService,collateralService,crmContactService){
        $rootScope.collateralContactList=[];
        $scope.getCollateralContactList=function(){
            collateralService.crm.contact.get({'collateral_id': $rootScope.currentCollateralId}).then(function(result){
                $rootScope.collateralContactList=result.data;
            })
        }
        $scope.getCollateralContactList();
        $scope.getContactList=function(search_key){
            $scope.visibleAddButton=false;
            return crmContactService.contactList({
                'collateral_id': $rootScope.currentCollateralId,
                'search_key': search_key,
                'company_id': $rootScope.id_company
            }).then(function(result){
                return result.data.data;
            });
        };
        $scope.onSelect=function($item,$model,$label){
            $scope.visibleAddButton=true;
            $scope.selectedItem=$item;
            $scope.visibleAddButton=false;
            $scope.visibleDesignationDropDown=true;
            $scope.asyncSelected='';
        };
        $scope.visibleDesignationDropDown=false;
        $scope.collateralContactSave=function(fd,form){
            console.log('selectedItem',$scope.selectedItem);
            $scope.submitStatus=true;
            if(form.$valid){
                var param={};
                param.collateral_id=$rootScope.currentCollateralId;
                param.crm_contact_id=$scope.selectedItem.id_crm_contact;
                param.relation_type_id=fd.relation_type_id;
                param.created_by=$rootScope.userId;
                param.created_by=$rootScope.userId;
                collateralService.crm.contact.post(param).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.visibleDesignationDropDown=false;
                        $scope.getCollateralContactList();
                    }else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                });
            }
        };
        $scope.cancelAssign=function(){
            $scope.visibleDesignationDropDown=false;
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.removeContactCompany=function($id){
            if($rootScope.confirmMsg()){
                collateralService.crm.contact.delete({'id_collateral_contact': $id}).then(function(result){
                    $scope.getCollateralContactList();
                })
            }
        }
    })

    .controller('crmDocumentsCtrl',function($scope,$rootScope,collateralService,$uibModal){
        $scope.documentList=[];
        $scope.getDocumentsList=function(){
            var param={};
            param.company_id=$rootScope.id_company;
            param.collateral_type_id=$rootScope.collateralTypeId;
            collateralService.documents.get(param).then(function(response){
                $scope.documentList=response.data;
                console.log('response',response);
            })
        }

    })

    .controller('collateralChecklistCtrl',function($scope,$rootScope,crmProjectService,collateralService,Upload,$q,crmCompanyService){
        $scope.categoryQuestionList=[];
        $scope.getAssessmentQuestion=function(){
            collateralService.crm.assessmentQuestions.get({
                'company_id': $rootScope.id_company,
                'collateral_type_id': $rootScope.collateralTypeId,
                'crm_reference_id': $rootScope.currentCollateralId,
                'assessment_question_type_key': 'collateral_document',
                'collateral_stage_id': $rootScope.currentCollateralStageId
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList=result.data;
                }
            });
        }
        $scope.saveAnswerRestrictions=function(category){
            bulkAnswers=[];
            angular.forEach(category,function(category){
                angular.forEach(category.question,function(question,$index){
                    var $obj={};
                    $obj.assessment_answer=question.answer.assessment_answer;
                    $obj.assessment_question_id=question.id_assessment_question;
                    $obj.crm_reference_id=$rootScope.currentCollateralId;
                    $obj.created_by=$rootScope.userId;
                    $obj.company_id=$rootScope.id_company;
                    $obj.id_assessment_question_type=$rootScope.projectParames.formId;
                    if(question.answer.id_assessment_answer!=undefined){
                        $obj.id_assessment_answer=question.answer.id_assessment_answer;
                    }
                    $rootScope.loaderOverlay=false;
                    if(question.question_type=='file'){
                        if(question.answer.file.name!=undefined){
                            Upload.upload({
                                url: API_URL+'Company/assessmentAnswer',
                                data: {
                                    file: question.answer.file,
                                    'answer': $obj
                                }
                            }).then(function(result){
                                if(result.status){
                                    if($index==category.question.length-1){
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success',result.message);
                                }else{
                                    $rootScope.toast('Error',result.error,'error');
                                }
                            });
                        }
                    }else{
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                if($index==category.question.length-1){
                                    $scope.getAssessmentQuestion();
                                }
                            }else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        })
                    }
                })
            })
        }
        $scope.saveAnswer=function(questions,form_key){
            var bulkAnswers=[];
            angular.forEach(questions,function(question,$index){
                var $obj={};
                $obj.assessment_answer=question.answer.assessment_answer;
                $obj.assessment_question_id=question.id_assessment_question;
                $obj.crm_reference_id=$rootScope.currentCollateralId;
                $obj.created_by=$rootScope.userId;
                $obj.company_id=$rootScope.id_company;
                $obj.comment=question.answer.comment;
                $obj.type='media';
                $obj.form_key='0';

                if(question.answer.id_assessment_answer!=undefined){
                    $obj.id_assessment_answer=question.answer.id_assessment_answer;
                }
                $rootScope.loaderOverlay=false;

                Upload.upload({
                    url: API_URL+'Company/assessmentAnswer',
                    data: {
                        file: question.answer.file,
                        'answer': $obj
                    }
                }).then(function(result){
                    if(result.data.status){

                        if($index==questions.length-1){
                            $rootScope.toast('Success',result.data.message);
                            $scope.getAssessmentQuestion();
                        }
                    }else{
                    }
                });

                /* if (question.answer.file.name != undefined) {
                 Upload.upload({
                 url: API_URL + 'Company/assessmentAnswer',
                 data: {
                 file: question.answer.file,
                 'answer': $obj
                 }
                 }).then(function (result) {
                 if (result.data.status) {
                 $rootScope.toast('Success', result.data.message);
                 if ($index == questions.length - 1) {
                 $scope.getAssessmentQuestion();
                 }
                 } else {
                 }
                 });
                 } else {
                 crmCompanyService.assessmentQuestion.answer.post($obj).then(function (result) {
                 if (result.data.status) {
                 $rootScope.toast('Success', result.data.message);
                 if ($index == questions.length - 1) {
                 $scope.getAssessmentQuestion(form_key);
                 }
                 } else {
                 }
                 })
                 }*/
            })
        }
    })

    .controller('mapFacilityCtrl',function($scope,$rootScope,$uibModal,collateralService,crmProjectService){
        $scope.facilityCollateralList=[];
        $scope.getCollateralFacilityList=function(){
            collateralService.facility.get({collateral_id: $rootScope.currentCollateralId}).then(function(result){
                $scope.facilityCollateralList=result.data;
                //stTable.pagination.numberOfPages = Math.ceil(result.data.total_records / stTable.pagination.number);
            })
        };
        $scope.removeFacilityCollateral=function($id){
            console.log($id);
            if($rootScope.confirmMsg()){
                collateralService.facility.delete({
                    'id_project_facility_collateral': $id,
                    'created_by': $rootScope.userId
                }).then(function(result){
                    $scope.getCollateralFacilityList();
                })
            }
        }

        $scope.getCollateralFacilityList();
        $scope.mapCollateral=function(row){
            $scope.selectedRow=row;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'map_collateral.html',
                scope: $scope,
                controller: function($uibModalInstance,$scope,$rootScope,collateralService,item){
                    $scope.title='Add';
                    if(item){
                        $scope.title='Edit';
                        $scope.field=item;
                    }

                    $scope.getFacilityListWithKey=function(search_key){
                        return collateralService.crm.facilitySearch({
                            company_id: $rootScope.id_company,
                            'search_key': search_key,
                            'collateral_id': $rootScope.currentCollateralId
                        },'search_loader').then(function(result){
                            return result.data;
                        });
                    };

                    $scope.visibleFacilityDropDown=false;
                    $scope.onSelect=function($item,$model,$label){
                        $scope.visibleAddButton=true;
                        $scope.selectedItem=$item;
                        $scope.visibleAddButton=false;
                        $scope.visibleFacilityDropDown=true;
                        $scope.asyncSelected='';
                    };
                    $scope.cancelAssign=function(){
                        $scope.visibleFacilityDropDown=false;
                    };
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.facilityCollateralSave=function(project_facility_id,form){
                        console.log('selectedItem',$scope.selectedItem);
                        $scope.submitStatus=true;
                        if(form.$valid){
                            var param={};
                            param.collateral_id=$rootScope.currentCollateralId;
                            param.project_facility_id=project_facility_id;
                            param.created_by=$rootScope.userId;
                            collateralService.facility.post(param).then(function(result){
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $scope.visibleFacilityDropDown=false;
                                    $scope.getCollateralFacilityList();
                                    $scope.cancel();
                                }else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            });
                        }
                    };
                },
                resolve: {
                    item: function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }
            });
            modalInstance.result.then(function($data){

            },function(){
            });
        }
    })
    .controller('riskEvaluationCtrl',function($scope,$uibModal,masterService){


        $scope.risk_items = {
            'name': '',
            'type': '',
            'description': '',
            'mitigation': '',
            'grade': ''
        };
        $scope.riskGrades = [
            {'value': '1', 'label':'1  (very low risk)'},
            {'value': '2', 'label':'2  (low risk)'},
            {'value': '3', 'label':'3  (medium risk)'},
            {'value': '4', 'label':'4  (high risk)'},
            {'value': '5', 'label':'5  (significant risk)'},
        ];

        $scope.getParent = function(id, field){
            var fieldName = '';
            var parentId = '';
            if(field == 'childs'){
                fieldName = ''+field;
                parentId = id.id_risk_type;
                $scope.subchilds = {};
            }else if(field == 'subchilds'){
                fieldName = ''+field;
                parentId = id.id_risk_type;
            }else{
                fieldName = 'parents';
                $scope.childs = {};
                $scope.subchilds = {};
                parentId = 0;
            }
            masterService.risk.getRiskStage({'parent_id': parentId}).then(function (result) {
                $scope[fieldName] = result.data;
            });
        };

        $scope.title = 'Add';
        if ($scope.selectedRow) {
            $scope.update = true;
            $scope.title = 'Edit';
            //$scope.risk_items = $scope.selectedRow;
            angular.copy($scope.selectedRow, $scope.risk_items);
            $scope.getParent($scope.risk_items.child ,'subchilds');
            $scope.getParent($scope.risk_items.parent ,'childs');
        }else{
            $scope.update = false;
        }
        masterService.risk.get().then(function (result) {
            $scope.riskList = result.data.data;
        });


        $scope.removeItem=function removeItem(data,row){
            var index=data.indexOf(row);
            if(index!== -1){
                data.splice(index,1);
            }
        }
        $scope.jsonConvert=function(data){
            return JSON.parse(data);
        }
        $scope.addRisk=function(row){
            $scope.selectedRow=row;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'risk-fields.html',
                scope: $scope,
                controller: function($uibModalInstance,$scope,masterService){
                    $scope.risk_items={
                        'name': '',
                        'type': '',
                        'description': '',
                        'mitigation': '',
                        'grade': ''
                    };
                    $scope.riskGrades=[
                        {'value': '1','label': '1  (very low risk)'},
                        {'value': '2','label': '2  (low risk)'},
                        {'value': '3','label': '3  (medium risk)'},
                        {'value': '4','label': '4  (high risk)'},
                        {'value': '5','label': '5  (significant risk)'},
                    ];

                    $scope.getParent=function(id,field){
                        var fieldName='';
                        var parentId='';
                        if(field=='childs'){
                            fieldName=''+field;
                            parentId=id.id_risk_type;
                            $scope.subchilds={};
                        }else if(field=='subchilds'){
                            fieldName=''+field;
                            parentId=id.id_risk_type;
                        }else{
                            fieldName='parents';
                            $scope.childs={};
                            $scope.subchilds={};
                            parentId=0;
                        }
                        masterService.risk.getRiskStage({'parent_id': parentId}).then(function(result){
                            $scope[fieldName]=result.data;
                        });
                    };

                    $scope.title='Add';
                    if($scope.selectedRow){
                        $scope.update=true;
                        $scope.title='Edit';
                        //$scope.risk_items = $scope.selectedRow;
                        angular.copy($scope.selectedRow,$scope.risk_items);
                        $scope.getParent($scope.risk_items.child,'subchilds');
                        $scope.getParent($scope.risk_items.parent,'childs');
                    }else{
                        $scope.update=false;
                    }
                    masterService.risk.get().then(function(result){
                        $scope.riskList=result.data.data;
                    });

                    $scope.updateFun=function(){
                        angular.copy($scope.risk_items,$scope.selectedRow);
                        return true;
                    };
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save=function($Data){
                        $scope.field.risk_factor.push($Data);
                        $scope.cancel();
                    }
                }
            });

        }
    })


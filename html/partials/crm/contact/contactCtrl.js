/**
 * Created by RAKESH on 18-12-2015.
 */
angular.module('app')
    .controller('contactCtrl', function ($scope, $rootScope,crmContactService, masterService) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.contactTypes = [];
        $scope.getContactTypes = function (){
            crmContactService.crmContactType().then(function(result){
                $scope.contactTypes=result.data;
            });
        }
        $rootScope.cInfo = {};
        $scope.getContactInfo = function(){
            crmContactService.contactInformation({'crm_contact_id': $rootScope.currentContactViewId}).then(function (result) {
                $rootScope.cInfo = result.data;
            });
        }
        $scope.saveBtnShow=function(){
            if($rootScope.cInfo.created_by == $rootScope.userId){
                return true
            }else{
                return false;
            }
        };
    })
    .controller('contactListCtrl', function ($scope, $rootScope, crmContactService, crmCompanyService, quickService, masterService, $state, encodeFilter, $timeout) {
        console.log('ABCD');
        $scope.db = {items: []};
        $scope.contactList = [];
        $scope.totalContactCount = 0;
        $scope.contact = {};
        $scope.contact.client_phone_number_isd = '';

        $scope.f_type = 'all_clients';
        $scope.my_contacts = 0;
        $scope.isEmptyRow = false;
        $scope.callServer = function (tableState) {
            tableState.company_id = $rootScope.id_company;
            tableState.created_by = undefined;
            if($scope.f_type=='my_clients')
                tableState.created_by = $rootScope.userId;

            crmContactService.contactList(tableState).then(function (result) {
                $scope.tableStateRef = tableState;
                $scope.contactList = result.data.data;
               // $scope.total_records = result.data.total_records;
                $scope.my_clients = result.data.my_clients;
                $scope.all_clients = result.data.all_clients;
                $scope.isEmptyRow = (result.data.data.length ==0)?true:false;
                // $scope.totalContactCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
            });
        };
        masterService.country.get().then(function ($result) {
            $scope.ISDCodes = $result.data;
        });
        $scope.gotoState = function (row) {
            var Id = encodeFilter(row.id_crm_contact);
            $state.go('contact.contact-form-list', {'id': Id});
        }


        $scope.submitQuickContact = function ($inputData, form, moreFields) {
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                if($inputData.client_phone_number){
                    $inputData.client_phone_number = $inputData.client_phone_number_isd+' '+$inputData.client_phone_number;
                }
                if($inputData.client_phone_number1){
                    $inputData.client_phone_number = $inputData.client_phone_number_isd+' '+$inputData.client_phone_number1;
                }
                quickService.saveQuickContact($inputData).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        form.$setPristine();
                        $scope.contact = {};
                        if (moreFields) {
                            $rootScope.currentContactViewId = result.data.crm_contact_id;
                            $state.go('contact.contact-form-list', {id: encodeFilter($rootScope.currentContactViewId)});
                        } else {
                            $scope.callServer($scope.tableStateRef);
                        }
                    }
                })
        };
        $scope.deleteContact = function(row) {
            var r=confirm('Are you sure you want to delete? Once deleted cannot be reverted');
            if(r) {
                quickService.deleteContact({'crm_contact_id': row.id_crm_contact}).then(function (result) {
                    if ($rootScope.toastMsg(result)) {
                        $scope.callServer($scope.tableStateRef);
                    }
                })
            }
        }

    })
    .controller('contactViewCtrl', function ($rootScope, $scope, crmContactService, decodeFilter, $stateParams, $state, $uibModal, crmCompanyService, $q, masterService) {

        $rootScope.contactId = $rootScope.currentContactViewId = decodeFilter($stateParams.id);
        console.log('ABCD : ',$rootScope.contactId);
        $rootScope.currentModuleId = 1;
        $scope.contactForms = [];

        $scope.init = function () {
            crmContactService.contactForms({
                'crm_contact_id': $rootScope.currentContactViewId,
                'crm_module_id': 1
            }).then(function (result) {
                console.log('contactForms : ',result);
                $rootScope.contactFormDetails = [];
                $scope.contactForms = result.data;
                angular.forEach($scope.contactForms, function (section) {
                    angular.forEach(section.forms, function ($item) {
                        var $obj = {};
                        $obj.sectionName = section.section_name;
                        $obj.formName = $item.form_name;
                        $obj.tag = $item.form_name;
                        $obj.formId = $item.id_form;
                        $obj.template = $item.form_template;
                        $obj.form_key = $item.form_key;
                        $rootScope.contactFormDetails.push($obj);
                    })
                })
            });
        }
        $scope.goToEditCtrl = function (data,section_name) {
            var $obj = {};
            $obj.sectionName = section_name;
            $obj.formName = data.form_name;
            $obj.tag = data.form_name;
            $obj.formId = data.id_form;
            $obj.template = data.form_template;
            $obj.form_key = data.form_key;
            $rootScope.contactParames = $obj;
            $state.go("contact.contact-form-edit", {'id': $stateParams.id});
        };
        $rootScope.selectedCompanyList = function () {
            crmContactService.getCompanyListForContact({'crm_contact_id': decodeFilter($stateParams.id)}).then(function (result) {
                $rootScope.selectedCompanyListData = result.data;
            });
        };
        /*Attachments*/
        $scope.quickAttachmentModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                template: '<div class="right-modal modal-custom" data-modal="attachment">' +
                ' <h3 class="right-modal-head">' +
                '<i class="fa fa-paperclip mr8"></i> {{"Attachments" | translate}}' +
                ' <a class="close-right pull-right" ng-click="cancel()" n-t=\'{"e_n":"Close","e_d":""}\'><img src="images/close.png"></a>' +
                '</h3>' +
                '<attachment-view module-id="1" ref-id="{{currentContactViewId}}"></attachment-view>' +
                '</div>',
                controller: function ($scope, $uibModalInstance) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                // $rootScope.$emit("reloadTouchPoint");
                // $scope.init();
                // $state.reload();
            });
        }
        /*Added Companies Modal*/
        $scope.AddedCompaniesModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/crm/contact/added-companies.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService) {
                    $rootScope.selectedCompanyList();
                    $scope.getCompanyList = function (search_key) {
                        $scope.visibleAddButton = false;
                        if(search_key.length > 3){
                            return crmCompanyService.companyListForMapping({
                                'crm_contact_id': decodeFilter($stateParams.id),
                                'search_key': search_key,
                                'company_id': $rootScope.id_company
                            }).then(function (result) {
                                return result.data;
                            });
                        }
                    };
                    $scope.getCompanyDesignations = function(key){
                        crmCompanyService.companyDesignation({'map_type':key}).then(function (result) {
                            $scope.companyDesignations = result.data;
                        });
                    }
                    $scope.onSelect = function ($item, $model, $label) {
                        $scope.visibleAddButton = true;
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function (designation, form) {
                        $scope.submitStatus = true;
                        if (form.$valid) {
                            var fields = {
                                'crm_contact_id': decodeFilter($stateParams.id),
                                'crm_company_id': $scope.selectedItem.id_crm_company,
                                'crm_company_designation_id': designation.id_child,
                                'is_primary_company': designation.is_primary_company,
                                'created_by': $rootScope.userId,
                                'type': 'contact'
                            };
                            crmCompanyService.addCompanyToContact(fields).then(function (result) {
                                $scope.visibleDesignationDropDown = false;
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedCompanyList();
                                    $scope.init();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        };
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.removeContactCompany = function ($id) {
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            crmCompanyService.deleteContactCompany($id).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedCompanyList();
                                    $scope.init();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            });
                        }
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                //$state.reload();
            });
        };
        $scope.init();
        $scope.postField = {'reminder_date': new Date()};
        $rootScope.selectedRelationsList = function () {
            $rootScope.selectedRelationsListData = {};
            var params = {};
            params.crm_contact_id = decodeFilter($stateParams.id);
            params.relation_type = 'all';
            crmContactService.getRelations(params).then(function (result) {
                $rootScope.selectedRelationsListData = result.data.relations;
            });
        };
        $rootScope.selectedRelationsList();
        $scope.addRelationsModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/component/Relations/add-relations-modal.html',
                controller: function ($scope, $uibModalInstance, crmCompanyService) {
                    $rootScope.selectedRelationsList();
                    $scope.searchRelations = function(key){
                        if(key.length>3){
                            var params = {};
                            params.crm_contact_id = decodeFilter($stateParams.id);
                            params.search_key = key;
                            return crmContactService.getRelationsList(params).then(function(result){
                                return result.data;
                            });
                        }
                    }
                    $scope.visibleRelationDropDown = false;
                    $scope.onSelect = function (data){
                        $scope.visibleRelationDropDown = true;
                        $scope.new_contact = {};
                        $scope.new_contact.client_first_name = data.first_name;
                        $scope.new_contact.client_last_name = data.last_name;
                        $scope.new_contact.client_email = data.email;
                        $scope.new_contact.crm_contact_id = data.id_crm_contact;
                        if(data.id_crm_contact>0) $scope.new_contact.child_contact_id = data.id_crm_contact;
                        $scope.new_contact.crm_contact_type_id = data.crm_contact_type_id;
                        $scope.new_contact.crm_contact_type_name = data.crm_contact_type_name;
                        $scope.new_contact.client_gender = data.data.client_gender;
                        $scope.new_contact.client_date_of_birth = data.data.client_date_of_birth;
                    }
                    $scope.saveContact = function(contact,data,form){
                        if (form.$valid) {
                            var fields = {};
                            fields = contact;
                            fields.crm_company_id = $rootScope.id_company;
                            fields.created_by = $rootScope.userId;
                            fields.crm_contact_id = decodeFilter($stateParams.id);
                            fields.save_create = 0;
                            fields.relation_type = data.id;
                            if(contact.child_contact_id=='')
                                delete contact.child_contact_id;
                            crmContactService.saveRelation(fields).then(function(result){
                                if(result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedRelationsList();
                                    $scope.visibleRelationDropDown = false;
                                }else $rootScope.toast('Error',result.error,'error');
                            })
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cancelAssign = function () {
                        $scope.visibleRelationDropDown = false;
                        $scope.relationForm ={};
                    };
                    $scope.removeRelationContact = function (row) {
                        var id = row.id_crm_contact_relation;
                        var r = confirm("Do you want to continue?");
                        if (r == true) {
                            crmContactService.deleteRefrenceGuarantor(id).then(function (result) {
                                if(result.status){
                                    $rootScope.toast('Success', result.message);
                                    $rootScope.selectedRelationsList();
                                }else $rootScope.toast('Error',result.error,'error');
                            });
                        }
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
                //$state.reload();
            });
        }
    })
    .controller('contactFormViewCtrl', function ($rootScope, $scope,crmCompanyService,crmContactService, decodeFilter, $stateParams, $state, masterService,crmProjectService,$uibModal,Upload) {
        $scope.minDate = $scope.minDate ? null : new Date();
        $rootScope.isFormChanged=false;

        if ($rootScope.contactParames == undefined) {
            $rootScope.back();
            $state.go("contact.contact-form-list", {'id': $stateParams.id});
            //return false;
        }
        $rootScope.currentCompanyViewId=$rootScope.id_company;
        $scope.checkNumber = function(data,id){
            if (data == "") {
                return "This field is required";
            }
        }
        $rootScope.crm_contact_id = $rootScope.currentContactViewId = decodeFilter($stateParams.id);
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;
        $scope.db = {items: [[]]};
        $scope.settings = {colHeaders: true, autoWrapCol: true, contextMenu: ['row_above', 'row_below', 'remove_row']};
        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;

        $scope.acualObj = {};
        $scope.userId =$rootScope.userId;

        //goto next form
        $scope.nextForm = function () {
            console.log($rootScope.isFormChanged);
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var nextIndex = $scope.currentFormIndex + 1;
                    $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                    $scope.init();
                } else
                    return false;
            } else {
                var nextIndex = $scope.currentFormIndex + 1;
                $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                $scope.init();
            }
            /*if(!angular.equals($scope.acualObj, $scope.contact)){
                //var r = confirm("Continue without saving data?");
                //if(r){
                    var nextIndex = $scope.currentFormIndex + 1;
                    $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                    $scope.init();
                //}
            }else{
                var nextIndex = $scope.currentFormIndex + 1;
                $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                $scope.init();
            }*/
        }
        //goto prev form
        $scope.prevForm = function () {
            /*if(("client_financial_credit_rating_description" in $scope.contact)){ $scope.acualObj.client_financial_credit_rating_description = $scope.contact.client_financial_credit_rating_description }

            if(!angular.equals($scope.acualObj, $scope.contact)){
                //var r = confirm("Continue without saving data?");
                //if(r){
                    var nextIndex = $scope.currentFormIndex - 1;
                    $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                    $scope.init();
                //}
            }else{
                var nextIndex = $scope.currentFormIndex - 1;
                $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                $scope.init();
            }*/
            if($rootScope.isFormChanged){
                var r = confirm("Continue without saving data?");
                if(r){
                    $rootScope.isFormChanged=false;
                    var nextIndex = $scope.currentFormIndex - 1;
                    $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                    $scope.init();
                } else
                    return false;
            } else {
                var nextIndex = $scope.currentFormIndex - 1;
                $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
                $scope.init();
            }
        }
        $scope.formChangeEvent = function(){
            $rootScope.isFormChanged=true;
        };
        $scope.Countries = [];
        //currecnyList
        $scope.getCountryList = function (data) {
            delete $scope.ISDCodes;
            delete $scope.Countries;
            $scope.ISDCodes ={};
            $scope.Countries ={};
            masterService.country.get(data).then(function ($result) {
                $scope.ISDCodes = $result.data;
                $scope.Countries = $result.data;
            });
        }
        $scope.getKnowledgeDetails=function(){
            crmCompanyService.getKnowledgeData({
                'company_id': $rootScope.id_company,
                'type': 'crm_contact',
                'tags': $scope.tag,
                'crm_contact_id': $rootScope.crm_contact_id
            }).then(function(result){
                $scope.knowledgeData=result.data;
            });
        }
        $scope.init = function () {
            if ($rootScope.contactParames.formId) {
                var a = _.findWhere($rootScope.contactFormDetails, {'tag': $rootScope.contactParames.tag});//searching Element in Array
                $scope.currentFormIndex = _.indexOf($rootScope.contactFormDetails, a);// getting index.
                if ($scope.currentFormIndex != -1) {
                    $scope.prevBtnShow = ($scope.currentFormIndex > 0) ? true : false;
                    $scope.nextBtnShow = ($scope.currentFormIndex < ($rootScope.contactFormDetails.length - 1)) ? true : false;
                }
            }
            $scope.template = $rootScope.contactParames.template;
            $scope.formName = $rootScope.contactParames.formName;
            $scope.sectionName = $rootScope.contactParames.sectionName;
            $scope.tag = $rootScope.contactParames.tag;
            $scope.formId = $rootScope.contactParames.formId;
            $scope.form_key = $rootScope.contactParames.form_key;
            $scope.contact = {};
            $scope.loading = true;
            crmContactService.contactFormDetails($rootScope.crm_contact_id, $scope.formId, 'edit').then(function (result) {
                if (result.status) {
                    $scope.loading = false;
                    $scope.contact = result.data.data;
                    if($scope.contact.client_phone_number){
                        var phone = $scope.contact.client_phone_number.split(' ');
                        if(typeof phone[1] == 'undefined'){
                            $scope.contact.client_phone_number1 = $scope.contact.client_phone_number;
                        }else{
                            $scope.contact.client_phone_number_isd = phone[0];
                            $scope.contact.client_phone_number1 = phone[1];
                        }
                    }
                    if($scope.contact.client_alternative_phone_number){
                        var phone = $scope.contact.client_alternative_phone_number.split(' ');
                        if(typeof phone[1] == 'undefined'){
                            $scope.contact.client_alternative_phone_number1 = $scope.contact.client_alternative_phone_number;
                        }else{
                            $scope.contact.client_alternative_phone_number_isd = phone[0];
                            $scope.contact.client_alternative_phone_number1 = phone[1];
                        }
                    }
                    if(!$scope.contact.client_residence){
                        $scope.contact.client_residence = 'Rented';
                    }else{
                        $scope.contact.client_residence = $scope.contact.client_residence;
                    }

                    /*$scope.acualObj = angular.copy($scope.contact);*/
                }
            });
            $scope.getKnowledgeDetails();
            switch ($rootScope.contactParames.form_key) {
                /*case 'client_information':
                    $scope.masterDropDownList({'master_key': 'contact_prefix'});
                    break;*/
                case 'personal_information':
                    console.log('personal_information');
                    $scope.gender = [{'name':'male', 'value':'1'}, {'name':'female', 'value':'2'}];
                    $scope.children = [];
                    $scope.isSpousePresent = true;
                    $scope.contactinfo = {};
                    crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function (result) {
                        $scope.currencyList= result.data;
                        $scope.currencyList.splice(-1,1);
                    });
                    $scope.masterDropDownList({'master_key': 'contact_marital_status'});
                    $scope.contactinfo['martial_status'] = '';
                    $scope.contactinfo.residence = '1';
                    $scope.currentDateTime = new Date();
                    $scope.getRelationsListData = function(type){
                        var params ={};
                        params.relation_type = type;
                        params.crm_contact_id = $rootScope.crm_contact_id;
                        params.crm_company_id = $rootScope.id_company;
                        crmContactService.getRelations(params).then(function(result){
                            if(result.status){
                                $scope['children'] = result.data.relations;
                            }
                            else $rootScope.toast('Error', result.error,'error');
                        });
                    };
                    $scope.getPersonalInformation=function(){
                        crmContactService.getPersonalInformation({'crm_contact_id':$rootScope.currentContactViewId, 'crm_company_id':$rootScope.id_company}).then(function (result) {
                            if (result.status) {
                                if(result.data){
                                    $scope.contactinfo = result.data;
                                    $scope.contactinfo.residence = '1';
                                    if(result.data && result.data.martial_status){
                                        for(var a in $scope.dropDownItems['contact_marital_status']){
                                            if($scope.dropDownItems['contact_marital_status'][a].id == result.data.martial_status){
                                                $scope.contactinfo.martial_status = $scope.dropDownItems['contact_marital_status'][a];
                                                break;
                                            }
                                        }
                                    }
                                    setTimeout(function(){
                                        $scope.getRelationsListData('children,spouse');
                                    },200);
                                }
                            }
                        });
                    };
                    $scope.saveContactPersonalInformation = function(contactInfo,formDetails,form){
                        var data = contactInfo;console.log('form',form);
                        if(data.martial_status){
                            if(data.martial_status.child_key=='marital_status_single'){
                                delete data.children;
                                delete data.spouse_name;
                                data.martial_status=data.martial_status.id;
                            } else {
                                data.martial_status=data.martial_status.id;
                            }
                        }else
                            console.log('not martial status');

                        data.children=$scope.children;

                        data.crm_contact_id = $rootScope.crm_contact_id;
                        data.crm_company_id = $rootScope.id_company;
                        data.created_by = $rootScope.userId;
                        data.form_id = $scope.formId;
                        delete data.id_contact_personal_information;

                        crmContactService.addPersonalInformation(data).then(function (result) {
                            if (result.status) {
                                $scope.getPersonalInformation();
                                $rootScope.toast('Success', result.message);
                            } else {
                                $rootScope.toast('Error', result.error, 'error', $scope.sector);
                            }
                        });
                    }
                    $scope.addContactSpouseChildrens=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-contact-children.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload){
                                $scope.title='Add';
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }
                                $scope.new_contact = {};
                                $scope.showForm = false;
                                $scope.reset = function(form){
                                    $scope.new_contact = {};
                                    $scope.search = {};
                                    $scope.search.name = '';
                                    $scope.new_contact.client_gender='Male';
                                    $scope.new_contact.relation_type='spouse';
                                    $scope.new_contact.crm_contact_type_id='1';
                                    $scope.new_contact.child_contact_id='';
                                    /*$scope.new_contact.client_gender='Male';
                                    $scope.new_contact.child_contact_id='';*/
                                    form.$setPristine();
                                    form.$setUntouched();
                                }
                                $scope.searchRelations = function(key){
                                    if(key.length>3){
                                        var params = {};
                                        params.crm_contact_id = $rootScope.crm_contact_id;
                                        params.search_key = key;
                                        params.relation_type = 'children';
                                        return crmContactService.getRelationsList(params).then(function(result){
                                            return result.data;
                                        });
                                    }
                                }
                                $scope.onSelect = function (data){
                                    $scope.showForm = true;
                                    $scope.new_contact={};
                                    $scope.new_contact.relation_type='spouse';
                                    $scope.new_contact.client_first_name = data.first_name;
                                    $scope.new_contact.client_last_name = data.last_name;
                                    $scope.new_contact.client_email = data.email;
                                    $scope.new_contact.crm_contact_id = data.id_crm_contact;
                                    if(data.id_crm_contact>0) $scope.new_contact.child_contact_id = data.id_crm_contact;
                                    $scope.new_contact.crm_contact_type_id = data.crm_contact_type_id;
                                    $scope.new_contact.crm_contact_type_name = data.crm_contact_type_name;
                                    $scope.new_contact.client_gender = data.data.client_gender;
                                    $scope.new_contact.client_date_of_birth = data.data.client_date_of_birth;
                                }
                                $scope.save=function(data, status){
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.form_id = $scope.formId;
                                    data.save_create = status;
                                    if(data.child_contact_id=='')
                                        delete data.child_contact_id;
                                    crmContactService.saveRelation(data).then(function(result){
                                        if(result.status){
                                            $scope.getRelationsListData('spouse,children');
                                            $rootScope.toast('Success',result.message);
                                            $scope.cancel();
                                        }
                                        else  $rootScope.toast('Error',result.error,'error');
                                    });
                                };

                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.deleteContactChildren=function(row){
                        if($rootScope.confirmMsg()){
                            crmContactService.deletePersonalInformation(row.id_crm_contact_relation).then(function (result) {
                                if (result.status) {
                                    $scope.getPersonalInformation();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.getPersonalInformation();
                    $scope.deletePersonalInformation=function(row){
                        if($rootScope.confirmMsg()){
                            crmContactService.deletePersonalInformation(row.id_crm_contact_relation).then(function (result) {
                                if (result.status) {
                                    $scope.getRelationsListData('spouse,children');
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    break;
                case 'reference_guarantor':
                    console.log('reference_guarantor');
                    $scope.contactRefGua = {};
                    /*$scope.new_contact = {};*/
                    $scope.getRefrenceGuarantor=function(){
                        crmContactService.getRefrenceGuarantor({'crm_contact_id':$rootScope.currentContactViewId, 'crm_company_id':$rootScope.id_company, 'relation_type':'reference_guarantor'}).then(function (result) {
                            $scope.contactRefGua = result.data;
                            $scope.contactRefGuaList = result.data.relations;
                        });
                    };
                    $scope.getRefrenceGuarantor();
                    $scope.addContactRefGua=function(row) {
                        $scope.selectedRow = row;
                        var modalInstance = $uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-referance-guarantor.html',
                            scope: $scope,
                            controller: function ($scope, $uibModalInstance, crmCompanyService, crmContactService, item, Upload) {
                                $scope.contactRefGua = {};
                                $scope.new_contact = {};
                                $scope.select = {};
                                $scope.new_contact.client_gender = 'Male';
                                $scope.cancel = function () {
                                    $scope.new_contact1 = {};
                                    $uibModalInstance.dismiss();
                                }
                                $scope.showForm = false;
                                $scope.new_contact1 = {};
                                $scope.new_contact1.client_gender = 'Male';
                                $scope.onSelect = function (data){
                                    $scope.new_contact1 = {};
                                    $scope.showForm = true;
                                    $scope.new_contact1.client_first_name = data.data.client_first_name;
                                    $scope.new_contact1.client_last_name = data.data.client_last_name;
                                    $scope.new_contact1.client_gender = data.data.client_gender;
                                   if(data.data.client_date_of_birth) $scope.new_contact1.client_date_of_birth = new Date(data.data.client_date_of_birth);
                                    $scope.new_contact1.client_email = data.email;
                                    $scope.new_contact1.child_contact_id = data.id_crm_contact;
                                    $scope.new_contact1.crm_contact_type_id = data.crm_contact_type_id;
                                    $scope.new_contact1.crm_contact_type_name = data.crm_contact_type_name;
                                }
                                $scope.newContact = false;
                                $scope.seletedReferenceType = function(type,form){
                                   if(type ==0){
                                       $scope.new_contact1 = {};
                                       form.$setPristine();
                                       form.$setUntouched();
                                       $scope.existing = true;
                                       if(!angular.isUndefined($scope.search))$scope.search.name = '';
                                   }
                                    else {
                                       $scope.new_contact1 = {};
                                       $scope.new_contact1.client_gender = 'Male';
                                       $scope.new_contact1.crm_contact_type_id = '1';
                                       $scope.existing = false;
                                       $scope.newContact = true;
                                       form.$setPristine();
                                       form.$setUntouched();
                                   }
                                }
                                $scope.saveContactRefGua = function (data,save_type) {
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.form_id = $scope.formId;
                                    data.form_key = $rootScope.contactParames.form_key;
                                    data.contact_relation_id = $scope.formId;
                                    data.relation_type = 'reference_guarantor';
                                    data.save_create = save_type;
                                    if(!$scope.showForm){
                                        data.child_contact_id = $scope.new_contact1.child_contact_id;
                                    }
                                    Upload.upload({
                                        url: API_URL + 'Crm/contactRelationalInformation',
                                        data: data,
                                    }).then(function (result) {
                                        if ( result.data.status) {
                                            $rootScope.toast('Success', result.data.message);
                                            $scope.cancel();
                                            $scope.getRefrenceGuarantor();
                                        } else {
                                            $rootScope.toast('Error', result.data.error, 'error');
                                        }
                                    });
                                }
                            },resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                }
                            }
                        });
                    }
                    $scope.searchRefrenceGuarantor = function(key){
                        if(key.length > 3){
                            var params = {};
                            params.crm_contact_id = $rootScope.crm_contact_id;
                            params.search_key = key;
                            params.relation_type = 'reference_guarantor';
                            return crmContactService.getRelationsList(params).then(function(result){
                                return result.data;
                            });
                        }
                    }
                    $scope.deleteRefrenceGuarantor=function(id){
                        if($rootScope.confirmMsg()){
                            crmContactService.deleteRefrenceGuarantor(id).then(function (result) {
                                if (result.status) {
                                    $scope.getRefrenceGuarantor();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    break;
                case 'assets_and_liabilities':
                    console.log('assets_and_liabilities');
                    $scope.contactTangible = {};
                    $scope.contactLiabilities = {};
                    $rootScope.getTangibleLiabilities=function(){
                        crmContactService.getTangibleLiabilities({'crm_contact_id':$rootScope.currentContactViewId, 'crm_company_id':$rootScope.id_company}).then(function (result) {
                            $scope.contactTangible = result.data.assets;
                            $scope.contactLiabilities = result.data.liabilities;
                        });
                    };
                    $scope.getIncomeAndExpenses=function(){
                        crmContactService.getIncomeAndExpense({'crm_contact_id':$rootScope.currentContactViewId, 'crm_company_id':$rootScope.id_company}).then(function (result) {
                            $scope.contactIncome = result.data.income;
                            $scope.contactExpenses = result.data.expenses;
                        });
                    };
                    $scope.pageInit=function(){
                        $scope.getIncomeAndExpenses();
                        $scope.getTangibleLiabilities();
                    }
                    $scope.addTangible=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-tangible-assets.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload,currency){
                                $scope.title='Add';
                                $scope.field = {};
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }

                                $scope.currecnyList = {};
                                for(var a in currency){
                                    if(currency[a]['id_company_currency']){
                                        $scope.currecnyList[a] = currency[a];
                                    }
                                }

                                $scope.getCollateralList=function(search_key){
                                    $scope.visibleAddButton=false;
                                    $scope.field.collateral_id = false;
                                    return crmContactService.getTangibleCollateral({
                                        'crm_contact_id': $rootScope.crm_contact_id,
                                        'crm_company_id': $rootScope.id_company,
                                        'search_key': search_key
                                    }).then(function(result){
                                        return result.data;
                                    });
                                };
                                $scope.save=function(data){
                                    $scope.isDisabled = true;
                                    if(data.in_collateral_database==1 && !data.collateral_id){
                                        alert('Please select Collateral');
                                        return;
                                    }
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.form_id = $scope.formId;
                                    data.form_key = $rootScope.contactParames.form_key;
                                    Upload.upload({
                                        url: API_URL + 'Crm/contactAssets',
                                        data: data
                                    }).then(function (result) {
                                        if (result.data.status) {
                                            $rootScope.getTangibleLiabilities();
                                            $rootScope.toast('Success', result.data.message);
                                            $scope.cancel();
                                        } else {
                                            $rootScope.toast('Error', result.data.error, 'error');
                                        }
                                    });
                                };
                                $scope.onSelect=function($item,$model,$label){
                                    $scope.visibleAddButton=true;
                                    $scope.selectedItem=$item;
                                    $scope.visibleAddButton=false;
                                    $scope.visibleDesignationDropDown=true;
                                    $scope.field.collateral_id = $item.id_collateral;
                                };
                                $scope.visibleDesignationDropDown=false;

                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                },
                                currency: function () {
                                    return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                                        return $result.data;
                                    });
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.addLiabilities=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-liabilities.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload,currency){
                                $scope.title='Add';
                                $scope.field ={};
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }
                                $scope.currecnyList = {};
                                for(var a in currency){
                                    if(currency[a]['id_company_currency']){
                                        $scope.currecnyList[a] = currency[a];
                                    }
                                }

                                $scope.save=function(data){
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.form_id = $scope.formId;
                                    crmContactService.saveLiabilities(data).
                                    then(function (result) {
                                        if (result.status) {
                                            $rootScope.getTangibleLiabilities();
                                            $scope.cancel();
                                            $rootScope.toast('Success', result.message);
                                        } else {
                                            $rootScope.toast('Error', result.error, 'error');
                                        }
                                    });
                                    //
                                };
                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                },
                                currency: function () {
                                    return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                                        return $result.data;
                                    });
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.addIncome=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-income.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload,currency){
                                $scope.title='Add';
                                $scope.field = {};
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }

                                $scope.currecnyList = {};
                                for(var a in currency){
                                    if(currency[a]['id_company_currency']){
                                        $scope.currecnyList[a] = currency[a];
                                    }
                                }

                                $scope.save=function(data){
                                    $scope.isDisabled = true;
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.form_id = $scope.formId;
                                    data.form_key = $rootScope.contactParames.form_key;
                                    data.type = 'income';
                                    crmContactService.saveIncomeOrExpense(data).then(function(result){
                                        if (result.status) {
                                            $scope.getIncomeAndExpenses();
                                            $rootScope.toast('Success', result.message);
                                            $scope.cancel();
                                        } else {
                                            $rootScope.toast('Error', result.error, 'error');
                                        }
                                    });
                                };
                                $scope.onSelect=function($item,$model,$label){
                                    $scope.visibleAddButton=true;
                                    $scope.selectedItem=$item;
                                    $scope.visibleAddButton=false;
                                    $scope.visibleDesignationDropDown=true;
                                    $scope.field.collateral_id = $item.id_collateral;
                                };
                                $scope.visibleDesignationDropDown=false;

                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                },
                                currency: function () {
                                    return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                                        return $result.data;
                                    });
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.addExpenses=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-expense.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload,currency){
                                $scope.title='Add';
                                $scope.field = {};
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }

                                $scope.currecnyList = {};
                                for(var a in currency){
                                    if(currency[a]['id_company_currency']){
                                        $scope.currecnyList[a] = currency[a];
                                    }
                                }

                                $scope.save=function(data){
                                    $scope.isDisabled = true;
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.crm_company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    data.form_id = $scope.formId;
                                    data.form_key = $rootScope.contactParames.form_key;
                                    data.type = 'expense';
                                    crmContactService.saveIncomeOrExpense(data).then(function(result){
                                        if (result.status) {
                                            $scope.getIncomeAndExpenses();
                                            $rootScope.toast('Success', result.message);
                                            $scope.cancel();
                                        } else {
                                            $rootScope.toast('Error', result.error, 'error');
                                        }
                                    });
                                };
                                $scope.onSelect=function($item,$model,$label){
                                    $scope.visibleAddButton=true;
                                    $scope.selectedItem=$item;
                                    $scope.visibleAddButton=false;
                                    $scope.visibleDesignationDropDown=true;
                                    $scope.field.collateral_id = $item.id_collateral;
                                };
                                $scope.visibleDesignationDropDown=false;

                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                },
                                currency: function () {
                                    return crmProjectService.currency.get({'company_id': $rootScope.id_company}).then(function ($result) {
                                        return $result.data;
                                    });
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.deleteTangible=function(row){
                        if($rootScope.confirmMsg()){
                            crmContactService.deleteTangible(row.id_tangible_assets).then(function (result) {
                                if (result.status) {
                                    $rootScope.getTangibleLiabilities();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.deleteLiabilities=function(row){
                        if($rootScope.confirmMsg()){
                            crmContactService.deleteLiabilities(row.id_liabilities).then(function (result) {
                                if (result.status) {
                                    $rootScope.getTangibleLiabilities();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    $scope.deleteIncomeOrExpense=function(row){
                        if($rootScope.confirmMsg()){
                            crmContactService.deleteIncomeOrExpense(row.id_contact_income_expense_id).then(function (result) {
                                if (result.status) {
                                    $scope.getIncomeAndExpenses();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }
                    };
                    break;
                default:
                    $scope.proofList = [];
                    $scope.getProofList = function (params) {
                        masterService.masterList(params).then(function (result) {
                            if (result.status)
                                $scope.proofList  = result.data;
                        })
                    }
                    $scope.getContactType = function(){
                        crmContactService.crmContactType().then(function (result) {
                            $scope.contactTypes = result.data;
                        });
                    }
                    $scope.tag='';
                    $scope.knowledgeData = [];
                    $scope.years = [];
                    var d = new Date();
                    for(var i=0;i<30;i++) {
                        var obj = {};

                        obj.value = (d.getFullYear() - i)+'';
                        obj.id = obj.value;
                        $scope.years.push(obj);
                    }


                    $scope.submitForm = function ($inputData, $form_data, form) {

                        $scope.submitStatus = true;
                        if (form.$valid) {
                            $inputData.company_id = $rootScope.id_company;
                            $inputData.form_id = $scope.formId;
                            $inputData.user_id = $rootScope.userId;
                            if ($rootScope.crm_contact_id != undefined) {
                                $inputData.crm_contact_id = $rootScope.crm_contact_id;
                            }
                            if($inputData.client_phone_number1){
                                $inputData.client_phone_number = $inputData.client_phone_number_isd+' '+$inputData.client_phone_number1;
                            }
                            if($inputData.client_alternative_phone_number1){
                                $inputData.client_alternative_phone_number = $inputData.client_alternative_phone_number_isd+' '+$inputData.client_alternative_phone_number1;
                            }
                            crmContactService.saveApplicant($inputData).then(function (result) {
                                if (result.status) {
                                    $rootScope.toast('Success', result.message);
                                    $scope.acualObj = angular.copy($scope.contact);
                                    $scope.getContactInfo();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            })
                        }
                    };

                    /*Identity widget*/
                    $scope.getidentityDocumentList= function(){
                        crmContactService.getIdentityDocumentList({'crm_contact_id':$rootScope.currentContactViewId}).then(function (result) {
                            if (result.status) {
                                $scope.identityDetails = result.data.data;
                            }
                        });
                    };
                    $scope.deleteIdentityDoc = function(data){
                        if($rootScope.confirmMsg()){
                            crmContactService.deleteIdentityDocument(data.id_contact_identification).then(function (result) {
                                if (result.status) {
                                    $scope.getidentityDocumentList();
                                    $rootScope.toast('Success', result.message);
                                } else {
                                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                }
                            });
                        }

                    };
                    $scope.AddIdentityDocument=function(row){
                        $scope.selectedRow=row;
                        var modalInstance=$uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'add-identity-document.html',
                            scope: $scope,
                            controller: function($scope,$uibModalInstance,crmCompanyService,item,Upload){
                                $scope.proofList = [];
                                $scope.getIdentityList = function (params) {
                                    masterService.masterList(params).then(function (result) {
                                        if (result.status)
                                            $scope.proofList  = result.data;
                                    });
                                };
                                $scope.title='Add';
                                if(item){
                                    $scope.title='Edit';
                                    $scope.field=item;
                                }
                                $scope.uploadFiles = function (file) {
                                    if (file != null && file != '') {
                                        $scope.file = file;
                                    } else {
                                        $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
                                    }
                                };

                                $scope.save=function(data){
                                    data.crm_contact_id = $rootScope.crm_contact_id;
                                    data.company_id = $rootScope.id_company;
                                    data.created_by = $rootScope.userId;
                                    if(item){
                                        data.id_contact_identification = item.id_contact_identification;
                                    }
                                    if(!data.expiry_date){
                                        delete data.expiry_date;
                                    }
                                    crmContactService.saveIdentityDocument(data).then(function (result) {
                                        if (result.status) {
                                            $scope.getidentityDocumentList();
                                            $uibModalInstance.dismiss('cancel');
                                            $rootScope.toast('Success', result.message);
                                        } else {
                                            $rootScope.toast('Error', result.error, 'error', $scope.sector);
                                        }
                                    });
                                };

                                $scope.cancel=function(){
                                    $uibModalInstance.dismiss('cancel');
                                };
                            },
                            resolve: {
                                item: function(){
                                    if($scope.selectedRow){
                                        return $scope.selectedRow;
                                    }
                                }
                            }
                        });
                        modalInstance.result.then(function(){
                            $state.reload();
                        },function(){
                        });
                    };
                    $scope.getidentityDocumentList();
                    /*End Identity widget*/


                    $scope.verifyEmail = function(objData){
                        var data = {};
                        data.type = 'contact';
                        data.module_id = objData.id_crm_contact;
                        data.email = objData.email;
                        crmContactService.emailVerify(data).then(function (result) {
                            if (result.status) {
                                $rootScope.toast('Success', result.message);
                                $scope.emailVerifyModal();
                            } else {
                                //$rootScope.toast('Error', result.error, 'error', $scope.sector);
                                $rootScope.toast('Error',result.message,'error');
                            }
                        });
                    };
                    $scope.emailVerifyModal = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            windowClass: 'my-class',
                            backdrop: 'static',
                            keyboard: false,
                            scope: $scope,
                            openedClass: 'right-panel-modal modal-open',
                            templateUrl: 'partials/email-verify.html',
                            controller: function ($scope, $uibModalInstance, crmContactService) {
                                $scope.verify = function (data) {
                                    $scope.submitStatus = true;
                                    var fields = {
                                        'module_id': $scope.cInfo.id_crm_contact,
                                        'email': $scope.cInfo.email,
                                        'code': data.code,
                                        'type': 'contact'
                                    };
                                    crmContactService.verifyEmailCode(fields).then(function (result) {
                                        if ($rootScope.toastMsg(result)) {
                                            $scope.cancel();
                                            $scope.getContactInfo();
                                        }
                                    });
                                };
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                            }
                        });
                        modalInstance.result.then(function () {
                        }, function () {
                            //$state.reload();
                        });
                    };
                    break;
            }
        }
        $scope.init();
    })
    .controller('ContactCompleteViewCtrl', function ($rootScope, $scope, crmContactService, $state, $uibModal, crmCompanyService, $stateParams, decodeFilter, $q, anchorSmoothScroll, $location) {
        $scope.abc = function (abc) {
            var name=[];
            name.push(abc);
            console.log('name',name);
        }
        $scope.gotoElement = function (eID) {
            $location.hash();
            anchorSmoothScroll.scrollTo(eID);
        };
        $scope.settings = {
            colHeaders: true,
            autoWrapCol: true,
            readOnly: true,
            contextMenu: ['row_above', 'row_below', 'remove_row']
        };
        $rootScope.currentContactViewId = decodeFilter($stateParams.id);
        $rootScope.contactId = $rootScope.currentContactViewId;
        $rootScope.currentModuleId = 1;
        $scope.companyContactsDetails ={};
        $scope.companyContactsDetails.company_name = '';
        var service1 = crmContactService.contactDetails($rootScope.contactId, 1).then(function (result) {
            $scope.companyContactsDetails = result.data.details[0];
            $scope.companyContactForms = result.data.forms;
            $rootScope.formStatus = result.data.active_forms;

            var i= 0,j=2;
            var text='';
            $scope.companyContactForms[0].form[0].field_data[2].field_label='Full name';
            $scope.companyContactForms[0].form[0].field_data[2].field_value=$scope.companyContactForms[0].form[0].field_data[0].field_value+' '+$scope.companyContactForms[0].form[0].field_data[1].field_value+' '+$scope.companyContactForms[0].form[0].field_data[2].field_value;
            var obj={};
            var obj1={};
            obj=$scope.companyContactForms[0].form[0].field_data;

            for(i;i<=obj.length;i++){
                if(obj[j]){
                    obj1[i]=obj[j];
                    j++;
                }
            }
            $scope.companyContactForms[0].form[0].field_data = obj1 ;

        });
        $q.all([service1]).then(function () {});
        /*$scope.buildMenu = function (contactType) {
            var menu = $scope.companyContactForms;
            var newMenu = [];
            if (contactType == 1 || contactType == 2) {
                angular.forEach(menu, function (pvalue, pkey) {
                    angular.forEach(pvalue.form, function (cvalue, ckey) {
                        if (pkey == 0 && ckey == 0) {
                            newMenu.push({
                                'id_section': pvalue.id_section,
                                'section_name': pvalue.section_name,
                                'form': []
                            });
                            newMenu[0].form.push({
                                'form_name': cvalue.form_name,
                                'form_template': cvalue.form_template,
                                'id_form': cvalue.id_form,
                                'id_section': cvalue.id_section,
                                'field_data': cvalue.field_data
                            });
                        }
                    });
                });
                $scope.companyContactForms = newMenu;
            }
        }*/

        $scope.getFormData = function (formId) {
            crmContactService.contactFormDetails($rootScope.contactId, formId, 'view').then(function (result) {
                if (result.status) {
                    $scope.viewContactForm = result.data.data.form_data;
                    $scope.formName = result.data.data.form_name;
                }
            });
        };
        crmContactService.getContactInternalRatingDetails({
            'company_id': $rootScope.id_company,
            'crm_contact_id': decodeFilter($stateParams.id)
        }).then(function (result) {
            $scope.internalRatingData = result.data;
        });
    })
    .controller('ContactCreationCtrl', function ($rootScope, $scope, crmContactService, $state, $stateParams, decodeFilter, $q, encodeFilter) {
        $scope.contact = {};
        $scope.contact.date_of_birth = new Date();
        $scope.maxDate = new Date();
        $rootScope.crm_contact_id = decodeFilter($stateParams.id);
        $rootScope.module_id = 1;
        $scope.contactObj = {};
        $scope.checkForm = [];
        if ($rootScope.crm_contact_id != undefined) {
            var service1 = crmContactService.contactDetails($rootScope.crm_contact_id, 1).then(function (result) {
                $scope.companyContactsDetails = result.data.details[0];
                $scope.companyContactForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                if ($rootScope.formStatus != undefined) {
                    angular.forEach($rootScope.formStatus, function (task, index) {
                        if (task.status) {
                            $scope.checkForm.push(task.id);
                        }
                    });
                }
            });
            var service2 = crmContactService.crmContactType().then(function (result) {
                $scope.contactTypes = result.data;
            });
            $q.all([service1, service2]).then(function () {
                crmContactService.getModules($rootScope.module_id).then(function ($result) {
                    $scope.contactObj = $result.data;
                    $scope.buildMenu($scope.companyContactsDetails.crm_contact_type_id);
                });
            });
        }
        $scope.buildMenu = function (contactType) {
            var menu = $scope.contactObj;
            var newMenu = [];
            if (contactType == 1 || contactType == 2) {
                angular.forEach(menu, function (pvalue, pkey) {
                    angular.forEach(pvalue.form, function (cvalue, ckey) {
                        if (pkey == 0 && ckey == 0) {
                            newMenu.push({
                                'id_section': pvalue.id_section,
                                'section_name': pvalue.section_name,
                                'form': []
                            });
                            newMenu[0].form.push({
                                'form_name': cvalue.form_name,
                                'form_template': cvalue.form_template,
                                'id_form': cvalue.id_form,
                                'id_section': cvalue.id_section
                            });
                        }
                    });
                });
                $scope.contactObj = newMenu;
            }
        }
        $scope.getChangeContactType = function (contactId) {
            crmContactService.changeUserContactType({
                'id_crm_contact': $rootScope.crm_contact_id,
                'crm_contact_type_id': contactId
            }).then(function (result) {
                $state.reload();
            });
        }
        $scope.accordionGroups = {};
        $scope.openAccordion = function ($id) {
            $scope.accordionGroups[$id] = true;
        };
        $scope.nextFormOpen = function ($form_data) {
            var form_id = $form_data.id_form;
            var section_id = $form_data.id_section;
            $arrayData = $scope.contactObj;
            angular.forEach($arrayData, function ($section) {
                if ($section.id_section == section_id) {
                    angular.forEach($section.form, function ($form) {
                        if ($form.id_form == form_id) {
                            if ($section.form.length > $section.form.indexOf($form) + 1) {
                                $scope.loadTemplate($section.form[$section.form.indexOf($form) + 1], $section.id_section);
                                return false;
                            } else {
                                if ($arrayData[$arrayData.indexOf($section) + 1]) {
                                    if ($arrayData[$arrayData.indexOf($section) + 1].form.length > 0) {
                                        $scope.loadTemplate($arrayData[$arrayData.indexOf($section) + 1].form[0], $arrayData[$arrayData.indexOf($section) + 1].id_section);
                                        return false;
                                    }
                                } else {
                                    $state.go("contact.contact-list", {'id': encodeFilter($rootScope.crm_contact_id)});
                                }
                            }
                        }
                    })
                }
            });
        };
        $scope.percentage = 0;
        $scope.getCheckSection = function ($section_id) {
            var $form = $activeForm = 0;
            angular.forEach($rootScope.formStatus, function (task, index) {
                if (task.section_id == $section_id) {
                    $form++;
                    if (task.status == 1) {
                        $activeForm++;
                    }
                }
            });
            if ($form == $activeForm && $activeForm > 0) {
                return 'fa fa-check-circle green';
            } else if ($activeForm > 0) {
                return 'fa-exclamation-circle orange';
            } else {
                return 'fa-times-circle red';
            }
        }
        $scope.getCheck = function ($form_id) {
            if ($.inArray($form_id, $scope.checkForm) != -1) {
                return 'fa-check green';
            } else {
                return 'fa-close red';
            }
        }
        $scope.submitForm = function ($inputData, $form_data, form) {
            $scope.submitStatus = true;
            if (form.$valid) {
                $inputData.company_id = $rootScope.id_company;
                $inputData.form_id = $form_data.id_form;
                $inputData.user_id = $rootScope.userId;
                if ($rootScope.crm_contact_id != undefined) {
                    $inputData.crm_contact_id = $rootScope.crm_contact_id;
                }
                crmContactService.saveApplicant($inputData).then(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $rootScope.crm_contact_id = result.data.crm_contact_id;
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.checkForm.push($form_data.id_form);
                        $scope.nextFormOpen($form_data);
                    } else {
                        $rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;
        $scope.db = {items: [[]]};
        $scope.settings = {colHeaders: true, autoWrapCol: true, contextMenu: ['row_above', 'row_below', 'remove_row']};
        $scope.anotherOptions = {
            barColor: '#02B000',
            trackColor: '#f9f9f9',
            scaleColor: '#dfe0e0',
            scaleLength: 5,
            lineCap: 'round',
            lineWidth: 3,
            size: 50,
            rotate: 0,
            animate: {
                duration: 1000,
                enabled: true
            }
        };
        $scope.checkFieldEdited = function (form) {
            $scope.myForm = form;
            angular.forEach($scope.myForm, function (value, key) {
                if (key[0] == '$') return;
            });
        };
        $scope.loadTemplate = function (form, id_section) {
            $scope.submitStatus = false;
            $scope.formData = form;
            $scope.formData.id_section = id_section;
            if ($rootScope.crm_contact_id != undefined && $rootScope.id_company != undefined) {
                crmContactService.contactFormDetails($rootScope.crm_contact_id, form.id_form, 'edit').then(function (result) {
                    if (result.status) {
                        $scope.contact = result.data.data;
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.contact_created_date = result.data.contact_details.created_date_time;
                        $scope.first_name = result.data.contact_details.first_name;
                        $scope.last_name = result.data.contact_details.last_name;
                        $scope.email = result.data.contact_details.email;
                        $scope.contact_type = $scope.crm_contact_type_id = result.data.contact_details.crm_contact_type_id;
                    }
                });
            }
        }
    })
    .controller('mapCtrl', function (NgMap, $scope,$uibModal) {
        $scope.$watch('contact',function(newVal, oldVal) {
            if(newVal.client_latitude && newVal.client_langitude){
                $scope.latitude =   $scope.contact.client_latitude;
                $scope.langitude = $scope.contact.client_langitude;
                $scope.mapAddress = ""+newVal.client_latitude+","+newVal.client_langitude;
            }else{
                $scope.mapAddress = newVal.client_city+","+newVal.client_country;
            }
            if(oldVal.client_country && newVal.client_country != oldVal.client_country){
                $scope.mapAddress = newVal.client_country;
            }

        },true);
        $scope.openMapModel = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope:$scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl:'partials/map-modal.html',
                controller: function ($scope, $uibModalInstance,$timeout) {
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.showMap = false;
                    $timeout(function(){
                        $scope.showMap = true;
                    })
                    $scope.getMap = function () {
                        var marker =  this;
                        $scope.contact.client_latitude = marker.getPosition().lat();
                        $scope.contact.client_langitude = marker.getPosition().lng();
                        $scope.latitude =   $scope.contact.client_latitude;
                        $scope.langitude =$scope.contact.client_langitude;
                        NgMap.getMap().then(function (map) {
                            map.getCenter().lat();
                            map.getCenter().lng();
                        });
                    }
                }
            });
        }
    })
    .controller('contactInternalRatingCtrl', function ($scope, $rootScope, crmContactService) {
        console.log('contactInternalRatingCtrl');
        $scope.riskCategory = [];
        $scope.getContactInfo();
        crmContactService.riskAssessment.get({
            'company_id': $rootScope.id_company,
            'crm_contact_id': $rootScope.crm_contact_id,
            'type':'contact'
        }).then(function (result) {
            if (result.status) {
                $scope.riskCategory = result.data;
            } else {
                $rootScope.toast('Error', result.error, 'error');
            }
        });
        $scope.convertToInt = function (value) {
            return parseInt(value);
        };
        $scope.subAttrFactor = 0;
        $scope.dropdownSelected = function (attribute) {
            angular.forEach(attribute.items, function (item) {
                if (item.selected == 1) {
                    attribute.selectedItem = item;
                }
            })
        }
        $scope.saveAttribute = function (data) {
            var param = {};
            param.company_id = $rootScope.id_company;
            param.crm_contact_id = $rootScope.crm_contact_id,
            param.user_id = $rootScope.userId;
            param.form_key = 'internal_rating';
            param.data = [];
            angular.forEach(data, function (item) {
                if (item.selectedItem != undefined) {
                    var obj = {};
                    obj.grade = item.grade;
                    obj.id_risk_category = item.id_risk_category;
                    obj.id_risk_category_item = item.selectedItem.id_risk_category_item;
                    obj.risk_category_id = item.id_risk_category;
                    param.data.push(obj);
                }
            })
            //console.log(param);
            //return false;
            crmContactService.riskAssessment.post(param).then(function (result) {
                if (result.status) {
                    $scope.getContactInfo();
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', result.error);
                }
            })
        }
    })
/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('crm.meeting',[])
    .controller('crmMeetingCtrl',function($scope,$rootScope,crmMeetingService){})

    .controller('crmMeetingListCtrl',function($scope,$rootScope,crmMeetingService,$uibModal,meetingService){
        $scope.collateralList=[];
        $scope.filter_type='all_collateral';
        $scope.getMeetingList=function(stTable){
            stTable.company_id=$rootScope.id_company;
            if(!stTable)
                stTable=$scope.refTable;
            $scope.refTable=stTable;
            stTable.company_id=$rootScope.id_company;
            stTable.assigned_to=$rootScope.userId,stTable.created_by=$rootScope.userId;
            crmMeetingService.crm.get(stTable).then(function(result){
                $scope.meetingList=result.data.data;
                $scope.all_meatings_count = result.data.total_records;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }

        $scope.preview = function (params) {
            $scope.params = {};
            angular.copy(params,$scope.params);
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                resolve: {
                    item: function () {
                        if ($scope.params.id_meeting) {
                            return meetingService.get($scope.params).then(function ($result) {
                                return $result.data.data;
                            });
                        }
                    }
                },
                templateUrl: 'partials/crm/meeting/meeting-min-modal.html',
                controller: function ($scope, $rootScope, $uibModalInstance, item, companyService,meetingService) {
                    $scope.userList = [];
                    $scope.getUsers = function () {
                        companyService.users({'company_id': $rootScope.id_company}).then(function (result) {
                            $scope.userList = result.data;
                        });
                    }
                    $scope.minDate = $scope.minDate ? null : new Date();
                    $scope.meeting = $scope.params;
                    $scope.meeting.invitation_id = [];
                    $scope.meetingDocuments = [];
                    $scope.meetingDetails = {};
                    $scope.meetingDetails.attachment = [];
                    $scope.meeting.invitation_id = [];
                    $scope.meeting.guest = [];
                    $scope.company_id = $rootScope.id_company;
                    $scope.timeList = function () {
                        var $time = [];
                        var $mintIncrement = 10;
                        var hr = 24;
                        for (var i = 0; i < 24; i++) {
                            for (var j = 0; j < 60; j = j + $mintIncrement) {
                                var $obj = {};
                                var jVal = (j < 10) ? '0' + '' + j : j;
                                var iVal = (i < 10) ? '0' + '' + i : i;
                                $obj.id = iVal + ':' + jVal;
                                $obj.value = iVal + ':' + jVal;
                                $time.push($obj);
                            }
                        }
                        $scope.timeListData  =  $time;
                    }
                    if (item) {
                        $scope.meeting = item;
                        if (item.guest != null) {
                            $scope.meeting.guest = item.guest.split(',');
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
    })



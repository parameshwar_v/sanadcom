/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('app')
    .controller('facilityCtrl', function ($scope,$rootScope,masterService) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
             masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.accClose = function(element){
            var a = angular.element(element).triggerHandler('click');
            console.log('a',a);

            //
        }
    })

    .controller('facilityTypeCtrl', ['masterService', '$scope', '$rootScope', '$uibModal', 'companyService', 'encodeFilter', '$state', function (masterService, $scope, $rootScope, $uibModal, companyService, encodeFilter, $state) {
        $scope.facilieyType = {};
        $scope.facilityTypeList = [];
        $scope.getFacilityTypeList = function () {
            companyService.facilityType.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.facilityTypeList = result.data;
            })
        }
        $scope.getFacilityTypeList();
        $scope.goto = function (stateName, facilityId) {
            $state.go(stateName, {'facilityId': encodeFilter(facilityId)});
        }
        $scope.FTModalOpen = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                //windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'addFacilityModel.html',
                controller: function ($uibModalInstance, $scope) {
                    $scope.ftSubmit = function (facility) {
                        facility.company_id = $rootScope.id_company;
                        facility.created_by = $rootScope.userId;
                        companyService.facilityType.post(facility).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.cancel();
                                $scope.getFacilityTypeList();
                            }
                        })
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
    }])
    .controller('facilityViewCtrl', function (masterService, $scope, $rootScope, $uibModal, companyService, decodeFilter, $state, $stateParams) {
        $scope.id_company_facility = decodeFilter($stateParams.facilityId);
        $scope.currencyList = [];
        $scope.getCurrencyList = function () {
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.currencyList = result.data;
                console.log(' $scope.currencyList', $scope.currencyList);
            })
        }
        $scope.facilityInfo ={};
        $scope.getFacilityInfo = function () {
            companyService.facilityType.get({
                'company_id': $rootScope.id_company,
                'id_company_facility': $scope.id_company_facility
            }).then(function (result) {
                $scope.facilityInfo = result.data;
            })
        }
        $scope.getFacilityInfo();
    })
    .controller('facilityGeneralTabCtrl', function (masterService, $scope, $rootScope, $uibModal, companyService, decodeFilter, $state, $stateParams) {
        $scope.ftSubmit = function (data) {
            var params = {};
            params.id_company_facility = data.id_company_facility;
            params.facility_name = data.facility_name;
            params.facility_description = data.facility_description;
            params.facility_amount = data.facility_amount;
            params.facility_amount_type = data.facility_amount_type;
            params.currency_id = data.currency_id;
            params.amount_description = data.amount_description;
            params.facility_type = data.facility_type;
            params.facility_sub_type = data.facility_sub_type;
            params.facility_term = data.facility_term;
            params.facility_type_description = data.facility_type_description;
            params.facility_purpose = data.facility_purpose;
            params.facility_tenor = data.facility_tenor;
            params.facility_tenor_description = data.facility_tenor_description;
            params.facility_source_of_payment = data.facility_source_of_payment;
            params.facility_security = data.facility_security;
            params.facility_expected_start_date = data.facility_expected_start_date;
            params.facility_maturity_date = data.facility_maturity_date;
            params.withdrawl_expiry_date = data.withdrawl_expiry_date;
            params.facility_term_type = data.facility_term_type;
            params.created_by = $rootScope.userId;
            params.payment_modalities = data.payment_modalities;
            params.lender_type = data.lender_type;
            params.lender_percentage = data.lender_percentage;
            params.loan_payment_type = data.loan_payment_type;
            params.credit_risk = data.credit_risk;
            companyService.facilityType.postFacilityGeneral(params).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                }
            })
        }
        $scope.noOfTermsList = [];
        $scope.getNoOfTermsList = function () {
            masterService.masterList({
                'company_id': $rootScope.id_company,
                'master_key': 'payment_modalities'
            }).then(function (result) {
                $scope.noOfTermsList = result.data;
            })
        }
        $scope.countryList = [];
        $scope.getCountryList = function () {
            masterService.country.get().then(function (result) {
                $scope.countries = result.data;
            });
        }
        $scope.facilityTermTypeList = [];
        $scope.getFacilityTermTypeList = function () {
            masterService.masterList({
                'company_id': $rootScope.id_company,
                'master_key': 'facility_term_type'
            }).then(function (result) {
                $scope.facilityTermTypeList = result.data;
            })
        }
    })
    .controller('facilityPriceTabCtrl', function (masterService, $scope, $rootScope, $uibModal, companyService, decodeFilter, $state, $stateParams) {
        $scope.id_company_facility = decodeFilter($stateParams.facilityId);
        $scope.fieldList = [];
        $scope.field_section = 'pricing';
        $scope.timePeriod = [];
        $scope.getTimePeriod = function () {
            companyService.facilityType.timePeriod({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.timePeriod = result.data;
            })
        }
        $scope.amountType = [];
        $scope.getAmountType = function () {
            companyService.facilityType.facilityAmountType({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.amountType = result.data;
            })
        }


        $scope.getCompanyFacilityFields = function () {
            $scope.fieldList = [];
            companyService.facilityType.getCompanyFacilityFields({
                'company_id': $rootScope.id_company,
                'field_section': $scope.field_section,
                'company_facility_id': $scope.id_company_facility
            }).then(function (result) {
                angular.forEach(result.data, function (data, key) {
                    var obj = {};
                    obj.facility_field_id = data.facility_field_id;
                    obj.field_name = data.field_name;
                    obj.field_key = data.field_key;
                    obj.data = data;
                    $scope.fieldList.push(obj);
                });
            })
        }
        $scope.getCompanyFacilityFields();
        $scope.deleteCompanyFacilityFields = function (id_company_facility_field,$index) {
            var params = {};
            //params.company_id = $rootScope.id_company;
            params.id_company_facility_field = id_company_facility_field;
            if ($rootScope.confirmMsg()) {
                if(id_company_facility_field){
                    companyService.facilityType.deleteCompanyFacilityFields(params).then(function (result) {
                        if ($rootScope.toastMsg(result)) {
                            $scope.getCompanyFacilityFields();
                        }
                    })
                }else{
                    $scope.fieldList.splice($index,1);
                }

            }
        }
        $scope.priceSubmit = function (data, facility_field_id) {
            var params = data;
            params.company_facility_id = $scope.id_company_facility;
            params.facility_field_id = facility_field_id;
            params.field_section = $scope.field_section;
            params.created_by = $rootScope.userId;
            companyService.facilityType.companyFacilityFields(params).then(function (result) {

                if ($rootScope.toastMsg(result)) {
                    $scope.getCompanyFacilityFields();
                    /*var a = _.findWhere($scope.fieldList, {'field_key': result.data.field_key})
                    var indexFieldList = _.indexOf($scope.fieldList, a);
                    if (indexFieldList !== -1) {
                        $scope.fieldList[indexFieldList].data = result.data;
                    }*/
                }
            })
        }
        $scope.FDModalOpen = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                //windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'addFacilityFieldModel.html',
                controller: 'facilityTypeModalCtrl'
            });
        }
    })
    .controller('facilityTermConditionsCtrl', function (masterService, $scope, $rootScope, $uibModal, companyService, decodeFilter, $state, $stateParams) {
        $scope.id_company_facility = decodeFilter($stateParams.facilityId);
        $scope.fieldList = [];
        $scope.field_section = 'terms_conditions';

        $scope.getCompanyFacilityFields = function () {
            companyService.facilityType.getCompanyFacilityFields({
                'company_id': $rootScope.id_company,
                'field_section': $scope.field_section,
                'company_facility_id': $scope.id_company_facility
            }).then(function (result) {
                $scope.fieldList = [];
                angular.forEach(result.data, function (data, key) {
                    var obj = {};
                    obj.facility_field_id = data.facility_field_id;
                    obj.field_key = data.field_key;
                    obj.field_name = data.field_name;
                    obj.data = data;
                    $scope.fieldList.push(obj);
                });
            })
        }
        $scope.getCompanyFacilityFields();
        $scope.deleteCompanyFacilityFields = function (id_company_facility_field,$index) {
            var params = {};
            //params.company_id = $rootScope.id_company;
            params.id_company_facility_field = id_company_facility_field;
            if($rootScope.confirmMsg()){
                if(id_company_facility_field){
                    companyService.facilityType.deleteCompanyFacilityFields(params).then(function (result) {
                        if ($rootScope.toastMsg(result)) {
                            $scope.getCompanyFacilityFields();
                        }
                    })
                }else{
                    $scope.fieldList.splice($index,1);
                }
            }

        }
        $scope.tcSubmit = function (data, facility_field_id) {
            var params = data;
            params.company_facility_id = $scope.id_company_facility;
            params.facility_field_id = facility_field_id;
            params.field_section = $scope.field_section;
            params.created_by = $rootScope.userId;
            companyService.facilityType.companyFacilityFields(params).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                    $scope.getCompanyFacilityFields();
                   /* var a = _.findWhere($scope.fieldList, {'field_key': result.data.field_key})
                    var indexFieldList = _.indexOf($scope.fieldList, a);
                    if (indexFieldList !== -1) {
                        $scope.fieldList[indexFieldList].data = result.data;
                    }*/
                }
            })
        }
        $scope.FTCModalOpen = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                //windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'addFacilityFieldModel.html',
                controller: 'facilityTypeModalCtrl'
            });
        }
    })
    .controller('facilityTypeModalCtrl', function ($uibModalInstance, $scope, $rootScope, companyService) {
        var param = {};
        $scope.fFieldList = [];
        param.company_id = $rootScope.id_company;
        param.field_section = $scope.field_section;
        companyService.facilityType.getFacilityFields(param).then(function (result) {
            if (result.status) {
                angular.forEach(result.data, function (data, key) {
                    var a = _.findWhere($scope.fieldList, {'field_key': data.field_key});
                    if (!a) {
                        $scope.fFieldList.push(data);
                    }
                })

            }
        })
        $scope.ftSubmit = function (item) {

            var a = _.findWhere($scope.fieldList, {'field_key': item.field_key})
            // var indexObj = _.indexOf($scope.fieldList,a)
            if (!a) {
                var obj = {};
                obj.facility_field_id = item.id_facility_field;
                obj.field_key = item.field_key;
                obj.field_name = item.field_name;
                obj.data = {};
                $scope.fieldList.push(obj);
                $scope.cancel();
            } else {
                $rootScope.toast('Error', 'Already Exist')
            }
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('app')
    .controller('covenantCtrl', function ($scope, $rootScope, masterService) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }

        $scope.sectorsAllList = [];
        $scope.getSectorAllList = function () {
            masterService.sector.getAll().then(function (result) {
                $scope.sectorsAllList = result.data.data;
            })
        }
    })
    .controller('covenantListCtrl', function ($scope, $rootScope, masterService, $uibModal, covenantService) {
        $scope.covenantList = [];
        $scope.getCovenantList = function (tableState) {
            if(!tableState)
                tableState = $scope.refTableState;
            $scope.refTableState = tableState;
            tableState.company_id = $rootScope.id_company;
            tableState.covenant_type_id = $scope.covenant_type_id;
            covenantService.get(tableState).then(function (result) {
                $scope.covenantList = result.data.data;
               // tableState.pagination.
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/ tableState.pagination.number);
            })
        }
        $scope.deleteCovenant = function (id_covenant) {
            if ($rootScope.confirmMsg()) {
                covenantService.delete({
                    'company_id': $rootScope.id_company,
                    'covenant_id': id_covenant
                }).then(function (result) {
                    if($rootScope.toastMsg(result))
                        $scope.initial();
                })
            }
        }
        $scope.clickCovenant = function(covenant_type_id){
            $scope.covenant_type_id=covenant_type_id;
            $scope.refTableState.covenant_type_id =covenant_type_id;
            $scope.initial();
        }

        $scope.covenantTypeList = [];
        $scope.getCovenantType = function(){
            covenantService.covenantType({'company_id': $rootScope.id_company}).then(function(result){
                $scope.covenantTypeList = result.data;
            })
        }
        $scope.covenantModalOpen = function (id_covenant) {

            var modalInstance = $uibModal.open({
                animation: true,
                openedClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                scope: $scope,
                templateUrl: 'partials/admin/covenant/create-covenant-modal.html',
                resolve: {
                    item: function () {
                        if (id_covenant) {
                            return covenantService.get({
                                'company_id': $rootScope.id_company,
                                'id_covenant': id_covenant
                            }).then(function (result) {
                                return result.data;
                            })
                        }
                    }
                },
                controller: function ($uibModalInstance, $scope, covenantService, item) {
                    $scope.covenant = {};
                    $scope.covenant_tags = [];
                    $scope.actionName = 'Create';
                    if (item) {
                        $scope.actionName = 'Update';
                        $scope.covenant = item;
                        if(item.covenant_tags)
                            $scope.covenant_tags = item.covenant_tags.split(',');
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.submitCovenant = function (covenant, form) {
                        var params = covenant;
                        var covenant_tags_array = $scope.covenant_tags.map(function (tag) {
                            return tag.text;
                        });
                        params.covenant_tags = covenant_tags_array.toString();
                        params.company_id = $rootScope.id_company;
                        params.created_by = $rootScope.userId;
                        covenantService.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                form.$setPristine();
                                $scope.cancel();
                                $scope.initial();
                            }
                        })
                    }


                }
            });
            modalInstance.result.then(function () {

            }, function () {

            });

        }
        $scope.CreateCovenantModalOpen = function (id_covenant) {

            var modalInstance = $uibModal.open({
                animation: true,
                openedClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                scope: $scope,
                templateUrl: 'partials/admin/covenant/create-covenant-user.html',
               /* resolve: {
                    item: function () {
                        if (id_covenant) {
                            return covenantService.get({
                                'company_id': $rootScope.id_company,
                                'id_covenant': id_covenant
                            }).then(function (result) {
                                return result.data;
                            })
                        }
                    }
                },*/
                controller: function ($uibModalInstance, $scope, covenantService, item) {

                }
            });
            modalInstance.result.then(function () {

            }, function () {

            });

        }

        //initial call
        $scope.initial =function(){
            $scope.getCovenantList();
            $scope.getCovenantType();
        }
        $scope.getCovenantType();
    })


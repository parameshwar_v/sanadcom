/**
 * Created by RAKESH on 15-04-2016.
 */
'use strict';
angular.module('roles.module',[])
    .controller('rolesList',function($rootScope,$scope,Auth,onlineStatus,$uibModal,companyService,$q,$timeout){
        $scope.roleList = [];
        $scope.selectRow = {};
        $scope.org_role_type = [];
        $scope.mappingUsers = {};
        $scope.mappingApprovalRole = {};

        $scope.getRoleList = function(){
            companyService.role.get({'company_id':$rootScope.id_company}).then(function(result){
                $scope.roleList = result.data;
            });
            console.log('$scope.roleList',$scope.roleList);
        }

        $scope.getOrgRoleList = function(){
            companyService.companyApprovalRoles({'company_id':$rootScope.id_company}).then(function(result){
                $scope.org_role_type = result.data;
            })
        }

        $scope.getUserList = function(){
            companyService.userList({'company_id':$rootScope.id_company,'type':'admin'}).then(function(result){
                $scope.userList = result.data.data;
            });
        }

        $scope.saveMapping = function(params){
            params.company_id = $rootScope.id_company;
            params.application_role_id = $scope.selectRow.id_application_role;
            companyService.role.mapping.post(params).then(function(result){
                if(result.status){
                    if(params.company_approval_role_id!=undefined){
                        $scope.getMappingList('approval_role');
                    } else{
                        $scope.getMappingList('user');
                    }
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }

        /**
         * //get mapping user and approval role list
         * @param type:approval_role or user
         */
        $scope.getMappingList = function(type){
            var params = {};
            params.company_id = $rootScope.id_company;
            params.application_role_id = $scope.selectRow.id_application_role;
            params.type = type;
            companyService.role.mapping.get(params).then(function(result){
                if(result.status){
                    if(type=='user')
                        $rootScope.applicationUserList = result.data;
                    if(type=='approval_role')
                        $rootScope.applicationApprovalList = result.data;
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.roleDelete = function(params,type){
            companyService.role.mapping.delete({'id_application_user_role':params.id_application_user_role}).then(function(result){
                if(result.status){
                    console.log('params',params);
                    if(type=='user'){
                        $scope.getMappingList('user');
                    } else{
                        $scope.getMappingList('approval_role');
                    }
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }

        $scope.treeCheck = function (tree) {
            var setValue;
            if($scope.parentNode.checked==1){
                setValue = 1;
            }else{
                setValue = 0;
            }
            angular.forEach(tree.childs, function (item) {
                item.checked = setValue;
                angular.forEach(item.action,function(item2){
                    item2.checked = setValue;

                   /* if(item2.checked==1){
                        tree.checked = true;
                    }*/
                });

               /* angular.forEach(item.action,function(item2){
                    item.checked = false;
                    if(item2.checked==1){
                        console.log('item',item);
                        item.checked = true;
                        return;
                        //console.log('item2',item);
                    }
                });*/
                $scope.treeCheck(item);
            },tree);
        }


        $scope.parentCheck = function(data){
            $scope.parentNode = data;
            $scope.treeCheck(data)
        }


        $scope.moduleList = [];
        $scope.treeCheck2 = function (data) {
            $scope.checkAction = function(action){
                var returnValue = 0;
                angular.forEach(action.action,function(a){
                    if(a.checked==1){
                        //console.log('item',item);
                        returnValue = 1;
                        return;
                    }
                });
                return returnValue;
            }
        }
        $scope.$watch('moduleList',function(){
            $scope.treeCheck2($scope.moduleList);
        },true)

        $scope.getRolePermission = function(row){
            $scope.selectRow = row;
            $scope.getMappingList('user');
            $scope.getMappingList('approval_role');
            $scope.role_id = row.id_application_role
            $scope.approval_role_id = [];
            companyService.role.module.get({
                'application_role_id':$scope.role_id,
                'company_id':$rootScope.id_company
            }).then(function(result){
                $scope.moduleList = result.data;
            })
        }
        $scope.moduleSave = function(){
            var params = {};
            params.data = $scope.moduleList;
            params.application_role_id = $scope.role_id;
            params.company_id = $rootScope.id_company;
            companyService.role.module.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }

        $scope.addRoleModal = function(selectedItem){
            var modalInstance = $uibModal.open({
                templateUrl:'partials/admin/roles-management/add-role-model.html',
                controller:'roleCreationModal',
                backdrop:'static',
                scope:$scope,
                keyboard:false,
                resolve:{
                    selectedItem:selectedItem
                }
            });
            modalInstance.result.then(function(){

            },function(){

            });
        };
    })
    .controller('roleCreationModal',function($rootScope,$scope,$uibModalInstance,companyService,selectedItem){
        $scope.role = {};

        $scope.modalTitle = 'Add Role'
        if(selectedItem){
            angular.copy(selectedItem,$scope.role)
        }
        $scope.saveRole = function(){
            $scope.role.company_id = $rootScope.id_company;
            companyService.role.post($scope.role).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.dismiss('cancel');
                    $scope.getRoleList();
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('moduleCreation',function($rootScope,$scope,companyService,encodeFilter,$stateParams){

        $scope.role_id = encodeFilter($stateParams.roleId);

        $scope.org_role_type = $scope.moduleList = [];
        $scope.approval_role_id = [];
        companyService.companyApprovalRoles({'company_id':$rootScope.id_company}).then(function(result){
            $scope.org_role_type = result.data;
            companyService.role.module.get({
                'application_role_id':$scope.role_id,
                'company_id':$rootScope.id_company
            }).then(function(result){
                $scope.moduleList = result.data;
                $scope.approval_role_id = result.approval_role_id;
            })
        })

        $scope.moduleSave = function(){
            var params = {};
            params.data = $scope.moduleList;
            params.application_role_id = $scope.role_id;
            params.approval_role_id = $scope.approval_role_id;
            params.company_id = $rootScope.id_company;
            companyService.role.module.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }

    })

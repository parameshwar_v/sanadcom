/**
 * Created by RAKESH on 09-08-2016.
 */
angular.module('app')
    .controller('currencyCtrl',['masterService','$scope','$rootScope','$uibModal',function(masterService,$scope,$rootScope,$uibModal){
        $scope.plansList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            tableState.company_id = $rootScope.id_company;
            masterService.currency.getTable(tableState).then(function(result){
                $scope.currencyList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-currency.html',
                controller:'createCurrencyCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    },
                    Countries:function(){
                        return masterService.country.get().then(function(result){
                            return result.data;
                        });
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.delete=function($mode,$editId){
            //alert($editId);
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('createCurrencyCtrl',function($scope,$uibModalInstance,items,Countries,masterService,$state,$rootScope){
        $scope.currency={};
        $scope.currency.country_flag='';
        $scope.Countries=Countries;
        $scope.title='Create';
        $scope.btn='Add';
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.currency.getAll({'id_currency':items.editId,'company_id':$rootScope.id_company}).then(function(result){
                $scope.currency=result.data;
            })
        }
        $scope.ok=function($postData){
            if($postData){
                // check to make sure the form is completely valid
                if($scope.form.$valid){
                    var param={}
                    param.updated_by = $rootScope.userId;
                    param.country_id=$postData.country_id;
                    param.currency_code=$postData.currency_code;
                    param.currency_symbol=$postData.currency_symbol;
                    param.rounding_option=$postData.rounding_option;
                    param.formatting=$postData.formatting;
                    param.company_id = $rootScope.id_company;
                    if($postData.id_currency!=undefined){
                        param.id_currency=$postData.id_currency;
                    }
                    masterService.currency.post($scope.currency.country_flag,param).then(function(result){
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            $uibModalInstance.close();
                        }else{
                            $rootScope.toast('Error',result.error,'error');
                        }
                    });
                }
            }
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.uploadCountryFlag=function(file){
            $scope.currency.country_flag=file;
            $scope.trash=true;
        };
        $scope.uploadCountryFlagRemove=function(){
            $scope.currency.country_flag='';
            $scope.trash=false;
        };
    })

    .controller('idProofCtrl',['masterService','$scope','$rootScope','$uibModal',function(masterService,$scope,$rootScope,$uibModal){
        $scope.plansList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            tableState.company_id = $rootScope.id_company;
            masterService.currency.getTable(tableState).then(function(result){
                $scope.currencyList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-currency.html',
                controller:'createIdProofCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    },
                    Countries:function(){
                        return masterService.country.get().then(function(result){
                            return result.data;
                        });
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.delete=function($mode,$editId){
            //alert($editId);
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('createIdProofCtrl',function($scope,$uibModalInstance,items,Countries,masterService,$state,$rootScope){
        $scope.currency={};
        $scope.currency.country_flag='';
        $scope.Countries=Countries;
        $scope.title='Create';
        $scope.btn='Add';
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.currency.getAll({'id_currency':items.editId,'company_id':$rootScope.id_company}).then(function(result){
                $scope.currency=result.data;
            })
        }
        $scope.ok=function($postData){
            if($postData){
                // check to make sure the form is completely valid
                if($scope.form.$valid){
                    var param={}
                    param.updated_by = $rootScope.userId;
                    param.country_id=$postData.country_id;
                    param.currency_code=$postData.currency_code;
                    param.currency_symbol=$postData.currency_symbol;
                    param.rounding_option=$postData.rounding_option;
                    param.formatting=$postData.formatting;
                    param.company_id = $rootScope.id_company;
                    if($postData.id_currency!=undefined){
                        param.id_currency=$postData.id_currency;
                    }
                    masterService.currency.post($scope.currency.country_flag,param).then(function(result){
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            $uibModalInstance.close();
                        }else{
                            $rootScope.toast('Error',result.error,'error');
                        }
                    });
                }
            }
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.uploadCountryFlag=function(file){
            $scope.currency.country_flag=file;
            $scope.trash=true;
        };
        $scope.uploadCountryFlagRemove=function(){
            $scope.currency.country_flag='';
            $scope.trash=false;
        };
    })

    .controller('sectorCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.displayed=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||10;  // Number of entries showed per page.
            masterService.sector.getPage(start,number,tableState).then(function(result){
                $scope.displayed=result.data;
                tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-sector.html',
                controller:'CreateSectorCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateSectorCtrl',function($scope,$uibModalInstance,items,masterService,$rootScope){
        $scope.parentSectorsList=[];
        masterService.sector.getAll().then(function(result){
            $scope.parentSectorsList=result.data.data;
        });
        $scope.title='Create';
        $scope.btn='Add';
        $scope.sector={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.sector.getById(items.editId).then(function(result){
                $scope.sector=result.data;
            })
        }
        $scope.sector={"sector_name":"","parent_sector_id":""};
        $scope.submitSectorForm=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_sector==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.update=function($postData){
            masterService.sector.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                }
            });
        };
        $scope.add=function($postData){
            masterService.sector.add($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('countryCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.countriesList=[];
        $scope.callServer=function callServer(tableState){
            $scope.isLoading=true;
            $scope.dtableState=tableState;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.country.getPage(start,number,tableState).then(function(result){
                $scope.countriesList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-country.html',
                controller:'CreateCountryCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateCountryCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.country={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.country.getById(items.editId).then(function(result){
                $scope.country=result.data;
            })
        }
        $scope.country={"country_name":'',"country_code":'', "country_visible":'1'};
        $scope.save=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_country==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            masterService.country.createCountry($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.country);
                }
            });
        };
        $scope.update=function($postData){
            masterService.country.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.country);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('branchTypeCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.countriesList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||10;  // Number of entries showed per page.
            tableState.company_id=0;
            masterService.branchType.getPage(tableState).then(function(result){
                $scope.countriesList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-branch-type.html',
                controller:'CreateBranchTypeCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateBranchTypeCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.branch={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.branchType.getById(items.editId).then(function(result){
                $scope.branch=result.data;
            })
        }
        $scope.branch={"branch_type_name":'',"branch_type_code":''};
        $scope.save=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_branch_type==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            masterService.branchType.createBranchType($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update=function($postData){
            masterService.branchType.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('ApprovalRoleCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.approvalRolesList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            tableState.company_id=0;
            masterService.approvalRole.getPage(tableState).then(function(result){
                $scope.approvalRolesList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-approval-role.html',
                controller:'CreateApprovalRoleCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateApprovalRoleCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.approvalRole={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.approvalRole.getById(items.editId).then(function(result){
                $scope.approvalRole=result.data;
            })
        }
        $scope.approvalRole={"approval_name":''};
        $scope.save=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_approval_role==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            masterService.approvalRole.createApprovalRole($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.approvalRole);
                }
            });
        };
        $scope.update=function($postData){
            masterService.approvalRole.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.approvalRole);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('bankCategoryCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.bankCategoryList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||10;  // Number of entries showed per page.
            masterService.bankCategory.getPage(start,number,tableState).then(function(result){
                $scope.bankCategoryList=result.data;
                tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-bank-category.html',
                controller:'CreateBankCategoryCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateBankCategoryCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.bankCategory={};
        $scope.title='Create';
        $scope.btn='Add';
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.bankCategory.getBankCategory(items.editId).then(function(result){
                $scope.bankCategory=result.data;
            })
        }
        $scope.bankCategory={"bank_category_name":''};
        $scope.ok=function($postData){
            if($postData){
                // check to make sure the form is completely valid
                if($scope.bankCategoryForm.$valid){
                    $postData.id_company = $rootScope.id_company;
                    $postData.updated_by = $rootScope.userId;
                    if($postData['id_bank_category']){
                        masterService.bankCategory.updateBankCategory($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            }else{
                                $rootScope.toast('Error',result.error,'error',$scope.bankCategory);
                            }
                        });
                    }
                    else{
                        masterService.bankCategory.createBankCategory($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            }else{
                                $rootScope.toast('Error',result.error,'error',$scope.bankCategory);
                            }
                        });
                    }
                }
            }
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('riskCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.riskList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.risk.get(start,number,tableState).then(function(result){
                $scope.riskList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};

            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-risk.html',
                controller:'CreateRiskCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateRiskCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.risk={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.risk.getById(items.editId).then(function(result){
                $scope.risk=result.data;
            })
        }
        masterService.risk.getAll().then(function(result){
            $scope.parent=result.data;
        });
        $scope.risk={"risk_name":'',"risk_description":'','parent_id':''};
        $scope.save=function($postData){
            if($postData.id_risk_type==undefined){
                delete $postData.id_risk_type;
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            masterService.risk.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            masterService.risk.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('socialCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.socialList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.social.get(start,number,tableState).then(function(result){
                $scope.socialList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-social.html',
                controller:'CreateSocialCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateSocialCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.social={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.social.getById(items.editId).then(function(result){
                $scope.social=result.data;
            })
        }
        $scope.social={"business_social_network":'',"business_social_network_description":''};
        $scope.save=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_business_social_network_type==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            masterService.social.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update=function($postData){
            masterService.social.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('contactCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.contactList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.contact.get(start,number,tableState).then(function(result){
                $scope.contactList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-contact.html',
                controller:'createContactCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('createContactCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.contact={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.contact.getById(items.editId).then(function(result){
                $scope.contact=result.data;
            })
        }
        $scope.contact={"business_contact":'',"business_contact_description":''};
        $scope.save=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_business_contact_type==undefined){
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            masterService.contact.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update=function($postData){
            masterService.contact.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('languageCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.contactList=[];
        $scope.callServer=function callServer(tableState){
            $scope.dtableState=tableState;
            $scope.isLoading=true;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.language.getTable(start,number,tableState).then(function(result){
                $scope.languageList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };
        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.open=function($mode,$editId){
            var $item={};
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-language.html',
                controller:'createlanguageCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
        /*$scope.plansList=[];
         $scope.callServer=function callServer(tableState){
         $scope.dtableState=tableState;
         $scope.isLoading=true;
         var pagination=tableState.pagination;
         var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
         var number=pagination.number||2;  // Number of entries showed per page.
         masterService.language.getTable(start,number,tableState).then(function(result){
         $scope.languageList=result.data.data;
         tableState.pagination.numberOfPages=Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
         $scope.isLoading=false;
         });
         };
         //add new row in modal
         $scope.animationsEnabled=true;
         $scope.open=function($mode,$editId){
         var $item={};
         if($mode=='edit'){
         $item.type='edit';
         $item.editId=$scope.editId;
         }
         var modalInstance=$uibModal.open({
         animation:$scope.animationsEnabled,
         templateUrl:'partials/admin/master-data/create-language.html',
         controller:'createlanguageCtrl',
         resolve:{
         items:function(){
         return $item;
         }
         }
         });
         modalInstance.result.then(function(){
         $scope.selected={};
         $scope.selectedCount=0;
         $scope.callServer($scope.dtableState);
         },function(){
         });
         };
         $scope.delete=function($mode,$editId){
         var r=confirm('Are you sure you want to delete!');
         if(r){
         masterService.language.delete({'id_language':$editId}).then(function(result){
         $scope.callServer($scope.dtableState);
         $scope.selected={};
         $scope.selectedCount=0;
         })
         }
         };
         $scope.toggleAnimation=function(){
         $scope.animationsEnabled= !$scope.animationsEnabled;
         };
         //Check updated
         $scope.editId=0;
         $scope.selected={};
         $scope.updateSelected=function(id, name){
         var count=0;
         for(x in $scope.selected){
         if($scope.selected[x]){
         $scope.editId=x;
         $scope.selectedText=name;
         count++;
         }
         }
         $scope.selectedCount=count;
         /!*$scope.editId=id;
         $scope.selectedText=name;
         $scope.open('edit',id);*!/
         };*/
    }])
    .controller('createlanguageCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.language={};
        $scope.language.flag='';
        $scope.title='Create';
        $scope.btn='Add';
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.language.getAll({'id_language':items.editId}).then(function(result){
                $scope.language=result.data;
            })
        }
        $scope.ok=function($postData){
            if($postData){
                // check to make sure the form is completely valid
                if($scope.form.$valid){
                    $postData.id_company = $rootScope.id_company;
                    $postData.updated_by = $rootScope.userId;
                    var param=$postData;
                    if($postData.id_language!=undefined){
                        param.id_language=$postData.id_language;
                    }
                    masterService.language.post($scope.language.flag,param).then(function(result){
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            $uibModalInstance.close();
                        }else{
                            $rootScope.toast('Error',result.error,'error');
                        }
                    });
                }
            }
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.uploadLanguageFlag=function(file){
            $scope.language.flag=file;
            $scope.trash=true;
        };
        $scope.uploadLanguageFlagRemove=function(){
            $scope.language.flag='';
            $scope.trash=false;
        };
    })

    .controller('otherCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        //dropdown
        //$scope.masterOtherSelected = '';
        masterService.other.otherList().then(function(result){
            $scope.masterOtherList=result.data.data;
            $scope.masterOtherSelected = $scope.masterOtherList[0].id_master;
        });
        //table
        $scope.otherList=[];
        $scope.callServer=function callServer(tableState){
            $scope.isLoading=true;
            $scope.dtableState=tableState;
            var pagination=tableState.pagination;
            var start=pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number=pagination.number||2;  // Number of entries showed per page.
            masterService.other.getPage(start,number,$scope.masterOtherSelected).then(function(result){
                $scope.otherList=result.data.data;
                tableState.pagination.numberOfPages=Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading=false;
            });
        };

        $scope.open=function($mode,$editId){
            var $item={};
            $item.masterOtherSelected=$scope.masterOtherSelected;
            if($mode=='edit'){
                $item.type='edit';
                $item.editId=$scope.editId;
            }
            var modalInstance=$uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/master-data/create-other.html',
                controller:'CreateOtherMasterCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected={};
                $scope.selectedCount=0;
                $scope.callServer($scope.dtableState);
            },function(){
            });
        };

        //
        $scope.updateOtherMaster = function(id){
            $scope.masterOtherSelected = id;
            $scope.callServer($scope.dtableState);
            $scope.selectedCount=0;
            $scope.selected={};
        }


        //add new row in modal
        $scope.animationsEnabled=true;
        $scope.toggleAnimation=function(){
            $scope.animationsEnabled= !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId=0;
        $scope.selected={};
        $scope.updateSelected=function(name){
            var count=0;
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId=x;
                    $scope.selectedText=name;
                    count++;
                }
            }
            $scope.selectedCount=count;
        };
    }])
    .controller('CreateOtherMasterCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title='Create';
        $scope.btn='Add';
        $scope.other={};
        if(items.type=='edit'){
            $scope.title='Update';
            $scope.btn='Update';
            masterService.other.getById(items.editId).then(function(result){
                $scope.other=result.data;
            })
        }
        $scope.other={"child_name":'',"description":''};
        $scope.save=function($postData){
            $postData.master_id = items.masterOtherSelected;
            $postData.id_company = $rootScope.id_company;
            $postData.updated_by = $rootScope.userId;
            if($postData.id_child==undefined){
                $postData.created_by = $rootScope.userId;
                $scope.add($postData);
            }else{
                $scope.update($postData);
            }
        };
        $scope.add=function($postData){
            masterService.other.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update=function($postData){
            masterService.other.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                }else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.cancel=function(){
            $uibModalInstance.dismiss('cancel');
        };
    })


/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('collateral',[])
    .controller('collateralCtrl',function($scope, $rootScope,masterService,companyService,collateralService){
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.collateralTypeList=[];
        $scope.getCollateralTypeList=function(){
            collateralService.get({'company_id': $rootScope.id_company}).then(function(result){
                $scope.collateralTypeList=result.data;
            });
        }
        $scope.currencyList=[];
        $scope.getCurrencyList=function(){
            companyService.currency.get({'company_id': $rootScope.id_company}).then(function(result){
                $scope.currencyList=result.data;
                //$scope.showUsdAmount($scope.facilityInfo.currency_id);
            })
        }
        $scope.countries=[];
        $scope.getCountries=function(){
            masterService.country.get().then(function(result){
                $scope.countries=result.data;
            });
        }
        $scope.showUsdAmount = function (currency_id) {
            $scope.showUsdAmountDiv = false;
            if (currency_id) {
                var a = _.findWhere($scope.currencyList, {'currency_id': currency_id});//searching Element in Array
                if (a) {
                    $scope.currentCurrencyCode = a.currency_code;
                    if (a.currency_code == 'USD') {
                        //$scope.facilityInfo.facility_usd_amount = $scope.facilityInfo.facility_amount;
                        $scope.showUsdAmountDiv = false;
                    } else {
                        $scope.showUsdAmountDiv = true;
                    }
                }
            }
            //$scope.currentFormIndex = _.indexOf($scope.companyFormDetails, a);
        }
    })
    .controller('collateralListCtrl', function ($scope, $rootScope,collateralService) {
        $scope.collateralTypeList = [];
        $scope.collateralType = function () {
            collateralService.get({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.collateralTypeList = result.data;
            });
        }
        $scope.collateralType();
    })
    .controller('collateralEditCtrl', function ($scope, $rootScope,collateralService,$stateParams,decodeFilter) {
        $scope.collateralId = decodeFilter($stateParams.collateralId);
        $rootScope.currentCollateralId =  $scope.collateralId;
        $scope.collateralStageList = [];
        $scope.stageTitle = '';
        $scope.getCollateralStageList = function () {
            collateralService.stage.get({'company_id':$rootScope.id_company,'collateral_type_id':$scope.collateralId}).then(function(result){
                $scope.collateralStageList = result.data;
            })
        }
        $scope.collateralStageChange =function(cs){
            var param = {};
            param.collateral_stage_id = cs.id_collateral_stage;
            param.collateral_type_id =  $scope.collateralId;
            param.status =(cs.checked)?1:0;
            param.company_id =$rootScope.id_company;
            collateralService.stage.post(param).then(function(result){
                if($rootScope.toastMsg(result)){

                }
            })
        }
        $scope.getFields = function(){
            collateralService.typeFields.get({'company_id':$rootScope.id_company,'collateral_type_id':$scope.collateralId}).then(function(result){
                $scope.collateralInfo = result.data;
                $scope.form_fields = result.data.fields;
            })
        }
        $scope.changeTemplate = function(cs){
            $rootScope.currentCollateralStageId = cs.id_collateral_stage;
            $scope.stageTitle = cs.collateral_stage_name;
            $scope.template=cs.collateral_stage_template;
        }
        $scope.getFields();
        $scope.getCollateralStageList();
    })
    .controller('collateralConfigCtrl', function ($scope, $rootScope,collateralService,$state,decodeFilter) {

        $scope.collateralId = decodeFilter($stateParams.collateralId);
        $scope.collateralStageList = [];
        collateralService.stage.get({'company_id':$rootScope.id_company,'collateral_type_id':$scope.collateralId}).then(function(result){
            $scope.collateralStageList = result.data;
        })

        $scope.templateInfo = {};
        if ($rootScope.collateralTypeInfo == undefined) {
            $state.go('collateral.collateral-list')
            return false;
        }
        $scope.tabActive1 = $scope.tabActive2 = $scope.tabActive3 = false;
        if ($rootScope.collateralTypeInfo.tab == 1) {
            $scope.tabActive1 = true;
        } else if ($rootScope.collateralTypeInfo.tab == 2) {
            $scope.tabActive2 = true;
        } else {
            $scope.tabActive3 = true;
        }
        $scope.init = function () {
            $scope.loadTemplate($rootScope.collateralTypeInfo.item);
        }
        $scope.getCountries = function () {
            masterService.country.get().then(function (result) {
                $scope.countries = result.data;
            });
        }
        $scope.loadTemplate = function (collateralType) {
            $rootScope.currentCollateralId = collateralType.id_collateral_type;
            $scope.templateInfo = collateralType;
            $scope.collateralStage(collateralType.id_collateral_type);
            $scope.getAssessmentQuestion = function () {
                companyService.assessmentQuestion.question.get({
                    'company_id': $rootScope.id_company,
                    'id_collateral_type': $rootScope.currentCollateralId,
                }).then(function (result) {
                    if (result.status) {
                        $scope.categoryQuestionList = result.data[0].category;
                    }
                })
            }
        }
        $scope.shuffle = function (item, type) {
            if (type) {
                item.status = 1;
            } else {
                item.status = 0;
            }
        }
        $scope.collateralStage = function (collateral_type_id) {
            $scope.collateralStageList = [];
            companyService.collateral.collateralStage.get({
                'company_id': $rootScope.id_company,
                'collateral_type_id': collateral_type_id
            }).then(function (result) {
                $scope.collateralStageList = result.data;
            });
        }
        $scope.saveCheckList = function () {
            $scope.arrayChecks = [];
            $scope.collateralStageList.forEach(function (data) {
                if (data.status == 1) {
                    $scope.arrayChecks.push(data.id_collateral_stage);
                }
            })
            companyService.collateral.collateralStage.post({
                'company_id': $rootScope.id_company,
                'collateral_type_id': $scope.templateInfo.id_collateral_type,
                'collateral_stage_ids': $scope.arrayChecks
            }).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            });
        }
        $scope.init();
    })
    .controller('collateralChecklistCtrl', function (decodeFilter, $scope, $rootScope, $uibModal, companyService, $state, $q, $stateParams) {
        $scope.categoryQuestionList = [];
        $scope.show_options = true;
        $scope.id_assessment_question_type = '';
        $scope.title = "Title";
        $scope.sortableOptions = {
            update: function (e, ui) {
            },
            stop: function (e, ui) {
            }
        };
        $scope.beforeDrop = function (evt, ui, category) {
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function (e) {
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function (e) {
            angular.element(e.target).removeClass("drop-hover");
        };
        $scope.getAssessmentQuestion = function () {
            companyService.assessmentQuestion.question.get({
                'company_id': $rootScope.id_company,
                'collateral_type_id': $rootScope.currentCollateralId,
                'assessment_question_type_key': 'collateral_document',
                'collateral_stage_id':$rootScope.currentCollateralStageId
            }).then(function (result) {
                if (result.status) {
                    $scope.categoryQuestionList = result.data[0].category;
                }
            })
        }
        $scope.statusChange = function (category, question) {
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.categoryStatusChange = function (category) {
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function (result) {
                if (result.status) {
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error');
                }
            })
        }
        $scope.assessmentModal = function (option_type, question) {
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'modal-full my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/assessment/assessment-questions-view.html',
                scope: $scope,
                controller: function ($uibModalInstance, $scope, $rootScope, item, category, companyService) {
                    $scope.question = {};
                    $scope.question.required = 1;
                    $scope.question.created_by = $rootScope.userId;
                    $scope.question.question_option = [];
                    $scope.question.question_type = category.value;
                    $rootScope.loaderOverlay = false;
                    $scope.getCategory = function () {
                        companyService.assessmentQuestion.category.get({
                            'company_id': $rootScope.id_company,
                            'assessment_question_type_id': 5,
                            'collateral_type_id': $rootScope.currentCollateralId,
                            'collateral_stage_id':$rootScope.currentCollateralStageId
                        }).then(function (result) {
                            if (result.status) {
                                $scope.categoryList = result.data;
                                if ($scope.id_assessment_question_category != undefined) {
                                    $scope.question.assessment_question_category_id = $scope.id_assessment_question_category;
                                }
                                if (item != undefined) {
                                    angular.copy(item, $scope.question);
                                    $scope.question.required = parseInt(item.required);
                                    $scope.option_type = item.question_type;
                                }
                            }
                        })
                    }
                    $scope.getCategory();
                    $scope.save = function (question) {
                        if ($scope.form.$valid) {
                            companyService.assessmentQuestion.question.post(question).then(function (result) {
                                if (result.status) {
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success', result.message);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            })
                        }
                    }
                    // add new option to the field
                    $scope.addOption = function (question, title) {
                        if (!question.question_option)
                            question.question_option = new Array();
                        var lastOptionID = 0;
                        if (question.question_option[question.question_option.length - 1])
                            lastOptionID = question.question_option[question.question_option.length - 1].option_id;
                        // new option's id
                        var option_id = lastOptionID + 1;
                        var newOption = {
                            "option_id": option_id,
                            "option_title": title,
                            "option_value": option_id,
                            "option_name": 'option_name_' + option_id,
                            "option_check": true
                        };
                        // put new option into field_options array
                        question.question_option.push(newOption);
                    }
                    // delete particular option
                    $scope.deleteOption = function (question, option) {
                        for (var i = 0; i < question.question_option.length; i++) {
                            if (question.question_option[i].option_id == option.option_id) {
                                question.question_option.splice(i, 1);
                                break;
                            }
                        }
                    }
                    // decides whether field options block will be shown (true for dropdown and radio fields)
                    $scope.showAddOptions = function (question) {
                        if (question.question_type == "radio" || question.question_type == "dropdown" || question.question_type == "checkbox") {
                            if (question.question_option.length == 0) {
                                if (question.question_type == "radio") {
                                    $scope.addOption(question, 'Yes');
                                    $scope.addOption(question, 'No');
                                } else {
                                    $scope.addOption(question);
                                }
                            }
                            return true;
                        }
                        else
                            return false;
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    category: option,
                    item: question
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
            });
        }
        $scope.addCategoryModal = function (selectedItem) {
            var modalInstance = $uibModal.open({
                animation: true,
                //windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/admin/assessment/add-category-modal.html',
                scope: $scope,
                resolve: {
                    item: selectedItem
                },
                controller: function ($uibModalInstance, $scope, $rootScope, item, companyService, masterService) {
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    masterService.sector.getAll().then(function (result) {
                        $scope.sectorsList = result.data.data;
                    });
                    if (item != undefined) {
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        if (item.sector_id != null && item.sector_id != undefined) {
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }
                    }
                    $scope.save = function (category) {
                        if ($scope.form.$valid) {
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = 5;
                            category.sector_id = category.sector_id.toString();
                            category.collateral_type_id = $rootScope.currentCollateralId;
                            category.collateral_stage_id = $rootScope.currentCollateralStageId;
                            companyService.assessmentQuestion.category.post(category).then(function (result) {
                                if (result.status) {
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success', result.message);
                                    $scope.cancel();
                                } else {
                                    $rootScope.toast('Error', result.error, 'error');
                                }
                            })
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function ($data) {
            }, function () {
                $scope.getAssessmentQuestion();
            });
        }
    })
    .controller('documentsCtrl',function($scope,$rootScope,collateralService,$uibModal){
        $scope.documentList = [];
        $scope.getDocumentsList=function(){
            var param = {};
            param.company_id = $rootScope.id_company;
            param.collateral_type_id = $rootScope.currentCollateralId;
            collateralService.documents.get(param).then(function(response){
                $scope.documentList =  response.data;
                console.log('response',response);
            })
        }
        $scope.deleteDocument = function(row){
            var params = {};
            params.id_collateral_document = row.id_collateral_document;
            collateralService.documents.delete(params).then(function(result){
                if($rootScope.toastMsg(result)){
                    $scope.getDocumentsList();
                }
            })
        }
        $scope.openDocumentModal=function(row){
            $scope.selectedRow=row;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'document-modal.html',
                scope: $scope,

                controller: function($uibModalInstance,$scope,$rootScope,collateralService){
                    $scope.field = {};
                    $scope.actionTitle = 'Add';
                    if($scope.selectedRow){
                        $scope.actionTitle = 'Update';
                        $scope.field =  $scope.selectedRow;
                    }

                    $scope.addDocument = function(){
                        var prams = $scope.field;
                        prams.company_id = $rootScope.id_company;
                        prams.created_by = $rootScope.userId;
                        prams.collateral_type_id = $rootScope.currentCollateralId;
                        collateralService.documents.post(prams).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.getDocumentsList();
                                $scope.cancel();
                            }
                        })

                    }
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
    })
    .controller('mapCtrl', function (NgMap, $scope) {
        $scope.getMap = function () {
            NgMap.getMap().then(function (map) {
                //console.log('map',map.getCenter().lat());
                //console.log('map',map.getCenter().lng());
            });
        }
    })
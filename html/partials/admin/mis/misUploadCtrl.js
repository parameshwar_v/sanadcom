/**
 * Created by naresh gurrala on 14/9/16.
 */
angular.module('app')

    .controller('misUploadCtrl',['$scope','$rootScope','$stateParams','Upload','crmCompanyService',function($scope,$rootScope,$stateParams,Upload,crmCompanyService){
        $scope.pageTitle = $scope.upload_type =  $stateParams.type;
        /*switch ($stateParams.type) {
            case 'disbursement':
            case 'repayment':
            case 'collection':
            default:
        }*/

        var currentDate = new Date();
        $scope.analysis_month = currentDate;
        $scope.outstanding_month = currentDate;

        $scope.statementData = [];
        $scope.statementValidData = [];
        $scope.uploadFinancialExcel = function (file) {
            if (file != null && file != '') {
                $scope.uploadFile = file;
                $scope.isExcel = false;
            }
        };
        $scope.submitUpload = function () {
            $scope.cancel();
            if ($scope.uploadFile == null || $scope.uploadFile == '') {
                $scope.isExcel = true;
            } else {
                $scope.isExcel = false;
            }
            if ($scope.uploadFile && $scope.uploadFile != null) {
                $scope.uploadData = {type:$scope.upload_type};
                Upload.upload({
                    url: API_URL + 'Mismaster/uploadFacilityPlans',
                    data: {
                        file: {'excel': $scope.uploadFile},
                        'financeData': $scope.uploadData
                    }
                }).success(function (result) {
                    if (result.status) {
                        $rootScope.toast('Success', result.message);
                        $scope.statementData = result.data;
                        $scope.uploadFile = '';
                        angular.element("input[type='file']").val(null);
                    } else {
                        $rootScope.toast('Error', [result.error], 'error', []);
                    }
                });
            }
        };

        $scope.submitRecords = function(Data){
            $scope.uploadData = {type:$scope.upload_type,facility_data:Data,id_company:$rootScope.id_company,created_by : $rootScope.userId};
            crmCompanyService.misUpload.post($scope.uploadData).then(function (result) {
                if (result.status) {
                    $scope.cancel();
                    if(result.data.type == 'collection' && result.data.facilityData != '' ){
                        $scope.statementValidData = result.data.facilityData;
                    }
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        }

        $scope.submitValidRecords = function(Data){
            $scope.uploadData = {type:$scope.upload_type,facility_data:Data,id_company:$rootScope.id_company,created_by : $rootScope.userId};
            crmCompanyService.misUpload.updateFacilityPlans($scope.uploadData).then(function (result) {
                if (result.status) {
                    $scope.cancel();
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        }

        $scope.cancel = function(){
            $scope.statementData = [];
            $scope.statementValidData = [];
        }

        $scope.checkNumber = function(val)
        {
            if(isNaN(val) || val=='')
                return false;
            else
                return true;
        }

        $scope.submitAgingUpload = function (valid) {
            $scope.cancel();
            if ($scope.uploadFile == null || $scope.uploadFile == '') {
                $scope.isExcel = true;
            } else {
                $scope.isExcel = false;
            }
            if(valid){
                if ($scope.uploadFile && $scope.uploadFile != null) {
                    $scope.uploadData = {type:$scope.upload_type};
                    Upload.upload({
                        url: API_URL + 'Mismaster/uploadAgingAnalysis',
                        data: {
                            file: {'excel': $scope.uploadFile},
                            'financeData': $scope.uploadData,
                            analysis_month: $scope.analysis_month
                        }
                    }).success(function (result) {
                        if (result.status) {
                            $rootScope.toast('Success', result.message);
                            $scope.statementData = result.data;
                            $scope.uploadFile = '';
                            angular.element("input[type='file']").val(null);
                        } else {
                            $rootScope.toast('Error', [result.error], 'error', []);
                        }
                    });
                }
            }
        };

        $scope.submitAgingRecords = function(Data){
            $scope.uploadData = {type:$scope.upload_type,
                aging_data:angular.toJson(Data),
                id_company:$rootScope.id_company,
                created_by : $rootScope.userId,
                analysis_month: $scope.analysis_month};
            crmCompanyService.misUpload.aging($scope.uploadData).then(function (result) {
                if (result.status) {
                    //console.log('resultresultresultresult',result.data);
                    $scope.cancel();
                    $scope.analysis_month = currentDate;
                    /*if(result.data.type == 'collection' && result.data.facilityData != '' ){
                        $scope.statementValidData = result.data.facilityData;
                    }*/
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        };

        $scope.submitOutstandingUpload = function (valid) {
            $scope.cancel();
            if ($scope.uploadFile == null || $scope.uploadFile == '') {
                $scope.isExcel = true;
            } else {
                $scope.isExcel = false;
            }
            if(valid){
                if ($scope.uploadFile && $scope.uploadFile != null) {
                    $scope.uploadData = {type:$scope.upload_type};
                    Upload.upload({
                        url: API_URL + 'Mismaster/uploadOutstandingAmount',
                        data: {
                            file: {'excel': $scope.uploadFile},
                            'outstanding_data': $scope.uploadData,
                            outstanding_month: $scope.outstanding_month
                        }
                    }).success(function (result) {
                        if (result.status) {
                            $rootScope.toast('Success', result.message);
                            $scope.statementData = result.data;
                            $scope.uploadFile = '';
                            angular.element("input[type='file']").val(null);
                        } else {
                            $rootScope.toast('Error', [result.error], 'error', []);
                        }
                    });
                }
            }
        };

        $scope.submitOutstandingRecords = function(Data){
              $scope.uploadData = {type:$scope.upload_type,
                outstanding_data:angular.toJson(Data),
                id_company:$rootScope.id_company,
                created_by : $rootScope.userId,
                outstanding_month: $scope.outstanding_month};
            crmCompanyService.misUpload.outstanding($scope.uploadData).then(function (result) {
                if (result.status) {
                    $scope.cancel();
                    $scope.outstanding_month = currentDate;
                    $rootScope.toast('Success', result.message);
                } else {
                    $rootScope.toast('Error', result.error, 'error', $scope.sector);
                }
            })
        };

    }])
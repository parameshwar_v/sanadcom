/**
 * Created by RAKESH on 10-08-2016.
 */
angular.module('app')
    .controller('ratiosManagementCtrl', function ($scope,$rootScope,masterService) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
             masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }


    })

    .controller('ratiosManagementListCtrl',function (masterService, $scope, $rootScope, $uibModal,crmCompanyService) {
        $scope.ratioCategoryList = [];
        $scope.getRationCategoryList = function(){
            crmCompanyService.ratiosCategory.get({'company_id':$rootScope.id_company}).then(function(result){
               $scope.ratioCategoryList = result.data;
            })
        }
        $scope.getRationCategoryList();
        $scope.openCategoryRatioModal = function(row){
            var modalInstance2 = $uibModal.open({
                animation: true,
                windowClass:'my-class',
                openedClass: 'right-panel-modal modal-open',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl: 'partials/admin/ratios-management/category-ratio-modal.html',
                controller:function($uibModalInstance,masterService,crmCompanyService){
                    masterService.sector.getAll().then(function (result) {
                        $scope.sectorsList = result.data.data;
                    });
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cRatio = {};
                    if(row){
                        $scope.cRatio = row;
                    }
                    $scope.saveCategoryRatio = function(cRatio){
                        cRatio.company_id = $rootScope.id_company;
                        cRatio.created_by = $rootScope.userId;
                        crmCompanyService.ratiosCategory.post(cRatio).then(function(result){
                          if($rootScope.toastMsg(result)){
                              $scope.getRationCategoryList();
                              $scope.cancel();
                          }
                        })
                    }
                }
            })
        }

        $scope.openRationModal = function(ratio_category_id,row){
            $scope.ratio_category_id = ratio_category_id;
            console.log('ratio_category_id',ratio_category_id);
            console.log('row',row);
            var modalInstance2 = $uibModal.open({
                animation: true,
                windowClass:'my-class',
                openedClass: 'right-panel-modal modal-open',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl: 'partials/admin/ratios-management/ratio-modal.html',
                controller:function($uibModalInstance,masterService,crmCompanyService){
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.ratio = {};
                    if(row){
                        $scope.ratio = row;
                    }
                    $scope.saveCategoryRatio = function(ratio){
                        ratio.company_id = $rootScope.id_company;
                        ratio.created_by = $rootScope.userId;
                        ratio.ratio_category_id = $scope.ratio_category_id;
                        crmCompanyService.ratios.post(ratio).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.getRationCategoryList();
                                $scope.cancel();
                            }
                        })
                    }
                }
            })
        }


    })

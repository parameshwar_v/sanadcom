/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 */
'use strict';
angular.module('crm.covenant',[])
    .controller('crmCovenantCtrl', function ($scope,confirm,$uibModal,$rootScope,covenantService,masterService,companyService,$timeout ) {
        $scope.dropDownItems = [];
        $scope.masterDropDownList = function (params) {
            masterService.masterList(params).then(function (result) {
                if (result.status)
                    $scope.dropDownItems[params.master_key] = result.data;
            })
        }
        $scope.userList = [];
        $scope.getUserAllList = function () {
            companyService.userList({'company_id': $rootScope.id_company}).then(function (result) {
                $scope.userList = result.data.data;
            });
        }
        $scope.getCovenantCrmList = function () {
            var params = {};
            //params.module_id = $rootScope.currentProjectViewId;
            //params.module_type = 'project';
            angular.forEach($scope.getData,function(value,key){
                params[key] = value;
            })
            params.company_id = $rootScope.id_company;
            covenantService.crmCovenant.get(params).then(function (result) {
                if (result.status){
                    $scope.covenantCrmList = result.data;
                    if($scope.covenantCrmList.length > 0){}
                        $scope.covenantActive($scope.covenantCrmList[0])
                }



            })
        }

        $scope.deleteCovenant = function (id_module_covenant) {
            confirm().then(function () {
                    covenantService.crmCovenant.delete({'id_module_covenant': id_module_covenant}).then(function (result) {
                        if ($rootScope.toastMsg(result)) {
                            console.log('id_module_covenant',id_module_covenant);
                            $scope.getCovenantCrmList();
                        }
                    })
            })
        }
        $scope.cTaskHistroy = {};
        $scope.getCovenantHistory = function (tableState) {
            $scope.refTableState = tableState;
            tableState.module_covenant_id = $scope.selectedCovenant.id_module_covenant;
            covenantService.crmCovenant.taskHistory(tableState).then(function (result) {
                $scope.cTaskHistroy[tableState.module_covenant_id] = result.data.data;
                // tableState.pagination.
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
            })
        }
        $scope.cActionHistoryModal = function (id_covenant_task, module_covenant_id) {
            $scope.id_covenant_task = id_covenant_task;
            $scope.id_module_covenant = module_covenant_id;
            var modalInstance2 = $uibModal.open({
                animation: true,
                openedClass: 'right-panel-modal modal-open',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                size: 'lg',
                templateUrl: 'partials/component/covenant/covenant-action-history-modal.html',
                controller: function ($uibModalInstance, covenantService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cTaskActionHistroy = [];
                    $scope.getCovenantTaskActionHistory = function (tableState) {
                        tableState.covenant_task_id = $scope.id_covenant_task;
                        //tableState.project_id = $rootScope.currentProjectViewId;
                        angular.forEach($scope.getData,function(value,key){
                            tableState[key] = value;
                        })
                        covenantService.crmCovenant.taskActionHistory(tableState).then(function (result) {
                            $scope.refTableState = tableState;
                            $scope.cTaskActionHistroy = result.data.data;
                            // tableState.pagination.
                            tableState.pagination.numberOfPages = Math.ceil(result.data.total_records / tableState.pagination.number);
                        })
                    }
                }
            })
        }
        $scope.cHistoryActionModal = function (id_covenant_task, module_covenant_id) {
            $scope.id_covenant_task = id_covenant_task;
            var modalInstance2 = $uibModal.open({
                animation: true,
                openedClass: 'right-panel-modal modal-open',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                size: 'lg',
                templateUrl: 'partials/component/covenant/covenant-history-action-modal.html',
                controller: function ($uibModalInstance, covenantService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.minDate = new Date();
                    $scope.fd = {};
                    $scope.cHistoryActionSave = function (fd, type) {
                        var params = fd;
                        if (!params.hasOwnProperty('comments')) {
                            params.comments = '';
                        }
                        params.covenant_task_id = $scope.id_covenant_task;
                        params.type = type;
                        params.created_by = $rootScope.userId;
                        params.company_id = $rootScope.id_company;
                        //console.log('params',params);
                        covenantService.crmCovenant.updateTaskStatus(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.refTableState.module_covenant_id = $scope.id_module_covenant;
                                $scope.getCovenantHistory($scope.refTableState);
                                $scope.cancel();
                            }
                        })
                    }
                }
            });
        }

        $scope.getCovenantDetails = function (id_module_covenant) {
            var params = {};
            params.id_module_covenant = id_module_covenant;
            params.company_id = $rootScope.id_company;
            angular.forEach($scope.getData,function(value,key){
                params[key] = value;
            })
            covenantService.crmCovenant.get(params).then(function (result) {
                if (result.status)
                    $scope.covenant = result.data;
            })
        }
        $scope.updateCovenant = function (params,form) {
            params.created_by = $rootScope.userId;
            covenantService.crmCovenant.update(params).then(function (result) {
                if ($rootScope.toastMsg(result)) {
                   // $scope.getCovenantCrmList();
                    $rootScope.resetForm(form);
                }

            })

        }
        $scope.covenantConfigForm = false;
        $scope.covenantActive = function(c){
            $scope.selectedCovenant = {};
            $scope.covenant = {};
            $timeout(function(){
                $scope.selectedCovenant = c;
                $scope.getCovenantDetails(c.id_module_covenant);
            })
        }

        $scope.configCovenantModalOpen = function (id_module_covenant) {
            $scope.id_module_covenant = id_module_covenant;
            var modalInstance = $uibModal.open({
                animation: true,
                openedClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl: 'partials/component/covenant/config-covenant-modal.html',
                controller: function ($uibModalInstance, covenantService, companyService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.covenant = {};
                    $scope.getCovenantDetails = function () {
                        var params = {};
                        params.id_module_covenant = id_module_covenant;
                        params.company_id = $rootScope.id_company;
                        angular.forEach($scope.getData,function(value,key){
                            params[key] = value;
                        })
                        covenantService.crmCovenant.get(params).then(function (result) {
                            if (result.status)
                                $scope.covenant = result.data;
                        })
                    }
                    $scope.updateCovenant = function (params) {
                        params.created_by = $rootScope.userId;
                        covenantService.crmCovenant.update(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.cancel();
                                $scope.getCovenantCrmList();
                            }

                        })

                    }

                    //$scope.getUserAllList();
                    $scope.getCovenantDetails();
                }
            })
        }
        $scope.CreateCovenantModalOpen = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                templateUrl: 'partials/component/covenant/create-covenant-user.html',
                controller: function ($uibModalInstance, covenantService) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.selectAll = function (checked) {
                        angular.forEach($scope.covenantChecked, function (value, key) {
                            $scope.covenantChecked[key] = (checked) ? true : false;
                        });
                    }
                    $scope.covenantSearchList = [];
                    $scope.covenantSearch = function (searchData) {
                        if(searchData){
                            var params = searchData;
                        }else{
                            var params = {};
                            //params.search_type = 0;
                        }
                        angular.forEach($scope.getData,function(value,key){
                            params[key] = value;
                        })
                        params.company_id = $rootScope.id_company;
                        covenantService.search(params).then(function (result) {
                            $scope.covenantChecked = {};
                            $scope.covenantSearchList = result.data;
                        })
                    }
                    $scope.covenantSearch({'search_type': 0});
                    $scope.saveCovenant = function (covenantList) {
                        $scope.checked = [];
                        angular.forEach(covenantList, function (value, key) {
                            if (value) {
                                $scope.checked.push(key);
                            }
                        });
                        var params = {};
                        params.covenant_id = $scope.checked;
                        angular.forEach($scope.getData,function(value,key){
                            params[key] = value;
                        })
                        params.company_id = $rootScope.id_company;
                        covenantService.crmCovenant.post(params).then(function (result) {
                            if ($rootScope.toastMsg(result)) {
                                $scope.cancel();
                                $scope.getCovenantCrmList();
                            }

                        })

                    }

                }
            });
        }
    })


    .directive('covenantView',function(){
        return {
            restrict:'EA',
            controller:'crmCovenantCtrl',
            scope:{
              getData:'='
            },
            link:function(scope,element,attrs){
                console.log('getData',scope.getData);
                scope.getCovenantCrmList();
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/covenant/covenent-view.html'
            }
        }
    })

/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 * --  $rootScope.$emit("reloadTouchPoint"); //reload touch point
 *
 */
'use strict';
angular.module('exposure',[])
    .controller('ExposureCtrl', function ($scope, $rootScope, crmFacilityService, $element,$attrs) {

        $scope.crm_contact_id = $rootScope.crm_contact_id;

        $scope.getContactExposure = function (params) {
            crmFacilityService.crm.contactExposure(params).then(function (result) {
                if (result.status)
                //console.log('result.data',result.data);
                    $scope.tableExposure = result.data;
            })
        }
        $scope.getCompanyExposure = function (params) {
            crmFacilityService.crm.companyExposure(params).then(function (result) {
                if (result.status)
                    $scope.tableExposure = result.data;
            })
        }
        var params = $attrs.componetParmes;
        if($attrs.componetType == 'contact'){
            $scope.getContactExposure($scope.componetParmes);
        }else if($attrs.componetType == 'company') {
            $scope.getCompanyExposure($scope.componetParmes);
        }
    })
    .controller('guaranteeCtrl', function ($scope, $rootScope, crmFacilityService, $element,$attrs) {

    $scope.crm_contact_id = $rootScope.crm_contact_id;

    $scope.getContactExposure = function (params) {
        crmFacilityService.crm.clientGuaranteeFacilities(params).then(function (result) {
            if (result.status)
            //console.log('result.data',result.data);
                $scope.tableExposure1 = result.data;
        })
    }
    /*$scope.getCompanyExposure = function (params) {
        crmFacilityService.crm.clientGuaranteeFacilities(params).then(function (result) {
            if (result.status)
                $scope.tableExposure1 = result.data;
        })
    }*/
    var params = $attrs.componetParmes;
    if($attrs.componetType == 'contact'){
        $scope.getContactExposure($scope.componetParmes);
    }else if($attrs.componetType == 'company') {
        $scope.getCompanyExposure($scope.componetParmes);
    }
})

    .directive('exposureComponent',function(){
        return {
            restrict:'EA',
            controller:'ExposureCtrl',
            scope: {
                componetParmes: '='
            },
            link:function(scope,element,attrs){

                //scope.getNotificationModuleList(scope.getData)
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/exposure/view.html'
            }
        }
    })





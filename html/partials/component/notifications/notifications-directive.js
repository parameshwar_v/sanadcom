/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 * --  $rootScope.$emit("reloadTouchPoint"); //reload touch point
 *
 */
'use strict';
angular.module('notifications',[])
    .controller('notificationModuleCtrl',function($rootScope,$scope,notificationService){
        $scope.notificationModuleList = [];
        $scope.getNotificationModuleList = function (stTable) {
            angular.extend(stTable,$scope.getData);
            if(stTable.pagination.start === 0){
                $scope.notificationModuleList = [];
            }
            notificationService.moduleList(stTable,'notification_module_loader').then(function (result) {
                angular.forEach(result.data.data,function(item){
                    $scope.notificationModuleList.push(item);
                })
               // $scope.notificationModuleList = result.data.data;
                $scope.totalNotificationCount = result.data.total_records;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            })
        }
    })
    .directive('notificationModuleList',function(){
        return {
            restrict:'EA',
            controller:'notificationModuleCtrl',
            scope: {
                getData: '='
            },
            link:function(scope,element,attrs){
                //scope.getNotificationModuleList(scope.getData)
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/notifications/view.html'
            }
        }
    })





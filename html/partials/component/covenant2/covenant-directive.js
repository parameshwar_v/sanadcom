/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 */
'use strict';
angular.module('covenant',[])

    .controller('covenantCtrl',function(encodeFilter,$scope,$rootScope,$uibModal,companyService){

    })

    .controller('covenantDashboardCtrl',function($scope,$rootScope,$uibModal,masterService,companyService){

        $scope.categoryList = [];

        $scope.getCategory = function(){
            companyService.covenant.category.get({
                'company_id':$rootScope.id_company,
                'covenant_type_key':$scope.key
            }).then(function(result){
                $scope.categoryList = result.data;
            })
        }
        $scope.beforeDrop = function(evt,ui,row){
            angular.element(evt.target).removeClass("drop-hover");
            $scope.addCovenant(row);

        };
        $scope.onOver = function(e){
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function(e){
            angular.element(e.target).removeClass("drop-hover");
        };

        //$scope.getCategory();
        $scope.statusChange = function(covenant){
            var params = covenant;
            params.created_by = $rootScope.userId;

            companyService.covenant.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                 } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })

        }

        $scope.addCovenant = function(category,item){

            var modalInstance = $uibModal.open({
                templateUrl:'partials/component/covenant/covenant-modal.html',
                scope:$scope,
                resolve:{
                    category:category,
                    selectedItem:item,
                },
                controller:'addCovenantModelCtrl'
            });

            modalInstance.result.then(function(){
                $scope.getCategory();
            });
        }

        $scope.addCategory = function(item){
            var modalInstance = $uibModal.open({
                templateUrl:'partials/component/covenant/category-modal.html',
                scope:$scope,
                resolve:{
                    selectedItem:item,
                    sectorList:masterService.sector.getAll(),
                    //covenantType:companyService.covenant.type.get({'company_id':$rootScope.id_company})
                },
                controller:function($rootScope,$scope,$uibModalInstance,sectorList,selectedItem,companyService){
                    //console.log('sectorList',sectorList);
                    $scope.category = {};
                    //$scope.covenantList = covenantType.data;
                    $scope.title = "Add New Category";
                    $scope.sectorsList = sectorList.data.data;
                    if(selectedItem){
                        $scope.title = "Update Category";
                        console.log('selectedItem',selectedItem);
                        angular.copy(selectedItem,$scope.category);
                        //$scope.category = selectedItem;
                        if($scope.category.sector=="0"){
                            $scope.category.sector_all = true;
                            $scope.category.sector = [];
                        } else{
                            $scope.category.sector = $scope.category.sector.split(",");
                        }
                    }
                    $scope.save = function(){
                        var params = {}
                        params.covenant_type_key = $scope.key;
                        params.company_id = $rootScope.id_company;
                        params.covenant_category = $scope.category.covenant_category;
                        params.id_covenant_category = $scope.category.id_covenant_category;
                        if($scope.category.sector_all){
                            params.sector = 0;
                        } else{
                            params.sector = $scope.category.sector.toString();
                        }

                        companyService.covenant.category.post(params).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        })

                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function(){
                $scope.getCategory();
            });
        }

    })

    .controller('addCovenantModelCtrl',function($rootScope,$scope,$uibModalInstance,category,selectedItem,companyService){
        //console.log('sectorList',sectorList);
        $scope.covenant = {};
        $scope.title = "Add New Covenant";
        if(category){
            $scope.covenant.covenant_category_id = category.id_covenant_category;
        }

        if(selectedItem){
            $scope.title = "Update Covenant ";
            angular.copy(selectedItem,$scope.covenant);
            //$scope.category = selectedItem;

        }
        $scope.save = function(){
            var params = $scope.covenant;
            params.created_by = $rootScope.userId;

            companyService.covenant.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('crmCovenantCtrl',function($scope,$rootScope,crmProjectService,$uibModal,companyService){
        $scope.categoryList = [];
        $scope.saveBtnShow = false;
        $scope.getCovenants = function(){
            crmProjectService.covenant.projectCovenant({
                'company_id':$rootScope.id_company,
                'crm_project_id':$rootScope.currentProjectViewId,
                'covenant_type_key':$scope.key
            }).then(function(result){
                $scope.categoryList = result.data;
                console.log('$scope.categoryList',$scope.categoryList);
            });
        }

        $scope.addCovenant = function(category,item){
            var modalInstance = $uibModal.open({
                templateUrl:'partials/component/covenant/project-covenant-modal.html',
                scope:$scope,
                resolve:{
                    category:category,
                    selectedItem:item
                },
                controller:'projectAddCovenantModelCtrl'
            })
            modalInstance.result.then(function(){

            },function(){
                $scope.getCovenants();
            });
        }

        $scope.saveCovenant = function(saveCovenant){
            console.log('saveCovenant',saveCovenant);
            var params = {}
            params.covenant_category_id = saveCovenant.id_covenant_category;
            params.covenant_name = saveCovenant.covenant_content;
            params.covenant_value = saveCovenant.covenant_value;
            params.covenant_details_defination = saveCovenant.covenant_details_defination;
            params.created_by = $rootScope.userId;
            params.crm_project_id = $rootScope.currentProjectViewId;

            companyService.covenant.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    saveCovenant.covenant_content = '';
                    $scope.covShow= false;
                    $scope.getCovenants();
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.deleteCovenant = function(id_covenant){
            var r = confirm("Do you want to continue?");
            if(r==true){
                companyService.covenant.delete({covenant_id:id_covenant,crm_project_id:$rootScope.currentProjectViewId}).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.getCovenants();
                    } else{
                        $rootScope.toast('Error',result.error,'error');
                    }
                })
            }
        }


    })

    .controller('projectAddCovenantModelCtrl',function($rootScope,$scope,$uibModalInstance,category,selectedItem,companyService,crmProjectService){
    //console.log('sectorList',sectorList);
    $scope.covenant = {};
    $scope.title = category.covenant_category+" Covenants";
        $scope.getCovenantsCategoryItems = function(){
            crmProjectService.covenant.getCovenantsCategoryItems({
                'company_id':$rootScope.id_company,
                'crm_project_id':$rootScope.currentProjectViewId,
                'covenant_type_key':$scope.key,
                'covenant_category_id':category.id_covenant_category
            }).then(function(result){
                $scope.covenant = result.data[0].covenant;
            });
        }
        $scope.getCovenantsCategoryItems();

    $scope.save = function(){
        $scope.covenant_ids = [];
            angular.forEach($scope.covenant, function (c) {
                if (c.is_checked) {
                    $scope.covenant_ids.push(c.id_covenant)
                }
        })
        var params = {};
        params.covenant_ids = $scope.covenant_ids.toString();
        params.id_covenant_category = category.id_covenant_category;
        params.crm_project_id = $rootScope.currentProjectViewId;
        params.created_by = $rootScope.userId;
        params.covenant_type_key = $scope.key;
        crmProjectService.covenant.post(params).then(function(result){
            if(result.status){
                $uibModalInstance.dismiss('cancel');
                $rootScope.toast('Success',result.message);
            } else{
                $rootScope.toast('Error',result.error,'error',$scope.sector);
            }
        })
    }
    $scope.cancel = function(){
        $uibModalInstance.dismiss('cancel');
    };
})

    .directive('covenant',function(){
        return {
            restrict:'EA',
            controller:'covenantDashboardCtrl',
            link:function(scope,element,attrs){
                scope.key = attrs.key;
                scope.getCategory();
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/covenant/covenant-admin-list.html'
            }
        }
    })

    .directive('covenantView',function(){
        return {
            restrict:'EA',
            controller:'crmCovenantCtrl',
            link:function(scope,element,attrs){
                scope.key = attrs.key;
                scope.getCovenants();
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/covenant/covenant-view.html'
            }
        }
    })




/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 * --  $rootScope.$emit("reloadTouchPoint"); //reload touch point
 *
 */
'use strict';
angular.module('utility',[])
    .controller('addressUtilityCtrl',function($rootScope,$scope,masterService,$uibModal){

        $scope.Countries=[];
        $scope.getCountries=function(){
            masterService.country.get().then(function($result){
                $scope.Countries=$result.data;
            });
        }
        $scope.$watch('address',function(newVal,oldVal){
            $scope.mapAddressArray=[];
            angular.forEach($scope.address,function(element){
                $scope.mapAddressArray.push(element);
            });
            if($scope.address.latitude&&$scope.address.langitude){
                $scope.latitude=$scope.address.latitude;
                $scope.langitude=$scope.address.langitude;
                $scope.mapAddress=$scope.address.latitude+','+$scope.address.langitude;
            }else{
                $scope.mapAddress=$scope.mapAddressArray.toString();
            }
            $scope.ngModel=JSON.stringify($scope.address);
        },true);

        $scope.openMapModel=function(mapAddress){
            // $scope.mapAddress = mapAddress;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/component/utility/map-modal.html',
                controller: function($scope,$uibModalInstance,$timeout){
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.showMap=false;
                    $timeout(function(){
                        $scope.showMap=true;
                    })
                    $scope.getMap=function(){
                        var marker=this;
                        $scope.address.latitude=marker.getPosition().lat();
                        $scope.address.langitude=marker.getPosition().lng();
                        $scope.latitude=$scope.address.latitude;
                        $scope.langitude=$scope.address.langitude;
                    }
                }
            });
        }
    })

    .directive('address',function(){
        return {
            restrict: 'EA',
            controller: 'addressUtilityCtrl',
            scope: {
                getData: '=',
                ngModel: '='
            },
            link: function(scope,element,attrs,ngModel){
                //scope.getNotificationModuleList(scope.getData)
                scope.IsJsonString=function(str){
                    try{
                        JSON.parse(str);
                    }catch(e){
                        return false;
                    }
                    return true;
                }
                if(scope.IsJsonString(scope.ngModel)){
                    scope.address=JSON.parse(scope.ngModel);
                }else{
                    scope.address={};
                }

            },
            templateUrl: function(elem,attrs){
                return 'partials/component/utility/address.html'
            }
        }
    })
    .directive('addressView',function(){
        return {
            restrict: 'EA',
            scope: {
                getData: '='
            },
            controller:function($uibModal,$scope){

                $scope.openMapModel=function(mapAddress){
                    // $scope.mapAddress = mapAddress;
                    var modalInstance=$uibModal.open({
                        animation: true,
                        windowClass: 'my-class',
                        backdrop: 'static',
                        keyboard: false,
                        scope: $scope,
                        openedClass: 'right-panel-modal modal-open',
                        templateUrl: 'partials/component/utility/map-modal.html',
                        controller: function($scope,$uibModalInstance,$timeout){
                            $scope.cancel=function(){
                                $uibModalInstance.dismiss('cancel');
                            }
                            $scope.mapAddressArray=[];
                            angular.forEach($scope.address,function(element){
                                $scope.mapAddressArray.push(element);
                            });
                            if($scope.address.latitude&&$scope.address.langitude){
                                $scope.latitude=$scope.address.latitude;
                                $scope.langitude=$scope.address.langitude;
                                $scope.mapAddress=$scope.address.latitude+','+$scope.address.langitude;
                            }else{
                                $scope.mapAddress=$scope.mapAddressArray.toString();
                            }
                            $scope.showMap=false;
                            $timeout(function(){
                                $scope.showMap=true;
                            })
                            $scope.getMap=function(){
                                var marker=this;
                                $scope.address.latitude=marker.getPosition().lat();
                                $scope.address.langitude=marker.getPosition().lng();
                                $scope.latitude=$scope.address.latitude;
                                $scope.langitude=$scope.address.langitude;
                            }
                        }
                    });
                }
            },
            link: function(scope,element,attrs,ngModel){
                //scope.getNotificationModuleList(scope.getData)
                scope.IsJsonString=function(str){
                    try{
                        JSON.parse(str);
                    }catch(e){
                        return false;
                    }
                    return true;
                }
                if(scope.IsJsonString(scope.getData)&&scope.getData){

                    scope.address=JSON.parse(scope.getData);
                }else{
                    scope.address={};
                }
            },
            templateUrl: function(elem,attrs){
                return 'partials/component/utility/address-view.html'
            }
        }
    })





/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 * --  $rootScope.$emit("reloadTouchPoint"); //reload touch point
 *
 */
'use strict';
angular.module('touch.points',[])
    .controller('touchPointViewCtrl',function($rootScope,$scope,crmService){
        $scope.togglePoint = false;
        $scope.touchPointsList = [];
        $scope.touchPointTypeList = [];
        $scope.grades = ['1', '2', '3', '4', '5'];
        $rootScope.$on("reloadTouchPoint", function(){
            $scope.touchPointsList = [];
            $scope.getTouchPoints();
        });
        /*add TouchPoint*/
        $scope.touchPointDescription = '';
        $scope.getTouchPointType =function(){
            crmService.touchPoint.touchPointType().then(function(result){
                $scope.touchPointTypeList = result.data;
            });
        }
        $scope.deleteTouchpoint = function(touch_point_id){
            if($rootScope.confirmMsg()){
                crmService.touchPoint.delete({'touch_point_id':touch_point_id}).then(function(result){
                    if($rootScope.toastMsg(result)){
                        $scope.getTouchPoints(true);
                    }
                });
            }

        }
        $scope.touchPointSubmit = function(postField,form){
            console.log(form.$valid);
            if(form.$valid){
                postField.crm_module_type = $scope.crm_module_type;
                postField.crm_module_id = $scope.crm_module_id;
                postField.touch_point_from_id = $scope.crm_module_id;
                postField.created_by = $rootScope.userId;
                //console.log('postField', postField);
                crmService.touchPoint.post(postField).then(function(result){
                    if($rootScope.toastMsg(result)){
                        $scope.togglePoint = false;
                        $scope.resetForm(form);
                        $scope.getTouchPoints(true)
                    }
                });
            }
        }
        $scope.getTouchPoints = function(refresh){
            if(refresh){
                $scope.touchPointsList = [];
            }

            var param = {};
            param.company_id = $rootScope.id_company;
            param.crm_module_type = $scope.crm_module_type;
            param.crm_module_id = $scope.crm_module_id;
            param.offset = $scope.touchPointsList.length;
            param.limit = 10;
            if( $scope.created_by){
                param.created_by = $rootScope.userId;
                param.type = 'touch_point';
            }
            if($scope.grade){
                param.grade = $scope.grade;
            } else {
                param.grade = '';
            }
            crmService.touchPoint.get(param,'touchpoint_loader').then(function(result){
                angular.forEach(result.data,function(item){
                    $scope.touchPointsList.push(item);
                })
                if(result.data.length>=10){
                    $scope.loadMoreShow = true;
                } else{
                    $scope.loadMoreShow = false;
                }
            });
        }
        $scope.loadMore = function(){
            $scope.getTouchPoints();
        }
        $scope.touchpointFilter = function(filterType){
            //console.log('filterType',filterType);
            if(filterType=='my_touchpoint'){
                $scope.created_by = $rootScope.userId;
                $scope.touchpointSearch.activity_description = '';
                $scope.touchpointSearch.grade = '';
                $scope.grade = '';
            }
            else if(filterType=='graded_touchpoint'){
                console.log('filterType',filterType);
                $scope.created_by = $rootScope.userId;
                $scope.grade = 'all';
            }
            else{
                $scope.created_by = undefined;
                $scope.type = undefined;
                $scope.touchpointSearch.activity_description = '';
                $scope.touchpointSearch.grade = '';
                $scope.grade = '';
            }
            $scope.getTouchPoints(true);
        }
    })

    .directive('touchPointView',function(){
        return {
            restrict:'EA',
            controller:'touchPointViewCtrl',
            link:function(scope,element,attrs){
                scope.crm_module_type = attrs.moduleType;
                scope.crm_module_id = attrs.moduleId;
                if(attrs.tabShow){
                    scope.tabShow= true;
                }else{
                    scope.tabShow= false;
                }
                scope.getTouchPoints();
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/touch_points/view.html'
            }
        }
    })





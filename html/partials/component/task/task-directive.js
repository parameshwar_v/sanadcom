/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 * --  $rootScope.$emit("reloadTouchPoint"); //reload touch point
 *
 */
'use strict';
angular.module('task',[])
    .controller('taskCtrl',function($rootScope,$scope,$stateParams,taskService,$uibModal,decodeFilter,companyService){
        $scope.refParams=$scope.getData;
        $scope.userId=$rootScope.userId;
        $scope.taskDetails=[];
        $scope.isEmptyRow = false;
        $scope.f_type='all_task';
        $scope.pendingTaskCount = 0;
        $scope.tasksList=function(stTable){
            if(!stTable){
                $scope.taskDetails=[];
                stTable=$scope.stTableRef;
                stTable.pagination.start=0
            }else{
                $scope.stTableRef=stTable;
            }


            if(stTable.pagination.start===0){
                $scope.taskDetails=[];
            }
            //filters
            delete stTable.created_by;
            delete stTable.assigned_to;
            delete stTable.type;
            if($scope.f_type=='my_task'){
                stTable.created_by=$rootScope.userId;
                stTable.assigned_to=$rootScope.userId;
            }else if($scope.f_type=='due_task' || $scope.f_type=='upcoming_task'){
                stTable.type = $scope.f_type;
            }
            angular.extend(stTable,$scope.refParams);
            stTable.user_id=$rootScope.userId;
            $scope.loading = true;
            taskService.getTasks(stTable,'task_module_loader').then(function(result){
                $scope.nodata=false;
                if($scope.loadMore){
                    angular.forEach(result.data.data,function(item){
                        $scope.taskDetails.push(item);
                    });
                    if($scope.taskDetails.length<=0){
                        $scope.nodata=true;
                    }
                }else{
                    $scope.taskDetails=result.data.data;
                }
                $scope.loading = false;
                $scope.isEmptyRow = (result.data.data.length == 0) ? true : false;
                $scope.pendingTaskCount = result.data.pending_task;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_records/stTable.pagination.number);
            });
        };
        $scope.deleteTask=function(taskId){
            if($rootScope.confirmMsg()){
                taskService.deleteTask({'id_task': taskId}).then(function(result){
                    if($rootScope.toastMsg(result)){
                        $scope.tasksList();
                    }
                });
            }
        };
        $scope.assignedUser=function(assigned_to){
            if(assigned_to){
                var assignedArray=assigned_to.split(',');
                if(assignedArray.indexOf($rootScope.userId)!== -1){
                    return true;
                }
            }
            return false;
        }
        $scope.getUserList = function(){
            companyService.userList({'company_id': $rootScope.id_company}).then(function(result){
                $scope.companyUsers=result.data.data;
            });
        }

        $scope.addTasksModal=function(id_task,type){
            $scope.id_task=id_task;
            var modalInstance=$uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'partials/component/task/add-tasks-modal.html',
                resolve: {
                    item: function () {
                        if (id_task) {
                           return taskService.getTasks({'id_task': id_task}).then(function(result){
                                return result.data.data[0];
                            });
                        }
                    }
                },
                controller: function($scope,$uibModalInstance,taskService,item,Upload){

                    //$scope.tasksList();
                    $scope.cancel=function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.task={};
                    //console.log('item',item);
                    if(item){
                        $scope.task = item;

                    }
                    if(type){
                        $scope.currentView = type;
                    }else{
                        $scope.currentView = 'create'
                    }

                    //console.log('$scope.currentView',$scope.currentView);
                    $scope.submitStatus=false;
                    $scope.saveTask=function(task,form,submitType){
                        $scope.submitStatus=true;
                        if(form.$valid){
                            var params = {};
                            angular.extend(params,$scope.refParams);
                           /* angular.forEach($scope.refParams,function(value,key){
                                taskDetails[key]=value;
                            })*/
                            params.created_by=$rootScope.userId;
                            params.company_id=$rootScope.id_company;

                            if(!task.member||task.member.length==0){
                                params.member=[];
                                //params.member.push($rootScope.userId);
                            }else{
                                params.member = task.member;
                            }

                            params.task_name=task.task_name;
                            params.task_description=task.task_description;
                            params.task_due_date=task.task_due_date;
                            params.id_task=task.id_task;
                            params.file = task.file;
                            params.comment = task.comment;
                            //console.log('params',params);return false;
                           // delete task.assigned_to;
                           // delete task.attachment;
                            if(submitType == 'completed'){
                                params.task_status = 'completed';
                            }
                            taskService.saveTask(params).then(function(result){
                                if($rootScope.toastMsg(result)){
                                    $scope.cancel();
                                    $rootScope.resetForm(form);
                                    //$scope.task={};
                                    $scope.tasksList();
                                }
                            })

                            /* Upload.upload({
                             async: true,
                             url: API_URL+'Activity/task',
                             data: taskDetails
                             }).success(function(data){
                             if(data.status){
                             //$scope.callback(data.message);
                             $rootScope.toast('Success',data.message);
                             $scope.currentView='List';
                             $scope.task={};
                             $scope.tasksList();
                             }
                             else{
                             $rootScope.toast('Error',data.error,'error',$scope.sector);
                             }
                             });*/
                        }
                    };

                    $scope.updateTaskDetails={};
                    $scope.updateStatus=function(taskId,taskStatus){
                        $scope.updateTaskDetails={
                            'id_task': taskId,
                            'task_status': taskStatus
                        };
                        taskService.updateTaskStatus($scope.updateTaskDetails).then(function(result){
                            if($rootScope.toastMsg(result)){
                                $scope.cancel();
                                $scope.tasksList();
                                $scope.task={};
                            }
                        });
                    };

                }
            });
            modalInstance.result.then(function(){
                // $state.reload();
            },function(){
            });
        }

        if($stateParams.taskId){
            var id = decodeFilter($stateParams.taskId);
            $scope.addTasksModal(id, 'view');
        }
    })

    .directive('taskModule',function(){
        return {
            restrict: 'EA',
            controller: 'taskCtrl',
            scope: {
                getData: "=",
                tmLoadMore: '=',
                permission: '='
            },
            templateUrl: function(elem,attrs){
                var template='partials/component/task/task-module.html';
                if(attrs.templateUrl)
                    template=attrs.templateUrl;
                return template;
            },
            link: function(scope,element,attrs){
                scope.loadMore=true;
                if(scope.tmLoadMore===false){
                    scope.loadMore=false;
                }
                // scope.tasksList();
            }
        }
    })








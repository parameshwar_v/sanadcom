/**
 * Created by RAKESH on 13-04-2016.
 */
'use strict';
angular.module('meeting',[])
    .run(['$http','$rootScope',function($http,$rootScope) {
       /* $rootScope.createMeeting=function(){
            $scope.meetingModal();
        }*/
    }])
    .controller('meetingCtrl',function($rootScope,$scope,$stateParams,$uibModal,decodeFilter,meetingService){
        /**
         * meeting module
         */
        $rootScope.meetingReload =false;
        $scope.userId = $rootScope.userId;
        $scope.id_meeting = undefined;
        $scope.formType = 'create';
        $scope.noData = false;
        $scope.editMeeting = function (id_meeting,actionType) {
            $scope.formType = actionType;
            $scope.id_meeting = id_meeting;
            $scope.createMeeting(null,$scope.formType,id_meeting);
        };
        $scope.checkMeetingDate = function(date){
            //date compare
            var current_date = new Date();
            current_date.setHours(0, 0, 0, 0, 0);
            var meeting_date = new Date(date);
            meeting_date.setHours(0, 0, 0, 0, 0);
            if(current_date <= meeting_date){
               return true;
            }
            return false;
        }
        $scope.createMeeting = function (params,formType,id_meeting) {
            //$scope.id_project_approval = id_project_approval;
            $scope.params = {};
            angular.copy(params,$scope.params);
            $scope.formType = formType;
            var modalInstance = $uibModal.open({
                animation: true,
                windowClass: 'my-class',
                backdrop: 'static',
                keyboard: false,
                scope: $scope,
                openedClass: 'right-panel-modal modal-open',
                resolve: {
                    item: function () {
                       if (id_meeting) {
                           var param = {};
                           param.id_meeting =  $scope.id_meeting;
                           angular.extend(param,$scope.getData);
                            return meetingService.get(param).then(function ($result) {
                                if($result.status){
                                    return $result.data.data;
                                }else{
                                    //error message
                                }
                            });
                        }
                    }
                },
                templateUrl: 'partials/component/meeting/meeting-min-modal.html',
                controller: function ($scope, $rootScope, $uibModalInstance, item, companyService,meetingService) {
                    $scope.userList = [];
                    $scope.getUsers = function () {
                        companyService.users({'company_id': $rootScope.id_company}).then(function (result) {
                            $scope.userList = result.data;
                        });
                    }

                    $scope.minDate = $scope.minDate ? null : new Date();
                    $scope.meeting = $scope.params;
                    $scope.meeting.invitation_id = [];
                    $scope.meetingDocuments = [];
                    $scope.meetingDetails = {};
                    $scope.meetingDetails.attachment = [];
                    $scope.meeting.guest = [];
                    $scope.company_id = $rootScope.id_company;
                    $scope.timeList = function () {
                        var $time = [];
                        var $mintIncrement = 10;
                        var hr = 24;
                        for (var i = 0; i < 24; i++) {
                            for (var j = 0; j < 60; j = j + $mintIncrement) {
                                var $obj = {};
                                var jVal = (j < 10) ? '0' + '' + j : j;
                                var iVal = (i < 10) ? '0' + '' + i : i;
                                $obj.id = iVal + ':' + jVal;
                                $obj.value = iVal + ':' + jVal;
                                $time.push($obj);
                            }
                        }
                        $scope.timeListData  =  $time;
                    }

                    if (item) {
                        $scope.meeting = item;
                        if (item.guest != null) {
                            $scope.meeting.guest = item.guest.split(',');
                        }
                    }
                    $scope.saveMeeting = function ($meeting, form,status) {
                        if (form.$valid) {
                            var $formData = $meeting;
                            if(status){
                                $scope.meeting.meeting_status = status;
                            }
                            $formData.from_time = $meeting.from_time;
                            $formData.to_time = $meeting.to_time;
                            //$formData.project_id = $rootScope.currentProjectViewId;
                          //  $formData.project_approval_id = $scope.id_project_approval;
                            $formData.created_by = $rootScope.userId;
                            $formData.company_id = $rootScope.id_company;
                           /* $formData.meeting_name = $meeting.meeting_name;
                            $formData.where = $meeting.where;
                            $formData.when = $meeting.when;
                            $formData.from_time = $meeting.from_time;
                            $formData.to_time = $meeting.to_time;
                            $formData.agenda = $meeting.agenda;
                            $formData.meeting_mom = $meeting.meeting_mom;
                            $formData.meeting_status = $meeting.meeting_status;*/
                            if (item) {
                                $formData.id_meeting = item.id_meeting;
                            }
                           /* var $invitation = $meeting.invitation_id.map(function (obj) {
                                if (obj.id_user != undefined && obj.id_user != '') {
                                    return obj.id_user;
                                }
                            });*/
                            $formData.invitation_id = $meeting.invitation_id;

                            if($meeting.guest){
                                var guest = $meeting.guest.map(function (tag) {
                                    return tag.text;
                                });
                                $formData.guest = guest;
                            }

                            meetingService.post($formData).then(function(result){
                                if($rootScope.toastMsg(result)){
                                    //$scope.getMeetingList();
                                    $rootScope.meetingReload =true;
                                    $scope.cancel();

                                }
                            })
                        }
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
        $scope.meetingList = [];
        $scope.isEmptyRow = false;
        $scope.totalMeetingCount = 0;
        $scope.pendingMeetingsCount = 0;
        $scope.f_type ='all_meetings';
        $scope.getMeetingList = function(stTable){

            if(!stTable){
                $scope.meetingList = [];
                stTable = $scope.stTableRef;
                stTable.pagination.start = 0
            }else{
                $scope.stTableRef = stTable;
            }
            $rootScope.meetingReload = false;

            if(stTable.pagination.start === 0){
                $scope.meetingList = [];
            }
            //filters
            delete stTable.created_by;
            delete stTable.assigned_to;
            delete stTable.type;
            if($scope.f_type=='my_meetings'){
                stTable.created_by = $rootScope.userId;
                stTable.assigned_to= $rootScope.userId;
            }else if($scope.f_type=='due_meeting' || $scope.f_type=='upcoming_meeting'){
                stTable.type = $scope.f_type;
            }

            angular.extend(stTable,$scope.getData);
            stTable.user_id = $rootScope.userId;
            meetingService.get(stTable,'meeting_loader').then(function(result){
                $scope.noData = false;
                if($scope.loadMore){
                    angular.forEach(result.data.data,function(item){
                        $scope.meetingList.push(item);
                    })
                }else{
                    $scope.meetingList=result.data.data;
                }
                if($scope.meetingList.length<=0){
                    $scope.noData = true;
                }
                $scope.totalMeetingCount = result.data.total_meetings;
                $scope.pendingMeetingsCount = result.data.pending_meetings;
                $scope.isEmptyRow = (result.data.data.length == 0) ? true : false;
                stTable.pagination.numberOfPages=Math.ceil(result.data.total_meetings/stTable.pagination.number);
            })
        }
        if($stateParams.meetingId){
            var id =decodeFilter($stateParams.meetingId);
            $scope.editMeeting(id, 'view');
        }
    })
    .directive('meeting',function(){
        return {
            link: function(scope, element, attrs) {

            },
            controller:'meetingCtrl'
        }
    })

    .directive('meetingCreate',function(){
        return {
            restrict:'EA',
            controller:'meetingCtrl',
            link:function(scope,element,attrs){

            },
            templateUrl:function(elem,attrs){
                return 'html/partials/component/meeting/create-meeting.html'
            }
        }
    })
    .directive('meetingList',function(){
        return {
            restrict:'EA',
            controller:'meetingCtrl',
            scope :{
                reload:'=reload',
                getData:'=',
                tmLoadMore: '=',
                permission: '='
            },
            link:function(scope,element,attrs,rootScope){
                var params = scope.getData;
                scope.loadMore=true;
                if(scope.tmLoadMore===false){
                    scope.loadMore=false;
                }
                //scope.inital = true;
                scope.$watch("reload",function(newValue,oldValue) {
                    if(newValue || scope.inital){
                        scope.getMeetingList();
                        //scope.inital = false;
                    }
                });
            },
            templateUrl:function(elem,attrs){
                var template = 'partials/component/meeting/meeting-list-min.html';
                if(attrs.templateUrl)
                    template = attrs.templateUrl;
                return template;
            }
        }
    })






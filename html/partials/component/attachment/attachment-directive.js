/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 *
 * <attachment-view module-id="1" ref-id="{{currentContactViewId}}"></attachment-view>
 */
'use strict';
angular.module('attachment',[])
    .controller('attachmentsCtrl',function($scope,$rootScope,crmContactService,Upload,crmService){
        $scope.nodata = false;
        //get uploaded attachments
        $scope.userId = $rootScope.userId;
        $scope.showAttachmentDiv = false;
        $scope.getDocumentType= function(){
            crmContactService.getAttachmentsTypes({'module_id':$scope.crmModuleId,'form_key':$scope.form_key}).then(function(result){
                if(result.status){
                    $scope.documentsTypes=result.data;
                }
            });
        }
        $scope.getFiles=function(){
            $scope.isLoading = true;
            var params ={};
            params.module_id = $scope.crmModuleId;
            params.group_id = $scope.refId;
            params.form_key = $scope.form_key;
            params.field_key = $scope.field_key;
            crmContactService.getUploadedAttachments(params,false).then(function(result){
                $scope.isLoading = false;
                $scope.nodata = false;
                if(result.status){
                    $scope.uploadedDocuments=result.data;
                    if($scope.uploadedDocuments.length<=0){
                        $scope.nodata = true;
                    }
                    $scope.ngModel = result.data.map(function(obj){return obj.id_crm_document});
                }
            });
        }
        $scope.clear=function(){
            $scope.uploadImage=[];
        }
        $scope.subUploadFile = function (file,document_id) {
            $scope.progressId = document_id;
            console.log('file',file);

            if(file){
                Upload.upload({
                    url: API_URL+'Crm/document',
                    data:{
                        file:{'excel':file},
                        'group_id':$scope.refId,
                        'company_id':$rootScope.id_company,
                        'user_id':$rootScope.userId,
                        'document_type':this.document_type,
                        'description':this.description,
                        'module_id':$scope.crmModuleId,
                        'document_id':document_id

                    }
                }).then(function (resp) {
                    //console.log('resp',resp);
                    $rootScope.toast('Success',resp.data.message);
                    $scope.getFiles();
                }, function (resp) {
                    $rootScope.toast('Error',resp.data.error,'error');
                }, function (evt) {
                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                });

            }else{
                $rootScope.toast('Error','invalid format','image-error');
            }

        };
        $scope.deleteFile = function(type,documentId){
            var param = {};
            param.type = type;
            param.document_id =documentId;
            var r=confirm("Do you want to continue?");
            $scope.deleConfirm = r;
            if(r==true){
                $rootScope.loaderOverlay = false;
                crmService.document.delete(param).then(function(result){
                    if(result.status){
                        $scope.getFiles();
                        $rootScope.toast('Success',result.message);
                    }else{

                        $rootScope.toast('Error',result.error,'error');
                    }
                });
            }
        }
        $scope.save=function(upload,form){
            console.log('$scope',$scope);
            console.log('$rootScope',$rootScope);
            $scope.submitStatus=true;
            if(form.$valid){
                var $fileCount=1;
                $.each($scope.uploadImage,function(parentKey,parentvalue){
                    Upload.upload({
                        async:true,
                        url:API_URL+'Crm/document',
                        data:{
                            file:{'excel':parentvalue},
                            'group_id':$scope.refId,
                            'company_id':$rootScope.currentCompanyViewId,
                            'user_id':$rootScope.userId,
                            'document_type':this.document_type,
                            'description':this.description,
                            'module_id':$scope.crmModuleId,
                            'form_key':$scope.form_key,
                            'field_key':$scope.field_key,
                            'id_project_stage_section_form':$rootScope.formId
                        }
                    }).progress(function(e){
                        $.each($scope.uploadImage,function(value,key){
                            if(this.$$hashKey==parentvalue.$$hashKey){
                                this.progress=parseInt(100.0*e.loaded/e.total);
                            }
                        })
                    }).success(function(data){
                        if($scope.uploadImage.length==$fileCount){
                            $scope.getFiles();
                            $rootScope.toast('Success',data.message);
                            $scope.uploadImage=[];
                            $scope.submitStatus=false;
                        }
                        $fileCount++;
                        if(data.status){
                        }else{
                            $rootScope.toast('Error',data.error,'error',$scope.sector);
                        }
                    });
                });
            }
        }
        $scope.uploadImage=[];
        $scope.uploadFiles=function(file){

            if(!$.isArray(file)){
                return false;
            }
            if(file.length==0){
                $rootScope.toast('Error','invalid format','image-error');
            }
            $.each(file,function(key,value){
                value.document_type='';
                value.progress=0;
                value.description='';
                value.descriptionName='description_'+key;
                value.documentTypeName='document_type_'+key;
                $scope.uploadImage.push(value);
            });
        };
        $scope.removeImage=function(obj,index){
            var r=confirm("Do you want to continue?");
            if(r==true){
                $scope.uploadImage.splice(index,1);
            }
        };
        $scope.getDownloadUrl = function(id){
            crmContactService.getUrl({
                id: id,
            }).then(function (result) {
                var a         = document.createElement('a');
                a.href        = result.data.url;
                a.target      = '_blank';
                a.download    = result.data.file;
                document.body.appendChild(a);
                a.click();
            });
        };
    })
    .controller('attachmentsFilesCtrl',function($scope,$rootScope,crmContactService,Upload,crmService){

        $scope.getDocumentType= function(){
            crmContactService.getAttachmentsTypes({'module_id':$scope.crmModuleId,'form_key':$scope.form_key}).then(function(result){
                if(result.status){
                    $scope.documentsTypes=result.data;
                }
            });
        }
        //get uploaded attachments
        $scope.clear=function(){
            $scope.uploadImage=[];
        }

        $scope.uploadImage=[];
        $scope.ngModel = [];
        $scope.uploadFiles=function(file){
            //console.log('file',file);
            if(!$.isArray(file)){
                return false;
            }
            if(file.length==0){
                $rootScope.toast('Error','invalid format','image-error');
            }
            if(!$scope.allowMultiple){
                $scope.uploadImage = [];
            }
            angular.forEach(file,function(item){
                item.document_type = '';
                item.description='';
                $scope.uploadImage.push(item);
            })

            $scope.ngModel = $scope.uploadImage;
        };
        $scope.removeImage=function(index){
            var r=confirm("Do you want to continue?");
            if(r==true){
                $scope.uploadImage.splice(index,1);
                $scope.ngModel = $scope.uploadImage;
            }
        };
    })
    .directive('attachmentView',function(){
        return {
            restrict:'EA',
            controller:'attachmentsCtrl',
            scope: {
                ngModel:'=',
                getData:'='
            },
            link:function(scope,element,attrs,ngModel){
                scope.crmModuleId=attrs.moduleId;
                scope.refId=attrs.refId;
                scope.form_key=attrs.formKey;
                scope.field_key=attrs.fieldKey;
                scope.hideSelectType=true;
                if(attrs.hideSelectType){
                    scope.hideSelectType = false;
                }else{
                    scope.getDocumentType();
                }
                scope.showAddBtn=true;
                //console.log('attrs.showAddBtn',attrs.showAddBtn);
                if(attrs.showAddBtn=="false"){
                    //console.log('attrs.showAddBtn222',attrs.showAddBtn);
                    scope.showAddBtn = false;
                }

                if(attrs.title){
                    scope.title = attrs.title;
                }else{
                    scope.title = 'Documents';
                }

                scope.getFiles();

            },
            templateUrl:function(elem,attrs){
                return 'partials/component/attachment/view.html'
            }
        }
    })
    .directive('attachmentList',function(){
        return {
            restrict:'EA',
            controller:'attachmentsCtrl',
            link:function(scope,element,attrs){
                scope.crmModuleId=attrs.moduleId;
                scope.refId=attrs.refId;
                scope.form_key=attrs.formKey;
                scope.field_key=attrs.fieldKey;
                scope.getFiles();
            },
            templateUrl:function(elem,attrs){
                var template = 'partials/component/attachment/list.html';
                if(attrs.templateUrl)
                    template = attrs.templateUrl;
                return template;
            }
        }
    })
    .directive('attachmentFiles',function(){
        return {
            restrict:'EA',
            controller:'attachmentsFilesCtrl',
            scope: {
                ngModel:'='
            },
            link:function(scope,element,attrs,ngModel){
                scope.crmModuleId=attrs.moduleId;
                scope.form_key=attrs.formKey;

                scope.allowMultiple = true;
                if(attrs.allowMultiple=='false'){
                    scope.allowMultiple = false;
                }
                scope.showSelectType=false;
                if(attrs.showSelectType==='true'){
                    scope.getDocumentType();
                    scope.showSelectType = true;
                }
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/attachment/files.html'
            }
        }
    })
    .directive('attachmentListTemplate',function(){
        return {
            restrict: 'EA',
            scope:{
                getData:"=",
                attachmentDelete:"=",
                attachmentModule:"=",
                altConfig:'='
            },
            controller: function($scope, $rootScope, crmService, crmContactService){
                $scope.deleteAttachment = function(type,document){
                    console.log(type,document);
                    console.log($scope.moduleName);
                    console.log($scope.uploadedDocuments);
                    var param = {};
                    /*param.type = type;*/
                    if(document.id_crm_document)
                        param.document_id =document.id_crm_document;
                    if(document.id_crm_document_version)
                        param.document_id =document.id_crm_document_version;

                    if(document.parent){
                        param.type = 'document';
                    }else{
                        param.type = 'version';
                    }
                    var r=confirm("Do you want to continue?");
                    $scope.deleConfirm = r;
                    if(r==true){
                        $rootScope.loaderOverlay = false;
                        crmService.document.delete(param).then(function(result){
                            if(result.status){
                                if($scope.moduleName=='contact_assets'){
                                    console.log('#uploaded-files-'+document.id_crm_document);
                                    jQuery('#uploaded-files-'+document.id_crm_document).hide();
                                    $rootScope.getTangibleLiabilities();
                                }
                                $rootScope.toast('Success',result.message);
                            }else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        });
                    }
                };
                $scope.getDownloadUrl = function(objData){
                    console.log('asdasd');
                    console.log(objData);
                    var data = {};
                    if(objData.id_crm_document_version && !objData.parent){
                        data = {'id_crm_document_version': objData.id_crm_document_version};
                    }else if(objData.id_crm_document){
                        data = {'crm_document_id': objData.id_crm_document};
                    }
                    crmContactService.getUrl(data).then(function (result) {
                        var a         = document.createElement('a');
                        a.href        = result.data.url;
                        a.target      = '_blank';
                        a.download    = result.data.file;
                        document.body.appendChild(a);
                        a.click();
                    });
                };
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/attachment/list.html'
            },
            link: function(scope,element,attrs){
                scope.uploadedDocuments = scope.getData;

                if(scope.attachmentDelete)
                    scope.ShowDelete = true;
                else
                    scope.ShowDelete = false;

                if(attrs.attachmentModule=='contact_assets'){
                    scope.moduleName = 'contact_assets';
                }

                if(scope.altConfig){
                    scope.title = (scope.altConfig.hasOwnProperty('title'))?scope.altConfig.title:'Documents';
                }else{
                    scope.title = 'Attachments';
                }


            }
        }
    })





<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 21-06-2017
 * Time: 10:16 AM
 */
//$url = MONGO_SERVICE_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
include_once('config.php');
try {
    $input = $_GET;
    $collection = $db->user_access_log;
    $searchparameters = array();
    $searchparameters2 = array();
    if (!empty($input['fromdate']) && !empty($input['todate'])) {
        $searchparameters["keys.date_time"] = ["\$gte" => new MongoDate(strtotime($input['fromdate'])), "\$lte" => new MongoDate(strtotime($input['todate']))];
    }
    if (isset($input['ids'])) {
        $searchparameters["keys.user_id"] = ["\$in"=>explode(',',$input['ids'])];
    }
    if (isset($input['search_key'])) {
        $search_key=array();
        $input['search_key']=new MongoRegex("/^".$input['search_key']."/i");
        $search_key['keys.user_name']=$input['search_key'];
        /*$search_key['data.event_name']=$input['search_key'];
        $search_key['data.event_description']=$input['search_key'];
        $search_key['data.url']=$input['search_key'];
        $search_key['data.ip']=$input['search_key'];
        $search_key['data.user_agent.browser_name']=$input['search_key'];
        $search_key['data.user_agent.browser_version']=$input['search_key'];
        $search_key['data.user_agent.os']=$input['search_key'];*/
        $searchparameters['$OR'][] = $search_key;

    }
    /*echo "<pre>";print_r($searchparameters);echo "</pre>";*/
    //$searchparameters2=["skip"=>(int)$input['offset'],(int)$input['limit']];
    $projectStageVersions = $collection->find($searchparameters, $searchparameters2)->sort(['keys.date_time'=>-1]);
    //echo "<pre>";print_r($searchparameters);echo "</pre>";exit;
    $num_docs = $projectStageVersions->count();
    $projectStageVersions = $collection->find($searchparameters, $searchparameters2)->skip((int)$input['offset'])->limit((int)$input['limit'])->sort(['keys.date_time'=>-1]);
    if ($projectStageVersions) {
        $projectStageVersion_array = [];
        foreach ($projectStageVersions as $projectStageVersion) {
            if (isset($projectStageVersion['keys']['date_time']))
                $projectStageVersion['keys']['date_time'] = date('Y-m-d H:i:s', $projectStageVersion['keys']['date_time']->sec);
            $projectStageVersion_array[] = $projectStageVersion;
        }
        $response['rows'] = $projectStageVersion_array;
        $response['total_records'] =  $num_docs;
        $response['success'] = true;
    } else {
        $response['success'] = false;
    }
}
catch(Exception $e){
    $response['success'] = false;
}
header('Content-Type: application/json');
echo json_encode($response);
exit;
?>
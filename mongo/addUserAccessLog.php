<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 21-06-2017
 * Time: 05:06 PM
 */
include_once('config.php');
$collection = $db->user_access_log;
$addData=$_POST;
$addData = file_get_contents("php://input");
$addData=json_decode($addData);
if(isset($addData->log_auth_key)){
    if($addData->log_auth_key!=LOG_AUTH_KEY){
        $response=['success'=>false];
    }
    else {
        unset($addData->log_auth_key);
        $current_date_time = new MongoDate();
        if (!empty($addData->keys))
            $addData->keys->date_time = $current_date_time;
        if (!empty($addData->updated_date_time))
            $addData->updated_date_time = new MongoDate(strtotime($addData->updated_date_time));
        $res = $collection->insert($addData); // Inserting Document
        if ($res) {
            $response = ['success' => true];
        } else {
            $response = ['success' => false];
        }
    }
}
else{
    $response=['success'=>false];
}
$connection->close();
header('Content-Type: application/json');
echo json_encode($response);
exit;
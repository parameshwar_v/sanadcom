<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21-06-2017
 * Time: 10:15 AM
 */
include_once('config.php');
try {
    $input = $_GET;
    $collection = $db->audit_log;
    $searchparameters = array();
    $searchparameters2 = array();
    if (!empty($input['fromdate']) && !empty($input['todate'])) {
        $searchparameters["date_time"] = ["\$gte" => new MongoDate(strtotime($input['fromdate'])), "\$lte" => new MongoDate(strtotime($input['todate']))];
    }
    if (!empty($input['key']) && !empty($input['value'])) {
        $searchparameters["data.".$input['key'].""] = $input['value'];
    }
    else if(!empty($input['key'])){
        $searchparameters["data.".$input['key'].""] = ['$exists'=>1];
    }

    $facilities = $collection->find($searchparameters)->sort(["updated_date_time"=>-1]);
    $num_docs = $facilities->count();
    if(isset($input['limit']) && isset($input['offset']))
        $facilities = $collection->find($searchparameters)->limit((int)$input['limit'])->skip((int)$input['offset'])->sort(["updated_date_time"=>-1]);
    else
        $facilities = $collection->find($searchparameters)->sort(["updated_date_time"=>-1]);
    if ($facilities) {
        $facility_array = [];
        foreach ($facilities as $facility) {
            if (isset($facility['date_time']))
                $facility['date_time'] = date('Y-m-d H:i:s', $facility['date_time']->sec);
            if (isset($facility['updated_date_time']))
                $facility['updated_date_time'] = date('Y-m-d H:i:s', $facility['updated_date_time']->sec);
            $facility_array [] = $facility;
        }
        $response['rows'] = $facility_array;
        $response['total_records'] =  $num_docs;
        $response['success'] = true;
    } else {
        $response['success'] = false;
    }
}
catch(Exception $e){
    $response['success'] = false;
}
header('Content-Type: application/json');
echo json_encode($response);
exit;
?>
<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 20-06-2017
 * Time: 12:59 PM
 */
//http://lap2-tss/mongo-php/bamboo/findFacilityDetails.php?project_stage_info_id=69&project_id=37
include_once('config.php');
try {
    $input = $_GET;
    $collection = $db->facility_details;
    $searchparameters = array();
    $searchparameters2 = array();
    if (!empty($input['fromdate']) && !empty($input['todate'])) {
        $searchparameters["date_time"] = ["\$gte" => new MongoDate(strtotime($input['fromdate'])), "\$lte" => new MongoDate(strtotime($input['todate']))];
    }
    if (isset($input['project_id'])) {
        $searchparameters["project_id"] = $input['project_id'];
    }
    if (isset($input['project_stage_info_id'])) {
        $searchparameters["project_stage_info_id"] = $input['project_stage_info_id'];
    }
    if (isset($input['id_project_facility'])) {
        $searchparameters["facilities.id_project_facility"] = $input['id_project_facility'];
        $searchparameters2["facilities.id_project_facility.$"] = $input['id_project_facility'];
    }

    $facilities = $collection->find($searchparameters,$searchparameters2)->sort(['date_time'=>-1]);
    if ($facilities) {
        $facility_array = [];
        foreach ($facilities as $facility) {
            if (isset($facility['date_time']))
                $facility['date_time'] = date('Y-m-d H:i:s', $facility['date_time']->sec);
            $facility_array [] = $facility;
        }
        $response['data'] = $facility_array;
        $response['success'] = true;
    } else {
        $response['success'] = false;
    }
}
catch(Exception $e){
    $response['success'] = false;
}
header('Content-Type: application/json');
echo json_encode($response);
exit;
?>
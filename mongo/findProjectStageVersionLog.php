<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 21-06-2017
 * Time: 10:16 AM
 */
//$url = MONGO_SERVICE_URL.'findProjectStageVersionLog.php?project_stage_info_id='.$data['project_stage_info_id'].'&version_number='.$project_stage_info[0]['version_number'].'&project_stage_section_form_id='.$data['project_stage_section_form_id'];
include_once('config.php');
try {
    $input = $_GET;
    $collection = $db->project_stage_version_log;
    $searchparameters = array();
    $searchparameters2 = array();
    if (!empty($input['fromdate']) && !empty($input['todate'])) {
        $searchparameters["date_time"] = ["\$gte" => new MongoDate(strtotime($input['fromdate'])), "\$lte" => new MongoDate(strtotime($input['todate']))];
    }
    if (isset($input['version_number'])) {
        $searchparameters["version_number"] = $input['version_number'];
    }
    if (isset($input['project_stage_info_id'])) {
        $searchparameters["project_stage_info_id"] = $input['project_stage_info_id'];
    }
    if (isset($input['project_stage_section_form_id'])) {
        $searchparameters["forms.project_stage_section_form_id"] = $input['project_stage_section_form_id'];
        $searchparameters2["forms.project_stage_section_form_id.$"] = $input['project_stage_section_form_id'];
    }
    $projectStageVersions = $collection->find($searchparameters, $searchparameters2);
    if ($projectStageVersions) {
        $projectStageVersion_array = [];
        foreach ($projectStageVersions as $projectStageVersion) {
            if (isset($projectStageVersion['date_time']))
                $projectStageVersion['date_time'] = date('Y-m-d H:i:s', $projectStageVersion['date_time']->sec);
            $projectStageVersion_array[] = $projectStageVersion;
        }
        $response['data'] = $projectStageVersion_array;
        $response['success'] = true;
    } else {
        $response['success'] = false;
    }
}
catch(Exception $e){
    $response['success'] = false;
}
header('Content-Type: application/json');
echo json_encode($response);
exit;
?>
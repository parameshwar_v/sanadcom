<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21-06-2017
 * Time: 10:16 AM
 */
include_once('config.php');

$collection = $db->project_stage_version_log;
$addData=$_POST;
$addData = file_get_contents("php://input");
$addData=json_decode($addData);
if(isset($addData->log_auth_key)) {
    if($addData->log_auth_key!=LOG_AUTH_KEY){
        $response=['success'=>false];
    }
    else {
        unset($addData->log_auth_key);
        $current_date_time = new MongoDate();
        $addData->date_time = $current_date_time;
        $res = $collection->insert($addData); // Inserting Document
        if ($res) {
            $response = ['success' => true];
        } else {
            $response = ['success' => false];
        }
    }
}
else{
    $response = ['success' => false];
}
header('Content-Type: application/json');
echo json_encode($response);
exit;
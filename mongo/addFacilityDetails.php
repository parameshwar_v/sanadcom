<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 20-06-2017
 * Time: 12:58 PM
 */
include_once('config.php');

$collection = $db->facility_details;
$addData=$_POST;
$addData = file_get_contents("php://input");
$addData=json_decode($addData);
if(isset($addData->log_auth_key)) {
    if($addData->log_auth_key!=LOG_AUTH_KEY){
        $response=['success'=>false];
    }
    else {
        unset($addData->log_auth_key);
        $current_date_time = new MongoDate();
        $addData->date_time = $current_date_time;
        $collection->insert($addData); // Inserting Document
        $response = ['success' => true];
    }
}
else{
    $response = ['success' => false];
}
$connection->close();
header('Content-Type: application/json');
echo json_encode($response);
exit;